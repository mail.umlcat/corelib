/** Module: "coretxt<name>s.h"
 ** Descr.: "..."
 **/
 
// namespace coretxt<name>s {
 
// ------------------
 
#ifndef CORETXT<NAME>S__H
#define CORETXT<NAME>S__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"

// ------------------

bool /* func */ coretxt<name>s__try<name>2nstr
  (/* in */  const <name><*>  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

bool /* func */ coretxt<name>s__try<name>2nstrcount
  (/* in */  const <name><*>   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

void /* func */ coretxt<name>s__<name>2nstr
  (/* in */  const <name><*>   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

void /* func */ coretxt<name>s__<name>2nstrcount
  (/* in */  const <name><*>   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);



 // ...
 
// ------------------

/* override */ int /* func */ coretxt<name>s__setup
  ( noparams );

/* override */ int /* func */ coretxt<name>s__setoff
  ( noparams );

// ------------------

#endif // CORETXT<NAME>S__H

// } // namespace coretxt<name>s