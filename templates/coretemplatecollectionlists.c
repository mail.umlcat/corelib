/** Module: "core<name>s.c"
 ** Descr.: "predefined library"
 **/
 
// namespace core<name>s {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"
#include "core<name>nodes.h"

// ------------------
 
#include "core<name>s.h"

// ------------------

<name>* /* func */ core<name>s__createlist
  ( noparams )
{
  <name>* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

<name>* /* func */ core<name>s__createlistbytype
  (/* in */ typecode_t* /* param */ ADataType)
{
  <name>* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

<name>* /* func */ core<name>s__createlistrestrictcount
  (/* in */ count_t* /* param */ ACount)
{
  <name>* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func
 
<name>* /* func */ core<name>s__createlistbysize
  (/* in */ size_t* /* param */ ADataSize)
{
  <name>* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ core<name>s__drop<name>
  (/* out */ <name>** /* param */ AList);
{
  coresystem__nothing();
} // func

// ------------------

bool /* func */ core<name>s__underiteration
  (/* in */ <name>* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
  
  if (AList != NULL)
  {
  	Result = (AList->IteratorCount > 0);
  }
     
  // ---
  return Result;
} // func

<name>iterator* /* func */ core<name>s__createiteratorforward
  (/* in */ <name>* /* param */ AList)
{
  <name>iterator* /* var */ Result = NULL;
  // ---
     
  if (AList != NULL)
  {
  	Result = (AList->IteratorCount > 0);
  }
  
  // ---
  return Result;
} // func

<name>iterator* /* func */ core<name>s__createiteratorbackward
  (/* in */ <name>* /* param */ AList)
{
  <name>iterator* /* var */ Result = NULL;
  // ---
     
  Result = (AList != NULL);
  if (Result)
  {
     coresystem__nothing();
  } // if
 
  // ---
  return Result;
} // func

void /* func */ core<name>s__dropiterator
  (/* out */ <name>iterator** /* param */ AIterator)
{
  Result = (AIterator != NULL);
  if (Result)
  {
    coresystem__nothing();
  } // if
} // func

// ------------------

bool /* func */ core<name>s__isdone
  (/* in */ const <name>iterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    coresystem__nothing();
  } // if

  // ---
  return Result;
} // func

bool /* func */ core<name>s__trymovenext
  (/* inout */ <name>iterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (AIterator != NULL);
  if (Result)
  {
    <name>iteratorheader** /* var */ ThisIterator = NULL;
    ThisIterator = (<name>iteratorheader**) AIterator;

    Result =
      (ThisIterator->CurrentItem == ThisIterator->BottomMarker);
 
      if (Result)
      {
        core<name>nodes__trymovebackward(&(ThisIterator->CurrentItem));      
      }
  } // if
     
  // ---
  return Result;
} // func

void /* func */ core<name>s__movenext
  (/* inout */ <name>iterator* /* param */ AIterator)
{
  Result = (AIterator != NULL);
  if (Result)
  {
    coresystem__nothing();
  } // if
} // func

bool /* func */ core<name>s__trygetitem
  (/* inout */ <name>iterator* /* param */ AIterator
   /* out */   pointer**      /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      <name>iteratorheader* /* var */ ThisIterator = NULL;
      <name>nodeheader*     /* var */ ThisNode = NULL;

      ThisIterator = (<name>iteratorheader*) AIterator;
      ThisNode =
        (<name>nodeheader*) ThisIterator->CurrentNode;

      corememory__safecopy
        (AItem, ThisNode, ThisIterator->ItemsSize);
    } // if
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ core<name>s__trysetitem
  (/* inout */ <name>iterator* /* param */ AIterator
   /* in */ const  pointer*   /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      <name>iteratorheader* /* var */ ThisIterator = NULL;
      <name>nodeheader*     /* var */ ThisNode = NULL;

      ThisIterator = (<name>iteratorheader*) AIterator;
      ThisNode =
        (<name>nodeheader*) ThisIterator->CurrentNode;

      corememory__safecopy
        (ThisNode, AItem, ThisIterator->ItemsSize);
    } // if
  } // if
     
  // ---
  return Result;
} // func

void /* func */ core<name>s__getitem
  (/* inout */ <name>iterator* /* param */ AIterator
   /* out */   pointer*       /* param */ AItem)
{
  /* discard */ core<name>s__trygetitem
    (AIterator, AItem);
} // func

void /* func */ core<name>s__setitem
  (/* inout */ <name>iterator* /* param */ AIterator
   /* in */ const  pointer*   /* param */ AItem)
{
  /* discard */ core<name>s__trygetitem
    (AIterator, AItem);
} // func

// ------------------

count_t /* func */ core<name>s__getcurrentcount
  (/* in */ const <name>* /* param */ AList)
{
  count_t /* var */ Result = 0;
  // ---
     
  if (AList != NULL)
  {
    <name>header* /* var */ ThisList = NULL;
    ThisList = (<name>header*) AList;
    Result = ThisList->ItemsCurrentCount;
  } // if
  
  // ---
  return Result;
} // func

count_t /* func */ core<name>s__getmaxcount
  (/* in */ const <name>* /* param */ AList)
{
  count_t /* var */ Result = 0;
  // ---
     
  if (AList != NULL)
  {
    <name>header* /* var */ ThisList = NULL;
    ThisList = (<name>header*) AList;
    Result = ThisList->ItemsMaxCount;
  } // if
  
  // ---
  return Result;
} // func

// ------------------

bool /* func */ core<name>s__tryclear
  (/* inout */ <name>* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
     
  if (AList != NULL)
  {
  	Result = (AList->IteratorCount > 0);
  }
   
  // ---
  return Result;
} // func

bool /* func */ core<name>s__isempty
  (/* in */ const <name>* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
     
  if (AList != NULL)
  {
  	Result = (AList->IteratorCount > 0);
  }
  
  // ---
  return Result;
} // func

bool /* func */ core<name>s__hasitems
  (/* in */ const <name>* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
     
  Result = !(core<name>s__isempty(AList));
     
  // ---
  return Result;
} // func

void /* func */ core<name>s__clear
  (/* inout */ <name>* /* param */ AList)
{
  if (AList != NULL)
  {
  	Result = (AList->IteratorCount > 0);
  }
} // func

// ------------------

/* override */ const ansinullstring* /* func */ core<name>s__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "core<name>s";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ core<name>s__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ core<name>s__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

 
 // ...

// } // namespace core<name>s