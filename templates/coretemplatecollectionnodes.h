/** Module: "core<name>nodes.h"
 ** Descr.: "..."
 **/
 
// namespace core<name>nodes {
 
// ------------------
 
#ifndef CORE<NAME>NODES__H
#define CORE<NAME>NODES__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

typedef
  pointer* /* as */ <name>node;

struct <name>nodeheader
{
  //<name>node* /* var */ Prev;
  //<name>node* /* var */ Next;  
  
  pointer*              /* var */ Data;
} ;

// ------------------

/* friend */ void /* func */ core<name>nodes__link
  (/* inout */ <name>node* /* param */ A,
   /* inout */ <name>node* /* param */ B);

/* friend */ void /* func */ core<name>nodes__moveforward
  (/* inout */ <name>node** /* param */ AListNode);

/* friend */ void /* func */ core<name>nodes__movebackward
  (/* inout */ <name>node** /* param */ AListNode);

// ------------------

<name>node* /* func */ core<name>nodes__createnode
  (/* in */ pointer* /* param */ AData);

pointer* /* func */ core<name>nodes__dropnode
  (/* out */ <name>node** /* param */ AListNode);

// ------------------


 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ core<name>s__modulename
  ( noparams );

/* override */ int /* func */ core<name>s__setup
  ( noparams );

/* override */ int /* func */ core<name>s__setoff
  ( noparams );

// ------------------

#endif // CORE<NAME>NODES__H

// } // namespace core<name>nodes