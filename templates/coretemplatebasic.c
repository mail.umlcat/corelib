/** Module: "core<name>s.c"
 ** Descr.: "predefined library"
 **/
 
// namespace core<name>s {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------
 
#include "core<name>s.h"
 
// ------------------

comparison /* func */ core<name>s__compare
  (/* in */ ansinullstring* /* param */ A, B)
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func
   
 // ------------------
 
void /* func */ core<name>s__clear
  (/* inout */ ansinullstring* /* param */ ADest)
{
  // ...
} // func
 
 // ...

// ------------------

/* override */ const ansinullstring* /* func */ core<name>s__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "core<name>s";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ core<name>s__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ core<name>s__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace core<name>s