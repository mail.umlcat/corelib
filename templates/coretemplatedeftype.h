/** Module: "core<name>s.h"
 ** Descr.: "..."
 **/
 
// namespace core<name>s {
 
// ------------------
 
#ifndef CORE<NAME>S__H
#define CORE<NAME>S__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

//$define void noparams

// ------------------
 
//typedef
//  sometype         /* as */ sometype;
   
// ------------------

// similar to exception
struct <name>header
{
  //uuid     /* var */ Identifier;
  
  // "ansistring" by now
  //pointer* /* var */ Message;
  
  // recasted from another exception
  //warning* /* var */ Parent;
} ;

typedef
  <name>header /* as */ <name>;
typedef
  <name>       /* as */ <name>_t;
typedef
  <name>*      /* as */ <name>_p;

// ------------------

bool /* func */ core<name>s__areequal
  (/* in */ const /* restricted */ <name>* /* param */ A,
   /* in */ const /* restricted */ <name>* /* param */ B);

comparison /* func */ core<name>s__compare
  (/* in */ const /* restricted */ <name>* /* param */ A,
   /* in */ const /* restricted */ <name>* /* param */ B);

// ------------------

void /* func */ core<name>s__clear
  (/* inout */ <name>* /* param */ ADest);

 // ------------------

bool /* func */ core<name>s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ core<name>s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ core<name>s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);


 // ...
 
// ------------------

/* override */ int /* func */ core<name>s__setup
  ( noparams );

/* override */ int /* func */ core<name>s__setoff
  ( noparams );

// ------------------

#endif // CORE<NAME>S__H

// } // namespace core<name>s