/** Module: "coretxt<name>s.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coretxt<name>s {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"

// ------------------

#include "coretxt<name>s.h"
 
// ------------------

bool /* func */ coretxt<name>s__try<name>2nstr
  (/* in */  const <name><*>   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coretxt<name>s__try<name>2nstrcount
  (/* in */  const <name><*>   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coretxt<name>s__<name>2nstr
  (/* in */  const <name><*>   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  /* discard */ coretxt<name>__try<name>2nstr
    (ASource, ADest);
} // func

void /* func */ coretxt<name>s__<name>2nstrcount
  (/* in */  const <name><*>   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  /* discard */ coretxt<name>__try<name>2nstrcount
    (ASource, ADest, ADestSize);
} // func

// ------------------

/* override */ int /* func */ coretxt<name>s__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coretxt<name>s__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------


 // ...

// } // namespace coretxt<name>s