/** Module: "core<name>s.h"
 ** Descr.: "..."
 **/
 
// namespace core<name>s {
 
// ------------------
 
#ifndef CORE<NAME>S__H
#define CORE<NAME>S__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

//$define void noparams

// ------------------
 
//typedef
//  sometype         /* as */ sometype;
   
// ------------------

//struct failure
//{
//  ansichar /* var */ Mesage;
//  pointer* /* var */ Parent;
//} ;

// ------------------

//pointer* /* func */ core<name>s__malloc
//  (/* in */ size_t /* param */ ASize);
  
//void /* func */ core<name>s__dealloc
//  (/* out */ pointer** /* param */ ADest,
//   /* in */  size_t    /* param */ ASize);

 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ core<name>s__modulename
  ( noparams );

/* override */ int /* func */ core<name>s__setup
  ( noparams );

/* override */ int /* func */ core<name>s__setoff
  ( noparams );

// ------------------

#endif // CORE<NAME>S__H

// } // namespace core<name>s