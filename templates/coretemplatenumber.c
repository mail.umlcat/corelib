/** Module: "core<name>s.c"
 ** Descr.: "predefined library"
 **/
 
// namespace core<name>s {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "core<name>s.h"
 
// ------------------

comparison /* func */ core<name>s__compare
  (/* in */ const <name>_t /* param */ A,
   /* in */ const <name>_t /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ core<name>s__areequal
  (/* in */ const <name>_t /* param */ A,
   /* in */ const <name>_t /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func*/ core<name>s__isempty
  (/* in */ const <name>_t /* param */ ASource);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ core<name>s__clear
  (/* inout */ <name>_t /* param */ ADest);
{
  coresystem__nothing();
} // func

// ------------------

/* inline */ bool /* func */ core<name>s__ptry<name>tohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const <name>_p      param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

/* inline */ bool /* func */ core<name>s__ptry<name>tosfwmaxfloat
// (   out    maxsfwmaxfloat*      param    ADest,
//     in     const <name>_p      param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

 

bool /* func */ core<name>s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ core<name>s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ core<name>s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

/* override */ void /* func */ core<name>s__setup
  ( noparams )
{
  coresystem__nothing();
} // func

/* override */ void /* func */ core<name>s__setoff
  ( noparams )
{
  coresystem__nothing();
} // func

// ------------------


 // ...

// } // namespace core<name>s