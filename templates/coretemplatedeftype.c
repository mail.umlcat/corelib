/** Module: "core<name>s.c"
 ** Descr.: "predefined library"
 **/
 
// namespace core<name>s {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "core<name>.h"
 
// ------------------

bool /* func */ core<name>s__areequal
  (/* in */ const /* restricted */ <name>* /* param */ A,
   /* in */ const /* restricted */ <name>* /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

comparison /* func */ core<name>s__compare
  (/* in */ const /* restricted */ <name>* /* param */ A,
   /* in */ const /* restricted */ <name>* /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

void /* func */ core<name>s__clear
  (/* inout */ <name>* /* param */ ADest);
{
  coresystem__nothing();
} // func

 // ------------------

bool /* func */ core<name>s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ core<name>s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ core<name>s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

// ------------------

/* override */ int /* func */ core<name>s__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ core<name>s__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------


 // ...

// } // namespace core<name>s