/** Module: "core<name>s.h"
 ** Descr.: "..."
 **/
 
// namespace core<name>s {
 
// ------------------
 
#ifndef CORE<NAME>S__H
#define CORE<NAME>S__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"
#include "core<name>nodes.h"

// ------------------

typedef
  pointer* /* as */ <name>;

struct <name>header
{
  // pointer to node before first node
  //<name>node* /* var */ TopMarker;
  
  // pointer to node before last node
  //<name>node* /* var */ BottomMarker;

  // only applies when items are same type
  // "unknowntype" value means different type
  typecode /* var */ ItemsType;

  // applies instead of "typecode"
  // "0" means not used
  size_t   /* var */ ItemsSize;

  // restricts how many items can be stored
  // "0" means not used
  count_t  /* var */ ItemsMaxCount;

  // how many items are currently stored
  count_t  /* var */ ItemsCurrentCount;
  
  // indicates iterator related objects
  count_t  /* var */ IteratorCount;
  
  // ...
} ;

typedef
  <name>header  /* as */ <name>_t;
typedef
  <name>header* /* as */ <name>_p;
  
typedef
  <name>header  /* as */ core<name>s__<name>_t;
typedef
  <name>header* /* as */ core<name>s__<name>_p;

// ------------------

typedef
  pointer* /* as */ <name>iterator;

struct <name>iteratorheader
{
  // backreference to collection
  <name>*     /* var */ MainList

  <name>node* /* var */ CurrentItem;

  count_t               /* var */ ItemsCount;
  size_t                /* var */ ItemsSize;

  listdirections        /* var */ Direction;
  
  // ...
} ;

typedef
  <name>iteratorheader  /* as */ <name>iterator_t;
typedef
  <name>iteratorheader* /* as */ <name>iterator_p;

typedef
  <name>iteratorheader  /* as */ core<name>s__<name>iterator_t;
typedef
  <name>iteratorheader* /* as */ core<name>s__<name>iterator_p;

// ------------------

<name>* /* func */ core<name>s__create<name>
  ( noparams );

<name>* /* func */ core<name>s__create<name>bytype
  (/* in */ typecode_t* /* param */ ADataType);

<name>* /* func */ core<name>s__create<name>restrictcount
  (/* in */ count_t* /* param */ ACount);
  
<name>* /* func */ core<name>s__create<name>bysize
  (/* in */ size_t* /* param */ ADataSize);

void /* func */ core<name>s__drop<name>
  (/* out */ <name>** /* param */ AList);

// ------------------

bool /* func */ core<name>s__underiteration
  (/* in */ <name>* /* param */ AList)

<name>iterator* /* func */ core<name>s__createiteratorforward
  (/* in */ <name>* /* param */ AList);

<name>iterator* /* func */ core<name>s__createiteratorbackward
  (/* in */ <name>* /* param */ AList);

void /* func */ core<name>s__dropiterator
  (/* out */ <name>iterator** /* param */ AIterator);

// ------------------

bool /* func */ core<name>s__isdone
  (/* in */ const <name>iterator* /* param */ AIterator);

bool /* func */ core<name>s__trymovenext
  (/* inout */ <name>iterator* /* param */ AIterator);

void /* func */ core<name>s__movenext
  (/* inout */ <name>iterator* /* param */ AIterator);

bool /* func */ core<name>s__trygetitem
  (/* inout */ <name>iterator* /* param */ AIterator
   /* out */   pointer*        /* param */ AItem);

bool /* func */ core<name>s__trysetitem
  (/* inout */ <name>iterator* /* param */ AIterator
   /* in */ const pointer*     /* param */ AItem);

void /* func */ core<name>s__getitem
  (/* inout */ <name>iterator* /* param */ AIterator
   /* out */   pointer*        /* param */ AItem);

void /* func */ core<name>s__setitem
  (/* inout */ <name>iterator* /* param */ AIterator
   /* in */ const pointer*     /* param */ AItem);

// ------------------

void /* func */ core<name>s__assignequalityfunc
  (/* inout */ <name>*  /* param */ AList,
   /* in */    equality /* param */ AMatchFunc);

// ------------------

count_t /* func */ core<name>s__getcurrentcount
  (/* in */ const <name>* /* param */ AStack);

count_t /* func */ core<name>s__getmaxcount
  (/* in */ const <name>* /* param */ AStack);

// ------------------

bool /* func */ core<name>s__tryclear
  (/* inout */ <name>* /* param */ AList);

bool /* func */ core<name>s__isempty
  (/* in */ const <name>* /* param */ AList);

bool /* func */ core<name>s__hasitems
  (/* in */ const <name>* /* param */ AList);

void /* func */ core<name>s__clear
  (/* inout */ <name>* /* param */ AList);

// ------------------


 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ core<name>s__modulename
  ( noparams );

/* override */ int /* func */ core<name>s__setup
  ( noparams );

/* override */ int /* func */ core<name>s__setoff
  ( noparams );

// ------------------

#endif // CORE<NAME>S__H

// } // namespace core<name>s