/** Module: "core<name>nodes.c"
 ** Descr.: "predefined library"
 **/
 
// namespace core<name>nodes {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "core<name>nodes.h"
 
// ------------------

/* friend */ void /* func */ core<name>node__link
  (/* inout */ <name>node* /* param */ A,
   /* inout */ <name>node* /* param */ B)
{
  // ...
} // func

/* friend */ void /* func */ core<name>node__moveforward
  (/* inout */ <name>node** /* param */ AListNode)
{
  // ...
} // func

/* friend */ void /* func */ core<name>node__movebackward
  (/* inout */ <name>node** /* param */ AListNode)
{
  // ...
} // func

// ------------------

<name>node* /* func */ core<name>node__createnode
  (/* in */ pointer* /* param */ AData);
{
  <name>node* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

pointer* /* func */ core<name>node__dropnode
  (/* out */ <name>node** /* param */ AListNode);
{
  pointer* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* override */ const ansinullstring* /* func */ core<name>s__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "core<name>s";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ core<name>s__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ core<name>s__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

 
 // ...

// } // namespace core<name>nodes