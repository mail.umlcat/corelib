/** Module: "core<name>s.h"
 ** Descr.: "..."
 **/
 
// namespace core<name>s {
 
// ------------------
 
#ifndef CORE<NAME>S__H
#define CORE<NAME>S__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ core<name>s__compare
  (/* in */ const <name>_t /* param */ A,
   /* in */ const <name>_t /* param */ B);

bool /* func */ core<name>s__areequal
  (/* in */ const <name>_t /* param */ A,
   /* in */ const <name>_t /* param */ B);

bool /* func*/ core<name>s__isempty
  (/* in */ const <name>_t /* param */ ASource);

void /* func */ core<name>s__clear
  (/* inout */ <name>_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ core<name>s__ptry<name>tohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const <name>_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ core<name>s__ptry<name>tosfwmaxfloat
// (   out    sfwmaxfloat_t*       param    ADest,
//     in     const <name>_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

/* inline */ void /* func */ core<name>s__p<name>tohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const <name>_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ void /* func */ core<name>s__p<name>tosfwmaxfloat
// (   out    sfwmaxfloat_t*       param    ADest,
//     in     const <name>_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

 // ------------------

bool /* func */ core<name>s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ core<name>s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ core<name>s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ core<name>s__setup
  ( noparams );

/* override */ void /* func */ core<name>s__setoff
  ( noparams );

// ------------------

#endif // CORE<NAME>S__H

// } // namespace core<name>s