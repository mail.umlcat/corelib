/** Module: "coretxtshared.c"
 ** Descr.: "Predefined library for shared operations,"
 **         "for conversion between text and other types."
 **/
 
// namespace coretxtshared {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
//#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------
 
#include "coretxtshared.h"
 
// ------------------

// --> global properties

const ansinullstring* /* func */ coretxtshared__decdigset
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "0123456789";
  
  return (const ansinullstring*) setbuffer;
} // func

const ansinullstring* /* func */ coretxtshared__hexdecdigset
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "0123456789abcdegABCDEF";
  
  return (const ansinullstring*) setbuffer;
} // func

const ansinullstring* /* func */ coretxtshared__octdigset
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "01234567";
  
  return (const ansinullstring*) setbuffer;
} // func

const ansinullstring* /* func */ coretxtshared__bindigset
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "01";
  
  return (const ansinullstring*) setbuffer;
} // func

const ansinullstring* /* func */ coretxtshared__signeddecdigset
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "0123456789+-";
  
  return (const ansinullstring*) setbuffer;
} // func

const ansinullstring* /* func */ coretxtshared__floatdigset
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "0123456789+-eE.";
  
  return (const ansinullstring*) setbuffer;
} // func

// ------------------

index_t /* func */ coretxtshared__indexofnondeccount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize)
{
  index_t /* var */ Result = -1;
  // ---

  bool /* var */ IsValid = false;
  
  IsValid =
    (ASourcePtr != NULL) && (ASourceSize != 0);
  if (IsValid) 
  {
    ansichar* /* var */ C = NULL;
    bool      /* var */ Match     = false;

    bool      /* var */ CanExit   = false;
    index_t   /* var */ EachIndex = 0;

    ansichar*  /* var */ DecDigits =
      coretxtshared__decdigset;
    index_t    /* var */ DecDigitsSize =
      strlen(DecDigits);

    C = ASourcePtr;
    
    while (CanExit)
    {
      Match =
        (coreansinullstrs__firstptrofchar
          (DecDigits, C, DecDigitsSize) != NULL);
           
      CanExit =
        (!Match) || (EachIndex >= ASourceSize);
      
      EachIndex++;
    } // while
    
    if (!Match)
    {
      Result = (EachIndex - 1);
    }
  } // if
  
  // ---
  return Result;
} // func

bool /* func */ coretxtshared__onlydeccount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (coretxtshared__indexofnondeccount
      (ASourcePtr, ASourceSize) < 0);
  
  // ---
  return Result;
} // func

bool /* func */ coretxtshared__onlydec
  (/* in */ ansinullstring* /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
  size_t /* var */ ASourceSize =
    coreansinullstrs__getlength(ASource);
  Result =
    (coretxtshared__indexofnondeccount
      (ASourcePtr, ASourceSize) < 0);

  // ---
  return Result;
} // func

// ------------------

index_t /* func */ coretxtshared__indexofnonhexcount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize)
{
  index_t /* var */ Result = -1;
  // ---

  bool /* var */ IsValid = false;
  
  IsValid =
    (ASourcePtr != NULL) && (ASourceSize != 0);
  if (IsValid) 
  {
    ansichar* /* var */ C = NULL;
    bool      /* var */ Match     = false;

    bool      /* var */ CanExit   = false;
    index_t   /* var */ EachIndex = 0;

    ansichar*  /* var */ HexDecDigits =
      coretxtshared__hexdecdigset;
    index_t    /* var */ HexDecDigitsSize =
      strlen(HexDecDigits);

    C = ASourcePtr;
    
    while (CanExit)
    {
      Match =
        (coreansinullstrs__firstptrofchar
          (HexDecDigits, C, HexDecDigitsSize) != NULL);
           
      CanExit =
        (!Match) || (EachIndex >= ASourceSize);
      
      EachIndex++;
    } // while
    
    if (!Match)
    {
      Result = (EachIndex - 1);
    }
  } // if
  
  // ---
  return Result;
} // func

bool /* func */ coretxtshared__onlyhexdeccount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (coretxtshared__indexofnonhexdeccount
      (ASourcePtr, ASourceSize) < 0);
 
  // ---
  return Result;
} // func
  
bool /* func */ coretxtshared__onlyhexdec
  (/* in */ ansinullstring* /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
  size_t /* var */ ASourceSize =
    coreansinullstrs__getlength(ASource);
  Result =
    (coretxtshared__indexofnonhexdeccount
      (ASourcePtr, ASourceSize) < 0);
     
  // ---
  return Result;
} // func

// ------------------

index_t /* func */ coretxtshared__indexofnonoctcount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize)
{
  index_t /* var */ Result = -1;
  // ---

  bool /* var */ IsValid = false;
  
  IsValid =
    (ASourcePtr != NULL) && (ASourceSize != 0);
  if (IsValid) 
  {
    ansichar* /* var */ C = NULL;
    bool      /* var */ Match     = false;

    bool      /* var */ CanExit   = false;
    index_t   /* var */ EachIndex = 0;

    ansichar*  /* var */ OctDigits =
      coretxtshared__octdigset;
    index_t    /* var */ OctDigitsSize =
      strlen(OctDigits);

    C = ASourcePtr;
    
    while (CanExit)
    {
      Match =
        (coreansinullstrs__firstptrofchar
          (OctDigits, C, OctDigitsSize) != NULL);
           
      CanExit =
        (!Match) || (EachIndex >= ASourceSize);
      
      EachIndex++;
    } // while
    
    if (!Match)
    {
      Result = (EachIndex - 1);
    }
  } // if
  
  // ---
  return Result;
} // func

bool /* func */ coretxtshared__onlyoctcount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (coretxtshared__indexofnonoctcount
      (ASourcePtr, ASourceSize) < 0);
 
  // ---
  return Result;
} // func

bool /* func */ coretxtshared__onlyoct
  (/* in */ ansinullstring* /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
  size_t /* var */ ASourceSize =
    coreansinullstrs__getlength(ASource);
  Result =
    (coretxtshared__indexofnonoctcount
      (ASourcePtr, ASourceSize) < 0);
     
  // ---
  return Result;
} // func

// ------------------

index_t /* func */ coretxtshared__indexofnonbincount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize)
{
  index_t /* var */ Result = -1;
  // ---

  bool /* var */ IsValid = false;
  
  IsValid =
    (ASourcePtr != NULL) && (ASourceSize != 0);
  if (IsValid) 
  {
    ansichar* /* var */ C = NULL;
    bool      /* var */ Match     = false;

    bool      /* var */ CanExit   = false;
    index_t   /* var */ EachIndex = 0;

    ansichar*  /* var */ BinDigits =
      coretxtshared__bindigset;
    index_t    /* var */ BinDigitsSize =
      strlen(BinDigits);

    C = ASourcePtr;
    
    while (CanExit)
    {
      Match =
        (coreansinullstrs__firstptrofchar
          (BinDigits, C, BinDigitsSize) != NULL);
           
      CanExit =
        (!Match) || (EachIndex >= ASourceSize);
      
      EachIndex++;
    } // while
    
    if (!Match)
    {
      Result = (EachIndex - 1);
    }
  } // if
  
  // ---
  return Result;
} // func

bool /* func */ coretxtshared__onlybincount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (coretxtshared__indexofnonbincount
      (ASourcePtr, ASourceSize) < 0);

  // ---
  return Result;
} // func

bool /* func */ coretxtshared__onlybin
  (/* in */ ansinullstring* /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
  size_t /* var */ ASourceSize =
    coreansinullstrs__getlength(ASource);
  Result =
    (coretxtshared__indexofnonbincount
      (ASourcePtr, ASourceSize) < 0);

  // ---
  return Result;
} // func

// ------------------

index_t /* func */ coretxtshared__indexofnonsigneddeccount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize);
{
  index_t /* var */ Result = -1;
  // ---

  bool /* var */ IsValid = false;
  
  IsValid =
    (ASourcePtr != NULL) && (ASourceSize != 0);
  if (IsValid) 
  {
    ansichar* /* var */ C = NULL;
    bool      /* var */ Match     = false;

    bool      /* var */ CanExit   = false;
    index_t   /* var */ EachIndex = 0;

    ansichar*  /* var */ SignedDecDigits =
      coretxtshared__signeddecdigdigset;
    index_t    /* var */ SignedDecDigitsSize =
      strlen(SignedDecDigits);

    C = ASourcePtr;
    
    while (CanExit)
    {
      Match =
        (coreansinullstrs__firstptrofchar
          (SignedDecDigits, C, SignedDecDigitsSize) != NULL);
           
      CanExit =
        (!Match) || (EachIndex >= ASourceSize);
      
      EachIndex++;
    } // while
    
    if (!Match)
    {
      Result = (EachIndex - 1);
    }
  } // if
  
  // ---
  return Result;
} // func

bool /* func */ coretxtshared__onlysigneddeccount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (coretxtshared__indexofnonsigneddeccount
      (ASourcePtr, ASourceSize) < 0);

  // ---
  return Result;
} // func

bool /* func */ coretxtshared__onlysigneddec
  (/* in */ ansinullstring* /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (coretxtshared__indexofnonsigneddeccount
      (ASourcePtr, ASourceSize) < 0);

  // ---
  return Result;
} // func

// ------------------

index_t /* func */ coretxtshared__indexofnonfloatdeccount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize)
{
  index_t /* var */ Result = -1;
  // ---

  bool /* var */ IsValid = false;
  
  IsValid =
    (ASourcePtr != NULL) && (ASourceSize != 0);
  if (IsValid) 
  {
    ansichar* /* var */ C = NULL;
    bool      /* var */ Match     = false;

    bool      /* var */ CanExit   = false;
    index_t   /* var */ EachIndex = 0;

    ansichar*  /* var */ FloatDecDigits =
      coretxtshared__floatdecdigdigset;
    index_t    /* var */ FloatDecDigitsSize =
      strlen(FloatDecDigits);

    C = ASourcePtr;
    
    while (CanExit)
    {
      Match =
        (coreansinullstrs__firstptrofchar
          (FloatDecDigits, C, FloatDecDigitsSize) != NULL);
           
      CanExit =
        (!Match) || (EachIndex >= ASourceSize);
      
      EachIndex++;
    } // while
    
    if (!Match)
    {
      Result = (EachIndex - 1);
    }
  } // if
  
  // ---
  return Result;
} // func

bool /* func */ coretxtshared__onlyfloatcount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (coretxtshared__indexofnonfloatdeccount
      (ASourcePtr, ASourceSize) < 0);

  // ---
  return Result;
} // func

bool /* func */ coretxtshared__onlyfloat
  (/* in */ ansinullstring* /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (coretxtshared__indexofnonfloatdeccount
      (ASourcePtr, ASourceSize) < 0);

  // ---
  return Result;
} // func

// ------------------


 // ...

// ------------------

/* override */ const ansinullstring* /* func */ coresystem__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "coreuuids";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coreintegers__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreintegers__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coretxtshared