/** Module: "coretxtfloat2dec.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coretxtfloat2dec {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"

// ------------------

#include "coretxtfloat2dec.h"
 
// ------------------

bool /* func */ coretxtfloat2dec__tryhf2decnstr
  (/* in */  const halffloat_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coretxtfloat2dec_tryhfdecnstrcount
  (/* in */  const halffloat_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */  size_t                           /* param */ ADestSize)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coretxtfloat2dec__hf2decnstr
  (/* in */  const halffloat_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest)
{
  /* discard */ coretxtfloat2dec__tryhf2decnstr
    (ASource, ADest);
} // func

void /* func */ coretxtfloat2dec__hf2decnstrcount
  (/* in */  const halffloat_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */  size_t                           /* param */ ADestSize)
{
  /* discard */ coretxtfloat2dec__tryhf2decnstrcount
    (ASource, ADest, ADestSize);
} // func

// ------------------

bool /* func */ coretxtfloat2dec__trysf2decnstr
  (/* in */  const singlefloat_t              /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coretxtfloat2dec__trysf2decnstrcount
  (/* in */  const singlefloat_t               /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */  size_t                           /* param */ ADestSize)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coretxtfloat2dec__sf2decnstr
  (/* in */  const singlefloat_t              /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest)
{
  /* discard */ coretxtfloat2dec__trysf2decnstr
    (ASource, ADest);
} // func

void /* func */ coretxtfloat2dec__sf2decnstrcount
  (/* in */  const singlefloat_t              /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */  size_t                           /* param */ ADestSize)
{
  /* discard */ coretxtfloat2dec__trysf2decnstrcount
    (ASource, ADest, ADestSize);
} // func

// ------------------

bool /* func */ coretxtfloat2dec__trydf2decnstr
  (/* in */  const doublefloat_t              /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coretxtfloat2dec__trydf2decnstrcount
  (/* in */  const doublefloat_t               /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */  size_t                           /* param */ ADestSize)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coretxtfloat2dec__df2decnstr
  (/* in */  const doublefloat_t              /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest)
{
  /* discard */ coretxtfloat2dec__trydf2decnstr
    (ASource, ADest);
} // func

void /* func */ coretxtfloat2dec__df2decnstrcount
  (/* in */  const doublefloat_t              /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */  size_t                           /* param */ ADestSize)
{
  /* discard */ coretxtfloat2dec__trydf2decnstrcount
    (ASource, ADest, ADestSize);
} // func

// ------------------

bool /* func */ coretxtfloat2dec__tryqf2decnstr
  (/* in */  const quadfloat_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coretxtfloat2dec__tryqf2decnstrcount
  (/* in */  const quadfloat_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */  size_t                           /* param */ ADestSize)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coretxtfloat2dec__qf2decnstr
  (/* in */  const quadfloat_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest)
{
  /* discard */ coretxtfloat2dec__tryqf2decnstr
    (ASource, ADest);
} // func

void /* func */ coretxtfloat2dec__qf2decnstrcount
  (/* in */  const quadfloat_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */  size_t                           /* param */ ADestSize)
{
  /* discard */ coretxtfloat2dec__tryqf2decnstrcount
    (ASource, ADest, ADestSize);
} // func

// ------------------

bool /* func */ coretxtfloat2dec__tryof2decnstr
  (/* in */  const octafloat_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coretxtfloat2dec__tryof2decnstrcount
  (/* in */  const octafloat_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */  size_t                           /* param */ ADestSize)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coretxtfloat2dec__of2decnstr
  (/* in */  const octafloat_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest)
{
  /* discard */ coretxtfloat2dec__tryof2decnstr
    (ASource, ADest);
} // func

void /* func */ coretxtfloat2dec__of2decnstrcount
  (/* in */  const octafloat_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */  size_t                           /* param */ ADestSize)
{
  /* discard */ coretxtfloat2dec__tryof2decnstrcount
    (ASource, ADest, ADestSize);
} // func

// ------------------

bool /* func */ coretxtfloat2dec__trylf2decnstr
  (/* in */  const longfloat_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coretxtfloat2dec__trylf2decnstrcount
  (/* in */  const longfloat_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */  size_t                           /* param */ ADestSize)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coretxtfloat2dec__lf2decnstr
  (/* in */  const longfloat_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest)
{
  /* discard */ coretxtfloat2dec__trylf2decnstr
    (ASource, ADest);
} // func

void /* func */ coretxtfloat2dec__lf2decnstrcount
  (/* in */  const longfloat_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */  size_t                           /* param */ ADestSize)
{
  /* discard */ coretxtfloat2dec__trylf2decnstrcount
    (ASource, ADest, ADestSize);
} // func

// ------------------



// ...

// ------------------

/* override */ int /* func */ coretxtfloat2dec__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coretxtfloat2dec__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------


 // ...

// } // namespace coretxtfloat2dec