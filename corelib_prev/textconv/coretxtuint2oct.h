/** Module: "coretxtuint2oct.h"
 ** Descr.: "..."
 **/
 
// namespace coretxtuint2oct {
 
// ------------------
 
#ifndef CORETXTUINT2OCT__H
#define CORETXTUINT2OCT__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"

// ------------------

bool /* func */ coretxtuint2oct__tryuint8_decnstr
  (/* in */  const uint8_t  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

bool /* func */ coretxtuint2oct__tryuint8_decnstrcount
  (/* in */  const uint8_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

void /* func */ coretxtuint2oct__uint8_decnstr
  (/* in */  const uint8_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

void /* func */ coretxtuint2oct__uint8_decnstrcount
  (/* in */  const uint8_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

// ------------------

bool /* func */ coretxtuint2oct__tryuint16_decnstr
  (/* in */  const uint16_t  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

bool /* func */ coretxtuint2oct__tryuint16_decnstrcount
  (/* in */  const uint16_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

void /* func */ coretxtuint2oct__uint16_decnstr
  (/* in */  const uint16_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

void /* func */ coretxtuint2oct__uint16_decnstrcount
  (/* in */  const uint16_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

// ------------------

bool /* func */ coretxtuint2oct__tryuint32_decnstr
  (/* in */  const uint32_t  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

bool /* func */ coretxtuint2oct__tryuint32_decnstrcount
  (/* in */  const uint32_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

void /* func */ coretxtuint2oct__uint32_decnstr
  (/* in */  const uint32_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

void /* func */ coretxtuint2oct__uint32_decnstrcount
  (/* in */  const uint32_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

// ------------------

bool /* func */ coretxtuint2oct__tryuint64_decnstr
  (/* in */  const uint64_t  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

bool /* func */ coretxtuint2oct__tryuint64_decnstrcount
  (/* in */  const uint64_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

void /* func */ coretxtuint2oct__uint64_decnstr
  (/* in */  const uint64_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

void /* func */ coretxtuint2oct__uint64_decnstrcount
  (/* in */  const uint64_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

// ------------------

bool /* func */ coretxtuint2oct__tryuint128_decnstr
  (/* in */  const /* restricted */ uint128_t*  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

bool /* func */ coretxtuint2oct__tryuint128_decnstrcount
  (/* in */  const /* restricted */ uint128_t*   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

void /* func */ coretxtuint2oct__uint128_decnstr
  (/* in */  const /* restricted */ uint128_t*   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

void /* func */ coretxtuint2oct__uint128_decnstrcount
  (/* in */  const /* restricted */ uint128_t*  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

// ------------------




 // ...
 
// ------------------
 
#endif // CORETXTUINT2OCT_H

// } // namespace coretxtuint2oct