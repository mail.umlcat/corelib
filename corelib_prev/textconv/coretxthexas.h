/** Module: "coretxthexas.h"
 ** Descr.: "..."
 **/
 
// namespace coretxthexas {
 
// ------------------
 
#ifndef CORE<NAME>S__H
#define CORE<NAME>S__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
//#include "coreuuids.h"
//#include "coreoswarnings.h"

// ------------------

//#include "coreiterators.h"

// ------------------

bool /* func */ coretxthexas__tryhexchartodecbytebyptr
  (/* in */  /* restricted */ ansichar* /* param */ ASource,
  (/* out */ /* restricted */ byte*     /* param */ ADest);

bool /* func */ coretxthexas__tryhexstrtodecbytebyptr
  (/* in */  /* restricted */ ansinullstring* /* param */ ASource,
  (/* out */ /* restricted */ byte*           /* param */ ADest);

 // ...
 
// ------------------

 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coretxthexas__modulename
  ( noparams );

/* override */ int /* func */ coretxthexas__setup
  ( noparams );

/* override */ int /* func */ coretxthexas__setoff
  ( noparams );

// ------------------

#endif // CORE<NAME>S__H

// } // namespace coretxthexas