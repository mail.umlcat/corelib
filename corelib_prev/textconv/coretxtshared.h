/** Module: "coretxtshared.h"
 ** Descr.: "Predefined library for shared operations,"
 **         "for conversion between text and other types."
 **/
 
// namespace coretxtshared {
 
// ------------------
 
#ifndef CORETXTSHARED__H
#define CORETXTSHARED__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
//#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

//$define void noparams

// ------------------
 
//typedef
//  sometype         /* as */ sometype;
   
// ------------------

// --> global properties

const ansinullstring* /* func */ coretxtshared__decdigset
  ( noparams );

const ansinullstring* /* func */ coretxtshared__hexdecdigset
  ( noparams );

const ansinullstring* /* func */ coretxtshared__octdigset
  ( noparams );

const ansinullstring* /* func */ coretxtshared__bindigset
  ( noparams );

const ansinullstring* /* func */ coretxtshared__signeddecdigset
  ( noparams );

const ansinullstring* /* func */ coretxtshared__floatdigset
  ( noparams );

// ------------------

index_t /* func */ coretxtshared__indexofnondeccount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize);

bool /* func */ coretxtshared__onlydeccount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize);

bool /* func */ coretxtshared__onlydec
  (/* in */ ansinullstring* /* param */ ASource);

// ------------------

index_t /* func */ coretxtshared__indexofnonhexdeccount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize);

bool /* func */ coretxtshared__onlyhexdeccount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize);

bool /* func */ coretxtshared__onlyhexdec
  (/* in */ ansinullstring* /* param */ ASource);

// ------------------

index_t /* func */ coretxtshared__indexofnonoctcount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize);

bool /* func */ coretxtshared__onlyoctcount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize);

bool /* func */ coretxtshared__onlyoct
  (/* in */ ansinullstring* /* param */ ASource);

// ------------------

index_t /* func */ coretxtshared__indexofnonbincount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize);

bool /* func */ coretxtshared__onlybincount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize);

bool /* func */ coretxtshared__onlybin
  (/* in */ ansinullstring* /* param */ ASource);

// ------------------

index_t /* func */ coretxtshared__indexofnonsigneddeccount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize);

bool /* func */ coretxtshared__onlysigneddeccount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize);

bool /* func */ coretxtshared__onlysigneddec
  (/* in */ ansinullstring* /* param */ ASource);

// ------------------

index_t /* func */ coretxtshared__indexofnonfloatdeccount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize);

bool /* func */ coretxtshared__onlyfloatcount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize);

bool /* func */ coretxtshared__onlyfloat
  (/* in */ ansinullstring* /* param */ ASource);

// ------------------


 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coresystem__modulename
  ( noparams );

/* override */ int /* func */ coretxtshared__setup
  ( noparams );

/* override */ int /* func */ coretxtshared__setoff
  ( noparams );

// ------------------

#endif // CORETXTSHARED__H

// } // namespace coretxtshared