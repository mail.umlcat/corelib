/** Module: "coretxtdcm2dec.h"
 ** Descr.: "..."
 **/
 
// namespace coretxtdcm2dec {
 
// ------------------
 
#ifndef CORETXTDCM2DEC__H
#define CORETXTDCM2DEC__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coredecimals.h"

#include "coreansichars.h"
#include "coreansinullstrs.h"

// ------------------

bool /* func */ coretxtdcm2dec__trydec32tonstr
  (/* in */  const decimal32_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

bool /* func */ coretxtdcm2dec__trydec32tonstrcount
  (/* in */  const decimal32_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */  size_t                           /* param */ ADestSize);

void /* func */ coretxtdcm2dec__dec32tonstr
  (/* in */  const decimal32_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

void /* func */ coretxtdcm2dec__dec32tonstrcount
  (/* in */  const decimal32_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */  size_t                           /* param */ ADestSize);
 
// ------------------

bool /* func */ coretxtdcm2dec__trydec64tonstr
  (/* in */  const decimal64_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

bool /* func */ coretxtdcm2dec__trydec64tonstrcount
  (/* in */  const decimal64_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */  size_t                           /* param */ ADestSize);

void /* func */ coretxtdcm2dec__dec64tonstr
  (/* in */  const decimal64_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

void /* func */ coretxtdcm2dec__dec64tonstrcount
  (/* in */  const decimal64_t                /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */  size_t                           /* param */ ADestSize);
 
// ------------------

bool /* func */ coretxtdcm2dec__trydec128tonstr
  (/* in */  const decimal128_t               /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

bool /* func */ coretxtdcm2dec__trydec128tonstrcount
  (/* in */  const decimal128_t               /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */  size_t                           /* param */ ADestSize);

void /* func */ coretxtdcm2dec__dec128tonstr
  (/* in */  const decimal128_t               /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

void /* func */ coretxtdcm2dec__dec128tonstrcount
  (/* in */  const decimal128_t               /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */  size_t                           /* param */ ADestSize);



// ------------------



 // ...
 
// ------------------

/* override */ int /* func */ coretxtdcm2dec__setup
  ( noparams );

/* override */ int /* func */ coretxtdcm2dec__setoff
  ( noparams );

// ------------------

#endif // CORETXTDCM2DEC__H

// } // namespace coretxtdcm2dec