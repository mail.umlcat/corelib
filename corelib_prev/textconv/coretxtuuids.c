/** Module: "coretxtuuids.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coretxtuuids {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
#include <ctype.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
//#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"
#include "coretxtshared.h"

// ------------------

#include "coretxtuuids.h"
 
// ------------------

// --> global properties

const ansinullstring* /* func */ coretxtuuids__uuidcharset
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "0123456789abcdefABCDEF-";
  
  return (const ansinullstring*) setbuffer;
} // func

// ------------------

bool /* func */ coretxtuuids___tryuuidtostr
  (/* in */  /* restricted */ const uuid_t*   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    ((ASource != NULL) && (ADest != NULL));
  if (Result)
  {
  	
  } //if
  
  // ---
  return Result;
} // func

bool /* func */ coretxtuuids___tryuuidtostrcount
  (/* in */  /* restricted */ const uuid_t*   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */                   size_t          /* param */ ASize);
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    ((ASource != NULL) && (ADest != NULL));
  if (Result)
  {
  	
  } //if
     
  // ---
  return Result;
} // func

void /* func */ coretxtuuids___uuidtostr
  (/* in */  /* restricted */ const uuid_t*   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  /* discard */
    coretxtuuids___tryuuidtostr
      (ASource, ADest);
} // func

void /* func */ coretxtuuids___uuidtostrcount
  (/* in */  /* restricted */ const uuid_t*   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */                   size_t          /* param */ ASize);
{
  /* discard */
    coretxtuuids___tryuuidtostr
      (ASource, ADest);
} // func

// ------------------

index_t /* func */ coretxtuuids__indexofnonuuidcount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize)
{
  index_t /* var */ Result = -1;
  // ---

  bool /* var */ IsValid = false;
  
  IsValid =
    (ASourcePtr != NULL) && (ASourceSize > 0);
  if (IsValid) 
  {
    ansichar* /* var */ C = NULL;
    bool      /* var */ Match     = false;

    bool      /* var */ CanExit   = false;
    index_t   /* var */ EachIndex = 0;

    const ansinullstring*  /* var */ UuidChars =
      coretxtshared__decdigset;
    index_t    /* var */ UuidCharsSize =
      strlen(UuidChars);

    C = ASourcePtr;
    
    while (CanExit)
    {
      Match =
        (coreansinullstrs__firstptrofchar
          (UuidChars, C, UuidCharsSize) != NULL);
           
      CanExit =
        (!Match) || (EachIndex >= ASourceSize);
      
      EachIndex++;
    } // while
    
    if (!Match)
    {
      Result = (EachIndex - 1);
    }
  } // if
  
  // ---
  return Result;
} // func

bool /* func */ coretxtuuids__tryremovehyphenscopy
  (/* in */  /* restricted */ ansinullstring* /* param */ ASource,
  (/* out */ /* restricted */ ansinullstring* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  size_t    /* var */ ASourceSize = 0;
  ansichar* /* var */ S = NULL;
  ansichar* /* var */ D = NULL;

  Result =
    ((ASource != NULL) && (ADest != NULL));
  if (Result)
  {
    ASourceSize = strlen(ASource);
    Result =
      (ASourceSize >= uuidwithhyphensize);
    if (Result)
    {
  	S = (ansichar*) ASource;
      D = (ansichar*) ADest;
      
      for (int /* var */ i = 0; i < uuidwithhyphensize; i++)
      {
        if (*S != '-')
        {
          *D = *S;
        }
        
        S++; D++;
      } // for
    } //if
  } //if

  // ---
  return Result;
} // func

bool /* func */ coretxtuuids__tryaddhyphenscopy
  (/* in  */ /* restricted */ ansinullstring* /* param */ ASource,
  (/* out */ /* restricted */ ansinullstring* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  size_t    /* var */ ASourceSize = 0;
  ansichar* /* var */ S = NULL;
  ansichar* /* var */ D = NULL;
  ansichar* /* var */ T = NULL;

  ansichar* /* var */ Template[uuidwithhyphensize + 1];

  Result =
    ((ASource != NULL) && (ADest != NULL));
  if (Result)
  {
    ASourceSize = strlen(ASource);
    Result =
      (ASourceSize >= uuidnohyphensize);
    if (Result)
    {
      // prepare template
  	strncpy
        (Template, niluuidstr, sizeof(Template));
  
  	T = (ansichar*) Template;
  	S = (ansichar*) ASource;
      D = (ansichar*) ADest;
      
      for (int /* var */ i = 0; i < uuidwithhyphensize; i++)
      {
        if (*T != '-')
        {
          *D = *S;
          S++;
        } else
        {
          *D = '-';
        }
        
        T++;
        D++;
      } // for
    } //if
  } //if

  // ---
  return Result;
} // func

bool /* func */ coretxtuuids__trydrophyphensrevcopy
  (/* in */  /* restricted */ ansinullstring* /* param */ ASource,
  (/* out */ /* restricted */ ansinullstring* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---
  
  size_t    /* var */ ASourceSize = 0;
  ansichar* /* var */ S = NULL;
  ansichar* /* var */ D = NULL;
  ansichar* /* var */ T = NULL;

  ansichar* /* var */ Template[uuidwithhyphensize + 1];

  Result =
    ((ASource != NULL) && (ADest != NULL));
  if (Result)
  {
    ASourceSize = strlen(ASource);
    Result =
      (ASourceSize >= uuidnohyphensize);
    if (Result)
    {
      // prepare template
  	strncpy
        (Template, niluuidstr, sizeof(Template));

      // set to the last char before null marker
  	S = (ansichar*)
        strchr(ASource, ansinullchar);
      S--;
  	T = (ansichar*) Template;
      D = (ansichar*) ADest;
      
      for (int /* var */ i = 0; i < uuidwithhyphensize; i++)
      {
        if (*T != '-')
        {
          *D = strtolower(*S);
          S--;
        } else
        {
          *D = '-';
        }
        
        T++;
        D++;
      } // for
    } //if
  } //if
    
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coretxtuuids__trystrtouuid
  (/* out */ /* restricted */ ansinullstring* /* param */ ASource,
   /* in */  /* restricted */ const uuid_t*   /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  size_t    /* var */ ASourceSize = 0;
  index_t   /* var */ ThisIndex = 0;

  ansichar* /* var */ S = NULL;
  ansichar* /* var */ D = NULL;

  // make a str copy without hyphens
  ansichar /* var */ tempbuffer[uuidnohyphensize + 1];
  // store 8 digits for a mem32
  ansichar /* var */ groupbuffer[8 + 1];

  // store 8 digits for a mem32
  mem32_t /* var */ groupbin;

  Result =
    ((ASource != NULL) && (ADest != NULL));
  if (Result)
  {
    ASourceSize = strlen(ASource);
    Result =
      (ASourceSize == uuidwithhyphensize);
    if (Result)
    {
      // lookout for non valid chars
      ThisIndex =
        coretxtuuids__indexofnonuuidcount
          (ASource, ASourceSize);
      Result = (ThisIndex < 0);
      if (Result)
      {
        Result =
      	coretxtuuids__trydrophyphensrevcopy
            (ASource, tempbuffer);
        if (Result)
        {
          // reversed str without hyphens
      	S = (ansichar*) tempbuffer;
          
          for (int /* var */ i = 0; i < 4; i++)
          {
            // reset group pointer
            D = (ansichar*) groupbuffer;
          	
            for (int /* var */ j = 0; j < 8; j++)
            {
              // copy char to group
              *D = *S;
              D++; S++;
            } // for
            
            // add null marker
            *D = ansinullchar;
            
            //
            
          } // for
        }
      }
    } //if
  } //if
     
  // ---
  return Result;
} // func

bool /* func */ coretxtuuids__trystrtouuidcount
  (/* out */ /* restricted */ ansinullstring* /* param */ ASource,
   /* in */                   size_t          /* param */ ASourceSize,
   /* in */  /* restricted */ const uuid_t*   /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    ((ASource != NULL) && (ADest != NULL) && (ASourceSize == uuidwithhyphensize);
  if (Result)
  {
  	
  } //if
     
  // ---
  return Result;
} // func

// ------------------

/* override */ const ansinullstring* /* func */ coresystem__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "coretxtuuids";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coreintegers__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreintegers__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coretxtuuids