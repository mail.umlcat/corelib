/** Module: "coretxtuint2dec.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coretxtuint2dec {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"

// ------------------

#include "coretxtuint2dec.h"
 
// ------------------

bool /* func */ coretxtuint2dec__tryuint8_decnstr
  (/* in */  const uint8_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coretxtuint2dec__tryuint8_decnstrcount
  (/* in */  const uint8_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coretxtuint2dec__uint8_decnstr
  (/* in */  const uint8_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  coresystem__nothing();
} // func

void /* func */ coretxtuint2dec__uint8_decnstrcount
  (/* in */  const uint8_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  coresystem__nothing();
} // func

// ------------------

bool /* func */ coretxtuint2dec__tryuint16_decnstr
  (/* in */  const uint16_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coretxtuint2dec__tryuint16_decnstrcount
  (/* in */  const uint16_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coretxtuint2dec__uint16_decnstr
  (/* in */  const uint16_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  coresystem__nothing();
} // func

void /* func */ coretxtuint2dec__uint16_decnstrcount
  (/* in */  const /* restricted */ uint16_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  coresystem__nothing();
} // func

// ------------------

bool /* func */ coretxtuint2dec__tryuint32_decnstr
  (/* in */  const uint32_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coretxtuint2dec__tryuint32_decnstrcount
  (/* in */  const uint32_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coretxtuint2dec__uint32_decnstr
  (/* in */  const uint32_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  coresystem__nothing();
} // func

void /* func */ coretxtuint2dec__uint32_decnstrcount
  (/* in */  const uint32_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  coresystem__nothing();
} // func

// ------------------

bool /* func */ coretxtuint2dec__tryuint64_decnstr
  (/* in */  const uint64_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coretxtuint2dec__tryuint64_decnstrcount
  (/* in */  const uint64_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coretxtuint2dec__uint64_decnstr
  (/* in */  const uint64_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  coresystem__nothing();
} // func

void /* func */ coretxtuint2dec__uint64_decnstrcount
  (/* in */  const uint64_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  coresystem__nothing();
} // func

// ------------------

bool /* func */ coretxtuint2dec__tryuint128_decnstr
  (/* in */  const /* restricted */ uint128_t*  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coretxtuint2dec__tryuint128_decnstrcount
  (/* in */  const /* restricted */ uint128_t*  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coretxtuint2dec__uint128_decnstr
  (/* in */  const /* restricted */ uint128_t*  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest)
{
  /* discard */
    coretxtuint2dec__tryuint128_decnstr
      (ASource, ADest);
} // func

void /* func */ coretxtuint2dec__uint128_decnstrcount
  (/* in */  const /* restricted */ uint128_t*  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize)
{
  /* discard */
    coretxtuint2dec__tryuint128_decnstrcount
      (ASource, ADest, ASize);
} // func

// ------------------

bool /* func */ coretxtuint2dec__trydecnstr_uint128
  (/* out */ /* restricted */ ansinullstring*  /* param */ ASource,
   /* in */  const /* restricted */ uint128_t* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

 // ...

// } // namespace coretxtuint2dec