/** Module: "coretxtsint2oct.h"
 ** Descr.: "..."
 **/
 
// namespace coretxtsint2oct {
 
// ------------------
 
#ifndef CORETXTSINT2OCT__H
#define CORETXTSINT2OCT__H
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

#include "coreansichars.h"
#include "coreansinullstrs.h"

// ------------------

bool /* func */ coretxtsint2oct__trysint8_decnstr
  (/* in */  const sint8_t  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

bool /* func */ coretxtsint2oct__trysint8_decnstrcount
  (/* in */  const sint8_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

void /* func */ coretxtsint2oct__sint8_decnstr
  (/* in */  const sint8_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

void /* func */ coretxtsint2oct__sint8_decnstrcount
  (/* in */  const sint8_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

// ------------------

bool /* func */ coretxtsint2oct__trysint16_decnstr
  (/* in */  const sint16_t  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

bool /* func */ coretxtsint2oct__trysint16_decnstrcount
  (/* in */  const sint16_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

void /* func */ coretxtsint2oct__sint16_decnstr
  (/* in */  const sint16_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

void /* func */ coretxtsint2oct__sint16_decnstrcount
  (/* in */  const sint16_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

// ------------------

bool /* func */ coretxtsint2oct__trysint32_decnstr
  (/* in */  const sint32_t  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

bool /* func */ coretxtsint2oct__trysint32_decnstrcount
  (/* in */  const sint32_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

void /* func */ coretxtsint2oct__sint32_decnstr
  (/* in */  const sint32_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

void /* func */ coretxtsint2oct__sint32_decnstrcount
  (/* in */  const sint32_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

// ------------------

bool /* func */ coretxtsint2oct__trysint64_decnstr
  (/* in */  const sint64_t  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

bool /* func */ coretxtsint2oct__trysint64_decnstrcount
  (/* in */  const sint64_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

void /* func */ coretxtsint2oct__sint64_decnstr
  (/* in */  const sint64_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

void /* func */ coretxtsint2oct__sint64_decnstrcount
  (/* in */  const sint64_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

// ------------------

bool /* func */ coretxtsint2oct__trysint128_decnstr
  (/* in */  const /* restricted */ sint128_t*  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

bool /* func */ coretxtsint2oct__trysint128_decnstrcount
  (/* in */  const /* restricted */ sint128_t*   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

void /* func */ coretxtsint2oct__sint128_decnstr
  (/* in */  const /* restricted */ sint128_t*   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

void /* func */ coretxtsint2oct__sint128_decnstrcount
  (/* in */  const /* restricted */ sint128_t*  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

// ------------------




 // ...
 
// ------------------
 
#endif // CORETXTSINT2OCT_H

// } // namespace coretxtsint2oct