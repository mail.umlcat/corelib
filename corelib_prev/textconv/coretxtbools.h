/** Module: "coretxtbools.h"
 ** Descr.: "..."
 **/
 
// namespace coretxtbools {
 
// ------------------
 
#ifndef CORETXTBOOLS__H
#define CORETXTBOOLS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"
#include "corebooleans.h"

// ------------------

bool /* func */ coretxtbools_trybool2nstr
  (/* in */  const bool  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

bool /* func */ coretxtbools_trybool2nstrcount
  (/* in */  const bool   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

void /* func */ coretxtbools_bool2nstr
  (/* in */  const bool   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

void /* func */ coretxtbools_bool2nstrcount
  (/* in */  const bool   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);



 // ...
 
// ------------------
 
#endif // CORETXTBOOLS__H

// } // namespace coretxtbools