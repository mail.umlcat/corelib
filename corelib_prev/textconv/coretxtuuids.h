/** Module: "coretxtuuids.h"
 ** Descr.: "..."
 **/
 
// namespace coretxtuuids {
 
// ------------------
 
#ifndef CORETXTUUIDS__H
#define CORETXTUUIDS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
#include <ctype.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
//#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"
#include "coretxtshared.h"

// ------------------

#define niluuidstr "00000000-0000-0000-0000-000000000000"

#define uuidwithhyphensize 36
#define uuidnohyphensize   32

// ------------------

// --> global properties

const ansinullstring* /* func */ coretxtuuids__uuidcharset
  ( noparams );

// ------------------

bool /* func */ coretxtuuids__tryuuidtostr
  (/* in */  /* restricted */ const uuid_t*   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

bool /* func */ coretxtuuids__tryuuidtostrcount
  (/* in */  /* restricted */ const uuid_t*   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

void /* func */ coretxtuuids__uuidtostr
  (/* in */  /* restricted */ const uuid_t*   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

void /* func */ coretxtuuids__uuidtonstrcount
  (/* in */  /* restricted */ const uuid_t*   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */                   size_t          /* param */ ADestSize);

// ------------------

index_t /* func */ coretxtuuids__indexofnonuuidcount
  (/* in */ ansinullstring* /* param */ ASourcePtr,
   /* in */ size_t          /* param */ ASourceSize);

bool /* func */ coretxtuuids__tryremovehyphenscopy
  (/* in */  /* restricted */ ansinullstring* /* param */ ASource,
  (/* out */ /* restricted */ ansinullstring* /* param */ ADest);

bool /* func */ coretxtuuids__tryaddhyphenscopy
  (/* in  */ /* restricted */ ansinullstring* /* param */ ASource,
  (/* out */ /* restricted */ ansinullstring* /* param */ ADest);

bool /* func */ coretxtuuids__trydrophyphensrevcopy
  (/* in */  /* restricted */ ansinullstring* /* param */ ASource,
  (/* out */ /* restricted */ ansinullstring* /* param */ ADest);

// ------------------

bool /* func */ coretxtuuids__trystrtouuid
  (/* out */ /* restricted */ ansinullstring* /* param */ ASource,
   /* in */  /* restricted */ const uuid_t*   /* param */ ADest);

bool /* func */ coretxtuuids__trystrtouuidcount
  (/* out */ /* restricted */ ansinullstring* /* param */ ASource,
   /* in */                   size_t          /* param */ ASourceSize,
   /* in */  /* restricted */ const uuid_t*   /* param */ ADest);

// ------------------

/* override */ const ansinullstring* /* func */ coresystem__modulename
  ( noparams );

/* override */ int /* func */ coreintegers__setup
  ( noparams );

/* override */ int /* func */ coreintegers__setoff
  ( noparams );

// ------------------

#endif // CORETXTUUIDS__H

// } // namespace coretxtuuids