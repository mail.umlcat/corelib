/** Module: "coretxtsint2dec.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coretxtsint2dec {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"

// ------------------

#include "coretxtsint2dec.h"
 
// ------------------

bool /* func */ coretxtsint2dec__trysint8_decnstr
  (/* in */  const sint8_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coretxtsint2dec__trysint8_decnstrcount
  (/* in */  const sint8_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coretxtsint2dec__sint8_decnstr
  (/* in */  const sint8_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  coresystem__nothing();
} // func

void /* func */ coretxtsint2dec__sint8_decnstrcount
  (/* in */  const sint8_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  coresystem__nothing();
} // func

// ------------------

bool /* func */ coretxtsint2dec__trysint16_decnstr
  (/* in */  const sint16_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coretxtsint2dec__trysint16_decnstrcount
  (/* in */  const sint16_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coretxtsint2dec__sint16_decnstr
  (/* in */  const sint16_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  coresystem__nothing();
} // func

void /* func */ coretxtsint2dec__sint16_decnstrcount
  (/* in */  const /* restricted */ sint16_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  coresystem__nothing();
} // func

// ------------------

bool /* func */ coretxtsint2dec__trysint32_decnstr
  (/* in */  const sint32_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coretxtsint2dec__trysint32_decnstrcount
  (/* in */  const sint32_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coretxtsint2dec__sint32_decnstr
  (/* in */  const sint32_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  coresystem__nothing();
} // func

void /* func */ coretxtsint2dec__sint32_decnstrcount
  (/* in */  const sint32_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  coresystem__nothing();
} // func

// ------------------

bool /* func */ coretxtsint2dec__trysint64_decnstr
  (/* in */  const sint64_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coretxtsint2dec__trysint64_decnstrcount
  (/* in */  const sint64_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coretxtsint2dec__sint64_decnstr
  (/* in */  const sint64_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  coresystem__nothing();
} // func

void /* func */ coretxtsint2dec__sint64_decnstrcount
  (/* in */  const sint64_t   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  coresystem__nothing();
} // func

// ------------------

bool /* func */ coretxtsint2dec__trysint128_decnstr
  (/* in */  const /* restricted */ sint128_t*  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coretxtsint2dec__trysint128_decnstrcount
  (/* in */  const /* restricted */ sint128_t*  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coretxtsint2dec__sint128_decnstr
  (/* in */  const /* restricted */ sint128_t*  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  coresystem__nothing();
} // func

void /* func */ coretxtsint2dec__sint128_decnstrcount
  (/* in */  const /* restricted */ sint128_t*  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  coresystem__nothing();
} // func

// ------------------


 // ...

// } // namespace coretxtsint2dec