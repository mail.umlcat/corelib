/** Module: "coretxtbools.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coretxtbools {
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

#include "coreansichars.h"
#include "coreansinullstrs.h"
#include "corebooleans.h"

// ------------------

#include "coretxtbools.h"
 
// ------------------

bool /* func */ coretxtbools__trybool2nstr
  (/* in */  const bool   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coretxtbools__trybool2nstrcount
  (/* in */  const bool   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coretxtbools__bool2nstr
  (/* in */  const bool   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  coresystem__nothing();
} // func

void /* func */ coretxtbools__bool2nstrcount
  (/* in */  const bool   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  coresystem__nothing();
} // func

// ------------------


 // ...

// } // namespace coretxtbools