/** Module: "coretxthexas.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coretxthexas {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
//#include "coreuuids.h"
//#include "coreoswarnings.h"

// ------------------

//#include "coreiterators.h"

// ------------------
 
#include "coretxthexas.h"
 
// ------------------

bool /* func */ coretxthexas__tryhexchartodecbytebyptr
  (/* in */  /* restricted */ ansichar* /* param */ ASource,
  (/* out */ /* restricted */ byte*     /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    ((ASource != NULL) && (ADest != NULL);
  if (Result)
  {
    *ADest = 0;

    *ASource = tolower(*ASource);
    if ((*ASource >= '0') && (*ASource <= '9'))
    {
      *ADest +=
    	(((unsigned char) *ASource) - 48);
    } else if ((*ASource >= 'a') && (*ASource <= 'f'))
    {
      *ADest +=
    	(((unsigned char) *ASource) - 55);
    } //if
  } //if

  // ---
  return Result;
} // func

bool /* func */ coretxthexas__tryhexstrtodecbytebyptr
  (/* in */  /* restricted */ ansinullstring* /* param */ ASource,
  (/* out */ /* restricted */ byte*           /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  bool /* var */ CanContinue = false;

  size_t /* var */ Base  = 1;
  int    /* var */ ASize = 0;
  int    /* var */ i = 0;

  Result =
    ((ASource != NULL) && (ADest != NULL);
  if (Result)
  {
    *ADest = 0;
    ASize = strlen(ASource);
    
    Result = (ASize == 2);
    if (Result)
    {
      i = (ASize - 1);
      CanContinue =
        (i >= 0) && Result;
      while (CanContinue)
      {
        *ASource = tolower(*ASource);
        if ((*ASource >= '0') && (*ASource <= '9'))
        {
          *ADest +=
        	(((unsigned char) *ASource) - 48) * Base;
        } else if ((*ASource >= 'a') && (*ASource <= 'f'))
        {
          *ADest +=
        	(((unsigned char) *ASource) - 55) * Base;
        } else
        {
          Result = false;
        } //if
    
        base = base * 16;
        i--;
        CanContinue =
        (i >= 0) && Result;
      } // while
    } //if
  } //if

  // ---
  return Result;
} // func

 // ...
 
// ------------------

 // ...
 
// ------------------

comparison /* func */ coretxthexas__compare
  (/* in */ ansinullstring* /* param */ A, B)
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func
   
 // ------------------
 
void /* func */ coretxthexas__clear
  (/* inout */ ansinullstring* /* param */ ADest)
{
  // ...
} // func
 
 // ...

// ------------------

/* override */ const ansinullstring* /* func */ coretxthexas__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "coretxthexas";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coretxthexas__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coretxthexas__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coretxthexas