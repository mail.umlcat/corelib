/** Module: "coretxtdcm2dec.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coretxtdcm2dec {
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

#include "coredecimals.h"

#include "coreansichars.h"
#include "coreansinullstrs.h"

// ------------------

#include "coretxtdcm2dec.h"
 
// ------------------

bool /* func */ coretxtdcm2dec__trydec2nstr
  (/* in */  const dec<*>   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coretxtdcm2dec__trydec2nstrcount
  (/* in */  const dec<*>   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coretxtdcm2dec__dec2nstr
  (/* in */  const dec<*>   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);
{
  /* discard */ coretxtdcm__trydec2nstr
    (ASource, ADest);
} // func

void /* func */ coretxtdcm2dec__dec2nstrcount
  (/* in */  const dec<*>   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ASize);
{
  /* discard */ coretxtdcm__trydec2nstrcount
    (ASource, ADest, ADestSize);
} // func

// ------------------

/* override */ int /* func */ coretxtdcm2dec__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coretxtdcm2dec__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------


 // ...

// } // namespace coretxtdcm2dec