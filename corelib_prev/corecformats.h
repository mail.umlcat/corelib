/** Module: "corecformats.h"
 ** Descr.: "Implements 'C' style string formatting".
 **/

// namespace corecformats {
 
// ------------------
 
#ifndef CORECFORMATS__H
#define CORECFORMATS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include <coreansichars.h>
#include <coreansinullstrs.h>
#include <coredlinklists.h>
#include <coreptrparams.h>
#include <coreuint32s.h>
#include <corebooleans.h>
#include <corefloats.h>
#include <coredatetimes.h>

// ------------------

typedef
  pointer /* as */ formatlist;
   
// ------------------

enum formatmodes
{
  formatmodes__None,
  formatmodes__PlainText,
  formatmodes__FormatSpecifier,
} ;

enum formatspecifiers
{
  /* none */  formatspecifiers__None,

  /* %%  */   formatspecifiers__PercentSymbol,
  
  /* %s  */   formatspecifiers__NullString,
  /* %c  */   formatspecifiers__Char,
  /* %d  */   formatspecifiers__DecimalInt,
  /* %f  */   formatspecifiers__Float,
  /* %p  */   formatspecifiers__Pointer,
  /* %h  */   formatspecifiers__HexadecimalInt,
  /* %o   */  formatspecifiers__OctalInt,
  /* %u   */  formatspecifiers__UnsignedInt,

  /* %g   */
  /* %G   */  
  /* %e   */
  /* %E   */  formatspecifiers__ScientificFloat,

  /* %hu  */  formatspecifiers__ShortUnsignedInt,
  /* %hi  */  formatspecifiers__ShortSignedInt,

  /* %l   */
  /* %lu  */
  /* %ld  */  
  /* %li  */  formatspecifiers__SignedLongInteger,

  /* %lld  */  
  /* %lli  */ formatspecifiers__SignedLongLongInteger,

  /* %lf   */  
  /* %Lf   */ formatspecifiers__SignedLongFloat,

  // non standard
  /* %:   */  formatspecifiers__BinaryInteger,
  
  // non standard
  /* %?   */  formatspecifiers__Boolean,

  // non standard
  /* %!   */  formatspecifiers__Time,
  
  // non standard
  /* %#   */  formatspecifiers__Date,

  // non standard
  /* %#!  */  formatspecifiers__DateTime,

} ;

struct formatitem
{
  ansinullstring*       /* var */ Text;

  pointer*              /* var */ Param;

  enum formatmodes      /* var */ Mode;
  enum formatspecifiers /* var */ Specifier;

  // ...
} ;

// ------------------

enum formatspecifiers /* func */ corecformats__char2specifier
  (/* in */ const ansichar /* param */ AChar);

ansichar /* func */ corecformats__specifier2char
  (/* in */ const enum formatspecifiers /* param */ ASpecifier);

// ------------------

bool /* func */ corecformats__trymerge
  (/* in */  const ansinullstring* /* param */ ASourceMsg,
   /* in */  const coreptrparams   /* param */ ASourceParams,
   /* out */ formatlist*           /* param */ ADestFormatList);

bool /* func */ corecformats__trymergesize
  (/* in */  const ansinullstring* /* param */ ASourceMsg,
   /* in */  const size_t          /* param */ ASourceSize,
   /* in */  const coreptrparams   /* param */ ASourceParams,
   /* out */ formatlist*           /* param */ ADestFormatList);

void /* func */ corecformats__merge
  (/* in */  const ansinullstring* /* param */ ASourceMsg,
   /* in */  const ptrparams*      /* param */ ASourceParams,
   /* out */ formatlist*           /* param */ ADestFormatList);

void /* func */ corecformats__mergesize
  (/* in */  const ansinullstring* /* param */ ASourceMsg,
   /* in */  const size_t          /* param */ ASourceSize,
   /* in */  const ptrparams*      /* param */ ASourceParams,
   /* out */ formatlist*           /* param */ ADestFormatList);

// ------------------

bool /* func */ corecformats__trysplit
  (/* in */  const formatlist*     /* param */ ASourceFormatList,
   /* out */ const ansinullstring* /* param */ ADestMsg,
   /* out */ const ptrparams*      /* param */ ADestParams);

bool /* func */ corecformats__trysplitsize
  (/* in */  const formatlist*     /* param */ ASourceFormatList,
   /* out */ const ansinullstring* /* param */ ADestMsg,
   /* in */  const size_t          /* param */ ADestSize,
   /* out */ const ptrparams*      /* param */ ADestParams);

void /* func */ corecformats__split
  (/* in */  const formatlist*     /* param */ ASourceFormatList,
   /* out */ const ansinullstring* /* param */ ADestMsg);

void /* func */ corecformats__splitsize
  (/* in */  const formatlist*     /* param */ ASourceFormatList,
   /* out */ const ansinullstring* /* param */ ADestMsg,
   /* in */  const size_t          /* param */ ADestSize);

// ------------------

struct formatitem* /* func */ corecformats__newcharitem
  (/* in */  const ansichar /* param */ ASource);

struct formatitem* /* func */ corecformats__newstritem
  (/* in */  const ansinullstring* /* param */ ASource);

struct formatitem* /* func */ corecformats__newstritemsize
  (/* in */  const ansinullstring* /* param */ ASourceBuffer,
   /* in */  const size_t          /* param */ ASourceSize);
   
// ------------------

struct formatitem* /* func */ corecformats__newparamitem
  (/* in */  const enum formatspecifiers /* param */ AFormatSpecifier,
   /* in */  const pointer*         /* param */ AItem);

// ------------------

/* override */ int /* func */ corecformats__setup
  ( noparams );

/* override */ int /* func */ corecformats__setoff
  ( noparams );

// ------------------

#endif // CORECFORMATS__H

// } // namespace corecformats