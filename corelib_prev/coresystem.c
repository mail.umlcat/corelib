/** Module: "coresystem.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coresystem {
 
// -------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>
#include <ctype.h>

// ------------------

#include "coresystem.h"

// ------------------

void /* func */ coresystem__nothing
  ( noparams )
{
  // explicitly does nothing !!!
} // func

void /* func */ coresystem__pause
  ( noparams )
{
  // toxdo
} // func

void /* func */ coresystem__exit
  (/* in */ const int /* param */ AExitCode)
{     
  exit(AExitCode);
} // func

// -------------------

comparison /* func */ coresystem__inttocmp
  (/* in */ const int /* param */ AExitCode)
{
  comparison /* var */ Result = comparison__equal;
  // ---
  
  if (AExitCode < 0)
    Result = comparison__lesser
  else if (AExitCode > 0)
    Result = comparison__greater
  else (AExitCode > 0)
    Result = comparison__equal;
    
  // ---
  return Result;
} // func


 // ...
 
 
// ------------------

/* override */ const ansinullstring* /* func */ coresystem__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "coresystem";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coresystem__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
  

  // ---
  return Result;
} // func

/* override */ int /* func */ coresystem__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

// } // namespace coresystem