/** Module: "coresystem.h"
 ** Descr.: "predefined library"
 **/
 
// namespace coresystem {
 
// ------------------
 
#ifndef CORESYSTEM__H
#define CORESYSTEM__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>
#include <ctype.h>

// ------------------

// "void SomeFunc ( noparams );" =>
// "void SomeFunc ( void );"
#define noparams void

// ------------------

// "noresult SomeFunc (void* x);" =>
// "void     SomeFunc (void* x);"
#define noresult void

// ------------------

// "pointer* SomeFunc ( void );" =>
// "void*    SomeFunc ( void );"
typedef
  void*     /* as */ pointer;

// ------------------

// indicate a function
// that explicit returns a value
#define discard /* discard */

// ------------------

// complement "const" parameters
// void somefunc ( noconst char* item);
#define noconst /* noconst */

// ------------------

// empty value for functors
#define undefined NULL

// ------------------

// unique identifier
typedef
  uint32_t    /* as */ uuid[4];

typedef
  uuid        /* as */ uuid_t;
typedef
  uuid*       /* as */ uuid_p;

typedef
  uuid_t      /* as */ system__uuid_t;
typedef
  uuid*       /* as */ system__uuid_p;

#define nouuid 0

// ------------------

// identifier for types, not pointer
// "typeid" is already taken
typedef
  uuid         /* as */ typecode;

typedef
  typecode     /* as */ typecode_t;
typedef
  typecode*    /* as */ typecode_p;

// ------------------

#define unknowntype 0

// identifier for modules
typedef
  uuid         /* as */ modulecode;

typedef
  modulecode   /* as */ module_t;
typedef
  modulecode*  /* as */ module_p;

#define unknownmodule 0

// ------------------

// used for floats
// and other CPU architectures
typedef
  uint8_t        /* as */ uint24_t[3];
typedef
  uint24_t*      /* as */ uint24_p;

// used for floats
// and other CPU architectures
typedef
  uint8_t        /* as */ uint48_t[6];
typedef
  uint48_t*      /* as */ uint48_p;

// used for floats
// and other CPU architectures
typedef
  uint8_t        /* as */ uint80_t[10];
typedef
  uint80_t*      /* as */ uint80_p;

typedef
  uint8_t        /* as */ uint256_t[32];
typedef
  uint256_t*     /* as */ uint256_p;

// ------------------

// used for floats
// and other CPU architectures
typedef
  sint8_t        /* as */ sint24_t[3];
typedef
  sint24_t*      /* as */ sint24_p;

// used for floats
// and other CPU architectures
typedef
  sint8_t        /* as */ sint48_t[6];
typedef
  sint48_t*      /* as */ sint48_p;

// used for floats
// and other CPU architectures
typedef
  sint8_t        /* as */ sint80_t[10];
typedef
  sint80_t*      /* as */ sint80_p;

typedef
  sint8_t        /* as */ sint256_t[32];
typedef
  sint256_t*     /* as */ sint256_p;

// ------------------

// clang?

typedef
  uint8_t*       /* as */ uint8_p;
typedef
  uint16_t*      /* as */ uint16_p;
typedef
  uint32_t*      /* as */ uint32_p;
typedef
  uint64_t*      /* as */ uint64_p;

// ------------------

typedef
  sint8_t*       /* as */ sint8_p;
typedef
  sint16_t*      /* as */ sint16_p;
typedef
  sint32_t*      /* as */ sint32_p;
typedef
  sint64_t*      /* as */ sint64_p;

// clang?

// ------------------

typedef
  unsigned char  /* as */ ansichar;
typedef
  ansichar       /* as */ ansinullstring;

typedef
  ansinullstring /* as */ ansicharset;

struct ansicharrange
{
  ansichar /* var */ lo, hi;
} ;

// ------------------

typedef
  uint16_t /* as */ widechar;
  
typedef
  widechar /* as */ widenullstring;

// ------------------

#define ANSINULLCHAR '\0'
#define WIDENULLCHAR L'\0'

#define coresystem__ANSINULLCHAR '\0'
#define coresystem__WIDENULLCHAR L'\0'

// ------------------

enum stringcasemodes
{
  // mode not assigned
  stringcasemodes__none,
  
  // "Hello","HELLO","hello",
  // are considered the same text
  stringcasemodes__insensitive,
  
  // "Hello","HELLO","hello",
  // can be inserted as different text
  stringcasemodes__sensitive,

} ;

// ------------------

enum comparison
{
  comparison__lesser  = -1,
  comparison__equal   = 0,
  comparison__greater = +1,
} ;

// ------------------

typedef
  size_t   /* as */ errorcode_t;

// ------------------

// similar to "Java" exception (s)
typedef
  pointer* /* as */ warning;

// ------------------

typedef
  pointer* /* as */ varargs;

// ------------------

typedef
  size_t   /* as */ index_t;
typedef
  size_t   /* as */ count_t;

// ------------------

struct sizedptr
{
  pointer* /* var */ itemptr;
  size_t   /* var */ itemsize;
} ;

struct sizedstartptr
{
  pointer* /* var */ startptr;
  size_t   /* var */ startsize;

  pointer* /* var */ itemptr;
  size_t   /* var */ itemsize;
} ;

struct typedptr
{
  pointer*   /* var */ itemptr;
  typecode_t /* var */ itemtype;
} ;

struct dynamicarray
{
  pointer* /* var */ arrayptr;
  size_t   /* var */ itemsize;
  count_t  /* var */ itemcount;
} ;

// ------------------

#define STARTMARKER  ((ansichar) 26)
#define FINISHMARKER ((ansichar) 27)

#define coresystem__STARTMARKER  ((ansichar) 26)
#define coresystem__FINISHMARKER ((ansichar) 27)

// ------------------

typedef
  ansichar /* as */ ansimarkstring;

// ------------------

typedef
  pointer* /* as */ osfilesystem;

// ------------------

extern osfilesystem* /* var */ defaultfilesys;
extern osfilesystem* /* var */ coresystem__defaultfilesys;

// ------------------

typedef
  pointer* /* as */ stream;

// ------------------

extern stream* /* var */ inputstream;
extern stream* /* var */ ouputstream;
extern stream* /* var */ errorstream;

extern stream* /* var */ coresystem__inputstream;
extern stream* /* var */ coresystem__ouputstream;
extern stream* /* var */ coresystem__errorstream;

// ------------------

extern stream* /* var */ inputstream;
extern stream* /* var */ ouputstream;
extern stream* /* var */ errorstream;

// ------------------

void /* func */ coresystem__nothing
  ( noparams );

void /* func */ coresystem__pause
  ( noparams );
  
void /* func */ coresystem__exit
  (/* in */ const int /* param */ AExitCode);

// ------------------

comparison /* func */ coresystem__inttocmp
  (/* in */ const int /* param */ AExitCode);



 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coresystem__modulename
  ( noparams );

/* override */ int /* func */ coresystem__setup
  ( noparams );

/* override */ int /* func */ coresystem__setoff
  ( noparams );

// ------------------

#endif // CORESYSTEM__H

// } // namespace coresystem