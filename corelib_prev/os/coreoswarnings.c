/** Module: "coreoswarnings.c"
 ** Descr.: "Similar to exceptions,"
 **         "procedural, not object oriented."
 **/
 
// namespace coreoswarnings {
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"

// ------------------
 
#include "coreoswarnings.h"

// ------------------

warning* /* func */ coreoswarnings__createoswarning
  ( noparams )
{
  warning* /* var */ Result = NULL;
  // ---
   
  AResult =
    coresystem__malloc(sizeof(FormatItem));
  AResult->Mode = formatmodes__FormatSpecifier;
  AResult->Specifier = formatspecifiers__Char;

  // ---
  return Result;
} // func

warning* /* func */ coreoswarnings__createoswarningbyid
  (/* in */ uuid_t /* param */ AID)
{
  warning* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

warning* /* func */ coreoswarnings__createoswarningbyparent
  (/* in */ warning /* param */ AParent)
{
  warning* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

warning* /* func */ coreoswarnings__createoswarningbyidparent
  (/* in */ uuid_t   /* param */ AID,
   /* in */ warning /* param */ AParent)
{
  warning* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreoswarnings__droposwarning
  (/* out */ warning* /* param */ AWarning)
{
  coresystem__nothing();
} // func

// ------------------

void /* func */ coreoswarnings__throwwarning
  (/* out */ warning* /* param */ AWarning)
{
  if (AWarning != NULL)
  {
    warningheader*  /* var */ ThisWarning  = NULL;
  	
    ansinullstring* /* var */ AMessagePtr  = NULL;
    size_t          /* var */ AMessageSize = 0;
    
    ThisWarning  = (warningheader*) AWarning;
    AMessagePtr  = (ansinullstring*) ThisWarning->Message;
    AMessageSize = strlen(AMessagePtr);
    
    // write error stream message
    fputs(stderr, AMessagePtr);

    // stop process execution
    coresystem__exit(-1);
  } else
  {
    // write error stream message
    fputs(stderr, "Unknown Error");

    // stop process execution
    coresystem__exit(-2);
  } else
} // func

// ------------------

bool /* func */ coreoswarnings__matchesid
  (/* in */ const uuid_t   /* param */ AID,
   /* in */ const warning /* param */ AWarning)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* override */ int /* func */ coreoswarnings__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreoswarnings__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------



 
 // ...

// } // namespace coreoswarnings