/** Module: "coreoswarnings.h"
 ** Descr.: "Similar to exceptions,"
 **         "procedural, not object oriented."
 **/
 
// namespace coreoswarnings {
 
// ------------------
 
#ifndef COREOSWARNINGS__H
#define COREOSWARNINGS__H
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"

// ------------------

// similar to "Java" exception (s)
struct warningheader
{
  uuid     /* var */ Identifier;
  
  // "ansinulls" by now
  pointer* /* var */ Message;
  
  // recasted from another exception
  warning* /* var */ Parent;
} ;

typedef
  warningheader  /* as */ warning_t;
typedef
  warningheader* /* as */ warning_p;

// ------------------

warning* /* func */ coreoswarnings__createoswarning
  ( noparams );

warning* /* func */ coreoswarnings__createoswarningbyid
  (/* in */ uuid_t /* param */ AID);

warning* /* func */ coreoswarnings__createoswarningbyparent
  (/* in */ warning* /* param */ AParent);

warning* /* func */ coreoswarnings__createoswarningbyidparent
  (/* in */ uuid_t   /* param */ AID,
   /* in */ warning* /* param */ AParent);

void /* func */ coreoswarnings__droposwarning
  (/* out */ warning** /* param */ AWarning);

void /* func */ coreoswarnings__throwwarning
  (/* out */ warning* /* param */ AWarning);

// ------------------

bool /* func */ coreoswarnings__matchesid
  (/* in */ const uuid_t   /* param */ AID,
   /* in */ const warning* /* param */ AWarning);

// ------------------

/* override */ int /* func */ coreoswarnings__setup
  ( noparams );

/* override */ int /* func */ coreoswarnings__setoff
  ( noparams );

// ------------------


 // ...
 
// ------------------
 
#endif // COREOSWARNINGS__H

// } // namespace coreoswarnings