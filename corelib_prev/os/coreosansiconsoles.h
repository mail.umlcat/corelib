/** Module: "coreosansiconsoles.h"
 ** Descr.: "Predefined input and output operations,"
 ** "from keyboard and into screen."
 ** "Look, Ma, no formatting !!!"
 **/
 
// namespace coreosansiconsoles {
 
// ------------------
 
#ifndef COREOSANSICONSOLES__H
#define COREOSANSICONSOLES__H
 
// ------------------
 
#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"
#include "corestreams.h"
#include "coreansicharstreams.h"

// ------------------

bool /* func */ coreosansiconsoles__tryreadchar
  (/* out */ ansichar* /* param */ ABuffer);

bool /* func */ coreosansiconsoles__trywritechar
  (/* in */ const ansichar* /* param */ ABuffer);

ansichar /* func */ coreosansiconsoles__getchar
  ( noparams );

// ------------------

bool /* func */ coreosansiconsoles__tryreadstr
  (/* inout */ stream*       /* param */ ASource,
   /* out */ ansinullstring* /* param */ ABuffer);

bool /* func */ coreosansiconsoles__trywritestr
  (/* in */ const ansinullstring* /* param */ ABuffer);

bool /* func */ coreosansiconsoles__trywriteeoln
  ( noparams );

bool /* func */ coreosansiconsoles__tryreadstrcount
  (/* out */ ansinullstring* /* param */ ADestBuffer,
   /* in */ const size_t     /* param */ ADestSize);

bool /* func */ coreosansiconsoles__trywritestrcount
  (/* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ASourceSize);

// ------------------

void /* func */ coreosansiconsoles__readstr
  (/* out */ ansinullstring* /* param */ ADestBuffer);

void /* func */ coreosansiconsoles__readstrcount
  (/* out */ ansinullstring* /* param */ ADestBuffer,
   /* in */ const size_t     /* param */ ADestSize)

void /* func */ coreosansiconsoles__writestr
  (/* in */ const ansinullstring* /* param */ ASourceBuffer);

void /* func */ coreosansiconsoles__trywritestrcount
  (/* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ASourceSize);

void /* func */ coreosansiconsoles__writeeoln
  ( noparams );



 // ...

// ------------------

/* override */ int /* func */ coreosansiconsoles__setup
  ( noparams );

/* override */ int /* func */ coreosansiconsoles__setoff
  ( noparams );


// ------------------
 
#endif // COREOSANSICONSOLES__H

// } // namespace coreosansiconsoles