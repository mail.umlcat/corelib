/** Module: "coreosansiconsoles.c"
 ** Descr.: "Predefined input and output operations,"
 ** "from keyboard and into screen."
 ** "Look, Ma, no formatting !!!"
 **/
 
// namespace coreosansiconsoles {
 
 // ------------------
 
#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"
#include "corestreams.h"
#include "coreansicharstreams.h"

 // ------------------
 
#include "coreosansiconsoles.h"

 // ------------------

bool /* func */ coreosansiconsoles__tryreadchar
  (/* out */ ansichar* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
  
  Result = (AValue != NULL);
  if (Result)
  {
    *AValue = ansinullchar;
    
    int /* var */ AIntValue = 0;
    
    AIntValue = getchar();
    Result = (AIntValue != EOF);
    if (Result)
    {
      *AValue = (ansichar) AIntValue;
    }
  } // if
  
  // ---
  return Result;
} // func

bool /* func */ coreosansiconsoles__trywritechar
  (/* in */ const ansichar /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
  
  Result = (AValue != NULL);
  if (Result)
  {
    putchar(AValue);
  } // if
  
  // ---
  return Result;
} // func

ansichar /* func */ coreosansiconsoles__getchar
  ( noparams )
{
  ansichar /* var */ Result = nullchar;
  // ---
  
  /* discard */ coreosansiconsoles__tryreadchar
    (&Result);
  
  // ---
  return Result;
} // func

 // ------------------

bool /* func */ coreosansiconsoles__tryreadstr
  (/* out */ ansinullstring* /* param */ ABuffer)
{
  bool /* var */ Result = false;
  // ---
  
  Result = (AValue != NULL);
  if (Result)
  {
    fgets(AValue, strlen(AValue), stdin);
  } // if
  
  // ---
  return Result;
} // func

bool /* func */ coreosansiconsoles__trywritestr
  (/* in */ const ansinullstring* /* param */ ABuffer)
{
  bool /* var */ Result = false;
  // ---
  
  Result = (AValue != NULL);
  if (Result)
  {
    puts(*AValue);
  } // if
  
  // ---
  return Result;
} // func

bool /* func */ coreosansiconsoles__trywriteeoln
  ( noparams )
{
  bool /* var */ Result = false;
  // ---
  
  puts("\n");
  
  // ---
  return Result;
} // func

bool /* func */ coreosansiconsoles__tryreadstrcount
  (/* out */ ansinullstring* /* param */ ADestBuffer,
   /* in */  const size_t    /* param */ ADestSize)
{
  bool /* var */ Result = false;
  // ---
  
  Result = (AValue != NULL);
  if (Result)
  {
    fgets(ADestBuffer, ADestSize, stdin);
  } // if
 
  // ---
  return Result;
} // func

bool /* func */ coreosansiconsoles__trywritestrcount
  (/* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ASourceSize)
{
  bool /* var */ Result = false;
  // ---
  
  Result = (AValue != NULL);
  if (Result)
  {
    fputs(ADestBuffer, ADestSize, stdin);
  } // if
  
  // ---
  return Result;
} // func

 // ------------------

void /* func */ coreosansiconsoles__readstr
  (/* out */ ansinullstring* /* param */ ADestBuffer)
{
  /* discard */ coreosansiconsoles__tryreadstr
    (ADestBuffer);
} // func

void /* func */ coreosansiconsoles__writestr
  (/* in */ const ansinullstring* /* param */ ASourceBuffer)
{
  /* discard */ coreosansiconsoles__trywritestr
    (ASourceBuffer);
} // func

void /* func */ coreosansiconsoles__writeeoln
  ( noparams )
{
  /* discard */ coreosansiconsoles__trywriteeoln
    ( );
} // func

// ------------------

/* override */ int /* func */ coreosansiconsoles__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreosansiconsoles__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

 
 // ...

// } // namespace coreosansiconsoles