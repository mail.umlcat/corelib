/** Module: "coreosfilesys.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coreosfilesys {
 
// ------------------
 
#include <stdlib.h"
#include <stddef.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"

#include <dirent.h"
#include <sys/stat.h"

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansistrs.h"
#include "coreansiospaths.h"

// ------------------
 
#include "coreosfilesys.h"
 
// ------------------

osfilesystem* /* func */ coreosfilesys__createosfilesystem
  ( noparams )
{
  osfilesystem* /* var */ Result = NULL;
  // ---
     
  Result =   
    corememory__clearallocate(sizeof(osfilesystemheader));
    
  // ---
  return Result;
} // func

void /* func */ coreosfilesys__droposfilesystem
  (/* out */ osfilesystem** /* param */ AFileSystem)
{
  corememory__cleardeallocate
    (AFileSystem, sizeof(osfilesystemheader));
} // func

// ------------------

bool /* func */ coreosfilesys__pathfound
  (/* in */ const ansinullstring /* param */ APath)
{
  bool /* var */ Result = false;
  // ---
     
  if (AFileSystem != NULL)
  {

  } // if
     
  // ---
  return Result;
} // func
 
bool /* func */ coreosfilesys__filefound
  (/* in */ const ansinullstring /* param */ AFullFileName)
{
  bool /* var */ Result = false;
  // ---
     
  if (AFileSystem != NULL)
  {

  } // if
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreosfilesys__supportlinks
  (/* in */ osfilesystem* /* param */ AFileSystem)
{
  bool /* var */ Result = false;
  // ---
     
  if (AFileSystem != NULL)
  {

  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coreosfilesys__supportlongfilenames
  (/* in */ osfilesystem* /* param */ AFileSystem)
{
  bool /* var */ Result = false;
  // ---
     
  if (AFileSystem != NULL)
  {

  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coreosfilesys__supportlongpaths
  (/* in */ osfilesystem* /* param */ AFileSystem)
{
  bool /* var */ Result = false;
  // ---
     
  if (AFileSystem != NULL)
  {

  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coreosfilesys__supportfileextensions
  (/* in */ osfilesystem* /* param */ AFileSystem)
{
  bool /* var */ Result = false;
  // ---
     
  if (AFileSystem != NULL)
  {

  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coreosfilesys__ishierarchical
  (/* in */ osfilesystem* /* param */ AFileSystem)
{
  bool /* var */ Result = false;
  // ---
     
  if (AFileSystem != NULL)
  {

  } // if
     
  // ---
  return Result;
} // func

// ------------------

oswarning* /* func */ coreosfilesys__recreatepath
  (/* in */ const ansinullstring /* param */ APath)
{
  oswarning* /* var */ Result = NULL;
  // ---
     
  if (Result != NULL)
  {

  } // if
     
  // ---
  return Result;
} // func
 
// ------------------

osfilesysiterator* /* func */ coreosfilesys__createiterator
  (/* in */ const osfilesystem* /* param */ AFileSys, 
   /* in */ osfilesysparams*    /* param */ AParams);
{
  osfilesysiterator* /* var */ Result = NULL;
  // ---
     
  if (! coreansinullstrs__isemptysize
       (AParams->SourcePathBuffer, AParams->SourcePathSize);
  {
    Result =
      corememory__clearallocate(sizeof(osfilesysiteratorheader));

    Result->Params->FullPathBuffer =
      coreansinullstrs__compactcopy(ASourcePathBuffer);
    Result->Params->FullPathSize =
      ASourcePathSize;
      
    Result->DirHeader =
      opendir(Result->Params->FullPathBuffer);
    
    if (Result->DirHeader == NULL)
    {
      coreosfilesys__dropiterator(*Result);
    } // if
  } // if
 
  // ---
  return Result;
} // func

void /* func */ coreosfilesys__dropiterator
  (/* out */ osfilesysiterator** /* param */ AIterator);
{
  if (! coreansinullstrs__isemptysize
       (AIterator->Params->SourcePathBuffer, AIterator->Params->SourcePathSize);
  {
    coreansinullstrs__dropstr	
      (&(AIterator->Params->FullPathBuffer), AIterator->Params->SourcePathSize);
      
    if (AIterator->Params->DirEntryItem != NULL)
    {
      //closedir(AIterator->DirHeader);
    }

    if (AIterator->DirHeader != NULL)
    {
      closedir(AIterator->DirHeader);
    }
    
    corememory__cleardeallocate
      (&AIterator, sizeof(osfilesysiteratorheader));   
  } // if
} // func

// ------------------

bool /* func */ coreosfilesys__isdone
  (/* in */ const osfilesysiterator* /* param */ AIterator);
{
  bool /* var */ Result = false;
  // ---
     
  if (AIterator != NULL)
  {

  } // if
     
  // ---
  return Result;
} // func

void /* func */ coreosfilesys__movenext
  (/* inout */ osfilesysiterator* /* param */ AIterator);
{
  if (AIterator != NULL)
  {

  } // if
} // func

void /* func */ coreosfilesys__readitem
  (/* inout */ osfilesysiterator*   /* param */ AIterator
   /* out */   const osfilesysitem* /* param */ AItem)
{
  bool CanContinue =
    (AIterator != NULL);
  if (CanContinue)
  {

  } // if
} // func

 // ...

// ------------------

/* override */ int /* func */ coreosfilesys__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  defaultfilesys =
    coreosfilesystems__createosfilesystem
      ( noparams );
  osfilesys__defaultfilesys =
    corememory__share(defaultfilesys);

  // ---
  return Result;
} // func

/* override */ int /* func */ coreosfilesys__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
  
  corememory__unshare
    (&coresystem__defaultfilesys);
  coreosfilesystems__droposfilesystem
    (&defaultfilesys);

  // ---
  return Result;
} // func

// ------------------

 
 // ...

// } // namespace coreosfilesys