/** Module: "coreosfilesys.h"
 ** Descr.: "..."
 **/
 
// namespace coreosfilesys {
 
// ------------------
 
#ifndef COREOSFILESYS__H
#define COREOSFILESYS__H
 
// ------------------
 
#include <stdlib.h"
#include <stddef.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"

#include <dirent.h"
#include <sys/stat.h"

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansistrs.h"
#include "coreansiospaths.h"

// ------------------

struct osfilesysitem
{
  dirent* /* var */ DirEntryItem;

	
//  listdirections        /* var */ Direction;
  
//  count_t               /* var */ ItemsCount;
//  size_t                /* var */ ItemsSize;
  
//  osfilesysnode* /* var */ CurrentItem;
} ;

// ------------------

struct osfilesystemheader
{
  // pointer to node before first node
  //osfilesystemnode* /* var */ TopMarker;
  
  // pointer to node before last node
  //osfilesystemnode* /* var */ BottomMarker;

  // only applies when items are same type
  // "unknowntype" value means different type
  typecode /* var */ ItemsType;

  // applies instead of "typecode"
  // "0" means not used
  size_t   /* var */ ItemsSize;

  // restricts how many items can be stored
  // "0" means not used
  count_t  /* var */ ItemsMaxCount;

  // how many items are currently stored
  count_t  /* var */ ItemsCurrentCount;
  
  // indicates iterator related objects
  count_t  /* var */ IteratorCount;

  bool /* var */ SupportLinks;

  bool /* var */ SupportLongFileNames;

  bool /* var */ SupportLongPaths;

  bool /* var */ SupportFileExtensions;

  bool /* var */ IsHierarchical;
  
  // ...
} ;

typedef
  osfilesystem       /* as */ osfilesystem_t;
typedef
  osfilesystem*      /* as */ osfilesystem_p;

// ------------------

struct osfilesysparamsheader
{
  ansinullstring* /* var */ ASourcePathBuffer, 
  size_t          /* var */ ASourcePathSize,
  
  bool            /* var */ WantsDirs;
  bool            /* var */ WantsFiles;
  bool            /* var */ WantsSpecialDirs;

} ;

typedef
  osfilesysparamsheader /* as */ osfilesysparams;
typedef
  osfilesysparams       /* as */ osfilesysparams_t;
typedef
  osfilesysparams*      /* as */ osfilesysparams_p;

// ------------------

struct osfilesysiteratorheader
{
  DIR*             /* var */ DirHeader;

  dirent*          /* var */ DirEntryItem;
  
  osfilesysparams* /* var */ Params;
 
//  listdirections        /* var */ Direction;
  
//  count_t               /* var */ ItemsCount;
} ;

typedef
  osfilesysiteratorheader /* as */ osfilesysiterator;
typedef
  osfilesysiterator       /* as */ osfilesysiterator_t;
typedef
  osfilesysiterator*      /* as */ osfilesysiterator_p;

// ------------------

osfilesystem* /* func */ coreosfilesys__createosfilesystem
  ( noparams );

void /* func */ coreosfilesys__droposfilesystem
  (/* out */ osfilesystem** /* param */ AFileSystem);

// ------------------

bool /* func */ coreosfilesys__pathfound
  (/* in */ const ansinullstring /* param */ APath);

bool /* func */ coreosfilesys__filefound
  (/* in */ const ansinullstring /* param */ AFullFileName);
  
// ------------------

bool /* func */ coreosfilesys__supportlinks
  (/* in */ osfilesystem* /* param */ AFileSystem);
  
bool /* func */ coreosfilesys__supportlongfilenames
  (/* in */ osfilesystem* /* param */ AFileSystem);
  
bool /* func */ coreosfilesys__supportlongpaths
  (/* in */ osfilesystem* /* param */ AFileSystem);

// ------------------

oswarning* /* func */ coreosfilesys__recreatepath
  (/* in */ const ansinullstring /* param */ APath);

// ------------------

osfilesysiterator* /* func */ coreosfilesys__createiterator
  (/* in */ const osfilesystem* /* param */ AFileSys, 
   /* in */ osfilesysparams*    /* param */ AParams);

void /* func */ coreosfilesys__dropiterator
  (/* out */ osfilesysiterator** /* param */ AIterator);

// ------------------

bool /* func */ coreosfilesys__isdone
  (/* in */ const osfilesysiterator* /* param */ AIterator);

void /* func */ coreosfilesys__movenext
  (/* inout */ osfilesysiterator* /* param */ AIterator);

void /* func */ coreosfilesys__readitem
  (/* inout */ osfilesysiterator*   /* param */ AIterator
   /* out */   const osfilesysitem* /* param */ AItem);

// ------------------

 // ...
 
// ------------------

/* override */ int /* func */ coreosfilesys__setup
  ( noparams );

/* override */ int /* func */ coreosfilesys__setoff
  ( noparams );

// ------------------
 
#endif // COREOSFILESYS__H

// } // namespace coreosfilesys