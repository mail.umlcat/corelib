/** Module: "coreansiospaths.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coreansiospaths {
 
// ------------------
 
#include <stdlib.h"
#include <stddef.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
 
// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "coreansinullstrs.h"
 
 // ------------------
 
#include "coreansiospaths.h"
 
// ------------------

pointer* /* func */ coreansiospaths__internaleolnmarker
  ( noparams )
{
  static ansichar /* var */ eolnbuffer[32];
  return (pointer*) eolnbuffer;
} // func

void /* func */ coreansiospaths__geteolnmarker
  (/* out */ ansinullstring** /* param */ ASourceValue)
{
  *ASourceValue =
     (ansinullstring*) coreansiospaths__internaleolnmarker();
} // func

void /* func */ coreansiospaths__seteolnmarker 
  (/* in */ const ansinullstring* /* param */ ADestValue)
{
  ansinullstring* /* var */ ABufferPtr =
    (ansinullstring*) coreansiospaths__internaleolnmarker();

  coreansinullstrs__assign
    (ABufferPtr, ADestValue);
} // func

pointer* /* func */ coreansiospaths__internalpathsep
  ( noparams )
{
  static ansichar /* var */ eolnbuffer[32];
  return (pointer*) eolnbuffer;
} // func

void /* func */ coreansiospaths__getpathsep
  (/* out */ ansinullstring** /* param */ ASourceValue)
{
  *ASourceValue =
     (ansinullstring*) coreansiospaths__internalpathsep();
} // func

void /* func */ coreansiospaths__setpathsep 
  (/* in */ const ansinullstring* /* param */ ADestValue)
{
  ansinullstring* /* var */ ABufferPtr =
    (ansinullstring*) coreansiospaths__internalpathsep();

  coreansinullstrs__assign
    (ABufferPtr, ADestValue);
} // func

// ------------------

bool /* func */ coreansiospaths__MatchesExt
  (/* in */ const ansinullstring* /* param */ AFileNameExt,
   /* i  */ const ansinullstring* /* param */ AExt)
{
  coresystem__nothing( /* noparams */ );
} // func

void /* func */ coreansiospaths__ChangeExtCopy
  (/* in */  const ansinullstring* /* param */ ASourceFullPath,
   /* in */  const ansinullstring* /* param */ ASourceExt,
   /* out */ ansinullstring*       /* param */ ADestFullPath)
{
  coresystem__nothing( /* noparams */ );
} // func
  
void /* func */ coreansiospaths__ChangeExtChange
  (/* inout */ ansinullstring*       /* param */ ADestFullPath,
   /* in */    const ansinullstring* /* param */ ASourceExt)
{
	
	
} // func


// ------------------

void /* func */ coreansiospaths__ExtractPath
  (/* in */  const ansinullstring* /* param */ ASourceFullPath,
   /* out */ ansinullstring*       /* param */ ADestPath);
{
	
	
} // func

void /* func */ coreansiospaths__ExtractName
  (/* in */  const ansinullstring* /* param */ ASourceFullPath,
   /* out */ ansinullstring*       /* param */ ADestName);
{
	
	
} // func

void /* func */ coreansiospaths__ExtractExt
  (/* in */  const ansinullstring* /* param */ ASourceFullPath,
   /* out */ ansinullstring*       /* param */ ADestName);
{
	
	
} // func


// -----------------

void /* func */ coreansiospaths__DecodePathFileName
  (/* in */  const ansinullstring* /* param */ ASourceFullPath,
   /* out */ ansinullstring*       /* param */ ADestPath,
   /* out */ ansinullstring*       /* param */ ADestNameExt);
{
	
	
} // func

void /* func */ coreansiospaths__EncodePathFileName
  (/* out */ ansinullstring*      /* param */ ADestFullPath,
   /* in */ const ansinullstring* /* param */ ASourcePath,
   /* in */ const ansinullstring* /* param */ ASourceNameExt);
{
	
	
} // func

// ------------------

void /* func */ coreansiospaths__DecodePathFileNameExt
  (/* in */  const ansinullstring* /* param */ ASourceFullPath,
   /* out */ ansinullstring* /* param */ ADestPath,
   /* out */ ansinullstring* /* param */ ADestName,
   /* out */ ansinullstring* /* param */ ADestExt)
{
	
	
} // func
   
void /* func */ coreansiospaths__EncodePathFileNameExt
  (/* out */ ansinullstring*       /* param */ ADestFullPath,
   /* in */  const ansinullstring* /* param */ ASourcePath,
   /* in */  const ansinullstring* /* param */ ASourceName,
   /* in */  const ansinullstring* /* param */ ASourceExt)
{
  coresystem__nothing( /* noparams */ );
} // func
   
 // ...
 
// ------------------

/* override */ int /* func */ coreansiospaths__setup
  ( noparams )
{
  coreansiospaths__seteolnmarker("\n");
} // func

/* override */ int /* func */ coreansiospaths__setoff
  ( noparams )
{
  coresystem__nothing( /* noparams */ );
} // func

// ------------------

// } // namespace coreansiospaths