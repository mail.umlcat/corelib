/** Module: "coreansiospaths.h"
 ** Descr.: "..."
 **/
 
// namespace coreansiospaths {
 
// ------------------
 
#ifndef COREOSANSIPATHS__H
#define COREOSANSIPATHS__H
 
// ------------------
 
#include <stdlib.h"
#include <stddef.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
 
// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansinullstrs.h"

// ------------------

// --> global properties

pointer* /* func */ coreansiospaths__internaleolnmarker
  ( noparams );

void /* func */ coreansiospaths__geteolnmarker
  (/* out */ ansinullstring* /* param */ ASourceValue);

void /* func */ coreansiospaths__seteolnmarker
  (/* in */ const ansinullstring* /* param */ ADestValue);

pointer* /* func */ coreansiospaths__internalpathsep
  ( noparams );

void /* func */ coreansiospaths__getpathsep
  (/* out */ ansinullstring* /* param */ ASourceValue);

void /* func */ coreansiospaths__setpathsep
  (/* in */ const ansinullstring* /* param */ ADestValue);

// ------------------

bool /* func */ coreansiospaths__MatchesExt
  (/* in */ const ansinullstring* /* param */ AFileNameExt,
   /* i  */ const ansinullstring* /* param */ AExt);

void /* func */ coreansiospaths__ChangeExtCopy
  (/* in */  const ansinullstring* /* param */ ASourceFullPath,
   /* in */  const ansinullstring* /* param */ ASourceExt,
   /* out */ ansinullstring*       /* param */ ADestFullPath);

void /* func */ coreansiospaths__ChangeExtChange
  (/* inout */ ansinullstring*       /* param */ ADestFullPath,
   /* in */    const ansinullstring* /* param */ ASourceExt);

// ------------------

void /* func */ coreansiospaths__ExtractPath
  (/* in */  const ansinullstring* /* param */ ASourceFullPath,
   /* out */ ansinullstring*       /* param */ ADestPath);

void /* func */ coreansiospaths__ExtractName
  (/* in */  const ansinullstring* /* param */ ASourceFullPath,
   /* out */ ansinullstring*       /* param */ ADestName);

void /* func */ coreansiospaths__ExtractExt
  (/* in */  const ansinullstring* /* param */ ASourceFullPath,
   /* out */ ansinullstring*       /* param */ ADestName);



// ------------------

void /* func */ coreansiospaths__DecodePathFileName
  (/* in */  const ansinullstring* /* param */ ASourceFullPath,
   /* out */ ansinullstring*       /* param */ ADestPath,
   /* out */ ansinullstring*       /* param */ ADestNameExt);

void /* func */ coreansiospaths__EncodePathFileName
  (/* out */ ansinullstring*      /* param */ ADestFullPath,
   /* in */ const ansinullstring* /* param */ ASourcePath,
   /* in */ const ansinullstring* /* param */ ASourceNameExt);

// ------------------

void /* func */ coreansiospaths__DecodePathFileNameExt
  (/* in */  const ansinullstring* /* param */ ASourceFullPath,
   /* out */ ansinullstring* /* param */ ADestPath,
   /* out */ ansinullstring* /* param */ ADestName,
   /* out */ ansinullstring* /* param */ ADestExt);

void /* func */ coreansiospaths__EncodePathFileNameExt
  (/* out */ ansinullstring*       /* param */ ADestFullPath,
   /* in */  const ansinullstring* /* param */ ASourcePath,
   /* in */  const ansinullstring* /* param */ ASourceName,
   /* in */  const ansinullstring* /* param */ ASourceExt);

 // ...
 
// ------------------

/* override */ int /* func */ coreansiospaths__setup
  ( noparams );

/* override */ int /* func */ coreansiospaths__setoff
  ( noparams );

// ------------------

#endif // COREOSANSIPATHS__H

// } // namespace coreansiospaths