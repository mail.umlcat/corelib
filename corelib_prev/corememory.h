/** Module: "corememory.h"
 ** Descr.: "Memory Management Library."
 **/
 
// namespace corememory {
 
// ------------------
 
#ifndef COREMEMORY__H
#define COREMEMORY__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
//#include "corefunctors.h>
//#include "coreuuids.h>
//#include "coreoswarnings.h>

// ------------------

//#include "coremem8s.h"
//#include "coremem16s.h"
//#include "coremem32s.h"
//#include "coremem64s.h"
//#include "coremem128s.h"
//#include "coremem256s.h"
//#include "coremem48s.h"
//#include "coremem80s.h"

// ------------------

enum memoryintegers
{
  memoryintegers__8_t,    
  memoryintegers__16_t,   
  memoryintegers__32_t,   
  memoryintegers__64_t,   
  memoryintegers__128_t,  
  memoryintegers__256_t,  
  memoryintegers__48_t,
  memoryintegers__80_t,
} ;

typedef
  enum memoryintegers /* as */ corememory__memoryintegers;

// ------------------

// used to separate memory allocation
// from math arithmetic

typedef
  uint8_t       /* as */ mem8_t;
typedef
  uint8_p       /* as */ mem8_p;
  
typedef
  uint16_t      /* as */ mem16_t;
typedef
  uint16_p      /* as */ mem16_p;

typedef
  uint32_t      /* as */ mem32_t;
typedef
  uint32_p      /* as */ mem32_p;
  
typedef
  uint64_t      /* as */ mem64_t;
typedef
  uint64_p      /* as */ mem64_p;

typedef
  uint32_t      /* as */ mem128_t[4];
typedef
  mem128_t*     /* as */ mem128_p;

typedef
  uint32_t      /* as */ mem256_t[8];
typedef
  mem256_t*     /* as */ mem256_p;

// ------------------

// commonly used aliases
typedef
  mem8_t   /* as */ byte_t;
typedef
  mem8_p   /* as */ byte_p;
  
typedef
  mem16_t  /* as */ word_t;
typedef
  mem16_p  /* as */ word_p;

typedef
  mem32_t  /* as */ dword_t;
typedef
  mem32_p  /* as */ dword_p;
  
typedef
  mem64_t  /* as */ qword_t;
typedef
  mem64_p  /* as */ qword_p;

typedef
  mem128_t /* as */ oword_t;
typedef
  mem128_p /* as */ oword_p;

// ------------------

// used for floats
typedef
  uint48_t       /* as */ mem48_t;
typedef
  uint48_t*      /* as */ mem48_p;

// used for floats
typedef
  uint80_t       /* as */ mem80_t;
typedef
  uint80_t       /* as */ mem80_p;

// ------------------

// include files:
// {
typedef
  uint64_t       /* as */ maxmem_t;
typedef
  uint64_p       /* as */ maxmem_p;
// }

// ------------------

/* inline */ bool /* func */ corememory__isassigned
  (/* in */ const pointer* /* param */ ASource);

/* inline */ bool /* func */ corememory__isassignedsize
  (/* in */ const pointer* /* param */ ASourceBuffer, 
   /* in */ const size_t   /* param */ ASourceSize);

/* inline */ bool /* func */ corememory__isassignedarray
  (/* in */ const pointer* /* param */ AArrayBuffer, 
   /* in */ const count_t  /* param */ AItemCount);

/* inline */ bool /* func */ corememory__isassignedarraysize
  (/* in */ const pointer* /* param */ AArrayBuffer, 
   /* in */ const size_t   /* param */ AItemSize,
   /* in */ const count_t  /* param */ AItemCount);

// ------------------

/* inline */ size_t /* func */ corememory__minsize
  (/* in */ const size_t /* param */ A,
   /* in */ const size_t /* param */ B);

/* inline */ size_t /* func */ corememory__maxsize
  (/* in */ const size_t /* param */ A,
   /* in */ const size_t /* param */ B);

/* inline */ size_t /* func */ corememory__zerominsize
  (/* in */ const size_t /* param */ A,
   /* in */ const size_t /* param */ B);

/* inline */ size_t /* func */ corememory__zeromaxsize
  (/* in */ const size_t /* param */ A,
   /* in */ const size_t /* param */ B);

/* inline */ size_t /* func */ corememory__oneminsize
  (/* in */ const size_t /* param */ A,
   /* in */ const size_t /* param */ B);

/* inline */ size_t /* func */ corememory__onemaxsize
  (/* in */ const size_t /* param */ A,
   /* in */ const size_t /* param */ B);

// ------------------

/* inline */ bool /* func */ corememory__trysinttohdwmaxint
  (/* in */  enum memoryintegers /* param */ AMemType,
   /* out */ pointer*       /* param */ ADestPtr,
   /* in */  pointer*       /* param */ ASourcePtr);

/* inline */ bool /* func */ corememory__trysinttosfwmaxint
  (/* in */  enum memoryintegers /* param */ AMemType,
   /* out */ pointer*       /* param */ ADestPtr,
   /* in */  pointer*       /* param */ ASourcePtr);

// ------------------

/* inline */ void /* func */ corememory__sinttohdwmaxint
  (/* in */  enum memoryintegers /* param */ AMemType,
   /* out */ pointer*       /* param */ ADestPtr,
   /* in */  pointer*       /* param */ ASourcePtr);

/* inline */ void /* func */ corememory__sinttosfwmaxint
  (/* in */  enum memoryintegers /* param */ AMemType,
   /* out */ pointer*       /* param */ ADestPtr,
   /* in */  pointer*       /* param */ ASourcePtr);

// ------------------

bool /* func */ corememory__areoverlapped
  (/* in */ const /* nonrestricted */ pointer* /* param */ A,
   /* in */ const /* nonrestricted */ pointer* /* param */ B,
   /* in */ const size_t                       /* param */ ACount);

bool /* func */ corememory__sametype
  (/* in */  const typecode /* param */ A,
   /* in */  const typecode /* param */ B);

// -------------------

size_t /* func */ corememory__typetosize
  (/* in */ const typecode /* param */ ADataType);

pointer* /* func */ corememory__new
  (/* in */ const typecode /* param */ ADataType);

void /* func */ corememory__drop
  (/* out */ pointer**      /* param */ ADest,
   /* in */  const typecode /* param */ ADataType);

pointer* /* func */ corememory__newarray
  (/* in */ const typecode /* param */ ADataType,
   /* in */ const count_t  /* param */ AItemCount);

void /* func */ corememory__droparray
  (/* out */ pointer**      /* param */ ADest,
   /* in */  const typecode /* param */ ADataType,
   /* in */  const count_t  /* param */ AItemCount);

// -------------------

/* inline */ pointer* /* func */ corememory__itemat
  (/* inout */ pointer*      /* param */ ABufferPtr,
   /* in */    const index_t /* param */ AItemIndex);

/* inline */ pointer* /* func */ corememory__itematbysize
  (/* inout */ pointer*      /* param */ ABufferPtr,
   /* in */    const index_t /* param */ AItemIndex,
   /* in */    const size_t  /* param */ AItemSize);

/* inline */ pointer* /* func */ corememory__itematbytype
  (/* inout */ pointer*       /* param */ ABufferPtr,
   /* in */    const index_t  /* param */ AItemIndex,
   /* in */    const typecode /* param */ AItemType);

// -------------------

void /* func */ corememory__insertat
  (/* inout */ pointer*      /* param */ ADestPtr,
   /* in */    const size_t  /* param */ ADestSize,
   /* in */    const index_t /* param */ AIndex,
   /* in */    const count_t /* param */ AItemCount);

void /* func */ corememory__deleteat
  (/* inout */ pointer*      /* param */ ADestPtr,
   /* in */    const size_t  /* param */ ADestSize,
   /* in */    const index_t /* param */ AIndex,
   /* in */    const count_t /* param */ AItemCount);

// -------------------

void /* func */ corememory__insertatarray
  (/* inout */ pointer*      /* param */ ADestPtr,
   /* in */    const size_t  /* param */ ADestSize,
   /* in */    const size_t  /* param */ AItemSize,
   /* in */    const index_t /* param */ AIndex,
   /* in */    const count_t /* param */ AItemCount);

void /* func */ corememory__deleteatarray
  (/* inout */ pointer*      /* param */ ADestPtr,
   /* in */    const size_t  /* param */ ADestSize,
   /* in */    const size_t  /* param */ AItemSize,
   /* in */    const index_t /* param */ AIndex,
   /* in */    const count_t /* param */ AItemCount);
   
// -------------------

/* inline */ bool /* func */ corememory__assigned
  (/* inout */ pointer* /* param */ APointer);

/* inline */ pointer* /* func */ corememory__transfer
  (/* inout */ pointer** /* param */ APointer);
 
/* inline */ pointer* /* func */ corememory__share
  (/* inout */ pointer** /* param */ APointer);

/* inline */ void /* func */ corememory__unshare
  (/* inout */ pointer** /* param */ APointer);

// -------------------

pointer* /* func */ corememory__allocate
  (/* in */ const size_t /* param */ ASize);

void /* func */ corememory__deallocate
  (/* out */ pointer**    /* param */ ADest,
   /* in */  const size_t /* param */ ASize);

// -------------------

pointer* /* func */ corememory__allocatearray
  (/* in */ const size_t  /* param */ AItemSize,
   /* in */ const count_t /* param */ AItemCount);

void /* func */ corememory__deallocatearray
  (/* out */ pointer**     /* param */ ADest,
   /* in */  const size_t  /* param */ AItemSize,
   /* in */  const count_t /* param */ AItemCount);

// -------------------

pointer* /* func */ corememory__clearallocate
  (/* in */ const size_t /* param */ ASize);

void /* func */ corememory__cleardeallocate
  (/* out */ pointer**    /* param */ ADest,
   /* in */  const size_t /* param */ ASize);

// -------------------

pointer* /* func */ corememory__clearallocatearray
  (/* in */ const size_t  /* param */ AItemSize,
   /* in */ const count_t /* param */ AItemCount);

void /* func */ corememory__cleardeallocatearray
  (/* out */ pointer**     /* param */ ADest,
   /* in */  const size_t  /* param */ AItemSize,
   /* in */  const count_t /* param */ AItemCount);

// -------------------

void /* func */ corememory__clear
  (/* out */ pointer*     /* param */ ADest,
   /* in */  const size_t /* param */ ASize);

void /* func */ corememory__cleararray
  (/* out */ pointer*      /* param */ ADest,
   /* in */  const size_t  /* param */ AItemSize,
   /* in */  const count_t /* param */ AItemCount);

void /* func */ corememory__fill
  (/* out */ pointer*     /* param */ ADest,
   /* in */  const size_t /* param */ ASize,
   /* in */  const byte_t /* param */ AValue);
   
void /* func */ corememory__fillarray
  (/* out */ pointer*      /* param */ ADest,
   /* in */  const size_t  /* param */ AItemSize,
   /* in */  const count_t /* param */ AItemCount,
   /* in */  const byte_t  /* param */ AValue);

// -------------------

bool /* func */ corememory__safeisclear
  (/* in */ /* restricted */ pointer* /* param */ ASource,
   /* in */ const size_t              /* param */ ACount);

bool /* func */ corememory__safeiscleararray
  (/* in */ /* restricted */ pointer* /* param */ ASource,
   /* in */  const size_t             /* param */ AItemSize,
   /* in */  const count_t            /* param */ AItemCount);

// -------------------

bool /* func */ corememory__safeequal
  (/* in */ /* restricted */ pointer* /* param */ A,
   /* in */ /* restricted */ pointer* /* param */ B,
   /* in */ const size_t              /* param */ ACount);

bool /* func */ corememory__safeequalarray
  (/* in */ /* restricted */ pointer* /* param */ A,
   /* in */ /* restricted */ pointer* /* param */ B,
   /* in */  const size_t             /* param */ AItemSize,
   /* in */  const count_t            /* param */ AItemCount);

// -------------------

void /* func */ corememory__safecopy
  (/* out */ /* restricted */ pointer*       /* param */ ADest,
   /* in */  /* restricted */ const pointer* /* param */ ASource,
   /* in */  const size_t                    /* param */ ACount);
   
void /* func */ corememory__safecopyarray
  (/* out */ /* restricted */ pointer*       /* param */ ADest,
   /* in */  /* restricted */ const pointer* /* param */ ASource,
   /* in */  const size_t                    /* param */ AItemSize,
   /* in */  const count_t                   /* param */ AItemCount);
   
void /* func */ corememory__safereversecopy
  (/* out */ /* restricted */ pointer*       /* param */ ADest,
   /* in */  /* restricted */ const pointer* /* param */ ASource,
   /* in */  const size_t                    /* param */ ACount);

void /* func */ corememory__safereversecopyarray
  (/* out */ /* restricted */ pointer*       /* param */ ADest,
   /* in */  /* restricted */ const pointer* /* param */ ASource,
   /* in */  const size_t                    /* param */ AItemSize,
   /* in */  const count_t                   /* param */ AItemCount);

void /* func */ corememory__safemove
  (/* out */ /* restricted */ pointer* /* param */ ADest,
   /* out */ /* restricted */ pointer* /* param */ ASource,
   /* in */  const size_t              /* param */ ACount);

void /* func */ corememory__safemovearray
  (/* out */ /* restricted */ pointer* /* param */ ADest,
   /* out */ /* restricted */ pointer* /* param */ ASource,
   /* in */  const size_t              /* param */ AItemSize,
   /* in */  const count_t             /* param */ AItemCount);

void /* func */ corememory__overlappedcopy
  (/* out */ /* nonrestricted */ pointer*       /* param */ ADest,
   /* in */  /* nonrestricted */ const pointer* /* param */ ASource,
   /* in */  const size_t                       /* param */ ACount);

void /* func */ corememory__overlappedcopyarray
  (/* out */ /* nonrestricted */ pointer*       /* param */ ADest,
   /* in */  /* nonrestricted */ const pointer* /* param */ ASource,
   /* in */  const size_t                       /* param */ AItemSize,
   /* in */  const count_t                      /* param */ AItemCount);

void /* func */ corememory__overlappedmove
  (/* out */ /* nonrestricted */ pointer* /* param */ ADest,
   /* out */ /* nonrestricted */ pointer* /* param */ ASource,
   /* in */  const size_t                 /* param */ ACount);

void /* func */ corememory__overlappedmovearray
  (/* out */ /* nonrestricted */ pointer* /* param */ ADest,
   /* out */ /* nonrestricted */ pointer* /* param */ ASource,
   /* in */  const size_t                 /* param */ AItemSize,
   /* in */  const count_t                /* param */ AItemCount);

// ------------------

/* inline*/ void /* func */ corememory__safeexchangeptr
  (/* inout */ /* restricted */ pointer** /* param */ A,
   /* inout */ /* restricted */ pointer** /* param */ B);

void /* func */ corememory__safeexchange
  (/* out */ /* restricted */ pointer* /* param */ A,
   /* out */ /* restricted */ pointer* /* param */ B,
   /* in */  const size_t              /* param */ ACount);

void /* func */ corememory__safeexchangearray
  (/* out */ /* restricted */ pointer* /* param */ A,
   /* out */ /* restricted */ pointer* /* param */ B,
   /* in */  const size_t              /* param */ AItemSize,
   /* in */  const count_t             /* param */ AItemCount);

// ------------------

void /* func */ corememory__overlappedexchange
  (/* out */ /* nonrestricted */ pointer* /* param */ A,
   /* out */ /* nonrestricted */ pointer* /* param */ B,
   /* in */  const size_t                 /* param */ ACount);

void /* func */ corememory__overlappedexchangearray
  (/* out */ /* nonrestricted */ pointer* /* param */ A,
   /* out */ /* nonrestricted */ pointer* /* param */ B,
   /* in */  const size_t                 /* param */ AItemSize,
   /* in */  const count_t                /* param */ AItemCount);

pointer* /* func */ corememory__duplicatecopy
  (/* in */ pointer*     /* param */ ASourceBuffer,
   /* in */ const size_t /* param */ ASourceSize);

pointer* /* func */ corememory__duplicatecopyarray
  (/* in */ pointer*       /* param */ ASourceBuffer,
   /* in */  const size_t  /* param */ AItemSize,
   /* in */  const count_t /* param */ AItemCount);

// ------------------
 
struct typedptr* /* func */ corememory__packtypedptr
  (/* in */ pointer* /* param */ ASourcePtr,
   /* in */ typecode /* param */ ASourceType);

struct sizedptr* /* func */ corememory__packsizedptr
  (/* in */ pointer* /* param */ ASourcePtr,
   /* in */ size_t   /* param */ ASourceSize);

struct indexedptr* /* func */ corememory__packindexedptr
  (/* in */ pointer* /* param */ ASourcePtr,
   /* in */ size_t   /* param */ ASourceIndex);


 // ...
 
// ------------------

/* override */ int /* func */ corememory__setup
  ( noparams );

/* override */ int /* func */ corememory__setoff
  ( noparams );

// ------------------

#endif // COREMEMORY__H

// } // namespace corememory