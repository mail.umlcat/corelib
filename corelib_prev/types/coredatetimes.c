/** Module: "coredatetimes.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coredatetimes {
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreintegers.h"

// ------------------
 
#include "coredatetimes.h"
 
// ------------------

comparison /* func */ coredatetimes__compare
  (/* in */ ansinullstring* /* param */ A, B)
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func
   
 // ------------------
 
void /* func */ coredatetimes__clear
  (/* inout */ ansinullstring* /* param */ ADest)
{
  // ...
} // func
 
 // ...

// ------------------

/* override */ const ansinullstring* /* func */ coresystem__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "coredatetimes";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coredatetimes__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coredatetimes__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------


// } // namespace coredatetimes