/** Module: "corenibbles.c"
 ** Descr.: "predefined library"
 **/
 
// namespace corenibbles {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "corenibbles.h"
 
// ------------------

nibbleptr* /* func */ corenibbles__createarray
  (/* in */ size_t /* param */ ASize)
{
  // ...
} // func

void /* func */ corenibbles__droparray
  (/* out */ nibbleptr** /* param */ ADest,
   /* in */  size_t      /* param */ ASize)
{
  // ...
} // func

// ------------------

comparison /* func */ corenibbles__compare
  (/* in */ ansinullstring* /* param */ A, B)
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func
   
 // ------------------
 
void /* func */ corenibbles__clear
  (/* inout */ ansinullstring* /* param */ ADest)
{
  // ...
} // func

 // ...

// ------------------

/* override */ const ansinullstring* /* func */ coresystem__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "corenibbles";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ corenibbles__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corenibbles__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------


// } // namespace corenibbles