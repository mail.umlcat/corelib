/** Module: "corenibbles.h"
 ** Descr.: "..."
 **/
 
// namespace corenibbles {
 
// ------------------
 
#ifndef CORENIBBLES__H
#define CORENIBBLES__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

typedef
  uint8_t /* as */ nibble_t;
typedef
  uint8_p /* as */ nibble_p;

// ------------------

enum NibbleStates
{
  nibbleitems__undefined,
  nibbleitems__low,
  nibbleitems__hi,
} ;

struct nibbleptr
{
  pointer*     /* as */ Ptr;
  NibbleStates /* as */ State;
} ;

// ------------------

nibbleptr* /* func */ corenibbles__createarray
  (/* in */ size_t /* param */ ASize);

void /* func */ corenibbles__droparray
  (/* out */ nibbleptr** /* param */ ADest,
   /* in */  size_t      /* param */ ASize);
 
// ------------------

bool /* func */ corenibbles__trygetitemat
  (/* in */  const nibbleptr* /* param */ AList,
   /* in */  const index_t    /* param */ AIndex,
   /* out */ uint8_t*         /* param */ AItem);
   
bool /* func */ coreptrlists__trysetitemat
  (/* in */ const nibbleptr* /* param */ AList,
   /* in */ const index_t    /* param */ AIndex,
   /* in */ const uint8_t    /* param */ AItem);

uint8_t /* func */ corenibbles__getitemat
  (/* in */  const nibbleptr* /* param */ AList,
   /* in */  const index_t    /* param */ AIndex);
   
bool /* func */ coreptrlists__setitemat
  (/* in */ const nibbleptr* /* param */ AList,
   /* in */ const index_t    /* param */ AIndex,
   /* in */ const uint8_t    /* param */ AItem);
 
// ------------------



 // ...
 
// ------------------

/* override */ int /* func */ corenibbles__setup
  ( noparams );

/* override */ int /* func */ corenibbles__setoff
  ( noparams );

// ------------------

#endif // CORENIBBLES__H

// } // namespace corenibbles