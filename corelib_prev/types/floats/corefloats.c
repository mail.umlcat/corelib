/** Module: "corefloats.c"
 ** Descr.: "predefined library"
 **/
 
// namespace corefloats {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "corefloats.h"
 
// ------------------

/* inline */ bool /* func */ corefloats__tryfloattohdwmaxfloat
  (/* in */  floats   /* param */ AFloatType,
   /* out */ pointer* /* param */ ADestPtr,
   /* in */  pointer* /* param */ ASourcePtr)
{
  bool /* var */ Result = FALSE;
  // ---

  switch (AIntType)
  {
    case floats__halffloat_t:
      Result =
        corehalffloats__tryhalffloattohdwmaxfloat
          (ADestPtr, ASourcePtr);
    break;
    
    case floats__singlefloat_t:
      Result =
        coresinglefloats__trysinglefloattohdwmaxfloat
          (ADestPtr, ASourcePtr);
    break;
    
    case floats__doublefloat_t:
      Result =
        coredoublefloats__trydoublefloattohdwmaxfloat
          (ADestPtr, ASourcePtr);
    break;
    
    case floats__quadfloat_t:
      Result =
        corequadfloats__tryquadfloattohdwmaxfloat
          (ADestPtr, ASourcePtr);
    break;
    
    case floats__halffloat_t:
      Result =
        corehalffloats__tryhalffloattohdwmaxfloat
          (ADestPtr, ASourcePtr);
    break;
    
    case floats__longfloat_t:
      Result =
        corelongfloats__trylongfloattohdwmaxfloat
          (ADestPtr, ASourcePtr);
    break;
    
   } // switch

  // ---
  return Result;
} // func

/* inline */ bool /* func */ corefloats__tryfloattosfwmaxfloat
  (/* in */  floats   /* param */ AFloatType,
   /* out */ pointer* /* param */ ADestPtr,
   /* in */  pointer* /* param */ ASourcePtr)
{
  bool /* var */ Result = FALSE;
  // ---

  switch (AIntType)
  {
    case floats__halffloat_t:
      Result =
        corehalffloats__tryhalffloattosfwmaxfloat
          (ADestPtr, ASourcePtr);
    break;
    
    case floats__singlefloat_t:
      Result =
        coresinglefloats__trysinglefloattosfwmaxfloat
          (ADestPtr, ASourcePtr);
    break;
    
    case floats__doublefloat_t:
      Result =
        coredoublefloats__trydoublefloattosfwmaxfloat
          (ADestPtr, ASourcePtr);
    break;
    
    case floats__quadfloat_t:
      Result =
        corequadfloats__tryquadfloattosfwmaxfloat
          (ADestPtr, ASourcePtr);
    break;
    
    case floats__halffloat_t:
      Result =
        corehalffloats__tryhalffloattosfwmaxfloat
          (ADestPtr, ASourcePtr);
    break;
    
    case floats__longfloat_t:
      Result =
        corelongfloats__trylongfloattosfwmaxfloat
          (ADestPtr, ASourcePtr);
    break;
    
  } // switch

  // ---
  return Result;
} // func

// ------------------

/* inline */ void  /* func */ corefloats__floattohdwmaxfloat
  (/* in */  floats   /* param */ AFloatType,
   /* out */ pointer* /* param */ ADestPtr,
   /* in */  pointer* /* param */ ASourcePtr)
{
  /* out */ ADest = 0;
  // ---
     
  /* discard */ corefloats__tryfloattohdwmaxfloat
    (AFloatType, ADestPtr, ASourcePtr);
} // func

/* inline */ void /* func */ corefloats__floattosfwmaxfloat
  (/* in */  floats   /* param */ AFloatType,
   /* out */ pointer* /* param */ ADestPtr,
   /* in */  pointer* /* param */ ASourcePtr)
{
  /* out */ ADest = 0;
  // ---
     
  /* discard */ corefloats__tryfloattosfwmaxfloat
    (AFloatType, ADestPtr, ASourcePtr);
} // func

// ------------------

pointer* /* func */ corefloats__floatptrcopy
  (/* in */ const float_t /* param */ AValue)
{
  pointer* /* var */ Result = NULL;
  // ---
     
  Result =
    coresystem__malloc(sizeof(float_t));
  coresystem__safecopy(Result, &AValue);
  
  // ---
  return Result;
} // func


// ------------------

 // ...
 
// ------------------

/* override */ int /* func */ corefloats__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corefloats__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace corefloats