/** Module: "corequadfloats.c"
 ** Descr.: "predefined library"
 **/
 
// namespace corequadfloats {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "corequadfloats.h"
 
// ------------------

comparison /* func */ corequadfloats__compare
  (/* in */ const quadfloat_t /* param */ A,
   /* in */ const quadfloat_t /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ corequadfloats__areequal
  (/* in */ const quadfloat_t /* param */ A,
   /* in */ const quadfloat_t /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func*/ corequadfloats__isempty
  (/* in */ const quadfloat_t /* param */ ASource);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ corequadfloats__clear
  (/* inout */ quadfloat_t /* param */ ADest);
{
  coresystem__nothing();
} // func

// ------------------

/* inline */ bool /* func */ corequadfloats__ptryquadfloattohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const quadfloat_p      param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

/* inline */ bool /* func */ corequadfloats__ptryquadfloattosfwmaxfloat
// (   out    maxsfwmaxfloat*      param    ADest,
//     in     const quadfloat_p      param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

 

bool /* func */ corequadfloats__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ corequadfloats__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ corequadfloats__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

/* override */ void /* func */ corequadfloats__setup
  ( noparams )
{
  coresystem__nothing();
} // func

/* override */ void /* func */ corequadfloats__setoff
  ( noparams )
{
  coresystem__nothing();
} // func

// ------------------


 // ...

// } // namespace corequadfloats