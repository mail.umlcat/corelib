/** Module: "coredoublefloats.h"
 ** Descr.: "..."
 **/
 
// namespace coredoublefloats {
 
// ------------------
 
#ifndef COREDOUBLEFLOATS__H
#define COREDOUBLEFLOATS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coredoublefloats__compare
  (/* in */ const doublefloat_t /* param */ A,
   /* in */ const doublefloat_t /* param */ B);

bool /* func */ coredoublefloats__areequal
  (/* in */ const doublefloat_t /* param */ A,
   /* in */ const doublefloat_t /* param */ B);

bool /* func*/ coredoublefloats__isempty
  (/* in */ const doublefloat_t /* param */ ASource);

void /* func */ coredoublefloats__clear
  (/* inout */ doublefloat_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ coredoublefloats__ptrydoublefloattohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const doublefloat_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coredoublefloats__ptrydoublefloattosfwmaxfloat
// (   out    sfwmaxfloat_t*       param    ADest,
//     in     const doublefloat_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

/* inline */ void /* func */ coredoublefloats__pdoublefloattohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const doublefloat_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ void /* func */ coredoublefloats__pdoublefloattosfwmaxfloat
// (   out    sfwmaxfloat_t*       param    ADest,
//     in     const doublefloat_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

 // ------------------

bool /* func */ coredoublefloats__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coredoublefloats__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coredoublefloats__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coredoublefloats__setup
  ( noparams );

/* override */ void /* func */ coredoublefloats__setoff
  ( noparams );

// ------------------

#endif // COREDOUBLEFLOATS__H

// } // namespace coredoublefloats