/** Module: "coredoublefloats.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coredoublefloats {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coredoublefloats.h"
 
// ------------------

comparison /* func */ coredoublefloats__compare
  (/* in */ const doublefloat_t /* param */ A,
   /* in */ const doublefloat_t /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coredoublefloats__areequal
  (/* in */ const doublefloat_t /* param */ A,
   /* in */ const doublefloat_t /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func*/ coredoublefloats__isempty
  (/* in */ const doublefloat_t /* param */ ASource);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coredoublefloats__clear
  (/* inout */ doublefloat_t /* param */ ADest);
{
  coresystem__nothing();
} // func

// ------------------

/* inline */ bool /* func */ coredoublefloats__ptrydoublefloattohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const doublefloat_p      param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

/* inline */ bool /* func */ coredoublefloats__ptrydoublefloattosfwmaxfloat
// (   out    maxsfwmaxfloat*      param    ADest,
//     in     const doublefloat_p      param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

 

bool /* func */ coredoublefloats__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coredoublefloats__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coredoublefloats__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coredoublefloats__setup
  ( noparams )
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coredoublefloats__setoff
  ( noparams )
{
  coresystem__nothing();
} // func

// ------------------


 // ...

// } // namespace coredoublefloats