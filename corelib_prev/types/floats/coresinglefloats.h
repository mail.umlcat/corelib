/** Module: "coresinglefloats.h"
 ** Descr.: "..."
 **/
 
// namespace coresinglefloats {
 
// ------------------
 
#ifndef CORESINGLEFLOATS__H
#define CORESINGLEFLOATS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coresinglefloats__compare
  (/* in */ const singlefloat_t /* param */ A,
   /* in */ const singlefloat_t /* param */ B);

bool /* func */ coresinglefloats__areequal
  (/* in */ const singlefloat_t /* param */ A,
   /* in */ const singlefloat_t /* param */ B);

bool /* func*/ coresinglefloats__isempty
  (/* in */ const singlefloat_t /* param */ ASource);

void /* func */ coresinglefloats__clear
  (/* inout */ singlefloat_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ coresinglefloats__ptrysinglefloattohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const singlefloat_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coresinglefloats__ptrysinglefloattosfwmaxfloat
// (   out    sfwmaxfloat_t*       param    ADest,
//     in     const singlefloat_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

/* inline */ void /* func */ coresinglefloats__psinglefloattohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const singlefloat_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ void /* func */ coresinglefloats__psinglefloattosfwmaxfloat
// (   out    sfwmaxfloat_t*       param    ADest,
//     in     const singlefloat_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

 // ------------------

bool /* func */ coresinglefloats__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coresinglefloats__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coresinglefloats__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coresinglefloat__setup
  ( noparams );

/* override */ void /* func */ coresinglefloat__setoff
  ( noparams );

// ------------------

#endif // CORESINGLEFLOATS__H

// } // namespace coresinglefloats