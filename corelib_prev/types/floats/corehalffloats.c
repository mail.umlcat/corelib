/** Module: "corehalffloats.c"
 ** Descr.: "predefined library"
 **/
 
// namespace corehalffloats {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "corehalffloats.h"
 
// ------------------

comparison /* func */ corehalffloats__compare
  (/* in */ const halffloat_t /* param */ A,
   /* in */ const halffloat_t /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ corehalffloats__areequal
  (/* in */ const halffloat_t /* param */ A,
   /* in */ const halffloat_t /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func*/ corehalffloats__isempty
  (/* in */ const halffloat_t /* param */ ASource);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ corehalffloats__clear
  (/* inout */ halffloat_t /* param */ ADest);
{
  coresystem__nothing();
} // func

// ------------------

/* inline */ bool /* func */ corehalffloats__ptryhalffloattohdwmaxfloat
// (   out    maxhdwfloat_t*     param    ADest,
//     in     const halffloat_p  param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

/* inline */ bool /* func */ corehalffloats__ptryhalffloattosfwmaxfloat
// (   out    maxsfwmaxfloat*    param    ADest,
//     in     const halffloat_p  param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

 

bool /* func */ corehalffloats__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ corehalffloats__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ corehalffloats__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

/* override */ void /* func */ corehalffloat__setup
  ( noparams )
{
  coresystem__nothing();
} // func

/* override */ void /* func */ corehalffloat__setoff
  ( noparams )
{
  coresystem__nothing();
} // func

// ------------------


 // ...

// } // namespace corehalffloats