/** Module: "corehalffloats.h"
 ** Descr.: "..."
 **/
 
// namespace corehalffloats {
 
// ------------------
 
#ifndef COREHALFFLOATS__H
#define COREHALFFLOATS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ corehalffloats__compare
  (/* in */ const halffloat_t /* param */ A,
   /* in */ const halffloat_t /* param */ B);

bool /* func */ corehalffloats__areequal
  (/* in */ const halffloat_t /* param */ A,
   /* in */ const halffloat_t /* param */ B);

bool /* func*/ corehalffloats__isempty
  (/* in */ const halffloat_t /* param */ ASource);

void /* func */ corehalffloats__clear
  (/* inout */ halffloat_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ corehalffloats__ptryhalffloattohdwmaxfloat
// (   out    hdwmaxfloat_t*     param    ADest,
//     in     const halffloat_p  param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ corehalffloats__ptryhalffloattosfwmaxfloat
// (   out    sfwmaxfloat_t*     param    ADest,
//     in     const halffloat_p  param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

/* inline */ void /* func */ corehalffloats__phalffloattohdwmaxfloat
// (   out    hdwmaxfloat_t*     param    ADest,
//     in     const halffloat_p  param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ void /* func */ corehalffloats__phalffloattosfwmaxfloat
// (   out    sfwmaxfloat_t*     param    ADest,
//     in     const halffloat_p  param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

 // ------------------

bool /* func */ corehalffloats__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ corehalffloats__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ corehalffloats__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ corehalffloat__setup
  ( noparams );

/* override */ void /* func */ corehalffloat__setoff
  ( noparams );

// ------------------

#endif // COREHALFFLOATS__H

// } // namespace corehalffloats