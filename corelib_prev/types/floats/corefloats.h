/** Module: "corefloats.h"
 ** Descr.: "..."
 **/
 
// namespace corefloats {
 
// ------------------
 
#ifndef COREFLOATS__H
#define COREFLOATS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "corehalffloats.h"
#include "coresinglefloats.h"
#include "coredoublefloats.h"
#include "corequadfloats.h"
#include "coreoctafloats.h"
#include "corelongfloats.h"

// ------------------

enum floats
{
  floats__halffloat_t,
  floats__singlefloat_t,
  floats__doublefloat_t,
  floats__quadfloat_t,
  floats__octafloat_t,
  floats__longfloat_t,
} ;

typedef
  floats /* as */ corefloats__floats;

// ------------------

typedef
  mem16_t   /* as */ halffloat_t;
typedef
  mem16_t*  /* as */ halffloat_p;

typedef
  mem32_t   /* as */ singlefloat_t;
typedef
  mem32_t*  /* as */ singlefloat_p;

typedef
  mem64_t   /* as */ doublefloat_t;
typedef
  mem64_t*  /* as */ doublefloat_p;

typedef
  mem128_t  /* as */ quadfloat_t;
typedef
  mem128_t* /* as */ quadfloat_p;

typedef
  mem256_t  /* as */ octafloat_t;
typedef
  mem256_t* /* as */ octafloat_p;

typedef
  mem80_t   /* as */ longfloat_t;
typedef
  mem80_t*  /* as */ longfloat_p;

// ------------------

// include files:
// {
typedef
  mem64_t   /* as */ hdwmaxfloat_t;
typedef
  mem64_t*  /* as */ hdwmaxfloat_p;
// }

// ------------------

// include files:
// {
typedef
  mem256_t  /* as */ sfwmaxfloat_t;
typedef
  mem256_t* /* as */ sfwmaxfloat_p;
// }

// ------------------

/* inline */ bool /* func */ corefloats__tryfloattohdwmaxfloat
  (/* in */  floats   /* param */ AFloatType,
   /* out */ pointer* /* param */ ADestPtr,
   /* in */  pointer* /* param */ ASourcePtr);

/* inline */ bool /* func */ corefloats__tryfloattosfwmaxfloat
  (/* in */  floats   /* param */ AFloatType,
   /* out */ pointer* /* param */ ADestPtr,
   /* in */  pointer* /* param */ ASourcePtr);

// ------------------

/* inline */ void  /* func */ corefloats__floattohdwmaxfloat
  (/* in */  floats   /* param */ AFloatType,
   /* out */ pointer* /* param */ ADestPtr,
   /* in */  pointer* /* param */ ASourcePtr);

/* inline */ void /* func */ corefloats__floattosfwmaxfloat
  (/* in */  floats   /* param */ AFloatType,
   /* out */ pointer* /* param */ ADestPtr,
   /* in */  pointer* /* param */ ASourcePtr);
   
// ------------------

pointer* /* func */ corefloats__floatptrcopy
  (/* in */ const float_t /* param */ AValue);

// ------------------

 // ...
 
// ------------------

/* override */ int /* func */ corefloats__setup
  ( noparams );

/* override */ int /* func */ corefloats__setoff
  ( noparams );

// ------------------

#endif // COREFLOATS__H

// } // namespace corefloats