/** Module: "coreoctafloats.h"
 ** Descr.: "..."
 **/
 
// namespace coreoctafloats {
 
// ------------------
 
#ifndef COREOCTAFLOATS__H
#define COREOCTAFLOATS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coreoctafloats__compare
  (/* in */ const octafloat_t /* param */ A,
   /* in */ const octafloat_t /* param */ B);

bool /* func */ coreoctafloats__areequal
  (/* in */ const octafloat_t /* param */ A,
   /* in */ const octafloat_t /* param */ B);

bool /* func*/ coreoctafloats__isempty
  (/* in */ const octafloat_t /* param */ ASource);

void /* func */ coreoctafloats__clear
  (/* inout */ octafloat_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ coreoctafloats__ptryoctafloattohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const octafloat_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coreoctafloats__ptryoctafloattosfwmaxfloat
// (   out    sfwmaxfloat_t*       param    ADest,
//     in     const octafloat_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

/* inline */ void /* func */ coreoctafloats__poctafloattohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const octafloat_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ void /* func */ coreoctafloats__poctafloattosfwmaxfloat
// (   out    sfwmaxfloat_t*       param    ADest,
//     in     const octafloat_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

 // ------------------

bool /* func */ coreoctafloats__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coreoctafloats__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coreoctafloats__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coreoctafloats__setup
  ( noparams );

/* override */ void /* func */ coreoctafloats__setoff
  ( noparams );

// ------------------

#endif // COREOCTAFLOATS__H

// } // namespace coreoctafloats