/** Module: "corelongfloats.h"
 ** Descr.: "..."
 **/
 
// namespace corelongfloats {
 
// ------------------
 
#ifndef CORELONGFLOATS__H
#define CORELONGFLOATS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ corelongfloats__compare
  (/* in */ const longfloat_t /* param */ A,
   /* in */ const longfloat_t /* param */ B);

bool /* func */ corelongfloats__areequal
  (/* in */ const longfloat_t /* param */ A,
   /* in */ const longfloat_t /* param */ B);

bool /* func*/ corelongfloats__isempty
  (/* in */ const longfloat_t /* param */ ASource);

void /* func */ corelongfloats__clear
  (/* inout */ longfloat_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ corelongfloats__ptrylongfloattohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const longfloat_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ corelongfloats__ptrylongfloattosfwmaxfloat
// (   out    sfwmaxfloat_t*       param    ADest,
//     in     const longfloat_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

/* inline */ void /* func */ corelongfloats__plongfloattohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const longfloat_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ void /* func */ corelongfloats__plongfloattosfwmaxfloat
// (   out    sfwmaxfloat_t*       param    ADest,
//     in     const longfloat_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

 // ------------------

bool /* func */ corelongfloats__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ corelongfloats__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ corelongfloats__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ corelongfloats__setup
  ( noparams );

/* override */ void /* func */ corelongfloats__setoff
  ( noparams );

// ------------------

#endif // CORELONGFLOATS__H

// } // namespace corelongfloats