/** Module: "coreoctafloats.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coreoctafloats {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "coreoctafloats.h"
 
// ------------------

comparison /* func */ coreoctafloats__compare
  (/* in */ const octafloat_t /* param */ A,
   /* in */ const octafloat_t /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreoctafloats__areequal
  (/* in */ const octafloat_t /* param */ A,
   /* in */ const octafloat_t /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func*/ coreoctafloats__isempty
  (/* in */ const octafloat_t /* param */ ASource);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreoctafloats__clear
  (/* inout */ octafloat_t /* param */ ADest);
{
  coresystem__nothing();
} // func

// ------------------

/* inline */ bool /* func */ coreoctafloats__ptryoctafloattohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const octafloat_p      param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

/* inline */ bool /* func */ coreoctafloats__ptryoctafloattosfwmaxfloat
// (   out    maxsfwmaxfloat*      param    ADest,
//     in     const octafloat_p      param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

 

bool /* func */ coreoctafloats__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreoctafloats__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreoctafloats__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coreoctafloats__setup
  ( noparams )
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coreoctafloats__setoff
  ( noparams )
{
  coresystem__nothing();
} // func

// ------------------


 // ...

// } // namespace coreoctafloats