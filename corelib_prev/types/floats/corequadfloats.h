/** Module: "corequadfloats.h"
 ** Descr.: "..."
 **/
 
// namespace corequadfloats {
 
// ------------------
 
#ifndef COREQUADFLOATS__H
#define COREQUADFLOATS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ corequadfloats__compare
  (/* in */ const quadfloat_t /* param */ A,
   /* in */ const quadfloat_t /* param */ B);

bool /* func */ corequadfloats__areequal
  (/* in */ const quadfloat_t /* param */ A,
   /* in */ const quadfloat_t /* param */ B);

bool /* func*/ corequadfloats__isempty
  (/* in */ const quadfloat_t /* param */ ASource);

void /* func */ corequadfloats__clear
  (/* inout */ quadfloat_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ corequadfloats__ptryquadfloattohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const quadfloat_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ corequadfloats__ptryquadfloattosfwmaxfloat
// (   out    sfwmaxfloat_t*       param    ADest,
//     in     const quadfloat_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

/* inline */ void /* func */ corequadfloats__pquadfloattohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const quadfloat_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ void /* func */ corequadfloats__pquadfloattosfwmaxfloat
// (   out    sfwmaxfloat_t*       param    ADest,
//     in     const quadfloat_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

 // ------------------

bool /* func */ corequadfloats__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ corequadfloats__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ corequadfloats__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ corequadfloats__setup
  ( noparams );

/* override */ void /* func */ corequadfloats__setoff
  ( noparams );

// ------------------

#endif // COREQUADFLOATS__H

// } // namespace corequadfloats