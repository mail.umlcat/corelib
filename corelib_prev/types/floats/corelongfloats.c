/** Module: "corelongfloats.c"
 ** Descr.: "predefined library"
 **/
 
// namespace corelongfloats {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "corelongfloats.h"
 
// ------------------

comparison /* func */ corelongfloats__compare
  (/* in */ const longfloat_t /* param */ A,
   /* in */ const longfloat_t /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ corelongfloats__areequal
  (/* in */ const longfloat_t /* param */ A,
   /* in */ const longfloat_t /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func*/ corelongfloats__isempty
  (/* in */ const longfloat_t /* param */ ASource);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ corelongfloats__clear
  (/* inout */ longfloat_t /* param */ ADest);
{
  coresystem__nothing();
} // func

// ------------------

/* inline */ bool /* func */ corelongfloats__ptrylongfloattohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const longfloat_p      param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

/* inline */ bool /* func */ corelongfloats__ptrylongfloattosfwmaxfloat
// (   out    maxsfwmaxfloat*      param    ADest,
//     in     const longfloat_p      param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

 

bool /* func */ corelongfloats__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ corelongfloats__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ corelongfloats__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

/* override */ void /* func */ corelongfloats__setup
  ( noparams )
{
  coresystem__nothing();
} // func

/* override */ void /* func */ corelongfloats__setoff
  ( noparams )
{
  coresystem__nothing();
} // func

// ------------------


 // ...

// } // namespace corelongfloats