/** Module: "coremathconsts.h"
 ** Descr.: "..."
 **/
 
// namespace coremathconsts {
 
// ------------------
 
#ifndef COREMATHCONSTS__H
#define COREMATHCONSTS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "corefloats.h"

// ------------------

void /* func */ coremathconsts__eulertohalffloat
  (/* out */ halffloat_t* /* param */ ADest);

void /* func */ coremathconsts__eulertosinglefloat
  (/* out */ singlefloat_t* /* param */ ADest);

void /* func */ coremathconsts__eulertodoublefloat
  (/* out */ doublefloat_t* /* param */ ADest);

void /* func */ coremathconsts__eulertoquadfloat
  (/* out */ quadfloat_t* /* param */ ADest);

void /* func */ coremathconsts__eulertooctafloat
  (/* out */ octafloat_t* /* param */ ADest);

void /* func */ coremathconsts__eulertolongfloat
  (/* out */ longfloat_t* /* param */ ADest);
 
// ------------------

void /* func */ coremathconsts__log2etohalffloat
  (/* out */ halffloat_t* /* param */ ADest);

void /* func */ coremathconsts__log2etosinglefloat
  (/* out */ singlefloat_t* /* param */ ADest);

void /* func */ coremathconsts__log2etodoublefloat
  (/* out */ doublefloat_t* /* param */ ADest);

void /* func */ coremathconsts__log2etoquadfloat
  (/* out */ quadfloat_t* /* param */ ADest);

void /* func */ coremathconsts__log2etooctafloat
  (/* out */ octafloat_t* /* param */ ADest);

void /* func */ coremathconsts__log2etolongfloat
  (/* out */ longfloat_t* /* param */ ADest);
 
// ------------------

void /* func */ coremathconsts__log10etohalffloat
  (/* out */ halffloat_t* /* param */ ADest);

void /* func */ coremathconsts__log10etosinglefloat
  (/* out */ singlefloat_t* /* param */ ADest);

void /* func */ coremathconsts__log10etodoublefloat
  (/* out */ doublefloat_t* /* param */ ADest);

void /* func */ coremathconsts__log10etoquadfloat
  (/* out */ quadfloat_t* /* param */ ADest);

void /* func */ coremathconsts__log10etooctafloat
  (/* out */ octafloat_t* /* param */ ADest);

void /* func */ coremathconsts__log10etolongfloat
  (/* out */ longfloat_t* /* param */ ADest);
 
// ------------------

void /* func */ coremathconsts__ln2tohalffloat
  (/* out */ halffloat_t* /* param */ ADest);

void /* func */ coremathconsts__ln2tosinglefloat
  (/* out */ singlefloat_t* /* param */ ADest);

void /* func */ coremathconsts__ln2todoublefloat
  (/* out */ doublefloat_t* /* param */ ADest);

void /* func */ coremathconsts__ln2toquadfloat
  (/* out */ quadfloat_t* /* param */ ADest);

void /* func */ coremathconsts__ln2tooctafloat
  (/* out */ octafloat_t* /* param */ ADest);

void /* func */ coremathconsts__ln2tolongfloat
  (/* out */ longfloat_t* /* param */ ADest);
 
// ------------------

void /* func */ coremathconsts__ln10tohalffloat
  (/* out */ halffloat_t* /* param */ ADest);

void /* func */ coremathconsts__ln10tosinglefloat
  (/* out */ singlefloat_t* /* param */ ADest);

void /* func */ coremathconsts__ln10todoublefloat
  (/* out */ doublefloat_t* /* param */ ADest);

void /* func */ coremathconsts__ln10toquadfloat
  (/* out */ quadfloat_t* /* param */ ADest);

void /* func */ coremathconsts__ln10tooctafloat
  (/* out */ octafloat_t* /* param */ ADest);

void /* func */ coremathconsts__ln10tolongfloat
  (/* out */ longfloat_t* /* param */ ADest);
 
// ------------------

void /* func */ coremathconsts__pitohalffloat
  (/* out */ halffloat_t* /* param */ ADest);

void /* func */ coremathconsts__pitosinglefloat
  (/* out */ singlefloat_t* /* param */ ADest);

void /* func */ coremathconsts__pitodoublefloat
  (/* out */ doublefloat_t* /* param */ ADest);

void /* func */ coremathconsts__pitoquadfloat
  (/* out */ quadfloat_t* /* param */ ADest);

void /* func */ coremathconsts__pitooctafloat
  (/* out */ octafloat_t* /* param */ ADest);

void /* func */ coremathconsts__pitolongfloat
  (/* out */ longfloat_t* /* param */ ADest);
 
// ------------------

void /* func */ coremathconsts__sqrt2tohalffloat
  (/* out */ halffloat_t* /* param */ ADest);

void /* func */ coremathconsts__sqrt2tosinglefloat
  (/* out */ singlefloat_t* /* param */ ADest);

void /* func */ coremathconsts__sqrt2todoublefloat
  (/* out */ doublefloat_t* /* param */ ADest);

void /* func */ coremathconsts__sqrt2toquadfloat
  (/* out */ quadfloat_t* /* param */ ADest);

void /* func */ coremathconsts__sqrt2tooctafloat
  (/* out */ octafloat_t* /* param */ ADest);

void /* func */ coremathconsts__sqrt2tolongfloat
  (/* out */ longfloat_t* /* param */ ADest);
 
// ------------------

void /* func */ coremathconsts__invpitohalffloat
  (/* out */ halffloat_t* /* param */ ADest);

void /* func */ coremathconsts__invpitosinglefloat
  (/* out */ singlefloat_t* /* param */ ADest);

void /* func */ coremathconsts__invpitodoublefloat
  (/* out */ doublefloat_t* /* param */ ADest);

void /* func */ coremathconsts__invpitoquadfloat
  (/* out */ quadfloat_t* /* param */ ADest);

void /* func */ coremathconsts__invpitooctafloat
  (/* out */ octafloat_t* /* param */ ADest);

void /* func */ coremathconsts__invpitolongfloat
  (/* out */ longfloat_t* /* param */ ADest);
 
// ------------------

void /* func */ coremathconsts__invsqrt2tohalffloat
  (/* out */ halffloat_t* /* param */ ADest);

void /* func */ coremathconsts__invsqrt2tosinglefloat
  (/* out */ singlefloat_t* /* param */ ADest);

void /* func */ coremathconsts__invsqrt2todoublefloat
  (/* out */ doublefloat_t* /* param */ ADest);

void /* func */ coremathconsts__invsqrt2toquadfloat
  (/* out */ quadfloat_t* /* param */ ADest);

void /* func */ coremathconsts__invsqrt2tooctafloat
  (/* out */ octafloat_t* /* param */ ADest);

void /* func */ coremathconsts__invsqrt2tolongfloat
  (/* out */ longfloat_t* /* param */ ADest);
 
// ------------------

void /* func */ coremathconsts__sqrt3tohalffloat
  (/* out */ halffloat_t* /* param */ ADest);

void /* func */ coremathconsts__sqrt3tosinglefloat
  (/* out */ singlefloat_t* /* param */ ADest);

void /* func */ coremathconsts__sqrt3todoublefloat
  (/* out */ doublefloat_t* /* param */ ADest);

void /* func */ coremathconsts__sqrt3toquadfloat
  (/* out */ quadfloat_t* /* param */ ADest);

void /* func */ coremathconsts__sqrt3tooctafloat
  (/* out */ octafloat_t* /* param */ ADest);

void /* func */ coremathconsts__sqrt3tolongfloat
  (/* out */ longfloat_t* /* param */ ADest);

// ------------------

void /* func */ coremathconsts__invsqrt3tohalffloat
  (/* out */ halffloat_t* /* param */ ADest);

void /* func */ coremathconsts__invsqrt3tosinglefloat
  (/* out */ singlefloat_t* /* param */ ADest);

void /* func */ coremathconsts__invsqrt3todoublefloat
  (/* out */ doublefloat_t* /* param */ ADest);

void /* func */ coremathconsts__invsqrt3toquadfloat
  (/* out */ quadfloat_t* /* param */ ADest);

void /* func */ coremathconsts__invsqrt3tooctafloat
  (/* out */ octafloat_t* /* param */ ADest);

void /* func */ coremathconsts__invsqrt3tolongfloat
  (/* out */ longfloat_t* /* param */ ADest);

// ------------------

void /* func */ coremathconsts__egammatohalffloat
  (/* out */ halffloat_t* /* param */ ADest);

void /* func */ coremathconsts__egammatosinglefloat
  (/* out */ singlefloat_t* /* param */ ADest);

void /* func */ coremathconsts__egammatodoublefloat
  (/* out */ doublefloat_t* /* param */ ADest);

void /* func */ coremathconsts__egammatoquadfloat
  (/* out */ quadfloat_t* /* param */ ADest);

void /* func */ coremathconsts__egammatooctafloat
  (/* out */ octafloat_t* /* param */ ADest);

void /* func */ coremathconsts__egammatolongfloat
  (/* out */ longfloat_t* /* param */ ADest);

// ------------------

void /* func */ coremathconsts__phitohalffloat
  (/* out */ halffloat_t* /* param */ ADest);

void /* func */ coremathconsts__phitosinglefloat
  (/* out */ singlefloat_t* /* param */ ADest);

void /* func */ coremathconsts__phitodoublefloat
  (/* out */ doublefloat_t* /* param */ ADest);

void /* func */ coremathconsts__phitoquadfloat
  (/* out */ quadfloat_t* /* param */ ADest);

void /* func */ coremathconsts__phitooctafloat
  (/* out */ octafloat_t* /* param */ ADest);

void /* func */ coremathconsts__phitolongfloat
  (/* out */ longfloat_t* /* param */ ADest);

// ------------------




 // ...
 
// ------------------

/* override */ int /* func */ coremathconsts__setup
  ( noparams );

/* override */ int /* func */ coremathconsts__setoff
  ( noparams );

// ------------------

#endif // COREMATHCONSTS__H

// } // namespace coremathconsts