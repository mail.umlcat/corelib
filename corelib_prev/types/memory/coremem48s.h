/** Module: "coremem48s.h"
 ** Descr.: "..."
 **/
 
// namespace coremem48s {
 
// ------------------
 
#ifndef COREMEM48S__H
#define COREMEM48S__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coremem48s__compare
  (/* in */ const mem48_t /* param */ A,
   /* in */ const mem48_t /* param */ B);

bool /* func */ coremem48s__areequal
  (/* in */ const mem48_t /* param */ A,
   /* in */ const mem48_t /* param */ B);

bool /* func*/ coremem48s__isempty
  (/* in */ const mem48_t /* param */ ASource);

void /* func */ coremem48s__clear
  (/* inout */ mem48_t /* param */ ADest);

// ------------------

void /* func */ coremem48s__Encodebytesto48
  (/* out */ mem48_t*     /* param */ ADest,
   /* in */  const mem8_t /* param */ (*)ASource[8]);

void /* func */ coremem48s__Encodewordsto48
  (/* out */ mem48_t*      /* param */ ADest,
   /* in */  const mem16_t /* param */ (*)ASource[3]);

void /* func */ coremem48s__Decode48tobytes
  (/* in */  const mem48_t /* param */ ASource,
   /* out */ mem8_t*       /* param */ (*)ADest[8]);

void /* func */ coremem48s__Decode48towords
  (/* in */  const mem48_t /* param */ ASource,
   /* out */ mem16_t*      /* param */ (*)ADest[3]);

// ------------------

/* inline */ bool /* func */ coremem48s__ptrymem48tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const mem48_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coremem48s__ptrymem48tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const mem48_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

bool /* func */ coremem48s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coremem48s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coremem48s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coremem48s__setup
  ( noparams );

/* override */ void /* func */ coremem48s__setoff
  ( noparams );

// ------------------

#endif // COREMEM48S__H

// } // namespace coremem48s