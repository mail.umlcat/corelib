/** Module: "coremem128s.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coremem128s {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "coremem128s.h"
 
// ------------------

comparison /* func */ coremem128s__compare
  (/* in */ const mem128_t /* param */ A,
   /* in */ const mem128_t /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coremem128s__areequal
  (/* in */ const mem128_t /* param */ A,
   /* in */ const mem128_t /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func*/ coremem128s__isempty
  (/* in */ const mem128_t /* param */ ASource);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coremem128s__clear
  (/* inout */ mem128_t /* param */ ADest);
{
  coresystem__nothing();
} // func

 // ------------------

bool /* func */ coremem128s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coremem128s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coremem128s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coremem128s__setup
  ( noparams )
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coremem128s__setoff
  ( noparams )
{
  coresystem__nothing();
} // func

// ------------------


 // ...

// } // namespace coremem128s