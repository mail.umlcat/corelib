/** Module: "coremem8s.h"
 ** Descr.: "..."
 **/
 
// namespace coremem8s {
 
// ------------------
 
#ifndef COREMEM8S__H
#define COREMEM8S__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coremem8s__compare
  (/* in */ const mem8_t /* param */ A,
   /* in */ const mem8_t /* param */ B);

bool /* func */ coremem8s__areequal
  (/* in */ const mem8_t /* param */ A,
   /* in */ const mem8_t /* param */ B);

bool /* func*/ coremem8s__isempty
  (/* in */ const mem8_t /* param */ ASource);

void /* func */ coremem8s__clear
  (/* inout */ mem8_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ coremem8s__ptrymem8tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const mem8_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coremem8s__ptrymem8tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const mem8_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

/* inline */ void /* func */ coremem8s__pmem8tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const mem8_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ void /* func */ coremem8s__pmem8tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const mem8_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

 // ------------------

bool /* func */ coremem8s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coremem8s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coremem8s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

// ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coremem8__setup
  ( noparams );

/* override */ void /* func */ coremem8__setoff
  ( noparams );

// ------------------

#endif // COREMEM8S__H

// } // namespace coremem8s