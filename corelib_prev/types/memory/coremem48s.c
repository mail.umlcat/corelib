/** Module: "coreuint48s.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coreuint48s {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "coreuint48s.h"
 
// ------------------

comparison /* func */ coreuint48s__compare
  (/* in */ const uint48_t /* param */ A,
   /* in */ const uint48_t /* param */ B)
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreuint48s__areequal
  (/* in */ const uint48_t /* param */ A,
   /* in */ const uint48_t /* param */ B)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func*/ coreuint48s__isempty
  (/* in */ const uint48_t /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreuint48s__clear
  (/* inout */ uint48_t /* param */ ADest)
{
  coresystem__nothing();
} // func

// ------------------

void /* func */ coremem48s__Encodebytesto48
  (/* out */ mem48_t*     /* param */ ADest,
   /* in */  const mem8_t /* param */ (*)ASource[8])
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (AHiValue != NULL) &&
    (ALoValue != NULL);

  if (Result)
  {
    *AHiValue = ((ASource >> 16) & 0xffff0000);
    *ALoValue = (ASource & 0x0000ffff);
  } // if
} // func

void /* func */ coremem48s__Encodewordsto48
  (/* out */ mem48_t*      /* param */ ADest,
   /* in */  const mem16_t /* param */ (*)ASource[3])
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (AHiValue != NULL) &&
    (ALoValue != NULL);

  if (Result)
  {
    *AHiValue = ((ASource >> 16) & 0xffff0000);
    *ALoValue = (ASource & 0x0000ffff);
  } // if
} // func

void /* func */ coremem48s__Decode48tobytes
  (/* in */  const mem48_t /* param */ ASource,
   /* out */ mem8_t*       /* param */ (*)ADest[8])
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (AHiValue != NULL) &&
    (ALoValue != NULL);

  if (Result)
  {
    *AHiValue = ((ASource >> 16) & 0xffff0000);
    *ALoValue = (ASource & 0x0000ffff);
  } // if
} // func

void /* func */ coremem48s__Decode48towords
  (/* in */  const mem48_t /* param */ ASource,
   /* out */ mem16_t*      /* param */ (*)ADest[3])
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (AHiValue != NULL) &&
    (ALoValue != NULL);

  if (Result)
  {
    *AHiValue = ((ASource >> 16) & 0xffff0000);
    *ALoValue = (ASource & 0x0000ffff);
  } // if
} // func

// ------------------

bool /* func */ coreuint48s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreuint48s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreuint48s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coreuint48s__setup
  ( noparams )
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coreuint48s__setoff
  ( noparams )
{
  coresystem__nothing();
} // func

// ------------------


 // ...

// } // namespace coreuint48s