/** Module: "coremem64s.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coremem64s {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "coremem64s.h"
 
// ------------------

comparison /* func */ coremem64s__compare
  (/* in */ const mem64_t /* param */ A,
   /* in */ const mem64_t /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coremem64s__areequal
  (/* in */ const mem64_t /* param */ A,
   /* in */ const mem64_t /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func*/ coremem64s__isempty
  (/* in */ const mem64_t /* param */ ASource);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coremem64s__clear
  (/* inout */ mem64_t /* param */ ADest);
{
  coresystem__nothing();
} // func

// ------------------

void /* func */ coremem64s__Encode8to64
  (/* out */ mem64_t*     /* param */ ADest,
   /* in */  const mem8_t /* param */ (*)ASource[10])
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (AHiValue != NULL) &&
    (ALoValue != NULL);

  if (Result)
  {
    *AHiValue = ((ASource >> 16) & 0xffff0000);
    *ALoValue = (ASource & 0x0000ffff);
  } // if
} // func

void /* func */ coremem64s__Encode32to64
  (/* out */ mem64_t*      /* param */ ADest,
   /* in */  const mem32_t /* param */ AHiValue,
   /* in */  const mem32_t /* param */ ALoValue)
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (AHiValue != NULL) &&
    (ALoValue != NULL);

  if (Result)
  {
    *AHiValue = ((ASource >> 16) & 0xffff0000);
    *ALoValue = (ASource & 0x0000ffff);
  } // if
} // func

void /* func */ coremem64s__Decode64to8
  (/* in */  const mem64_t /* param */ ASource,
   /* out */ mem8_t*       /* param */ (*)ASource[10])
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (AHiValue != NULL) &&
    (ALoValue != NULL);

  if (Result)
  {
    *AHiValue = ((ASource >> 16) & 0xffff0000);
    *ALoValue = (ASource & 0x0000ffff);
  } // if
} // func

void /* func */ coremem64s__Decode64to32
  (/* in */  const mem64_t /* param */ ASource,
   /* out */ mem32_t*      /* param */ AHiValue,
   /* out */ mem32_t*      /* param */ ALoValue)
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (AHiValue != NULL) &&
    (ALoValue != NULL);

  if (Result)
  {
    *AHiValue = ((ASource >> 16) & 0xffff0000);
    *ALoValue = (ASource & 0x0000ffff);
  } // if
} // func


// ------------------

bool /* func */ coremem64s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coremem64s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coremem64s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coremem64s__setup
  ( noparams )
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coremem64s__setoff
  ( noparams )
{
  coresystem__nothing();
} // func

// ------------------


 // ...

// } // namespace coremem64s