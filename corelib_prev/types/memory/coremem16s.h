/** Module: "coremem16s.h"
 ** Descr.: "..."
 **/
 
// namespace coremem16s {
 
// ------------------
 
#ifndef COREMEM16S__H
#define COREMEM16S__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coremem16s__compare
  (/* in */ const mem16_t /* param */ A,
   /* in */ const mem16_t /* param */ B);

bool /* func */ coremem16s__areequal
  (/* in */ const mem16_t /* param */ A,
   /* in */ const mem16_t /* param */ B);

bool /* func*/ coremem16s__isempty
  (/* in */ const mem16_t /* param */ ASource);

void /* func */ coremem16s__clear
  (/* inout */ mem16_t /* param */ ADest);
  
// ------------------

void /* func */ coremem16s__Encode8to16
  (/* out */ mem16_t*     /* param */ ADest,
   /* in */  const mem8_t /* param */ AHiValue,
   /* in */  const mem8_t /* param */ ALoValue);
   
void /* func */ coremem16s__Decode16to8
  (/* in */  const mem16_t /* param */ ASource,
   /* out */ mem8_t*       /* param */ AHiValue,
   /* out */ mem8_t*       /* param */ ALoValue);



// ------------------

/* inline */ bool /* func */ coremem16s__ptrymem16tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const mem16_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coremem16s__ptrymem16tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const mem16_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

bool /* func */ coremem16s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coremem16s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coremem16s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);


 
// ------------------

/* override */ int /* func */ coremem16s__setup
  ( noparams );

/* override */ int /* func */ coremem16s__setoff
  ( noparams );

// ------------------

#endif // COREMEM16S__H

// } // namespace coremem16s