/** Module: "coremem128s.h"
 ** Descr.: "..."
 **/
 
// namespace coremem128s {
 
// ------------------
 
#ifndef COREMEM128S__H
#define COREMEM128S__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coremem128s__compare
  (/* in */ const mem128_t /* param */ A,
   /* in */ const mem128_t /* param */ B);

bool /* func */ coremem128s__areequal
  (/* in */ const mem128_t /* param */ A,
   /* in */ const mem128_t /* param */ B);

bool /* func*/ coremem128s__isempty
  (/* in */ const mem128_t /* param */ ASource);

void /* func */ coremem128s__clear
  (/* inout */ mem128_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ coremem128s__ptrymem128tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const mem128_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coremem128s__ptrymem128tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const mem128_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

bool /* func */ coremem128s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coremem128s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coremem128s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coremem128s__setup
  ( noparams );

/* override */ void /* func */ coremem128s__setoff
  ( noparams );

// ------------------

#endif // COREMEMS128S__H

// } // namespace coremem128s