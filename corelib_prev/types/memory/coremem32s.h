/** Module: "coremem32s.h"
 ** Descr.: "..."
 **/
 
// namespace coremem32s {
 
// ------------------
 
#ifndef COREMEM32S__H
#define COREMEM32S__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coremem32s__compare
  (/* in */ const mem32_t /* param */ A,
   /* in */ const mem32_t /* param */ B);

bool /* func */ coremem32s__areequal
  (/* in */ const mem32_t /* param */ A,
   /* in */ const mem32_t /* param */ B);

bool /* func*/ coremem32s__isempty
  (/* in */ const mem32_t /* param */ ASource);

void /* func */ coremem32s__clear
  (/* inout */ mem32_t* /* param */ ADest);

// ------------------

void /* func */ coremem32s__Encode8to32
  (/* out */ mem32_t*     /* param */ ADest,
   /* in */  const mem8_t /* param */ A,
   /* in */  const mem8_t /* param */ B,
   /* in */  const mem8_t /* param */ C,
   /* in */  const mem8_t /* param */ D);

void /* func */ coremem32s__Encode16to32
  (/* out */ mem32_t*      /* param */ ADest,
   /* in */  const mem16_t /* param */ AHiValue,
   /* in */  const mem16_t /* param */ ALoValue);
   
void /* func */ coremem32s__Decode32to16
  (/* in */  const mem32_t /* param */ ASource,
   /* out */ mem16_t*      /* param */ AHiValue,
   /* out */ mem16_t*      /* param */ ALoValue);

void /* func */ coremem32s__Decode32to8
  (/* in */  const mem32_t /* param */ ASource,
   /* out */ mem16_t*      /* param */ A,
   /* out */ mem16_t*      /* param */ B,
   /* out */ mem16_t*      /* param */ C,
   /* out */ mem16_t*      /* param */ D);


// ------------------

/* inline */ bool /* func */ coremem32s__ptrymem32tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const mem32_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coremem32s__ptrymem32tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const mem32_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

bool /* func */ coremem32s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coremem32s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coremem32s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

pointer* /* func */ coremem32s__mem32ptrcopy
  (/* in */ const mem32_t /* param */ AValue);

 
// ------------------

/* override */ int /* func */ coremem32s__setup
  ( noparams );

/* override */ int /* func */ coremem32s__setoff
  ( noparams );

// ------------------

#endif // COREMEM32S__H

// } // namespace coremem32s