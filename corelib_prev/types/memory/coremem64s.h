/** Module: "coremem64s.h"
 ** Descr.: "..."
 **/
 
// namespace coremem64s {
 
// ------------------
 
#ifndef COREMEM64S__H
#define COREMEM64S__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coremem64s__compare
  (/* in */ const mem64_t /* param */ A,
   /* in */ const mem64_t /* param */ B);

bool /* func */ coremem64s__areequal
  (/* in */ const mem64_t /* param */ A,
   /* in */ const mem64_t /* param */ B);

bool /* func*/ coremem64s__isempty
  (/* in */ const mem64_t /* param */ ASource);

void /* func */ coremem64s__clear
  (/* inout */ mem64_t /* param */ ADest);

// ------------------

void /* func */ coremem64s__Encode8to64
  (/* out */ mem64_t*     /* param */ ADest,
   /* in */  const mem8_t /* param */ (*)ASource[10]);

void /* func */ coremem64s__Encode32to64
  (/* out */ mem64_t*      /* param */ ADest,
   /* in */  const mem32_t /* param */ AHiValue,
   /* in */  const mem32_t /* param */ ALoValue);

void /* func */ coremem64s__Decode64to8
  (/* in */  const mem64_t /* param */ ASource,
   /* out */ mem8_t*       /* param */ (*)ASource[10]);

void /* func */ coremem64s__Decode64to32
  (/* in */  const mem64_t /* param */ ASource,
   /* out */ mem32_t*      /* param */ AHiValue,
   /* out */ mem32_t*      /* param */ ALoValue);


// ------------------

/* inline */ bool /* func */ coremem64s__ptrymem64tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const mem64_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coremem64s__ptrymem64tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const mem64_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

bool /* func */ coremem64s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coremem64s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coremem64s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coremem64s__setup
  ( noparams );

/* override */ void /* func */ coremem64s__setoff
  ( noparams );

// ------------------

#endif // COREMEM64S__H

// } // namespace coremem64s