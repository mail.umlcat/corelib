/** Module: "coremem80s.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coremem80s {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "coremem80s.h"
 
// ------------------

comparison /* func */ coremem80s__compare
  (/* in */ const mem80_t /* param */ A,
   /* in */ const mem80_t /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coremem80s__areequal
  (/* in */ const mem80_t /* param */ A,
   /* in */ const mem80_t /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func*/ coremem80s__isempty
  (/* in */ const mem80_t /* param */ ASource);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coremem80s__clear
  (/* inout */ mem80_t /* param */ ADest);
{
  coresystem__nothing();
} // func

 // ------------------

bool /* func */ coremem80s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coremem80s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coremem80s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coremem80s__setup
  ( noparams )
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coremem80s__setoff
  ( noparams )
{
  coresystem__nothing();
} // func

// ------------------


 // ...

// } // namespace coremem80s