/** Module: "coremem16s.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coremem16s {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "coremem16s.h"
 
// ------------------

comparison /* func */ coremem16s__compare
  (/* in */ const mem16_t /* param */ A,
   /* in */ const mem16_t /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
  if (ADest != NULL)
  {
    coresystem__nothing();
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coremem16s__areequal
  (/* in */ const mem16_t /* param */ A,
   /* in */ const mem16_t /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
  if (ADest != NULL)
  {
    coresystem__nothing();
  } // if
     
  // ---
  return Result;
} // func

bool /* func*/ coremem16s__isempty
  (/* in */ const mem16_t /* param */ ASource);
{
  bool /* var */ Result = false;
  // ---
{
  if (ADest != NULL)
  {
    coresystem__nothing();
  } // if
} // func
     
     
  // ---
  return Result;
} // func

void /* func */ coremem16s__clear
  (/* inout */ mem16_t /* param */ ADest);
{
  if (ADest != NULL)
  {
    coresystem__nothing();
  } // if
} // func

// ------------------

void /* func */ coremem16s__Encode8to16
  (/* out */ mem16_t*     /* param */ ADest,
   /* in */  const mem8_t /* param */ AHiValue,
   /* in */  const mem8_t /* param */ ALoValue)
{
  if (ADest != NULL)
  {
    *ADest =
      (((AHiValue & 0xff) << 8) | (ALoValue & 0xff));
  } // if
} // func
  
void /* func */ coremem16s__Decode16to8
  (/* in */  const mem16_t /* param */ ASource,
   /* out */ mem8_t*       /* param */ AHiValue,
   /* out */ mem8_t*       /* param */ ALoValue)
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (AHiValue != NULL) &&
    (ALoValue != NULL);

  if (Result)
  {
    *AHiValue = ((ASource >> 8) & 0xff00);
    *ALoValue = (ASource & 0x00ff);
  } // if
} // func



// ------------------

/* inline */ bool /* func */ coremem16s__ptrymem16tomaxhdwint
// (   out    maxhdwint_t*       param    ADest,
//     in     const mem16_p     param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
  if (ADest != NULL)
  {
    coresystem__nothing();
  } // if
     
  // ---
  return Result;
} // func

/* inline */ bool /* func */ coremem16s__ptrymem16tosfwmaxint
// (   out    maxsfwmaxint*      param    ADest,
//     in     const mem16_p     param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
  
  if (ADest != NULL)
  {
    coresystem__nothing();
  } // if
     
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coremem16s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  if (ADest != NULL)
  {
    coresystem__nothing();
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coremem16s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  if (ADest != NULL)
  {
    coresystem__nothing();
  } // if
     
  // ---
  return Result;
} // func

void /* func */ coremem16s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  if (ADest != NULL)
  {
    coresystem__nothing();
  } // if
} // func

// ------------------

 // ...

/* override */ void /* func */ coremem16s__setup
  ( noparams )
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coremem16s__setoff
  ( noparams )
{
  coresystem__nothing();
} // func

// ------------------

// } // namespace coremem16s