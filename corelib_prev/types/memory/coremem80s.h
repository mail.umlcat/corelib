/** Module: "coremem80s.h"
 ** Descr.: "..."
 **/
 
// namespace coremem80s {
 
// ------------------
 
#ifndef COREMEM80S__H
#define COREMEM80S__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coremem80s__compare
  (/* in */ const mem80_t /* param */ A,
   /* in */ const mem80_t /* param */ B);

bool /* func */ coremem80s__areequal
  (/* in */ const mem80_t /* param */ A,
   /* in */ const mem80_t /* param */ B);

bool /* func*/ coremem80s__isempty
  (/* in */ const mem80_t /* param */ ASource);

void /* func */ coremem80s__clear
  (/* inout */ mem80_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ coremem80s__ptrymem80tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const mem80_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coremem80s__ptrymem80tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const mem80_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

bool /* func */ coremem80s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coremem80s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coremem80s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coremem80s__setup
  ( noparams );

/* override */ void /* func */ coremem80s__setoff
  ( noparams );

// ------------------

#endif // COREMEM80S__H

// } // namespace coremem80s