/** Module: "coremem32s.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coremem32s {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "coremem32s.h"
 
// ------------------

comparison /* func */ coremem32s__compare
  (/* in */ const mem32_t /* param */ A,
   /* in */ const mem32_t /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coremem32s__areequal
  (/* in */ const mem32_t /* param */ A,
   /* in */ const mem32_t /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
  Result = (A == B);
     
  // ---
  return Result;
} // func

bool /* func*/ coremem32s__isempty
  (/* in */ const mem32_t /* param */ ASource);
{
  bool /* var */ Result = false;
  // ---
  
  Result = (ASource == 0);
     
  // ---
  return Result;
} // func

void /* func */ coremem32s__clear
  (/* inout */ mem32_t* /* param */ ADest);
{
  if (ADest != NULL)
  {
    ASource = 0;
  } // if
} // func

// ------------------

void /* func */ coremem32s__Encode8to32
  (/* out */ mem32_t*      /* param */ ADest,
   /* in */  const mem8_t /* param */ A,
   /* in */  const mem8_t /* param */ B,
   /* in */  const mem8_t /* param */ C,
   /* in */  const mem8_t /* param */ D)
{
  if (ADest != NULL)
  {
    *ADest = ((AHiValue << 16) | (ALoValue));
  } // if
} // func

void /* func */ coremem32s__Encode16to32
  (/* out */ mem32_t*      /* param */ ADest,
   /* in */  const mem16_t /* param */ AHiValue,
   /* in */  const mem16_t /* param */ ALoValue)
{
  if (ADest != NULL)
  {
    *ADest = ((AHiValue << 16) | (ALoValue));
  } // if
} // func

void /* func */ coremem32s__Decode32to16
  (/* in */  const mem32_t /* param */ ASource,
   /* out */ mem16_t*      /* param */ AHiValue,
   /* out */ mem16_t*      /* param */ ALoValue)
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (AHiValue != NULL) &&
    (ALoValue != NULL);

  if (Result)
  {
    *AHiValue = ((ASource >> 16) & 0xffff0000);
    *ALoValue = (ASource & 0x0000ffff);
  } // if
} // func

void /* func */ coremem32s__Decode32to8
  (/* in */  const mem32_t /* param */ ASource,
   /* out */ mem16_t*      /* param */ A,
   /* out */ mem16_t*      /* param */ B,
   /* out */ mem16_t*      /* param */ C,
   /* out */ mem16_t*      /* param */ D)
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (AHiValue != NULL) &&
    (ALoValue != NULL);

  if (Result)
  {
    *AHiValue = ((ASource >> 16) & 0xffff0000);
    *ALoValue = (ASource & 0x0000ffff);
  } // if
} // func

// ------------------

/* inline */ bool /* func */ coremem32s__ptrymem32tomaxhdwint
// (   out    maxhdwint_t*       param    ADest,
//     in     const mem32_p     param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
  CanContinue =
    (AHiValue != NULL) &&
    (ALoValue != NULL);

  if (Result)
  {

  } // if
     
  // ---
  return Result;
} // func

/* inline */ bool /* func */ coremem32s__ptrymem32tosfwmaxint
// (   out    maxsfwmaxint*      param    ADest,
//     in     const mem32_p     param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
  CanContinue =
    (AHiValue != NULL) &&
    (ALoValue != NULL);

  if (Result)
  {

  } // if
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coremem32s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  CanContinue =
    (AHiValue != NULL) &&
    (ALoValue != NULL);

  if (Result)
  {

  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coremem32s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  CanContinue =
    (AHiValue != NULL) &&
    (ALoValue != NULL);

  if (Result)
  {

  } // if
     
  // ---
  return Result;
} // func

void /* func */ coremem32s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

pointer* /* func */ coremem32s__mem32ptrcopy
  (/* in */ const mem32_t /* param */ AValue)
{
  pointer* /* var */ Result = NULL;
  // ---
     
  Result =
    coresystem__malloc(sizeof(mem32_t));
  coresystem__safecopy(Result, &AValue);
  
  // ---
  return Result;
} // func

// ------------------

 // ...

/* override */ void /* func */ coremem32s__setup
  ( noparams )
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coremem32s__setoff
  ( noparams )
{
  coresystem__nothing();
} // func

// ------------------

// } // namespace coremem32s