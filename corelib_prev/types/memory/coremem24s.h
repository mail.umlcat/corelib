/** Module: "coremem24s.h"
 ** Descr.: "..."
 **/
 
// namespace coremem24s {
 
// ------------------
 
#ifndef COREMEM24S__H
#define COREMEM24S__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coremem24s__compare
  (/* in */ const mem24_t /* param */ A,
   /* in */ const mem24_t /* param */ B);

bool /* func */ coremem24s__areequal
  (/* in */ const mem24_t /* param */ A,
   /* in */ const mem24_t /* param */ B);

bool /* func*/ coremem24s__isempty
  (/* in */ const mem24_t /* param */ ASource);

void /* func */ coremem24s__clear
  (/* inout */ mem24_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ coremem24s__ptrymem24tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const mem24_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coremem24s__ptrymem24tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const mem24_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

/* inline */ void /* func */ coremem24s__pmem24tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const mem24_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ void /* func */ coremem24s__pmem24tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const mem24_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

 // ------------------

bool /* func */ coremem24s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coremem24s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coremem24s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

// ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coremem24__setup
  ( noparams );

/* override */ void /* func */ coremem24__setoff
  ( noparams );

// ------------------

#endif // COREMEM24S__H

// } // namespace coremem24s