/** Module: "coremem256s.h"
 ** Descr.: "..."
 **/
 
// namespace coremem256s {
 
// ------------------
 
#ifndef COREMEM256S__H
#define COREMEM256S__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coremem256s__compare
  (/* in */ const mem256_t /* param */ A,
   /* in */ const mem256_t /* param */ B);

bool /* func */ coremem256s__areequal
  (/* in */ const mem256_t /* param */ A,
   /* in */ const mem256_t /* param */ B);

bool /* func*/ coremem256s__isempty
  (/* in */ const mem256_t /* param */ ASource);

void /* func */ coremem256s__clear
  (/* inout */ mem256_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ coremem256s__ptrymem256tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const mem256_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coremem256s__ptrymem256tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const mem256_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

bool /* func */ coremem256s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coremem256s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coremem256s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coremem256s__setup
  ( noparams );

/* override */ void /* func */ coremem256s__setoff
  ( noparams );

// ------------------

#endif // COREMEMS256S__H

// } // namespace coremem256s