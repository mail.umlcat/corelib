/** Module: "corebooleans.c"
 ** Descr.: "predefined library"
 **/
 
// namespace corebooleans {
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreintegers.h"

// ------------------
 
#include "corebooleans.h"
 
// ------------------

bool /* func */ corebooleans__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ corebooleans__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ corebooleans__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

pointer* /* func */ corebooleans__boolptrcopy
  (/* in */ const bool /* param */ AValue)
{
  pointer* /* var */ Result = NULL;
  // ---
     
  Result =
    coresystem__malloc(sizeof(bool));
  coresystem__safecopy(Result, &AValue);
  
  // ---
  return Result;
} // func

// ------------------

/* override */ const ansinullstring* /* func */ coresystem__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "corebooleans";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ corebooleans__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corebooleans__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------


// } // namespace corebooleans