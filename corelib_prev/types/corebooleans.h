/** Module: "corebooleans.h"
 ** Descr.: "..."
 **/
 
// namespace corebooleans {
 
// ------------------
 
#ifndef COREBOOLEANS__H
#define COREBOOLEANS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreintegers.h"

// ------------------

// used for interoperability
typedef
  mem8_t     /* as */ bool8_t;
typedef
  bool8_t*   /* as */ bool8_p;

typedef
  mem16_t    /* as */ bool16_t;
typedef
  bool16_t*  /* as */ bool16_p;

typedef
  mem32_t    /* as */ bool32_t;
typedef
  bool32_t*  /* as */ bool32_p;

typedef
  mem64_t    /* as */ bool64_t;
typedef
  bool64_t*  /* as */ bool64_p;

typedef
  mem32_t    /* as */ bool128_t[4];
typedef
  bool128_t* /* as */ bool128_p;

// ------------------

bool /* func */ corebooleans__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ corebooleans__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ corebooleans__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

pointer* /* func */ corebooleans__boolptrcopy
  (/* in */ const bool /* param */ AValue);

// ------------------

/* override */ int /* func */ corebooleans__setup
  ( noparams );

/* override */ int /* func */ corebooleans__setoff
  ( noparams );
 
// ------------------
 
#endif // COREBOOLEANS__H

// } // namespace corebooleans