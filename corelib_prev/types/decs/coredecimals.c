/** Module: "coredecimals.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coredecimals {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coredec32s.h"
#include "coredec64s.h"
#include "coredec128s.h"

// ------------------
 
#include "coredecimals.h"
 
// ------------------

bool /* func */ coretxtdecs__trydec2nstr
  (/* in */  const dec<*>  /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

bool /* func */ coretxtdecs__trydec2nstrcount
  (/* in */  const dec<*>   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */ size_t /* param */ ADestSize);

void /* func */ coretxtdecs__dec2nstr
  (/* in */  const dec<*>   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest);

void /* func */ coretxtdecs__dec2nstrcount
  (/* in */  const dec<*>   /* param */ ASource,
   /* out */ /* restricted */ ansinullstring* /* param */ ADest,
   /* in */  size_t /* param */ ADestSize);
 
// ------------------


 // ...

// ------------------

/* override */ int /* func */ coredecimals__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coredecimals__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------


// } // namespace coredecimals