/** Module: "coredec32s.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coredec32s {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coredec32s.h"
 
// ------------------

comparison /* func */ coredec32s__compare
  (/* in */ const dec32_t /* param */ A,
   /* in */ const dec32_t /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coredec32s__areequal
  (/* in */ const dec32_t /* param */ A,
   /* in */ const dec32_t /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func*/ coredec32s__isempty
  (/* in */ const dec32_t /* param */ ASource);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coredec32s__clear
  (/* inout */ dec32_t /* param */ ADest);
{
  coresystem__nothing();
} // func

// ------------------

/* inline */ bool /* func */ coredec32s__ptrydec32tohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const dec32_p      param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

/* inline */ bool /* func */ coredec32s__ptrydec32tosfwmaxfloat
// (   out    maxsfwmaxfloat*      param    ADest,
//     in     const dec32_p      param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

 

bool /* func */ coredec32s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coredec32s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coredec32s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coredec32s__setup
  ( noparams )
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coredec32s__setoff
  ( noparams )
{
  coresystem__nothing();
} // func

// ------------------


 // ...

// } // namespace coredec32s