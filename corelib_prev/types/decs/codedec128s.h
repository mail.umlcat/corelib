/** Module: "coredec128s.h"
 ** Descr.: "..."
 **/
 
// namespace coredec128s {
 
// ------------------
 
#ifndef COREDEC128S__H
#define COREDEC128S__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coredec128s__compare
  (/* in */ const dec128_t /* param */ A,
   /* in */ const dec128_t /* param */ B);

bool /* func */ coredec128s__areequal
  (/* in */ const dec128_t /* param */ A,
   /* in */ const dec128_t /* param */ B);

bool /* func*/ coredec128s__isempty
  (/* in */ const dec128_t /* param */ ASource);

void /* func */ coredec128s__clear
  (/* inout */ dec128_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ coredec128s__ptrydec128tohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const dec128_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coredec128s__ptrydec128tosfwmaxfloat
// (   out    sfwmaxfloat_t*       param    ADest,
//     in     const dec128_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

/* inline */ void /* func */ coredec128s__pdec128tohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const dec128_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ void /* func */ coredec128s__pdec128tosfwmaxfloat
// (   out    sfwmaxfloat_t*       param    ADest,
//     in     const dec128_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

 // ------------------

bool /* func */ coredec128s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coredec128s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coredec128s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coredec128s__setup
  ( noparams );

/* override */ void /* func */ coredec128s__setoff
  ( noparams );

// ------------------

#endif // COREDEC128S__H

// } // namespace coredec128s