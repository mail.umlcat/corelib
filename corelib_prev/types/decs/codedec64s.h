/** Module: "coredec64s.h"
 ** Descr.: "..."
 **/
 
// namespace coredec64s {
 
// ------------------
 
#ifndef COREDEC64S__H
#define COREDEC64S__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coredec64s__compare
  (/* in */ const dec64_t /* param */ A,
   /* in */ const dec64_t /* param */ B);

bool /* func */ coredec64s__areequal
  (/* in */ const dec64_t /* param */ A,
   /* in */ const dec64_t /* param */ B);

bool /* func*/ coredec64s__isempty
  (/* in */ const dec64_t /* param */ ASource);

void /* func */ coredec64s__clear
  (/* inout */ dec64_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ coredec64s__ptrydec64tohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const dec64_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coredec64s__ptrydec64tosfwmaxfloat
// (   out    sfwmaxfloat_t*       param    ADest,
//     in     const dec64_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

/* inline */ void /* func */ coredec64s__pdec64tohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const dec64_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ void /* func */ coredec64s__pdec64tosfwmaxfloat
// (   out    sfwmaxfloat_t*       param    ADest,
//     in     const dec64_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

 // ------------------

bool /* func */ coredec64s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coredec64s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coredec64s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coredec64s__setup
  ( noparams );

/* override */ void /* func */ coredec64s__setoff
  ( noparams );

// ------------------

#endif // COREDEC64S__H

// } // namespace coredec64s