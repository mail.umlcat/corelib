/** Module: "coredecimals.h"
 ** Descr.: "..."
 **/
 
// namespace coredecimals {
 
// ------------------
 
#ifndef COREDECIMALS__H
#define COREDECIMALS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coredec32s.h"
#include "coredec64s.h"
#include "coredec128s.h"

// ------------------

enum decimals
{
  decimals__decimal32_t,
  decimals__decimal64_t,
  decimals__decimal128_t,
} ;

typedef
  decimals  /* as */ coredecimals__decimals;

// ------------------

typedef
  mem32_t   /* as */ decimal32_t;
typedef
  mem32_t*  /* as */ decimal32_p;

typedef
  mem64_t   /* as */ decimal64_t;
typedef
  mem64_t*  /* as */ decimal64_p;

typedef
  mem128_t  /* as */ decimal128_t;
typedef
  mem128_t* /* as */ decimal128_p;

// ------------------

// include files:
// {
typedef
  decimal128_t  /* as */ maxdecimal_t;
typedef
  decimal128_t* /* as */ maxdecimal_p;
// }

// ------------------

/* inline */ bool /* func */ coredecimals__trydectohdwmaxdec
  (/* in */  decimals /* param */ ADecType,
   /* out */ pointer* /* param */ ADestPtr,
   /* in */  pointer* /* param */ ASourcePtr);

/* inline */ bool /* func */ coredecimals__trydectosfwmaxdec
  (/* in */  decimals /* param */ ADecType,
   /* out */ pointer* /* param */ ADestPtr,
   /* in */  pointer* /* param */ ASourcePtr);

// ------------------

/* inline */ void  /* func */ coredecimals__dectohdwmaxdec
  (/* in */  decimals /* param */ ADecType,
   /* out */ pointer* /* param */ ADestPtr,
   /* in */  pointer* /* param */ ASourcePtr);

/* inline */ void /* func */ coredecimals__dectosfwmaxdec
  (/* in */  decimals /* param */ ADecType,
   /* out */ pointer* /* param */ ADestPtr,
   /* in */  pointer* /* param */ ASourcePtr);
   
// ------------------

 // ...
 
// ------------------

/* override */ int /* func */ coredecimals__setup
  ( noparams );

/* override */ int /* func */ coredecimals__setoff
  ( noparams );

// ------------------

#endif // COREDECIMALS__H

// } // namespace coredecimals