/** Module: "coredec32s.h"
 ** Descr.: "..."
 **/
 
// namespace coredec32s {
 
// ------------------
 
#ifndef COREDEC32S__H
#define COREDEC32S__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coredec32s__compare
  (/* in */ const dec32_t /* param */ A,
   /* in */ const dec32_t /* param */ B);

bool /* func */ coredec32s__areequal
  (/* in */ const dec32_t /* param */ A,
   /* in */ const dec32_t /* param */ B);

bool /* func*/ coredec32s__isempty
  (/* in */ const dec32_t /* param */ ASource);

void /* func */ coredec32s__clear
  (/* inout */ dec32_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ coredec32s__ptrydec32tohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const dec32_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coredec32s__ptrydec32tosfwmaxfloat
// (   out    sfwmaxfloat_t*       param    ADest,
//     in     const dec32_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

/* inline */ void /* func */ coredec32s__pdec32tohdwmaxfloat
// (   out    hdwmaxfloat_t*       param    ADest,
//     in     const dec32_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ void /* func */ coredec32s__pdec32tosfwmaxfloat
// (   out    sfwmaxfloat_t*       param    ADest,
//     in     const dec32_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

 // ------------------

bool /* func */ coredec32s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coredec32s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coredec32s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coredec32s__setup
  ( noparams );

/* override */ void /* func */ coredec32s__setoff
  ( noparams );

// ------------------

#endif // COREDEC32S__H

// } // namespace coredec32s