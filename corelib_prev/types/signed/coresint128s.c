/** Module: "coresint128s.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coresint128s {
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "coresint128s.h"
 
// ------------------

comparison /* func */ coresint128s__compare
  (/* in */ const sint128_t /* param */ A,
   /* in */ const sint128_t /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coresint128s__areequal
  (/* in */ const sint128_t /* param */ A,
   /* in */ const sint128_t /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func*/ coresint128s__isempty
  (/* in */ const sint128_t /* param */ ASource);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coresint128s__clear
  (/* inout */ sint128_t /* param */ ADest);
{
  coresystem__nothing();
} // func

 // ------------------

bool /* func */ coresint128s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coresint128s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coresint128s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coresint128s__setup
  ( noparams )
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coresint128s__setoff
  ( noparams )
{
  coresystem__nothing();
} // func

// ------------------


 // ...

// } // namespace coresint128s