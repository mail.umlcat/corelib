/** Module: "coresint80s.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coresint80s {
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "coresint80s.h"
 
// ------------------

comparison /* func */ coresint80s__compare
  (/* in */ const sint80_t /* param */ A,
   /* in */ const sint80_t /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coresint80s__areequal
  (/* in */ const sint80_t /* param */ A,
   /* in */ const sint80_t /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func*/ coresint80s__isempty
  (/* in */ const sint80_t /* param */ ASource);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coresint80s__clear
  (/* inout */ sint80_t /* param */ ADest);
{
  coresystem__nothing();
} // func

 // ------------------

bool /* func */ coresint80s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coresint80s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coresint80s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coresint80s__setup
  ( noparams )
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coresint80s__setoff
  ( noparams )
{
  coresystem__nothing();
} // func

// ------------------


 // ...

// } // namespace coresint80s