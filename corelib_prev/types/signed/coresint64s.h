/** Module: "coresint64s.h"
 ** Descr.: "..."
 **/
 
// namespace coresint64s {
 
// ------------------
 
#ifndef CORESINT64S__H
#define CORESINT64S__H
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------
 
#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coresint64s__compare
  (/* in */ const sint64_t /* param */ A,
   /* in */ const sint64_t /* param */ B);

bool /* func */ coresint64s__areequal
  (/* in */ const sint64_t /* param */ A,
   /* in */ const sint64_t /* param */ B);

bool /* func*/ coresint64s__isempty
  (/* in */ const sint64_t /* param */ ASource);

void /* func */ coresint64s__clear
  (/* inout */ sint64_t /* param */ ADest);

// ------------------

pointer* /* func*/ coreuint64s__uint64toptrcopy
  (/* in */ const uint64_t /* param */ ASource);

bool /* func */ coreuint64s__areequalbyptr
  (pointer* /* param */ A, 
   pointer* /* param */ B);

// ------------------

/* inline */ bool /* func */ coresint64s__ptrysint64tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const sint64_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coresint64s__ptrysint64tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const sint64_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

bool /* func */ coresint64s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coresint64s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coresint64s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coresint64s__setup
  ( noparams );

/* override */ void /* func */ coresint64s__setoff
  ( noparams );

// ------------------

#endif // CORESINT64S__H

// } // namespace coresint64s