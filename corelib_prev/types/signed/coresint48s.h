/** Module: "coresint48s.h"
 ** Descr.: "..."
 **/
 
// namespace coresint48s {
 
// ------------------
 
#ifndef CORESINT48S__H
#define CORESINT48S__H
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coresint48s__compare
  (/* in */ const sint48_t /* param */ A,
   /* in */ const sint48_t /* param */ B);

bool /* func */ coresint48s__areequal
  (/* in */ const sint48_t /* param */ A,
   /* in */ const sint48_t /* param */ B);

bool /* func*/ coresint48s__isempty
  (/* in */ const sint48_t /* param */ ASource);

void /* func */ coresint48s__clear
  (/* inout */ sint48_t /* param */ ADest);

// ------------------

pointer* /* func*/ coresint48s__sint48toptrcopy
  (/* in */ const sint48_t /* param */ ASource);

bool /* func */ coresint48s__areequalbyptr
  (pointer* /* param */ A, 
   pointer* /* param */ B);

// ------------------

/* inline */ bool /* func */ coresint48s__trysint48tohdwmaxintbyptr
// (   out    hdwmaxint_t*       param    ADest,
//     in     const sint48_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coresint48s__trysint48tosfwmaxintbyptr
// (   out    sfwmaxint_t*       param    ADest,
//     in     const sint48_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

/* inline */ void /* func */ coreuint48s__uint8tohdwmaxintbyptr
// (   out    hdwmaxint_t*       param    ADest,
//     in     const uint8_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ void /* func */ coreuint48s__uint8tosfwmaxintbyptr
// (   out    sfwmaxint_t*       param    ADest,
//     in     const uint8_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

bool /* func */ coresint48s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coresint48s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coresint48s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coresint48s__setup
  ( noparams );

/* override */ void /* func */ coresint48s__setoff
  ( noparams );

// ------------------

#endif // CORESINT48S__H

// } // namespace coresint48s