/** Module: "coresint8s.h"
 ** Descr.: "..."
 **/
 
// namespace coresint8s {
 
// ------------------
 
#ifndef CORESINT8S__H
#define CORESINT8S__H
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------
 
#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coresint8s__compare
  (/* in */ const sint8_t /* param */ A,
   /* in */ const sint8_t /* param */ B);

bool /* func */ coresint8s__areequal
  (/* in */ const sint8_t /* param */ A,
   /* in */ const sint8_t /* param */ B);

bool /* func*/ coresint8s__isempty
  (/* in */ const sint8_t /* param */ ASource);

void /* func */ coresint8s__clear
  (/* inout */ sint8_t /* param */ ADest);

// ------------------

pointer* /* func*/ coresint8s__sint8toptrcopy
  (/* in */ const sint8_t /* param */ ASource);
  
bool /* func */ coresint8s__areequalbyptr
  (pointer* /* param */ A, 
   pointer* /* param */ B);

// ------------------

/* inline */ bool /* func */ coresint8s__ptrysint8tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const sint8_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coresint8s__ptrysint8tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const sint8_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

/* inline */ void /* func */ coresint8s__psint8tomaxregint
// (   out    maxregint_t*       param    ADest,
//     in     const sint8_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ void /* func */ coresint8s__psint8tomaxvalint
// (   out    maxvalint_t*       param    ADest,
//     in     const sint8_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

 // ------------------

bool /* func */ coresint8s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coresint8s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coresint8s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coresint8__setup
  ( noparams );

/* override */ void /* func */ coresint8__setoff
  ( noparams );

// ------------------

#endif // CORESINT8S__H

// } // namespace coresint8s