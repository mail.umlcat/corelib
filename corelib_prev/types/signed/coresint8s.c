/** Module: "coresint8s.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coresint8s {
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------
 
#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "coresint8s.h"
 
// ------------------

comparison /* func */ coresint8s__compare
  (/* in */ const sint8_t /* param */ A,
   /* in */ const sint8_t /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coresint8s__areequal
  (/* in */ const sint8_t /* param */ A,
   /* in */ const sint8_t /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func*/ coresint8s__isempty
  (/* in */ const sint8_t /* param */ ASource);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coresint8s__clear
  (/* inout */ sint8_t /* param */ ADest);
{
  coresystem__nothing();
} // func

// ------------------

pointer* /* func*/ coresint8s__sint8toptrcopy
  (/* in */ const sint8_t /* param */ ASource)
{
  pointer* /* var */ Result = 0;
  // ---
  
  Result =
    corememory__duplicatecopy
      (&ASource, sizeof(sint8_t);
     
  // ---
  return Result;
} // func
  
bool /* func */ coresint8s__areequalbyptr
  (pointer* /* param */ A, 
   pointer* /* param */ B)
{
  bool /* var */ Result = FALSE;
  // ---
  
  Result =
    (A == B);
     
  // ---
  return Result;
} // func


// ------------------

/* inline */ bool /* func */ coresint8s__ptrysint8tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const sint8_p      param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

/* inline */ bool /* func */ coresint8s__ptrysint8tosfwmaxint
// (   out    maxsfwmaxint*      param    ADest,
//     in     const sint8_p      param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

 

bool /* func */ coresint8s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coresint8s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coresint8s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coresint8__setup
  ( noparams )
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coresint8__setoff
  ( noparams )
{
  coresystem__nothing();
} // func

// ------------------


 // ...

// } // namespace coresint8s