/** Module: "coresint48s.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coresint48s {
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "coresint48s.h"
 
// ------------------

comparison /* func */ coresint48s__compare
  (/* in */ const sint48_t /* param */ A,
   /* in */ const sint48_t /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coresint48s__areequal
  (/* in */ const sint48_t /* param */ A,
   /* in */ const sint48_t /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func*/ coresint48s__isempty
  (/* in */ const sint48_t /* param */ ASource);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coresint48s__clear
  (/* inout */ sint48_t /* param */ ADest);
{
  coresystem__nothing();
} // func

// ------------------

pointer* /* func*/ coreuint48s__uint48toptrcopy
  (/* in */ const uint48_t /* param */ ASource)
{
  pointer* /* var */ Result = 0;
  // ---
  
  Result =
    corememory__duplicatecopy
      (&ASource, sizeof(uint48_t);
     
  // ---
  return Result;
} // func

bool /* func */ coreuint48s__areequalbyptr
  (pointer* /* param */ A, 
   pointer* /* param */ B)
{
  bool /* var */ Result = FALSE;
  // ---
  
  Result =
    (A == B);
     
  // ---
  return Result;
} // func

// ------------------

/* inline */ bool /* func */ coresint48s__trysint48tomaxhdwintbyptr
// (   out    maxhdwint_t*       param    ADest,
//     in     const sint48_p      param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

/* inline */ bool /* func */ coresint48s__trysint48tosfwmaxintbyptr
// (   out    maxsfwmaxint*      param    ADest,
//     in     const sint48_p      param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* inline */ void /* func */ coresint48s__sint48tohdwmaxintbyptr
// (   out    hdwmaxint_t*       param    ADest,
//     in     const sint48_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  /* discard */ coresint8s__trysint48tohdwmaxintbyptr
    (A, B);
} // func

/* inline */ void /* func */ coresint48s__sint48tosfwmaxintbyptr
// (   out    sfwmaxint_t*       param    ADest,
//     in     const sint48_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  /* discard */ coresint48s__trysint48tosfwmaxintbyptr
    (A, B);
} // func

// ------------------

bool /* func */ coresint48s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coresint48s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coresint48s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

// ------------------

/* override */ void /* func */ coresint48s__setup
  ( noparams )
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coresint48s__setoff
  ( noparams )
{
  coresystem__nothing();
} // func

// ------------------


 // ...

// } // namespace coresint48s