/** Module: "coresint80s.h"
 ** Descr.: "..."
 **/
 
// namespace coresint80s {
 
// ------------------
 
#ifndef CORESINT80S__H
#define CORESINT80S__H
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coresint80s__compare
  (/* in */ const sint80_t /* param */ A,
   /* in */ const sint80_t /* param */ B);

bool /* func */ coresint80s__areequal
  (/* in */ const sint80_t /* param */ A,
   /* in */ const sint80_t /* param */ B);

bool /* func*/ coresint80s__isempty
  (/* in */ const sint80_t /* param */ ASource);

void /* func */ coresint80s__clear
  (/* inout */ sint80_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ coresint80s__ptrysint80tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const sint80_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coresint80s__ptrysint80tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const sint80_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

bool /* func */ coresint80s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coresint80s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coresint80s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coresint80s__setup
  ( noparams );

/* override */ void /* func */ coresint80s__setoff
  ( noparams );

// ------------------

#endif // CORESINT80S__H

// } // namespace coresint80s