/** Module: "coresint128s.h"
 ** Descr.: "..."
 **/
 
// namespace coresint128s {
 
// ------------------
 
#ifndef CORESINT128S__H
#define CORESINT128S__H
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coresint128s__compare
  (/* in */ const sint128_t /* param */ A,
   /* in */ const sint128_t /* param */ B);

bool /* func */ coresint128s__areequal
  (/* in */ const sint128_t /* param */ A,
   /* in */ const sint128_t /* param */ B);

bool /* func*/ coresint128s__isempty
  (/* in */ const sint128_t /* param */ ASource);

void /* func */ coresint128s__clear
  (/* inout */ sint128_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ coresint128s__ptrysint128tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const sint128_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coresint128s__ptrysint128tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const sint128_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

bool /* func */ coresint128s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coresint128s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coresint128s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coresint128s__setup
  ( noparams );

/* override */ void /* func */ coresint128s__setoff
  ( noparams );

// ------------------

#endif // CORESINTS128S__H

// } // namespace coresint128s