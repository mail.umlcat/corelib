/** Module: "coresint24s.h"
 ** Descr.: "..."
 **/
 
// namespace coresint24s {
 
// ------------------
 
#ifndef CORESINT24S__H
#define CORESINT24S__H
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coresint24s__compare
  (/* in */ const sint24_t /* param */ A,
   /* in */ const sint24_t /* param */ B);

bool /* func */ coresint24s__areequal
  (/* in */ const sint24_t /* param */ A,
   /* in */ const sint24_t /* param */ B);

bool /* func*/ coresint24s__isempty
  (/* in */ const sint24_t /* param */ ASource);

void /* func */ coresint24s__clear
  (/* inout */ sint24_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ coresint24s__ptrysint24tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const sint24_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coresint24s__ptrysint24tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const sint24_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

/* inline */ void /* func */ coresint24s__psint24tomaxregint
// (   out    maxregint_t*       param    ADest,
//     in     const sint24_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ void /* func */ coresint24s__psint24tomaxvalint
// (   out    maxvalint_t*       param    ADest,
//     in     const sint24_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

 // ------------------

bool /* func */ coresint24s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coresint24s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coresint24s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coresint24__setup
  ( noparams );

/* override */ void /* func */ coresint24__setoff
  ( noparams );

// ------------------

#endif // CORESINT24S__H

// } // namespace coresint24s