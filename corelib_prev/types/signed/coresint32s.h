/** Module: "coresint32s.h"
 ** Descr.: "..."
 **/
 
// namespace coresint32s {
 
// ------------------
 
#ifndef CORESINT32S__H
#define CORESINT32S__H
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------
 
#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coresint32s__compare
  (/* in */ const sint32_t /* param */ A,
   /* in */ const sint32_t /* param */ B);

bool /* func */ coresint32s__areequal
  (/* in */ const sint32_t /* param */ A,
   /* in */ const sint32_t /* param */ B);

bool /* func*/ coresint32s__isempty
  (/* in */ const sint32_t /* param */ ASource);

void /* func */ coresint32s__clear
  (/* inout */ sint32_t /* param */ ADest);

// ------------------

pointer* /* func*/ coreuint32s__uint32toptrcopy
  (/* in */ const uint32_t /* param */ ASource);

bool /* func */ coreuint32s__areequalbyptr
  (pointer* /* param */ A, 
   pointer* /* param */ B);

// ------------------

/* inline */ bool /* func */ coresint32s__ptrysint32tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const sint32_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coresint32s__ptrysint32tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const sint32_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

bool /* func */ coresint32s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coresint32s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coresint32s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

pointer* /* func */ coresint32s__sint32ptrcopy
  (/* in */ const sint32_t /* param */ AValue);

 
// ------------------

/* override */ int /* func */ coresint32s__setup
  ( noparams );

/* override */ int /* func */ coresint32s__setoff
  ( noparams );

// ------------------

#endif // CORESINT32S__H

// } // namespace coresint32s