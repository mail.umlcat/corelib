/** Module: "coresint16s.h"
 ** Descr.: "..."
 **/
 
// namespace coresint16s {
 
// ------------------
 
#ifndef CORESINT16S__H
#define CORESINT16S__H
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------
 
#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coresint16s__compare
  (/* in */ const sint16_t /* param */ A,
   /* in */ const sint16_t /* param */ B);

bool /* func */ coresint16s__areequal
  (/* in */ const sint16_t /* param */ A,
   /* in */ const sint16_t /* param */ B);

bool /* func*/ coresint16s__isempty
  (/* in */ const sint16_t /* param */ ASource);

void /* func */ coresint16s__clear
  (/* inout */ sint16_t /* param */ ADest);

// ------------------

pointer* /* func*/ coresint16s__sint16toptrcopy
  (/* in */ const sint16_t /* param */ ASource);

bool /* func */ coresint16s__areequalbyptr
  (pointer* /* param */ A, 
   pointer* /* param */ B);

// ------------------

/* inline */ bool /* func */ coresint16s__ptrysint16tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const sint16_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coresint16s__ptrysint16tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const sint16_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

bool /* func */ coresint16s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coresint16s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coresint16s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);


 
// ------------------

/* override */ int /* func */ coresint16s__setup
  ( noparams );

/* override */ int /* func */ coresint16s__setoff
  ( noparams );

// ------------------

#endif // CORESINT16S__H

// } // namespace coresint16s