/** Module: "coresint256s.h"
 ** Descr.: "..."
 **/
 
// namespace coresint256s {
 
// ------------------
 
#ifndef CORESINT256S__H
#define CORESINT256S__H
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coresint256s__compare
  (/* in */ const sint256_t /* param */ A,
   /* in */ const sint256_t /* param */ B);

bool /* func */ coresint256s__areequal
  (/* in */ const sint256_t /* param */ A,
   /* in */ const sint256_t /* param */ B);

bool /* func*/ coresint256s__isempty
  (/* in */ const sint256_t /* param */ ASource);

void /* func */ coresint256s__clear
  (/* inout */ sint256_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ coresint256s__ptrysint256tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const sint256_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coresint256s__ptrysint256tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const sint256_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

bool /* func */ coresint256s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coresint256s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coresint256s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coresint256s__setup
  ( noparams );

/* override */ void /* func */ coresint256s__setoff
  ( noparams );

// ------------------

#endif // CORESINTS256S__H

// } // namespace coresint256s