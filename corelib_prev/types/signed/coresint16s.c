/** Module: "coresint16s.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coresint16s {
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------
 
#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "coresint16s.h"
 
// ------------------

comparison /* func */ coresint16s__compare
  (/* in */ const sint16_t /* param */ A,
   /* in */ const sint16_t /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coresint16s__areequal
  (/* in */ const sint16_t /* param */ A,
   /* in */ const sint16_t /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func*/ coresint16s__isempty
  (/* in */ const sint16_t /* param */ ASource);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coresint16s__clear
  (/* inout */ sint16_t /* param */ ADest);
{
  coresystem__nothing();
} // func

// ------------------

pointer* /* func*/ coresint16s__sint16toptrcopy
  (/* in */ const sint16_t /* param */ ASource)
{
  pointer* /* var */ Result = 0;
  // ---
  
  Result =
    corememory__duplicatecopy
      (&ASource, sizeof(sint16_t);
     
  // ---
  return Result;
} // func
  
bool /* func */ coresint16s__areequalbyptr
  (pointer* /* param */ A, 
   pointer* /* param */ B)
{
  bool /* var */ Result = FALSE;
  // ---
  
  Result =
    (A == B);
     
  // ---
  return Result;
} // func


// ------------------

/* inline */ bool /* func */ coresint16s__ptrysint16tomaxhdwint
// (   out    maxhdwint_t*       param    ADest,
//     in     const sint16_p     param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

/* inline */ bool /* func */ coresint16s__ptrysint16tosfwmaxint
// (   out    maxsfwmaxint*      param    ADest,
//     in     const sint16_p     param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coresint16s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coresint16s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coresint16s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

// ------------------

 // ...

/* override */ void /* func */ coresint16s__setup
  ( noparams )
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coresint16s__setoff
  ( noparams )
{
  coresystem__nothing();
} // func

// ------------------

// } // namespace coresint16s