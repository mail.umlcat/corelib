/** Module: "coresignedints.h"
 ** Descr.: "..."
 **/
 
// namespace coresignedints {
 
// ------------------
 
#ifndef CORESIGNEDINTS__H
#define CORESIGNEDINTS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coresint8s.h"
#include "coresint16s.h"
#include "coresint32s.h"
#include "coresint64s.h"
#include "coresint128s.h"
#include "coresint256s.h"
#include "coresint24s.h"
#include "coresint48s.h"
#include "coresint80s.h"

// ------------------

enum signedintegers
{
  signedintegers__8_t,    
  signedintegers__16_t,   
  signedintegers__32_t,   
  signedintegers__64_t,   
  signedintegers__128_t,  
  signedintegers__256_t,  
  signedintegers__24_t,
  signedintegers__48_t,
  signedintegers__80_t,
} ;

typedef
  signedintegers   /* as */ coresignedints__signedintegers;

// ------------------

// greater signed integer
// supported by a cpu register

// include files:
// {
typedef
  sint64_t       /* as */ maxhdwint_t;
typedef
  sint64_p       /* as */ maxhdwint_p;
// }

// greater signed integer value
// supported by the library

// include files:
// {
typedef
  sint256_t      /* as */ maxsfwint_t;
typedef
  sint256p       /* as */ maxsfwint_p;
// }

// ------------------

/* inline */ bool /* func */ coresignedints__tryuinttohdwmaxint
  (/* in */  unsignedintegers /* param */ AIntType,
   /* out */ pointer*         /* param */ ADestPtr,
   /* in */  pointer*         /* param */ ASourcePtr);

/* inline */ bool /* func */ coresignedints__tryuinttosfwmaxint
  (/* in */  unsignedintegers /* param */ AIntType,
   /* out */ pointer*         /* param */ ADestPtr,
   /* in */  pointer*         /* param */ ASourcePtr);

// ------------------

/* inline */ void /* func */ coresignedints__uinttohdwmaxint
  (/* in */  unsignedintegers /* param */ AIntType,
   /* out */ pointer*         /* param */ ADestPtr,
   /* in */  pointer*         /* param */ ASourcePtr);

/* inline */ void /* func */ coresignedints__uinttosfwmaxint
  (/* in */  unsignedintegers /* param */ AIntType,
   /* out */ pointer*         /* param */ ADestPtr,
   /* in */  pointer*         /* param */ ASourcePtr);
   
// ------------------

/* inline */ bool /* func */ coresignedints__trysinttohdwmaxint
  (/* in */  signedintegers /* param */ AIntType,
   /* out */ pointer*       /* param */ ADestPtr,
   /* in */  pointer*       /* param */ ASourcePtr);

/* inline */ bool /* func */ coresignedints__trysinttosfwmaxint
  (/* in */  signedintegers /* param */ AIntType,
   /* out */ pointer*       /* param */ ADestPtr,
   /* in */  pointer*       /* param */ ASourcePtr);

// ------------------

/* inline */ void /* func */ coresignedints__sinttohdwmaxint
  (/* in */  signedintegers /* param */ AIntType,
   /* out */ pointer*       /* param */ ADestPtr,
   /* in */  pointer*       /* param */ ASourcePtr);

/* inline */ void /* func */ coresignedints__sinttosfwmaxint
  (/* in */  signedintegers /* param */ AIntType,
   /* out */ pointer*       /* param */ ADestPtr,
   /* in */  pointer*       /* param */ ASourcePtr);
 
// ------------------

/* implements */ /* tryequality */
bool /* func */ coresignedints__tryareequalsfwmaxint
  (/* in */  pointer* /* param */ A, 
   /* in */  pointer* /* param */ B,
   /* out */ bool*    /* param */ AResult);
{
  bool /* var */ Result = FALSE;
  // ---

  Result =
    (A != NULL) &&
    (B != NULL) &&
    (AResult != NULL);
 
  if (Result)
  {
    maxsfwint_t* /* var */ ThisA = NULL;
    maxsfwint_t* /* var */ ThisB = NULL;

    ThisA =
      (maxsfwint_t*) A;
    ThisB =
      (maxsfwint_t*) B;
      
    Result =
      (*ThisA == *ThisB);
  } // if

  // ---
  return Result;
} // func

/* implements */ /* tryequality */
bool /* func */ coresignedints__tryareequalhdwmaxint
  (/* in */  pointer* /* param */ A, 
   /* in */  pointer* /* param */ B,
   /* out */ bool*    /* param */ AResult);
{
  bool /* var */ Result = FALSE;
  // ---

  Result =
    (A != NULL) &&
    (B != NULL) &&
    (AResult != NULL);
 
  if (Result)
  {
    maxhdwint_t* /* var */ ThisA = NULL;
    maxhdwint_t* /* var */ ThisB = NULL;

    ThisA =
      (maxhdwint_t*) A;
    ThisB =
      (maxhdwint_t*) B;
      
    Result =
      (*ThisA == *ThisB);
  } // if

  // ---
  return Result;
} // func

// ------------------

/* implements */ /* equality */
bool /* func */ areequalhdwmaxint
  (pointer* /* param */ A, 
   pointer* /* param */ B)
{
  bool /* var */ CanContinue = FALSE;

  CanContinue =
    (A != NULL) &&
    (B != NULL) &&
    (AResult != NULL);
 
  if (CanContinue)
  {
    maxsfwint_t* /* var */ ThisA = NULL;
    maxsfwint_t* /* var */ ThisB = NULL;

    ThisA =
      (maxhdwint_t*) A;
    ThisB =
      (maxhdwint_t*) B;
      
    Result =
      (*ThisA == *ThisB);
  } // if
} // func

/* implements */ /* equality */
bool /* func */ areequalsfwmaxint
  (pointer* /* param */ A, 
   pointer* /* param */ B)
{
  return (*((maxsfwint_t*) A)== *((maxsfwint_t*) B));
} // func


// ------------------

 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coresystem__modulename
  ( noparams );

/* override */ int /* func */ coresignedints__setup
  ( noparams );

/* override */ int /* func */ coresignedints__setoff
  ( noparams );

// ------------------

#endif // CORESIGNEDINTS__H

// } // namespace coresignedints