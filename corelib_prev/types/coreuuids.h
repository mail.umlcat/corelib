/** Module: "coreuuids.h"
 ** Descr.: "Universal Unique Identifier"
 ** implementation library."
 **/
 
// namespace coreuuids {
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------
 
#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
//#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

struct uuid128header
{
   uint32_t /* var */ TimeLow;
   uint16_t /* var */ TimeMid;
   uint16_t /* var */ TimeHi;
   uint8_t  /* var */ ClockSeqHi;
   uint8_t  /* var */ ClockSeqLo;
   uint48_t /* var */ Node;
} ;

// ------------------

bool /* func */ coreuuids__iscleared
  (/* in */ const uuid* /* param */ ASource);
 
bool /* func */ coreuuids__areequal
  (/* in */ const uuid* /* param */ A, B);

// ------------------

void /* func */ coreuuids__generate
  (/* out */ uuid* /* param */ ADest);

void /* func */ coreuuids__clear
  (/* out */ uuid* /* param */ ADest);

// ------------------

void /* func */ coreuuids__encodeuuidtostruct
  (/* in */  const uuid*           /* param */ ASource,
   /* out */ struct uuid128header* /* param */ ADest);

void /* func */ coreuuids__decodestructtouuid
  (/* in */  const struct uuid128header* /* param */ ADest,
   /* out */ uuid*                       /* param */ ASource);

// ------------------

void /* func */ coreuuids__encodeuuid
  (/* out */ uuid*           /* param */ ADest,
   /* in */  const uint32_t  /* param */ ATimeLow,
   /* in */  const uint16_t  /* param */ ATimeMid,
   /* in */  const uint16_t  /* param */ ATimeHi,
   /* in */  const uint8_t   /* param */ AClockSeqHi,
   /* in */  const uint8_t   /* param */ AClockSeqLo,
   /* in */  const uint48_t* /* param */ ANode);

void /* func */ coreuuids__decodeuuid
  (/* in */  const uuid* /* param */ ASource,
   /* out */ uint32_t*   /* param */ ATimeLo,
   /* out */ uint16_t*   /* param */ ATimeMid,
   /* out */ uint16_t*   /* param */ ATimeHi,
   /* out */ uint8_t*    /* param */ AClockSeqHi,
   /* out */ uint8_t*    /* param */ AClockSeqLo,
   /* out */ uint48_t*   /* param */ ANode);

// ------------------

void /* func */ coreuuids__decodebytestouuid
  (/* out */ uuid*         /* param */ ADest,
   /* in */  const mem8_t  /* param */ (*); ASource[16]);

void /* func */ coreuuids__decodeuuidtobytes
  (/* in */  const uuid* /* param */ ASource,
   /* out */ mem8_t      /* param */ (*); ADest[16]);

// ------------------

void /* func */ coreuuids__encodequartets
  (/* out */ uuid*         /* param */ ADest,
   /* in */  const mem32_t /* param */ A,
   /* in */  const mem32_t /* param */ B,
   /* in */  const mem32_t /* param */ C,
   /* in */  const mem32_t /* param */ D);

void /* func */ coreuuids__decodequartets
  (/* in */  const uuid* /* param */ ASource,
   /* out */ mem32_t*    /* param */ A,
   /* out */ mem32_t*    /* param */ B,
   /* out */ mem32_t*    /* param */ C,
   /* out */ mem32_t*    /* param */ D);

// ------------------

bool /* func */ coreuuids__tryduplicate
  (/* in */  const pointer*  /* param */ ASource,
   /* out */       pointer** /* param */ ADest);

// ------------------

pointer* /* func */ coreuuids__duplicate
  (/* in */ const pointer* /* param */ AItem);

// ------------------

bool /* func */ coreuuids__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coreuuids__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coreuuids__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);


 // ...

// ------------------

/* override */ const ansinullstring* /* func */ coresystem__modulename
  ( noparams );

/* override */ int /* func */ coreintegers__setup
  ( noparams );

/* override */ int /* func */ coreintegers__setoff
  ( noparams );

// ------------------

// }// namespace coreuuids