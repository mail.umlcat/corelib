/** Module: "coreuint48s.h"
 ** Descr.: "..."
 **/
 
// namespace coreuint48s {
 
// ------------------
 
#ifndef COREUINT48S__H
#define COREUINT48S__H
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coreuint48s__compare
  (/* in */ const uint48_t /* param */ A,
   /* in */ const uint48_t /* param */ B);

bool /* func */ coreuint48s__areequal
  (/* in */ const uint48_t /* param */ A,
   /* in */ const uint48_t /* param */ B);

bool /* func*/ coreuint48s__isempty
  (/* in */ const uint48_t /* param */ ASource);

void /* func */ coreuint48s__clear
  (/* inout */ uint48_t /* param */ ADest);

// ------------------

pointer* /* func*/ coreuint48s__uint48toptrcopy
  (/* in */ const uint48_t /* param */ ASource);

bool /* func */ coreuint48s__areequalbyptr
  (pointer* /* param */ A, 
   pointer* /* param */ B);

// ------------------

/* inline */ bool /* func */ coreuint48s__tryuint48tohdwmaxintbyptr
// (   out    hdwmaxint_t*       param    ADest,
//     in     const uint48_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coreuint48s__tryuint48tosfwmaxintbyptr
// (   out    sfwmaxint_t*       param    ADest,
//     in     const uint48_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

/* inline */ void /* func */ coreuint48s__uint8tohdwmaxintbyptr
// (   out    hdwmaxint_t*       param    ADest,
//     in     const uint8_p      param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)

/* inline */ void /* func */ coreuint48s__uint8tosfwmaxintbyptr
// (   out    sfwmaxint_t*       param    ADest,
//     in     const uint8_p      param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)

// ------------------

bool /* func */ coreuint48s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coreuint48s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coreuint48s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coreuint48s__setup
  ( noparams );

/* override */ void /* func */ coreuint48s__setoff
  ( noparams );

// ------------------

#endif // COREUINT48S__H

// } // namespace coreuint48s