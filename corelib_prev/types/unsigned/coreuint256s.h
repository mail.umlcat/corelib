/** Module: "coreuint256s.h"
 ** Descr.: "..."
 **/
 
// namespace coreuint256s {
 
// ------------------
 
#ifndef COREUINT256S__H
#define COREUINT256S__H
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coreuint256s__compare
  (/* in */ const uint256_t /* param */ A,
   /* in */ const uint256_t /* param */ B);

bool /* func */ coreuint256s__areequal
  (/* in */ const uint256_t /* param */ A,
   /* in */ const uint256_t /* param */ B);

bool /* func*/ coreuint256s__isempty
  (/* in */ const uint256_t /* param */ ASource);

void /* func */ coreuint256s__clear
  (/* inout */ uint256_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ coreuint256s__ptryuint256tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const uint256_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coreuint256s__ptryuint256tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const uint256_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

bool /* func */ coreuint256s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coreuint256s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coreuint256s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coreuint256s__setup
  ( noparams );

/* override */ void /* func */ coreuint256s__setoff
  ( noparams );

// ------------------

#endif // COREUINTS256S__H

// } // namespace coreuint256s