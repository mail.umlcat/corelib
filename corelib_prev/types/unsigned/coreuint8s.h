/** Module: "coreuint8s.h"
 ** Descr.: "..."
 **/
 
// namespace coreuint8s {
 
// ------------------
 
#ifndef COREUINT8S__H
#define COREUINT8S__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------
 
#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coreuint8s__compare
  (/* in */ const uint8_t /* param */ A,
   /* in */ const uint8_t /* param */ B);

bool /* func */ coreuint8s__areequal
  (/* in */ const uint8_t /* param */ A,
   /* in */ const uint8_t /* param */ B);

bool /* func*/ coreuint8s__isempty
  (/* in */ const uint8_t /* param */ ASource);

void /* func */ coreuint8s__clear
  (/* inout */ uint8_t /* param */ ADest);

// ------------------

pointer* /* func*/ coreuint8s__uint8toptrcopy
  (/* in */ const uint8_t /* param */ ASource);

// ------------------

pointer* /* func*/ coreuint8s__duplicatebyptr
  //(/* in */ const uint8_t* /* param */ ASource);
  (/* in */ const pointer* /* param */ ASource);

bool /* func */ coreuint8s__areequalbyptr
//  (uint8_t* /* param */ A, 
//   uint8_t* /* param */ B);
  (pointer* /* param */ A, 
   pointer* /* param */ B);

// ------------------

/* inline */ bool /* func */ coreuint8s__tryuint8tohdwmaxintbyptr
// (   out    hdwmaxint_t*       param    ADest,
//     in     const uint8_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coreuint8s__tryuint8tosfwmaxintbyptr
// (   out    sfwmaxint_t*       param    ADest,
//     in     const uint8_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

/* inline */ void /* func */ coreuint8s__uint8tohdwmaxintbyptr
// (   out    hdwmaxint_t*       param    ADest,
//     in     const uint8_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ void /* func */ coreuint8s__uint8tosfwmaxintbyptr
// (   out    sfwmaxint_t*       param    ADest,
//     in     const uint8_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

 // ------------------

bool /* func */ coreuint8s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coreuint8s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coreuint8s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

// ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coreuint8__setup
  ( noparams );

/* override */ void /* func */ coreuint8__setoff
  ( noparams );

// ------------------

#endif // COREUINT8S__H

// } // namespace coreuint8s