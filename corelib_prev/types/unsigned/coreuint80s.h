/** Module: "coreuint80s.h"
 ** Descr.: "..."
 **/
 
// namespace coreuint80s {
 
// ------------------
 
#ifndef COREUINT80S__H
#define COREUINT80S__H
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coreuint80s__compare
  (/* in */ const uint80_t /* param */ A,
   /* in */ const uint80_t /* param */ B);

bool /* func */ coreuint80s__areequal
  (/* in */ const uint80_t /* param */ A,
   /* in */ const uint80_t /* param */ B);

bool /* func*/ coreuint80s__isempty
  (/* in */ const uint80_t /* param */ ASource);

void /* func */ coreuint80s__clear
  (/* inout */ uint80_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ coreuint80s__ptryuint80tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const uint80_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coreuint80s__ptryuint80tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const uint80_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

bool /* func */ coreuint80s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coreuint80s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coreuint80s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coreuint80s__setup
  ( noparams );

/* override */ void /* func */ coreuint80s__setoff
  ( noparams );

// ------------------

#endif // COREUINT80S__H

// } // namespace coreuint80s