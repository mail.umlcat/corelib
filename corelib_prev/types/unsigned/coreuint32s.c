/** Module: "coreuint32s.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coreuint32s {
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------
 
#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreuint32s.h"
 
// ------------------

comparison /* func */ coreuint32s__compare
  (/* in */ const uint32_t /* param */ A,
   /* in */ const uint32_t /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreuint32s__areequal
  (/* in */ const uint32_t /* param */ A,
   /* in */ const uint32_t /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func*/ coreuint32s__isempty
  (/* in */ const uint32_t /* param */ ASource);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreuint32s__clear
  (/* inout */ uint32_t /* param */ ADest);
{
  coresystem__nothing();
} // func

// ------------------

/* inline */ bool /* func */ coreuint32s__ptryuint32tomaxhdwint
// (   out    maxhdwint_t*       param    ADest,
//     in     const uint32_p     param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

/* inline */ bool /* func */ coreuint32s__ptryuint32tosfwmaxint
// (   out    maxsfwmaxint*      param    ADest,
//     in     const uint32_p     param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

pointer* /* func*/ coreuint32s__uint32toptrcopy
  (/* in */ const uint32_t /* param */ ASource)
{
  pointer* /* var */ Result = 0;
  // ---
  
  Result =
    corememory__duplicatecopy
      (&ASource, sizeof(uint32_t);
     
  // ---
  return Result;
} // func

bool /* func */ coreuint32s__areequalbyptr
  (pointer* /* param */ A, 
   pointer* /* param */ B)
{
  bool /* var */ Result = FALSE;
  // ---
  
  Result =
    (A == B);
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreuint32s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreuint32s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreuint32s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

pointer* /* func */ coreuint32s__uint32ptrcopy
  (/* in */ const uint32_t /* param */ AValue)
{
  pointer* /* var */ Result = NULL;
  // ---
     
  Result =
    coresystem__malloc(sizeof(uint32_t));
  coresystem__safecopy(Result, &AValue);
  
  // ---
  return Result;
} // func

// ------------------

 // ...

/* override */ void /* func */ coreuint32s__setup
  ( noparams )
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coreuint32s__setoff
  ( noparams )
{
  coresystem__nothing();
} // func

// ------------------

// } // namespace coreuint32s