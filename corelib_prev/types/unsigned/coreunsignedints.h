/** Module: "coreunsignedints.h"
 ** Descr.: "..."
 **/
 
// namespace coreunsignedints {
 
// ------------------
 
#ifndef COREUNSIGNEDINTS__H
#define COREUNSIGNEDINTS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreuint8s.h"
#include "coreuint16s.h"
#include "coreuint32s.h"
#include "coreuint64s.h"
#include "coreuint128s.h"
#include "coreuint256s.h"
#include "coresint24s.h"
#include "coresint48s.h"
#include "coresint80s.h"

// ------------------

enum unsignedintegers
{
  unsignedintegers__8_t,    
  unsignedintegers__16_t,   
  unsignedintegers__32_t,   
  unsignedintegers__64_t,   
  unsignedintegers__128_t,  
  unsignedintegers__256_t,
  unsignedintegers__24_t,
  unsignedintegers__48_t,
  unsignedintegers__80_t,
} ;

typedef
  unsignedintegers /* as */ coreunsignedints__unsignedintegers;


// ------------------

/* inline */ bool /* func */ coreunsignedints__tryuinttohdwmaxint
  (/* in */  unsignedintegers /* param */ AIntType,
   /* out */ pointer*         /* param */ ADestPtr,
   /* in */  pointer*         /* param */ ASourcePtr);

/* inline */ bool /* func */ coreunsignedints__tryuinttosfwmaxint
  (/* in */  unsignedintegers /* param */ AIntType,
   /* out */ pointer*         /* param */ ADestPtr,
   /* in */  pointer*         /* param */ ASourcePtr);

// ------------------

/* inline */ void /* func */ coreunsignedints__uinttohdwmaxint
  (/* in */  unsignedintegers /* param */ AIntType,
   /* out */ pointer*         /* param */ ADestPtr,
   /* in */  pointer*         /* param */ ASourcePtr);

/* inline */ void /* func */ coreunsignedints__uinttosfwmaxint
  (/* in */  unsignedintegers /* param */ AIntType,
   /* out */ pointer*         /* param */ ADestPtr,
   /* in */  pointer*         /* param */ ASourcePtr);
   
// ------------------

/* inline */ bool /* func */ coreunsignedints__trysinttohdwmaxint
  (/* in */  signedintegers /* param */ AIntType,
   /* out */ pointer*       /* param */ ADestPtr,
   /* in */  pointer*       /* param */ ASourcePtr);

/* inline */ bool /* func */ coreunsignedints__trysinttosfwmaxint
  (/* in */  signedintegers /* param */ AIntType,
   /* out */ pointer*       /* param */ ADestPtr,
   /* in */  pointer*       /* param */ ASourcePtr);

// ------------------

/* inline */ void /* func */ coreunsignedints__sinttohdwmaxint
  (/* in */  signedintegers /* param */ AIntType,
   /* out */ pointer*       /* param */ ADestPtr,
   /* in */  pointer*       /* param */ ASourcePtr);

/* inline */ void /* func */ coreunsignedints__sinttosfwmaxint
  (/* in */  signedintegers /* param */ AIntType,
   /* out */ pointer*       /* param */ ADestPtr,
   /* in */  pointer*       /* param */ ASourcePtr);
 
// ------------------

/* implements */ /* tryequality */
bool /* func */ coreunsignedints__tryareequalsfwmaxint
  (/* in */  pointer* /* param */ A, 
   /* in */  pointer* /* param */ B,
   /* out */ bool*    /* param */ AResult);
{
  bool /* var */ Result = FALSE;
  // ---

  Result =
    (A != NULL) &&
    (B != NULL) &&
    (AResult != NULL);
 
  if (Result)
  {
    maxsfwint_t* /* var */ ThisA = NULL;
    maxsfwint_t* /* var */ ThisB = NULL;

    ThisA =
      (maxsfwint_t*) A;
    ThisB =
      (maxsfwint_t*) B;
      
    Result =
      (*ThisA == *ThisB);
  } // if

  // ---
  return Result;
} // func

/* implements */ /* tryequality */
bool /* func */ coreunsignedints__tryareequalhdwmaxint
  (/* in */  pointer* /* param */ A, 
   /* in */  pointer* /* param */ B,
   /* out */ bool*    /* param */ AResult);
{
  bool /* var */ Result = FALSE;
  // ---

  Result =
    (A != NULL) &&
    (B != NULL) &&
    (AResult != NULL);
 
  if (Result)
  {
    maxhdwint_t* /* var */ ThisA = NULL;
    maxhdwint_t* /* var */ ThisB = NULL;

    ThisA =
      (maxhdwint_t*) A;
    ThisB =
      (maxhdwint_t*) B;
      
    Result =
      (*ThisA == *ThisB);
  } // if

  // ---
  return Result;
} // func

// ------------------

/* implements */ /* equality */
bool /* func */ areequalhdwmaxint
  (pointer* /* param */ A, 
   pointer* /* param */ B)
{
  bool /* var */ CanContinue = FALSE;

  CanContinue =
    (A != NULL) &&
    (B != NULL) &&
    (AResult != NULL);
 
  if (CanContinue)
  {
    maxsfwint_t* /* var */ ThisA = NULL;
    maxsfwint_t* /* var */ ThisB = NULL;

    ThisA =
      (maxhdwint_t*) A;
    ThisB =
      (maxhdwint_t*) B;
      
    Result =
      (*ThisA == *ThisB);
  } // if
} // func

/* implements */ /* equality */
bool /* func */ areequalsfwmaxint
  (pointer* /* param */ A, 
   pointer* /* param */ B)
{
  return (*((maxsfwint_t*) A)== *((maxsfwint_t*) B));
} // func


// ------------------

 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coresystem__modulename
  ( noparams );

/* override */ int /* func */ coreunsignedints__setup
  ( noparams );

/* override */ int /* func */ coreunsignedints__setoff
  ( noparams );

// ------------------

#endif // COREUNSIGNEDINTS__H

// } // namespace coreunsignedints