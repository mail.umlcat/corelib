/** Module: "coreuint24s.h"
 ** Descr.: "..."
 **/
 
// namespace coreuint24s {
 
// ------------------
 
#ifndef COREUINT24S__H
#define COREUINT24S__H
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coreuint24s__compare
  (/* in */ const uint24_t /* param */ A,
   /* in */ const uint24_t /* param */ B);

bool /* func */ coreuint24s__areequal
  (/* in */ const uint24_t /* param */ A,
   /* in */ const uint24_t /* param */ B);

bool /* func*/ coreuint24s__isempty
  (/* in */ const uint24_t /* param */ ASource);

void /* func */ coreuint24s__clear
  (/* inout */ uint24_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ coreuint24s__ptryuint24tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const uint24_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coreuint24s__ptryuint24tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const uint24_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

/* inline */ void /* func */ coreuint24s__puint24tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const uint24_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ void /* func */ coreuint24s__puint24tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const uint24_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

 // ------------------

bool /* func */ coreuint24s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coreuint24s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coreuint24s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

// ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coreuint24__setup
  ( noparams );

/* override */ void /* func */ coreuint24__setoff
  ( noparams );

// ------------------

#endif // COREUINT24S__H

// } // namespace coreuint24s