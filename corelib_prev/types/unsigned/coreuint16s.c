/** Module: "coreuint16s.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coreuint16s {
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------
 
#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreuint16s.h"
 
// ------------------

comparison /* func */ coreuint16s__compare
  (/* in */ const uint16_t /* param */ A,
   /* in */ const uint16_t /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreuint16s__areequal
  (/* in */ const uint16_t /* param */ A,
   /* in */ const uint16_t /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func*/ coreuint16s__isempty
  (/* in */ const uint16_t /* param */ ASource);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreuint16s__clear
  (/* inout */ uint16_t /* param */ ADest);
{
  coresystem__nothing();
} // func

// ------------------

pointer* /* func*/ coreuint16s__uint16toptrcopy
  (/* in */ const uint16_t /* param */ ASource)
{
  pointer* /* var */ Result = 0;
  // ---
  
  Result =
    corememory__duplicatecopy
      (&ASource, sizeof(uint16_t);
     
  // ---
  return Result;
} // func

bool /* func */ coreuint16s__areequalbyptr
  (pointer* /* param */ A, 
   pointer* /* param */ B)
{
  bool /* var */ Result = FALSE;
  // ---
  
  Result =
    (A == B);
     
  // ---
  return Result;
} // func

// ------------------

/* inline */ bool /* func */ coreuint16s__ptryuint16tomaxhdwint
// (   out    maxhdwint_t*       param    ADest,
//     in     const uint16_p     param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

/* inline */ bool /* func */ coreuint16s__ptryuint16tosfwmaxint
// (   out    maxsfwmaxint*      param    ADest,
//     in     const uint16_p     param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreuint16s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreuint16s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreuint16s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

// ------------------

 // ...

/* override */ void /* func */ coreuint16s__setup
  ( noparams )
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coreuint16s__setoff
  ( noparams )
{
  coresystem__nothing();
} // func

// ------------------

// } // namespace coreuint16s