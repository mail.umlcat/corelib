/** Module: "coreuint128s.h"
 ** Descr.: "..."
 **/
 
// namespace coreuint128s {
 
// ------------------
 
#ifndef COREUINT128S__H
#define COREUINT128S__H
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coreuint128s__compare
  (/* in */ const uint128_t /* param */ A,
   /* in */ const uint128_t /* param */ B);

bool /* func */ coreuint128s__areequal
  (/* in */ const uint128_t /* param */ A,
   /* in */ const uint128_t /* param */ B);

bool /* func*/ coreuint128s__isempty
  (/* in */ const uint128_t /* param */ ASource);

void /* func */ coreuint128s__clear
  (/* inout */ uint128_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ coreuint128s__ptryuint128tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const uint128_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coreuint128s__ptryuint128tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const uint128_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

bool /* func */ coreuint128s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coreuint128s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coreuint128s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coreuint128s__setup
  ( noparams );

/* override */ void /* func */ coreuint128s__setoff
  ( noparams );

// ------------------

#endif // COREUINTS128S__H

// } // namespace coreuint128s