/** Module: "coreuint32s.h"
 ** Descr.: "..."
 **/
 
// namespace coreuint32s {
 
// ------------------
 
#ifndef COREUINT32S__H
#define COREUINT32S__H
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------
 
#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coreuint32s__compare
  (/* in */ const uint32_t /* param */ A,
   /* in */ const uint32_t /* param */ B);

bool /* func */ coreuint32s__areequal
  (/* in */ const uint32_t /* param */ A,
   /* in */ const uint32_t /* param */ B);

bool /* func*/ coreuint32s__isempty
  (/* in */ const uint32_t /* param */ ASource);

void /* func */ coreuint32s__clear
  (/* inout */ uint32_t /* param */ ADest);

// ------------------

/* inline */ bool /* func */ coreuint32s__ptryuint32tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const uint32_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coreuint32s__ptryuint32tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const uint32_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

pointer* /* func*/ coreuint32s__uint32toptrcopy
  (/* in */ const uint32_t /* param */ ASource);

bool /* func */ coreuint32s__areequalbyptr
  (pointer* /* param */ A, 
   pointer* /* param */ B);

// ------------------

bool /* func */ coreuint32s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coreuint32s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coreuint32s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

pointer* /* func */ coreuint32s__uint32ptrcopy
  (/* in */ const uint32_t /* param */ AValue);

 
// ------------------

/* override */ int /* func */ coreuint32s__setup
  ( noparams );

/* override */ int /* func */ coreuint32s__setoff
  ( noparams );

// ------------------

#endif // COREUINT32S__H

// } // namespace coreuint32s