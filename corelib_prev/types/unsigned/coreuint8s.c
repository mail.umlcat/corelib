/** Module: "coreuint8s.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coreuint8s {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------
 
#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreuint8s.h"
 
// ------------------

comparison /* func */ coreuint8s__compare
  (/* in */ const uint8_t /* param */ A,
   /* in */ const uint8_t /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreuint8s__areequal
  (/* in */ const uint8_t /* param */ A,
   /* in */ const uint8_t /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func*/ coreuint8s__isempty
  (/* in */ const uint8_t /* param */ ASource);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreuint8s__clear
  (/* inout */ uint8_t /* param */ ADest);
{
  coresystem__nothing();
} // func

// ------------------

pointer* /* func*/ coreuint8s__uint8toptrcopy
  (/* in */ const uint8_t /* param */ ASource)
{
  pointer* /* var */ Result = 0;
  // ---
  
  Result =
    corememory__duplicatecopy
      (&ASource, sizeof(uint8_t);
     
  // ---
  return Result;
} // func

// ------------------

pointer* /* func*/ coreuint8s__duplicatebyptr
  //(/* in */ const uint8_t* /* param */ ASource);
  (/* in */ const pointer* /* param */ ASource);
{
  pointer* /* var */ Result = 0;
  // ---
  
  Result =
    corememory__duplicatecopy
      (ASource, sizeof(uint8_t);
     
  // ---
  return Result;
} // func

bool /* func */ coreuint8s__areequalbyptr
//  (uint8_t* /* param */ A, 
//   uint8_t* /* param */ B)
  (pointer* /* param */ A, 
   pointer* /* param */ B)
{
  bool /* var */ Result = FALSE;
  // ---
  
  Result =
    (A == B);
     
  // ---
  return Result;
} // func

// ------------------

/* inline */ bool /* func */ coreuint8s__tryuint8tomaxhdwintbyptr
// (   out    maxhdwint_t*       param    ADest,
//     in     const uint8_p      param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

/* inline */ bool /* func */ coreuint8s__tryuint8tosfwmaxintbyptr
// (   out    maxsfwmaxint*      param    ADest,
//     in     const uint8_p      param    ASource)
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* inline */ void /* func */ coreuint8s__uint8tohdwmaxintbyptr
// (   out    hdwmaxint_t*       param    ADest,
//     in     const uint8_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  /* discard */ coreuint8s__tryuint8tohdwmaxintbyptr
    (A, B);
} // func

/* inline */ void /* func */ coreuint8s__uint8tosfwmaxintbyptr
// (   out    sfwmaxint_t*       param    ADest,
//     in     const uint8_p      param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource)
{
  /* discard */ coreuint8s__tryint8tosfwmaxintbyptr
    (A, B);
} // func

 // ------------------

bool /* func */ coreuint8s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreuint8s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreuint8s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

// ------------------

/* override */ void /* func */ coreuint8__setup
  ( noparams )
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coreuint8__setoff
  ( noparams )
{
  coresystem__nothing();
} // func

// ------------------


 // ...

// } // namespace coreuint8s