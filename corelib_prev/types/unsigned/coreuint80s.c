/** Module: "coreuint80s.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coreuint80s {
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "coreuint80s.h"
 
// ------------------

comparison /* func */ coreuint80s__compare
  (/* in */ const uint80_t /* param */ A,
   /* in */ const uint80_t /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreuint80s__areequal
  (/* in */ const uint80_t /* param */ A,
   /* in */ const uint80_t /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func*/ coreuint80s__isempty
  (/* in */ const uint80_t /* param */ ASource);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreuint80s__clear
  (/* inout */ uint80_t /* param */ ADest);
{
  coresystem__nothing();
} // func

 // ------------------

bool /* func */ coreuint80s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreuint80s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreuint80s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coreuint80s__setup
  ( noparams )
{
  coresystem__nothing();
} // func

/* override */ void /* func */ coreuint80s__setoff
  ( noparams )
{
  coresystem__nothing();
} // func

// ------------------


 // ...

// } // namespace coreuint80s