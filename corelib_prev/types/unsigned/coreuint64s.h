/** Module: "coreuint64s.h"
 ** Descr.: "..."
 **/
 
// namespace coreuint64s {
 
// ------------------
 
#ifndef COREUINT64S__H
#define COREUINT64S__H
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------
 
#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coreuint64s__compare
  (/* in */ const uint64_t /* param */ A,
   /* in */ const uint64_t /* param */ B);

bool /* func */ coreuint64s__areequal
  (/* in */ const uint64_t /* param */ A,
   /* in */ const uint64_t /* param */ B);

bool /* func*/ coreuint64s__isempty
  (/* in */ const uint64_t /* param */ ASource);

void /* func */ coreuint64s__clear
  (/* inout */ uint64_t /* param */ ADest);

// ------------------

pointer* /* func*/ coreuint64s__uint64toptrcopy
  (/* in */ const uint64_t /* param */ ASource);

bool /* func */ coreuint64s__areequalbyptr
  (pointer* /* param */ A, 
   pointer* /* param */ B);

// ------------------

/* inline */ bool /* func */ coreuint64s__ptryuint64tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const uint64_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coreuint64s__ptryuint64tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const uint64_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

bool /* func */ coreuint64s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coreuint64s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coreuint64s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* override */ void /* func */ coreuint64s__setup
  ( noparams );

/* override */ void /* func */ coreuint64s__setoff
  ( noparams );

// ------------------

#endif // COREUINT64S__H

// } // namespace coreuint64s