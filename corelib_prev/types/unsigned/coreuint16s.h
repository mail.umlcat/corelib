/** Module: "coreuint16s.h"
 ** Descr.: "..."
 **/
 
// namespace coreuint16s {
 
// ------------------
 
#ifndef COREUINT16S__H
#define COREUINT16S__H
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------
 
#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

comparison /* func */ coreuint16s__compare
  (/* in */ const uint16_t /* param */ A,
   /* in */ const uint16_t /* param */ B);

bool /* func */ coreuint16s__areequal
  (/* in */ const uint16_t /* param */ A,
   /* in */ const uint16_t /* param */ B);

bool /* func*/ coreuint16s__isempty
  (/* in */ const uint16_t /* param */ ASource);

void /* func */ coreuint16s__clear
  (/* inout */ uint16_t /* param */ ADest);

// ------------------

pointer* /* func*/ coreuint16s__uint16toptrcopy
  (/* in */ const uint16_t /* param */ ASource);

bool /* func */ coreuint16s__areequalbyptr
  (pointer* /* param */ A, 
   pointer* /* param */ B);

// ------------------

/* inline */ bool /* func */ coreuint16s__ptryuint16tohdwmaxint
// (   out    hdwmaxint_t*       param    ADest,
//     in     const uint16_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

/* inline */ bool /* func */ coreuint16s__ptryuint16tosfwmaxint
// (   out    sfwmaxint_t*       param    ADest,
//     in     const uint16_p     param    ASource);
   (/* out */ pointer*        /* param */ ADest,
    /* in  */ const pointer*  /* param */ ASource);

// ------------------

bool /* func */ coreuint16s__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coreuint16s__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coreuint16s__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);


 
// ------------------

/* override */ int /* func */ coreuint16s__setup
  ( noparams );

/* override */ int /* func */ coreuint16s__setoff
  ( noparams );

// ------------------

#endif // COREUINT16S__H

// } // namespace coreuint16s