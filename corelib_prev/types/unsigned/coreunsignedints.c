/** Module: "coreunsignedints.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coreunsignedints {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreunsignedints.h"

// ------------------

#include "coreuint8s.h"
#include "coreuint16s.h"
#include "coreuint32s.h"
#include "coreuint64s.h"
#include "coreuint128s.h"
#include "coreuint256s.h"
#include "coreuint24s.h"
#include "coreuint48s.h"
#include "coreuint80s.h"

// ------------------

/* inline */ bool /* func */ coreunsignedints__tryuinttohdwmaxint
  (/* in */  unsignedintegers /* param */ AIntType,
   /* out */ pointer*         /* param */ ADestPtr,
   /* in */  pointer*         /* param */ ASourcePtr)
{
  bool /* var */ Result = FALSE;
  // ---

  switch (AIntType)
  {
    case unsignedintegers__8_t:
      Result =
        coreuint8s__ptryuint8tohdwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case unsignedintegers__16_t:
      Result =
        coreuint16s__ptryuint16tohdwmaxint
          (ADestPtr, ASourcePtr);
    break;    
    
    case unsignedintegers__32_t:
      Result =
        coreuint32s__ptryuint32tohdwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case unsignedintegers__64_t:
      Result =
        coreuint64s__ptryuint64tohdwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case unsignedintegers__128_t:
      Result =
        coreuint128s__ptryuint128tohdwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case unsignedintegers__256_t:
      Result =
        coreuint256s__ptryuint256tohdwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case unsignedintegers__48_t:
      Result =
        coreuint48s__ptryuint48tohdwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case unsignedintegers__80_t:
      Result =
        coreuint80s__ptryuint80tohdwmaxint
          (ADestPtr, ASourcePtr);
    break;    
  }

  // ---
  return Result;
} // func

/* inline */ bool /* func */ coreunsignedints__tryuinttosfwmaxint
  (/* in */  unsignedintegers /* param */ AIntType,
   /* out */ pointer*         /* param */ ADestPtr,
   /* in */  pointer*         /* param */ ASourcePtr)
{
  bool /* var */ Result = FALSE;
  // ---

  switch (AIntType)
  {
    case unsignedintegers__8_t:
      Result =
        coreuint8s__ptryuint8tosfwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case unsignedintegers__16_t:
      Result =
        coreuint16s__ptryuint16tosfwmaxint
          (ADestPtr, ASourcePtr);
    break;    
    
    case unsignedintegers__32_t:
      Result =
        coreuint32s__ptryuint32tosfwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case unsignedintegers__64_t:
      Result =
        coreuint64s__ptryuint64tosfwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case unsignedintegers__128_t:
      Result =
        coreuint128s__ptryuint128tosfwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case unsignedintegers__256_t:
      Result =
        coreuint256s__ptryuint256tosfwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case unsignedintegers__48_t:
      Result =
        coreuint48s__ptryuint48tosfwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case unsignedintegers__80_t:
      Result =
        coreuint80s__ptryuint80tosfwmaxint
          (ADestPtr, ASourcePtr);
    break;    
  }

  // ---
  return Result;
} // func

// ------------------

/* inline */ bool /* func */ coreunsignedints__trysinttohdwmaxint
  (/* in */  signedintegers /* param */ AIntType,
   /* out */ pointer*         /* param */ ADestPtr,
   /* in */  pointer*         /* param */ ASourcePtr)
{
  bool /* var */ Result = FALSE;
  // ---

  switch (AIntType)
  {
    case signedintegers__8_t:
      Result =
        coresint8s__ptrysint8tohdwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case signedintegers__16_t:
      Result =
        coresint16s__ptrysint16tohdwmaxint
          (ADestPtr, ASourcePtr);
    break;    
    
    case signedintegers__32_t:
      Result =
        coresint32s__ptrysint32tohdwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case signedintegers__64_t:
      Result =
        coresint64s__ptrysint64tohdwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case signedintegers__128_t:
      Result =
        coresint128s__ptrysint128tohdwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case signedintegers__256_t:
      Result =
        coresint256s__ptrysint256tohdwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case signedintegers__48_t:
      Result =
        coresint48s__ptrysint48tohdwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case signedintegers__80_t:
      Result =
        coresint80s__ptrysint80tohdwmaxint
          (ADestPtr, ASourcePtr);
    break;    
  }

  // ---
  return Result;
} // func

/* inline */ bool /* func */ coreunsignedints__trysinttosfwmaxint
  (/* in */  signedintegers /* param */ AIntType,
   /* out */ pointer*         /* param */ ADestPtr,
   /* in */  pointer*         /* param */ ASourcePtr)
{
  bool /* var */ Result = FALSE;
  // ---

  switch (AIntType)
  {
    case signedintegers__8_t:
      Result =
        coresint8s__ptrysint8tosfwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case signedintegers__16_t:
      Result =
        coresint16s__ptrysint16tosfwmaxint
          (ADestPtr, ASourcePtr);
    break;    
    
    case signedintegers__32_t:
      Result =
        coresint32s__ptrysint32tosfwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case signedintegers__64_t:
      Result =
        coresint64s__ptrysint64tosfwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case signedintegers__128_t:
      Result =
        coresint128s__ptrysint128tosfwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case signedintegers__256_t:
      Result =
        coresint256s__ptrysint256tosfwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case signedintegers__48_t:
      Result =
        coresint48s__ptrysint48tosfwmaxint
          (ADestPtr, ASourcePtr);
    break;
    
    case signedintegers__80_t:
      Result =
        coresint80s__ptrysint80tosfwmaxint
          (ADestPtr, ASourcePtr);
    break;    
  }

  // ---
  return Result;
} // func

// ------------------

/* inline */ void /* func */ coreunsignedints__uinttohdwmaxint
  (/* in */  unsignedintegers /* param */ AIntType,
   /* out */ pointer*         /* param */ ADestPtr,
   /* in */  pointer*         /* param */ ASourcePtr)
{
  /* discard */ uinttohdwmaxint
    (AIntType, ADestPtr, ASourcePtr);
} // func

/* inline */ void /* func */ coreunsignedints__uinttosfwmaxint
  (/* in */  unsignedintegers /* param */ AIntType,
   /* out */ pointer*         /* param */ ADestPtr,
   /* in */  pointer*         /* param */ ASourcePtr)
{
  /* discard */ uinttosfwmaxint
    (AIntType, ADestPtr, ASourcePtr);
} // func

// ------------------

/* inline */ void /* func */ coreunsignedints__sinttohdwmaxint
  (/* in */  signedintegers /* param */ AIntType,
   /* out */ pointer*       /* param */ ADestPtr,
   /* in */  pointer*       /* param */ ASourcePtr)
{
  /* discard */ sinttohdwmaxint
    (AIntType, ADestPtr, ASourcePtr);
} // func

/* inline */ void /* func */ coreunsignedints__sinttosfwmaxint
  (/* in */  signedintegers /* param */ AIntType,
   /* out */ pointer*       /* param */ ADestPtr,
   /* in */  pointer*       /* param */ ASourcePtr)
{
  /* discard */ sinttosfwmaxint
    (AIntType, ADestPtr, ASourcePtr);
} // func


 // ...

// ------------------

/* override */ const ansinullstring* /* func */ coresystem__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "coreunsignedints";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coreunsignedints__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreunsignedints__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coreunsignedints