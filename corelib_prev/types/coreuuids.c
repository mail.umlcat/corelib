/** Module: "coreuuids.h"
 ** Descr.: "Universal Unique Identifier"
 ** implementation library."
 **/

// namespace coreuuids {
 
// ------------------
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------
 
#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
//#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "coreuuids.h"
 
// ------------------

bool /* func */ coreuuids__iscleared
  (/* in */ const uuid* /* param */ ASource)
{
    bool /* var */ Result = false;
    // ---
    
    Result =
      (ADest != NULL);
    if (Result)
    {
      int /* var */ i = 0;
      bool /* var */ CanContinue = false;
      bool /* var */ Found = false;
    
      while (CanContinue)
      {
         Found =
          (ADest[i] != 0);
      	
         CanContinue =
          (i < 4) && (!Found);
         i++;
      } // while
      
      Result = (!Found);
    } // if
     
    // ---
    return Result;
} // func
 
bool /* func */ coreuuids__areequal
  (/* in */ const uuid* /* param */ A, B)
{
    bool /* var */ Result = false;
    // ---
    
    Result =
      corememory__safeequal
        (A, B, sizeof(uuid_t));
     
    // ---
    return Result;
} // func

// ------------------

void /* func */ coreuuids__generate
  (/* out */ uuid* /* param */ ADest);
{
  if (ADest != NULL)
  {

  } // if
} // func

void /* func */ coreuuids__clear
  (/* out */ uuid* /* param */ ADest)
{
  if (ADest != NULL)
  {
    for (int /* var */ i = 0; i < 4; i++)
    {
      ADest[i] = 0;
    }
  } // if
} // func

// ------------------

void /* func */ coreuuids__encodeuuidtostruct
  (/* in */  const uuid*           /* param */ ASource,
   /* out */ struct uuid128header* /* param */ ADest)
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (ADest != NULL) &&
    (ASource != NULL);

  if (CanContinue)
  {
     mem8_t /* var */ ASourceBytes[16];
     mem8_t /* var */ AMem48Bytes[6];

     // copy uuid to byte array
     coreuuids__encodeuuidtobytes
       (ASource, ASourceBytes);

     // copy byte array to structs fields
     coremem32s__Encode8to32
       (&(ADest->TimeLow), ASourceBytes[0], ASourceBytes[1], ASourceBytes[2], ASourceBytes[3]);
     coremem16s__Encode8to16
       (&(ADest->TimeMid), ASourceBytes[4], ASourceBytes[5]);
     coremem16s__Encode8to16
       (&(ADest->TimeHi), ASourceBytes[6], ASourceBytes[7]);
       
     ADest->ClockSeqHi = ASourceBytes[8];
     ADest->ClockSeqLo = ASourceBytes[9];

     // copy bytes from global array,
     // to temp array
     AMem48Bytes[0] = ASourceBytes[10];
     AMem48Bytes[1] = ASourceBytes[11];
     AMem48Bytes[2] = ASourceBytes[12];
     AMem48Bytes[3] = ASourceBytes[13];
     AMem48Bytes[4] = ASourceBytes[14];
     AMem48Bytes[5] = ASourceBytes[15];

     // copy bytes from temp array,
     // to mem48 field in struct
     coremem48s__Encodebytesto48
       (&(ADest->Node), AMem48Bytes[6], AMem48Bytes[7]);
  } // if
} // func

void /* func */ coreuuids__decodestructtouuid
  (/* in */  const struct uuid128header* /* param */ ADest,
   /* out */ uuid*                       /* param */ ASource)
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (ADest != NULL) &&
    (ASource != NULL);

  if (CanContinue)
  {
     mem8_t /* var */ ADestBytes[16];
     mem8_t /* var */ AMem48Bytes[6];

     // copy structs fields to byte array
     coremem32s__Decode8to32
       (ADest->TimeLow, ADestBytes[0], ADestBytes[1], ADestBytes[2], ADestBytes[3]);
     coremem16s__Decode8to16
       (ADest->TimeMid, ADestBytes[4], ADestBytes[5]);
     coremem16s__Decode8to16
       (ADest->TimeHi, ADestBytes[6], ADestBytes[7]);
       
     ADestBytes[8] = ADest->ClockSeqHi;
     ADestBytes[9] = ADest->ClockSeqLo;

     // copy bytes from mem48 field in struct,
     // to temp array
     coremem48s__Decode48tobytes
       (&(ADest->Node), AMem48Bytes[6], AMem48Bytes[7]);

     // copy bytes from temp array,
     // to global array
     ADestBytes[10] = AMem48Bytes[0];
     ADestBytes[11] = AMem48Bytes[1];
     ADestBytes[12] = AMem48Bytes[2];
     ADestBytes[13] = AMem48Bytes[3];
     ADestBytes[14] = AMem48Bytes[4];
     ADestBytes[15] = AMem48Bytes[5];
  } // if
} // func

// ------------------

void /* func */ coreuuids__encodeuuid
  (/* out */ uuid*           /* param */ ADest,
   /* in */  const uint32_t  /* param */ ATimeLow,
   /* in */  const uint16_t  /* param */ ATimeMid,
   /* in */  const uint16_t  /* param */ ATimeHi,
   /* in */  const uint8_t   /* param */ AClockSeqHi,
   /* in */  const uint8_t   /* param */ AClockSeqLo,
   /* in */  const uint48_t* /* param */ ANode)
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (ADest != NULL) &&
    (ANode != NULL);

  if (CanContinue)
  {
     mem32_t /* var */ A = 0;
     mem32_t /* var */ B = 0;
     mem32_t /* var */ C = 0;
     mem32_t /* var */ D = 0;
  
     A = ATimeLow;
     coremem32s__Encode16to32
       (&B, ATimeMid, ATimeHi);
       
    AClockSeqHi
    AClockSeqLo
    
  } // if
} // func

void /* func */ coreuuids__decodeuuid
  (/* in */  const uuid* /* param */ ASource,
   /* out */ uint32_t*   /* param */ ATimeLo,
   /* out */ uint16_t*   /* param */ ATimeMid,
   /* out */ uint16_t*   /* param */ ATimeHi,
   /* out */ uint8_t*    /* param */ AClockSeqHi,
   /* out */ uint8_t*    /* param */ AClockSeqLo,
   /* out */ uint48_t*   /* param */ ANode)
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (ASource != NULL) &&
    (ATimeLo != NULL) &&
    (ATimeMid != NULL) &&
    (ATimeHi != NULL) &&
    (AClockSeqHi != NULL) &&
    (AClockSeqLo != NULL) &&
    (ANode != NULL);

  if (CanContinue)
  {
  	
  } // if
} // func

// ------------------

void /* func */ coreuuids__decodebytestouuid
  (/* out */ uuid*         /* param */ ADest,
   /* in */  const mem8_t  /* param */ (*) ASource[16])
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (ADest != NULL) &&
    (ASource != NULL);
    
  if (CanContinue)
  {
    coremem32s__Encode8to32
      (ADest[0], ASource[0], ASource[1], ASource[2], ASource[3]);
    coremem32s__Encode8to32
      (ADest[1], ASource[4], ASource[5], ASource[6], ASource[7]);
    coremem32s__Encode8to32
      (ADest[2], ASource[8], ASource[9], ASource[10], ASource[11]);
    coremem32s__Encode8to32
      (ADest[3], ASource[12], ASource[13], ASource[14], ASource[15]);
  } // if
} // func
 
void /* func */ coreuuids__decodeuuidtobytes
  (/* in */  const uuid* /* param */ ASource,
   /* out */ mem8_t      /* param */ (*) ADest[16])
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (ADest != NULL) &&
    (ASource != NULL);
    
  if (CanContinue)
  {
    coremem32s__Decode8to32
      (ASource[0], ADest[0], ADest[1], ADest[2], ADest[3]);
    coremem32s__Decode8to32
      (ASource[1], ADest[4], ADest[5], ADest[6], ADest[7]);
    coremem32s__Decode8to32
      (ASource[2], ADest[8], ADest[9], ADest[10], ADest[11]);
    coremem32s__Decode8to32
      (ASource[3], ADest[12], ADest[13], ADest[14], ADest[15]);
  } // if
} // func

// ------------------

void /* func */ coreuuids__encodequartets
  (/* out */ uuid*         /* param */ ADest,
   /* in */  const mem32_t /* param */ A,
   /* in */  const mem32_t /* param */ B,
   /* in */  const mem32_t /* param */ C,
   /* in */  const mem32_t /* param */ D)
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (ADest != NULL) &&
    (A != NULL) &&
    (B != NULL) &&
    (C != NULL) &&
    (D != NULL);
    
  if (CanContinue)
  {
    ADest[0] = *A;
    ADest[1] = *B;
    ADest[2] = *C;
    ADest[3] = *D;
  } // if
} // func

void /* func */ coreuuids__decodequartets
  (/* in */  const uuid* /* param */ ASource,
   /* out */ mem32_t*    /* param */ A,
   /* out */ mem32_t*    /* param */ B,
   /* out */ mem32_t*    /* param */ C,
   /* out */ mem32_t*    /* param */ D)
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (ASource != NULL) &&
    (A != NULL) &&
    (B != NULL) &&
    (C != NULL) &&
    (D != NULL);
    
  if (CanContinue)
  {
    *A = ASource[0];
    *B = ASource[1];
    *C = ASource[2];
    *D = ASource[3];
  } // if
} // func
  
// ------------------

bool /* func */ coreuuids__tryduplicate
  (/* in */  const pointer*  /* param */ ASource,
   /* out */       pointer** /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (ASource != NULL) && (ADest != NULL);
  if (Result)
  {
     *ADest =
       corememory__duplicatecopy
         (ASource, sizeof (uuid_t));,
  }
  // ---
  return Result;
} // func

// ------------------

pointer* /* func */ coreuuids__duplicate
  (/* in */ const pointer* /* param */ AItem)
{
  pointer* /* var */ Result = NULL;
 // ---
  
  /* discard */ coreuuids__tryduplicate
    (AItem, AResult);
     
 // ---
  return Result;
} // func

// ------------------

bool /* func */ coreuuids__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
 // ---
 
 Result = 
   corememory__safeisclear
     (AValue, sizeof(uuid));
 // ---
  return Result;
} // func

bool /* func */ coreuuids__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
 // ---
     
 corememory__safeclear
   (AValue, sizeof(uuid));
     
 // ---
  return Result;
} // func

void /* func */ coreuuids__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
   corememory__safeclear
     (AValue, sizeof(uuid));
} // func


 // ...

// ------------------

/* override */ const ansinullstring* /* func */ coresystem__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "coreuuids";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coreintegers__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreintegers__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// }// namespace coreuuids