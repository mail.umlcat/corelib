/** Module: "coredbluuids.h"
 ** Descr.: "Universal Unique Identifier"
 ** implementation library."
 **/
 
// namespace coredbluuids {
 
// ------------------
 
#ifndef COREDBLUUIDS__H
#define COREDBLUUIDS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------
 
#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

// unique identifier
typedef
  uint32_t    /* as */ doubleuuid[8];
typedef
  doubleuuid  /* as */ coreuuids_doubleuuid;

typedef
  doubleuuid  /* as */ doubleuuid_t;
typedef
  doubleuuid* /* as */ doubleuuid_p;

typedef
  doubleuuid  /* as */ coreuuids_doubleuuid_t;
typedef
  doubleuuid* /* as */ coreuuids_doubleuuid_p;

// ------------------

bool /* func */ coredbluuids__iscleared
  (/* in */ const uuid* /* param */ ASource);
 
bool /* func */ coredbluuids__areequal
  (/* in */ const uuid* /* param */ A, B);

// ------------------

void /* func */ coredbluuids__generate
  (/* out */ dbluuid* /* param */ ADest);;

void /* func */ coredbluuids__clear
  (/* out */ dbluuid* /* param */ ADest);

// ------------------

void /* func */ coredbluuids__encodeuuidtostruct
  (/* in */  const dbluuid*    /* param */ ASource,
   /* out */ dbluuid128struct* /* param */ ADest);

void /* func */ coredbluuids__decodestructtouuid
  (/* in */  const dbluuid128struct* /* param */ ADest,
   /* out */ dbluuid*                /* param */ ASource);

// ------------------

void /* func */ coredbluuids__encodestruct
  (/* out */ dbluuid*           /* param */ ADest,
   /* in */  const uint32_t  /* param */ ATimeLow,
   /* in */  const uint16_t  /* param */ ATimeMid,
   /* in */  const uint16_t  /* param */ ATimeHi,
   /* in */  const uint8_t   /* param */ AClockSeqHi,
   /* in */  const uint8_t   /* param */ AClockSeqLo,
   /* in */  const uint48_t* /* param */ ANode);

void /* func */ coredbluuids__decodestruct
  (/* in */  const dbluuid* /* param */ ASource,
   /* out */ uint32_t*   /* param */ ATimeLo,
   /* out */ uint16_t*   /* param */ ATimeMid,
   /* out */ uint16_t*   /* param */ ATimeHi,
   /* out */ uint8_t*    /* param */ AClockSeqHi,
   /* out */ uint8_t*    /* param */ AClockSeqLo,
   /* out */ uint48_t*   /* param */ ANode);

// ------------------

void /* func */ coredbluuids__decodebytestouuid
  (/* out */ dbluuid*         /* param */ ADest,
   /* in */  const mem8_t  /* param */ (*); ASource[16]);

void /* func */ coredbluuids__decodeuuidtobytes
  (/* in */  const dbluuid* /* param */ ASource,
   /* out */ mem8_t      /* param */ (*); ADest[16]);

// ------------------

void /* func */ coredbluuids__encodequartets
  (/* out */ dbluuid*         /* param */ ADest,
   /* in */  const mem32_t /* param */ A,
   /* in */  const mem32_t /* param */ B,
   /* in */  const mem32_t /* param */ C,
   /* in */  const mem32_t /* param */ D);

void /* func */ coredbluuids__decodequartets
  (/* in */  const dbluuid* /* param */ ASource,
   /* out */ mem32_t*    /* param */ A,
   /* out */ mem32_t*    /* param */ B,
   /* out */ mem32_t*    /* param */ C,
   /* out */ mem32_t*    /* param */ D);

// ------------------

bool /* func */ coredbluuids__tryduplicate
  (/* in */  const pointer*  /* param */ ASource,
   /* out */       pointer** /* param */ ADest);

// ------------------

pointer* /* func */ coredbluuids__duplicate
  (/* in */ const pointer* /* param */ AItem);

// ------------------

bool /* func */ coredbluuids__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coredbluuids__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coredbluuids__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);


 // ...

// ------------------

/* override */ const ansinullstring* /* func */ coresystem__modulename
  ( noparams );

/* override */ const ansinullstring* /* func */ coresystem__modulename
  ( noparams );

/* override */ int /* func */ coreintegers__setup
  ( noparams );

/* override */ int /* func */ coreintegers__setoff
  ( noparams );

// ------------------
 
#endif // COREDBLUUIDS__H

// }// namespace coredbluuids