/** Module: "coreansistrconstlists.h"
 ** Descr.: "..."
 **/
 
// namespace coreansistrconstlists {
 
// ------------------
 
#ifndef COREANSISTRCONSTLISTS__H
#define COREANSISTRCONSTLISTS__H
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"
#include "coreiterators.h"
#include "coreansistrconstbuffers.h"
#include "coreptrlists.h"

// ------------------

struct ansistringconstlistheader
{
  stringcasemodes        /* var */ CaseMode;

  ansistringconstbuffer* /* var */ Buffer;

  pointerlist            /* var */ Items;

  // how many items are currently stored
  count_t                /* var */ ItemsCurrentCount;
  
  // indicates iterator related objects
  count_t                /* var */ IteratorCount;
  
  // ...
} ;

typedef
  ansistringconstlistheader /* as */ ansistringconstlist;
typedef
  ansistringconstlist       /* as */ ansistringconstlist_t;
typedef
  ansistringconstlist*      /* as */ ansistringconstlist_p;

// ------------------

struct ansistringconstlistiteratorheader
{
  listdirections  /* var */ Direction;
  
  count_t         /* var */ ItemsCount;
  size_t          /* var */ ItemsSize;
  
  ansinullstring* /* var */ CurrentItem;
} ;

typedef
  ansistringconstlistiteratorheader /* as */ ansistringconstlistiterator;
typedef
  ansistringconstlistiterator       /* as */ ansistringconstlistiterator_t;
typedef
  ansistringconstlistiterator*      /* as */ ansistringconstlistiterator_p;

// ------------------

ansistringconstlist* /* func */ coreansistringconstlists__createlist
  ( noparams );

ansistringconstlist* /* func */ coreansistringconstlists__createlistbybuffersize
  (/* in */ size_t* /* param */ ABufferSize);

void /* func */ coreansistringconstlists__droplist
  (/* out */ ansistringconstlist** /* param */ AList);

// ------------------

ansistringconstlistiterator* /* func */ coreansistringconstlists__createiteratorforward
  (/* in */ ansistringconstlist* /* param */ AList);

ansistringconstlistiterator* /* func */ coreansistringconstlists__createiteratorbackward
  (/* in */ ansistringconstlist* /* param */ AList);

void /* func */ coreansistringconstlists__dropiterator
  (/* out */ ansistringconstlistiterator** /* param */ AIterator);

// ------------------

bool /* func */ coreansistringconstlists__isdone
  (/* in */ const ansistringconstlistiterator* /* param */ AIterator);

void /* func */ coreansistringconstlists__movenext
  (/* inout */ ansistringconstlistiterator* /* param */ AIterator);

void /* func */ coreansistringconstlists__readitem
  (/* inout */ ansistringconstlistiterator* /* param */ AIterator
   /* out */   ansinullstring*              /* param */ AItem);

// ------------------

bool /* func */ coreansistringconstlists__tryfound
  (/* in */  const ansinullstring* /* param */ ATextPtr,
   /* out */ bool*                 /* param */ ATextFound);

bool /* func */ coreansistringconstlists__tryptrof
  (/* in */  const ansinullstring* /* param */ ASourceTextPtr,
  (/* out */ ansinullstring*       /* param */ ADestTextPtr);

bool /* func */ coreansistringconstlists__found
  (/* in */ const ansinullstring* /* param */ AText);

const ansinullstring* /* func */ coreansistringconstlists__ptrof
  (/* in */ const ansinullstring* /* param */ AText);

// ------------------

bool /* func */ coreansistrconstlists__tryadd 
  (/* inout */ ansistringconstlist*  /* param */ AList,
   /* in */    const ansinullstring* /* param */ ASourcePtr,
   /* out */   ansinullstring**      /* param */ ADestPtr);

bool /* func */ coreansistrconstlists__tryaddcount
  (/* inout */ ansistringconstlist*  /* param */ AList,
   /* in */    const ansinullstring* /* param */ ASourcePtr,
   /* in */    const size_t          /* param */ ASourceSize,
   /* out */   ansinullstring**      /* param */ ADestPtr);

const ansinullstring* /* func */ coreansistrconstlists__add
  (/* inout */ ansistringconstlist* /* param */ AList,
   /* in */ const ansinullstring*   /* param */ AItem);

const ansinullstring* /* func */ coreansistrconstlists__addcount
  (/* inout */ ansistringconstlist* /* param */ AList,
   /* in */ const ansinullstrinh*   /* param */ AItemPtr,
   /* in */ const size_t            /* param */ AItemSize);

// ------------------

bool /* func */ coreansistringconstlists__tryclear
  (/* inout */ ansistringconstlist* /* param */ AList);

bool /* func */ coreansistringconstlists__isempty
  (/* in */ const ansistringconstlist* /* param */ AList);

bool /* func */ coreansistringconstlists__hasitems
  (/* in */ const ansistringconstlist* /* param */ AList);

void /* func */ coreansistringconstlists__clear
  (/* inout */ ansistringconstlist* /* param */ AList);

// ------------------

count_t /* func */ coreansistrconstlists__getcount
  (/* in */ const ansistringconstlist* /* param */ AList,

size_t /* func */ coreansistrconstlists__getbuffersize
  (/* in */ const ansistringconstlist* /* param */ AList);

bool /* func */ coreansistrconstlists__trybufferresize
  (/* inout */ ansistringconstlist* /* param */ AList,
   /* in */    const count_t        /* param */ ANewSize);

void /* func */ coreansistrconstlists__bufferesize
  (/* inout */ ansistringconstlist* /* param */ AList,
   /* in */    const count_t        /* param */ ANewSize);

// ------------------


 // ...
 
// ------------------
 
#endif // COREANSISTRCONSTLISTS__H

// } // namespace coreansistrconstlists