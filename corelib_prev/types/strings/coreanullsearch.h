/** Module: "coreanullsearch.h"
 ** Descr.: "ANSI Null Character Terminated Strings,"
 **         "Search Operations."
 **/

// namespace coreanullsearch {

// ------------------
 
#ifndef COREANULLSEARCH__H
#define COREANULLSEARCH__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansicharptrs.h"
#include "coreansinullsets.h"
#include "coreacharidxptrs.h"
#include "coreacharszptrs.h"

// ------------------

bool /* func */ coreanullsearch__isempty
  (/* in */ const struct ansicharsizedptr* /* param */ ASource);

// ------------------

bool /* func */ coreanullsearch__trysearchnullmarker
  (/* in */  /* restricted */ noconst struct ansicharsizedptr* /* param */ ASource,
   /* out */ /* restricted */ noconst struct ansicharidxptr*   /* param */ ADest);

bool /* func */ coreanullsearch__trysearchlastchar
  (/* in */  /* restricted */ noconst struct ansicharsizedptr* /* param */ ASource,
   /* out */ /* restricted */ noconst struct ansicharidxptr*   /* param */ ADest);

// ------------------

bool /* func */ coreanullsearch__startswithequal
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const struct ansicharsizedptr* /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest);

bool /* func */ coreanullsearch__finisheswithequal
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const struct ansicharsizedptr* /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest);

// ------------------

bool /* func */ coreanullsearch__startswithsame
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const struct ansicharsizedptr* /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest);

bool /* func */ coreanullsearch__finisheswithsame
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const struct ansicharsizedptr* /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest);

// ------------------

bool /* func */ coreansearch__tryfirstptrofchar
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  const ansichar                                  /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest);

bool /* func */ coreansearch__tryfirstptrofcharptr
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const ansichar                 /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest);

bool /* func */ coreansearch__tryfirstptrofcharset
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const struct ansicharsizedptr* /* param */ ANeedleSet,
   /* out */ /* restricted */ noconst struct ansicharidxptr*         /* param */ ADest);

bool /* func */ coreansearch__tryfirstptrofstrequal
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const struct ansicharsizedptr* /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest);

bool /* func */ coreansearch__tryfirstptrofstrsame
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const struct ansicharsizedptr* /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest);

// ------------------

bool /* func */ coreansearch__trylastptrofchar
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  const ansichar                                  /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest);

bool /* func */ coreansearch__trylastptrofcharptr
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const ansichar*                /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest);

bool /* func */ coreansearch__trylastptrofcharset
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const struct ansicharsizedptr* /* param */ ANeedleSet,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest);

bool /* func */ coreansearch__trylastptrofstrequal
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const struct ansicharsizedptr* /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest);

bool /* func */ coreansearch__trylastptrofstrsame
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const struct ansicharsizedptr* /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest);

// ------------------




 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coreanullsearch__modulename
  ( noparams );

/* override */ int /* func */ coreanullsearch__setup
  ( noparams );

/* override */ int /* func */ coreanullsearch__setoff
  ( noparams );

// ------------------

#endif // COREANULLSEARCH__H

// } // namespace coreanullsearch