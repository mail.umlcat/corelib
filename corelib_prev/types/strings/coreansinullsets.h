/** Module: "coreansinullsets.h"
 ** Descr.: "ANSI Character Set implemented,"
 **         "with NULL marker terminated strings, library."
 **/

// namespace coreansinullsets {
 
// ------------------
 
#ifndef COREANSINULLSETS__H
#define COREANSINULLSETS__H

// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
//#include "coreuuids.h"
//#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"

// ------------------

#define ANSICHARSETMAXSIZE 256

typedef
  ansinullstring  /* as */ ansicharset;

// ------------------

/* inline */ bool /* func */ coreansinullsets__isempty
  (/* in */ const ansicharset* /* param */ ASet);

/* inline */ bool /* func */ coreansinullsets__isemptysize
  (/* in */ const ansicharset* /* param */ ASetBuffer,
   /* in */ const size_t       /* param */ ASetSize);

bool /* func */ coreansinullsets__ismember
  (/* in */ const ansicharset* /* param */ ASet,
   /* in */ const ansichar     /* param */ AItem);

bool /* func */ coreansinullsets__ismemberptr
  (/* in */ const ansicharset* /* param */ ASet,
   /* in */ const ansichar*    /* param */ AItem);

bool /* func */ coreansinullsets__ismembersize
  (/* in */ const ansicharset* /* param */ ASetBuffer,
   /* in */ const size_t       /* param */ ASetSize,
   /* in */ const ansichar     /* param */ AItem);

bool /* func */ coreansinullsets__ismemberptrsize
  (/* in */ const ansicharset* /* param */ ASetBuffer,
   /* in */ const size_t       /* param */ ASetSize,
   /* in */ const ansichar*    /* param */ AItem);

//void /* func */ coreansinullsets__dealloc
//  (/* out */ pointer** /* param */ ADest,
//   /* in */  size_t    /* param */ ASize);

 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coreansinullsets__modulename
  ( noparams );

/* override */ int /* func */ coreansinullsets__setup
  ( noparams );

/* override */ int /* func */ coreansinullsets__setoff
  ( noparams );

// ------------------

#endif // COREANSINULLSETS__H

// } // namespace coreansinullsets