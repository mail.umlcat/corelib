/** Module: "coreansipasstrs.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coreansipasstrs {
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansipasstrs.h"
 
 // ------------------

bool /* func */ coreansipastrs__safeareequal
  (/* in */ const /* restricted */ ansipascalstring* /* param */ A,
   /* in */ const /* restricted */ ansipascalstring* /* param */ B)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreansipastrs__safearesame
  (/* in */ const /* restricted */ ansipascalstring* /* param */ A,
   /* in */ const /* restricted */ ansipascalstring* /* param */ B)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

comparison /* func */ coreansipasstrs__safecompare
  (/* in */ const /* restricted */ ansipascalstring* /* param */ A,
   /* in */ const /* restricted */ ansipascalstring* /* param */ B)
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func

 // ------------------
 
void /* func */ coreansipasstrs__clear
  (/* inout */ ansipascalstring* /* param */ ADest);
{
  // ...
} // func

void /* func */ coreansipasstrs__assignconst
  (/* out */ /* restricted */ ansipascalstring*     /* param */ ADest,
   /* in */  /* restricted */ const ansinullstring* /* param */ ASource)
{
  // ...
} // func

void /* func */ coreansipasstrs__concatconst
  (/* inout */ /* restricted */ ansipascalstring*   /* param */ ADest,
   /* in */  /* restricted */ const ansinullstring* /* param */ ASource)
{
  // ...
} // func

void /* func */ coreansipasstrs__concatchar
  (/* inout */ ansipascalstring* /* param */ ADest,
   /* in */ const ansichar       /* param */ ASource)
{
  // ...
} // func

void /* func */ coreansipasstrs__assignpascal
  (/* out */ /* restricted */ ansipascalstring*       /* param */ ADest,
   /* in */  /* restricted */ const ansipascalstring* /* param */ ASource)
{
  // ...
} // func

void /* func */ coreansipasstrs__concatpascal
  (/* inout */ /* restricted */ ansipascalstring*     /* param */ ADest,
   /* in */  /* restricted */ const ansipascalstring* /* param */ ASource)
{
  // ...
} // func

// ------------------

void /* func */ coreansipasstrs__uppercasechange
  (/* inout */ ansipascalstring* /* param */ ADestBuffer)
{
  // ...
} // func

void /* func */ coreansipasstrs__lowercasechange
  (/* inout */ ansipascalstring* /* param */ ADestBuffer)
{
  // ...
} // func

void /* func */ coreansipasstrs__uppercasecopy
  (/* out */ /* restricted */ ansipascalstring*       /* param */ ADest,
   /* in */  /* restricted */ const ansipascalstring* /* param */ ASource)
{
  // ...
} // func

void /* func */ coreansipasstrs__lowercasecopy
  (/* out */ /* restricted */ ansipascalstring*       /* param */ ADest,
   /* in */  /* restricted */ const ansipascalstring* /* param */ ASource)
{
  // ...
} // func

// ------------------

void /* func */ coreansipasstrs__leftcopy
  (/* out */ /* restricted */ ansipascalstring*       /* param */ ADestBuffer,
   /* in */  /* restricted */ const ansipascalstring* /* param */ ASourceBuffer,
   /* in */ const size_t                              /* param */ ADestSize)
{
  // ...
} // func

void /* func */ coreansipasstrs__rightcopy
  (/* out */ /* restricted */ ansipascalstring*       /* param */ ADestBuffer,
   /* in */  /* restricted */ const ansipascalstring* /* param */ ASourceBuffer,
   /* in */ const size_t                              /* param */ ADestSize)
{
  coresystem__nothing();
} // func

 // ------------------

bool /* func */ coreansipasstrs__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreansipasstrs__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreansipasstrs__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

 // ------------------

 
 
 
 // ...

// } // namespace coreansipasstrs