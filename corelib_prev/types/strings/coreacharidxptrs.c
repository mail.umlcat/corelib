/** Module: "coreacharidxptrs.c"
 ** Descr.: "A composite pointer and index operations library,"
 **         "also known as a 'fat pointer',"
 **         "the base data type is 'ansichar' ."
 **/

// namespace coreacharidxptrs {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
//#include "corefunctors.h"
//#include "coreuuids.h"
//#include "coreoswarnings.h"

// ------------------

//#include "coreiterators.h"

// ------------------
 
#include "coreacharidxptrs.h"

// ------------------

bool /* func */ coreacharidxptrs__isassigned
  (/* in */ const struct ansicharidxptr* /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
 
  Result =
    (ASource != NULL) &&
    (ASource->ItemPtr != NULL);
    (ASource->ItemIndex >= 0);

  // ---
  return Result;
} // func

// ------------------

/* friend */ bool /* func */ coreacharidxptrs__areequal
  (/* in */ const struct ansicharidxptr* /* param */ A,
   /* in */ const struct ansicharidxptr* /* param */ B)
{
  bool /* var */ Result = false;
  // ---
 
  Result =
    (A != NULL) && (B != NULL) &&
    (A->ItemPtr == B->ItemPtr) &&
    (A->ItemIndex == B->ItemIndex);

  // ---
  return Result;
} // func

/* friend */ bool /* func */ coreacharidxptrs__areequalonlyptr
  (/* in */ const struct ansicharidxptr* /* param */ A,
   /* in */ const struct ansicharidxptr* /* param */ B)
{
  struct ansicharidxptr* /* var */ Result = NULL;
  // ---
 
  Result =
    (A != NULL) && (B != NULL) &&
    (A->ItemPtr == B->ItemPtr);

  // ---
  return Result;
} // func

// ------------------

ansicharidxptr* /* func */ coreacharidxptrs__new
  ( noparams )
{
  struct ansicharidxptr* /* var */ Result = NULL;
  // ---
 
  Result =
    corememory__clearallocate(sizeof(ansicharidxptr));

  // ---
  return Result;
} // func

void /* func */ coreacharidxptrs__drop
  (/* out */ nonconst struct ansicharidxptr** /* param */ ADest)
{
  if (ADest != NULL)
  {
    corememory__deallocate(ADest);
  } // if
} // func

// ------------------

void /* func */ coreacharidxptrs__pack
  (/* out */ nonconst struct ansicharidxptr** /* param */ ADest,
   /* in */  const pointer*   /* param */ ASourcePtr,
   /* in */  const index_t    /* param */ ASourceIndex)
{
  if (ADest != NULL)
  {
    (*ADest)->ItemPtr   = ASourcePtr;
    (*ADest)->ItemIndex = ASourceIndex;
  } // if
} // func

void /* func */ coreacharidxptrs__unpack
  (/* in */  const struct ansicharidxptr* /* param */ ASource,
   /* out */ pointer**             /* param */ ADestPtr,
   /* out */ index_t*              /* param */ ADestIndex)
{
  if (ASource != NULL)
  {
    ADestPtr   = ASource->ItemPtr;
    ADestIndex = ASource->ItemIndex;
  } // if
} // func

// ------------------

void /* func */ coreacharidxptrs__clear
  (/* inout */ nonconst struct ansicharidxptr* /* param */ ADest)
{
  if (ADest != NULL)
  {
    ADest->ItemPtr   = NULL;
    ADest->ItemIndex = 0;
  } // if
} // func

// ------------------

ansicharidxptr* /* func */ coreacharidxptrs__pred
  (/* in */ const struct ansicharidxptr* /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---

  if (coreacharidxptrs__isassigned(ASource))
  {
    Result =
      coreacharidxptrs__new();

    Result->ItemPtr   = (ASource->ItemPtr - 1);
    Result->ItemIndex = (ASource->ItemIndex - 1);
  } // if

  // ---
  return Result;
} // func

ansicharidxptr* /* func */ coreacharidxptrs__predbycount
  (/* in */ const struct ansicharidxptr* /* param */ ASource,
   /* in */ const count_t         /* param */ ACount)
{
  bool /* var */ Result = false;
  // ---

  if (coreacharidxptrs__isassigned(ASource) &&
      (ACount >= 0))
  {
    Result =
      coreacharidxptrs__new();

    Result->ItemPtr   = (ASource->ItemPtr - ACount);
    Result->ItemIndex = (ASource->ItemIndex - ACount);
  } // if

  // ---
  return Result;
} // func

ansicharidxptr* /* func */ coreacharidxptrs__succ
  (/* in */ const struct ansicharidxptr* /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---

  if (coreacharidxptrs__isassigned(ASource))
  {
    Result =
      coreacharidxptrs__new();

    Result->ItemPtr   = (ASource->ItemPtr +  1);
    Result->ItemIndex = (ASource->ItemIndex + 1);
  } // if

  // ---
  return Result;
} // func

ansicharidxptr* /* func */ coreacharidxptrs__succbycount
  (/* in */ const struct ansicharidxptr* /* param */ ASource,
   /* in */ const count_t         /* param */ ACount)
{
  bool /* var */ Result = false;
  // ---

  if (coreacharidxptrs__isassigned(ASource))
  {
    Result =
      coreacharidxptrs__new();

    Result->ItemPtr   = (ASource->ItemPtr + 1);
    Result->ItemIndex = (ASource->ItemIndex + 1);
  } // if

  // ---
  return Result;
} // func

// ------------------

void /* func */ coreacharidxptrs__inc
  (/* inout */ nonconst struct ansicharidxptr* /* param */ ADest)
{
  if (coreacharidxptrs__isassigned(ADest))
  {
    ADest->ItemPtr   = (ADest->ItemPtr + 1);
    ADest->ItemIndex = (ADest->ItemIndex + 1);
  } // if
} // func

void /* func */ coreacharidxptrs__incbycount
  (/* inout */ nonconst struct ansicharidxptr* /* param */ ADest,
   /* in */    const count_t   /* param */ ACount)
{
  if (coreacharidxptrs__isassigned(ADest))
  {
    ADest->ItemPtr   = (ADest->ItemPtr + ACount);
    ADest->ItemIndex = (ADest->ItemIndex + ACount);
  } // if
} // func

void /* func */ coreacharidxptrs__dec
  (/* inout */ nonconst struct ansicharidxptr* /* param */ ADest)
{
  if (coreacharidxptrs__isassigned(ADest))
  {
    ADest->ItemPtr   = (ADest->ItemPtr - 1);
    ADest->ItemIndex = (ADest->ItemIndex - 1);
  } // if
} // func

void /* func */ coreacharidxptrs__decbycount
  (/* inout */ nonconst struct ansicharidxptr* /* param */ ADest,
   /* in */    const count_t   /* param */ ACount)
{
  if (coreacharidxptrs__isassigned(ADest))
  {
    ADest->ItemPtr   = (ADest->ItemPtr - ACount);
    ADest->ItemIndex = (ADest->ItemIndex - ACount);
  } // if
} // func

// ------------------

bool /* func */ coreacharidxptrs__trysearchnullmarker
  (/* in */  /* restricted */ ansinullstring*   /* param */ ASource,
   /* out */ /* restricted */ nonconst struct ansicharsizedptr* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  size_t           /* var */ ADestSize;

  struct ansicharsizedptr /* var */ ASourcePack;

  ASourceBuffer.ItemSize = 1024;
  ASourceBuffer.ItemPtr  = ASource;

  Result =
    coreacharidxptrs__trysearchnullmarkersize
      (ASourceBuffer, ADest);

  // ---
  return Result;
} // func

bool /* func */ coreacharidxptrs__trysearchnullmarkersize
  (/* in */  /* restricted */ nonconst struct ansicharsizedptr* /* param */ ASource,
   /* out */ /* restricted */ nonconst struct ansicharidxptr*   /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  Result =
    ((! coreansinullstrs__isemptysize(ASourceBuffer, ASourceSize) &&
     (ADestBuffer != NULL) && (ADestSize != NULL));
  if (Result)
  {
    ansichar* /* var */ EachPtr = NULL;

    size_t    /* var */ EachIndex;

    bool      /* var */ Found;
    bool      /* var */ CanContinue;

    // clear result
    *ADestBuffer = NULL;
    *ADestSize   = NULL;

    // prepare local variables
    EachPtr = 
      (ansinullstring*) ASourceBuffer;
    EachIndex = 0;

    Found       = false;
    CanContinue = false;

    // find null character marker
    while (CanContinue)
    {
      Found =
        (*EachPtr == ansinullchar);
      if (! Found)
      {
        EachPtr++;
        EachIndex++;
      }

      CanContinue =
        (! Found) && (EachIndex <= ASourceSize));
    } // while

    Result = Found;
    if (Result)
    {
      *ADest      = EachPtr;
      *ADestIndex = EachIndex;
    }
  } // if

  // ---
  return Result;
} // func

// ------------------


 // ...

// ------------------

/* override */ const ansinullstring* /* func */ coreacharidxptrs__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "coreacharidxptrs";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coreacharidxptrs__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreacharidxptrs__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coreacharidxptrs