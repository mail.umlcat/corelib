/** Module: "coreansistrconstbuffers.h"
 ** Descr.: "..."
 **/
 
// namespace coreansistrconstbuffers {
 
// ------------------
 
#ifndef COREANSISTRCONSTBUFFERS__H
#define COREANSISTRCONSTBUFFERS__H
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"

// ------------------

struct stringconstbufferheader
{
  // pointer to start of items
  ansinullstring* /* var */ ItemsPtr;
  
  // global size of all items
  size_t          /* var */ ItemsSize;

  // how many items are currently stored
  count_t         /* var */ ItemsCount;

  // ...
} ;

typedef
  stringconstbufferheader /* as */ stringconstbuffer;
typedef
  stringconstbuffer       /* as */ stringconstbuffer_t;
typedef
  stringconstbuffer*      /* as */ stringconstbuffer_p;

// ------------------

void /* func */ coreansistrconstbuffers__createlist
 (/* in */ const size_t /* param */ ABufferSize);

void /* func */ coreansistrconstbuffers__droplist
  (/* inout */ stringconstbuffer** /* param */ AList);

// ------------------

bool /* func */ coreansistrconstbuffers__tryadd
  (/* inout */ stringconstbuffer*      /* param */ AList,
   /* in */    const ansinullstring* /* param */ ASourcePtr,
   /* out */   ansinullstring**      /* param */ ADestPtr);

bool /* func */ coreansistrconstbuffers__tryaddcount
  (/* inout */ stringconstbuffer*      /* param */ AList,
   /* in */    const ansinullstring* /* param */ ASourcePtr,
   /* in */    const size_t          /* param */ ASourceSize,
   /* out */   ansinullstring**      /* param */ ADestPtr);

const ansinullstring* /* func */ coreansistrconstbuffers__add
  (/* inout */ stringconstbuffer* /* param */ AList,
   /* in */ const ansinullstring* /* param */ AItem);

const ansinullstring* /* func */ coreansistrconstbuffers__addcount
  (/* inout */ stringconstbuffer* /* param */ AList,
   /* in */ const ansinullstrinh* /* param */ AItemPtr,
   /* in */ const size_t          /* param */ AItemSize);

// ------------------

count_t /* func */ coreansistrconstbuffers__getcount
  (/* in */ const stringconstbuffer* /* param */ AList);

size_t /* func */ coreansistrconstbuffers__getsize
  (/* in */ const stringconstbuffer* /* param */ AList);

bool /* func */ coreansistrconstbuffers__tryresize
  (/* inout */ stringconstbuffer* /* param */ AList,
   /* in */    const count_t      /* param */ ANewSize);

void /* func */ coreansistrconstbuffers__resize
  (/* inout */ stringconstbuffer* /* param */ AList,
   /* in */    const count_t      /* param */ ANewSize);

// ------------------


 // ...
 
// ------------------
 
#endif // COREANSISTRCONSTBUFFERS__H

// } // namespace coreansistrconstbuffers