/** Module: "coreacharidxptrs.h"
 ** Descr.: "A composite pointer and index operations library,"
 **         "also known as a 'fat pointer',"
 **         "the base data type is 'ansichar' ."
 **/

// namespace coreacharidxptrs {
 
// ------------------
 
#ifndef COREACHARIDXPTRS__H
#define COREACHARIDXPTRS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
//#include "corefunctors.h"
//#include "coreuuids.h"
//#include "coreoswarnings.h"

// ------------------

//#include "coreiterators.h"

// ------------------

struct struct ansicharidxptr
{
  ansichar* /* var */ ItemPtr;
  index_t   /* var */ ItemIndex;
} ;

typedef
  struct ansicharidxptr    /* as */ nonconst struct ansicharidxptr_t;
typedef
  struct ansicharidxptr_t* /* as */ nonconst struct ansicharidxptr_p;

typedef
  struct ansicharidxptr_t /* as */ coreacharidxptrs__ansicharidxptr_t;
typedef
  struct ansicharidxptr_p /* as */ coreacharidxptrs__ansicharidxptr_p;

// ------------------

bool /* func */ coreacharidxptrs__isassigned
  (/* in */ const struct ansicharidxptr* /* param */ ASource);

// ------------------

/* friend */ bool /* func */ coreacharidxptrs__areequal
  (/* in */ const struct ansicharidxptr* /* param */ A,
   /* in */ const struct ansicharidxptr* /* param */ B);

/* friend */ bool /* func */ coreacharidxptrs__areequalonlyptr
  (/* in */ const struct ansicharidxptr* /* param */ A,
   /* in */ const struct ansicharidxptr* /* param */ B);

// ------------------

ansicharidxptr* /* func */ coreacharidxptrs__new
  ( noparams );

void /* func */ coreacharidxptrs__drop
  (/* out */ nonconst struct ansicharidxptr** /* param */ ADest);

// ------------------

void /* func */ coreacharidxptrs__pack
  (/* out */ nonconst struct ansicharidxptr** /* param */ ADest,
   /* in */  const pointer*   /* param */ APtr,
   /* in */  const index_t    /* param */ AIndex);

void /* func */ coreacharidxptrs__unpack
  (/* in */  const struct ansicharidxptr* /* param */ ASource,
   /* out */ pointer**             /* param */ APtr,
   /* out */ index_t               /* param */ AIndex);

// ------------------

void /* func */ coreacharidxptrs__clear
  (/* inout */ nonconst struct ansicharidxptr* /* param */ ADest);

// ------------------

ansicharidxptr* /* func */ coreacharidxptrs__pred
  (/* in */ const struct ansicharidxptr* /* param */ ASource);

ansicharidxptr* /* func */ coreacharidxptrs__predbycount
  (/* in */ const struct ansicharidxptr* /* param */ ASource,
   /* in */ const count_t         /* param */ ACount);

ansicharidxptr* /* func */ coreacharidxptrs__succ
  (/* in */ const struct ansicharidxptr* /* param */ ASource);

ansicharidxptr* /* func */ coreacharidxptrs__succbycount
  (/* in */ const struct ansicharidxptr* /* param */ ASource,
   /* in */ const count_t         /* param */ ACount);

// ------------------

void /* func */ coreacharidxptrs__inc
  (/* inout */ nonconst struct ansicharidxptr* /* param */ ADest);

void /* func */ coreacharidxptrs__incbycount
  (/* inout */ nonconst struct ansicharidxptr* /* param */ ADest,
   /* in */    const count_t   /* param */ ACount);

void /* func */ coreacharidxptrs__dec
  (/* inout */ nonconst struct ansicharidxptr* /* param */ ADest);

void /* func */ coreacharidxptrs__decbycount
  (/* inout */ nonconst struct ansicharidxptr* /* param */ ADest,
   /* in */    const count_t   /* param */ ACount);

// ------------------

bool /* func */ coreacharidxptrs__trysearchnullmarker
  (/* in */  /* restricted */ ansinullstring*   /* param */ ASource,
   /* out */ /* restricted */ nonconst struct ansicharsizedptr* /* param */ ADest);

bool /* func */ coreacharidxptrs__trysearchnullmarkersize
  (/* in */  /* restricted */ nonconst struct ansicharsizedptr* /* param */ ASource,
   /* out */ /* restricted */ nonconst struct ansicharidxptr*   /* param */ ADest);

// ------------------




 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coreacharidxptrs__modulename
  ( noparams );

/* override */ int /* func */ coreacharidxptrs__setup
  ( noparams );

/* override */ int /* func */ coreacharidxptrs__setoff
  ( noparams );

// ------------------

#endif // COREACHARIDXPTRS__H

// } // namespace coreacharidxptrs