/** Module: "coremarksearch.c"
 ** Descr.: "ANSI Marker Character Pair Strings,"
 **         "Search Operations."
 **/

// namespace coremarksearch {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------
 
#include "coremarksearch.h"
 
// ------------------

bool /* func */ coreanullsearch__atstart
  (/* in */ const struct ansicharsizedptr* /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
 
  Result =
    (ASource != NULL) &&
    (ASource->ItemPtr != NULL) &&
    (ASource->ItemIndex >= 0) &&
    (*(Source->ItemPtr) == STARTMARKER);

  // ---
  return Result;
} // func

bool /* func */ coreanullsearch__atfinish
  (/* in */ const struct ansicharsizedptr* /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
 
  Result =
    (ASource != NULL) &&
    (ASource->ItemPtr != NULL) &&
    (ASource->ItemIndex >= 0) &&
    (*(Source->ItemPtr) == FINISHMARKER);

  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreamarksearch__trysearchstartmarker
  (/* in */  /* restricted */ noconst struct ansicharsizedptr* /* param */ ASource,
   /* out */ /* restricted */ noconst struct ansicharidxptr*   /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (coreacharszptrs__isassigned(ASource)) &&
    (ADest != NULL);
  if (Result)
  {
    ansichar* /* var */ EachPtr = NULL;

    size_t    /* var */ EachIndex;

    bool      /* var */ Found;
    bool      /* var */ CanContinue;
    bool      /* var */ OutOfBounds;

    // ---

    // clear result
    coreacharidxptrs__clear
      (ADest);

    // prepare local variables
    EachPtr = 
      (ansichar*) ASource->ItemPtr;
    EachIndex =
      ASource->ItemSize;

    OutOfBounds = false;
    Found       = false;
    CanContinue = true;

    // move backward and find
    // start character marker
    while (CanContinue)
    {
      Found =
        (*EachPtr == STARTMARKER);
      if (! Found)
      {
        // move pointers backwards
        EachPtr--;
        EachIndex--;
      }

      // check it doesn't go out if the string
      OutOfBounds =
        (EachIndex < 0);

      CanContinue =
        ((! Found) && (! OutOfBounds));
    } // while

    Result = Found;
    if (Result)
    {
      coreacharidxptrs__pack
        (&ADest, (pointer*) EachPtr, EachIndex);
    }
  } // if

  // ---
  return Result;
} // func

bool /* func */ coreamarksearch__trysearchfinishmarker
  (/* in */  /* restricted */ noconst struct ansicharsizedptr* /* param */ ASource,
   /* out */ /* restricted */ noconst struct ansicharidxptr*   /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  Result =
    (coreacharszptrs__isassigned(ASource)) &&
    (ADest != NULL);
  if (Result)
  {
    ansichar* /* var */ EachPtr = NULL;

    size_t    /* var */ EachIndex;

    bool      /* var */ Found;
    bool      /* var */ CanContinue;
    bool      /* var */ OutOfBounds;

    // ---

    // clear result
    coreacharidxptrs__clear
      (ADest);

    // prepare local variables
    EachPtr = 
      (ansichar*) ASource->ItemPtr;
    EachIndex =
      ASource->ItemSize;

    OutOfBounds = false;
    Found       = false;
    CanContinue = true;

    // move foreward and find
    // finish character marker
    while (CanContinue)
    {
      Found =
        (*EachPtr == FINISHMARKER);
      if (! Found)
      {
        // move pointers backwards
        EachPtr++;
        EachIndex++;
      }

      // check it doesn't go out if the string
      OutOfBounds =
        (EachIndex >= ASource->ItemSize);

      CanContinue =
        ((! Found) && (! OutOfBounds));
    } // while

    Result = Found;
    if (Result)
    {
      coreacharidxptrs__pack
        (&ADest, (pointer*) EachPtr, EachIndex);
    }
  } // if

  // ---
  return Result;
} // func

bool /* func */ coreamarksearch__trysearchlastchar
  (/* in */  /* restricted */ noconst struct ansicharsizedptr* /* param */ ASource,
   /* out */ /* restricted */ noconst struct ansicharidxptr*   /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  // find "finish" character marker,
  // also indicate pointer is assigned,
  // and there are other characters, as well
  Result =
    coreamarkstrs__trysearchfinishmarker
      (ASource, ADest);
  if (Result)
  {
    // go back one character
    coreacharidxptrs__inc(ADest);
  } // if

  // ---
  return Result;
} // func

// ------------------




 // ...

// ------------------

/* override */ const ansinullstring* /* func */ coremarksearch__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "coremarksearch";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coremarksearch__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coremarksearch__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coremarksearch