/** Module: "coreansimarkstrs.h"
 ** Descr.: "Similar to null character terminated strings,"
 **         "uses one special character at start,"
 **         "uses one special character at finish."
 **/

// namespace coreansimarkstrs {
 
// ------------------
 
#ifndef COREANSIMARKSTRS__H
#define COREANSIMARKSTRS__H

// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

/* friend */ ansimarkstring* /* func */ coreansimarkstrs__newstr
  (/* in */ const size_t /* param */ ADestSize);

/* friend */ ansimarkstring* /* func */ coreansimarkstrs__newstrclear
  (/* in */ const size_t /* param */ ADestSize);

/* friend */ void /* func */ coreansimarkstrs__dropstr
  (/* inout */ ansimarkstring** /* param */ ADestBuffer,
   /* in */    const size_t     /* param */ ADestSize);

/* friend */ void /* func */ coreansimarkstrs__dropstrclear
  (/* inout */ ansimarkstring** /* param */ ADestBuffer,
   /* in */    const size_t     /* param */ ADestSize);

// ------------------

/* friend */ comparison /* func */ coreansimarkstrs__safecompare
  (/* in */ /* restricted */ const ansimarkstring* /* param */ A,
   /* in */ /* restricted */ const ansimarkstring* /* param */ B);

/* friend */ comparison /* func */ coreansimarkstrs__safecomparesize
  (/* in */ /* restricted */ const ansimarkstring* /* param */ AFirstBuffer,
   /* in */ const size_t                           /* param */ AFirstSize,
   /* in */ /* restricted */ const ansimarkstring* /* param */ ASecondBuffer,
   /* in */ const size_t                           /* param */ AFirstSize);

/* friend */ comparison /* func */ coreansimarkstrs__overlapcompare
  (/* in */ const /* nonrestricted */ ansimarkstring* /* param */ A,
   /* in */ const /* nonrestricted */ ansimarkstring* /* param */ B);

/* friend */ comparison /* func */ coreansimarkstrs__overlapcomparesize
  (/* in */ const ansimarkstring* /* param */ AFirstBuffer,
   /* in */ const size_           /* param */ AFirstSize,
   /* in */ const ansimarkstring* /* param */ ASecondBuffer,
   /* in */ const size_t          /* param */ AFirstSize);

/* friend */ bool /* func */ coreansimarkstrs__safeareequal
  (/* in */ /* restricted */ const ansimarkstring* /* param */ A,
   /* in */ /* restricted */ const ansimarkstring* /* param */ B);

/* friend */ bool /* func */ coreansimarkstrs__safeareequalsize
  (/* in */ /* restricted */ const ansimarkstring* /* param */ AFirstBuffer,
   /* in */ const size_t                           /* param */ AFirstSize,
   /* in */ /* restricted */ const ansimarkstring* /* param */ ASecondBuffer,
   /* in */ const size_t                           /* param */ AFirstSize);

/* friend */ bool /* func */ coreansimarkstrs__overlapareequal
  (/* in */ const /* nonrestricted */ ansimarkstring* /* param */ A,
   /* in */ const /* nonrestricted */ ansimarkstring* /* param */ B);

/* friend */ bool /* func */ coreansimarkstrs__overlapareequalsize
  (/* in */ /* restricted */ const ansimarkstring* /* param */ AFirstBuffer,
   /* in */ const size_t                           /* param */ AFirstSize,
   /* in */ /* restricted */ const ansimarkstring* /* param */ ASecondBuffer,
   /* in */ const size_t                           /* param */ AFirstSize);

/* friend */ bool /* func */ coreansimarkstrs__safearesame
  (/* in */ /* restricted */ const ansimarkstring* /* param */ A,
   /* in */ /* restricted */ const ansimarkstring* /* param */ B);

/* friend */ bool /* func */ coreansimarkstrs__safearesamesize
  (/* in */ /* restricted */ const ansimarkstring* /* param */ AFirstBuffer,
   /* in */ const size_t                           /* param */ AFirstSize,
   /* in */ /* restricted */ const ansimarkstring* /* param */ ASecondBuffer,
   /* in */ const size_t                           /* param */ AFirstSize);

/* friend */ bool /* func */ coreansimarkstrs__overlaparesame
  (/* in */ const /* nonrestricted */ ansimarkstring* /* param */ A,
   /* in */ const /* nonrestricted */ ansimarkstring* /* param */ B);

/* friend */ bool /* func */ coreansimarkstrs__overlaparesamesize
  (/* in */ /* nonrestricted */ const ansimarkstring* /* param */ AFirstBuffer,
   /* in */ const size_t                              /* param */ AFirstSize,
   /* in */ /* nonrestricted */ const ansimarkstring* /* param */ ASecondBuffer,
   /* in */ const size_t                              /* param */ AFirstSize);

// ------------------

/* inline */ bool /* func */ coreansimarkstrs__isassigned
  (/* in */ const ansimarkstring* /* param */ ASource);

/* inline */ bool /* func */ coreansimarkstrs__isassignedsize
  (/* in */ const ansimarkstring* /* param */ ASourceBuffer, 
   /* in */ size_t                /* param */ ASourceSize);

// ------------------

/* inline */ bool /* func */ coreansimarkstrs__isempty
  (/* in */ const ansimarkstring* /* param */ ASource);

/* inline */ bool /* func */ coreansimarkstrs__isemptysize
  (/* in */ const ansimarkstring* /* param */ ASourceBuffer, 
   /* in */ size_t                /* param */ ASourceSize);

// ------------------

bool /* func */ coreansimarkstrs__isstartmarker
  (/* in */ const ansichar /* param */ AValue);

bool /* func */ coreansimarkstrs__isfinishmarker
  (/* in */ const ansichar /* param */ AValue);

bool /* func */ coreansimarkstrs__isstartmarkerptr
  (/* in */ const ansichar* /* param */ AValue);

bool /* func */ coreansimarkstrs__isfinishmarkerptr
  (/* in */ const ansichar* /* param */ AValue);

// ------------------

void /* func */ coreansimarkstrs__writestartmarker
  (/* inout */ ansichar* /* param */ ADestBuffer);

void /* func */ coreansimarkstrs__writefinishmarker
  (/* inout */ ansichar* /* param */ ADestBuffer);

// ------------------





 // ...
 
// ------------------

/* override */ const ansimarkstring* /* func */ coreansimarkstrs__modulename
  ( noparams );

/* override */ int /* func */ coreansimarkstrs__setup
  ( noparams );

/* override */ int /* func */ coreansimarkstrs__setoff
  ( noparams );

// ------------------

#endif // COREANSIMARKSTRS__H

// } // namespace coreansimarkstrs