/** Module: "coreopenansistrs.h"
 ** Descr.: "..."
 **/
 
// namespace coreopenansistrs {
 
// ------------------
 
#ifndef COREOPENANSISTRS__H
#define COREOPENANSISTRS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"

// ------------------

// pascal string alike
struct openansistringheader
{
  // how many chars are allocated
  uint16 /* var */ MaxSize;
  // how many are currently used
  uint16 /* var */ RealSize;
  
  // follows
  // ansichar /* var */ Items[MaxSize];
} ;

typedef
  openansistringheader /* as */ openansistring;
typedef
  openansistring       /* as */ openansistring_t;
typedef
  openansistring*      /* as */ openansistring_p;

// ------------------

openansistring* /* func */ coreopenansistrs__createstring
  (/* in */ size_t /* param */ AMaxSize);
  
void /* func */ coreopenansistrs__dropstring
  (/* out */ openansistring** /* param */ ADest);

// ------------------

bool /* func*/ coreopenansistrs__isempty
  (/* in */ const openansistring* /* param */ ASource);

// ------------------

void /* func */ coreopenansistrs__clear
  (/* inout */ ansinullstring* /* param */ ADest);

void /* func */ coreopenansistrs__assign
  (/* out */ /* restricted */ openansistring*       /* param */ ADest,
   /* in */  /* restricted */ const ansinullstring* /* param */ ASource);

void /* func */ coreopenansistrs__concat
  (/* out */ /* restricted */ openansistring*       /* param */ ADest,
   /* in */  /* restricted */ const ansinullstring* /* param */ ASource);

void /* func */ coreopenansistrs__addchar
  (/* inout */ openansistring* /* param */ ADest,
   /* in */ const ansichar     /* param */ ASource);

// ------------------

openansistring* /* func */ coreopenansistrs__compactcopy
  (/* in */ const openansistring* /* param */ ASource);

openansistring* /* func */ coreopenansistrs__extendedcopy
  (/* in */ const openansistring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ASourceSize);

// ------------------

void /* func */ coreopenansistrs__assignvarsize
  (/* out */ /* restricted */ ansinullstring* /* param */ ADestBuffer,
   /* in */  /* restricted */ ansinullstring* /* param */ ASourceBuffer,
   /* in */  const size_t                     /* param */ ASourceSize);

void /* func */ coreopenansistrs__concatvarsize
  (/* inout */ /* restricted */ ansinullstring*       /* param */ ADestBuffer,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */    const size_t                           /* param */ ASourceSize);

// ------------------

void /* func */ coreopenansistrs__uppercasechange
  (/* inout */ openansistring* /* param */ ADestBuffer);

void /* func */ coreopenansistrs__lowercasechange
  (/* inout */ openansistring* /* param */ ADestBuffer);

void /* func */ coreopenansistrs__capitalcasechange
  (/* inout */ openansistring* /* param */ ADestBuffer);

// ------------------

openansistring* /* func */ coreopenansistrs__uppercasecopy
  (/* inout */ openansistring* /* param */ ADestBuffer);

openansistring* /* func */ coreopenansistrs__lowercasecopy
  (/* inout */ openansistring* /* param */ ADestBuffer);

openansistring* /* func */ coreopenansistrs__capitalcasecopy
  (/* inout */ openansistring* /* param */ ADestBuffer);

// ------------------




 // ...
 
// ------------------

/* override */ int /* func */ coreopenansistrs__setup
  ( noparams );

/* override */ int /* func */ coreopenansistrs__setoff
  ( noparams );

// ------------------

#endif // COREOPENANSISTRS__H

// } // namespace coreopenansistrs