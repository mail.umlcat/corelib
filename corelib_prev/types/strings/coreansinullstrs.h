/** Module: "coreansinullstrs.h"
 ** Descr.: "Null character terminated strings,"
 **         "predefined library."
 **/

// namespace coreansinullstrs {
 
// ------------------
 
#ifndef COREANSINULLSTRS__H
#define COREANSINULLSTRS__H
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansicharptrs.h"
#include "coreansinullsets.h"
#include "coreacharidxptrs.h"
#include "coreacharsizedptrs.h"

// ------------------

typedef
  ansinullstring /* as */ coreansinullstrs__ansinullstring;

// ------------------

// --> global properties

const ansinullstring* /* func */ coreansinullstrs__blankset
  ( noparams );

bool /* func */ coreansinullstrs__trygetblanksetsize
  (/* out */ ansicharset** /* param */ ASetBuffer,
   /* out */ size_t*       /* param */ ASetSize);

// ------------------

ansinullstring* /* func */ coreansinullstrs__newstr
  (/* in */ const size_t /* param */ ADestSize);

ansinullstring* /* func */ coreansinullstrs__newstrclear
  (/* in */ const size_t /* param */ ADestSize);

void /* func */ coreansinullstrs__dropstr
  (/* inout */ ansinullstring** /* param */ ADestBuffer,
   /* in */    const size_t     /* param */ ADestSize);

void /* func */ coreansinullstrs__dropstrclear
  (/* inout */ ansinullstring** /* param */ ADestBuffer,
   /* in */    const size_t     /* param */ ADestSize);

// ------------------

/* friend */ comparison /* func */ coreansinullstrs__safecompare
  (/* in */ /* restricted */ const ansinullstring* /* param */ A,
   /* in */ /* restricted */ const ansinullstring* /* param */ B);

/* friend */ comparison /* func */ coreansinullstrs__safecomparesize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AFirstBuffer,
   /* in */ const size_t                           /* param */ AFirstSize,
   /* in */ /* restricted */ const ansinullstring* /* param */ ASecondBuffer,
   /* in */ const size_t                           /* param */ AFirstSize);

/* friend */ comparison /* func */ coreansinullstrs__overlapcompare
  (/* in */ const /* nonrestricted */ ansinullstring* /* param */ A,
   /* in */ const /* nonrestricted */ ansinullstring* /* param */ B);

/* friend */ comparison /* func */ coreansinullstrs__overlapcomparesize
  (/* in */ const ansinullstring* /* param */ AFirstBuffer,
   /* in */ const size_           /* param */ AFirstSize,
   /* in */ const ansinullstring* /* param */ ASecondBuffer,
   /* in */ const size_t          /* param */ AFirstSize);

/* friend */ bool /* func */ coreansinullstrs__safeareequal
  (/* in */ /* restricted */ const ansinullstring* /* param */ A,
   /* in */ /* restricted */ const ansinullstring* /* param */ B);

/* friend */ bool /* func */ coreansinullstrs__safeareequalsize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AFirstBuffer,
   /* in */ const size_t                           /* param */ AFirstSize,
   /* in */ /* restricted */ const ansinullstring* /* param */ ASecondBuffer,
   /* in */ const size_t                           /* param */ AFirstSize);

/* friend */ bool /* func */ coreansinullstrs__overlapareequal
  (/* in */ const /* nonrestricted */ ansinullstring* /* param */ A,
   /* in */ const /* nonrestricted */ ansinullstring* /* param */ B);

/* friend */ bool /* func */ coreansinullstrs__overlapareequalsize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AFirstBuffer,
   /* in */ const size_t                           /* param */ AFirstSize,
   /* in */ /* restricted */ const ansinullstring* /* param */ ASecondBuffer,
   /* in */ const size_t                           /* param */ AFirstSize);

/* friend */ bool /* func */ coreansinullstrs__safearesame
  (/* in */ /* restricted */ const ansinullstring* /* param */ A,
   /* in */ /* restricted */ const ansinullstring* /* param */ B);

/* friend */ bool /* func */ coreansinullstrs__safearesamesize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AFirstBuffer,
   /* in */ const size_t                           /* param */ AFirstSize,
   /* in */ /* restricted */ const ansinullstring* /* param */ ASecondBuffer,
   /* in */ const size_t                           /* param */ AFirstSize);

/* friend */ bool /* func */ coreansinullstrs__overlaparesame
  (/* in */ const /* nonrestricted */ ansinullstring* /* param */ A,
   /* in */ const /* nonrestricted */ ansinullstring* /* param */ B);

/* friend */ bool /* func */ coreansinullstrs__overlaparesamesize
  (/* in */ /* nonrestricted */ const ansinullstring* /* param */ AFirstBuffer,
   /* in */ const size_t                              /* param */ AFirstSize,
   /* in */ /* nonrestricted */ const ansinullstring* /* param */ ASecondBuffer,
   /* in */ const size_t                              /* param */ AFirstSize);

// ------------------

/* inline */ bool /* func */ coreansinullstrs__isempty
  (/* in */ const ansinullstring* /* param */ ASource);

/* inline */ bool /* func */ coreansinullstrs__isemptysize
  (/* in */ const ansinullstring* /* param */ ASourceBuffer, 
   /* in */ size_t                /* param */ ASourceSize);

// ------------------

/* inline */ bool /* func */ coreansinullstrs__isrefempty
  (/* out */ ansinullstring** /* param */ ADest);

// ------------------

/* inline */ bool /* func */ coreansinullstrs__canconcatdest
  (/* in */ const ansinullstring* /* param */ ADest);

/* inline */ bool /* func */ coreansinullstrs__canconcatdestsize
  (/* in */ const ansinullstring* /* param */ ADestBuffer, 
   /* in */ const size_t          /* param */ ADestSize);

/* inline */ bool /* func */ coreansinullstrs__canconcatsrc
  (/* in */ const ansinullstring* /* param */ ASource);

/* inline */ bool /* func */ coreansinullstrs__canconcatsrcsize
  (/* in */ const ansinullstring* /* param */ ASourceBuffer, 
   /* in */ const size_t          /* param */ ASourceSize);

// ------------------

count_t /* func */ coreansinullstrs__getlength
  (/* in */ const ansinullstring* /* param */ ASource);

count_t /* func */ coreansinullstrs__getlengthsize
  (/* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ASourceMaxSize);

// ------------------

/* inline */ void /* func */ coreansinullstrs__writenullmarker
  (/* inout */ const ansichar* /* param */ ADestBuffer);

// ------------------

void /* func */ coreansinullstrs__duplicatechar
  (/* in */  const ansichar  /* param */ ASourceChar,
   /* in */  const count_t   /* param */ ASourceCount,
   /* out */ ansinullstring* /* param */ ADestBuffer);

void /* func */ coreansinullstrs__duplicatecharsize
  (/* in */  const ansichar  /* param */ ASourceChar,
   /* in */  const count_t   /* param */ ASourceCount,
   /* out */ ansinullstring* /* param */ ADestBuffer,
   /* in */  const size_t    /* param */ ADestSize);

void /* func */ coreansinullstrs__duplicatenstr
  (/* in */  const ansinullstring* /* param */ ASourceBuffer,
   /* in */  const count_t         /* param */ ASourceCount,
   /* out */ ansinullstring*       /* param */ ADestBuffer);

void /* func */ coreansinullstrs__duplicatenstrsize
  (/* in */  const ansinullstring* /* param */ ASourceBuffer,
   /* in */  const count_t         /* param */ ASourceCount,
   /* out */ ansinullstring*       /* param */ ADestBuffer,
   /* in */  const size_t          /* param */ ADestSize);

// ------------------

count_t /* func */ coreansinullstrs__getcharsepcount
  (/* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const ansichar        /* param */ ASeparator);

count_t /* func */ coreansinullstrs__getcharsepcountsize
  (/* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ASourceSize,
   /* in */ const ansichar        /* param */ ASeparator);

count_t /* func */ coreansinullstrs__getcharsetsepcount
  (/* in */ const /* restricted */ ansinullstring* /* param */ ASourceBuffer,
   /* in */ const /* restricted */ ansicharset*    /* param */ ASepSetBuffer);

count_t /* func */ coreansinullstrs__getcharsetsepcountsize
  (/* in */ const /* restricted */ ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t                           /* param */ ASourceSize,
   /* in */ const /* restricted */ ansicharset*    /* param */ ASepSetBuffer,
   /* in */ const size_t                           /* param */ ASepSetSize);

count_t /* func */ coreansinullstrs__getstrsepcount
  (/* in */ const /* restricted */ ansinullstring* /* param */ ASourceBuffer,
   /* in */ const /* restricted */ ansinullstring* /* param */ ASeparatorBuffer);

count_t /* func */ coreansinullstrs__getstrsepcountsize
  (/* in */ const /* restricted */ ansinullstring* /* param */ ASourceBuffer, 
   /* in */ const size_t                           /* param */ ASourceSize,
   /* in */ const /* restricted */ ansinullstring* /* param */ ASeparatorBuffer,
   /* in */ const size_t                           /* param */ ASeparatorSize);

// ------------------

void /* func */ coreansinullstrs__clear
  (/* inout */ ansinullstring* /* param */ ADest);

void /* func */ coreansinullstrs__clearsize
  (/* inout */ ansinullstring* /* param */ ADest,
   /* in */    const size_t    /* param */ ADestSize);

void /* func */ coreansinullstrs__assign
  (/* out */ /* restricted */ ansinullstring*       /* param */ ADest,
   /* in */  /* restricted */ const ansinullstring* /* param */ ASource);

void /* func */ coreansinullstrs__assignsize
  (/* out */ /* restricted */ ansinullstring*       /* param */ ADestBuffer,
   /* in */  const size_t                           /* param */ ADestSize,
   /* in */  /* restricted */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */  const size_t                           /* param */ ASourceSize);

void /* func */ coreansinullstrs__concat
  (/* out */ /* restricted */ ansinullstring*       /* param */ ADest,
   /* in */  /* restricted */ const ansinullstring* /* param */ ASource);

void /* func */ coreansinullstrs__concatsize
  (/* inout */ /* restricted */ ansinullstring*       /* param */ ADestBuffer,
   /* in */    const size_t                           /* param */ ADestSize,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */    const size_t                           /* param */ ADestSize)

void /* func */ coreansinullstrs__addchar
  (/* inout */ ansinullstring* /* param */ ADest,
   /* in */ const ansichar     /* param */ ASource);

void /* func */ coreansinullstrs__addcharsize
  (/* inout */ ansinullstring* /* param */ ADestBuffer,
   /* in */    const size_t    /* param */ ADestSize,
   /* in */    const ansichar  /* param */ ASource)

ansinullstring* /* func */ coreansinullstrs__compactcopy
  (/* in */ const ansinullstring* /* param */ ASource);

ansinullstring* /* func */ coreansinullstrs__extendedcopy
  (/* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ASourceSize);

// ------------------

ansinullstring* /* func */ coreansinullstrs__ansichartonstrcopy
  (/* in */ const ansichar /* param */ ASource);

ansichar /* func */ coreansinullstrs__nstrtoansicharcopy
  (/* in */ const ansinullstring* /* param */ ASource);

// ------------------

void /* func */ coreansinullstrs__reversechange
  (/* in */ const ansinullstring* /* param */ ADestBuffer);

void /* func */ coreansinullstrs__reversechangesize
  (/* in */ const ansinullstring* /* param */ ADestBuffer,
   /* in */ const size_t          /* param */ ASourceSize);

ansinullstring* /* func */ coreansinullstrs__reversecopy
  (/* in */ const ansinullstring* /* param */ ASourceBuffer);

ansinullstring* /* func */ coreansinullstrs__reversecopysize
  (/* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ASourceSize);

// ------------------

bool /* func */ coreansinullstrs__startswithchar
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ /* restricted */ const ansichar        /* param */ ANeedleBuffer);

bool /* func */ coreansinullstrs__startswithcharsize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                           /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansichar        /* param */ ANeedleBuffer);

bool /* func */ coreansinullstrs__startswithcharptr
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ /* restricted */ const ansichar*       /* param */ ANeedleBuffer);

bool /* func */ coreansinullstrs__startswithcharptrsize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                           /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansichar*       /* param */ ANeedleBuffer);

bool /* func */ coreansinullstrs__startswithcharset
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ /* restricted */ const ansicharset     /* param */ ANeedleSetBuffer);

bool /* func */ coreansinullstrs__startswithcharsetsize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                           /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansicharset*    /* param */ ANeedleSetBuffer,
   /* in */ const size_t                           /* param */ ANeedleSetSize);

bool /* func */ coreansinullstrs__startswithstr
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ /* restricted */ const ansinullstring* /* param */ ANeedleBuffer);

bool /* func */ coreansinullstrs__startswithstrsize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                           /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansinullstring* /* param */ ANeedleBuffer,
   /* in */ const size_t                           /* param */ ANeedleSize);

// ------------------

bool /* func */ coreansinullstrs__finisheswithchar
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ /* restricted */ const ansichar        /* param */ ANeedleBuffer);

bool /* func */ coreansinullstrs__finisheswithcharsize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                           /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansichar        /* param */ ANeedleBuffer);

bool /* func */ coreansinullstrs__finisheswithcharptr
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ /* restricted */ const ansichar*       /* param */ ANeedleBuffer);

bool /* func */ coreansinullstrs__finisheswithcharptrsize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                           /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansichar*       /* param */ ANeedleBuffer);

bool /* func */ coreansinullstrs__finisheswithcharset
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ /* restricted */ const ansicharset     /* param */ ANeedleSetBuffer);

bool /* func */ coreansinullstrs__finisheswithcharsetsize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                           /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansicharset*    /* param */ ANeedleSetBuffer,
   /* in */ const size_t                           /* param */ ANeedleSetSize);

bool /* func */ coreansinullstrs__finisheswithstr
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ /* restricted */ const ansinullstring* /* param */ ANeedleBuffer);

bool /* func */ coreansinullstrs__finisheswithstrsize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                           /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansinullstring* /* param */ ANeedleBuffer,
   /* in */ const size_t                           /* param */ ANeedleSize);

// ------------------

void /* func */ coreansinullstrs__uppercasechange
  (/* inout */ ansinullstring* /* param */ ADestBuffer);

void /* func */ coreansinullstrs__uppercasechangesize
  (/* inout */ ansinullstring* /* param */ ADestBuffer,
   /* in */    const size_t    /* param */ ADestSize);

void /* func */ coreansinullstrs__lowercasechange
  (/* inout */ ansinullstring* /* param */ ADestBuffer);

void /* func */ coreansinullstrs__lowercasechangesize
  (/* inout */ ansinullstring* /* param */ ADestBuffer,
   /* in */    const size_t    /* param */ ADestSize);

void /* func */ coreansinullstrs__capitalcasechange
  (/* inout */ ansinullstring* /* param */ ADestBuffer);

void /* func */ coreansinullstrs__capitalcasechangesize
  (/* inout */ ansinullstring* /* param */ ADestBuffer,
   /* in */    const size_t    /* param */ ADestSize);

void /* func */ coreansinullstrs__reversecasechange
  (/* inout */ ansinullstring* /* param */ ADestBuffer);

void /* func */ coreansinullstrs__reversecasechangesize
  (/* inout */ ansinullstring* /* param */ ADestBuffer,
   /* in */    const size_t    /* param */ ADestSize);

// ------------------

ansinullstring* /* func */ coreansinullstrs__uppercasecopy
  (/* inout */ ansinullstring* /* param */ ASourceBuffer);

ansinullstring* /* func */ coreansinullstrs__uppercasecopysize
  (/* inout */ ansinullstring* /* param */ ASourceBuffer,
   /* in */    const size_t    /* param */ ASourceSize);

ansinullstring* /* func */ coreansinullstrs__lowercasecopy
  (/* inout */ ansinullstring* /* param */ ASourceBuffer);

ansinullstring* /* func */ coreansinullstrs__lowercasecopysize
  (/* inout */ ansinullstring* /* param */ ASourceBuffer,
   /* in */    const size_t    /* param */ ASourceSize);

ansinullstring* /* func */ coreansinullstrs__capitalcasecopy
  (/* inout */ ansinullstring* /* param */ ASourceBuffer);

ansinullstring* /* func */ coreansinullstrs__capitalcasecopysize
  (/* inout */ ansinullstring* /* param */ ASourceBuffer,
   /* in */    const size_t    /* param */ ASourceSize);

ansinullstring* /* func */ coreansinullstrs__reversecasecopy
  (/* inout */ ansinullstring* /* param */ ASourceBuffer);

ansinullstring* /* func */ coreansinullstrs__reversecasecopysize
  (/* inout */ ansinullstring* /* param */ ASourceBuffer,
   /* in */    const size_t    /* param */ ASourceSize);

// ------------------

ansinullstring* /* func */ coreansinullstrs__leftcopy
  (/* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ADestSize);

ansinullstring* /* func */ coreansinullstrs__leftcopysize
  (/* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ASourceSize,
   /* in */ const size_t          /* param */ ADestSize);

ansinullstring* /* func */ coreansinullstrs__rightcopy
  (/* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ADestSize);

ansinullstring* /* func */ coreansinullstrs__rightcopysize
  (/* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ASourceSize,
   /* in */ const size_t          /* param */ ADestSize);

// ------------------

void /* func */ coreansinullstrs__lefttrimchange
  (/* inout */ ansinullstring* /* param */ ADestBuffer);

void /* func */ coreansinullstrs__lefttrimchangesize
  (/* inout */ ansinullstring* /* param */ ADestBuffer,
   /* in */  const size_t      /* param */ ADestSize);

void /* func */ coreansinullstrs__righttrimchange
  (/* inout */ ansinullstring* /* param */ ADestBuffer);

void /* func */ coreansinullstrs__righttrimchangesize
  (/* inout */ ansinullstring* /* param */ ADestBuffer,
   /* in */  const size_t      /* param */ ADestSize);

void /* func */ coreansinullstrs__trimchange
  (/* inout */ ansinullstring* /* param */ ADestBuffer);

void /* func */ coreansinullstrs__trimchangesize
  (/* inout */ ansinullstring* /* param */ ADestBuffer,
   /* in */  const size_t      /* param */ ADestSize);

// ------------------

ansinullstring* /* func */ coreansinullstrs__lefttrimcopy
  (/* inout */ ansinullstring* /* param */ ASourceBuffer);

ansinullstring* /* func */ coreansinullstrs__lefttrimcopysize
  (/* inout */ ansinullstring* /* param */ ASourceBuffer,
   /* in */    const size_t    /* param */ ASourceSize);

ansinullstring* /* func */ coreansinullstrs__righttrimcopy
  (/* inout */ ansinullstring* /* param */ ASourceBuffer);

ansinullstring* /* func */ coreansinullstrs__righttrimcopysize
  (/* inout */ ansinullstring* /* param */ ASourceBuffer,
   /* in */    const size_t    /* param */ ASourceSize);

ansinullstring* /* func */ coreansinullstrs__trimcopy
  (/* inout */ ansinullstring* /* param */ ASourceBuffer);

ansinullstring* /* func */ coreansinullstrs__trimcopysize
  (/* inout */ ansinullstring* /* param */ ASourceBuffer,
   /* in */    const size_t    /* param */ ASourceSize);

// ------------------

ansinullstring* /* func */ coreansinullstrs__firstptrofchar
  (/* in */ ansinullstring* /* param */ AHaystack,
   /* in */ const ansichar  /* param */ ANeedle);

ansinullstring* /* func */ coreansinullstrs__firstptrofcharsize
  (/* in */ ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t    /* param */ AHaystackSize,
   /* in */ const ansichar  /* param */ ANeedle);

ansinullstring* /* func */ coreansinullstrs__firstptrofcharptr
  (/* in */ /* restricted */ ansinullstring* /* param */ AHaystack,
   /* in */ /* restricted */ const ansichar* /* param */ ANeedlePtr);

ansinullstring* /* func */ coreansinullstrs__firstptrofcharptrsize
  (/* in */ /* restricted */ ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                     /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansichar* /* param */ ANeedlePtr);

ansinullstring* /* func */ coreansinullstrs__firstptrofcharset
  (/* in */ /* restricted */ ansinullstring*    /* param */ AHaystack,
   /* in */ /* restricted */ const ansicharset* /* param */ ANeedleSet);

ansinullstring* /* func */ coreansinullstrs__firstptrofcharsetsize
  (/* in */ /* restricted */ ansinullstring*    /* param */ AHaystackBuffer,
   /* in */ const size_t                        /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansicharset* /* param */ ANeedleSetBuffer,
   /* in */ const size_t                        /* param */ ANeedleSetSize);

// ------------------

ansinullstring* /* func */ coreansinullstrs__firstptrofstrequal
  (/* in */ /* restricted */ ansinullstring*    /* param */ AHaystack,
   /* in */ /* restricted */ const ansicharset* /* param */ ANeedle);

ansinullstring* /* func */ coreansinullstrs__firstptrofstrequalsize
  (/* in */ /* restricted */ ansinullstring*    /* param */ AHaystackBuffer,
   /* in */ const size_t                        /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansicharset* /* param */ ANeedleBuffer,
   /* in */ const size_t                        /* param */ ANeedleSize);

// ------------------

ansinullstring* /* func */ coreansinullstrs__firstptrofstrsame
  (/* in */ /* restricted */ ansinullstring*    /* param */ AHaystack,
   /* in */ /* restricted */ const ansicharset* /* param */ ANeedle);

ansinullstring* /* func */ coreansinullstrs__firstptrofstrsamesize
  (/* in */ /* restricted */ ansinullstring*    /* param */ AHaystackBuffer,
   /* in */ const size_t                        /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansicharset* /* param */ ANeedleBuffer,
   /* in */ const size_t                        /* param */ ANeedleSize);

// ------------------

ansinullstring* /* func */ coreansinullstrs__lastptrofchar
  (/* in */ ansinullstring* /* param */ AHaystack,
   /* in */ const ansichar  /* param */ ANeedle);

ansinullstring* /* func */ coreansinullstrs__lastptrofcharsize
  (/* in */ ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t    /* param */ AHaystackSize,
   /* in */ const ansichar  /* param */ ANeedle);

ansinullstring* /* func */ coreansinullstrs__lastptrofcharptr
  (/* in */ /* restricted */ ansinullstring* /* param */ AHaystack,
   /* in */ /* restricted */ const ansichar* /* param */ ANeedlePtr);

ansinullstring* /* func */ coreansinullstrs__lastptrofcharptrsize
  (/* in */ /* restricted */ ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                     /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansichar* /* param */ ANeedlePtrBuffer);

// ------------------

ansinullstring* /* func */ coreansinullstrs__lastptrofcharset
  (/* in */ /* restricted */ ansinullstring*    /* param */ AHaystack,
   /* in */ /* restricted */ const ansicharset* /* param */ ANeedleSet);

ansinullstring* /* func */ coreansinullstrs__lastptrofcharsetsize
  (/* in */ /* restricted */ ansinullstring*    /* param */ AHaystackBuffer,
   /* in */ const size_t                        /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansicharset* /* param */ ANeedleSetBuffer,
   /* in */ const size_t                        /* param */ ANeedleSetSize);

ansinullstring* /* func */ coreansinullstrs__lastptrofstr
  (/* in */ /* restricted */ ansinullstring*    /* param */ AHaystackBuffer,
   /* in */ /* restricted */ const ansicharset* /* param */ ANeedle);

ansinullstring* /* func */ coreansinullstrs__lastptrofstrsize
  (/* in */ /* restricted */ ansinullstring*    /* param */ AHaystackBuffer,
   /* in */ const size_t                        /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansicharset* /* param */ ANeedleBuffer,
   /* in */ const size_t                        /* param */ ANeedleSize);

// ------------------

ansinullstring* /* func */ coreansinullstrs__searchnextnonsepchar
  (/* inout */ /* restricted */ ansinullstring* /* param */ AHaystackPtr,
   /* in */ const ansichar                      /* param */ ASeparator);

ansinullstring* /* func */ coreansinullstrs__searchnextnonsepcharsize
  (/* inout */ /* restricted */ ansinullstring* /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*         /* param */ AHaystackSize,
   /* in */    const ansichar                   /* param */ ASeparator);

 // ------------------

ansinullstring* /* func */ coreansinullstrs__searchprevnonsepchar
  (/* inout */ /* restricted */ ansinullstring* /* param */ AHaystackPtr,
   /* in */ const ansichar                      /* param */ ASeparator);

ansinullstring* /* func */ coreansinullstrs__searchprevnonsepcharsize
  (/* inout */ /* restricted */ ansinullstring* /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*         /* param */ AHaystackSize,
   /* in */    const ansichar                   /* param */ ASeparator);

 // ------------------

ansinullstring* /* func */ coreansinullstrs__searchnextnonsepcharset
  (/* inout */ /* restricted */ ansinullstring*   /* param */ AHaystackPtr,
   /* in */    /* restricted */ const ansicharset /* param */ ASeparatorSet);

ansinullstring* /* func */ coreansinullstrs__searchnextnonsepcharsetsize
  (/* inout */ /* restricted */ ansinullstring*   /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*           /* param */ AHaystackSize,
   /* in */    /* restricted */ const ansicharset /* param */ ASeparatorSet);

 // ------------------

ansinullstring* /* func */ coreansinullstrs__searchprevnonsepcharset
  (/* inout */ /* restricted */ ansinullstring*    /* param */ AHaystackPtr,
   /* in */    /* restricted */ const ansicharset* /* param */ ASeparatorSet);

ansinullstring* /* func */ coreansinullstrs__searchprevnonsepcharsetsize
  (/* inout */ /* restricted */ ansinullstring*   /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*           /* param */ AHaystackSize,
   /* in */    /* restricted */ const ansicharset /* param */ ASeparatorSet);

 // ------------------

ansinullstring* /* func */ coreansinullstrs__searchnextnonsepstr
  (/* inout */ /* restricted */ ansinullstring*       /* param */ AHaystackPtr,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASeparatorStr);

ansinullstring* /* func */ coreansinullstrs__searchnextnonsepstrsize
  (/* inout */ /* restricted */ ansinullstring*       /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*               /* param */ AHaystackSize,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASeparatorStr,
   /* inout */ /* restricted */ size_t                /* param */ ASeparatorSize);

 // ------------------

ansinullstring* /* func */ coreansinullstrs__searchprevnonsepstr
  (/* inout */ /* restricted */ ansinullstring*       /* param */ AHaystackPtr,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASeparatorStr);

ansinullstring* /* func */ coreansinullstrs__searchprevnonsepstrsize
  (/* inout */ /* restricted */ ansinullstring*       /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*               /* param */ AHaystackSize,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASeparatorStr,
   /* inout */ /* restricted */ size_t                /* param */ ASeparatorSize);

// ------------------
 
ansinullstring* /* func */ coreansinullstrs__searchnextsepchar
  (/* inout */ /* restricted */ ansinullstring* /* param */ AHaystackPtr,
   /* in */ const ansichar                      /* param */ ASeparator);

ansinullstring* /* func */ coreansinullstrs__searchnextsepcharsize
  (/* inout */ /* restricted */ ansinullstring* /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*         /* param */ AHaystackSize,
   /* in */    const ansichar                   /* param */ ASeparator);

// ------------------

ansinullstring* /* func */ coreansinullstrs__searchprevsepchar
  (/* inout */ /* restricted */ ansinullstring* /* param */ AHaystackPtr,
   /* in */ const ansichar                      /* param */ ASeparator);

ansinullstring* /* func */ coreansinullstrs__searchprevsepcharsize
  (/* inout */ /* restricted */ ansinullstring* /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*         /* param */ AHaystackSize,
   /* in */    const ansichar                   /* param */ ASeparator);

 // ------------------

ansinullstring* /* func */ coreansinullstrs__searchnextsepcharset
  (/* inout */ /* restricted */ ansinullstring*   /* param */ AHaystackPtr,
   /* in */    /* restricted */ const ansicharset /* param */ ASeparatorSet);

ansinullstring* /* func */ coreansinullstrs__searchnextsepcharsetsize
  (/* inout */ /* restricted */ ansinullstring*   /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*           /* param */ AHaystackSize,
   /* in */    /* restricted */ const ansicharset /* param */ ASeparatorSet);

 // ------------------

ansinullstring* /* func */ coreansinullstrs__searchprevsepcharset
  (/* inout */ /* restricted */ ansinullstring*    /* param */ AHaystackPtr,
   /* in */    /* restricted */ const ansicharset* /* param */ ASeparatorSet);

ansinullstring* /* func */ coreansinullstrs__searchprevsepcharsetsize
  (/* inout */ /* restricted */ ansinullstring*   /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*           /* param */ AHaystackSize,
   /* in */    /* restricted */ const ansicharset /* param */ ASeparatorSet);

 // ------------------

ansinullstring* /* func */ coreansinullstrs__searchnextsepstr
  (/* inout */ /* restricted */ ansinullstring*       /* param */ AHaystackPtr,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASeparatorStr);

ansinullstring* /* func */ coreansinullstrs__searchnextsepstrsize
  (/* inout */ /* restricted */ ansinullstring*       /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*               /* param */ AHaystackSize,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASeparatorStr,
   /* inout */ /* restricted */ size_t                /* param */ ASeparatorSize);

 // ------------------

ansinullstring* /* func */ coreansinullstrs__searchprevsepstr
  (/* inout */ /* restricted */ ansinullstring*       /* param */ AHaystackPtr,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASeparatorStr);

ansinullstring* /* func */ coreansinullstrs__searchprevsepstrsize
  (/* inout */ /* restricted */ ansinullstring*       /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*               /* param */ AHaystackSize,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASeparatorStr,
   /* inout */ /* restricted */ size_t                /* param */ ASeparatorSize);

 // ------------------

bool /* func */ coreansinullstrs__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coreansinullstrs__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coreansinullstrs__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

pointer* /* func */ coreansinullstrs__ansinstr2ptrcopy
  (/* inout */ pointer* /* param */ AValue);


 // ...

// ------------------

/* override */ int /* func */ coreansinullstrs__setup
  ( noparams );

/* override */ int /* func */ coreansinullstrs__setoff
  ( noparams );

// ------------------

#endif // COREANSINULLSTRS__H

// } // namespace coreansinullstrs