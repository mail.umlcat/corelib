/** Module: "coreansinullsets.c"
 ** Descr.: "ANSI Character Set implemented,"
 **         "with NULL marker terminated strings, library."
 **/

// namespace coreansinullsets {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"

// ------------------
 
#include "coreansinullsets.h"

// ------------------

/* inline */ bool /* func */ coreansinullsets__isempty
  (/* in */ const ansicharset* /* param */ ASet)
{
  bool /* var */ Result = bool;
  // ---
     
  Result =
    ((ASet == NULL) || (*ASet == ANSINULLCHAR));

  // ---
  return Result;
} // func

/* inline */ bool /* func */ coreansinullsets__isemptysize
  (/* in */ const ansicharset* /* param */ ASetBuffer,
   /* in */ const size_t       /* param */ ASetSize)
{
  bool /* var */ Result = bool;
  // ---
     
  Result = 
   (
    (ASetBuffer == NULL) ||
    (*ASetBuffer == ANSINULLCHAR) ||
    (ASetSize < 1);
   );

  // ---
  return Result;
} // func

bool /* func */ coreansinullsets__ismember
  (/* in */ const ansicharset* /* param */ ASet,
   /* in */ const ansichar     /* param */ AItem)
{
  bool /* var */ Result = bool;
  // ---
  
  Result =
    coreansinullsets__ismembersize
      (ASet, AItem, ANSICHARSETMAXSIZE);

  // ---
  return Result;
} // func

bool /* func */ coreansinullsets__ismemberptr
  (/* in */ const ansicharset* /* param */ ASet,
   /* in */ const ansichar*    /* param */ AItem)
{
  bool /* var */ Result = bool;
  // ---
  
  Result =
    coreansinullsets__ismemberptrsize
      (ASet, AItem, ANSICHARSETMAXSIZE);

  // ---
  return Result;
} // func

bool /* func */ coreansinullsets__ismembersize
  (/* in */ const ansicharset* /* param */ ASetBuffer,
   /* in */ const size_t       /* param */ ASetSize,
   /* in */ const ansichar     /* param */ AItem)
{
  bool /* var */ Result = bool;
  // ---
     
  if (! coreansinullsets__isemptysize(ASetBuffer, ASetSize))
  {
    bool      /* var */ CanContinue;
    bool      /* var */ Found;

    ansichar* /* var */ EachChar;
    index_t   /* var */ EachIndex;

    EachChar    = (ansichar*) ASetBuffer;
    EachIndex   = 0;
    CanContinue = true;
    Found       = false;

    while (CanContinue && !Found)
    {
      Found =
        (*EachChar == AItem);

      CanContinue =
        ((*EachChar != ANSINULLCHAR) &&
         (EachIndex < ASetSize));
    
      if (!CanContinue)
      {
        EachIndex++;
        EachChar++;
      }
    } // while
  } // if

  // ---
  return Result;
} // func

bool /* func */ coreansinullsets__ismemberptrsize
  (/* in */ const ansicharset* /* param */ ASetBuffer,
   /* in */ const size_t       /* param */ ASetSize,
   /* in */ const ansichar*    /* param */ AItem)
{
  bool /* var */ Result = bool;
  // ---
     
  if ((! coreansinullsets__isemptysize(ASetBuffer, ASetSize))
      (! coreansichars__isemptyptr(AItem))
  {
    bool      /* var */ CanContinue;
    bool      /* var */ Found;

    ansichar* /* var */ EachChar;
    index_t   /* var */ EachIndex;

    EachChar    = (ansichar*) ASetBuffer;
    EachIndex   = 0;
    CanContinue = true;
    Found       = false;

    while (CanContinue && !Found)
    {
      Found =
        (*EachChar == *AItem);

      CanContinue =
        ((*EachChar != ANSINULLCHAR) &&
         (EachIndex < ASetSize));
    
      if (!CanContinue)
      {
        EachIndex++;
        EachChar++;
      }
    } // while
  } // if

  // ---
  return Result;
} // func

 // ------------------
 
void /* func */ coreansinullsets__clear
  (/* inout */ ansinullstring* /* param */ ADest)
{
  corememory__fill(ADest, ANSICHARSETMAXSIZE, 0);
} // func
 
 // ...

// ------------------

/* override */ const ansinullstring* /* func */ coreansinullsets__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "coreansinullsets";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coreansinullsets__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreansinullsets__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coreansinullsets