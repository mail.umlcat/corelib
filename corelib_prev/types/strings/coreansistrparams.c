/** Module: "coreansistrparams.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coreansistrparams {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"

// ------------------
 
#include "coreansistrparams.h"
 
// ------------------

ansistrparams* /* func */ coreansistrparams__splitbycharsepcopy
  (/* in */ const ansinullstring* /* param */ ASource,
   /* in */ const ansichar        /* param */ ASeparator)
{
  ansistrparams* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansistrparams* /* func */ coreansistrparams__splitbycharsetsepcopy
  (/* in */ const /* restricted */ ansinullstring* /* param */ ASource,
   /* in */ const /* restricted */ ansicharset*    /* param */ ASeparatorSet)
{
  ansistrparams* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansistrparams* /* func */ coreansistrparams__splitbystrsepcopy
  (/* in */ const /* restricted */ ansinullstring* /* param */ ASource,
   /* in */ const /* restricted */ ansinullstring* /* param */ ASeparatorStr)
{
  ansistrparams* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansistrparams* /* func */ coreansistrparams__splitbycharsepcopysize
  (/* in */    const ansinullstring* /* param */ ASourceBuffer, 
   /* inout */ size_t                /* param */ ASourceSize,
   /* in */    const ansichar        /* param */ ASeparator)
{
  ansistrparams* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansistrparams* /* func */ coreansistrparams__splitbycharsetsepcopysize
  (/* in */    const /* restricted */ ansinullstring* /* param */ ASource,
   /* inout */ size_t                                 /* param */ ASourceSize,
   /* in */    const /* restricted */ ansicharset*    /* param */ ASeparatorSet,
   /* inout */ size_t*                                /* param */ ASeparatorSize)
{
  ansistrparams* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansistrparams* /* func */ coreansistrparams__splitbystrsepcopysize
  (/* in */    const /* restricted */ ansinullstring* /* param */ ASource,
   /* inout */ size_t                                 /* param */ ASourceSize,
   /* in */    const /* restricted */ ansinullstring* /* param */ ASeparatorStr,
   /* inout */ size_t                                 /* param */ ASeparatorSize)
{
  ansistrparams* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

ansinullstring* /* func */ coreansistrparams__joinskipsepcopy
  (/* in */ const ansistrparams* /* param */ ASourceList)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansistrparams__joinbycharsepcopy
  (/* in */ const ansistrparams* /* param */ ASourceList,
   /* in */ const ansichar       /* param */ ASeparator)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansistrparams__joinbycharsetsepcopy
  (/* in */ /* restricted */ const ansistrparams* /* param */ ASourceList,
   /* in */ /* restricted */ const ansicharset*   /* param */ ASeparatorSet)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansistrparams__joinbystrsepcopy
  (/* in */ /* restricted */ const ansistrparams*  /* param */ ASourceList,
   /* in */ /* restricted */ const ansinullstring* /* param */ ASeparatorStr)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansistrparams__joinbycharsetsepcopysize
  (/* in */ /* restricted */ const ansistrparams* /* param */ ASourceList,
   /* in */ /* restricted */ const ansicharset*   /* param */ ASeparatorSet,
   /* in*/ const size_t                           /* param */ ASeparatorSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansistrparams__joinbystrsepcopysize
  (/* in */ /* restricted */ const ansistrparams*  /* param */ ASourceList,
   /* in */ /* restricted */ const ansinullstring* /* param */ ASeparatorStr,
   /* in */ const size_t                           /* param */ ASeparatorSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

ansinullstring* /* func */ coreansistrparams__enclosebycharsepcopy
  (/* in */ const ansistrparams* /* param */ ASourceList,
   /* in */ const ansichar       /* param */ ABeforeDelimiter,
   /* in */ const ansichar       /* param */ AAfterDelimiter)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansistrparams__enclosebystrsepcopy
  (/* in */ /* restricted */ const ansistrparams*  /* param */ ASourceList,
   /* in */ /* restricted */ const ansinullstring* /* param */ ABeforeDelimiterStr,
   /* in */ /* restricted */ const ansinullstring* /* param */ AAfterDelimiterStr)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansistrparams__enclosebystrsepcopysize
  (/* in */ /* restricted */ const ansistrparams*  /* param */ ASourceList,
   /* in */ /* restricted */ const ansinullstring* /* param */ ABeforeDelimiterStr,
   /* in */ /* restricted */ const ansinullstring* /* param */ AAfterDelimiterStr,
   /* in */ const size_t                           /* param */ ASeparatorSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

size_t /* func */ coreansistrparams__getminsize
  (/* in */ const ansistrparams* /* param */ AList)
{
  size_t /* var */ Result = 0;
  // ---
     
     
  // ---
  return Result;
} // func

size_t /* func */ coreansistrparams__getmaxsize
  (/* in */ const ansistrparams* /* param */ AList)
{
  size_t /* var */ Result = 0;
  // ---
     
     
  // ---
  return Result;
} // func

size_t /* func */ coreansistrparams__getsumsize
  (/* in */ const ansistrparams* /* param */ AList)
{
  size_t /* var */ Result = 0;
  // ---
     
     
  // ---
  return Result;
} // func

count_t /* func */ coreansistrparams__getcount
  (/* in */ const ansistrparams* /* param */ AList)
{
  count_t /* var */ Result = 0;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

ansistrparams* /* func */ coreansistrparams__lowercasecopy
  (/* in */ const ansistrparams* /* param */ ASourceList)
{
  ansistrparams* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansistrparams* /* func */ coreansistrparams__uppercasecopy
  (/* in */ const ansistrparams* /* param */ ASourceList)
{
  ansistrparams* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansistrparams* /* func */ coreansistrparams__capitalcasecopy
  (/* in */ const ansistrparams* /* param */ ASourceList)
{
  ansistrparams* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreansistrparams__lowercasechange
  (/* inout */ const ansistrparams* /* param */ ADestList)
{
  coresystem__nothing();
} // func

void /* func */ coreansistrparams__uppercasechange
  (/* inout */ const ansistrparams* /* param */ ADestList)
{
  coresystem__nothing();
} // func

void /* func */ coreansistrparams__capitalcasechange
  (/* inout */ const ansistrparams* /* param */ ADestList)
{
  coresystem__nothing();
} // func

// ------------------

ansistrparams* /* func */ coreansistrparams__ltrimcopy
  (/* in */ const ansistrparams* /* param */ ASourceList)
{
  ansistrparams* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansistrparams* /* func */ coreansistrparams__rtrimcopy
  (/* in */ const ansistrparams* /* param */ ASourceList)
{
  ansistrparams* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansistrparams* /* func */ coreansistrparams__trimcopy
  (/* in */ const ansistrparams* /* param */ ASourceList)
{
  ansistrparams* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreansistrparams__ltrimchange
  (/* inout */ const ansistrparams* /* param */ ADestList)
{
  coresystem__nothing();
} // func

void /* func */ coreansistrparams__rtrimchange
  (/* inout */ const ansistrparams* /* param */ ADestList)
{
  coresystem__nothing();
} // func

void /* func */ coreansistrparams__trimchange
  (/* inout */ const ansistrparams* /* param */ ADestList)
{
  coresystem__nothing();
} // func

// ------------------

 // ...

// ------------------

/* override */ int /* func */ coreansistrparams__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreansistrparams__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------


// } // namespace coreansistrparams