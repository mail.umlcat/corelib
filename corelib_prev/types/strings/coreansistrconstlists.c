/** Module: "corestringconstlists.c"
 ** Descr.: "predefined library"
 **/
 
// namespace corestringconstlists {
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"
#include "coreiterators.h"
#include "coreansistrconstbuffers.h"
#include "coreptrlists.h"

// ------------------
 
#include "corestringconstlists.h"

// ------------------

stringconstlist* /* func */ corestringconstlists__createlist
  ( noparams )
{
  stringconstlist* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func
 
stringconstlist* /* func */ corestringconstlists__createlistbybuffersize
  (/* in */ size_t* /* param */ ADataSize)
{
  stringconstlist* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ corestringconstlists__droplist
  (/* out */ stringconstlist** /* param */ AList)
{
  coresystem__nothing();
} // func

// ------------------

ansistringconstlistiterator* /* func */ coreansistringconstlists__createiteratorforward
  (/* in */ ansistringconstlist* /* param */ AList)
{
  ansistringconstlistiterator* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansistringconstlistiterator* /* func */ coreansistringconstlists__createiteratorbackward
  (/* in */ ansistringconstlist* /* param */ AList)
{
  ansistringconstlistiterator* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreansistringconstlists__dropiterator
  (/* out */ ansistringconstlistiterator** /* param */ AIterator)
{
  coresystem__nothing();
} // func

// ------------------

bool /* func */ corestringconstlists__isdone
  (/* in */ const stringconstlistiterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ corestringconstlists__movenext
  (/* inout */ stringconstlistiterator* /* param */ AIterator)
{
  coresystem__nothing();
} // func

void /* func */ coreansistringconstlists__readitem
  (/* inout */ ansistringconstlistiterator* /* param */ AIterator
   /* out */   ansinullstring*              /* param */ AItem);
{
  coresystem__nothing();
} // func

// ------------------

bool /* func */ coreansistringconstlists__tryfound
  (/* in */  const ansinullstring* /* param */ ATextPtr,
   /* out */ bool*                 /* param */ ATextFound)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreansistringconstlists__tryptrof
  (/* in */  const ansinullstring* /* param */ ASourceTextPtr,
  (/* out */ ansinullstring*       /* param */ ADestTextPtr)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreansistringconstlists__found
  (/* in */ const ansinullstring* /* param */ AText)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

const ansinullstring* /* func */ coreansistringconstlists__ptrof
  (/* in */ const ansinullstring* /* param */ AText)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return (const ansinullstring*) Result;
} // func

// ------------------

bool /* func */ coreansistrconstlists__tryadd 
  (/* inout */ ansistringconstlist*      /* param */ AList,
   /* in */    const ansinullstring* /* param */ ASourcePtr,
   /* out */   ansinullstring**      /* param */ ADestPtr)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreansistrconstlists__tryaddcount
  (/* inout */ ansistringconstlist*      /* param */ AList,
   /* in */    const ansinullstring* /* param */ ASourcePtr,
   /* in */    const size_t          /* param */ ASourceSize,
   /* out */   ansinullstring**      /* param */ ADestPtr)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

const ansinullstring* /* func */ coreansistrconstlists__add
  (/* inout */ ansistringconstlist*   /* param */ AList,
   /* in */ const ansinullstring* /* param */ AItem)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return (const ansinullstring*) Result;
} // func

const ansinullstring* /* func */ coreansistrconstlists__addcount
  (/* inout */ ansistringconstlist*   /* param */ AList,
   /* in */ const ansinullstrinh* /* param */ AItemPtr,
   /* in */ const size_t          /* param */ AItemSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return (const ansinullstring*) Result;
} // func

// ------------------

bool /* func */ corestringconstlists__tryclear
  (/* inout */ stringconstlist* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ corestringconstlists__isempty
  (/* in */ const stringconstlist* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ corestringconstlists__hasitems
  (/* in */ const stringconstlist* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
     
  Result = !(corestringconstlists__isempty(AList));

  // ---
  return Result;
} // func

void /* func */ corestringconstlists__clear
  (/* inout */ stringconstlist* /* param */ AList)
{
  coresystem__nothing();
} // func

// ------------------

count_t /* func */ corestringconstlists__getcount
  (/* in */ const stringconstlist* /* param */ AList)
{
  count_t /* var */ Result = 0;
  // ---
     
     
  // ---
  return Result;
} // func

size_t /* func */ coreansistrconstlists__getbuffersize
  (/* in */ const ansistringconstlist* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreansistrconstlists__trybufferresize
  (/* inout */ ansistringconstlist* /* param */ AList,
   /* in */    const count_t        /* param */ ANewSize)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreansistrconstlists__bufferesize
  (/* inout */ ansistringconstlist* /* param */ AList,
   /* in */    const count_t        /* param */ ANewSize)
{
  coresystem__nothing();
} // func

// ------------------

 
 // ...

// } // namespace corestringconstlists