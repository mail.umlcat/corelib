/** Module: "coreanullsearch.c"
 ** Descr.: "ANSI Null Character Terminated Strings,"
 **         "Search Operations."
 **/

// namespace coreanullsearch {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansicharptrs.h"
#include "coreansinullsets.h"
#include "coreacharidxptrs.h"
#include "coreacharszptrs.h"

// ------------------

#include "coreanullsearch.h"
 
// ------------------

bool /* func */ coreanullsearch__isempty
  (/* in */ const struct ansicharsizedptr* /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
 
  Result =
    (ASource == NULL) ||
    (ASource->ItemPtr == NULL) ||
    (ASource->ItemIndex < 0) ||
    (*(Source->ItemPtr) == ANSINULLCHAR);

  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreanullsearch__trysearchnullmarker
  (/* in */  /* restricted */ noconst ansicharsizedptr*      /* param */ ASource,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  Result =
    (coreacharszptrs__isassigned(ASource)) &&
    (ADest != NULL);
  if (Result)
  {
    ansichar* /* var */ EachPtr = NULL;

    size_t    /* var */ EachIndex;

    bool      /* var */ Found;
    bool      /* var */ CanContinue;
    bool      /* var */ OutOfBounds;

    // ---

    // clear result
    coreacharidxptrs__clear
      (ADest);

    // prepare local variables
    EachPtr = 
      (ansichar*) ASource->ItemPtr;
    EachIndex = 0;

    OutOfBounds = false;
    Found       = false;
    CanContinue = true;

    // move foreward and find
    // null character marker
    while (CanContinue)
    {
      Found =
        (*EachPtr == ANSINULLCHAR);
      if (! Found)
      {
        EachPtr++;
        EachIndex++;
      }

      // check it doesn't go out if the string
      OutOfBounds =
        (EachIndex <= ASourceSize);

      CanContinue =
        ((! Found) && (! OutOfBounds));
    } // while

    Result = Found;
    if (Result)
    {
      coreacharidxptrs__pack
        (&ADest, (pointer*) EachPtr, EachIndex);
    }
  } // if

  // ---
  return Result;
} // func

bool /* func */ coreanullsearch__trysearchlastchar
  (/* in */  /* restricted */ noconst ansicharsizedptr*      /* param */ ASource,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  // find null character marker,
  // also indicate pointer is assigned,
  // and there are other characters, as well
  Result =
    ((! coreanullsearch__isempty(ASource)) &&
     (coreanullsearch__trysearchnullmarker(ASource, ADest)));
  if (Result)
  {
    // go back one character
    coreacharidxptrs__inc(ADest);
  } // if

  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreanullsearch__startswithequal
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const struct ansicharsizedptr* /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest);
{
  bool /* var */ Result = false;
  // ---

  Result =
    ((coreacharszptrs__isassigned(AHaystack)) &&
     (coreacharszptrs__isassigned(ANeedle)));
  if (Result)
  {
    Result =
      ((AHaystack->ItemSize >= ANeedle->ItemSize) &&
       (ADest != NULL));
    if (Result)
    {
      bool      /* var */ CanContinue;
      bool      /* var */ Match;
      bool      /* var */ OutOfBounds;

      ansichar* /* var */ EachHaystackChar;
      ansichar* /* var */ EachNeedleChar;

      size_t    /* var */ AMatchedCount;

      // ---

      // clear the result
      coreacharidxptrs__clear(ADest);

      CanContinue = true;
      OutOfBounds = false;
      Match = false;

      EachHaystackChar = (ansichar*) AHaystack->ItemPtr;
      EachNeedleChar   = (ansichar*) ANeedle->ItemPtr;

      AMatchedCount    = 0;

      while (CanContinue)
      {
        Match = (*EachHaystackChar == *EachNeedleChar);
        if (Match)
        {
          AMatchedCount++
        } 

        EachHaystackChar++;
        EachNeedleChar++;

        // check it doesn't go out if the string
        OutOfBounds =
          (AMatchedCount >= ANeedle->ItemSize);

        CanContinue =
          ((! OutOfBounds) && (Match));
      } // while

      Result = Match;
      if (Result)
      {
        ADest->ItemPtr  = (--EachHaystackChar);
        ADest->ItemSize = (AHaystack->ItemSize + AMatchedCount - 1);
      }
    } // if
  } // if

  // ---
  return Result;
} // func

bool /* func */ coreanullsearch__finisheswithequal
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const struct ansicharsizedptr* /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest);
{
  bool /* var */ Result = false;
  // ---

  Result =
    ((coreacharszptrs__isassigned(AHaystack)) &&
     (coreacharszptrs__isassigned(ANeedle)));
  if (Result)
  {
    // "Haystack" should be larger than "Needle"
    Result =
      ((AHaystack->ItemSize >= ANeedle->ItemSize) &&
       (ADest != NULL));
    if (Result)
    {
      bool            /* var */ CanContinue;
      bool            /* var */ Match;
      bool            /* var */ OutOfBounds;

      ansichar*       /* var */ EachHaystackChar;
      ansichar*       /* var */ EachNeedleChar;

      size_t          /* var */ AMatchedCount;

      struct ansicharidxptr* /* var */ ALastChar;

      // ---

      // clear the result
      coreacharidxptrs__clear(ADest);

      // move both pointers to the end
      // of respective null-string
      ALastChar =
        coreacharidxptrs__new();

      Result =
        (trysearchlastchar(AHaystack, ALastChar));
      if (Result)
      {
        EachHaystackChar = (ansichar*) ALastChar->ItemPtr;

        Result =
          (trysearchlastchar(ANeedle, ALastChar))
        if (Result)
        {
          EachNeedleChar   = (ansichar*) ALastChar->ItemPtr;


          CanContinue = true;
          OutOfBounds = false;
          Match       = false;

          // and then, start comparison, bacwards
          AMatchedCount    = 0;

          while (CanContinue)
          {
            Match = (*EachHaystackChar == *EachNeedleChar);
            if (Match)
            {
              AMatchedCount++
            } 
 
            EachHaystackChar--;
            EachNeedleChar--;

            // check it doesn't go out if the string
            OutOfBounds =
              (AMatchedCount >= ANeedle->ItemSize);

            CanContinue =
              ((! OutOfBounds) && (Match));
          } // while

          Result = Match;
          if (Result)
          {
            ADest->ItemPtr  = (++EachHaystackChar);
            ADest->ItemSize = (AHaystack->ItemSize - AMatchedCount + 1);
          }
        } // if
      } // it

      coreacharidxptrs__drop(&ALastChar);
    } // if
  } // if

  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreanullsearch__startswithsame
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const struct ansicharsizedptr* /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest);
{
  bool /* var */ Result = false;
  // ---

  Result =
    ((coreacharszptrs__isassigned(AHaystack)) &&
     (coreacharszptrs__isassigned(ANeedle)));
  if (Result)
  {
    // "Haystack" should be larger than "Needle"
    Result =
      ((Haystack->ItemSize >= ANeedle->ItemSize) &&
       (ADest != NULL));
    if (Result)
    {
      bool      /* var */ CanContinue;
      bool      /* var */ Match;
      bool      /* var */ OutOfBounds;

      ansichar* /* var */ EachHaystackChar;
      ansichar* /* var */ EachNeedleChar;

      size_t    /* var */ AMatchedCount;

      // ---

      // clear the result
      coreacharidxptrs__clear(ADest);

      CanContinue = true;
      OutOfBounds = false;
      Match       = false;

      EachHaystackChar = (ansichar*) AHaystack->ItemPtr;
      EachNeedleChar   = (ansichar*) ANeedle->ItemPtr;

      AMatchedCount    = 0;

      while (CanContinue)
      {
        Match =
          (coreansicharptrs__safearesame(EachHaystackChar, EachNeedleChar));
        if (Match)
        {
          AMatchedCount++
        } 

        EachHaystackChar++;
        EachNeedleChar++;

        // check it doesn't go out if the string
        OutOfBounds =
          (AMatchedCount >= ANeedle->ItemSize);

        CanContinue =
          ((! OutOfBounds) && (Match));
      } // while

      Result = Match;
      if (Result)
      {
        ADest->ItemPtr  = (--EachHaystackChar);
        ADest->ItemSize = (AHaystack->ItemSize + AMatchedCount - 1);
      }
    } // if
  } // if

  // ---
  return Result;
} // func

bool /* func */ coreanullsearch__finisheswithsame
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const struct ansicharsizedptr* /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest);
{
  bool /* var */ Result = false;
  // ---

  Result =
    ((coreacharszptrs__isassigned(AHaystack)) &&
     (coreacharszptrs__isassigned(ANeedle)));
  if (Result)
  {
    // "Haystack" should be larger than "Needle"
    Result =
      ((AHaystack->ItemSize >= ANeedle->ItemSize) &&
       (ADest != NULL));
    if (Result)
    {
      bool            /* var */ CanContinue;
      bool            /* var */ Match;
      bool            /* var */ OutOfBounds;

      ansichar*       /* var */ EachHaystackChar;
      ansichar*       /* var */ EachNeedleChar;

      size_t          /* var */ AMatchedCount;

      struct ansicharidxptr* /* var */ ALastChar;

      // ---

      // clear the result
      coreacharidxptrs__clear(ADest);

      ALastChar =
        coreacharidxptrs__new();

      Result =
        (trysearchlastchar(AHaystack, ALastChar));
      if (Result)
      {
        EachHaystackChar = (ansichar*) ALastChar->ItemPtr;

        Result =
          (trysearchlastchar(ANeedle, ALastChar))
        if (Result)
        {
          EachNeedleChar   = (ansichar*) ALastChar->ItemPtr;

          CanContinue = true;
          OutOfBounds = false;
          Match       = false;

          AMatchedCount    = 0;

          while (CanContinue)
          {
            Match =
              (coreansicharptrs__safearesame(EachHaystackChar, EachNeedleChar));
            if (Match)
            {
              AMatchedCount++
            } 

            EachHaystackChar--;
            EachNeedleChar--;

            // check it doesn't go out if the string
            OutOfBounds =
              (AMatchedCount >= ANeedle->ItemSize);

            CanContinue =
              ((! OutOfBounds) && (Match));
          } // while

          Result = Match;
          if (Result)
          {
            ADest->ItemPtr  = (++EachHaystackChar);
            ADest->ItemSize = (AHaystack->ItemSize - AMatchedCount + 1);
          }
        } // if
      } // it

      coreacharidxptrs__drop(&ALastChar);
    } // if
  } // if

  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreanullsearch__tryfirstptrofchar
  (/* in */  /* restricted */ const ansicharsizedptr*        /* param */ AHaystack,
   /* in */  const ansichar                                  /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  Result =
     ((coreacharszptrs__isassigned(AHaystack)) &&
      (ADest != NULL) &&
      (ANeedle != ANSINULLCHAR));
  if (Result)
  { 
    bool      /* var */ Found;
    bool      /* var */ CanContinue;
    bool      /* var */ OutOfBounds;

    ansichar* /* var */ EachChar;
    size_t    /* var */ ACount;

    // ---

    // clear the result
    coreacharidxptrs__clear(ADest);

    EachChar =
      (ansichar*) AHaystackBuffer;

    ACount = 0;

    CanContinue = true;
    OutOfBounds = false;
    Found = false;

    while (CanContinue)
    {
      Found =
        (*EachChar == ANeedle);
      if (Found)
      {
        ADest->ItemPtr   = EachChar;
        ADest->ItemIndex = ACount;
      }

      EachChar++;
      ACount++;

      // check it doesn't go out if the string
      OutOfBounds =
        (ACount <= AHaystackSize);

      CanContinue =
        ((! Found) && (! OutOfBounds));
    } // while

    Result = Match;
  } // if

  // ---
  return Result;
} // func

bool /* func */ coreanullsearch__tryfirstptrofcharptr
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const ansichar                 /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  Result =
    ((coreacharszptrs__isassigned(AHaystack)) &&
     (ADest != NULL));
  if (Result)
  { 
    bool      /* var */ Found;
    bool      /* var */ CanContinue;
    bool      /* var */ OutOfBounds;

    ansichar* /* var */ EachChar;
    size_t    /* var */ ACount;

    // ---

    // clear the result
    coreacharidxptrs__clear(ADest);

    EachChar =
      (ansichar*) AHaystackBuffer;

    ACount = 0;

    CanContinue = true;
    OutOfBounds = false;
    Found       = false;

    while (CanContinue)
    {
      Found =
        (*EachChar == *ANeedle);
      if (Found)
      {
        ADest->ItemPtr   = EachChar;
        ADest->ItemIndex = ACount;
      }

      EachChar++;
      ACount++;

      // check it doesn't go out if the string
      OutOfBounds =
        (ACount >= AHaystackSize);

      CanContinue =
        ((! Found) && (! OutOfBounds));
    } // while

    Result = Found;
  } // if

  // ---
  return Result;
} // func

bool /* func */ coreanullsearch__tryfirstptrofcharset
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const struct ansicharsizedptr* /* param */ ANeedleSet,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  Result =
    ((coreacharszptrs__isassigned(AHaystack)) &&
     (coreacharszptrs__isassigned(ANeedleSet)) &&
     (ADest != NULL));
  if (Result)
  { 
    bool      /* var */ Found;
    bool      /* var */ CanContinue;
    bool      /* var */ OutOfBounds;

    ansichar* /* var */ EachChar;
    size_t    /* var */ ACount;

    // ---

    // clear the result
    coreacharidxptrs__clear(ADest);

    EachChar =
      (ansichar*) AHaystackBuffer;

    ACount = 0;

    CanContinue = true;
    OutOfBounds = false;
    Found       = false;

    while (CanContinue)
    {
      Found =
        (coreansinullsets__ismembersize
           (ANeedleSet->ItemPtr, ANeedleSet->ItemSize, *EachChar))
      if (Found)
      {
        ADest->ItemPtr   = EachChar;
        ADest->ItemIndex = ACount;
      }

      EachChar++;
      ACount++;

      // check it doesn't go out if the string
      OutOfBounds =
        (ACount >= AHaystackSize);

      CanContinue =
        ((! Found) && (! OutOfBounds));
    } // while

    Result = Found;
  } // if

  // ---
  return Result;
} // func

bool /* func */ coreanullsearch_tryfirstptrofstrequal
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const struct ansicharsizedptr* /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  Result =
     ((coreacharszptrs__isassigned(AHaystack)) &&
      (coreacharszptrs__isassigned(ANeedle));
  if (Result)
  { 
    Result =
      ((AHaystack->ItemSize >= ANeedle->ItemSize) &&
       (ADest != NULL));
    if (Result)
    {
      bool      /* var */ Found;
      bool      /* var */ CanContinue;
      bool      /* var */ OutOfBounds;

      ansichar* /* var */ EachChar;
      size_t    /* var */ ACount;

      // ---

      // clear the result
      coreacharidxptrs__clear(ADest);

      EachChar =
        (ansichar*) AHaystackBuffer;

      ACount = 0;

      CanContinue = true;
      OutOfBounds = false;
      Found       = false;

      while (CanContinue)
      {
        Found =
          (coreanullsearch__startswithequal(AHaystack, ANeedle, ADest));
        if (Found)
        {
          ADest->ItemPtr   = EachChar;
          ADest->ItemIndex = ACount;
        }

        EachChar++;
        ACount++;

        // check it doesn't go out if the string
        OutOfBounds =
          (ACount >= AHaystackSize);

        CanContinue =
          ((! Found) && (! OutOfBounds));
      } // while

      Result = Found;
      if (Result)
      {
        ADest->ItemPtr  = (--EachHaystackChar);
        ADest->ItemSize = (AHaystack->ItemSize + AMatchedCount - 1);
      }
    } // if
  } // if

  // ---
  return Result;
} // func

bool /* func */ coreanullsearch_tryfirstptrofstrsame 
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const struct ansicharsizedptr* /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  Result =
     ((coreacharszptrs__isassigned(AHaystack)) &&
      (coreacharszptrs__isassigned(ANeedle));
  if (Result)
  { 
    Result =
      ((AHaystack->ItemSize >= ANeedle->ItemSize) &&
       (ADest != NULL));
    if (Result)
    {
      bool      /* var */ Found;
      bool      /* var */ CanContinue;
      bool      /* var */ OutOfBounds;

      ansichar* /* var */ EachChar;
      size_t    /* var */ ACount;

      // ---

      // clear the result
      coreacharidxptrs__clear(ADest);

      EachChar =
        (ansichar*) AHaystackBuffer;

      ACount = 0;

      CanContinue = true;
      OutOfBounds = false;
      Found       = false;

      while (CanContinue)
      {
        Found =
          (coreanullsearch__startswithequal(AHaystack, ANeedle, ADest));
        if (Found)
        {
          ADest->ItemPtr   = EachChar;
          ADest->ItemIndex = ACount;
        }

        EachChar++;
        ACount++;

        // check it doesn't go out if the string
        OutOfBounds =
          (ACount >= AHaystackSize);

        CanContinue =
          ((! Found) && (! OutOfBounds));
      } // while

      Result = Found;
      if (Result)
      {
        ADest->ItemPtr  = (--EachHaystackChar);
        ADest->ItemSize = (AHaystack->ItemSize + AMatchedCount - 1);
      }
    } // if
  } // if

  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreanullsearch__trylastptrofchar
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  const ansichar                                  /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  Result =
    ((coreacharszptrs__isassigned(AHaystack)) &&
     (ADest != NULL));
  if (Result)
  { 
    bool      /* var */ Found;
    bool      /* var */ CanContinue;
    bool      /* var */ OutOfBounds;

    ansichar* /* var */ EachChar;
    ansichar* /* var */ LastChar;

    size_t    /* var */ ACount;

    // ---

    // clear the result
    coreacharidxptrs__clear(ADest);

    ALastChar =
      coreacharidxptrs__new();

    Result =
      (trysearchlastchar(AHaystack, ALastChar));
    if (Result)
    {
      EachChar = (ansichar*) ALastChar->ItemPtr;

      ACount = 0;

      CanContinue = true;
      OutOfBounds = false;
      Found       = false;

      while (CanContinue)
      {
        Found =
          (*EachChar == ANeedle);
        if (Found)
        {
          ADest->ItemPtr   = EachChar;
          ADest->ItemIndex = ACount;
        }

        EachChar--;
        ACount++;

        // check it doesn't go out if the string
        OutOfBounds =
          (ACount >= AHaystackSize);

        CanContinue =
          ((! Found) && (! OutOfBounds));
      } // while

      Result = Found;
    } // if

    coreacharidxptrs__drop(&ALastChar);
  } // if

  // ---
  return Result;
} // func

bool /* func */ coreanullsearch__trylastptrofcharptr
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const ansichar*                /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  Result =
    ((coreacharszptrs__isassigned(AHaystack)) &&
     (ADest != NULL));
  if (Result)
  { 
    bool      /* var */ Found;
    bool      /* var */ CanContinue;
    bool      /* var */ OutOfBounds;

    ansichar* /* var */ EachChar;
    ansichar* /* var */ LastChar;

    size_t    /* var */ ACount;

    // ---

    // clear the result
    coreacharidxptrs__clear(ADest);

    ALastChar =
      coreacharidxptrs__new();

    Result =
      (trysearchlastchar(AHaystack, ALastChar));
    if (Result)
    {
      EachHaystackChar = (ansichar*) ALastChar->ItemPtr;

      ACount = 0;

      CanContinue = true;
      OutOfBounds = false;
      Found       = false;

      while (CanContinue)
      {
        Found =
          (*EachChar == *ANeedle);
        if (Found)
        {
          ADest->ItemPtr   = EachChar;
          ADest->ItemIndex = ACount;
        }

        EachChar--;
        ACount++;

        // check it doesn't go out if the string
        OutOfBounds =
          (ACount >= AHaystackSize);

        CanContinue =
          ((! Found) && (! OutOfBounds));
      } // while

      Result = Found;
    } // if

    coreacharidxptrs__drop(&ALastChar);
  } // if

  // ---
  return Result;
} // func

bool /* func */ coreanullsearch__trylastptrofcharset
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const struct ansicharsizedptr* /* param */ ANeedleSet,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  Result =
     ((coreacharszptrs__isassigned(AHaystack)) &&
      (coreacharszptrs__isassigned(ANeedleSet)) &&
      (ADest != NULL));
  if (Result)
  {
    bool      /* var */ Found;
    bool      /* var */ CanContinue;
    bool      /* var */ OutOfBounds;

    ansichar* /* var */ EachChar;
    ansichar* /* var */ LastChar;

    size_t    /* var */ ACount;

    // ---

    // clear the result
    coreacharidxptrs__clear(ADest);

    ALastChar =
      coreacharidxptrs__new();

    Result =
      (trysearchlastchar(AHaystack, ALastChar));
    if (Result)
    {
      EachHaystackChar = (ansichar*) ALastChar->ItemPtr;

      CanContinue = true;
      OutOfBounds = false;
      Found       = false;

      ACount = 0;

      while (CanContinue)
      {
        Found =
          (coreansinullsets__ismembersize
             (ANeedleSet->ItemPtr, ANeedleSet->ItemSize, *EachHaystackChar))
        if (Found)
        {
          ADest->ItemPtr   = EachChar;
          ADest->ItemIndex = ACount;
        }

        EachHaystackChar--;
        ACount++;

        // check it doesn't go out if the string
        OutOfBounds =
          (ACount >= AHaystackSize);

        CanContinue =
          ((! Found) && (! OutOfBounds));
      } // while

      Result = Found;
    } // if

    coreacharidxptrs__drop(&ALastChar);
  } // if

  // ---
  return Result;
} // func

bool /* func */ coreanullsearch__trylastptrofstrequal
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const struct ansicharsizedptr* /* param */ ANeedle,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  Result =
     ((coreacharszptrs__isassigned(AHaystack)) &&
      (coreacharszptrs__isassigned(ANeedle));
  if (Result)
  { 
    Result =
      ((AHaystack->ItemSize >= ANeedle->ItemSize) &&
       (ADest != NULL));
    if (Result)
    {
      bool      /* var */ Found;
      bool      /* var */ CanContinue;
      bool      /* var */ OutOfBounds;

      ansichar* /* var */ EachChar;
      ansichar* /* var */ LastChar;

      size_t    /* var */ ACount;

      // ---

      // clear the result
      coreacharidxptrs__clear(ADest);

      ALastChar =
        coreacharidxptrs__new();

      Result =
        (trysearchlastchar(AHaystack, ALastChar));
      if (Result)
      {
        EachHaystackChar = (ansichar*) ALastChar->ItemPtr;

        CanContinue = true;
        OutOfBounds = false;
        Found       = false;

        ACount = 0;

        while (CanContinue)
        {
          Found =
            (coreanullsearch__startswithequal(EachHaystackChar, ANeedle, ADest));
          if (Found)
          {
            ADest->ItemPtr   = EachHaystackChar;
            ADest->ItemIndex = ACount;
          }

          EachHaystackChar--;
          ACount++;

          // check it doesn't go out if the string
          OutOfBounds =
            (ACount >= AHaystackSize);

          CanContinue =
            ((! Found) && (! OutOfBounds));
        } // while

        Result = Found;
      } // if

      coreacharidxptrs__drop(&ALastChar);
    } // if
  } // if

  // ---
  return Result;
} // func

bool /* func */ coreanullsearch__trylastptrofstrsame
  (/* in */  /* restricted */ const struct ansicharsizedptr* /* param */ AHaystack,
   /* in */  /* restricted */ const struct ansicharsizedptr* /* param */ ANeedleSet,
   /* out */ /* restricted */ noconst struct ansicharidxptr* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  Result =
     ((coreacharszptrs__isassigned(AHaystack)) &&
      (coreacharszptrs__isassigned(ANeedle));
  if (Result)
  { 
    Result =
      ((AHaystack->ItemSize >= ANeedle->ItemSize) &&
       (ADest != NULL));
    if (Result)
    {
      bool      /* var */ Found;
      bool      /* var */ CanContinue;
      bool      /* var */ OutOfBounds;

      ansichar* /* var */ EachChar;
      ansichar* /* var */ LastChar;

      size_t    /* var */ ACount;

      // ---

      // clear the result
      coreacharidxptrs__clear(ADest);

      ALastChar =
        coreacharidxptrs__new();

      Result =
        (trysearchlastchar(AHaystack, ALastChar));
      if (Result)
      {
        EachHaystackChar = (ansichar*) ALastChar->ItemPtr;

        CanContinue = true;
        OutOfBounds = false;
        Found       = false;

        ACount = 0;

        while (CanContinue)
        {
          Found =
            (coreanullsearch__startsamewith(EachHaystackChar, ANeedle, ADest));
          if (Found)
          {
            ADest->ItemPtr   = EachHaystackChar;
            ADest->ItemIndex = ACount;
          }

          EachHaystackChar--;
          ACount++;

          // check it doesn't go out if the string
          OutOfBounds =
            (ACount >= AHaystackSize);

          CanContinue =
            ((! Found) && (! OutOfBounds));
        } // while

        Result = Found;
      } // if

      coreacharidxptrs__drop(&ALastChar);
    } // if
  } // if

  // ---
  return Result;
} // func

// ------------------




 // ...

// ------------------

/* override */ const ansinullstring* /* func */ coreanullsearch__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "coreanullsearch";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coreanullsearch__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreanullsearch__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coreanullsearch