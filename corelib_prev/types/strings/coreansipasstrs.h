/** Module: "coreansipasstrs.h"
 ** Descr.: "..."
 **/
 
// namespace coreansipasstrs {
 
// ------------------
 
#ifndef COREANSIPASSTRS__H
#define COREANSIPASSTRS__H
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

typedef
  ansichar /* as */ ansipascalstring[256];
typedef
  ansipascalstring /* as */ coreansipasstrs__ansipascalstring;

// ------------------

bool /* func */ coreansipastrs__safeareequal
  (/* in */ const /* restricted */ ansipascalstring* /* param */ A,
   /* in */ const /* restricted */ ansipascalstring* /* param */ B);

bool /* func */ coreansipastrs__safearesame
  (/* in */ const /* restricted */ ansipascalstring* /* param */ A,
   /* in */ const /* restricted */ ansipascalstring* /* param */ B);

comparison /* func */ coreansipasstrs__safecompare
  (/* in */ const /* restricted */ ansipascalstring* /* param */ A,
   /* in */ const /* restricted */ ansipascalstring* /* param */ B);

// ------------------

void /* func */ coreansipasstrs__clear
  (/* inout */ ansipascalstring* /* param */ ADest);

void /* func */ coreansipasstrs__assignconst
  (/* out */ /* restricted */ ansipascalstring*     /* param */ ADest,
   /* in */  /* restricted */ const ansinullstring* /* param */ ASource);

void /* func */ coreansipasstrs__concatconst
  (/* inout */ /* restricted */ ansipascalstring*   /* param */ ADest,
   /* in */  /* restricted */ const ansinullstring* /* param */ ASource);

void /* func */ coreansipasstrs__concatchar
  (/* inout */ ansipascalstring* /* param */ ADest,
   /* in */ const ansichar       /* param */ ASource);

void /* func */ coreansipasstrs__assignpascal
  (/* out */ /* restricted */ ansipascalstring*       /* param */ ADest,
   /* in */  /* restricted */ const ansipascalstring* /* param */ ASource);

void /* func */ coreansipasstrs__concatpascal
  (/* inout */ /* restricted */ ansipascalstring*     /* param */ ADest,
   /* in */  /* restricted */ const ansipascalstring* /* param */ ASource);

// ------------------

void /* func */ coreansipasstrs__uppercasechange
  (/* inout */ ansipascalstring* /* param */ ADestBuffer);

void /* func */ coreansipasstrs__lowercasechange
  (/* inout */ ansipascalstring* /* param */ ADestBuffer);

void /* func */ coreansipasstrs__uppercasecopy
  (/* out */ /* restricted */ ansipascalstring*       /* param */ ADest,
   /* in */  /* restricted */ const ansipascalstring* /* param */ ASource);

void /* func */ coreansipasstrs__lowercasecopy
  (/* out */ /* restricted */ ansipascalstring*       /* param */ ADest,
   /* in */  /* restricted */ const ansipascalstring* /* param */ ASource);

// ------------------

void /* func */ coreansipasstrs__leftcopy
  (/* out */ /* restricted */ ansipascalstring*       /* param */ ADestBuffer,
   /* in */  /* restricted */ const ansipascalstring* /* param */ ASourceBuffer,
   /* in */ const size_t                              /* param */ ADestSize);

void /* func */ coreansipasstrs__rightcopy
  (/* out */ /* restricted */ ansipascalstring*       /* param */ ADestBuffer,
   /* in */  /* restricted */ const ansipascalstring* /* param */ ASourceBuffer,
   /* in */ const size_t                              /* param */ ADestSize);

 // ------------------

bool /* func */ coreansipasstrs__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coreansipasstrs__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coreansipasstrs__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);

 // ------------------



 // ...
 
// ------------------

#endif // COREANSIPASSTRS__H

// } // namespace coreansipasstrs