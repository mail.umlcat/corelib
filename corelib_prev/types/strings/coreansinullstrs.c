/** Module: "coreansinullstrs.c"
 ** Descr.: "Null character terminated strings,"
 **         "predefined library."
 **/

// namespace coreansinullstrs {
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansicharptrs.h"
#include "coreansinullsets.h"
#include "coreacharidxptrs.h"
#include "coreacharsizedptrs.h"

// ------------------

#include "coreansinullstrs.h"
 
// ------------------

// --> global properties

const ansinullstring* /* func */ coreansinullstrs__blankset
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    " \n\r\t\v";
  
  return (const ansinullstring*) setbuffer;
} // func

bool /* func */ coreansinullstrs__trygetblanksetsize
  (/* out */ ansicharset** /* param */ ASetBuffer,
   /* out */ size_t*       /* param */ ASetSize)
{
  bool /* var */ Result = false;
  // ---

  static ansichar /* var */ setbuffer[] =
    " \n\r\t\v";
  static size_t   /* var */ setsize =
    (sizeof(char) * 5);

  Result =
    corememory__isassignedsize(ASetBuffer, ASetSize);
  if (Result)
  {
    *ASetBuffer =
      (ansicharset*) setbuffer;
    *ASetSize = setsize;
  } // if

  // ---
  return Result;
} // func

// ------------------

/* friend */ ansinullstring* /* func */ coreansinullstrs__newstr
  (/* in */ const size_t /* param */ ADestSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
       
  Result =
    corememory__allocate(++ADestSize);

  // ---
  return Result;
} // func

/* friend */ ansinullstring* /* func */ coreansinullstrs__newstrclear
  (/* in */ const size_t /* param */ ADestSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
       
  Result =
    corememory__clearallocate(++ADestSize);

  // ---
  return Result;
} // func

/* friend */ void /* func */ coreansinullstrs__dropstr
  (/* inout */ ansinullstring** /* param */ ADestBuffer,
   /* in */    const size_t     /* param */ ADestSize)
{
  if ((ADestBuffer != NULL) && (ADestSize > 0))
  {
    corememory__deallocate(*ADestBuffer, ++ADestSize);
  } // if
} // func

/* friend */ void /* func */ coreansinullstrs__dropstrclear
  (/* inout */ ansinullstring** /* param */ ADestBuffer,
   /* in */    const size_t     /* param */ ADestSize)
{
  if ((ADestBuffer != NULL) && (ADestSize > 0))
  {
    corememory__cleardeallocate(*ADestBuffer, ++ADestSize);
  } // if
} // func

// ------------------

/* friend */ comparison /* func */ coreansinullstrs__safecompare
  (/* in */ /* restricted */ const ansinullstring* /* param */ A,
   /* in */ /* restricted */ const ansinullstring* /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
  
  int /* var */ ErrorCode = 0;

  if ((A != NULL) && (B != NULL))
  {
    ErrorCode =
      strcmp(A, B);
    Result =
       coresystem__inttocmp(ErrorCode);
  } // if
 
  // ---
  return Result;
} // func

/* friend */ comparison /* func */ coreansinullstrs__safecomparesize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AFirstBuffer,
   /* in */ const size_t                           /* param */ AFirstSize,
   /* in */ /* restricted */ const ansinullstring* /* param */ ASecondBuffer,
   /* in */ const size_t                           /* param */ AFirstSize);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
  if (! coreansinullstrs__isemptysize(AFirstBuffer, AFirstSize) &&
     (! coreansinullstrs__isemptysize(ASecondBuffer, ASecondSize))
  {
    size_t    /* var */ AMaxSize;

    ansichar* /* var */ AA;
    ansichar* /* var */ BB;

    bool      /* var */ CanContinue;
    index_t   /* var */ Count;
    size_t    /* var */ AMaxSize;

    AMaxSize =
      corememory__oneminsize
        (AFirstSize, ASecondSize);

    AA = (ansichar*) A;
    BB = (ansichar*) B;

    Count = 0;
    CanContinue = true;

    while (CanContinue)
    {
      Result =
        coreansicharptrs__safecompare(AA, BB);

      AA++;
      BB++;

      Count++;

      CanContinue =
        (*AA != ANSINULLCHAR) &&
        (*BB != ANSINULLCHAR) &&
        (Result == comparison__lesser) &&
        (Count <= AMaxSize);
    } // while

    // if one parameter is a full substring,
    // from another, and are equal,
    // the shorter is sorted lesser
    if (Result == comparison__equal)
    {
      if (AFirstSize < ASecondSize)
      {
        Result = comparison__lesser;
      } 
      else
      {
        Result = comparison__greater;
      } 
    } 
  } // if
    
  // ---
  return Result;
} // func

/* friend */ comparison /* func */ coreansinullstrs__overlapcompare
  (/* in */ const /* nonrestricted */ ansinullstring* /* param */ A,
   /* in */ const /* nonrestricted */ ansinullstring* /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
  if ((A != NULL) && (B != NULL))
  {
  	
  
  
  } // if

  // ---
  return Result;
} // func

/* friend */ comparison /* func */ coreansinullstrs__overlapcomparesize
  (/* in */ const ansinullstring* /* param */ AFirstBuffer,
   /* in */ const size_t          /* param */ AFirstSize,
   /* in */ const ansinullstring* /* param */ ASecondBuffer,
   /* in */ const size_t          /* param */ AFirstSize);
{
  comparison /* var */ Result = comparison__equal;
  // ---
   
  if (! coreansinullstrs__isemptysize(AFirstBuffer, AFirstSize) &&
     (! coreansinullstrs__isemptysize(ASecondBuffer, ASecondSize))
  {
    size_t    /* var */ AMaxSize;

    ansichar* /* var */ AA;
    ansichar* /* var */ BB;

    bool      /* var */ CanContinue;
    index_t   /* var */ Count;
    size_t    /* var */ AMaxSize;

    AMaxSize =
      corememory__oneminsize
        (AFirstSize, ASecondSize);

    AA = (ansichar*) A;
    BB = (ansichar*) B;

    Count = 0;
    CanContinue = true;

    while (CanContinue)
    {
      Result =
        coreansicharptrs__safecompare(AA, BB);

      AA++;
      BB++;

      Count++;

      CanContinue =
        (*AA != ANSINULLCHAR) &&
        (*BB != ANSINULLCHAR) &&
        (Result == comparison__lesser) &&
        (Count <= AMaxSize);
    } // while

    // if one parameter is a full substring,
    // from another, and are equal,
    // the shorter is sorted lesser
    if (Result == comparison__equal)
    {
      if (AFirstSize < ASecondSize)
      {
        Result = comparison__lesser;
      } 
      else
      {
        Result = comparison__greater;
      } 
    } 
  } // if

  // ---
  return Result;
} // func

/* friend */ bool /* func */ coreansinullstrs__safeareequal
  (/* in */ /* restricted */ const ansinullstring* /* param */ A,
   /* in */ /* restricted */ const ansinullstring* /* param */ B)
{
  bool /* var */ Result = false;
  // ---
     
  size_t /* var */ AFirstSize;
  size_t /* var */ ASecondSize;

  AFirstSize  = 1024;
  ASecondSize = 1024;

  Result =
    coreansinullstrs__safeareequalsize
     (A, AFirstSize, B, ASecondSize);

  // ---
  return Result;
} // func

/* friend */ bool /* func */ coreansinullstrs__safeareequalsize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AFirstBuffer,
   /* in */ const size_t                           /* param */ AFirstSize,
   /* in */ /* restricted */ const ansinullstring* /* param */ ASecondBuffer,
   /* in */ const size_t                           /* param */ AFirstSize);
{
  bool /* var */ Result = false;
  // ---
     
  if (! coreansinullstrs__isemptysize(AFirstBuffer, AFirstSize) &&
     (! coreansinullstrs__isemptysize(ASecondBuffer, ASecondSize))
  {
    size_t    /* var */ AMaxSize;

    ansichar* /* var */ AA;
    ansichar* /* var */ BB;

    bool      /* var */ CanContinue;
    index_t   /* var */ Count;
    size_t    /* var */ AMaxSize;

    AMaxSize =
      corememory__oneminsize
        (AFirstSize, ASecondSize);

    AA = (ansichar*) A;
    BB = (ansichar*) B;

    Count = 0;
    CanContinue = true;

    while (CanContinue)
    {
      Result =
        coreansicharptrs__safeareequal(AA, BB);

      AA++;
      BB++;

      Count++;

      CanContinue =
        (*AA != ANSINULLCHAR) &&
        (*BB != ANSINULLCHAR) &&
        (Result) &&
        (Count <= AMaxSize);
    } // while

    // if one parameter is a full substring,
    // from another, and are equal,
    // the shorter is sorted lesser
    if (Result)
    {
      Result =
       (AFirstSize == ASecondSize)
    } 
  } // if

  // ---
  return Result;
} // func 

/* friend */ bool /* func */ coreansinullstrs__overlapareequal
  (/* in */ const /* nonrestricted */ ansinullstring* /* param */ A,
   /* in */ const /* nonrestricted */ ansinullstring* /* param */ B)
{
  bool /* var */ Result = false;
  // ---
     
  size_t /* var */ AFirstSize;
  size_t /* var */ ASecondSize;

  AFirstSize  = 1024;
  ASecondSize = 1024;

  Result =
    coreansinullstrs__overlapareequalsize
     (A, AFirstSize, B, ASecondSize);

  // ---
  return Result;
} // func

/* friend */ bool /* func */ coreansinullstrs__overlapareequalsize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AFirstBuffer,
   /* in */ const size_t                           /* param */ AFirstSize,
   /* in */ /* restricted */ const ansinullstring* /* param */ ASecondBuffer,
   /* in */ const size_t                           /* param */ AFirstSize);
{
  bool /* var */ Result = false;
  // ---
     
  if (! coreansinullstrs__isemptysize(AFirstBuffer, AFirstSize) &&
     (! coreansinullstrs__isemptysize(ASecondBuffer, ASecondSize))
  {
    size_t    /* var */ AMaxSize;

    ansichar* /* var */ AA;
    ansichar* /* var */ BB;

    bool      /* var */ CanContinue;
    index_t   /* var */ Count;
    size_t    /* var */ AMaxSize;

    AMaxSize =
      corememory__oneminsize
        (AFirstSize, ASecondSize);

    AA = (ansichar*) A;
    BB = (ansichar*) B;

    Count = 0;
    CanContinue = true;

    while (CanContinue)
    {
      Result =
        coreansicharptrs__overlapareequal(AA, BB);

      AA++;
      BB++;

      Count++;

      CanContinue =
        (*AA != ANSINULLCHAR) &&
        (*BB != ANSINULLCHAR) &&
        (Result) &&
        (Count <= AMaxSize);
    } // while

    // if one parameter is a full substring,
    // from another, and are equal,
    // the shorter is sorted lesser
    if (Result)
    {
      Result =
       (AFirstSize == ASecondSize)
    } 
  } // if
  
  // ---
  return Result;
} // func 

/* friend */ bool /* func */ coreansinullstrs__safearesame
  (/* in */ /* restricted */ const ansinullstring* /* param */ A,
   /* in */ /* restricted */ const ansinullstring* /* param */ B)
{
  bool /* var */ Result = false;
  // ---
     
  size_t /* var */ AFirstSize;
  size_t /* var */ ASecondSize;

  AFirstSize  = 1024;
  ASecondSize = 1024;

  Result =
    coreansinullstrs__safearesamesize
     (A, AFirstSize, B, ASecondSize);

  // ---
  return Result;
} // func

/* friend */ bool /* func */ coreansinullstrs__safearesamesize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AFirstBuffer,
   /* in */ const size_t                           /* param */ AFirstSize,
   /* in */ /* restricted */ const ansinullstring* /* param */ ASecondBuffer,
   /* in */ const size_t                           /* param */ AFirstSize)
{
  bool /* var */ Result = false;
  // ---
     
  if (! coreansinullstrs__isemptysize(AFirstBuffer, AFirstSize) &&
     (! coreansinullstrs__isemptysize(ASecondBuffer, ASecondSize))
  {
    size_t    /* var */ AMaxSize;

    ansichar* /* var */ AA;
    ansichar* /* var */ BB;

    bool      /* var */ CanContinue;
    index_t   /* var */ Count;
    size_t    /* var */ AMaxSize;

    AMaxSize =
      corememory__oneminsize
        (AFirstSize, ASecondSize);

    AA = (ansichar*) A;
    BB = (ansichar*) B;

    Count = 0;
    CanContinue = true;

    while (CanContinue)
    {
      Result =
        coreansicharptrs__safearesame(AA, BB);

      AA++;
      BB++;

      Count++;

      CanContinue =
        (*AA != ANSINULLCHAR) &&
        (*BB != ANSINULLCHAR) &&
        (Result) &&
        (Count <= AMaxSize);
    } // while

    // if one parameter is a full substring,
    // from another, and are equal,
    // the shorter is sorted lesser
    if (Result)
    {
      Result =
       (AFirstSize == ASecondSize)
    } 
  } // if
  
  // ---
  return Result;
} // func 

/* friend */ bool /* func */ coreansinullstrs__overlaparesame
  (/* in */ const /* nonrestricted */ ansinullstring* /* param */ A,
   /* in */ const /* nonrestricted */ ansinullstring* /* param */ B)
{
  bool /* var */ Result = false;
  // ---
     
  size_t /* var */ AFirstSize;
  size_t /* var */ ASecondSize;

  AFirstSize  = 1024;
  ASecondSize = 1024;

  Result =
    coreansinullstrs__overlaparesamesize
     (A, AFirstSize, B, ASecondSize);

  // ---
  return Result;
} // func

/* friend */ bool /* func */ coreansinullstrs__overlaparesamesize
  (/* in */ /* nonrestricted */ const ansinullstring* /* param */ AFirstBuffer,
   /* in */ const size_t                              /* param */ AFirstSize,
   /* in */ /* nonrestricted */ const ansinullstring* /* param */ ASecondBuffer,
   /* in */ const size_t                              /* param */ AFirstSize)
{
  bool /* var */ Result = false;
  // ---
     
  if (! coreansinullstrs__isemptysize(AFirstBuffer, AFirstSize) &&
     (! coreansinullstrs__isemptysize(ASecondBuffer, ASecondSize))
  {
    size_t    /* var */ AMaxSize;

    ansichar* /* var */ AA;
    ansichar* /* var */ BB;

    bool      /* var */ CanContinue;
    index_t   /* var */ Count;
    size_t    /* var */ AMaxSize;

    AMaxSize =
      corememory__oneminsize
        (AFirstSize, ASecondSize);

    AA = (ansichar*) A;
    BB = (ansichar*) B;

    Count = 0;
    CanContinue = true;

    while (CanContinue)
    {
      Result =
        coreansicharptrs__safearesame(AA, BB);

      AA++;
      BB++;

      Count++;

      CanContinue =
        (*AA != ANSINULLCHAR) &&
        (*BB != ANSINULLCHAR) &&
        (Result) &&
        (Count <= AMaxSize);
    } // while

    // if one parameter is a full substring,
    // from another, and are equal,
    // the shorter is sorted lesser
    if (Result)
    {
      Result =
       (AFirstSize == ASecondSize)
    } 
  } // if
  
  // ---
  return Result;
} // func 

// ------------------

/* inline */ bool /* func */ coreansinullstrs__isempty
  (/* in */ const ansinullstring* /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
 
  Result = 
    ((ASource != NULL) && (*ASource != ANSINULLCHAR));
     
  // ---
  return Result;
} // func

/* inline */ bool /* func */ coreansinullstrs__isemptysize
  (/* in */ const ansinullstring* /* param */ ASourceBuffer, 
   /* in */ size_t                /* param */ ASourceSize)
{
  bool /* var */ Result = false;
  // ---
 
  Result = 
   (
    (ASourceBuffer == NULL) ||
    (*ASourceBuffer == ANSINULLCHAR) ||
    (ASourceSize < 1)
   );
     
  // ---
  return Result;
} // func

// ------------------

/* inline */ bool /* func */ coreansinullstrs__isrefempty
  (/* out */ ansinullstring** /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---
 
  Result = 
    (ASourceBuffer == (ansinullstring**) NULL);
  
  // ---
  return Result;
} // func

// ------------------

/* inline */ bool /* func */ coreansinullstrs__canconcatdest
  (/* in */ const ansinullstring* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  Result = 
    (ADest != NULL);

  // ---
  return Result;
} // func

/* inline */ bool /* func */ coreansinullstrs__canconcatdestsize
  (/* in */ const ansinullstring* /* param */ ADestBuffer, 
   /* in */ const size_t          /* param */ ADestSize)
{
  bool /* var */ Result = false;
  // ---

  Result = 
    (ADestBuffer != NULL) &&
    (ADestSize > 0);

  // ---
  return Result;
} // func

/* inline */ bool /* func */ coreansinullstrs__canconcatsrc
  (/* in */ const ansinullstring* /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---

  Result = 
    (ASource != NULL)
    (*Source != ANSINULLCHAR)

  // ---
  return Result;
} // func

/* inline */ bool /* func */ coreansinullstrs__canconcatsrcsize
  (/* in */ const ansinullstring* /* param */ ASourceBuffer, 
   /* in */ const size_t          /* param */ ASourceSize)
{
  bool /* var */ Result = false;
  // ---
 
  Result = 
    (ASourceBuffer != NULL) &&
    (*ASourceBuffer != ANSINULLCHAR) &&
    (ASourceSize > 0);

  // ---
  return Result;
} // func

// ------------------

count_t /* func */ coreansinullstrs__getlength
  (/* in */ const ansinullstring* /* param */ ASource)
{
  count_t /* var */ Result = 0;
  // ---
     
  Result = strlen(ASource);
  
  // ---
  return Result;
} // func

count_t /* func */ coreansinullstrs__getlengthsize
  (/* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ASourceMaxSize)
{
  count_t /* var */ Result = 0;
  // ---

  count_t   /* var */ AMaxSize  = 0;
  index_t   /* var */ EachIndex = 0;
  ansichar* /* var */ EachChar  = NULL;
  bool      /* var */ CanContinue = false;

  if (ASourceBuffer != NULL)
  {
    AMaxSize =
      coresystem__maxsize(ASourceMaxSize, 0);
    
    EachChar = (ansichar*) ASourceBuffer;
    
    CanContinue = true;
    while (CanContinue)
    {
      CanContinue =
        ((*EachChar != ANSINULLCHAR) &&
         (EachIndex < AMaxSize));
    
      if (!CanContinue)
      {
        EachIndex++;
        EachChar++;
      }
    } // while
    
    Result = --EachIndex;
  } // if

  // ---
  return Result;
} // func

// ------------------

/* inline */ void /* func */ coreansinullstrs__writenullmarker
  (/* inout */ const ansichar* /* param */ ADestBuffer)
{  
  if (ADestBuffer != NULL)
  {
    *ADestBuffer = ANSINULLCHAR;
  } // if
} // func

// ------------------

void /* func */ coreansinullstrs__duplicatechar
  (/* in */  const ansichar  /* param */ ASourceChar,
   /* in */  const count_t   /* param */ ASourceCount,
   /* out */ ansinullstring* /* param */ ADestBuffer)
{  
  if ((ADestBuffer != NULL) && (ASourceCount > 0))
  {
    corememory__fill
      ((ansinullstring*) ADestBuffer, ASourceCount, (byte_t) ASourceChar);
  } // if
} // func

void /* func */ coreansinullstrs__duplicatecharsize
  (/* in */  const ansichar  /* param */ ASourceChar,
   /* in */  const count_t   /* param */ ASourceCount,
   /* out */ ansinullstring* /* param */ ADestBuffer,
   /* in */  const size_t    /* param */ ADestSize)
{
  if (
    (ADestBuffer != NULL) &&
    (ASourceCount > 0) &&
    (ADestSize > ASourceCount) 
    )
  {
    corememory__fill
      ((ansinullstring*) ADestBuffer, ASourceCount, (byte_t) ASourceChar);
  } // if
} // func

void /* func */ coreansinullstrs__duplicatenstr
  (/* in */  const ansinullstring* /* param */ ASourceBuffer,
   /* in */  const count_t         /* param */ ASourceCount,
   /* out */ ansinullstring*       /* param */ ADestBuffer)
{  
  if ((ADestBuffer != NULL) &&
      (! coreansinullstrs__isempty(ASourceBuffer)) &&
      (ASourceCount > 0))
{
    size_t    /* var */ ASourceSize;

    ansichar* /* var */ EachSourceChar;
    ansichar* /* var */ EachDestChar;

    ASourceSize =
      coreansinullstrs__getlength(ASourceBuffer) + 1;

    // prepare destination
    EachDestChar = 
      (ansichar*) ADestBuffer;

    for (size_t /* var */ I = 0; I < ASourceCount; I++)
    {
      // start again copy
      EachSourceChar =
        (ansichar*) ASourceBuffer; 

      for (size_t /* var */ J = 0; J < ASourceSize; J++)
      {
        *EachDestChar = *EachSourceChar;

        EachSourceChar++;
        EachDestChar++;
      } // for
    } // for

    coreansinullstrs__writenullmarker
      (EachDestChar);
  } // if
} // func

void /* func */ coreansinullstrs__duplicatenstrsize
  (/* in */  const ansinullstring* /* param */ ASourceBuffer,
   /* in */  const count_t         /* param */ ASourceCount,
   /* out */ ansinullstring*       /* param */ ADestBuffer,
   /* in */  const size_t          /* param */ ADestSize)
{
  if (
    (ADestBuffer != NULL) &&
    (ASourceCount > 0)
    )
  {   
    bool      /* var */ CanContinue;

    ansichar* /* var */ EachSourceChar;
    ansichar* /* var */ EachDestChar;

    size_t    /* var */ ADestCount;
    size_t    /* var */ ASourceSize;

    size_t    /* var */ I;
    size_t    /* var */ J;

    // prepare destination
    EachDestChar = 
      (ansichar*) ADestBuffer;
    ADestCount = 0;

    ASourceSize =
      coreansinullstrs__getlength(ASourceBuffer) + 1;

    CanContinue = true;

    I = 0;
    while (CanContinue && (I < ASourceCount))
    {
      // start again copy
      EachSourceChar =
        (ansichar*) ASourceBuffer; 

      J = 0;
      while (CanContinue && (J < ASourceSize))
      {
        *EachDestChar = *EachSourceChar;

        EachSourceChar++;
        EachDestChar++;

        ADestCount++;
        CanContinue =
          (ADestCount < ADestSize);

        J++;
      } // while

      I++;
    } // while

    coreansinullstrs__writenullmarker
      (EachDestChar);
  } // if
} // func

// ------------------

count_t /* func */ coreansinullstrs__getcharsepcount
  (/* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const ansichar        /* param */ ASeparator)
{
  count_t /* var */ Result = 0;
  // ---

  size_t  /* var */ ASourceSize;

  ASourceSize = 1024;
  Result =
    coreansinullstrs__getcharsepcount
      (ASourceBuffer, ASourceSize, ASeparator);

  // ---
  return Result;
} // func

count_t /* func */ coreansinullstrs__getcharsepcountsize
  (/* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ASourceSize,
   /* in */ const ansichar        /* param */ ASeparator)
{
  count_t /* var */ Result = 0;
  // ---

  if ((! coreansinullstrs__isemptysize(ASourceBuffer, ASourceSize)) &&
      (! coreansichars__isnull(ASeparator)))
  {
    bool      /* var */ CanContinue;
    ansichar* /* var */ EachSourceChar;
    size_t    /* var */ ASourceCount;

    EachSourceChar =
      (ansichar*) ASourceBuffer; 
    ASourceCount = 0;

    CanContinue = true;

    I = 0;
    while (CanContinue)
    {
      if (*EachSourceChar == ASeparator)
      {
        Result++
      }

      ASourceCount++;
      EachSourceChar++;
      I++;

      CanContinue =
        ((! coreansichars__isnull(*EachSourceChar)) || 
         (ASourceCount > ASourceSize))
    } // while
  } // if

  // ---
  return Result;
} // func

count_t /* func */ coreansinullstrs__getcharsetsepcount
  (/* in */ const /* restricted */ ansinullstring* /* param */ ASourceBuffer,
   /* in */ const /* restricted */ ansicharset*    /* param */ ASepSetBuffer)
{
  count_t /* var */ Result = 0;
  // ---

  size_t  /* var */ ASourceSize;
  size_t  /* var */ ASepSetSize;

  ASourceSize = 1024;
  // ANSI character sets can only have 256 elements
  ASepSetSize = ANSICHARSETMAXSIZE;

  Result =
    coreansinullstrs__getcharsetsepcountsize
      (ASourceBuffer, ASourceSize, ASepSetBuffer, ASepSetSize);

  // ---
  return Result;
} // func

count_t /* func */ coreansinullstrs__getcharsetsepcountsize
  (/* in */ const /* restricted */ ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t                           /* param */ ASourceSize,
   /* in */ const /* restricted */ ansicharset*    /* param */ ASepSetBuffer,
   /* in */ const size_t                           /* param */ ASepSetSize)
{
  count_t /* var */ Result = 0;
  // ---

  if ((! coreansinullstrs__isemptysize(ASourceBuffer, ASourceSize)) &&
      (! coreansinullstrs__isemptysize(ASepSetBuffer, ASepSetSize)))
  {
    bool      /* var */ CanContinue;
    bool      /* var */ Match;

    ansichar* /* var */ EachSourceChar;

    size_t    /* var */ ASourceCount;

    EachSourceChar =
      (ansichar*) ASourceBuffer; 
    ASourceCount = 0;

    CanContinue = true;
    Match = true;

    while (CanContinue)
    {
      Match =
        coreansinullsets__ismembersize
          (ASepSetBuffer, ASepSetSize, *EachSourceChar);
      if (Match)
      {
        Result++;
      }

      ASourceCount++;

      CanContinue =
        ((! coreansichars__isnull(*EachSourceChar)) || 
         (ASourceCount > ASourceSize))
    } // while
  } // if

  // ---
  return Result;
} // func

count_t /* func */ coreansinullstrs__getstrsepcount
  (/* in */ const /* restricted */ ansinullstring* /* param */ ASourceBuffer,
   /* in */ const /* restricted */ ansinullstring* /* param */ ASeparatorBuffer)
{
  count_t /* var */ Result = 0;
  // ---

  size_t  /* var */ ASourceSize;
  size_t  /* var */ ASeparatorSize;

  ASourceSize = 1024;
  // ANSI character sets can only have 256 elements
  ASeparatorSize = ANSICHARSETMAXSIZE;

  Result =
    coreansinullstrs__getstrsepcountsize
      (ASourceBuffer, ASourceSize, ASeparatorBuffer, ASeparatorSize);

  // ---
  return Result;
} // func

count_t /* func */ coreansinullstrs__getstrsepcountsize
  (/* in */ const /* restricted */ ansinullstring* /* param */ ASourceBuffer, 
   /* in */ const size_t                           /* param */ ASourceSize,
   /* in */ const /* restricted */ ansinullstring* /* param */ ASeparatorBuffer,
   /* in */ const size_t                           /* param */ ASeparatorSize)
{
  count_t /* var */ Result = 0;
  // ---

  if ((! coreansinullstrs__isemptysize(ASourceBuffer, ASourceSize)) &&
      (! coreansinullstrs__isemptysize(ASeparatorBuffer, ASeparatorSize)))
  {
    bool      /* var */ CanContinue;
    bool      /* var */ Match;

    ansichar* /* var */ EachSourceChar;

    size_t    /* var */ ASourceCount;

    EachSourceChar =
      (ansichar*) ASourceBuffer; 
    ASourceCount = 0;

    CanContinue = true;
    Match = true;

    while (CanContinue)
    {
      Match =
        coreansinullsets__ismembersize
          (ASepSetBuffer, ASepSetSize, *EachSourceChar);
      if (Match)
      {
        Result++;
      }

      ASourceCount++;





    } // while
  } // if

  // ---
  return Result;
} // func

// ------------------

void /* func */ coreansinullstrs__clear
  (/* inout */ ansinullstring* /* param */ ADest)
{
  if (ADestBuffer != NULL)
  {
    *ADestBuffer = NULL;
  } // if
} // func 

void /* func */ coreansinullstrs__clearsize
  (/* inout */ ansinullstring* /* param */ ADestBuffer,
   /* in */    const size_t    /* param */ ADestSize)
{
  if (! coreansinullstrs__isemptysize(ADestBuffer, ADestSize))
  {
    corememory__fill
     ((pointer*) ADestBuffer, ADestSize, (byte_t) ANSINULLCHAR);
  } // if
} // func 

void /* func */ coreansinullstrs__assign
  (/* out */ /* restricted */ ansinullstring*       /* param */ ADest,
   /* in */  /* restricted */ const ansinullstring* /* param */ ASource)
{
  strcpy(ADest, ASource);

  /*
  size_t  var ADestSize;
  size_t var ASourceSize;

  ADestSize   = 1024;
  ASourceSize = 1024;

  coreansinullstrs__assignsize
    (ADest, ADestSize, ASource, ASourceSize);
  */
} // func 

void /* func */ coreansinullstrs__assignsize
  (/* out */ /* restricted */ ansinullstring*       /* param */ ADestBuffer,
   /* in */  const size_t                           /* param */ ADestSize,
   /* in */  /* restricted */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */  const size_t                           /* param */ ASourceSize)
{
  strncpy(ADestBuffer, ASourceBuffer, ASourceSize);
} // func 

void /* func */ coreansinullstrs__concat
  (/* out */ /* restricted */ ansinullstring*       /* param */ ADest,
   /* in */  /* restricted */ const ansinullstring* /* param */ ASource)
{
  if ((coreansinullstrs__canconcatsrcsize(ASourceBuffer) &&
      (coreansinullstrs__canconcatdestsize(ADestBuffer))
  {
    strcat(ADestBuffer, ASourceBuffer);
  } // if
} // func 

void /* func */ coreansinullstrs__concatsize
  (/* inout */ /* restricted */ ansinullstring*       /* param */ ADestBuffer,
   /* in */    const size_t                           /* param */ ADestSize,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */    const size_t                           /* param */ ASourceSize)
{
  if ((coreansinullstrs__canconcatsrcsize(ASourceBuffer, ASourceSize) &&
      (coreansinullstrs__canconcatdestsize(ADestBuffer, ADestSize))
  {
    strncat(ADestBuffer, ASourceBuffer, ASourceSize);
  } // if
} // func 

void /* func */ coreansinullstrs__addchar
  (/* inout */ ansinullstring* /* param */ ADest,
   /* in */    ansichar        /* param */ ASource);
{
  if (coreansinullstrs__canconcatdest(ADestBuffer))
  {
    bool             /* var */ Found;
    ansinullstring** /* var */ ADestBuffer;
    size_t*          /* var */ ADestSize;

    Found =
         coreansinullstrs__trysearchlastcharpairsize
       !!!  (ADest, &ADestBuffer, &ADestSize);
    if (Found)
    {

    }

    //strncat(ADestBuffer, ASourceBuffer, ASourceSize);
  } // if
} // func 

void /* func */ coreansinullstrs__addcharsize
  (/* inout */ ansinullstring* /* param */ ADestBuffer,
   /* in */    const size_t    /* param */ ADestSize,
   /* in */    const ansichar  /* param */ ASource)
{
  if ((coreansinullstrs__canconcatdestsize(ADestBuffer, ADestSize))
  {




  } // if
} // func 

ansinullstring* /* func */ coreansinullstrs__compactcopy
  (/* in */ const ansinullstring* /* param */ ASource)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__extendedcopy
  (/* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ASourceSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
  if ((ASourceBuffer != NULL) && (ASourceSize > 0))
  {
    //corememory__deallocate(&Result, ++ADestSize);
  } // if
    
  // ---
  return Result;
} // func

// ------------------

ansinullstring* /* func */ coreansinullstrs__ansichartonstrcopy
  (/* in */ const ansichar /* param */ ASource)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
  Result = corememory__clearallocate(2);
  *Result = ASource;

  // ---
  return Result;
} // func

ansichar /* func */ coreansinullstrs__nstrtoansicharcopy
  (/* in */ const ansinullstring* /* param */ ASource);
{
  ansichar /* var */ Result = ANSINULLCHAR;
  // ---
     
  if (ASource != NULL)
  {
    Result = *ASource;
  } // if

  // ---
  return Result;
} // func

// ------------------

void /* func */ coreansinullstrs__reversechange
  (/* in */ const ansinullstring* /* param */ ADestBuffer)
{
  if (ADestBuffer != NULL)
  {
    size_t /* var */ ADestSize = 0;
    
    ADestSize = strlen(ADestBuffer);
    if (ADestSize > 1)
    {
  	
    } // if
  } // if
} // func 

void /* func */ coreansinullstrs__reversechangesize
  (/* in */ const ansinullstring* /* param */ ADestBuffer,
   /* in */ const size_t          /* param */ ADestSize)
{
  if ((ADestBuffer != NULL) && (ADestSize > 1))
  {

  } // if
} // func 

ansinullstring* /* func */ coreansinullstrs__reversecopy
  (/* in */ const ansinullstring* /* param */ ASourceBuffer)
{
  ansinullstring* /* var */ Result = NULL;
  // ---

  if (ASourceBuffer != NULL)
  {
    size_t /* var */ ASourceSize = 0;
    
    ASourceSize = strlen(ASourceBuffer);
    if (ASourceSize > 1)
    {
      Result =
        corememory__allocate(ASourceSize + 1);
        
      ansichar* /* var */ S = NULL;
      ansichar* /* var */ D = NULL;
      
      S = (ansichar*) 
         coreansinullstrs__searchnullmarkersize
           (ASourceBuffer, ASourceSize);
      S--;
      
      D = (ansichar*) Result;
      
      for (int /* var*/ i = 0; i < ASourceSize; i++)
      {
         *D = *S;
         S++; D++;
      } // for
      
      *D = ANSINULLCHAR;
    } // if
  } // if
  
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__reversecopysize
  (/* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ASourceSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---

  if ((ASourceBuffer != NULL) && (ASourceSize > 1))
  {
    Result =
      corememory__allocate(ASourceSize + 1);
        
    ansichar* /* var */ S = NULL;
    ansichar* /* var */ D = NULL;
      
    S = (ansichar*) 
       coreansinullstrs__searchnullmarkersize
         (ASourceBuffer, ASourceSize);
    S--;
      
    D = (ansichar*) Result;
      
    for (int /* var*/ i = 0; i < ASourceSize; i++)
    {
       *D = *S;
       S++; D++;
    } // for
      
    *D = ANSINULLCHAR;
  } // if
  
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreansinullstrs__startswithchar
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ /* restricted */ const ansichar        /* param */ ANeedleBuffer)
{
  bool /* var */ Result = false;
  // ---

  size_t /* var */ AHaystackSize;

  AHaystackSize = 1024;

  Result =
    coreansinullstrs__startswithcharsize
      (AHaystackBuffer, AHaystackSize, ANeedleBuffer);

  // ---
  return Result;
} // func

bool /* func */ coreansinullstrs__startswithcharsize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                           /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansichar        /* param */ ANeedleBuffer)
{
  bool /* var */ Result = false;
  // ---

  if ((! coreansinullstrs__isempty(AHaystackBuffer)) &&
      (! coreansichars__isnull(ANeedleBuffer)))
  {
    Result =
      (*AHaystackBuffer == ANeedleBuffer);
  } // if

  // ---
  return Result;
} // func

bool /* func */ coreansinullstrs__startswithcharptr
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ /* restricted */ const ansichar*       /* param */ ANeedleBuffer)
{
  bool /* var */ Result = false;
  // ---

  size_t /* var */ AHaystackSize;

  AHaystackSize = 1024;

  Result =
    coreansinullstrs__startswithcharptrsize
      (AHaystackBuffer, AHaystackSize, ANeedleBuffer);

  // ---
  return Result;
} // func

bool /* func */ coreansinullstrs__startswithcharptrsize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                           /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansichar*       /* param */ ANeedleBuffer)
{
  bool /* var */ Result = false;
  // ---

  if ((! coreansinullstrs__isempty(AHaystackBuffer)) &&
      (! coreansichars__isnullmarkerptr(ANeedleBuffer)))
  {
    Result =
      (*AHaystackBuffer == *ANeedleBuffer);
  } // if

  // ---
  return Result;
} // func

bool /* func */ coreansinullstrs__startswithcharset
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ /* restricted */ const ansicharset     /* param */ ANeedleSetBuffer)
{
  bool /* var */ Result = false;
  // ---

  size_t /* var */ AHaystackSize;
  size_t /* var */ ANeedleSetSize;

  AHaystackSize = 1024;
  ANeedleSetSize = ANSICHARSETMAXSIZE;

  Result =
    coreansinullstrs__startswithcharsetsize
      (AHaystackBuffer, AHaystackSize, ANeedleSetBuffer, ANeedleSetSize);

  // ---
  return Result;
} // func

bool /* func */ coreansinullstrs__startswithcharsetsize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                           /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansicharset*    /* param */ ANeedleSetBuffer,
   /* in */ const size_t                           /* param */ ANeedleSetSize)
{
  bool /* var */ Result = false;
  // ---

  if ((! coreansinullstrs__isemptysize(AHaystackBuffer, AHaystackSize)) &&
      (! coreansicharsets__isemptysize(ANeedleSetBuffer, ANeedleSetSize)))
  {
    Result = 
      coreansinullsets__ismemberptrsize
        (ANeedleSetBuffer, ANeedleSetSize, AHaystackBuffer);
  } // if

  // ---
  return Result;
} // func

bool /* func */ coreansinullstrs__startswithstr
  (/* in */ /* restricted */ ansinullstring*       /* param */ AHaystackBuffer,
   /* in */ /* restricted */ const ansinullstring* /* param */ ANeedleBuffer)
{
  bool /* var */ Result = false;
  // ---

  size_t /* var */ AHaystackSize;
  size_t /* var */ ANeedleSize;

  AHaystackSize = 1024;
  ANeedleSize   = 1024;

  Result =
    coreansinullstrs__startswithstrsize
      (AHaystackBuffer, AHaystackSize, ANeedleBuffer, ANeedleSize);

  // ---
  return Result;
} // func

bool /* func */ coreansinullstrs__startswithstrsize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                           /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansinullstring* /* param */ ANeedleBuffer,
   /* in */ const size_t                           /* param */ ANeedleSize)
{
  bool /* var */ Result = false;
  // ---

  if ((! coreansinullstrs__isemptysize(AHaystackBuffer, AHaystackSize)) &&
      (! coreansicharsets__isemptysize(ANeedleBuffer, ANeedleSize)) &&
      (AHaystackSize >= ANeedleSize)) 
  {
    bool      /* var */ Match;
    bool      /* var */ CanContinue;

    ansichar* /* var */ EachHaystackChar;
    ansichar* /* var */ EachNeedleChar;

    size_t    /* var */ AMatchedCount;

    Match = false;
    CanContinue = false;

    EachHaystackChar = (ansichar*) AHaystackBuffer;
    EachNeedleChar   = (ansichar*) ANeedleSetBuffer;

    AMatchedCount    = 0;

    while (CanContinue)
    {
      Match = (*EachHaystackChar == *EachNeedleChar);
      if (Match)
      {
        AMatchedCount++
      } 

      EachHaystackChar++;
      EachNeedleChar++;

      CanContinue =
        (AMatchedCount < ANeedleSize) && (Match));
    } // while

    Result = Match;
  } // if

  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreansinullstrs__finisheswithchar
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ /* restricted */ const ansichar        /* param */ ANeedleBuffer)
{
  bool /* var */ Result = false;
  // ---

  size_t /* var */ AHaystackSize;

  AHaystackSize = 1024;

  Result =
    coreansinullstrs__finisheswithcharsize
      (AHaystackBuffer, AHaystackSize, ANeedleBuffer);

  // ---
  return Result;
} // func

bool /* func */ coreansinullstrs__finisheswithcharsize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                           /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansichar        /* param */ ANeedleBuffer)
{
  bool /* var */ Result = false;
  // ---

  if ((! coreansinullstrs__isemptysize(AHaystackBuffer, AHaystackSize)) &&
      (! coreansichars__isnullmarker(ANeedleBuffer)))
  {
    ansinullstring* /* var */ EachSourceCharPtr;

    EachSourceCharPtr =
      coreansinullstrs__searchlastptrofcharsize
        (AHaystackBuffer, AHaystackSize);

    Result =
      (*EachSourceCharPtr == ANeedleBuffer);
  } // if

  // ---
  return Result;
} // func

bool /* func */ coreansinullstrs__finisheswithcharptr
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ /* restricted */ const ansichar*       /* param */ ANeedleBuffer)
{
  bool /* var */ Result = false;
  // ---

  size_t /* var */ AHaystackSize;

  AHaystackSize = 1024;

  Result =
    coreansinullstrs__finisheswithcharptrsize
      (AHaystackBuffer, AHaystackSize, ANeedleBuffer);

  // ---
  return Result;
} // func

bool /* func */ coreansinullstrs__finisheswithcharptrsize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                           /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansichar*       /* param */ ANeedleBuffer)
{
  bool /* var */ Result = false;
  // ---

  if ((! coreansinullstrs__isemptysize(AHaystackBuffer, AHaystackSize)) &&
      (! coreansicharptrs__isnullmarker(ANeedleBuffer)))
  {
    ansinullstring* /* var */ EachSourceCharPtr;

    EachSourceCharPtr =
      coreansinullstrs__searchlastptrofcharsize
        (AHaystackBuffer, AHaystackSize);

    Result =
      (*EachSourceCharPtr == *ANeedleBuffer);
  } // if

  // ---
  return Result;
} // func

bool /* func */ coreansinullstrs__finisheswithcharset
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ /* restricted */ const ansicharset     /* param */ ANeedleSetBuffer)
{
  bool /* var */ Result = false;
  // ---

  size_t /* var */ AHaystackSize;

  AHaystackSize  = 1024;
  ANeedleSetSize = 1024;

  Result =
    coreansinullstrs__finisheswithcharsetsize
      (AHaystackBuffer, AHaystackSize, ANeedleSetBuffer, ANeedleSetSize);

  // ---
  return Result;
} // func

bool /* func */ coreansinullstrs__finisheswithcharsetsize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                           /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansicharset*    /* param */ ANeedleSetBuffer,
   /* in */ const size_t                           /* param */ ANeedleSetSize)
{
  bool /* var */ Result = false;
  // ---

  if ((! coreansinullstrs__isemptysize(AHaystackBuffer, AHaystackSize)) &&
      (! coreansinullstrs__isemptysize(ANeedleSetBuffer, ANeedleSetSize)))
  {
    ansinullstring* /* var */ EachSourceCharPtr;

    EachSourceCharPtr =
      coreansinullstrs__searchlastptrofcharsize
        (AHaystackBuffer, AHaystackSize);

    Result =
      coreansinullsets__ismemberptrsize
        (ANeedleSetBuffer, ANeedleSetSize, EachSourceCharPtr));
  } // if

  // ---
  return Result;
} // func

bool /* func */ coreansinullstrs__finisheswithstr
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ /* restricted */ const ansinullstring* /* param */ ANeedleBuffer)
{
  bool /* var */ Result = false;
  // ---

  size_t /* var */ AHaystackSize;
  size_t /* var */ ANeedleSize;

  AHaystackSize = 1024;
  ANeedleSize   = 1024;

  Result =
    coreansinullstrs__finisheswithcharsetsize
      (AHaystackBuffer, AHaystackSize, ANeedleBuffer, ANeedleSize);

  // ---
  return Result;
} // func

bool /* func */ coreansinullstrs__finisheswithstrsize
  (/* in */ /* restricted */ const ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                           /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansinullstring* /* param */ ANeedleBuffer,
   /* in */ const size_t                           /* param */ ANeedleSize)
{
  bool /* var */ Result = false;
  // ---

  if ((! coreansinullstrs__isemptysize(AHaystackBuffer, AHaystackSize)) &&
      (! coreansinullstrs__isemptysize(ANeedleBuffer, ANeedleSize)) &&
      (ANeedleSize <= AHaystackSize))
  {
    ansinullstring* /* var */ H;
    ansinullstring* /* var */ N;

    bool            /* var */ CanContinue;
    bool            /* var */ Match;

    index_t         /* var */ Count;

    H =
      coreansinullstrs__searchlastcharptr
        (AHaystackBuffer, AHaystackSize);
    N =
      coreansinullstrs__searchlastcharptr
        (ANeedleBuffer, ANeedleSize);

    Count = 0;
    CanContinue = true;

    while (CanContinue)
    {
      Match =
        (*N == *H);

      N--;
      H--;

      Count++;

      CanContinue =
        (Match && (Count <= ANeedleSize));
    } // while

    Result =
      Match;
  } // if

  // ---
  return Result;
} // func

// ------------------

void /* func */ coreansinullstrs__uppercasechange
  (/* inout */ ansinullstring* /* param */ ADestBuffer)
{
  size_t /* var */ AHaystackSize;
  size_t /* var */ ANeedleSize;

  AHaystackSize = 1024;
  ANeedleSize   = 1024;

  Result =
    coreansinullstrs__uppercasechangesize
      (AHaystackBuffer, AHaystackSize, ANeedleBuffer, ANeedleSize);
} // func 

void /* func */ coreansinullstrs__uppercasechangesize
  (/* inout */ ansinullstring* /* param */ ADestBuffer,
   /* in */    const size_t    /* param */ ADestSize)
{
  if (! coreansinullstrs__isemptysize(ADestBuffer))
  {
    ansichar* /* var */ EachChar;
    bool      /* var */ CanContinue;
    index_t   /* var */ Count;

    EachChar =
      (ansichar*) ADestBuffer;

    Count = 0;
    CanContinue = true;

    while (CanContinue)
    {
      *EachChar =
        coreansicharptrs__chartoupper
          (EachChar);

      EachChar++;
      Count++;

      CanContinue =
        (*EachChar != ANSINULLCHAR) && (Count <= ADestSize);
    } // while
  } // if
} // func 

void /* func */ coreansinullstrs__lowercasechange
  (/* inout */ ansinullstring* /* param */ ADestBuffer)
{
  size_t /* var */ AHaystackSize;
  size_t /* var */ ANeedleSize;

  AHaystackSize = 1024;
  ANeedleSize   = 1024;

  Result =
    coreansinullstrs__lowercasechangesize
      (AHaystackBuffer, AHaystackSize, ANeedleBuffer, ANeedleSize);
} // func 

void /* func */ coreansinullstrs__lowercasechangesize
  (/* inout */ ansinullstring* /* param */ ADestBuffer,
   /* in */    const size_t    /* param */ ADestSize)
{
  if (! coreansinullstrs__isemptysize(ADestBuffer))
  {
    ansichar* /* var */ EachChar;
    bool      /* var */ CanContinue;
    index_t   /* var */ Count;

    EachChar =
      (ansichar*) ADestBuffer;

    Count = 0;
    CanContinue = true;

    while (CanContinue)
    {
      *EachChar =
        coreansicharptrs__chartolower
          (EachChar);

      EachChar++;
      Count++;

      CanContinue =
        (*EachChar != ANSINULLCHAR) && (Count <= ADestSize);
    } // while
  } // if
} // func 

void /* func */ coreansinullstrs__capitalcasechange
  (/* inout */ ansinullstring* /* param */ ADestBuffer)
{
  size_t /* var */ AHaystackSize;
  size_t /* var */ ANeedleSize;

  AHaystackSize = 1024;
  ANeedleSize   = 1024;

  Result =
    coreansinullstrs__capitalcasechangesize
      (AHaystackBuffer, AHaystackSize, ANeedleBuffer, ANeedleSize);
} // func 

void /* func */ coreansinullstrs__capitalcasechangesize
  (/* inout */ ansinullstring* /* param */ ADestBuffer,
   /* in */    const size_t    /* param */ ADestSize)
{
  if (! coreansinullstrs__isemptysize(ADestBuffer))
  {
    ansichar* /* var */ EachChar;
    bool      /* var */ CanContinue;
    index_t   /* var */ Count;

    EachChar =
      (ansichar*) ADestBuffer;

    Count = 0;
    CanContinue = true;

    // change all characters to lowercase, first
    while (CanContinue)
    {
      *EachChar =
        coreansicharptrs__chartolower
          (EachChar);

      EachChar++;
      Count++;

      CanContinue =
        (*EachChar != ANSINULLCHAR) && (Count <= ADestSize);
    } // while

    // change first character
    EachChar =
      (ansichar*) ADestBuffer;
    *EachChar =
      coreansicharptrs__chartoupper
        (EachChar);
  } // if
} // func 

void /* func */ coreansinullstrs__reversecasechange
  (/* inout */ ansinullstring* /* param */ ADestBuffer)
{
  size_t /* var */ AHaystackSize;
  size_t /* var */ ANeedleSize;

  AHaystackSize = 1024;
  ANeedleSize   = 1024;

  Result =
    coreansinullstrs__reversecasechangesize
      (AHaystackBuffer, AHaystackSize, ANeedleBuffer, ANeedleSize);
} // func 

void /* func */ coreansinullstrs__reversecasechangesize
  (/* inout */ ansinullstring* /* param */ ADestBuffer,
   /* in */    const size_t    /* param */ ADestSize)
{
  if (! coreansinullstrs__isemptysize(ADestBuffer))
  {
    ansichar* /* var */ EachChar;
    bool      /* var */ CanContinue;
    index_t   /* var */ Count;

    EachChar =
      (ansichar*) ADestBuffer;

    Count = 0;
    CanContinue = true;

    while (CanContinue)
    {
      *EachChar =
        coreansicharptrs__chartorevcase
          (EachChar);

      EachChar++;
      Count++;

      CanContinue =
        (*EachChar != ANSINULLCHAR) && (Count <= ADestSize);
    } // while
  } // if
} // func 

// ------------------

ansinullstring* /* func */ coreansinullstrs__uppercasecopy
  (/* inout */ ansinullstring* /* param */ ASourceBuffer)
{
  size_t /* var */ ASourceSize;

  ASouceSize = 1024;

  Result =
    coreansinullstrs__uppercasecopysize
      (ASourceBuffer, ASourceSize);
} // func 

ansinullstring* /* func */ coreansinullstrs__uppercasecopysize
  (/* inout */ ansinullstring* /* param */ ASourceBuffer,
   /* in */    const size_t    /* param */ ASourceSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
  if (! coreansinullstrs__isemptysize(ASourceBuffer))
  {
    // first, obtain memory and space for marker
    Result =
      corememory__allocate
        (ADestSize + 1);

    ansichar* /* var */ EachSourceChar;
    ansichar* /* var */ EachDestChar;

    bool      /* var */ CanContinue;
    index_t   /* var */ Count;

    EachSourceChar =
      (ansichar*) ASourceBuffer;
    EachDestChar =
      (ansichar*) Result;

    Count = 0;
    CanContinue = true;

    while (CanContinue)
    {
      *EachDestChar =
         coreansicharptrs__chartoupper
           (EachSourceChar);

      EachSourceChar++;
      EachDestChar++;

      Count++;

      CanContinue =
        (*EachSourceChar != ANSINULLCHAR) && (Count <= ADestSize);
    } // while

    // finally, add null character marker
    coreansinullstrs__writenullmarker
      (EachDestChar);
  } // if

  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__lowercasecopy
  (/* inout */ ansinullstring* /* param */ ASourceBuffer)
{
  size_t /* var */ ASourceSize;

  ASouceSize = 1024;

  Result =
    coreansinullstrs__lowercasecopysize
      (ASourceBuffer, ASourceSize);
} // func 

ansinullstring* /* func */ coreansinullstrs__lowercasecopysize
  (/* inout */ ansinullstring* /* param */ ASourceBuffer,
   /* in */    const size_t    /* param */ ASourceSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
  if (! coreansinullstrs__isemptysize(ASourceBuffer))
  {
    // first, obtain memory and space for marker
    Result =
      corememory__allocate
        (ADestSize + 1);

    ansichar* /* var */ EachSourceChar;
    ansichar* /* var */ EachDestChar;

    bool      /* var */ CanContinue;
    index_t   /* var */ Count;

    EachSourceChar =
      (ansichar*) ASourceBuffer;
    EachDestChar =
      (ansichar*) Result;

    Count = 0;
    CanContinue = true;

    while (CanContinue)
    {
      *EachDestChar =
         coreansicharptrs__chartolower
           (EachSourceChar);

      EachSourceChar++;
      EachDestChar++;

      Count++;

      CanContinue =
        (*EachSourceChar != ANSINULLCHAR) && (Count <= ADestSize);
    } // while

    // finally, add null character marker
    coreansinullstrs__writenullmarker
      (EachDestChar);
  } // if

  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__capitalcasecopy
  (/* inout */ ansinullstring* /* param */ ASourceBuffer)
{
  size_t /* var */ ASourceSize;

  ASouceSize = 1024;

  Result =
    coreansinullstrs__capitalcopysize
      (ASourceBuffer, ASourceSize);
} // func 

ansinullstring* /* func */ coreansinullstrs__capitalcasecopysize
  (/* inout */ ansinullstring* /* param */ ASourceBuffer,
   /* in */    const size_t    /* param */ ASourceSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
  if (! coreansinullstrs__isemptysize(ASourceBuffer))
  {
    // first, obtain memory and space for marker
    Result =
      corememory__allocate
        (ADestSize + 1);

    ansichar* /* var */ EachSourceChar;
    ansichar* /* var */ EachDestChar;

    bool      /* var */ CanContinue;
    index_t   /* var */ Count;

    EachSourceChar =
      (ansichar*) ASourceBuffer;
    EachDestChar =
      (ansichar*) Result;

    Count = 0;
    CanContinue = true;

    // copy each character as lowercase
    while (CanContinue)
    {
      *EachDestChar =
         coreansicharptrs__chartolower
           (EachSourceChar);

      EachSourceChar++;
      EachDestChar++;

      Count++;

      CanContinue =
        (*EachSourceChar != ANSINULLCHAR) && (Count <= ADestSize);
    } // while

    EachSourceChar =
      (ansichar*) ASourceBuffer;
    EachDestChar =
      (ansichar*) Result;

    // change first character as uppercase
    *EachDestChar =
       coreansicharptrs__chartoupper
         (EachSourceChar);

    // finally, add null character marker
    coreansinullstrs__writenullmarker
      (EachDestChar);
  } // if

  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__reversecasecopy
  (/* inout */ ansinullstring* /* param */ ASourceBuffer)
{
  size_t /* var */ ASourceSize;

  ASouceSize = 1024;

  Result =
    coreansinullstrs__reversecasecopysize
      (ASourceBuffer, ASourceSize);
} // func 

ansinullstring* /* func */ coreansinullstrs__reversecasecopysize
  (/* inout */ ansinullstring* /* param */ ASourceBuffer,
   /* in */    const size_t    /* param */ ASourceSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
  if (! coreansinullstrs__isemptysize(ASourceBuffer))
  {
    // first, obtain memory and space for marker
    Result =
      corememory__allocate
        (ADestSize + 1);

    ansichar* /* var */ EachSourceChar;
    ansichar* /* var */ EachDestChar;

    bool      /* var */ CanContinue;
    index_t   /* var */ Count;

    EachSourceChar =
      (ansichar*) ASourceBuffer;
    EachDestChar =
      (ansichar*) Result;

    Count = 0;
    CanContinue = true;

    while (CanContinue)
    {
      *EachDestChar =
         coreansicharptrs__chartorevcase
           (EachSourceChar);

      EachSourceChar++;
      EachDestChar++;

      Count++;

      CanContinue =
        (*EachSourceChar != ANSINULLCHAR) && (Count <= ADestSize);
    } // while

    // finally, add null character marker
    coreansinullstrs__writenullmarker
      (EachDestChar);
  } // if

  // ---
  return Result;
} // func

// ------------------

ansinullstring* /* func */ coreansinullstrs__leftcopy
  (/* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ADestSize)
{
  size_t /* var */ ASourceSize;

  ASouceSize = 1024;

  Result =
    coreansinullstrs__leftcopysize
      (ASourceBuffer, ASourceSize, ADestSize);
} // func 

ansinullstring* /* func */ coreansinullstrs__leftcopysize
  (/* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ASourceSize,
   /* in */ const size_t          /* param */ ADestSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
  if (! coreansinullstrs__isemptysize(ASourceBuffer, ASourceSize))
  {
    // first, obtain memory and space for marker
    Result =
      corememory__allocate
        (ADestSize + 1);

    ansichar* /* var */ EachSourceChar;
    ansichar* /* var */ EachDestChar;

    bool      /* var */ CanContinue;
    index_t   /* var */ Count;
    size_t    /* var */ AMaxSize;

    EachSourceChar =
      (ansichar*) ASourceBuffer;
    EachDestChar =
      (ansichar*) Result;

    AMaxSize =
      corememory__oneminsize
        (ASourceSize, ADestSize);

    Count = 0;
    CanContinue = true;

    while (CanContinue)
    {
      *EachDestChar =
         coreansicharptrs__chartorevcase
           (EachSourceChar);

      EachSourceChar++;
      EachDestChar++;

      Count++;

      CanContinue =
        (*EachSourceChar != ANSINULLCHAR) && (Count <= AMaxSize);
    } // while

    // finally, add null character marker
    coreansinullstrs__writenullmarker
      (EachDestChar);
  } // if

  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__rightcopy
  (/* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ADestSize)
{
  size_t /* var */ ASourceSize;

  ASouceSize = 1024;

  Result =
    coreansinullstrs__rightcopysize
      (ASourceBuffer, ASourceSize, ADestSize);
} // func 

ansinullstring* /* func */ coreansinullstrs__rightcopysize
  (/* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ASourceSize,
   /* in */ const size_t          /* param */ ADestSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
  if (! coreansinullstrs__isemptysize(ASourceBuffer, ASourceSize))
  {
    // first, obtain memory and space for marker
    Result =
      corememory__allocate
        (ADestSize + 1);

    ansichar* /* var */ EachSourceChar;
    ansichar* /* var */ EachDestChar;

    bool      /* var */ CanContinue;
    index_t   /* var */ Count;
    size_t    /* var */ AMaxSize;

    // seek 
    EachSourceChar = (ansichar*) 
      coreansinullstrs__searchlastptrofcharsize
        (ASourceBuffer, ASourceSize + 1);
    // go back
    EachSourceChar =
      (EachSourceChar - ASourceSize;

    EachDestChar = (ansichar*) 
      Result;

    AMaxSize =
      corememory__oneminsize
        (ASourceSize, ADestSize);

    Count = 0;
    CanContinue = true;

    while (CanContinue)
    {
      *EachDestChar =
         coreansicharptrs__chartorevcase
           (EachSourceChar);

      EachSourceChar++;
      EachDestChar++;

      Count++;

      CanContinue =
        (*EachSourceChar != ANSINULLCHAR) && (Count <= AMaxSize);
    } // while

    // finally, add null character marker
    coreansinullstrs__writenullmarker
      (EachDestChar);
  } // if

  // ---
  return Result;
} // func

// ------------------

void /* func */ coreansinullstrs__lefttrimchange
  (/* inout */ ansinullstring* /* param */ ADestBuffer)
{
  size_t /* var */ ADestSize;

  ADestSize = 1024;

  coreansinullstrs__lefttrimchangesize
    (ADestBuffer, ADestSize);
} // func 

void /* func */ coreansinullstrs__lefttrimchangesize
  (/* inout */ ansinullstring* /* param */ ADestBuffer,
   /* in */  const size_t      /* param */ ADestSize)
{
  if (! coreansinullstrs__isemptysize(ADestBuffer, ADestSize))
  {

  } // if
} // func 

void /* func */ coreansinullstrs__righttrimchange
  (/* inout */ ansinullstring* /* param */ ADestBuffer)
{
  size_t /* var */ ADestSize;

  ADestSize = 1024;

  coreansinullstrs__lefttrimchangesize
    (ADestBuffer, ADestSize);
} // func 

void /* func */ coreansinullstrs__righttrimchangesize
  (/* inout */ ansinullstring* /* param */ ADestBuffer,
   /* in */  const size_t      /* param */ ADestSize)
{
  if (! coreansinullstrs__isemptysize(ADestBuffer, ADestSize))
  {

  } // if
} // func 

void /* func */ coreansinullstrs__trimchange
  (/* inout */ ansinullstring* /* param */ ADestBuffer)
{
  size_t /* var */ ADestSize;

  ADestSize = 1024;

  coreansinullstrs__trimchangesize
    (ADestBuffer, ADestSize);
} // func 

void /* func */ coreansinullstrs__trimchangesize
  (/* inout */ ansinullstring* /* param */ ADestBuffer,
   /* in */  const size_t      /* param */ ADestSize)
{
  if (! coreansinullstrs__isemptysize(ADestBuffer, ADestSize))
  {

  } // if
} // func 

// ------------------

ansinullstring* /* func */ coreansinullstrs__lefttrimcopy
  (/* inout */ ansinullstring* /* param */ ASourceBuffer)
{
  ansinullstring* /* var */ Result = NULL;
  // ---

  size_t /* var */ ASourceSize;

  ASourceSize = 1024;

  Result =
    coreansinullstrs__lefttrimcopysize
      (ASourceBuffer, ASourceSize);
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__lefttrimcopysize
  (/* inout */ ansinullstring* /* param */ ASourceBuffer,
   /* in */    const size_t    /* param */ ASourceSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
  if (! coreansinullstrs__isemptysize(ASourceBuffer, ASourceSize))
  {
    const ansinullstring* /* var */ ABlankSet;

    ansichar*       /* var */ EachSourceChar;
    ansichar*       /* var */ EachDestChar;

    size_t          /* var */ ASourceCount;
    size_t          /* var */ ADestCount;

    bool            /* var */ CanContinue;
    bool            /* var */ IsBlank;

    Result =
      coreansinullstrs__newstr(ASourceSize);

    ASourceCount = 0;
    ADestCount   = 0;

    EachSourceChar =
      (ansichar*) ASourceBuffer;
    EachDestChar   =
      (ansichar*) Result;

    ABlankSet = 
      coreansinullstrs__blankset();

    // skip leading / "left" blanks
    CanContinue = true;
    while (CanContinue)
    {
      IsBlank =
        (coreansinullsets__ismemberptr
           (ABlankSet, EachSourceChar);
      if (IsBlank)
      {
        EachSourceChar++;
        ASourceCount++;
      }

      CanContinue =
        (IsBlank) &&
        (*EachSourceChar != ANSINULLCHAR) &&
        (ASourceCount <= ASourceSize);
    } // while

    // copy the following characters,
    // including blanks
    CanContinue =
      (*EachSourceChar != ANSINULLCHAR) &&
      (ASourceCount <= ASourceSize);
    while (CanContinue)
    {
      *EachDestChar = *EachSourceChar;

      ADestCount++;
      ASourceCount++;

      EachSourceChar++;
      EachDestChar++;

      CanContinue =
        (*EachSourceChar != ANSINULLCHAR) &&
        (ASourceCount <= ASourceSize);
    } // while

    // assign null character marker
    coreansinullstrs__writenullmarker
      (EachDestChar);
  } // if

  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__righttrimcopy
  (/* inout */ ansinullstring* /* param */ ASourceBuffer)
{
  ansinullstring* /* var */ Result = NULL;
  // ---

  size_t /* var */ ASourceSize;

  ASourceSize = 1024;

  Result =
    coreansinullstrs__righttrimcopysize
      (ASourceBuffer, ASourceSize);
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__righttrimcopysize
  (/* inout */ ansinullstring* /* param */ ASourceBuffer,
   /* in */  const size_t      /* param */ ASourceSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
  if (! coreansinullstrs__isemptysize(ASourceBuffer, ASourceSize))
  {
    const ansinullstring* /* var */ ABlankSet;

    ansichar*       /* var */ EachSourceChar;
    ansichar*       /* var */ EachDestChar;

    size_t          /* var */ ASourceCount;
    size_t          /* var */ ADestCount;

    bool            /* var */ CanContinue;
    bool            /* var */ IsBlank;

    ansinullstring* /* var */ ATempBuffer;

    ATempBuffer =
      coreansinullstrs__newstr(ASourceSize);

    ASourceCount = 0;
    ADestCount = 0;

    ABlankSet = 
      coreansinullstrs__blankset();

    // get pointer to last character,
    // from a non empty nullstring
    EachSourceChar = (ansichar*) 
      coreansinullstrs__searchlastptrofcharsize
        (ASourceBuffer, ASourceSize);
    EachDestChar   =
      (ansichar*) ATempBuffer;

    // skip leading / "left" blanks
    CanContinue = true;
    while (CanContinue)
    {
      IsBlank =
        (coreansinullsets__ismemberptr
           (ABlankSet, EachSourceChar);
      if (IsBlank)
      {
        EachSourceChar--;
        ASourceCount++;
      }

      CanContinue =
        (IsBlank) &&
        (*EachSourceChar != ANSINULLCHAR) &&
        (ASourceCount <= ASourceSize);
    } // while

    // copy the following characters,
    // including blanks
    CanContinue =
      (*EachSourceChar != ANSINULLCHAR) &&
      (ASourceCount <= ASourceSize);
    while (CanContinue)
    {
      *EachDestChar = *EachSourceChar;

      ADestCount++;
      ASourceCount++;

      EachSourceChar--;
      EachDestChar++;

      CanContinue =
        (*EachSourceChar != ANSINULLCHAR) &&
        (ASourceCount <= ASourceSize);
    } // while

    // assign null character marker
    coreansinullstrs__writenullmarker
      (EachDestChar);

    // the result was storef reversed,
    // in a temporal buffer
    Result =
      coreansinullstrs__reversecopysize
        (ATempBuffer, ADestCount);

    // release temporal buffer
    coreansinullstrs__dropstr
      (&ATempBuffer, ASourceSize);
  } // if

  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__trimcopy
  (/* inout */ ansinullstring* /* param */ ASourceBuffer)
{
  ansinullstring* /* var */ Result = NULL;
  // ---

  size_t /* var */ ASourceSize;

  ASourceSize = 1024;

  Result =
    coreansinullstrs__trimcopysize
      (ASourceBuffer, ASourceSize);
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__trimcopysize
  (/* inout */ ansinullstring* /* param */ ASourceBuffer,
   /* in */    const size_t    /* param */ ASourceSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---

  if (! coreansinullstrs__isemptysize(ASourceBuffer, ASourceSize))
  {
    ansinullstring* /* var */ TempBuffer;

    // remove leading / "first" blanks
    TempBuffer =
      coreansinullstrs__lefttrimcopysize
        (ASourceBuffer, ASourceSize);

    // remove trailing / "last" blanks
    Return =
      coreansinullstrs__righttrimcopysize
        (TempBuffer, ASourceSize);

    coreansinullstrs__dropstr
      (TempBuffer, ASourceSize);
  } // if

  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreansinullstrs__trysearchnullmarker
  (/* in */ /* restricted */ ansinullstring*  /* param */ AHaystack,
   /* in */ /* restricted */ ansichar**       /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  ansicharsizedptr* /* var */ ASource;


  size_t  /* var */ ADestSize;

  ADestSize = 1024;

  Result =
    coreansinullstrs__trysearchnullmarkerpair
      (AHaystack, ADest, &ADestSize);

  // ---
  return Result;
} // func

bool /* func */ coreansinullstrs__trysearchnullmarkersize
  (/* in */ /* restricted */ ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                     /* param */ AHaystackSize,
   /* in */ /* restricted */ ansichar**      /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  size_t  /* var */ AHaystackSize;
  size_t  /* var */ ADestSize;

  AHaystackSize = 1024;
  ADestSize     = 1024;

  Result =
    coreansinullstrs__trysearchnullmarkerpairsize
      (AHaystack, AHaystackSize, ADest, &ADestSize);

  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__searchnullmarker
  (/* in */ /* restricted */ ansinullstring* /* param */ AHaystack)
{
  bool /* var */ Result = false;
  // ---
     
  /* discard */ coreansinullstrs__trysearchnullmarker
    (AHaystack, &Result);

  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__searchnullmarkersize
  (/* in */ /* restricted */ ansinullstring* /* param */ AHaystack,
   /* in */ const size_t                     /* param */ AHaystackSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
  /* discard */ coreansinullstrs__trysearchnullmarkersize
    (AHaystack, AHaystackSize, &Result);
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreansinullstrs__trysearchlastcharptr
  (/* in */ /* restricted */ ansinullstring*  /* param */ AHaystack,
   /* in */ /* restricted */ ansinullstring** /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---

  bool /* var */ CanContinue = false;
  bool /* var */ Found = false;

  ansichar* /* var */ P = NULL;
  size_t    /* var */ AHaystackSize = 0;
  size_t    /* var */ ThisIndex = 0;

  Result =
    (AHaystack != NULL) && (ADest!= NULL);
  if (Result)
  {
    // in case of any error
    // return "NULL"
    *ADest = NULL;
  
    P = (ansichar*) AHaystack;
    AHaystackSize = 1024;
    ThisIndex = 0;
    
    CanContinue = true;
    Found = false;
       
    while (CanContinue)
    {
      Found = (*P == ANSINULLCHAR);
      
      CanContinue =
        (!Found && (ThisIndex >= AHaystackSize));
        
      if (CanContinue)
      {
        P++;
        ThisIndex++;
      }
    } // while
    
    // don't go back,
    // if is empty, only null marker
    Result = 
      (Found && (ThisIndex > 1));
    if (Result)
    {
      *ADest = P;
    }
  } // if
  
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__searchlastcharptr
  (/* in */ /* restricted */ ansinullstring* /* param */ AHaystack)
{
  bool /* var */ Result = false;
  // ---
     
  /* discard */ coreansinullstrs__trysearchlastptrofchar
    (AHaystack, &Result);

  // ---
  return Result;
} // func

// ------------------

ansinullstring* /* func */ coreansinullstrs__firstptrofchar
  (/* in */ /* restricted */ ansinullstring* /* param */ AHaystack,
   /* in */ /* restricted */ const ansichar* /* param */ ANeedle)
{
  ansinullstring* /* var */ Result = NULL;
  // ---

  bool              /* var */ Found;

  ansicharsizedptr* /* var */ AHaystackPtr;
  ansichar          /* var */ ANeedlePtr;
  ansicharidxptr*   /* var */ ADestPtr;

  // ---

  AHaystackPtr =
   coreacharszptrs__new();
  ADestPtr   =
   coreacharidxptrs__new();

  coreacharszptrs__pack
    (&AHaystackPtr, AHaystack, 1024);

  Found =
    coreansinullstrs__tryfirstptrofchar
      (AHaystackPtr, ANeedle, ADestPtr);
  if (Found)
  {
    Result = ADestPtr->ItemPtr;
  } 

  coreacharidxptrs__drop
    (&ADestPtr);

  coreacharszptrs__drop
    (&AHaystackPtr);

  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__firstptrofcharsize
  (/* in */ /* restricted */ ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                     /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansichar* /* param */ ANeedle)
{
  ansinullstring* /* var */ Result = NULL;
  // ---

  bool              /* var */ Found;

  ansicharsizedptr* /* var */ AHaystackPtr;
  ansichar          /* var */ ANeedlePtr;
  ansicharidxptr*   /* var */ ADestPtr;

  // ---

  AHaystackPtr =
   coreacharszptrs__new();
  ADestPtr   =
   coreacharidxptrs__new();

  coreacharszptrs__pack
    (&AHaystackPtr, AHaystackBuffer, AHaystackSize);

  Found =
    coreansinullstrs__tryfirstptrofchar
      (AHaystackPtr, ANeedle, ADestPtr);
  if (Found)
  {
    Result = ADestPtr->ItemPtr;
  } 

  coreacharidxptrs__drop
    (&ADestPtr);

  coreacharszptrs__drop
    (&AHaystackPtr);

  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__firstptrofcharset
  (/* in */ /* restricted */ ansinullstring*    /* param */ AHaystack,
   /* in */ /* restricted */ const ansicharset* /* param */ ANeedleSet);
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
  bool              /* var */ Found;

  ansicharsizedptr* /* var */ AHaystackPtr;
  ansicharsizedptr* /* var */ ANeedleSetPtr;
  ansicharidxptr*   /* var */ ADestPtr;

  // ---

  AHaystackPtr =
   coreacharszptrs__new();
  ANeedleSetPtr =
   coreacharszptrs__new();
  ADestPtr   =
   coreacharidxptrs__new();

  coreacharszptrs__pack
    (&AHaystackPtr, AHaystack, 1024);
  coreacharszptrs__pack
    (&ANeedleSetPtr, ANeedleSet, ANSICHARSETMAXSIZE);

  Found =
    coreansinullstrs__tryfirstptrofcharset
      (AHaystackPtr, ANeedleSetPtr, ADestPtr);
  if (Found)
  {
    Result = ADestPtr->ItemPtr;
  } 

  coreacharidxptrs__drop
    (&ADestPtr);
  coreacharszptrs__drop
    (&ANeedleSetPtr);
  coreacharszptrs__drop
    (&AHaystackPtr);

  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__firstptrofcharsetsize
  (/* in */ /* restricted */ ansinullstring*    /* param */ AHaystackBuffer,
   /* in */ const size_t                        /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansicharset* /* param */ ANeedleSetBuffer,
   /* in */ const size_t                        /* param */ ANeedleSetSize);
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
  bool              /* var */ Found;

  ansicharsizedptr* /* var */ AHaystackPtr;
  ansicharsizedptr* /* var */ ANeedleSetPtr;
  ansicharidxptr*   /* var */ ADestPtr;

  // ---

  AHaystackPtr =
   coreacharszptrs__new();
  ANeedleSetPtr =
   coreacharszptrs__new();
  ADestPtr   =
   coreacharidxptrs__new();

  coreacharszptrs__pack
    (&AHaystackPtr, AHaystackBuffer, AHaystackSize);
  coreacharszptrs__pack
    (&ANeedleSetPtr, ANeedleSetBuffer, ANeedleSize);

  Found =
    coreansinullstrs__tryfirstptrofset
      (AHaystackPtr, ANeedlePtr, ADestPtr);
  if (Found)
  {
    Result = ADestPtr->ItemPtr;
  } 

  coreacharidxptrs__drop
    (&ADestPtr);
  coreacharszptrs__drop
    (&ANeedleSetPtr);
  coreacharszptrs__drop
    (&AHaystackPtr);

  // ---
  return Result;
} // func

// ------------------

ansinullstring* /* func */ coreansinullstrs__firstptrofstrequal
  (/* in */ /* restricted */ ansinullstring*    /* param */ AHaystack,
   /* in */ /* restricted */ const ansicharset* /* param */ ANeedle);
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
  bool              /* var */ Found;

  ansicharsizedptr* /* var */ AHaystackPtr;
  ansicharsizedptr* /* var */ ANeedlePtr;
  ansicharidxptr*   /* var */ ADestPtr;

  // ---

  AHaystackPtr =
   coreacharszptrs__new();
  ANeedlePtr =
   coreacharszptrs__new();
  ADestPtr   =
   coreacharidxptrs__new();

  coreacharszptrs__pack
    (&AHaystackPtr, AHaystack, 1024);
  coreacharszptrs__pack
    (&ANeedlePtr, ANeedle, 1024);

  Found =
    coreansinullstrs__tryfirstptrofstr
      (AHaystackPtr, ANeedlePtr, ADestPtr);
  if (Found)
  {
    Result = ADestPtr->ItemPtr;
  } 

  coreacharidxptrs__drop
    (&ADestPtr);
  coreacharszptrs__drop
    (&ANeedlePtr);
  coreacharszptrs__drop
    (&AHaystackPtr);

  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__firstptrofstrequalsize
  (/* in */ /* restricted */ ansinullstring*    /* param */ AHaystackBuffer,
   /* in */ const size_t                        /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansicharset* /* param */ ANeedleBuffer,
   /* in */ const size_t                        /* param */ ANeedleSize);
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
  bool              /* var */ Found;

  ansicharsizedptr* /* var */ AHaystackPtr;
  ansicharsizedptr* /* var */ ANeedlePtr;
  ansicharidxptr*   /* var */ ADestPtr;

  // ---

  AHaystackPtr =
   coreacharszptrs__new();
  ANeedlePtr =
   coreacharszptrs__new();
  ADestPtr   =
   coreacharidxptrs__new();

  coreacharszptrs__pack
    (&AHaystackPtr, AHaystackBuffer, AHaystackSize);
  coreacharszptrs__pack
    (&ANeedlePtr, ANeedleBuffer, ANeedleSize);

  Found =
    coreansinullstrs__tryfirstptrofstr
      (AHaystackPtr, ANeedlePtr, ADestPtr);
  if (Found)
  {
    Result = ADestPtr->ItemPtr;
  } 

  coreacharidxptrs__drop
    (&ADestPtr);
  coreacharszptrs__drop
    (&ANeedlePtr);
  coreacharszptrs__drop
    (&AHaystackPtr);

  // ---
  return Result;
} // func

// ------------------

ansinullstring* /* func */ coreansinullstrs__firstptrofstrsame
  (/* in */ /* restricted */ ansinullstring*    /* param */ AHaystack,
   /* in */ /* restricted */ const ansicharset* /* param */ ANeedle);
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
  bool              /* var */ Found;

  ansicharsizedptr* /* var */ AHaystackPtr;
  ansicharsizedptr* /* var */ ANeedlePtr;
  ansicharidxptr*   /* var */ ADestPtr;

  // ---

  AHaystackPtr =
   coreacharszptrs__new();
  ANeedlePtr =
   coreacharszptrs__new();
  ADestPtr   =
   coreacharidxptrs__new();

  coreacharszptrs__pack
    (&AHaystackPtr, AHaystack, 1024);
  coreacharszptrs__pack
    (&ANeedlePtr, ANeedle, 1024);

  Found =
    coreansinullstrs__tryfirstptrofstr
      (AHaystackPtr, ANeedlePtr, ADestPtr);
  if (Found)
  {
    Result = ADestPtr->ItemPtr;
  } 

  coreacharidxptrs__drop
    (&ADestPtr);
  coreacharszptrs__drop
    (&ANeedlePtr);
  coreacharszptrs__drop
    (&AHaystackPtr);

  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__firstptrofstrsamesize
  (/* in */ /* restricted */ ansinullstring*    /* param */ AHaystackBuffer,
   /* in */ const size_t                        /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansicharset* /* param */ ANeedleBuffer,
   /* in */ const size_t                        /* param */ ANeedleSize);
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
  bool              /* var */ Found;

  ansicharsizedptr* /* var */ AHaystackPtr;
  ansicharsizedptr* /* var */ ANeedlePtr;
  ansicharidxptr*   /* var */ ADestPtr;

  // ---

  AHaystackPtr =
   coreacharszptrs__new();
  ANeedlePtr =
   coreacharszptrs__new();
  ADestPtr   =
   coreacharidxptrs__new();

  coreacharszptrs__pack
    (&AHaystackPtr, AHaystackBuffer, AHaystackSize);
  coreacharszptrs__pack
    (&ANeedlePtr, ANeedleBuffer, ANeedleSize);

  Found =
    coreansinullstrs__tryfirstptrofstr
      (AHaystackPtr, ANeedlePtr, ADestPtr);
  if (Found)
  {
    Result = ADestPtr->ItemPtr;
  } 

  coreacharidxptrs__drop
    (&ADestPtr);
  coreacharszptrs__drop
    (&ANeedlePtr);
  coreacharszptrs__drop
    (&AHaystackPtr);

  // ---
  return Result;
} // func

// ------------------

ansinullstring* /* func */ coreansinullstrs__lastptrofchar
  (/* in */ /* restricted */ ansinullstring* /* param */ AHaystack,
   /* in */ /* restricted */ const ansichar* /* param */ ANeedle)
{
  ansinullstring* /* var */ Result = NULL;
  // ---

  struct ansicharsizedptr* /* param */ AHaystackPtr;
  struct ansicharidxptr*   /* param */ ADestPtr;

  // ---

  AHaystackPtr =
   coreacharszptrs__new();
  ADestPtr   =
   coreacharidxptrs__new();

  coreacharszptrs__pack
    (&AHaystackPtr, AHaystack, 1024);

  Result =
    coreansearch__trylastptrofchar
      (AHaystackPtr, ANeedle, ADestPtr);
  if (Found)
  {
    Result = ADestPtr->ItemPtr;
  } 

  coreacharidxptrs__drop
    (&ADestPtr);
  coreacharszptrs__drop
    (&AHaystackPtr);

  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__lastptrofcharsize
  (/* in */ /* restricted */ ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                     /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansichar  /* param */ ANeedle)
{
  ansinullstring* /* var */ Result = NULL;
  // ---

  struct ansicharsizedptr* /* param */ AHaystackPtr;
  struct ansicharidxptr*   /* param */ ADestPtr;

  // ---

  AHaystackPtr =
   coreacharszptrs__new();
  ADestPtr   =
   coreacharidxptrs__new();

  coreacharszptrs__pack
    (&AHaystackPtr, AHaystackBuffer, AHaystackSize);

  Result =
    coreansearch__trylastptrofchar
      (AHaystackPtr, ANeedle, ADestPtr);
  if (Found)
  {
    Result = ADestPtr->ItemPtr;
  } 

  coreacharidxptrs__drop
    (&ADestPtr);
  coreacharszptrs__drop
    (&AHaystackPtr);

  // ---
  return Result;
} // func

// ------------------

ansinullstring* /* func */ coreansinullstrs__lastptrofcharptr
  (/* in */ /* restricted */ ansinullstring* /* param */ AHaystack,
   /* in */ /* restricted */ const ansichar* /* param */ ANeedlePtr)
{
  ansinullstring* /* var */ Result = NULL;
  // ---

  struct ansicharsizedptr* /* param */ AHaystackPtr;
  struct ansicharidxptr*   /* param */ ADestPtr;

  // ---

  AHaystackPtr =
   coreacharszptrs__new();
  ADestPtr   =
   coreacharidxptrs__new();

  coreacharszptrs__pack
    (&AHaystackPtr, AHaystackBuffer, 1024);

  Result =
    coreansearch__trylastptrofcharptr
      (AHaystackPtr, ANeedlePtr, ADestPtr);
  if (Found)
  {
    Result = ADestPtr->ItemPtr;
  } 

  coreacharidxptrs__drop
    (&ADestPtr);
  coreacharszptrs__drop
    (&AHaystackPtr);

  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__lastptrofcharptrsize
  (/* in */ /* restricted */ ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                     /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansichar* /* param */ ANeedlePtr)
{
  ansinullstring* /* var */ Result = NULL;
  // ---

  struct ansicharsizedptr* /* param */ AHaystackPtr;
  struct ansicharidxptr*   /* param */ ADestPtr;

  // ---

  AHaystackPtr =
   coreacharszptrs__new();
  ADestPtr   =
   coreacharidxptrs__new();

  coreacharszptrs__pack
    (&AHaystackPtr, AHaystackBuffer, AHaystackSize);

  Result =
    coreansearch__trylastptrofcharptr
      (AHaystackPtr, ANeedlePtr, ADestPtr);
  if (Found)
  {
    Result = ADestPtr->ItemPtr;
  } 

  coreacharidxptrs__drop
    (&ADestPtr);
  coreacharszptrs__drop
    (&AHaystackPtr);

  // ---
  return Result;
} // func

// ------------------

ansinullstring* /* func */ coreansinullstrs__lastptrofcharset
  (/* in */ /* restricted */ ansinullstring* /* param */ AHaystackBuffer,
   /* in */ const size_t                     /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansichar* /* param */ ANeedleSet)
{
  ansinullstring* /* var */ Result = NULL;
  // ---

  struct ansicharsizedptr* /* var */ AHaystackPtr;
  struct ansicharsizedptr* /* var */ ANeedleSetPtr;
  struct ansicharidxptr*   /* var */ ADestPtr;

  // ---

  AHaystackPtr =
   coreacharszptrs__new();
  ANeedleSetPtr =
   coreacharszptrs__new();
  ADestPtr   =
   coreacharidxptrs__new();

  coreacharszptrs__pack
    (&AHaystackPtr, AHaystackBuffer, AHaystackSize);
  coreacharszptrs__pack
    (&ANeedleSetPtr, ANeedleSet, ANSICHARSETMAXSIZE);

  Result =
    coreanullsearch__trylastptrofcharset
      (AHaystackPtr, ANeedleSetPtr, ADestPtr);
  if (Found)
  {
    Result = ADestPtr->ItemPtr;
  } 

  coreacharidxptrs__drop
    (&ADestPtr);
  coreacharszptrs__drop
    (&ANeedleSetPtr);
  coreacharszptrs__drop
    (&AHaystackPtr);

  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__lastptrofcharsetsize
  (/* in */ /* restricted */ ansinullstring*    /* param */ AHaystackBuffer,
   /* in */ const size_t                        /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansicharset* /* param */ ANeedleSetBuffer,
   /* in */ const size_t                        /* param */ ANeedleSetSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---

  struct ansicharsizedptr* /* var */ AHaystackPtr;
  struct ansicharsizedptr* /* var */ ANeedleSetPtr;
  struct ansicharidxptr*   /* var */ ADestPtr;

  // ---

  AHaystackPtr =
   coreacharszptrs__new();
  ANeedleSetPtr =
   coreacharszptrs__new();
  ADestPtr   =
   coreacharidxptrs__new();

  coreacharszptrs__pack
    (&AHaystackPtr, AHaystackBuffer, AHaystackSize);
  coreacharszptrs__pack
    (&ANeedleSetPtr, ANeedleSet, ANeedleSetSize);

  Result =
    coreanullsearch__trylastptrofcharset
      (AHaystackPtr, ANeedleSetPtr, ADestPtr);
  if (Found)
  {
    Result = ADestPtr->ItemPtr;
  } 

  coreacharidxptrs__drop
    (&ADestPtr);
  coreacharszptrs__drop
    (&ANeedleSetPtr);
  coreacharszptrs__drop
    (&AHaystackPtr);

  // ---
  return Result;
} // func

// ------------------

ansinullstring* /* func */ coreansinullstrs__lastptrofstrequal
  (/* in */ /* restricted */ ansinullstring*    /* param */ AHaystackBuffer,
   /* in */ const size_t                        /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansicharset* /* param */ ANeedle)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__lastptrofstrequalsize
  (/* in */ /* restricted */ ansinullstring*    /* param */ AHaystackBuffer,
   /* in */ const size_t                        /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansicharset* /* param */ ANeedleBuffer,
   /* in */ const size_t                        /* param */ ANeedleSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

ansinullstring* /* func */ coreansinullstrs__lastptrofstrsame
  (/* in */ /* restricted */ ansinullstring*    /* param */ AHaystackBuffer,
   /* in */ const size_t                        /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansicharset* /* param */ ANeedle)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__lastptrofstrsamesize
  (/* in */ /* restricted */ ansinullstring*    /* param */ AHaystackBuffer,
   /* in */ const size_t                        /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansicharset* /* param */ ANeedleBuffer,
   /* in */ const size_t                        /* param */ ANeedleSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

ansinullstring* /* func */ coreansinullstrs__searchnextnonsepchar
  (/* inout */ /* restricted */ ansinullstring* /* param */ AHaystackPtr,
   /* in */ const ansichar                      /* param */ ASeparator)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__searchnextnonsepcharsize
  (/* inout */ /* restricted */ ansinullstring* /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*         /* param */ AHaystackSize,
   /* in */    const ansichar                   /* param */ ASeparator)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

 // ------------------

ansinullstring* /* func */ coreansinullstrs__searchprevnonsepchar
  (/* inout */ /* restricted */ ansinullstring* /* param */ AHaystackPtr,
   /* in */ const ansichar                      /* param */ ASeparator)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__searchprevnonsepcharsize
  (/* inout */ /* restricted */ ansinullstring* /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*         /* param */ AHaystackSize,
   /* in */    const ansichar                   /* param */ ASeparator)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

 // ------------------

ansinullstring* /* func */ coreansinullstrs__searchnextnonsepcharset
  (/* inout */ /* restricted */ ansinullstring*   /* param */ AHaystackPtr,
   /* in */    /* restricted */ const ansicharset /* param */ ASeparatorSet)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__searchnextnonsepcharsetsize
  (/* inout */ /* restricted */ ansinullstring*   /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*           /* param */ AHaystackSize,
   /* in */    /* restricted */ const ansicharset /* param */ ASeparatorSet)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

 // ------------------

ansinullstring* /* func */ coreansinullstrs__searchprevnonsepcharset
  (/* inout */ /* restricted */ ansinullstring*    /* param */ AHaystackPtr,
   /* in */    /* restricted */ const ansicharset* /* param */ ASeparatorSet)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__searchprevnonsepcharsetsize
  (/* inout */ /* restricted */ ansinullstring*   /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*           /* param */ AHaystackSize,
   /* in */    /* restricted */ const ansicharset /* param */ ASeparatorSet)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

 // ------------------

ansinullstring* /* func */ coreansinullstrs__searchnextnonsepstr
  (/* inout */ /* restricted */ ansinullstring*       /* param */ AHaystackPtr,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASeparatorStr)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__searchnextnonsepstrsize
  (/* inout */ /* restricted */ ansinullstring*       /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*               /* param */ AHaystackSize,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASeparatorStr,
   /* inout */ /* restricted */ size_t                /* param */ ASeparatorSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

 // ------------------

ansinullstring* /* func */ coreansinullstrs__searchprevnonsepstr
  (/* inout */ /* restricted */ ansinullstring*       /* param */ AHaystackPtr,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASeparatorStr)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__searchprevnonsepstrsize
  (/* inout */ /* restricted */ ansinullstring*       /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*               /* param */ AHaystackSize,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASeparatorStr,
   /* inout */ /* restricted */ size_t                /* param */ ASeparatorSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------
 
ansinullstring* /* func */ coreansinullstrs__searchnextsepchar
  (/* inout */ /* restricted */ ansinullstring* /* param */ AHaystackPtr,
   /* in */ const ansichar                      /* param */ ASeparator)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__searchnextsepcharsize
  (/* inout */ /* restricted */ ansinullstring* /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*         /* param */ AHaystackSize,
   /* in */    const ansichar                   /* param */ ASeparator)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

 // ------------------

ansinullstring* /* func */ coreansinullstrs__searchprevsepchar
  (/* inout */ /* restricted */ ansinullstring* /* param */ AHaystackPtr,
   /* in */ const ansichar                      /* param */ ASeparator)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__searchprevsepcharsize
  (/* inout */ /* restricted */ ansinullstring* /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*         /* param */ AHaystackSize,
   /* in */    const ansichar                   /* param */ ASeparator)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

 // ------------------

ansinullstring* /* func */ coreansinullstrs__searchnextsepchar
  (/* inout */ /* restricted */ ansinullstring* /* param */ AHaystackPtr,
   /* in */ const ansichar                      /* param */ ASeparator)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__searchnextsepcharsize
  (/* inout */ /* restricted */ ansinullstring* /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*         /* param */ AHaystackSize,
   /* in */    const ansichar                   /* param */ ASeparator)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

 // ------------------

ansinullstring* /* func */ coreansinullstrs__searchprevsepchar
  (/* inout */ /* restricted */ ansinullstring* /* param */ AHaystackPtr,
   /* in */ const ansichar                      /* param */ ASeparator)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__searchprevsepcharsize
  (/* inout */ /* restricted */ ansinullstring* /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*         /* param */ AHaystackSize,
   /* in */    const ansichar                   /* param */ ASeparator)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

 // ------------------

ansinullstring* /* func */ coreansinullstrs__searchnextsepcharset
  (/* inout */ /* restricted */ ansinullstring*   /* param */ AHaystackPtr,
   /* in */    /* restricted */ const ansicharset /* param */ ASeparatorSet)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__searchnextsepcharsetsize
  (/* inout */ /* restricted */ ansinullstring*   /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*           /* param */ AHaystackSize,
   /* in */    /* restricted */ const ansicharset /* param */ ASeparatorSet)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

 // ------------------

ansinullstring* /* func */ coreansinullstrs__searchprevsepcharset
  (/* inout */ /* restricted */ ansinullstring*    /* param */ AHaystackPtr,
   /* in */    /* restricted */ const ansicharset* /* param */ ASeparatorSet)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__searchprevsepcharsetsize
  (/* inout */ /* restricted */ ansinullstring*       /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*               /* param */ AHaystackSize,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASeparatorSet)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

 // ------------------

ansinullstring* /* func */ coreansinullstrs__searchnextsepstr
  (/* inout */ /* restricted */ ansinullstring*       /* param */ AHaystackPtr,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASeparatorStr)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__searchnextsepstrsize
  (/* inout */ /* restricted */ ansinullstring*       /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*               /* param */ AHaystackSize,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASeparatorStr,
   /* inout */ /* restricted */ size_t                /* param */ ASeparatorSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

 // ------------------

ansinullstring* /* func */ coreansinullstrs__searchprevsepstr
  (/* inout */ /* restricted */ ansinullstring*       /* param */ AHaystackPtr,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASeparatorStr)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ansinullstring* /* func */ coreansinullstrs__searchprevsepstrsize
  (/* inout */ /* restricted */ ansinullstring*       /* param */ AHaystackPtr,
   /* inout */ /* restricted */ size_t*               /* param */ AHaystackSize,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASeparatorStr,
   /* inout */ /* restricted */ size_t                /* param */ ASeparatorSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

 // ------------------

bool /* func */ coreansinullstrs__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreansinullstrs__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreansinullstrs__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);
{
  coresystem__nothing();
} // func

pointer* /* func */ coreansinullstrs__ansinstr2ptrcopy
  (/* inout */ pointer* /* param */ AValue)
{
  pointer* /* var */ Result = NULL;
  // ---
     
  Result = coreansinullstrs__compactcopy(AValue);
  
  // ---
  return Result;
} // func

 // ...

 // ------------------

/* override */ int /* func */ coreansinullstrs__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
     
  // ---
  return Result;
} // func

/* override */ int /* func */ coreansinullstrs__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

// } // namespace coreansinullstrs