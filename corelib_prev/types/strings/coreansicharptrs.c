/** Module: "coreansicharptrs.c"
 ** Descr.: "Individual characters, accessed by pointer,"
 **         "operations library."
 **/

// namespace coreansicharptrs {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------
 
#include "coreansicharptrs.h"
 
// ------------------

/* inline */ bool /* func */ coreansicharptrs__isempty
  (/* in */ const ansichar* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (AValue != NULL) && (*AValue != ANSINULLCHAR);

  // ---
  return Result;
} // func
 
// ------------------

/* inline */ comparison /* func */ coreansicharptrs__safecompare
  (/* in */ /* restricted */ const ansichar* /* param */ A,
   /* in */ /* restricted */ const ansichar* /* param */ B)
{
  ansichar /* var */ Result = ansichar;
  // ---
  
  if (*A < *B)
  {
    Result = comparison__lesser;
  }
  else if (*A > *B)
  {
    Result = comparison__greater;
  }
  else
  {
    Result = comparison__equal;
  }

  // ---
  return Result;
} // func

/* inline */ comparison /* func */ coreansicharptrs__overlapcompare
  (/* in */ /* nonrestricted */ const ansichar* /* param */ A,
   /* in */ /* nonrestricted */ const ansichar* /* param */ B)
{
  ansichar /* var */ Result = ansichar;
  // ---
  
  if (*A < *B)
  {
    Result = comparison__lesser;
  }
  else if (*A > *B)
  {
    Result = comparison__greater;
  }
  else
  {
    Result = comparison__equal;
  }

  // ---
  return Result;
} // func

// ------------------

/* inline */ bool /* func */ coreansicharptrs__safeareequal
  (/* in */ /* restricted */ const ansichar* /* param */ A,
   /* in */ /* restricted */ const ansichar* /* param */ B)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (A != NULL) &&
    (B != NULL) &&
    (*A == *B);

  // ---
  return Result;
} // func

/* inline */ bool /* func */ coreansicharptrs__overlapareequal
  (/* in */ /* nonrestricted */ const ansichar* /* param */ A,
   /* in */ /* nonrestricted */ const ansichar* /* param */ B)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (A != NULL) &&
    (B != NULL) &&
    (*A == *B);

  // ---
  return Result;
} // func

// ------------------

/* inline */ bool /* func */ coreansicharptrs__safearesame
  (/* in */ /* restricted */ const ansichar* /* param */ A,
   /* in */ /* restricted */ const ansichar* /* param */ B)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (A != NULL) && (B != NULL);
  if (Result)
  {
    Result =
      (tolower(*A) == tolower(*B));
  } // if

  // ---
  return Result;
} // func

/* inline */ bool /* func */ coreansicharptrs__overlaparesame
  (/* in */ /* nonrestricted */ const ansichar* /* param */ A,
   /* in */ /* nonrestricted */ const ansichar* /* param */ B)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (A != NULL) && (B != NULL);
  if (Result)
  {
    Result =
      (tolower(*A) == tolower(*B));
  } // if

  // ---
  return Result;
} // func

// ------------------

ansichar /* func */ coreansicharptrs__chartolower
  (/* in */ const ansichar* /* param */ AValue)
{
  ansichar /* var */ Result = ansichar;
  // ---
  
  Result = tolower(*AValue);

  // ---
  return Result;
} // func

ansichar /* func */ coreansicharptrs__chartoupper
  (/* in */ const ansichar* /* param */ AValue);
{
  ansichar /* var */ Result = ansichar;
  // ---
     
  Result = toupper(*AValue);
     
  // ---
  return Result;
} // func

ansichar /* func */ coreansicharptrs__chartorevcase
  (/* in */ const ansichar* /* param */ AValue);
{
  ansichar /* var */ Result = ansichar;
  // ---
    
  if (coreansicharptrs__isalpha(AValue))
  {
    if (coreansicharptrs__isupper(AValue))
    {
      Result = tolower(*AValue);
    }
    else
    {
      Result = toupper(*AValue);
    }
  }
     
  // ---
  return Result;
} // func

// ------------------

/* inline */ byte_t /* func */ coreansicharptrs__chartodec
  (/* in */ const ansichar* /* param */ AValue);
{
  byte_t /* var */ Result = 0;
  // ---
     
  Result =
     ((byte_t) *AValue);
     
  // ---
  return Result;
} // func

/* inline */ ansichar /* func */ coreansicharptrs__dectochar
  (/* in */ const byte_t /* param */ AValue);
{
  ansichar /* var */ Result = ansichar;
  // ---
     
  Result =
     ((unsigned char) *AValue);

  // ---
  return Result;
} // func

// ------------------

/* inline */ bool /* func */ coreansicharptrs__isnullmarker
  (/* in */ const ansichar* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (AValue != NULL) &&
    (*AValue == ANSINULLCHAR);

  // ---
  return Result;
} // func

/* inline */ void /* func */ coreansicharptrs__clearptr
  (/* out */ ansichar* /* param */ ADest)
{
  (*ADest) = ANSINULLCHAR;
} // func


// ------------------

bool /* func */ coreansicharptrs__islowercase
  (/* in */ const ansichar* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = islower(AValue);
  
  // ---
  return Result;
} // func

bool /* func */ coreansicharptrs__isuppercase
  (/* in */ const ansichar* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = isupper(AValue);
  
  // ---
  return Result;
} // func

bool /* func */ coreansicharptrs__isspace
  (/* in */ const ansichar* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = isspace(AValue);
  
  // ---
  return Result;
} // func

bool /* func */ coreansicharptrs__isblank
  (/* in */ const ansichar* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = isblank(AValue);
  
  // ---
  return Result;
} // func

bool /* func */ coreansicharptrs__isdigit
  (/* in */ const ansichar* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = isdigit(AValue);
  
  // ---
  return Result;
} // func

bool /* func */ coreansicharptrs__isalpha
  (/* in */ const ansichar* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = isalpha(AValue);
  
  // ---
  return Result;
} // func

bool /* func */ coreansicharptrs__isalphanum
  (/* in */ const ansichar* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = isalnum(AValue);
  
  // ---
  return Result;
} // func

bool /* func */ coreansicharptrs__ispunct
  (/* in */ const ansichar* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = ispunct(AValue);
  
  // ---
  return Result;
} // func

bool /* func */ coreansicharptrs__iscontrol
  (/* in */ const ansichar* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = iscntrl(AValue);
  
  // ---
  return Result;
} // func

bool /* func */ coreansicharptrs__isgraph
  (/* in */ const ansichar* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = isgraph(AValue);
  
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreansicharptrs__isid
  (/* in */ const ansichar* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (isalnum(*AValue) || (*AValue == '_');
  
  // ---
  return Result;
} // func

// ------------------




 // ...

// ------------------

/* override */ const ansinullstring* /* func */ coreansicharptrs__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "coreansicharptrs";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coreansicharptrs__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreansicharptrs__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coreansicharptrs