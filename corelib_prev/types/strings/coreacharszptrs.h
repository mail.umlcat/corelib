/** Module: "coreacharszptrs.h"
 ** Descr.: "A composite pointer and index operations library,"
 **         "also known as a 'fat pointer',"
 **         "the base data type is 'ansichar' ."
 **/

// namespace coreacharszptrs {
 
// ------------------
 
#ifndef COREACHARSZPTRS__H
#define COREACHARSZPTRS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
//#include "corefunctors.h"
//#include "coreuuids.h"
//#include "coreoswarnings.h"

// ------------------

//#include "coreiterators.h"

// ------------------

struct ansicharsizedptr
{
  ansichar* /* var */ ItemPtr;
  size_t    /* var */ ItemSize;
} ;

typedef
  ansicharsizedptr    /* as */ ansicharsizedptr_t;
typedef
  ansicharsizedptr_t* /* as */ ansicharsizedptr_p;

typedef
  ansicharsizedptr_t /* as */ coreacharszptrs__ansicharsizedptr_t;
typedef
  ansicharsizedptr_p /* as */ coreacharszptrs__ansicharsizedptr_p;

// ------------------

bool /* func */ coreacharszptrs__isassigned
  (/* in */ const struct ansicharsizedptr* /* param */ ASource);

// ------------------

/* friend */ bool /* func */ coreacharszptrs__areequal
  (/* in */ const struct ansicharsizedptr* /* param */ A,
   /* in */ const struct ansicharsizedptr* /* param */ B);

/* friend */ bool /* func */ coreacharszptrs__areequalonlyptr
  (/* in */ const struct ansicharsizedptr* /* param */ A,
   /* in */ const struct ansicharsizedptr* /* param */ B);

// ------------------

ansicharsizedptr* /* func */ coreacharszptrs__new
  ( noparams );

void /* func */ coreacharszptrs__drop
  (/* out */ ansicharsizedptr** /* param */ ADest);

// ------------------

void /* func */ coreacharszptrs__pack
  (/* out */ ansicharsizedptr** /* param */ ADest,
   /* in */  const pointer*     /* param */ APtr,
   /* in */  const size_t       /* param */ ASize);

void /* func */ coreacharszptrs__unpack
  (/* in */  const ansicharsizedptr* /* param */ ASource,
   /* out */ pointer**               /* param */ APtr,
   /* out */ size_t                  /* param */ ASize);

// ------------------

void /* func */ coreacharszptrs__clear
  (/* inout */ ansicharsizedptr* /* param */ ADest);

// ------------------

 struct ansicharsizedptr* /* func */ coreacharszptrs__pred
  (/* in */ const struct ansicharsizedptr* /* param */ ASource);

 struct ansicharsizedptr* /* func */ coreacharszptrs__predbycount
  (/* in */ const struct ansicharsizedptr* /* param */ ASource,
   /* in */ const count_t         /* param */ ACount);

 struct ansicharsizedptr* /* func */ coreacharszptrs__succ
  (/* in */ const struct ansicharsizedptr* /* param */ ASource);

 struct ansicharsizedptr* /* func */ coreacharszptrs__succbycount
  (/* in */ const struct ansicharsizedptr* /* param */ ASource,
   /* in */ const count_t         /* param */ ACount);

// ------------------

void /* func */ coreacharszptrs__inc
  (/* inout */  struct ansicharsizedptr* /* param */ ADest);

void /* func */ coreacharszptrs__incbycount
  (/* inout */ struct ansicharsizedptr* /* param */ ADest,
   /* in */    const count_t   /* param */ ACount);

void /* func */ coreacharszptrs__dec
  (/* inout */  struct ansicharsizedptr* /* param */ ADest);

void /* func */ coreacharszptrs__decbycount
  (/* inout */  struct ansicharsizedptr* /* param */ ADest,
   /* in */     const count_t   /* param */ ACount);

// ------------------




 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coreacharszptrs__modulename
  ( noparams );

/* override */ int /* func */ coreacharszptrs__setup
  ( noparams );

/* override */ int /* func */ coreacharszptrs__setoff
  ( noparams );

// ------------------

#endif // COREACHARSZPTRS__H

// } // namespace coreacharszptrs