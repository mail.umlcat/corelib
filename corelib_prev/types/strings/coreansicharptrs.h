/** Module: "coreansicharptrs.h"
 ** Descr.: "Individual characters, accessed by pointer,"
 **         "operations library."
 **/

// namespace coreansicharptrs {
 
// ------------------
 
#ifndef COREANSICHARPTRS__H
#define COREANSICHARPTRS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

/* inline */ bool /* func */ coreansicharptrs__isempty
  (/* in */ const ansichar* /* param */ AValue);

// ------------------

/* inline */ comparison /* func */ coreansicharptrs__safecompare
  (/* in */ /* restricted */ const ansichar* /* param */ A,
   /* in */ /* restricted */ const ansichar* /* param */ B);

/* inline */ comparison /* func */ coreansicharptrs__overlapcompare
  (/* in */ /* nonrestricted */ const ansichar* /* param */ A,
   /* in */ /* nonrestricted */ const ansichar* /* param */ B);

// ------------------

/* inline */ bool /* func */ coreansicharptrs__safeareequal
  (/* in */ /* restricted */ const ansichar* /* param */ A,
   /* in */ /* restricted */ const ansichar* /* param */ B);

/* inline */ bool /* func */ coreansicharptrs__overlapareequal
  (/* in */ /* nonrestricted */ const ansichar* /* param */ A,
   /* in */ /* nonrestricted */ const ansichar* /* param */ B);

// ------------------

/* inline */ bool /* func */ coreansicharptrs__safearesame
  (/* in */ /* restricted */ const ansichar* /* param */ A,
   /* in */ /* restricted */ const ansichar* /* param */ B);

/* inline */ bool /* func */ coreansicharptrs__overlaparesame
  (/* in */ /* nonrestricted */ const ansichar* /* param */ A,
   /* in */ /* nonrestricted */ const ansichar* /* param */ B);

// ------------------

ansichar /* func */ coreansicharptrs__chartolower
  (/* in */ const ansichar* /* param */ AValue);

ansichar /* func */ coreansicharptrs__chartoupper
  (/* in */ const ansichar* /* param */ AValue);

ansichar /* func */ coreansicharptrs__chartorevcase
  (/* in */ const ansichar* /* param */ AValue);

// ------------------

/* inline */ byte_t /* func */ coreansicharptrs__chartodec
  (/* in */ const ansichar* /* param */ AValue);

/* inline */ ansichar /* func */ coreansicharptrs__dectochar
  (/* in */ const byte_t /* param */ AValue);

// ------------------

/* inline */ bool /* func */ coreansicharptrs__isnullmarker
  (/* in */ const ansichar* /* param */ AValue);

/* inline */ void /* func */ coreansicharptrs__clear
  (/* out */ ansichar* /* param */ ADest);

// ------------------

bool /* func */ coreansicharptrs__islowercase
  (/* in */ const ansichar* /* param */ AValue);
 
bool /* func */ coreansicharptrs__isuppercase
  (/* in */ const ansichar* /* param */ AValue);

bool /* func */ coreansicharptrs__isspace
  (/* in */ const ansichar* /* param */ AValue);

bool /* func */ coreansicharptrs__isblank
  (/* in */ const ansichar* /* param */ AValue);

bool /* func */ coreansicharptrs__isdigit
  (/* in */ const ansichar* /* param */ AValue);
  
bool /* func */ coreansicharptrs__isalpha
  (/* in */ const ansichar* /* param */ AValue);
  
bool /* func */ coreansicharptrs__isalphanum
  (/* in */ const ansichar* /* param */ AValue);

bool /* func */ coreansicharptrs__ispunct
  (/* in */ const ansichar* /* param */ AValue);
  
bool /* func */ coreansicharptrs__iscontrol
  (/* in */ const ansichar* /* param */ AValue);
  
bool /* func */ coreansicharptrs__isgraph
  (/* in */ const ansichar* /* param */ AValue);

// ------------------

bool /* func */ coreansicharptrs__isid
  (/* in */ const ansichar* /* param */ AValue);

 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coreansicharptrs__modulename
  ( noparams );

/* override */ int /* func */ coreansicharptrs__setup
  ( noparams );

/* override */ int /* func */ coreansicharptrs__setoff
  ( noparams );

// ------------------

#endif // COREANSICHARPTRS__H

// } // namespace coreansicharptrs