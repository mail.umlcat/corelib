/** Module: "coreansichars.c"
 ** Descr.: "Individual character operations library."
 **/

// namespace coreansichars {
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
#include <ctype.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
//#include "coreuuids.h"
//#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
 
// ------------------

ansichar /* func */ coreansichars__chartolower
  (/* in */ const ansichar /* param */ AValue)
{
  ansichar /* var */ Result = ansichar;
  // ---
  
  Result = tolower(AValue);
     
  // ---
  return Result;
} // func

ansichar /* func */ coreansichars__chartoupper
  (/* in */ const ansichar /* param */ AValue);
{
  ansichar /* var */ Result = ansichar;
  // ---
     
  Result = toupper(AValue);
     
  // ---
  return Result;
} // func

byte_t /* func */ coreansichars__chartodec
  (/* in */ const ansichar /* param */ AValue);
{
  byte_t /* var */ Result = 0;
  // ---
     
  Result =
     ((byte_t) AValue);
     
  // ---
  return Result;
} // func

ansichar /* func */ coreansichars__dectochar
  (/* in */ const byte_t /* param */ AValue);
{
  ansichar /* var */ Result = ansichar;
  // ---
     
  Result =
     ((unsigned char) AValue);

  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreansichars__isnull
  (/* in */ const ansichar /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AValue == ansinullchar);
  
  // ---
  return Result;
} // func

void /* func */ coreansichars__clear
  (/* out */ ansichar* /* param */ ADest)
{
  (*ADest) = ansinullchar;
} // func
 
// ------------------

bool /* func */ coreansichars__islowercase
  (/* in */ const ansichar /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = islower(AValue);
  
  // ---
  return Result;
} // func

bool /* func */ coreansichars__isuppercase
  (/* in */ const ansichar /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = isupper(AValue);
  
  // ---
  return Result;
} // func

bool /* func */ coreansichars__isspace
  (/* in */ const ansichar /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = isspace(AValue);
  
  // ---
  return Result;
} // func

bool /* func */ coreansichars__isblank
  (/* in */ const ansichar /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = isblank(AValue);
  
  // ---
  return Result;
} // func

bool /* func */ coreansichars__isdigit
  (/* in */ const ansichar /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = isdigit(AValue);
  
  // ---
  return Result;
} // func

bool /* func */ coreansichars__isalpha
  (/* in */ const ansichar /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = isalpha(AValue);
  
  // ---
  return Result;
} // func

bool /* func */ coreansichars__isalphanum
  (/* in */ const ansichar /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = isalnum(AValue);
  
  // ---
  return Result;
} // func

bool /* func */ coreansichars__ispunct
  (/* in */ const ansichar /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = ispunct(AValue);
  
  // ---
  return Result;
} // func

bool /* func */ coreansichars__iscontrol
  (/* in */ const ansichar /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = iscntrl(AValue);
  
  // ---
  return Result;
} // func

bool /* func */ coreansichars__isgraph
  (/* in */ const ansichar /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = isgraph(AValue);
  
  // ---
  return Result;
} // func

// ------------------





// ------------------

bool /* func */ coreansichars__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AValue == (pointer*) ansinullchar);
     
  // ---
  return Result;
} // func

bool /* func */ coreansichars__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
  
  Result = (*AValue != NULL);
  if (Result)
  {
    (*ADest) = ansinullchar;
  }
     
  // ---
  return Result;
} // func

void /* func */ coreansichars__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue)
{
  /* discard */ coreansichars__tryapplydefaultvalue
    (AValue);
} // func

// ------------------

/* override */ const ansinullstring* /* func */ coreansichars__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "coreansichars";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coreansichars__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreansichars__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coreansichars