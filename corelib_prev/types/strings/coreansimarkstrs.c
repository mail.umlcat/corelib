/** Module: "coreansimarkstrs.c"
 ** Descr.: "Similar to null character terminated strings,"
 **         "uses one special character at start,"
 **         "uses one special character at finish."
 **/

// namespace coreansimarkstrs {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------
 
#include "coreansimarkstrs.h"
 
// ------------------

/* friend */ ansimarkstring* /* func */ coreansimarkstrs__newstr
  (/* in */ const size_t /* param */ ADestSize)
{
  ansimarkstring* /* var */ Result = NULL;
  // ---
       
  Result =
    corememory__allocate(ADestSize + 2);

  // ---
  return Result;
} // func

/* friend */ ansimarkstring* /* func */ coreansimarkstrs__newstrclear
  (/* in */ const size_t /* param */ ADestSize)
{
  ansimarkstring* /* var */ Result = NULL;
  // ---
       
  Result =
    corememory__clearallocate(ADestSize + 2);

  // ---
  return Result;
} // func

/* friend */ void /* func */ coreansimarkstrs__dropstr
  (/* inout */ ansimarkstring** /* param */ ADestBuffer,
   /* in */    const size_t     /* param */ ADestSize)
{
  if ((ADestBuffer != NULL) && (ADestSize > 0))
  {
    corememory__deallocate(*ADestBuffer, ADestSize + 2);
  } // if
} // func

/* friend */ void /* func */ coreansimarkstrs__dropstrclear
  (/* inout */ ansimarkstring** /* param */ ADestBuffer,
   /* in */    const size_t     /* param */ ADestSize)
{
  if ((ADestBuffer != NULL) && (ADestSize > 0))
  {
    corememory__cleardeallocate(*ADestBuffer, ADestSize + 2);
  } // if
} // func

// ------------------

/* friend */ comparison /* func */ coreansimarkstrs__safecompare
  (/* in */ /* restricted */ const ansimarkstring* /* param */ A,
   /* in */ /* restricted */ const ansimarkstring* /* param */ B)
{
  comparison /* var */ Result = comparison__equal;
  // ---

  size_t /* var */ AFirstSize;
  size_t /* var */ ASecondSize;

  AFirstSize  = 1024;
  ASecondSize = 1024;

  Result =
    coreansimarkstrs__safecomparesize
     (A, AFirstSize, B, ASecondSize);

  // ---
  return Result;
} // func

/* friend */ comparison /* func */ coreansimarkstrs__safecomparesize
  (/* in */ /* restricted */ const ansimarkstring* /* param */ AFirstBuffer,
   /* in */ const size_t                           /* param */ AFirstSize,
   /* in */ /* restricted */ const ansimarkstring* /* param */ ASecondBuffer,
   /* in */ const size_t                           /* param */ AFirstSize)
{
  comparison /* var */ Result = comparison__equal;
  // ---

  if (! coreansimarkstrs__isemptysize(AFirstBuffer, AFirstSize) &&
     (! coreansimarkstrs__isemptysize(ASecondBuffer, ASecondSize))
  {


  } // if

  // ---
  return Result;
} // func

/* friend */ comparison /* func */ coreansimarkstrs__overlapcompare
  (/* in */ const /* nonrestricted */ ansimarkstring* /* param */ A,
   /* in */ const /* nonrestricted */ ansimarkstring* /* param */ B);
{
  comparison /* var */ Result = comparison__equal;
  // ---

  size_t /* var */ AFirstSize;
  size_t /* var */ ASecondSize;

  AFirstSize  = 1024;
  ASecondSize = 1024;

  Result =
    coreansimarkstrs__overlapcomparesize
     (A, AFirstSize, B, ASecondSize);

  // ---
  return Result;
} // func

/* friend */ comparison /* func */ coreansimarkstrs__overlapcomparesize
  (/* in */ const ansimarkstring* /* param */ AFirstBuffer,
   /* in */ const size_           /* param */ AFirstSize,
   /* in */ const ansimarkstring* /* param */ ASecondBuffer,
   /* in */ const size_t          /* param */ AFirstSize)
{
  comparison /* var */ Result = comparison__equal;
  // ---

  if (! coreansimarkstrs__isemptysize(AFirstBuffer, AFirstSize) &&
     (! coreansimarkstrs__isemptysize(ASecondBuffer, ASecondSize))
  {


  } // if

  // ---
  return Result;
} // func

/* friend */ bool /* func */ coreansimarkstrs__safeareequal
  (/* in */ /* restricted */ const ansimarkstring* /* param */ A,
   /* in */ /* restricted */ const ansimarkstring* /* param */ B)
{
  bool /* var */ Result = false;
  // ---

  size_t /* var */ AFirstSize;
  size_t /* var */ ASecondSize;

  AFirstSize  = 1024;
  ASecondSize = 1024;

  Result =
    coreansimarkstrs__safeareequalsize
     (A, AFirstSize, B, ASecondSize);

  // ---
  return Result;
} // func

/* friend */ bool /* func */ coreansimarkstrs__safeareequalsize
  (/* in */ /* restricted */ const ansimarkstring* /* param */ AFirstBuffer,
   /* in */ const size_t                           /* param */ AFirstSize,
   /* in */ /* restricted */ const ansimarkstring* /* param */ ASecondBuffer,
   /* in */ const size_t                           /* param */ AFirstSize)
{
  bool /* var */ Result = false;
  // ---

  if (! coreansimarkstrs__isemptysize(AFirstBuffer, AFirstSize) &&
     (! coreansimarkstrs__isemptysize(ASecondBuffer, ASecondSize))
  {


  } // if

  // ---
  return Result;
} // func

/* friend */ bool /* func */ coreansimarkstrs__overlapareequal
  (/* in */ const /* nonrestricted */ ansimarkstring* /* param */ A,
   /* in */ const /* nonrestricted */ ansimarkstring* /* param */ B)
{
  bool /* var */ Result = false;
  // ---

  size_t /* var */ AFirstSize;
  size_t /* var */ ASecondSize;

  AFirstSize  = 1024;
  ASecondSize = 1024;

  Result =
    coreansimarkstrs__overlapareequalsize
     (A, AFirstSize, B, ASecondSize);

  // ---
  return Result;
} // func

/* friend */ bool /* func */ coreansimarkstrs__overlapareequalsize
  (/* in */ /* restricted */ const ansimarkstring* /* param */ AFirstBuffer,
   /* in */ const size_t                           /* param */ AFirstSize,
   /* in */ /* restricted */ const ansimarkstring* /* param */ ASecondBuffer,
   /* in */ const size_t                           /* param */ AFirstSize)
{
  bool /* var */ Result = false;
  // ---

  if (! coreansimarkstrs__isemptysize(AFirstBuffer, AFirstSize) &&
     (! coreansimarkstrs__isemptysize(ASecondBuffer, ASecondSize))
  {


  } // if

  // ---
  return Result;
} // func

/* friend */ bool /* func */ coreansimarkstrs__safearesame
  (/* in */ /* restricted */ const ansimarkstring* /* param */ A,
   /* in */ /* restricted */ const ansimarkstring* /* param */ B)
{
  bool /* var */ Result = false;
  // ---

  size_t /* var */ AFirstSize;
  size_t /* var */ ASecondSize;

  AFirstSize  = 1024;
  ASecondSize = 1024;

  Result =
    coreansimarkstrs__safearesamesize
     (A, AFirstSize, B, ASecondSize);

  // ---
  return Result;
} // func

/* friend */ bool /* func */ coreansimarkstrs__safearesamesize
  (/* in */ /* restricted */ const ansimarkstring* /* param */ AFirstBuffer,
   /* in */ const size_t                           /* param */ AFirstSize,
   /* in */ /* restricted */ const ansimarkstring* /* param */ ASecondBuffer,
   /* in */ const size_t                           /* param */ AFirstSize)
{
  bool /* var */ Result = false;
  // ---

  if (! coreansimarkstrs__isemptysize(AFirstBuffer, AFirstSize) &&
     (! coreansimarkstrs__isemptysize(ASecondBuffer, ASecondSize))
  {


  } // if

  // ---
  return Result;
} // func

/* friend */ bool /* func */ coreansimarkstrs__overlaparesame
  (/* in */ const /* nonrestricted */ ansimarkstring* /* param */ A,
   /* in */ const /* nonrestricted */ ansimarkstring* /* param */ B)
{
  bool /* var */ Result = false;
  // ---

  size_t /* var */ AFirstSize;
  size_t /* var */ ASecondSize;

  AFirstSize  = 1024;
  ASecondSize = 1024;

  Result =
    coreansimarkstrs__overlaparesamesize
     (A, AFirstSize, B, ASecondSize);

  // ---
  return Result;
} // func

/* friend */ bool /* func */ coreansimarkstrs__overlaparesamesize
  (/* in */ /* nonrestricted */ const ansimarkstring* /* param */ AFirstBuffer,
   /* in */ const size_t                              /* param */ AFirstSize,
   /* in */ /* nonrestricted */ const ansimarkstring* /* param */ ASecondBuffer,
   /* in */ const size_t                              /* param */ AFirstSize)
{
  bool /* var */ Result = false;
  // ---

  if (! coreansimarkstrs__isemptysize(AFirstBuffer, AFirstSize) &&
     (! coreansimarkstrs__isemptysize(ASecondBuffer, ASecondSize))
  {


  } // if

  // ---
  return Result;
} // func

// ------------------

/* inline */ bool /* func */ coreansimarkstrs__isassigned
  (/* in */ const ansimarkstring* /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
       
  Result =
    (ASourceBuffer != NULL);

  // ---
  return Result;
} // func

/* inline */ bool /* func */ coreansimarkstrs__isassignedsize
  (/* in */ const ansimarkstring* /* param */ ASourceBuffer, 
   /* in */ size_t                /* param */ ASourceSize)
{
  bool /* var */ Result = false;
  // ---
       
  Result =
    ((ASourceBuffer != NULL) && (ASourceSize > 0));

  // ---
  return Result;
} // func

// ------------------

/* inline */ bool /* func */ coreansimarkstrs__isempty
  (/* in */ const ansimarkstring* /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
       
  Result =
    (ASourceBuffer != NULL) &&
    (*ASourceBuffer == coreansimarkstrs__STARTMARKER) &&
    (*(++ASourceBuffer) == coreansimarkstrs__FINISHMARKER);

  // ---
  return Result;
} // func

/* inline */ bool /* func */ coreansimarkstrs__isemptysize
  (/* in */ const ansimarkstring* /* param */ ASourceBuffer, 
   /* in */ size_t                /* param */ ASourceSize)
{
  bool /* var */ Result = false;
  // ---
       
  Result =
    (ASourceBuffer != NULL) &&
    (*ASourceBuffer == coreansimarkstrs__STARTMARKER) &&
    (*(++ASourceBuffer) == coreansimarkstrs__FINISHMARKER) &&
    (ASourceSize > 0);

  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreansimarkstrs__isstartmarker
  (/* in */ const ansichar /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
       
  Result =
    (AValue == coreansimarkstrs__STARTMARKER);

  // ---
  return Result;
} // func

bool /* func */ coreansimarkstrs__isfinishmarker
  (/* in */ const ansichar /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
       
  Result =
    (AValue == coreansimarkstrs__FINISHMARKER);

  // ---
  return Result;
} // func

bool /* func */ coreansimarkstrs__isstartmarkerptr
  (/* in */ const ansichar* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
       
  Result =
    (*AValue == coreansimarkstrs__STARTMARKER);

  // ---
  return Result;
} // func

bool /* func */ coreansimarkstrs__isfinishmarkerptr
  (/* in */ const ansichar* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
       
  Result =
    (AValue == coreansimarkstrs__FINISHMARKER);

  // ---
  return Result;
} // func

// ------------------

void /* func */ coreansimarkstrs__writestartmarker
  (/* inout */ ansichar* /* param */ ADestBuffer)
{
  if (ADestBuffer != NULL)
  {
    *ADestBuffer = coreansimarkstrs__STARTMARKER;
  } // if
} // func

void /* func */ coreansimarkstrs__writefinishmarker
  (/* inout */ ansichar* /* param */ ADestBuffer)
{
  if (ADestBuffer != NULL)
  {
    *ADestBuffer = coreansimarkstrs__FINISHMARKER;
  } // if
} // func

// ------------------






 // ...

// ------------------

/* override */ const ansimarkstring* /* func */ coreansimarkstrs__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "coreansimarkstrs";
  
  return (const ansimarkstring*) setbuffer;
} // func

/* override */ int /* func */ coreansimarkstrs__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreansimarkstrs__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coreansimarkstrs