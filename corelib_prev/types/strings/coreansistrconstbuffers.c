/** Module: "coreansistrconstbuffers.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coreansistrconstbuffers {
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"

// ------------------
 
#include "coreansistrconstbuffers.h"
 
// ------------------

void /* func */ coreansistrconstbuffers__createlist
 (/* in */ const size_t /* param */ ABufferSize)
{
  coresystem__nothing();
} // func

void /* func */ coreansistrconstbuffers__droplist
  (/* out */ stringconstbuffer** /* param */ AList)
{
  coresystem__nothing();
} // func

// ------------------

bool /* func */ coreansistrconstbuffers__tryadd
  (/* inout */ stringconstbuffer*      /* param */ AList,
   /* in */    const ansinullstring* /* param */ ASourcePtr,
   /* out */   ansinullstring**      /* param */ ADestPtr)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func 

bool /* func */ coreansistrconstbuffers__tryaddcount
  (/* inout */ stringconstbuffer*      /* param */ AList,
   /* in */    const ansinullstring* /* param */ ASourcePtr,
   /* in */    const size_t          /* param */ ASourceSize,
   /* out */   ansinullstring**      /* param */ ADestPtr)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func 

const ansinullstring* /* func */ coreansistrconstbuffers__add
  (/* inout */ stringconstbuffer*   /* param */ AList,
   /* in */ const ansinullstring* /* param */ AItem)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return (const ansinullstring*) Result;
} // func

const ansinullstring* /* func */ coreansistrconstbuffers__addcount
  (/* inout */ stringconstbuffer*   /* param */ AList,
   /* in */ const ansinullstring* /* param */ AItemPtr,
   /* in */ const size_t          /* param */ AItemSize)
{
  ansinullstring* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return (const ansinullstring*) Result;
} // func

// ------------------

count_t /* func */ coreansistrconstbuffers__getcount
  (/* in */ const stringconstbuffer* /* param */ AList)
   /* out */   ansinullstring**      /* param */ ADestPtr)
{
  count_t /* var */ Result = 0;
  // ---
     
     
  // ---
  return Result;
} // func 

size_t /* func */ coreansistrconstbuffers__getsize
  (/* in */ const stringconstbuffer* /* param */ AList)
{
  size_t /* var */ Result = 0;
  // ---
     
     
  // ---
  return Result;
} // func 

bool /* func */ coreansistrconstbuffers__tryresize
  (/* inout */ stringconstbuffer* /* param */ AList,
   /* in */    const count_t      /* param */ ANewSize)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func 

void /* func */ coreansistrconstbuffers__resize
  (/* inout */ stringconstbuffer* /* param */ AList,
   /* in */    const count_t      /* param */ ANewSize)
{
  coresystem__nothing();
} // func

// ------------------


 // ...

// } // namespace coreansistrconstbuffers