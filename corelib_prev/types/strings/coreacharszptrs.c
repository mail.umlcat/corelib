/** Module: "coreacharszptrs.c"
 ** Descr.: "A composite pointer and index operations library,"
 **         "also known as a 'fat pointer',"
 **         "the base data type is 'ansichar' ."
 **/

// namespace coreacharszptrs {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
//#include "corefunctors.h"
//#include "coreuuids.h"
//#include "coreoswarnings.h"

// ------------------

//#include "coreiterators.h"

// ------------------
 
#include "coreacharszptrs.h"

// ------------------

bool /* func */ coreacharszptrs__isassigned 
  (/* in */ const struct ansicharsizedptr* /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---
 
  Result =
    (ASource != NULL) &&
    (ASource->ItemPtr != NULL);
    (ASource->ItemIndex >= 0);

  // ---
  return Result;
} // func

// ------------------

/* friend */ bool /* func */ coreacharszptrs__areequal
  (/* in */ const struct ansicharsizedptr* /* param */ A,
   /* in */ const struct ansicharsizedptr* /* param */ B)
{
  bool /* var */ Result = false;
  // ---
 
  Result =
    (A != NULL) && (B != NULL) &&
    (A->ItemPtr == B->ItemPtr) &&
    (A->ItemIndex == B->ItemIndex);

  // ---
  return Result;
} // func

/* friend */ bool /* func */ coreacharszptrs__areequalonlyptr
  (/* in */ const struct ansicharsizedptr* /* param */ A,
   /* in */ const struct ansicharsizedptr* /* param */ B)
{
   struct ansicharsizedptr* /* var */ Result = NULL;
  // ---
 
  Result =
    (A != NULL) && (B != NULL) &&
    (A->ItemPtr == B->ItemPtr);

  // ---
  return Result;
} // func

// ------------------

ansicharsizedptr* /* func */ coreacharszptrs__new
  ( noparams )
{
  ansicharsizedptr* /* var */ Result = NULL;
  // ---
 
  Result =
    corememory__clearallocate(sizeof(ansicharsizedptr));

  // ---
  return Result;
} // func

void /* func */ coreacharszptrs__drop
  (/* out */ ansicharsizedptr** /* param */ ADest)
{
  if (ADest != NULL)
  {
    corememory__deallocate(ADest);
  } // if
} // func

// ------------------

void /* func */ coreacharszptrs__pack
  (/* out */ ansicharsizedptr** /* param */ ADest,
   /* in */  const pointer*     /* param */ ASourcePtr,
   /* in */  const size_t       /* param */ ASourceSize)
{
  if (ADest != NULL)
  {
    (*ADest)->ItemPtr  = ASourcePtr;
    (*ADest)->ItemSize = ASourceSize;
  } // if
} // func

void /* func */ coreacharszptrs__unpack
  (/* in */  const ansicharsizedptr* /* param */ ASource,
   /* out */ pointer**               /* param */ ADestPtr,
   /* out */ size_t*                 /* param */ ADestSize)
{
  if (ASource != NULL)
  {
    ADestPtr  = ASource->ItemPtr;
    ADestSize = ASource->ItemSize;
  } // if
} // func

// ------------------

void /* func */ coreacharszptrs__clear
  (/* inout */ ansicharsizedptr* /* param */ ADest)
{
  if (ADest != NULL)
  {
    ADest->ItemPtr  = NULL;
    ADest->ItemSize = 0;
  } // if
} // func

// ------------------

 struct ansicharsizedptr* /* func */ coreacharszptrs__pred
  (/* in */ const struct ansicharsizedptr* /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---

  if (coreacharszptrs__isassigned(ASource))
  {
    Result =
      coreacharszptrs__new();

    Result->ItemPtr   = (ASource->ItemPtr - 1);
    Result->ItemIndex = (ASource->ItemIndex - 1);
  } // if

  // ---
  return Result;
} // func

 struct ansicharsizedptr* /* func */ coreacharszptrs__predbycount
  (/* in */ const struct ansicharsizedptr* /* param */ ASource,
   /* in */ const count_t                  /* param */ ACount)
{
  bool /* var */ Result = false;
  // ---

  if (coreacharszptrs__isassigned(ASource) &&
      (ACount >= 0))
  {
    Result =
      coreacharszptrs__new();

    Result->ItemPtr   = (ASource->ItemPtr - ACount);
    Result->ItemIndex = (ASource->ItemIndex - ACount);
  } // if

  // ---
  return Result;
} // func

 struct ansicharsizedptr* /* func */ coreacharszptrs__succ
  (/* in */ const struct ansicharsizedptr* /* param */ ASource)
{
  bool /* var */ Result = false;
  // ---

  if (coreacharszptrs__isassigned(ASource))
  {
    Result =
      coreacharszptrs__new();

    Result->ItemPtr   = (ASource->ItemPtr +  1);
    Result->ItemIndex = (ASource->ItemIndex + 1);
  } // if

  // ---
  return Result;
} // func

 struct ansicharsizedptr* /* func */ coreacharszptrs__succbycount
  (/* in */ const struct ansicharsizedptr* /* param */ ASource,
   /* in */ const count_t                  /* param */ ACount)
{
  bool /* var */ Result = false;
  // ---

  if (coreacharszptrs__isassigned(ASource))
  {
    Result =
      coreacharszptrs__new();

    Result->ItemPtr   = (ASource->ItemPtr + 1);
    Result->ItemIndex = (ASource->ItemIndex + 1);
  } // if

  // ---
  return Result;
} // func

// ------------------

void /* func */ coreacharszptrs__inc
  (/* inout */  struct ansicharsizedptr* /* param */ ADest)
{
  if (coreacharszptrs__isassigned(ADest))
  {
    ADest->ItemPtr   = (ADest->ItemPtr + 1);
    ADest->ItemIndex = (ADest->ItemIndex + 1);
  } // if
} // func

void /* func */ coreacharszptrs__incbycount
  (/* inout */ struct ansicharsizedptr* /* param */ ADest,
   /* in */    const count_t            /* param */ ACount)
{
  if (coreacharszptrs__isassigned(ADest))
  {
    ADest->ItemPtr   = (ADest->ItemPtr + ACount);
    ADest->ItemIndex = (ADest->ItemIndex + ACount);
  } // if
} // func

void /* func */ coreacharszptrs__dec
  (/* inout */ struct ansicharsizedptr* /* param */ ADest)
{
  if (coreacharszptrs__isassigned(ADest))
  {
    ADest->ItemPtr   = (ADest->ItemPtr - 1);
    ADest->ItemIndex = (ADest->ItemIndex - 1);
  } // if
} // func

void /* func */ coreacharszptrs__decbycount
  (/* inout */ struct ansicharsizedptr* /* param */ ADest,
   /* in */    const count_t            /* param */ ACount)
{
  if (coreacharszptrs__isassigned(ADest))
  {
    ADest->ItemPtr   = (ADest->ItemPtr - ACount);
    ADest->ItemIndex = (ADest->ItemIndex - ACount);
  } // if
} // func

// ------------------



 // ...

// ------------------

/* override */ const ansinullstring* /* func */ coreacharszptrs__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "coreacharszptrs";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coreacharszptrs__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreacharszptrs__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coreacharszptrs