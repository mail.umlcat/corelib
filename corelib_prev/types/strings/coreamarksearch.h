/** Module: "coremarksearch.h"
 ** Descr.: "ANSI Marker Character Pair Strings,"
 **         "Search Operations."
 **/

// namespace coremarksearch {
 
// ------------------
 
#ifndef COREAMARKSEARCH__H
#define COREAMARKSEARCH__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

bool /* func */ coreamarksearch__atstart
  (/* in */ const struct ansicharsizedptr* /* param */ ASource);

bool /* func */ coreamarksearch__atfinish
  (/* in */ const struct ansicharsizedptr* /* param */ ASource);

// ------------------

bool /* func */ coreamarksearch__trysearchstartmarker
  (/* in */  /* restricted */ noconst struct ansicharsizedptr* /* param */ ASource,
   /* out */ /* restricted */ noconst struct ansicharidxptr*   /* param */ ADest);

bool /* func */ coreamarksearch__trysearchfinishmarker
  (/* in */  /* restricted */ noconst struct ansicharsizedptr* /* param */ ASource,
   /* out */ /* restricted */ noconst struct ansicharidxptr*   /* param */ ADest);

bool /* func */ coreamarksearch__trysearchlastchar
  (/* in */  /* restricted */ noconst struct ansicharsizedptr* /* param */ ASource,
   /* out */ /* restricted */ noconst struct ansicharidxptr*   /* param */ ADest);

// ------------------




// ------------------





 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coremarksearch__modulename
  ( noparams );

/* override */ int /* func */ coremarksearch__setup
  ( noparams );

/* override */ int /* func */ coremarksearch__setoff
  ( noparams );

// ------------------

#endif // COREAMARKSEARCH__H

// } // namespace coremarksearch