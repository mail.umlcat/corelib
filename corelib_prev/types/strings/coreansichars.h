/** Module: "coreansichars.h"
 ** Descr.: "Individual character operations library."
 **/

// namespace coreansichars {
 
// ------------------
 
#ifndef COREANSICHARS__H
#define COREANSICHARS__H
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
#include <ctype.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
//#include "coreuuids.h"
//#include "coreoswarnings.h"

// ------------------

typedef
  ansichar /* as */ coreansichars__ansichar;

// ------------------

ansichar /* func */ coreansichars__chartolower
  (/* in */ const ansichar /* param */ AValue);

ansichar /* func */ coreansichars__chartoupper
  (/* in */ const ansichar /* param */ AValue);

byte_t /* func */ coreansichars__chartodec
  (/* in */ const ansichar /* param */ AValue);

ansichar /* func */ coreansichars__dectochar
  (/* in */ const byte_t /* param */ AValue);

// ------------------

bool /* func */ coreansichars__isnull
  (/* in */ const ansichar /* param */ AValue);

void /* func */ coreansichars__clear
  (/* out */ ansichar* /* param */ ADest);

// ------------------

bool /* func */ coreansichars__islowercase
  (/* in */ const ansichar /* param */ AValue);
 
bool /* func */ coreansichars__isuppercase
  (/* in */ const ansichar /* param */ AValue);

bool /* func */ coreansichars__isspace
  (/* in */ const ansichar /* param */ AValue);

bool /* func */ coreansichars__isblank
  (/* in */ const ansichar /* param */ AValue);

bool /* func */ coreansichars__isdigit
  (/* in */ const ansichar /* param */ AValue);
  
bool /* func */ coreansichars__isalpha
  (/* in */ const ansichar /* param */ AValue);
  
bool /* func */ coreansichars__isalphanum
  (/* in */ const ansichar /* param */ AValue);
  
bool /* func */ coreansichars__ispunct
  (/* in */ const ansichar /* param */ AValue);
  
bool /* func */ coreansichars__iscontrol
  (/* in */ const ansichar /* param */ AValue);
  
bool /* func */ coreansichars__isgraph
  (/* in */ const ansichar /* param */ AValue);

// ------------------

bool /* func */ coreansichars__isdefaultvalue
  (/* in */ const pointer* /* param */ AValue);

bool /* func */ coreansichars__tryapplydefaultvalue
  (/* inout */ pointer* /* param */ AValue);

void /* func */ coreansichars__applydefaultvalue
  (/* inout */ const pointer* /* param */ AValue);


// ------------------

/* override */ const ansinullstring* /* func */ corechars__modulename
  ( noparams );

/* override */ int /* func */ corechars__setup
  ( noparams );

/* override */ int /* func */ corechars__setoff
  ( noparams );

// ------------------

#endif // COREANSICHARS__H

// } // namespace coreansichars