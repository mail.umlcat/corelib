/** Module: "coreopenansistrs.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coreopenansistrs {
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <float.h"
#include <time.h"
#include <chrono.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"

// ------------------
 
#include "coreopenansistrs.h"

// ------------------

openansistring* /* func */ coreopenansistrs__createstring
  (/* in */ size_t /* param */ AMaxSize)
{
  openansistring* /* var */ Result = NULL;
  // ---
  
  size_t /* var */ SizeInBytes =
    ((AMaxSize * sizeof(ansichar)) + sizeof (openansistringheader));

  Result =
    coresystem__malloc(SizeInBytes);

  // ---
  return Result;
} // func

void /* func */ coreopenansistrs__dropstring
  (/* out */ openansistring** /* param */ ADest)
{
  if (ADest != NULL)
  {
    size_t /* var */ SizeInBytes =
      ((ADest->MaxSize * sizeof(ansichar)) + sizeof (openansistringheader));
    
    coresystem__dealloc(*ADest, SizeInBytes);
    ADest = NULL;
  }
} // func

// ------------------




 // ...
 
// ------------------

comparison /* func */ coreopenansistrs__compare
  (/* in */ openansistring* /* param */ A, B)
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func
   
 // ------------------
 
void /* func */ coreopenansistrs__clear
  (/* inout */ openansistring* /* param */ ADest)
{
  // ...
} // func
 
 // ...

// ------------------

/* override */ int /* func */ coreopenansistrs__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreopenansistrs__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------


// } // namespace coreopenansistrs