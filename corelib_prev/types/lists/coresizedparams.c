/** Module: "coresizedparams.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coresizedparams {
 
 // ------------------
 
 #include <stdlib.h"
 #include <stddef.h"
 #include <string.h"
 #include <limits.h"
 #include <stdbool.h"
 #include <stdint.h"
 #include <inttypes.h"
 #include <stdio.h"
 
// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

#include "coreiterators.h"

// ------------------

 #include "coresizedparams.h"
 
 // ------------------

sizedparams* /* func */ coresizedparams__create
  (/* in */ count_t /* param */ AItemCount)
{
  // ...
} // func

void /* func */ coresizedparams__drop
  (/* out */ sizedparams** /* param */ ADest)
{
  // ...
} // func

// ------------------

sizedparamsvisitor* /* func */ coresizedparams__createvisitor
  (/* in */ sizedparams* /* param */ AParamList)
{
  // ...
} // func

void /* func */ coresizedparams__dropvisitor
  (/* out */ sizedparamsvisitor** /* param */ AVisitor)
{
  // ...
} // func

// ------------------

bool /* func */ coresizedparams__isdone
  (/* in */ const sizedparamsvisitor* /* param */ AVisitor)
{
	bool /* var */ Result = false;
    // ---
     
     
    // ---
    return Result;
} // func

pointer* /* func */ coresizedparams__getcurrentitem
  (/* inout */ sizedparamsvisitor* /* param */ AVisitor)
 {
 	pointer* /* var */ Result = NULL;
     // ---
     
     
     // ---
     return Result;
 } // func

void /* func */ coresizedparams__movenext
  (/* inout */ sizedparamsvisitor* /* param */ AVisitor)
{
  // ...
} // func

// ------------------

size_t /* func */ coresizedparams__getcount
  (/* in */ const sizedparams* /* param */ AParamList)
 {
 	size_t /* var */ Result = 0;
     // ---
     
     
     // ---
     return Result;
 } // func

pointer* /* func */ coresizedparams__itemat
  (/* in */ const sizedparams* /* param */ AParamList,
   /* in */ const index_t    /* param */ AIndex)
 {
 	pointer* /* var */ Result = NULL;
     // ---
     
     
     // ---
     return Result;
 } // func

// ------------------

void /* func */ coresizedparams__resize
  (/* inout */ sizedparams* /* param */ AParamList,
   /* in */ const size_t  /* param */ ANewSize)
{
  // ...
} // func

void /* func */ coresizedparams__insertat
  (/* inout */ sizedparams*  /* param */ AParamList,
   /* in */ const pointer* /* param */ AItem,
   /* in */ const index_t  /* param */ AIndex)
{
  // ...
} // func

pointer* /* func */ coresizedparams__extractat
  (/* inout */ sizedparams*  /* param */ AParamList,
   /* in */ const index_t  /* param */ AIndex)
 {
 	pointer* /* var */ Result = NULL;
     // ---
     
     
     // ---
     return Result;
 } // func


   
// ------------------
 
 // ...

// } // namespace coresizedparams