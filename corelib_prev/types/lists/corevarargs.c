/** Module: "corevarargs.c"
 ** Descr.: "Unknown type or size data collection,"
 **         "items are referenced by pointers,"
 **         "collection can't release items from memory,
 **         "by itelf."
 **/
 
// namespace corevarargs {
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------
 
#include "corevarargs.h"
 
// ------------------

varargs* /* func */ corevarargs__create
  (/* in */ count_t /* param */ AItemCount)
{
  varargs* /* var */ Result = NULL;
  // ---
     
  Result =
    corememory__clearallocate
      (sizeof(varargsheader));
      
  Result->ItemsMaxCount =
    AItemCount;
  Result->ItemsCurrentCount =
    0;
    
  Result->ItemsFirst =
    corememory__clearallocatearray
      (sizeof(pointer*), AItemCount);
      
  Result->ItemsCurrent =
    corememory__share
      (Result->ItemsFirst);
  
  // ---
  return Result;
} // func

void /* func */ corevarargs__drop
  (/* out */ varargs** /* param */ ADest)
{
  if (AParamList != NULL)
  {
    corememory__unshare
      (&Result->ItemsCurrent);
  
    corememory__cleardeallocatearray
      (&Result->ItemsFirst, sizeof(pointer*), AItemCount);
 	
    corememory__cleardeallocate
     (ADest, sizeof(varargsheader);     
  } // if
} // func

// ------------------

varargsvisitor* /* func */ corevarargs__createvisitor
  (/* in */ varargs* /* param */ AParamList)
{
  varargs* /* var */ Result = NULL;
  // ---
  
  if (AParamList != NULL)
  {
    Result =
      corememory__clearallocate
        (sizeof(varargsvisitorheader));
     
    Result->CurrentItem =
      corememory__share
        (Result->ItemsFirst);

    Result->ItemsCurrentCount =
      AParamList->ItemsCurrentCount;

    Result->ItemIndex =
      0;
  } // if
 
  // ---
  return Result;
} // func

void /* func */ corevarargs__dropvisitor
  (/* out */ varargsvisitor** /* param */ AIterator)
{
  if (AIterator != NULL)
  {
    corememory__unshare
      (&AIterator->CurrentItem);

    corememory__cleardeallocate
      (AIterator);
  } // if
} // func

// ------------------

bool /* func */ corevarargs__isdone
  (/* in */ const varargsvisitor* /* param */ AIterator)
{
  bool /* var */ Result = false;
 // ---
     
  if (AIterator != NULL)
  {
    Result =
      (AIterator->ItemIndex >= AIterator->ItemsCurrentCount);
  } // if
    
 // ---
  return Result;
} // func

pointer* /* func */ corevarargs__getcurrentitem
  (/* inout */ varargsvisitor* /* param */ AIterator)
{
  pointer* /* var */ Result = NULL;
 // ---
     
  if (AIterator != NULL)
  {
    Result =
      AIterator->CurrentItem;
  } // if
     
 // ---
  return Result;
} // func

void /* func */ corevarargs__movenext
  (/* inout */ varargsvisitor* /* param */ AIterator)
{
  if (AIterator != NULL)
  {
    AIterator->CurrentItem++;
    AIterator->ItemIndex++;
  } // if
} // func

count_t /* func */ corevarargs__getcount
  (/* inout */ varargsvisitor* /* param */ AIterator)
{
  count_t /* var */ Result = 0;
  // ---
     
  if (AIterator != NULL)
  {
    Result = 
      AIterator->ItemsCurrentCount;
  } // if
  
  // ---
  return Result;
} // func

index_t /* func */ corevarargs__getindex
  (/* inout */ varargsvisitor* /* param */ AIterator)
{
  index_t /* var */ Result = 0;
  // ---
     
  if (AIterator != NULL)
  {
    Result = 
      AIterator->ItemIndex;
  } // if
  
  // ---
  return Result;
} // func

// ------------------

bool /* func */ corevarargs__isvalidindex
  (/* in */ const varargs* /* param */ AParamList,
   /* in */ const index_t  /* param */ AIndex);
{
  bool /* var */ Result = FALSE;
  // ---
     
  if (AParamList != NULL)
  {
    Result =
      (AIndex > 0) &&
      (AIndex <= AParamList->ItemsCurrentCount);
  } // if
 
  // ---
  return Result;
} // func

count_t /* func */ corevarargs__getcount
  (/* in */ const varargs* /* param */ AParamList)
{
  count_t /* var */ Result = 0;
  // ---
     
  if (AParamList != NULL)
  {
    Result = 
      AParamList->ItemsCurrentCount;
  } // if
  
  // ---
  return Result;
} // func

pointer* /* func */ corevarargs__itemat
  (/* in */ const varargs* /* param */ AParamList,
   /* in */ const index_t  /* param */ AIndex)
{
  pointer* /* var */ Result = NULL;
  // ---

  if (AParamList != NULL)
  {
    if (corevarargs__isvalidindex(AIndex))
    {
      Result =
        Result->ItemsFirst[AIndex];
    } // if
  } // if
  
  // ---
  return Result;
} // func

// ------------------

void /* func */ corevarargs__resize
  (/* inout */ varargs*      /* param */ AParamList,
   /* in */    const size_t  /* param */ ANewCount)
{
  if (AParamList != NULL)
  {
 	corememory__unshare
       (&Result->ItemsCurrent);
  
     pointer* /* var */ ABuffer =
       corememory__clearallocatearray
         (sizeof(pointer*), ANewCount);

     size_t /* var */ AMatchCount =
       corememory__zeromaxsize
         (Result->ItemsMaxCount, ANewCount);
     
     corememory__safecopyarray
       (ABuffer, Result->ItemsFirst, sizeof(pointer*), AMatchCount);

     index_t /* var */ AMatchIndex =
       corememory__zeromaxsize
         (Result->ItemsCurrentCount, ANewCount);

     Result->ItemsCurrent =
       corememory__share
         (Result->ItemsFirst[AMatchIndex]));
               
     corememory__cleardeallocatearray
       (&Result->ItemsFirst, sizeof(pointer*), Result->ItemsMaxCount);
      
     Result->ItemsFirst =
       corememory__transfer(&ABuffer);
          
     Result->ItemsMaxCount =
       ANewCount;
  } // if
} // func

void /* func */ corevarargs__add
  (/* inout */ varargs*       /* param */ AParamList,
   /* in */    const pointer* /* param */ AItem)
{
  if (AParamList != NULL)
  {
    if (AParamList->ItemsCurrentCount < AParamList->ItemsMaxCount)
    {
      AParamList->ItemsCurrent =
        AItem;
 
      AParamList->ItemsCurrent++;
      AParamList->ItemsCurrentCount++;
    } 
  } // if
} // func

void /* func */ corevarargs__insertat
  (/* inout */ varargs*       /* param */ AParamList,
   /* in */    const index_t  /* param */ AIndex,
   /* in */    const pointer* /* param */ AItem)
{
  if (AParamList != NULL)
  {
    // is there enough space for more items ?
    if (AParamList->ItemsCurrentCount < AParamList->ItemsMaxCount)
    {
    	size_t /* var */ Delta =
          AParamList->ItemsCurrentCount - (AIndex + 1);
                
        corememory__insertatarray
          (AParamList->ItemsFirst, sizeof(pointer*), AParamList->ItemsMaxCount, AIndex, Delta);
          
        corevarargs__setitemat
          (AParamList, AIndex, AItem);
     }
  } // if
} // func

pointer* /* func */ corevarargs__extractat
  (/* inout */ varargs*      /* param */ AParamList,
   /* in */    const index_t /* param */ AIndex)
 {
    pointer* /* var */ Result = NULL;
    // ---
     
    if (AParamList != NULL)
    {
    	Result =
          corevarargs__getitemat
            (AParamList, AIndex);
    
    	size_t /* var */ Delta =
          AParamList->ItemsCurrentCount - (AIndex + 1);
                
        corememory__deleteatarray
          (AParamList->ItemsFirst, sizeof(pointer*), AParamList->ItemsMaxCount, AIndex, Delta);
    } // if
     
    // ---
    return Result;
 } // func

// ------------------

/* override */ int /* func */ corevarargs__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corevarargs__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace corevarargs