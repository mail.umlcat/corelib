/** Module: "coresizedparams.h"
 ** Descr.: "..."
 **/
 
// namespace coresizedparams {
 
// ------------------
 
#ifndef CORESIZEDPARAMS__H
#define CORESIZEDPARAMS__H
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

typedef
  pointer /* as */ sizedparams;
 
typedef
  pointer /* as */ sizedparamsvisitor;

// ------------------

struct sizedparamsheader
{
  // pointer to first item
  sizedptr* /* var */ ItemsFirst;

  // pointer to last item
  sizedptr* /* var */ ItemsCurrent;  

  // how many items can be stored
  count_t          /* var */ ItemsMax;

  // how many items are currently stored
  count_t          /* var */ ItemsCount;

  // ...
} ;

struct sizedparamsvisitorheader
{
  size_t    /* var */ ItemsCount;
  sizedptr* /* var */ CurrentItem;
} ;

// ------------------

sizedparams* /* func */ coresizedparams__create
  (/* in */ count_t /* param */ AItemCount);

void /* func */ coresizedparams__drop
  (/* out */ sizedparams** /* param */ ADest);

// ------------------

sizedparamsvisitor* /* func */ coresizedparams__createvisitor
  (/* in */ sizedparams* /* param */ AParamList);

void /* func */ coresizedparams__dropvisitor
  (/* out */ sizedparamsvisitor** /* param */ AVisitor);

// ------------------

bool /* func */ coresizedparams__isdone
  (/* in */ const sizedparamsvisitor* /* param */ AVisitor);

pointer* /* func */ coresizedparams__getcurrentitem
  (/* inout */ sizedparamsvisitor* /* param */ AVisitor);

void /* func */ coresizedparams__movenext
  (/* inout */ sizedparamsvisitor* /* param */ AVisitor);

// ------------------

size_t /* func */ coresizedparams__getcount
  (/* in */ const sizedparams* /* param */ AParamList);

pointer* /* func */ coresizedparams__itemat
  (/* in */ const sizedparams* /* param */ AParamList,
   /* in */ const index_t    /* param */ AIndex);

// ------------------

void /* func */ coresizedparams__resize
  (/* inout */ sizedparams* /* param */ AParamList,
   /* in */ const size_t  /* param */ ANewSize);

void /* func */ coresizedparams__insertat
  (/* inout */ sizedparams*  /* param */ AParamList,
   /* in */ const pointer* /* param */ AItem,
   /* in */ const index_t  /* param */ AIndex);

pointer* /* func */ coresizedparams__extractat
  (/* inout */ sizedparams*  /* param */ AParamList,
   /* in */ const index_t  /* param */ AIndex);



 // ...
 
// ------------------
 
#endif // CORESIZEDPARAMS__H

// } // namespace coresizedparams