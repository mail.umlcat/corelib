/** Module: "coretypedparams.c"
 ** Descr.: "Different type / different type data collection,"
 **         "items are referenced by pointers,"
 **         "collection can release items from memory."
 **/
 
// namespace coretypedparams {
 
 // ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------
 
 #include "coretypedparams.h"
 
 // ------------------

typedparams* /* func */ coretypedparams__create
  (/* in */ count_t /* param */ AItemCount)
{
  // ...
} // func

void /* func */ coretypedparams__drop
  (/* out */ typedparams** /* param */ ADest)
{
  // ...
} // func

// ------------------

typedparamsvisitor* /* func */ coretypedparams__createvisitor
  (/* in */ typedparams* /* param */ AParamList)
{
  // ...
} // func

void /* func */ coretypedparams__dropvisitor
  (/* out */ typedparamsvisitor** /* param */ AVisitor)
{
  // ...
} // func

// ------------------

bool /* func */ coretypedparams__isdone
  (/* in */ const typedparamsvisitor* /* param */ AVisitor)
{
	bool /* var */ Result = false;
    // ---
     
     
    // ---
    return Result;
} // func

pointer* /* func */ coretypedparams__getcurrentitem
  (/* inout */ typedparamsvisitor* /* param */ AVisitor)
 {
 	pointer* /* var */ Result = NULL;
     // ---
     
     
     // ---
     return Result;
 } // func

void /* func */ coretypedparams__movenext
  (/* inout */ typedparamsvisitor* /* param */ AVisitor)
{
  // ...
} // func

// ------------------

pointer* /* func */ coretypedparams__itemat
  (/* in */ const typedparams* /* param */ AParamList,
   /* in */ const index_t    /* param */ AIndex)
 {
 	pointer* /* var */ Result = NULL;
     // ---
     
     
     // ---
     return Result;
 } // func

// ------------------

count_t /* func */ coretypedparams__getcount
  (/* in */ const typedparams* /* param */ AParamList)
{
  count_t /* var */ Result = 0;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coretypedparams__resize
  (/* inout */ typedparams* /* param */ AParamList,
   /* in */ const size_t  /* param */ ANewSize)
{
  // ...
} // func

// ------------------

void /* func */ coretypedparams__insertat
  (/* inout */ typedparams*  /* param */ AParamList,
   /* in */ const pointer* /* param */ AItem,
   /* in */ const index_t  /* param */ AIndex)
{
  // ...
} // func

pointer* /* func */ coretypedparams__extractat
  (/* inout */ typedparams*  /* param */ AParamList,
   /* in */ const index_t  /* param */ AIndex)
 {
 	pointer* /* var */ Result = NULL;
     // ---
     
     
     // ---
     return Result;
 } // func


   
// ------------------
 
 // ...

// } // namespace coretypedparams