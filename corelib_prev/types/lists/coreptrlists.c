/** Module: "coreptrlists.c"
 ** Descr.: "Same size / same type data collection,"
 **         "items are stored by pointers,"
 **         "collection can release items from memory."
 **/

// namespace coreptrlists {
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

#include "coreptrlists.h"
 
// ------------------

pointerlist* /* func */ coreptrlists__createbysize
  (/* in */ count_t /* param */ AItemsMaxCount,
   /* in */ size_t  /* param */ AItemsSize)
{
  pointerlist* /* var */ Result = NULL;
  // ---
     
  AResult =
    coresystem__malloc(sizeof(pointerlist));
  AResult->ItemMaxCount = AItemsMaxCount;
  AResult->CurrentCount = 0;
  AResult->ItemsType    = unknowntype;
  AResult->ItemsSize    = AItemsSize;
  AResult->ItemsStart   =
    corememory__malloc(AItemsMaxCount * AItemsSize);
  AResult->ItemsCurrent. =
    AResult->ItemsStart;
  
  // ---
  return Result;
} // func

pointerlist* /* func */ coreptrlists__createbytype
  (/* in */ count_t    /* param */ AItemsMaxCount,
   /* in */ typecode_t /* param */ AItemsType)
{
  pointerlist* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreptrlists__drop
  (/* out */ pointerlist** /* param */ AList)
{
  if ((AList != NULL) && (*AList != NULL))
  {
    //pointerlist* /* var */ ThisList = *AList;
    coreptrlists__clear(*AList);
    corememory__dealloc(*AList, sizeof(pointerlist));
  } // if
  
  AList = NULL;
} // func

// ------------------

pointerlistiterator* /* func */ coreptrlists__createiteratorforward
  (/* in */ pointerlist* /* param */ AList)
{
  pointerlistiterator* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

pointerlistiterator* /* func */ coreptrlists__createiteratorbackward
  (/* in */ pointerlist* /* param */ AList)
{
  pointerlistiterator* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreptrlists__dropiterator
  (/* out */ pointerlistiterator** /* param */ AIterator)
{
  coresystem__nothing();
} // func

// ------------------

bool /* func */ coreptrlists__isdone
  (/* in */ const pointerlistiterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

pointer* /* func */ coreptrlists__readitem
  (/* inout */ pointerlistiterator* /* param */ AIterator)
{
  pointer* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreptrlists__movenext
  (/* inout */ pointerlistiterator* /* param */ AIterator)
{
  coresystem__nothing();
} // func

void /* func */ coreptrlists__getitem
  (/* inout */ pointerlistiterator* /* param */ AIterator
   /* out */   pointer*             /* param */ AItem)
{
  coresystem__nothing();
} // func

void /* func */ coreptrlists__setitem
  (/* inout */ pointerlistiterator* /* param */ AIterator
   /* in */ const  pointer*   /* param */ AItem)
{
  coresystem__nothing();
} // func

// ------------------

count_t /* func */ coreptrlists__getcount
  (/* in */ const pointerlist* /* param */ AList)
{
  count_t /* var */ Result = 0;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreptrlists__tryresize
  (/* inout */ pointerlist*  /* param */ AParamList,
   /* in */    const count_t /* param */ ANewSize)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreptrlists__resize
  (/* inout */ pointerlist*  /* param */ AParamList,
   /* in */    const count_t /* param */ ANewSize)
{
  coresystem__nothing();
} // func

// ------------------

bool /* func */ coreptrlists__tryclear
  (/* inout */ pointerlist* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreptrlists__isempty
  (/* in */ const pointerlist* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreptrlists__hasitems
  (/* in */ const pointerlist* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (!coreptrlists__isempty(AList);
     
  // ---
  return Result;
} // func

void /* func */ coreptrlists__clear
  (/* inout */ pointerlist* /* param */ AList)
{
  coresystem__nothing();
} // func

// ------------------

void /* func */ coreptrlists__resize
  (/* inout */ pointerlist* /* param */ AParamList,
   /* in */ const size_t  /* param */ ANewSize);

void /* func */ coreptrlists__insertat
  (/* inout */ pointerlist*  /* param */ AParamList,
   /* in */ const index_t  /* param */ AIndex,
   /* in */ const pointer* /* param */ AItem);

pointer* /* func */ coreptrlists__extractat
  (/* inout */ pointerlist*  /* param */ AParamList,
   /* in */ const index_t  /* param */ AIndex);
 
// ------------------

bool /* func */ coreptrlists__tryexchange
  (/* inout */ /* restricted */ pointerlist*  /* param */ AList,
   /* in */    /* restricted */ const index_t /* param */ AFirstIndex,
   /* in */    /* restricted */ const index_t /* param */ ASecondIndex)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreptrlists__exchange
  (/* inout */ /* restricted */ pointerlist*  /* param */ AList,
   /* in */    /* restricted */ const index_t /* param */ AFirstIndex,
   /* in */    /* restricted */ const index_t /* param */ ASecondIndex)
{
  coresystem__nothing();
} // func

// ------------------

/* override */ int /* func */ coreptrlists__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreptrlists__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coreptrlists