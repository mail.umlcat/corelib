/** Module: "coretypedparams.h"
 ** Descr.: "Different type / different type data collection,"
 **         "items are referenced by pointers,"
 **         "collection can release items from memory."
 **/
 
// namespace coretypedparams {
 
// ------------------
 
#ifndef CORETYPEDPARAMS__H
#define CORETYPEDPARAMS__H
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

typedef
  pointer /* as */ typedparams;
 
typedef
  pointer /* as */ typedparamsvisitor;

// ------------------

struct typedparamsheader
{
  // pointer to first item
  typedptr* /* var */ ItemsStart;
  
  // pointer to last item
  typedptr* /* var */ ItemsCurrent;

  // only applies when items are same type
  // "unknowntype" value means different type
  typecode         /* var */ ItemsType;

  // how many items can be stored
  count_t          /* var */ ItemsMax;

  // how many items are currently stored
  count_t          /* var */ ItemsCount;
  
  // ...
} ;

struct typedparamsvisitorheader
{
  typedptr* /* var */ CurrentItem;

  count_t   /* var */ ItemsCount;
} ;

// ------------------

typedparams* /* func */ coretypedparams__create
  (/* in */ count_t /* param */ AItemCount);

void /* func */ coretypedparams__drop
  (/* out */ typedparams** /* param */ ADest);

// ------------------

typedparamsvisitor* /* func */ coretypedparams__createvisitor
  (/* in */ typedparams* /* param */ AParamList);

void /* func */ coretypedparams__dropvisitor
  (/* out */ typedparamsvisitor** /* param */ AVisitor);

// ------------------

bool /* func */ coretypedparams__isdone
  (/* in */ const typedparamsvisitor* /* param */ AVisitor);

pointer* /* func */ coretypedparams__getcurrentitem
  (/* inout */ typedparamsvisitor* /* param */ AVisitor);

void /* func */ coretypedparams__movenext
  (/* inout */ typedparamsvisitor* /* param */ AVisitor);

// ------------------

pointer* /* func */ coretypedparams__itemat
  (/* in */ const typedparams* /* param */ AParamList,
   /* in */ const index_t    /* param */ AIndex);

// ------------------

count_t /* func */ coretypedparams__getcount
  (/* in */ const typedparams* /* param */ AParamList);

void /* func */ coretypedparams__resize
  (/* inout */ typedparams* /* param */ AParamList,
   /* in */ const count_t  /* param */ ANewSize);

// ------------------

void /* func */ coretypedparams__insertat
  (/* inout */ typedparams*  /* param */ AParamList,
   /* in */ const pointer* /* param */ AItem,
   /* in */ const index_t  /* param */ AIndex);

pointer* /* func */ coretypedparams__extractat
  (/* inout */ typedparams*  /* param */ AParamList,
   /* in */ const index_t  /* param */ AIndex);



 // ...
 
// ------------------
 
#endif // CORETYPEDPARAMS__H

// } // namespace coretypedparams