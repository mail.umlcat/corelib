/** Module: "corevarargs.h"
 ** Descr.: "Unknown type or size data collection,"
 **         "items are referenced by pointers,"
 **         "collection can't release items from memory,
 **         "by itelf."
 **/
 
// namespace corevarargs {
 
// ------------------
 
#ifndef COREVARARGS__H
#define COREVARARGS__H
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

struct varargsheader
{
  // pointer to first item
  pointer*  /* var */ ItemsFirst;

  // pointer to last item
  pointer*  /* var */ ItemsCurrent;

  // how many items can be stored
  count_t   /* var */ ItemsMaxCount;

  // how many items are currently stored
  count_t   /* var */ ItemsCurrentCount;

  // ...
} ;

typedef
  varargsheader  /* as */ varargs_t;
typedef
  varargsheader* /* as */ varargs_p;

// ------------------

struct varargsvisitorheader*
{
  pointer* /* var */ CurrentItem;

  count_t  /* var */ ItemIndex;
 
  count_t  /* var */ ItemsCurrentCount;

  // ...
} ;

typedef
  varargsvisitorheader /* as */ varargsvisitor;
typedef
  varargsvisitor       /* as */ varargsvisitor_t;
typedef
  varargsvisitor*      /* as */ varargsvisitor_p;

// ------------------

varargs* /* func */ corevarargs__create
  (/* in */ count_t /* param */ AItemCount);

void /* func */ corevarargs__drop
  (/* out */ varargs** /* param */ ADest);

// ------------------

varargsvisitor* /* func */ corevarargs__createvisitor
  (/* in */ varargs* /* param */ AParamList);

void /* func */ corevarargs__dropvisitor
  (/* out */ varargsvisitor** /* param */ AVisitor);

// ------------------

bool /* func */ corevarargs__isdone
  (/* in */ const varargsvisitor* /* param */ AVisitor);

pointer* /* func */ corevarargs__getcurrentitem
  (/* inout */ varargsvisitor* /* param */ AVisitor);

void /* func */ corevarargs__movenext
  (/* inout */ varargsvisitor* /* param */ AVisitor);

count_t /* func */ corevarargs__getcount
  (/* inout */ varargsvisitor* /* param */ AVisitor);

index_t /* func */ corevarargs__getindex
  (/* inout */ varargsvisitor* /* param */ AVisitor);

// ------------------

bool /* func */ corevarargs__isvalidindex
  (/* in */ const varargs* /* param */ AParamList,
   /* in */ const index_t  /* param */ AIndex);

size_t /* func */ corevarargs__getcount
  (/* in */ const varargs* /* param */ AParamList);

pointer* /* func */ corevarargs__itemat
  (/* in */ const varargs* /* param */ AParamList,
   /* in */ const index_t  /* param */ AIndex);

// ------------------

void /* func */ corevarargs__resize
  (/* inout */ varargs*      /* param */ AParamList,
   /* in */    const size_t  /* param */ ANewSize);

void /* func */ corevarargs__add
  (/* inout */ varargs*       /* param */ AParamList,
   /* in */    const pointer* /* param */ AItem);

void /* func */ corevarargs__insertat
  (/* inout */ varargs*       /* param */ AParamList,
   /* in */    const index_t  /* param */ AIndex,
   /* in */    const pointer* /* param */ AItem);

pointer* /* func */ corevarargs__extractat
  (/* inout */ varargs*       /* param */ AParamList,
   /* in */    const index_t  /* param */ AIndex);
 
// ------------------



// ...
 
// ------------------

/* override */ int /* func */ corevarargs__setup
  ( noparams );

/* override */ int /* func */ corevarargs__setoff
  ( noparams );

// ------------------

#endif// COREVARARGS__H

// }// namespace corevarargs