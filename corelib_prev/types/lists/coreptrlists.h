/** Module: "coreptrlists.h"
 ** Descr.: "Same size / same type data collection,"
 **         "items are stored by pointers,"
 **         "collection can release items from memory."
 **/

// namespace coreptrlists {
 
// ------------------
 
#ifndef COREPTRLISTS__H
#define COREPTRLISTS__H
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

struct pointerlistheader
{
  // pointer to first item
  pointer* /* var */ ItemsStart;
  
  // pointer to last item
  pointer* /* var */ ItemsCurrent;

  // only applies when items are same type
  // "unknowntype" value means different type
  typecode_t /* var */ ItemsType;

  // applies instead of "typecode"
  size_t   /* var */ ItemsSize;

  // how many items can be stored
  count_t  /* var */ ItemsMaxCount;

  // how many items are currently stored
  count_t  /* var */ ItemsCurrentCount;
 
  // ...
} ;

typedef
  pointerlistheader /* as */ pointerlist;
typedef
  pointerlist       /* as */ pointerlist_t;
typedef
  pointerlist*      /* as */ pointerlist_p;

// ------------------

struct pointerlistiteratorheader
{
  pointer*       /* var */ CurrentItem;

  count_t        /* var */ ItemsCount;
  size_t         /* var */ ItemsSize;

  listdirections /* var */ Direction;

  // ...
} ;

typedef
  pointerlistiteratorheader /* as */ pointerlistiterator;
typedef
  pointerlistiterator       /* as */ pointerlistiterator_t;
typedef
  pointerlistiterator*      /* as */ pointerlistiterator_p;

// ------------------

pointerlist* /* func */ coreptrlists__createbysize
  (/* in */ count_t /* param */ AItemsMaxCount,
   /* in */ size_t  /* param */ AItemsSize);

pointerlist* /* func */ coreptrlists__createbytype
  (/* in */ count_t    /* param */ AItemsMaxCount,
   /* in */ typecode_t /* param */ AItemsType);

void /* func */ coreptrlists__drop
  (/* out */ pointerlist** /* param */ AList);

// ------------------

pointerlistiterator* /* func */ coreptrlists__createiteratorforward
  (/* in */ pointerlist* /* param */ AList);

pointerlistiterator* /* func */ coreptrlists__createiteratorbackward
  (/* in */ pointerlist* /* param */ AList);

void /* func */ coreptrlists__dropiterator
  (/* out */ pointerlistiterator** /* param */ AIterator);

// ------------------

bool /* func */ coreptrlists__isdone
  (/* in */ const pointerlistiterator* /* param */ AIterator);

pointer* /* func */ coreptrlists__readitem
  (/* inout */ pointerlistiterator* /* param */ AIterator);

void /* func */ coreptrlists__movenext
  (/* inout */ pointerlistiterator* /* param */ AIterator);

void /* func */ coreptrlists__getitem
  (/* inout */ pointerlistiterator* /* param */ AIterator
   /* out */   pointer*             /* param */ AItem);

void /* func */ coreptrlists__setitem
  (/* inout */ pointerlistiterator* /* param */ AIterator
   /* in */    const  pointer*      /* param */ AItem);

// ------------------

count_t /* func */ coreptrlists__getcount
  (/* in */ const pointerlist* /* param */ AList);

bool /* func */ coreptrlists__tryresize
  (/* inout */ pointerlist*  /* param */ AParamList,
   /* in */    const count_t /* param */ ANewSize);

void /* func */ coreptrlists__resize
  (/* inout */ pointerlist* /* param */ AParamList,
   /* in */   const count_t /* param */ ANewSize);

// ------------------

bool /* func */ coreptrlists__tryclear
  (/* inout */ pointerlist* /* param */ AList);

bool /* func */ coreptrlists__isempty
  (/* in */ const pointerlist* /* param */ AList);

bool /* func */ coreptrlists__hasitems
  (/* in */ const pointerlist* /* param */ AList);

void /* func */ coreptrlists__clear
  (/* inout */ pointerlist* /* param */ AList);

// ------------------

bool /* func */ coreptrlists__trygetitemat
  (/* in */  const pointerlist* /* param */ AList,
   /* in */  const index_t      /* param */ AIndex,
   /* out */ pointer*           /* param */ AItem);

bool /* func */ coreptrlists__trysetitemat
  (/* in */ const pointerlist*  /* param */ AList,
   /* in */ const index_t       /* param */ AIndex);

void /* func */ coreptrlists__getitemat
  (/* in */ const pointerlist* /* param */ AList,
   /* in */ const index_t      /* param */ AIndex,
   /* out */ pointer*          /* param */ AItem);

void /* func */ coreptrlists__setitemat
  (/* in */ const pointerlist*  /* param */ AList,
   /* in */ const index_t       /* param */ AIndex);

// ------------------

bool /* func */ coreptrlists__tryinsertfirst
  (/* inout */ pointerlist*   /* param */ AList,
   /* in */    const pointer* /* param */ AItem);

bool /* func */ coreptrlists__tryinsertlast
  (/* inout */ pointerlist*   /* param */ AList,
   /* in */    const pointer* /* param */ AItem);

bool /* func */ coreptrlists__tryinsertat
  (/* inout */ pointerlist*   /* param */ AList,
   /* in */    const pointer* /* param */ AItem,
   /* in */    const index_t  /* param */ AIndex);

void /* func */ coreptrlists__insertfirst
  (/* inout */ pointerlist*   /* param */ AList,
   /* in */    const pointer* /* param */ AItem);

void /* func */ coreptrlists__insertlast
  (/* inout */ pointerlist*   /* param */ AList,
   /* in */    const pointer* /* param */ AItem);

void /* func */ coreptrlists__insertat
  (/* inout */  pointerlist*   /* param */ AList,
   /* in */     const index_t  /* param */ AIndex,
   /* in */     const pointer* /* param */ AItem);

void /* func */ coreptrlists__add
  (/* inout */ pointerlist*   /* param */ AList,
   /* in */    const pointer* /* param */ AItem);

bool /* func */ coreptrlists__tryinsertlast
  (/* inout */ pointerlist*   /* param */ AList,
   /* in */    const pointer* /* param */ AItem);

// ------------------

bool /* func */ coreptrlists__tryextractfirst
  (/* inout */ pointerlist* /* param */ AList,
   /* out */ pointer*       /* param */ AItem);

bool /* func */ coreptrlists__tryextractlast
  (/* inout */ pointerlist* /* param */ AList,
   /* out */   pointer*     /* param */ AItem);
   
bool /* func */ coreptrlists__tryextractat
  (/* inout */ pointerlist*  /* param */ AList,
   /* in */    const index_t /* param */ AIndex,
   /* out */   pointer*      /* param */ AItem);

pointer* /* func */ coreptrlists__extractfirst
  (/* inout */ pointerlist* /* param */ AList);

pointer* /* func */ coreptrlists__extractlast
  (/* inout */ pointerlist* /* param */ AList);

pointer* /* func */ coreptrlists__extractat
  (/* inout */ pointerlist*  /* param */ AList,
   /* in */    const index_t /* param */ AIndex);

// ------------------

bool /* func */ coreptrlists__tryexchange
  (/* inout */ /* restricted */ pointerlist*  /* param */ AList,
   /* in */    /* restricted */ const index_t /* param */ AFirstIndex,
   /* in */    /* restricted */ const index_t /* param */ ASecondIndex);

void /* func */ coreptrlists__exchange
  (/* inout */ /* restricted */ pointerlist*  /* param */ AList,
   /* in */    /* restricted */ const index_t /* param */ AFirstIndex,
   /* in */    /* restricted */ const index_t /* param */ ASecondIndex);

// ------------------


 // ...
 
// ------------------

/* override */ int /* func */ coreptrlists__setup
  ( noparams );

/* override */ int /* func */ coreptrlists__setoff
  ( noparams );

// ------------------

#endif // COREPTRLISTS__H

// } // namespace coreptrlists