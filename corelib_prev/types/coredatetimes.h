/** Module: "coredatetimes.h"
 ** Descr.: "..."
 **/
 
// namespace coredatetimes {
 
// ------------------
 
#ifndef COREDATETIMES__H
#define COREDATETIMES__H
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreintegers.h"

// ------------------

//$define void noparams

// ------------------
 
//typedef
//  sometype         /* as */ sometype;
   
// ------------------

//struct failure
//{
//  ansichar /* var */ Mesage;
//  pointer* /* var */ Parent;
//} ;

// ------------------

//pointer* /* func */ coredatetimes__malloc
//  (/* in */ size_t /* param */ ASize);
  
//void /* func */ coredatetimes__dealloc
//  (/* out */ pointer** /* param */ ADest,
//   /* in */  size_t    /* param */ ASize);

 // ...
 
// ------------------

/* override */ int /* func */ coredatetimes__setup
  ( noparams );

/* override */ int /* func */ coredatetimes__setoff
  ( noparams );

// ------------------

#endif // COREDATETIMES__H

// } // namespace coredatetimes