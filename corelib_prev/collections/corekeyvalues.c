/** Module: "corekeyvalues.c"
 ** Descr.: "Generic Type Key Value Pair."
 **/
 
// namespace corekeyvalues {
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <float.h"
#include <time.h"
//#include <chrono.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------
 
#include "corekeyvalues.h"
 
// ------------------
 
keyvalue* /* func */ corekeyvalues__createkeyvalue
  ( noparams );
{
  keyvalue /* var */ Result = NULL;
  // ---
     
  Result =
    corememory__createallocate
      (sizeof(keyvalueheader));
  
  // ---
  return Result;
} // func

keyvalue* /* func */ corekeyvalues__createkeyvaluebykey
  (/* in */ pointer* /* param */ AKey)
{
  keyvalue /* var */ Result = NULL;
  // ---
     
  Result =
    corememory__createallocate
      (sizeof(keyvalueheader));
  Result->Key = AKey;
   
  // ---
  return Result;
} // func

keyvalue* /* func */ corekeyvalues__createkeyvaluebyvalue
  (/* in */ pointer* /* param */ AKey
   /* in */ pointer* /* param */ AValue)
{
  keyvalue /* var */ Result = NULL;
  // ---
     
  Result =
    corememory__createallocate
      (sizeof(keyvalueheader));
  Result->Key   = AKey;
  Result->Value = AValue;
  
  // ---
  return Result;
} // func

// ------------------

void /* func */ corekeyvalues__dropbysize
 (/* out */ keyvalue** /* param */ AKeyValue,
  /* in */  size_t     /* param */ AKeySize,
  /* in */  size_t     /* param */ AValueSize)
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (AKeyValue != NULL) && (AKeySize > 0) && (AValueSize > 0);
  if (CanContinue)
  {
    keyvalueheader* /* var */ ThisKeyValue = NULL;
  	
    ThisKeyValue = (keyvalueheader*) *AKeyValue;
      
    corememory__cleardeallocate
      (&(ThisKeyValue->Key), AKeySize);
    corememory__cleardeallocate
      (&(ThisKeyValue->Value), AValueSize);
      
    corememory__cleardeallocate
      (AKeyValue, sizeof(keyvalueheader));
  } // if
} // func

void /* func */ corekeyvalues__dropextract
 (/* out */ keyvalue** /* param */ AKeyValue)
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (AKeyValue != NULL);
  if (CanContinue)
  {
    keyvalueheader* /* var */ ThisKeyValue = NULL;
  	
    ThisKeyValue = (keyvalueheader*) *AKeyValue;
    *AKey =
      corememory__transfer(&(ThisKeyValue->Key));
    *AValue =
      corememory__transfer(&(ThisKeyValue->Value));

    corememory__cleardeallocate
      (&ThisKeyValue, sizeof(keyvalueheader));
  } // if
} // func

void /* func */ corekeyvalues__dropextract
 (/* out */ keyvalue** /* param */ AKeyValue,
  /* out */ pointer**  /* param */ AKey
  /* out */ pointer**  /* param */ AValue)
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (ADest != NULL) && (AKey != NULL) && (AValue != NULL);
  if (CanContinue)
  {
    keyvalueheader* /* var */ ThisKeyValue = NULL;
  	
    ThisKeyValue = (keyvalueheader*) *AKeyValue;
    *AKey =
      corememory__transfer(&(ThisKeyValue->Key));
    *AValue =
      corememory__transfer(&(ThisKeyValue->Value));
      
    corememory__cleardeallocate
      (&ThisKeyValue, sizeof(keyvalueheader));

    *ADestKeyValue = NULL;
  } // if
} // func

// ------------------

pointer* /* func */ corekeyvalues__replacekey
  (/* out */ keyvalue* /* param */ AKeyValue,
   /* in */  pointer*  /* param */ ANewKey)
{
  pointer* /* var */ Result = NULL;
  // ---
  
  bool /* var */ CanContinue = FALSE;

  CanContinue =
    (AKeyValue != NULL) &&
    (ANewKey != NULL);
 
  if (CanContinue)
  {
    keyvalueheader* /* var */ ThisKeyValue = NULL;

    ThisKeyValue =
      (keyvalueheader*) corememory__share(AKeyValue);
  	
    Result =
      corememory__transfer(&(ThisKeyValue->Key));

    ThisKeyValue->Key = ANewKey;
  } // if
     
  // ---
  return Result;
} // func

pointer* /* func */ corekeyvalues__replacevalue
  (/* out */ keyvalue* /* param */ AKeyValue,
   /* in */  pointer*  /* param */ ANewValue)
{
  pointer* /* var */ Result = NULL;
  // ---
  
  bool /* var */ CanContinue = FALSE;

  CanContinue =
    (AKeyValue != NULL) &&
    (ANewValue != NULL);
 
  if (CanContinue)
  {
    keyvalueheader* /* var */ ThisKeyValue = NULL;

    ThisKeyValue =
      (keyvalueheader*) corememory__share(AKeyValue);
  	
    Result =
      corememory__transfer(&(ThisKeyValue->Value));

    ThisKeyValue->Value = ANewValue;
  } // if
     
  // ---
  return Result;
} // func

// ------------------

/* friend */ bool /* func */ corekeyvalues__tryexchangevalues
  (/* inout */ /* restricted */ keyvalue* /* param */ A,
   /* inout */ /* restricted */ keyvalue* /* param */ B)
{
  bool /* var */ Result = false;
  // ---
 
  keyvalueheader* /* var */ ThisFirstItem = NULL;
  keyvalueheader* /* var */ ThisSecondItem = NULL;
  
  Result =
    ((A != NULL) && (B != NULL));
  if (Result)
  {
    ThisFirstItem =
      (keyvalueheader*) A;
    ThisSecondItem =
      (keyvalueheader*) B;
      
    corememory__safeexchangeptr
      (&(ThisFirstItem->Value), &(ThisSecondItem->Value));
  } // if

  // ---
  return Result;
} // func

/* friend */ void /* func */ corekeyvalues__exchangevalues
  (/* inout */ /* restricted */ keyvalue* /* param */ A,
   /* inout */ /* restricted */ keyvalue* /* param */ B)
{
  /* discard */ corekeyvalues__tryexchangevalues
    (A, B);
} // func


 // ...

// ------------------

/* override */ const ansinullstring* /* func */ corekeyvalues__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "corekeyvalues";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ corekeyvalues__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corekeyvalues__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace corekeyvalues