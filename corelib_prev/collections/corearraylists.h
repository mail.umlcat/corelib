/** Module: "corearraylists.h"
 ** Descr.: "Double Linked Indexed List"
 **         "Data Structure."
 **/

// namespace corearraylists {
 
// ------------------
 
#ifndef COREARRAYLISTS__H
#define COREARRAYLISTS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"

// ------------------

#include "coreiterators.h"
#include <arraylistnodes.h"

// ------------------

// opaque type
typedef
  pointer* /* as */ arraylist;
  
typedef
  pointer* /* as */ corearraylists__arraylist;

struct arraylistheader
{
  // pointer to node before first node
  arraylistnode* /* var */ FirstMarker;
  
  // pointer to node before last node
  arraylistnode* /* var */ LastMarker;
  
  // only applies when items are same type
  // "unknowntype" value means different type
  typecode /* var */ ItemsType;

  // applies instead of "typecode"
  // "0" means not used
  size_t   /* var */ ItemsSize;

  // restricts how many items can be stored
  // "0" means not used
  count_t  /* var */ ItemsMaxCount;

  // how many items are currently stored
  count_t  /* var */ ItemsCurrentCount;
 
  // indicates iterator related objects
  count_t  /* var */ IteratorCount;
  
  areequalfunctor /* var */ MatchFunc;
  
  // "globalindex" counter
  count_t  /* var */ ItemsCurrentIndex;
  
  // ...
} ;

typedef
  arraylistheader  /* as */ arraylist_t;
typedef
  arraylistheader* /* as */ arraylist_p;

typedef
  arraylistheader  /* as */ corearraylists__arraylist_t;
typedef
  arraylistheader* /* as */ corearraylists__arraylist_p;

// ------------------

// opaque type
typedef
  pointer* /* as */ arraylistiterator;
  
struct arraylistiteratorheader
{
  arraylist*     /* var */ MainArrayList;

  arraylistnode* /* var */ CurrentNode;

  count_t        /* var */ ItemsCount;
  size_t         /* var */ ItemsSize;

  arraylistdirections /* var */ Direction; 
} ;
  
typedef
  arraylistiteratorheader  /* as */ arraylistiterator_t;
typedef
  arraylistiteratorheader* /* as */ arraylistiterator_p;
  
typedef
  arraylistiteratorheader  /* as */ corearraylists__arraylistiterator_t;
typedef
  arraylistiteratorheader* /* as */ corearraylists__arraylistiterator_p;
  
// ------------------

/* friend */ bool /* func */ corearraylists__sameitemsize
  (/* in */ arraylist* /* param */ AFirstList,
  (/* in */ arraylist* /* param */ ASecondList);

/* friend */ bool /* func */ corearraylists__sameitemtype
  (/* in */ arraylist* /* param */ AFirstList,
  (/* in */ arraylist* /* param */ ASecondList);

// ------------------

arraylist* /* func */ corearraylists__createlist
  ( noparams );

arraylist* /* func */ corearraylists__createlistbytype
  (/* in */ typecode_t* /* param */ AItemType);
  
arraylist* /* func */ corearraylists__createlistbysize
  (/* in */ size_t* /* param */ AItemSize);

arraylist* /* func */ corearraylists__createlistrestrictcount
  (/* in */ count_t* /* param */ ACount);

void /* func */ corearraylists__droplist
  (/* out */ arraylist** /* param */ AList);

// ------------------

bool /* func */ corearraylists__underiteration
  (/* in */ arraylist* /* param */ AList);

arraylistiterator* /* func */ corearraylists__createiteratorfwd
  (/* in */ arraylist* /* param */ AList);

arraylistiterator* /* func */ corearraylists__createiteratorback
  (/* in */ arraylist* /* param */ AList);

void /* func */ corearraylists__dropiterator
  (/* out */ arraylistiterator** /* param */ AIterator);

// ------------------

void /* func */ corearraylists__assignequalityfunc
  (/* inout */ arraylist* /* param */ AList,
   /* in */    equality   /* param */ AMatchFunc);

// ------------------

bool /* func */ corearraylists__isdone
  (/* in */ const arraylistiterator* /* param */ AIterator);

bool /* func */ corearraylists__trymovenext
  (/* inout */ arraylistiterator* /* param */ AIterator);

void /* func */ corearraylists__movenext
  (/* inout */ arraylistiterator* /* param */ AIterator);

bool /* func */ corearraylists__trygetnode
  (/* inout */ arraylistiterator* /* param */ AIterator
   /* out */   arraylistnode*     /* param */ ANode);

bool /* func */ corearraylists__trygetitem
  (/* inout */ arraylistiterator* /* param */ AIterator
   /* out */   arraylistnode**    /* param */ AItem);

bool /* func */ corearraylists__tryreaditem
  (/* inout */ arraylistiterator* /* param */ AIterator
   /* out */   pointer*           /* param */ AItem);

bool /* func */ corearraylists__trywriteitem
  (/* inout */ arraylistiterator* /* param */ AIterator
   /* out */   pointer*           /* param */ AItem);

arraylistnode* /* func */ corearraylists__getnode
  (/* inout */ arraylistiterator* /* param */ AIterator);

pointer* /* func */ corearraylists__getitem
  (/* inout */ arraylistiterator* /* param */ AIterator);

void /* func */ corearraylists__readitem
  (/* inout */ arraylistiterator* /* param */ AIterator
   /* out */   pointer*           /* param */ AItem);

void /* func */ corearraylists__writeitem
  (/* inout */ arraylistiterator* /* param */ AIterator
   /* in */    const pointer*     /* param */ AItem);

// ------------------

void /* func */ corearraylists__assignequalityfunc
  (/* inout */ arraylist* /* param */ AList,
   /* in */    equality   /* param */ AMatchFunc);

// ------------------

count_t /* func */ corearraylists__getcurrentcount
  (/* in */ const arraylist* /* param */ AList);

count_t /* func */ corearraylists__getmaxcount
  (/* in */ const arraylist* /* param */ AList);

// ------------------

bool /* func */ corearraylists__tryreindex
  (/* inout */ arraylist* /* param */ AList);

void /* func */ corearraylists__reindex
  (/* inout */ arraylist* /* param */ AList);

// ------------------

bool /* func */ corearraylists__isempty
  (/* in */ const arraylist* /* param */ AList);
  
bool /* func */ corearraylists__isfull
  (/* in */ const arraylist* /* param */ AList);

bool /* func */ corearraylists__hasitems
  (/* in */ const arraylist* /* param */ AList);

// ------------------

bool /* func */ corearraylists__tryclear
  (/* inout */ arraylist* /* param */ AList);

void /* func */ corearraylists__clear
  (/* inout */ arraylist* /* param */ AList);
  
// ------------------

bool /* func */ corearraylists__isvalidindex
  (/* in */ const arraylist* /* param */ AList
   /* in */ const index_t    /* param */ AIndex);

bool /* func */ corearraylists__trygetitemat
  (/* in */  const arraylist* /* param */ AList,
   /* in */  const index_t    /* param */ AIndex,
   /* out */ pointer**        /* param */ AItem);

bool /* func */ corearraylists__tryreaditemat
  (/* in */ const arraylist* /* param */ AList,
   /* in */ const index_t    /* param */ AIndex,
   /* out */ pointer*        /* param */ AItem);

bool /* func */ corearraylists__trywriteitemat
  (/* in */ const arraylist* /* param */ AList,
   /* in */ const index_t    /* param */ AIndex);

pointer* /* func */ corearraylists__getitemat
  (/* in */ /* restricted */ const arraylist* /* param */ AList,
   /* in */ /* restricted */ const index_t    /* param */ AIndex);

void /* func */ corearraylists__readitemat
  (/* in */ const arraylist* /* param */ AList,
   /* in */ const index_t    /* param */ AIndex,
   /* out */ pointer*        /* param */ AItem);

void /* func */ corearraylists__writeitemat
  (/* in */ const arraylist* /* param */ AList,
   /* in */ const index_t    /* param */ AIndex);

// ------------------

bool /* func */ corearraylists__tryseeknodeatfwd
  (/* in  */ const arraylist*      /* param */ AList,
   /* in  */ const index_t         /* param */ AIndex,
   /* out */ const arraylistnode** /* param */ ADestNode);

bool /* func */ corearraylists__tryseeknodeatbwd
  (/* in  */ const arraylist*      /* param */ AList,
   /* in  */ const index_t         /* param */ AIndex,
   /* out */ const arraylistnode** /* param */ ADestNode);

arraylistnode* /* func */ corearraylists__seeknodeatfwd
  (/* in */ const arraylist* /* param */ AList,
   /* in */ const index_t    /* param */ AIndex);

arraylistnode* /* func */ corearraylists__seeknodeatbwd
  (/* in */ const arraylist* /* param */ AList,
   /* in */ const index_t    /* param */ AIndex);

arraylistnode* /* func */ corearraylists__firstnodethat
  (/* in */ const arraylist* /* param */ AList,
   /* in */ matchesfunctor   /* param */ MatchesFunc,
   /* in */ pointer*         /* param */ AData);

arraylistnode* /* func */ corearraylists__lastnodethat
  (/* in */ const arraylist* /* param */ AList,
   /* in */ matchesfunctor   /* param */ MatchesFunc,
   /* in */ pointer*         /* param */ AData);

// ------------------

bool /* func */ corearraylists__tryinsertfirst
  (/* inout */ arraylist*      /* param */ AList,
   /* in */    const pointer*  /* param */ AItem,
   /* out */   arraylistnode** /* param */ ANewNode);

bool /* func */ corearraylists__tryinsertlast
  (/* inout */ arraylist*      /* param */ AList,
   /* in */    const pointer*  /* param */ AItem,
   /* out */   arraylistnode** /* param */ ANewNode);

bool /* func */ corearraylists__tryinsertat
  (/* inout */ arraylist*      /* param */ AList,
   /* in */    const index_t   /* param */ AIndex,
   /* in */    const pointer*  /* param */ AItem,
   /* out */   arraylistnode** /* param */ ANewNode);

arraylistnode* /* func */ corearraylists__insertfirst
  (/* inout */ arraylist*      /* param */ AList,
   /* in */    const pointer*  /* param */ AItem);

arraylistnode* /* func */ corearraylists__insertlast
  (/* inout */ arraylist*      /* param */ AList,
   /* in */    const pointer*  /* param */ AItem);

arraylistnode* /* func */ corearraylists__insertat
  (/* inout */ arraylist*      /* param */ AList,
   /* in */    const index_t   /* param */ AIndex,
   /* in */    const pointer*  /* param */ AItem);

// ------------------

bool /* func */ corearraylists__tryextractfirst
  (/* inout */ arraylist* /* param */ AList,
   /* out */   pointer**  /* param */ AItem);

bool /* func */ corearraylists__tryextractlast
  (/* inout */ arraylist* /* param */ AList,
   /* out */   pointer**  /* param */ AItem);
   
bool /* func */ corearraylists__tryextractat
  (/* inout */ arraylist*    /* param */ AList,
   /* in */    const index_t /* param */ AIndex,
   /* out */   pointer**     /* param */ AItem);

bool /* func */ corearraylists__tryremovefirst
  (/* inout */ arraylist* /* param */ AList);
   
bool /* func */ corearraylists__tryremovelast
  (/* inout */ arraylist* /* param */ AList);

pointer* /* func */ corearraylists__extractfirst
  (/* inout */ arraylist* /* param */ AList);

pointer* /* func */ corearraylists__extractlast
  (/* inout */ arraylist* /* param */ AList);

pointer* /* func */ corearraylists__extractat
  (/* inout */ arraylist*    /* param */ AList,
   /* in */    const index_t /* param */ AIndex);
   
void /* func */ corearraylists__removefirst
  (/* inout */ arraylist* /* param */ AList);

void /* func */ corearraylists__removelast
  (/* inout */ arraylist* /* param */ AList);

// ------------------

bool /* func */ corearraylists__tryexchangeat
  (/* inout */ /* restricted */ arraylist*    /* param */ AList,
   /* in */    /* restricted */ const index_t /* param */ AFirstIndex,
   /* in */    /* restricted */ const index_t /* param */ ASecondIndex);

void /* func */ corearraylists__exchangeat
  (/* inout */ /* restricted */ arraylist*    /* param */ AList,
   /* in */    /* restricted */ const index_t /* param */ AFirstIndex,
   /* in */    /* restricted */ const index_t /* param */ ASecondIndex);
   
 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ corearraylists__modulename
  ( noparams );

/* override */ int /* func */ corearraylists__setup
  ( noparams );

/* override */ int /* func */ corearraylists__setoff
  ( noparams );

// ------------------
 
#endif // COREARRAYLISTS__H

// } // namespace corearraylists