/** Module: "corecollarrays.c"
 ** Descr.: "Dynamically allocated arrays library."
 **/
 
// namespace corecollarrays {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------
 
#include "corecollarrays.h"
 
// ------------------

comparison /* func */ corecollarrays__compare
  (/* in */ ansinullstring* /* param */ A, B)
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func
   
 // ------------------
 
void /* func */ corecollarrays__clear
  (/* inout */ ansinullstring* /* param */ ADest)
{
  // ...
} // func
 
 // ...

// ------------------

/* override */ const ansinullstring* /* func */ corecollarrays__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "corecollarrays";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ corecollarrays__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corecollarrays__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace corecollarrays