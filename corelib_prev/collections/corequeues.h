/** Module: "corequeues.h"
 ** Descr.: "..."
 **/
 
// namespace corequeues {
 
// ------------------
 
#ifndef COREQUEUES__H
#define COREQUEUES__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"
#include "corequeuenodes.h"

// ------------------

typedef
  pointer* /* as */ queue;

struct queueheader
{
  // pointer to node before first node
  queuenode* /* var */ FirstMarker;
  
  // pointer to node before last node
  queuenode* /* var */ LastMarker;

  // only applies when items are same type
  // "unknowntype" value means different type
  typecode /* var */ ItemsType;

  // applies instead of "typecode"
  // "0" means not used
  size_t   /* var */ ItemsSize;

  // restricts how many items can be stored
  // "0" means not used
  count_t  /* var */ ItemsMaxCount;

  // how many items are currently stored
  count_t  /* var */ ItemsCurrentCount;
  
  // indicates iterator related objects
  count_t  /* var */ IteratorCount;
  
  equality /* var */ MatchFunc;
  
  // ...
} ;

typedef
  queueheader  /* as */ queue_t;
typedef
  queueheader* /* as */ queue_p;

typedef
  queueheader  /* as */ corequeues__queue_t;
typedef
  queueheader* /* as */ corequeues__queue_p;

// ------------------

typedef
  pointer* /* as */ queueiterator;

struct queueiteratorheader
{
  // backreference to collection
  queue*         /* var */ MainQueue;

  queuenode*     /* var */ CurrentNode;

  count_t        /* var */ ItemsCount;
  size_t         /* var */ ItemsSize;

  listdirections /* var */ Direction;
  
  // ...
} ;

typedef
  queueiteratorheader  /* as */ queueiterator_t;
typedef
  queueiteratorheader* /* as */ queueiterator_p;

typedef
  queueiteratorheader  /* as */ corequeues__queueiterator_t;
typedef
  queueiteratorheader* /* as */ corequeues__queueiterator_p;

// ------------------

/* friend */ bool /* func */ corequeues__sameitemsize
  (/* in */ queue* /* param */ AFirstQueue,
  (/* in */ queue* /* param */ ASecondQueue);

/* friend */ bool /* func */ corequeues__sameitemtype
  (/* in */ queue* /* param */ AFirstQueue,
  (/* in */ queue* /* param */ ASecondQueue);

// ------------------

queue* /* func */ corequeues__createqueue
  ( noparams );

queue* /* func */ corequeues__createqueuebytype
  (/* in */ typecode_t* /* param */ AItemType);

queue* /* func */ corequeues__createqueuerestrictcount
  (/* in */ count_t* /* param */ ACount);
  
queue* /* func */ corequeues__createqueuebysize
  (/* in */ size_t* /* param */ AItemSize);

void /* func */ corequeues__dropqueue
  (/* out */ queue** /* param */ AQueue);

// ------------------

bool /* func */ corequeues__underiteration
  (/* in */ queue* /* param */ AQueue)

queueiterator* /* func */ corequeues__createiterator
  (/* in */ queue* /* param */ AQueue);

void /* func */ corequeues__dropiterator
  (/* out */ queueiterator** /* param */ AIterator);

// ------------------

bool /* func */ corequeues__isdone
  (/* in */ const queueiterator* /* param */ AIterator);
  
bool /* func */ corequeues__trymovenext
  (/* inout */ queueiterator* /* param */ AIterator);

void /* func */ corequeues__movenext
  (/* inout */ queueiterator* /* param */ AIterator);

// ------------------

bool /* func */ corequeues__trygetnode
  (/* inout */ queueiterator* /* param */ AIterator
   /* out */   queuenode*     /* param */ ANode);
   
bool /* func */ corequeues__trygetitem
  (/* inout */ queueiterator* /* param */ AIterator
   /* out */   pointer*       /* param */ AItem);
 
bool /* func */ corequeues__trysetitem
  (/* inout */ queueiterator*  /* param */ AIterator
   /* in */    const  pointer* /* param */ AItem);

void /* func */ corequeues__getnode
  (/* inout */ listiterator* /* param */ AIterator
   /* out */   pointer*      /* param */ ANode);

void /* func */ corequeues__getitem
  (/* inout */ queueiterator* /* param */ AIterator
   /* out */   pointer*                  /* param */ AItem);

void /* func */ corequeues__setitem
  (/* inout */ queueiterator* /* param */ AIterator
   /* in */ const pointer*               /* param */ AItem);

// ------------------

count_t /* func */ corequeues__getcurrentcount
  (/* in */ const queue* /* param */ AQueue);

count_t /* func */ corequeues__getmaxcount
  (/* in */ const queue* /* param */ AQueue);

// ------------------

bool /* func */ corequeues__tryclear
  (/* inout */ queue* /* param */ AQueue);

bool /* func */ corequeues__isempty
  (/* in */ const queue* /* param */ AQueue);

bool /* func */ corequeues__isfull
  (/* in */ const queue* /* param */ AQueue);

bool /* func */ corequeues__hasitems
  (/* in */ const queue* /* param */ AQueue);

void /* func */ corequeues__clear
  (/* inout */ queue* /* param */ AQueue);

// ------------------

bool /* func */ corequeues__tryinsert
  (/* inout */ queue*         /* param */ AQueue
   /* in */    const pointer* /* param */ AItem);

bool /* func */ corequeues__tryextract
  (/* inout */ queue*    /* param */ AQueue
   /* out */   pointer** /* param */ AItem);

bool /* func */ corequeues__tryremove
  (/* inout */ queue* /* param */ AQueue);

void /* func */ corequeues__insert
  (/* inout */ queue*         /* param */ AQueue,
   /* in */    const pointer* /* param */ AItem);

void /* func */ corequeues__extract
  (/* inout */ queue**  /* param */ AQueue,
   /* out */   pointer* /* param */ AItem);

void /* func */ corequeues__remove
  (/* inout */ queue** /* param */ AQueue);

// ------------------


 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ corequeues__modulename
  ( noparams );

/* override */ int /* func */ corequeues__setup
  ( noparams );

/* override */ int /* func */ corequeues__setoff
  ( noparams );

// ------------------

#endif // COREQUEUES__H

// } // namespace corequeues