/** Module: "coreansikeyvalues.h"
 ** Descr.: "String Key, Generic Type Value Pair."
 **/

// namespace coreansikeyvalues {
 
// ------------------
 
#ifndef COREANSIKEYVALUES__H
#define COREANSIKEYVALUES__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"
#include "coreansichars.h"
#include "coreansinullstrs.h"

// ------------------

// opaque type
typedef
  pointer* /* as */ ansikeyvalue;
typedef
  pointer* /* as */ coreansikeyvalues__ansikeyvalue;

struct ansikeyvalueheader
{
  ansinullstring* /* var */ Key;
  pointer*        /* var */ Value;
} ;

typedef
  ansikeyvalueheader  /* as */ ansikeyvalue_t;
typedef
  ansikeyvalueheader* /* as */ ansikeyvalue_p;

// ------------------

ansikeyvalue* /* func */ coreansikeyvalues__createansikeyvalue
  ( noparams );

ansikeyvalue* /* func */ coreansikeyvalues__createansikeyvaluebykey
  (/* in */ pointer* /* param */ AKey);

ansikeyvalue* /* func */ coreansikeyvalues__createansikeyvaluebyvalue
  (/* in */ pointer* /* param */ AKey
   /* in */ pointer* /* param */ AValue);
 
// ------------------

void /* func */ coreansikeyvalues__drop
 (/* out */ ansikeyvalue** /* param */ AKeyValue);

void /* func */ coreansikeyvalues__dropbysize
 (/* out */ ansikeyvalue** /* param */ AKeyValue,
  /* in */  size_t     /* param */ AKeySize,
  /* in */  size_t     /* param */ AValueSize);

void /* func */ coreansikeyvalues__dropextract
 (/* out */ ansikeyvalue** /* param */ AKeyValue,
  /* out */ pointer**  /* param */ AKey,
  /* out */ pointer**  /* param */ AValue);
 
// ------------------

pointer* /* func */ coreansikeyvalues__replacekey
  (/* out */ ansikeyvalue* /* param */ AKeyValue,
   /* in */  pointer*      /* param */ ANewKey);

pointer* /* func */ coreansikeyvalues__replacevalue
  (/* out */ ansikeyvalue* /* param */ AKeyValue,
   /* in */  pointer*      /* param */ ANewValue);
 
// ------------------

/* friend */ bool /* func */ coreansikeyvalues__tryexchangevalues
  (/* inout */ /* restricted */ ansikeyvalue* /* param */ A,
   /* inout */ /* restricted */ ansikeyvalue* /* param */ B);

/* friend */ bool /* func */ coreansikeyvalues__exchangevalues
  (/* inout */ /* restricted */ ansikeyvalue* /* param */ A,
   /* inout */ /* restricted */ ansikeyvalue* /* param */ B);



 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coreansikeyvalues__modulename
  ( noparams );

/* override */ int /* func */ coreansikeyvalues__setup
  ( noparams );

/* override */ int /* func */ coreansikeyvalues__setoff
  ( noparams );

// ------------------

#endif // COREANSIKEYVALUES__H

// } // namespace coreansikeyvalues