/** Module: "corestacknodes.c"
 ** Descr.: "Last In, First Out, Collection Items."
 **/
 
// namespace corestacknodes {
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "corestacknodes.h"
 
// ------------------

/* friend */ bool /* func */ corestacknodes__arelinked
  (/* in */ const stacknode* /* param */ A,
   /* in */ const stacknode* /* param */ B)
{
  bool /* var */ Result = false;
  // ---

  Result = (A != NULL) && (B != NULL);
  if (Result)
  {
      stacknodeheader* /* var */ ThisFirstStackNode  = NULL;
      stacknodeheader* /* var */ ThisSecondStackNode = NULL;

      ThisFirstStackNode  = (stacknodeheader*) A;
      ThisSecondStackNode = (stacknodeheader*) B;
      
      Result =
        (ThisFirstStackNode->Prev == ThisSecondStackNode);
  } // if
    
  // ---
  return Result;
} // func

/* friend */ bool /* func */ corestacknodes__trylink
  (/* inout */ stacknode* /* param */ A,
   /* inout */ stacknode* /* param */ B)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (! ((A == NULL) || (B == NULL)));
  if (Result)
  {
    stacknodeheader* /* var */ ThisFirstNode  = NULL;
    stacknodeheader* /* var */ ThisSecondNode = NULL;
    
    ThisFirstNode  = (stacknode*) A;
    ThisSectorNode = (stacknode*) B;

    ThisFirstNode->Prev = B;
  } // if
     
  // ---
  return Result;
} // func

/* friend */ bool /* func */ corestacknode__trymoveforward
  (/* inout */ stacknode** /* param */ AStackNode)
{
  bool /* var */ Result = false;
  // ---
  
  Result = (AStackNode != NULL);
  if (Result)
  {
    stacknodeheader** /* var */ ThisStackNode = NULL;
   
    ThisStackNode  = (stacknode**) A;

    *ThisStackNode = *ThisStackNode->Prev = B;
  } // if
     
  // ---
  return Result;
} // func

// ------------------

/* friend */ void /* func */ corestacknode__link
  (/* inout */ stacknode* /* param */ A,
   /* inout */ stacknode* /* param */ B)
{
  /* discard */ corestacknodes__trylink(A, B);
} // func

/* friend */ void /* func */ corestacknode__movebackward
  (/* inout */ stacknode** /* param */ AStackNode)
{
  /* discard */ corestacknode__movebackward(AStackNode);
} // func

// ------------------

stacknode* /* func */ corestacknode__createnode
  (/* in */ pointer* /* param */ AItem);
{
  stacknode* /* var */ Result = NULL;
  // ---

  stacknodeheader* /* var */ ThisDestNode = NULL;
     
    Result =
      corememory__clearallocate
        (sizeof(stacknodeheader));
       
    ThisDestNode = (stacknodeheader*) Result;
    ThisDestNode->Item = AItem;
     
  // ---
  return Result;
} // func

pointer* /* func */ corestacknode__dropnode
  (/* out */ stacknode** /* param */ AStackNode);
{
  pointer* /* var */ Result = NULL;
  // ---
  
    stacknodeheader* /* var */ ThisDestNode = NULL;

    ThisDestNode = (stacknodeheader*) AStackNode;
    
    Result =
      corememory__transfer
        (&ThisDestNode->Item);
    
    corememory__cleardeallocate
     (AStackNode, sizeof(stacknodeheader);     
    
  // ---
  return Result;
} // func

// ------------------

/* override */ const ansinullstring* /* func */ corestacks__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "corestacks";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ corestacks__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corestacks__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

 
 // ...

// } // namespace corestacknodes