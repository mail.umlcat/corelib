/** Module: "corequeuenodes.c"
 ** Descr.: "predefined library"
 **/
 
// namespace corequeuenodes {
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "corequeuenodes.h"
 
// ------------------

/* friend */ bool /* func */ corequeuenodes__arelinked
  (/* in */ const queuenode* /* param */ A,
   /* in */ const queuenode* /* param */ B)
{
  bool /* var */ Result = false;
  // ---

  Result = (A != NULL) && (B != NULL);
  if (Result)
  {
      queuenodeheader* /* var */ ThisFirstQueueNode  = NULL;
      queuenodeheader* /* var */ ThisSecondQueueNode = NULL;

      ThisFirstQueueNode  = (queuenodeheader*) A;
      ThisSecondQueueNode = (queuenodeheader*) B;
      
      Result =
        (ThisFirstQueueNode->Next == ThisSecondQueueNode);
  } // if
    
  // ---
  return Result;
} // func

/* friend */ bool /* func */ corequeuenodes__trylink
  (/* inout */ queuenode* /* param */ A,
   /* inout */ queuenode* /* param */ B)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (! ((A == NULL) || (B == NULL)));
  if (Result)
  {
    queuenodeheader* /* var */ ThisFirstNode  = NULL;
    queuenodeheader* /* var */ ThisSecondNode = NULL;
    
    ThisFirstNode  = (queuenode*) A;
    ThisSectorNode = (queuenode*) B;

    ThisFirstNode->Next = B;
  } // if
     
  // ---
  return Result;
} // func

/* friend */ bool /* func */ corequeuenode__trymoveforward
  (/* inout */ queuenode** /* param */ AQueueNode)
{
  bool /* var */ Result = false;
  // ---
  
  Result = (AQueueNode != NULL);
  if (Result)
  {
    queuenodeheader** /* var */ ThisQueueNode = NULL;
   
    ThisQueueNode  = (queuenode**) A;

    *ThisQueueNode = *ThisQueueNode->Next = B;
  } // if
     
  // ---
  return Result;
} // func

// ------------------

/* friend */ void /* func */ corequeuenode__link
  (/* inout */ queuenode* /* param */ A,
   /* inout */ queuenode* /* param */ B)
{
  /* discard */ corequeuenodes__trylink(A, B);
} // func

/* friend */ void /* func */ corequeuenode__moveforward
  (/* inout */ queuenode** /* param */ AQueueNode)
{
  /* discard */ corequeuenode__moveforward(AQueueNode);
} // func

// ------------------

queuenode* /* func */ corequeuenode__createnode
  (/* in */ pointer* /* param */ AItem);
{
  queuenode* /* var */ Result = NULL;
  // ---

  queuenodeheader* /* var */ ThisDestNode = NULL;
    
    Result =
      corememory__clearallocate
        (sizeof(queuenodeheader));
       
    ThisDestNode = (queuenodeheader*) Result;
    ThisDestNode->Item = AItem;
     
  // ---
  return Result;
} // func

pointer* /* func */ corequeuenode__dropnode
  (/* out */ queuenode** /* param */ AQueueNode);
{
  pointer* /* var */ Result = NULL;
  // ---
  
    queuenodeheader* /* var */ ThisDestNode = NULL;

    ThisDestNode = (queuenodeheader*) AQueueNode;
    
    Result =
      corememory__transfer
        (&ThisDestNode->Item);
    
    corememory__cleardeallocate
     (AQueueNode, sizeof(queuenodeheader);     
    
  // ---
  return Result;
} // func

// ------------------

/* override */ const ansinullstring* /* func */ corequeuenodes__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "corequeues";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ corequeuenodes__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corequeuenodes__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

 
 // ...

// } // namespace corequeuenodes