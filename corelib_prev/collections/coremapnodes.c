/** Module: "coremapnodes.c"
 ** Descr.: "Unique Key and duplicated value,"
 **         "list items."
 **/

// namespace coremapnodes {
 
// ------------------
 
#include <stdlib.h"
#include <stddef.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "corekeyvalues.h"

// ------------------

#include "coremapnodes.h"
 
// ------------------

/* friend */ bool /* func */ coremapnodes__arelinked
  (/* in */ const mapnode* /* param */ A,
   /* in */ const mapnode* /* param */ B)
{
  bool /* var */ Result = false;
  // ---

  Result = (A != NULL) && (B != NULL);
  if (Result)
  {
      mapnodeheader* /* var */ ThisFirstMapNode  = NULL;
      mapnodeheader* /* var */ ThisSecondMapNode = NULL;

      ThisFirstMapNode  = (mapnodeheader*) A;
      ThisSecondMapNode = (mapnodeheader*) B;
      
      Result =
        (ThisFirstMapNode->Next  == ThisSecondMapNode) &&
        (ThisSecondMapNode->Prev == ThisFirstMapNode);
  } // if
    
  // ---
  return Result;
} // func

/* friend */ bool /* func */ coremapnodes__trylink
  (/* inout */ mapnode* /* param */ A,
   /* inout */ mapnode* /* param */ B);
{
  bool /* var */ Result = false;
  // ---

  Result = (A != NULL) && (B != NULL);
  if (Result)
  {
      mapnodeheader* /* var */ ThisFirstMapNode  = NULL;
      mapnodeheader* /* var */ ThisSecondMapNode = NULL;

      ThisFirstMapNode  = (mapnodeheader*) A;
      ThisSecondMapNode = (mapnodeheader*) B;
      
      ThisFirstMapNode->Next  = ThisSecondMapNode;
      ThisSecondMapNode->Prev = ThisFirstMapNode;
  } // if
} // func

/* friend */ bool /* func */ coremapnodes__trymoveforward
  (/* inout */ mapnode* /* param */ AMapNode)
{
  bool /* var */ Result = false;
  // ---
  
  Result = (AMapNode != NULL);
  if (Result)
  {
    listnodeheader** /* var */ ThisMapNode = NULL;
   
    ThisMapNode  = (listnode**) A;

    *ThisMapNode = *ThisMapNode->Next = B;
  } // if
     
  // ---
  return Result;
} // func

/* friend */ bool /* func */ coremapnodes__trymovebackward
  (/* inout */ mapnode* /* param */ AMapNode)
{
  bool /* var */ Result = false;
  // ---
  
  Result = (AMapNode != NULL);
  if (Result)
  {
    listnodeheader** /* var */ ThisMapNode = NULL;
   
    ThisMapNode  = (listnode**) A;

    *ThisMapNode = *ThisMapNode->Next = B;
  } // if
     
  // ---
  return Result;
} // func

// ------------------

/* friend */ void /* func */ coremapnodes__link
  (/* inout */ mapnode* /* param */ A,
   /* inout */ mapnode* /* param */ B)
{
  /* discard */ coremapnodes__trylink(A, B);
} // func

/* friend */ void /* func */ coremapnodes__moveforward
  (/* inout */ mapnode** /* param */ AMapNode)
{
  /* discard */ coremapnodes__moveforward(AMapNode);
} // func

/* friend */ void /* func */ coremapnodes__movebackward
  (/* inout */ mapnode** /* param */ AMapNode)
{
  /* discard */ coremapnodes__movebackward(AMapNode);
} // func

// ------------------

mapnode* /* func */ coremapnodes__createnodeempty
  ( noparams )
{
  mapnode* /* var */ Result = NULL;
  // ---
  
  if (AKey != NULL)
  {
    mapnodeheader* /* var */ ThisMapNode = NULL;
  	  
    ThisMapNode =
      corememory__clearallocate
        (sizeof(mapnodeheader));
    ThisMapNode->KeyValue = NULL;
    
    Result =
      corememory__transfer(&ThisMapNode);
  } // if
     
  // ---
  return Result;
} // func

mapnode* /* func */ coremapnodes__createnodebykey
  (/* in */ pointer* /* param */ AKey)
{
  mapnode* /* var */ Result = NULL;
  // ---
  
  if (AKey != NULL)
  {
    mapnodeheader* /* var */ ThisMapNode = NULL;
  	 
    ThisMapNode =
      corememory__clearallocate
        (sizeof(mapnodeheader));
    ThisMapNode->KeyValue =
      corekeyvalues__createkeyvaluebykey
        (AKey);
        
    Result =
      corememory__transfer(&ThisMapNode);
  } // if
     
  // ---
  return Result;
} // func

mapnode* /* func */ coremapnodes__createnodebyvalue
  (/* in */ pointer* /* param */ AKey,
   /* in */ pointer* /* param */ AValue)
{
  mapnode* /* var */ Result = NULL;
  // ---
  
  if (AKey != NULL)
  {
    mapnodeheader* /* var */ ThisMapNode = NULL;
  	  
    ThisMapNode =
      corememory__clearallocate
        (sizeof(mapnodeheader));
    ThisMapNode->KeyValue =
      corekeyvalues__createkeyvaluebyvalue
        (AKey, AValue);
        
    Result =
      corememory__transfer(&ThisMapNode);
  } // if
         
  // ---
  return Result;
} // func

mapnode* /* func */ coremapnodes__createnodebykeyvalue
  (/* in */ pointer* /* param */ AKeyValue)
{
  mapnode* /* var */ Result = NULL;
  // ---
   
  if (AKeyValue != NULL)
  {
    mapnodeheader* /* var */ ThisMapNode = NULL;
  	
    ThisMapNode =
      corememory__clearallocate
        (sizeof(mapnodeheader));
            
    ThisMapNode->KeyValue =
        (AKeyValue);
        
    Result =
      corememory__transfer(&ThisMapNode);
  } // if
     
  // ---
  return Result;
} // func

// ------------------

void /* func */ coremapnodes__dropnode
  (/* out */ mapnode**  /* param */ AMapNode)
{
  bool /* var */ CanContinue = FALSE;

  CanContinue =
    (AMapNode != NULL);
 
  if (CanContinue)
  {
    corememory__cleardeallocate
      (AMapNode, sizeof(mapnodeheader));
  } // if
} // func

void /* func */ coremapnodes__dropnodebysize
  (/* out */ mapnode** /* param */ AMapNode,
   /* in */  size_t    /* param */ AKeySize,
   /* in */  size_t    /* param */ AValueSize)
{
  bool /* var */ CanContinue = FALSE;

  CanContinue =
    (AKeyValue != NULL) && (AKeySize > 0) && (AValueSize > 0);
  if (CanContinue)
  {
    mapnodeheader* /* var */ ThisMapNode = NULL;
    keyvalue*      /* var */ AKeyValue = NULL;
   
    ThisMapNode =
      (mapnodeheader*) AMapNode;

    AKeyValue =
      ThisMapNode->KeyValue;
      
    corekeyvalues__dropbysize
      (&AKeyValue, AKeySize, AValueSize);
      
    corememory__cleardeallocate
      (AMapNode, sizeof(mapnodeheader));
  } // if
} // func

void /* func */ coremapnodes__dropnodeextractkeyvalue
  (/* out */ mapnode**  /* param */ AMapNode,
   /* out */ keyvalue** /* param */ AKeyValue)
{
  bool /* var */ CanContinue = FALSE;

  CanContinue =
    (AKeyValue != NULL) &&
    (AMapNode != NULL);
 
  if (CanContinue)
  {
    mapnodeheader* /* var */ ThisMapNode = NULL;
    
    ThisMapNode =
      (mapnodeheader*) AMapNode;

    AKeyValue =
      corememory__transfer(&(ThisMapNode->KeyValue));
 
    corememory__cleardeallocate
      (AMapNode, sizeof(mapnodeheader));
  } // if
} // func

void /* func */ coremapnodes__dropnodeextract
  (/* out */ mapnode** /* param */ AMapNode,
   /* out */ pointer** /* param */ AKey,
   /* out */ pointer** /* param */ AValue)
{
  bool /* var */ CanContinue = FALSE;

  CanContinue =
    (AMapNode != NULL) &&
    (AKey != NULL) &&
    (AValue != NULL);
 
  if (CanContinue)
  {
    mapnodeheader* /* var */ ThisMapNode = NULL;
    
    ThisMapNode =
      (mapnodeheader*) AMapNode;

    AKeyValue =
      corememory__transfer(&(ThisMapNode->KeyValue));
 
    corememory__cleardeallocate
      (AMapNode, sizeof(mapnodeheader));
  } // if
} // func

// ------------------

pointer* /* func */ coremapnodes__getkey
  (/* inout */ mapnode* /* param */ AMapNode)
{
  pointer* /* var */ Result = NULL;
  // ---

  Result = (AMapNode != NULL);
  if (Result)
  {
    mapnodeheader*  /* var */ ThisMapNode  = NULL;
    keyvalueheader* /* var */ ThisKeyValue = NULL;

    ThisMapNode =
      (mapnodeheader*) corememory__share(AMapNode);

    ThisKeyValue =
      (keyvalueheader*) corememory__share
        (ThisMapNode->KeyValue);
      
    Result =
      ThisKeyValue->Key;
  } // if
    
  // ---
  return Result;
} // func

pointer* /* func */ coremapnodes__getvalue
  (/* inout */ mapnode* /* param */ AMapNode)

// ------------------

bool /* func */ coremapnodes__MatchKey
  (/* in */ mapnode* /* param */ AMapNode,
   /* in */ pointer* /* param */ AKey,
   /* in */ equality /* param */ MatchFunc)
{
  bool /* var */ Result = FALSE;


  Result =
    (AMapNode != NULL) &&
    (AKey != NULL) &&
    (MatchFunc != NULL);
 
  if (Result)
  {
    mapnodeheader*  /* var */ ThisMapNode  = NULL;
    keyvalueheader* /* var */ ThisKeyValue = NULL;
    keyvalue*       /* var */ AKeyValue = NULL;

    ThisMapNode =
      (mapnodeheader*) AMapNode;

    Result =
      MatchFunc(ThisMapNode->Key, AKey);
  } // if
  
  
} // func

// ------------------

pointer* /* func */ coremapnodes__replacekey
  (/* out */ mapnode* /* param */ AMapNode,
   /* in */  pointer* /* param */ AKey)
{
  pointer* /* var */ Result = NULL;
  // ---
  
  bool /* var */ CanContinue = FALSE;

  CanContinue =
    (AMapNode != NULL) &&
    (AKey != NULL);
 
  if (CanContinue)
  {
    mapnodeheader*  /* var */ ThisMapNode  = NULL;
    keyvalueheader* /* var */ ThisKeyValue = NULL;

    ThisMapNode =
      (mapnodeheader*) corememory__share(AMapNode);

    Result =
      corekeyvalues__replacekey
        (ThisMapNode->KeyValue, AKey);
  } // if
     
  // ---
  return Result;
} // func

pointer* /* func */ coremapnodes__replacevalue
  (/* out */ mapnode* /* param */ AMapNode,
   /* in */  pointer* /* param */ AValue)
{
  pointer* /* var */ Result = NULL;
  // ---
  
  bool /* var */ CanContinue = FALSE;

  CanContinue =
    (AMapNode != NULL) &&
    (AValue != NULL);
 
  if (CanContinue)
  {
    mapnodeheader*  /* var */ ThisMapNode  = NULL;
    keyvalueheader* /* var */ ThisKeyValue = NULL;

    ThisMapNode =
      (mapnodeheader*) corememory__share(AMapNode);

    Result =
      corekeyvalues__replacevalue
        (ThisMapNode->KeyValue, AValue);
  } // if
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coremapnodes__tryextractkeyvalue
  (/* out */ mapnode*   /* param */ AMapNode,
   /* out */ keyvalue** /* param */ AKeyValue)
{
  bool /* var */ Result = NULL;
  // ---
  
  Result =
    (AMapNode != NULL) &&
    (AKeyValue != NULL);
 
  if (Result)
  {
    mapnodeheader*  /* var */ ThisMapNode  = NULL;

    ThisMapNode =
      (mapnodeheader*) corememory__share(AMapNode);

    AKeyValue =
      corememory__transfer(&(ThisMapNode->KeyValue));
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coremapnodes__tryextractitems
  (/* out */ mapnode*  /* param */ AMapNode,
   /* out */ pointer** /* param */ AKey,
   /* out */ pointer** /* param */ AValue)
{
  bool /* var */ Result = NULL;
  // ---
  
  Result =
    (AMapNode != NULL) &&
    (AKey != NULL) &&
    (AValue != NULL);
 
  if (Result)
  {
    mapnodeheader*  /* var */ ThisMapNode  = NULL;
    keyvalue*       /* var */ AKeyValue = NULL;
    
    ThisMapNode =
      (mapnodeheader*) corememory__share(AMapNode);

    AKeyValue =
      corememory__transfer(&(ThisMapNode->KeyValue));
    
    corekeyvalues__dropextract
      (&AKeyValue, AKey, AValue);
  } // if
     
  // ---
  return Result;
} // func
   
void /* func */ coremapnodes__extractkeyvalue
  (/* out */ mapnode*   /* param */ AMapNode,
   /* out */ keyvalue** /* param */ AKeyValue)
{
  /* discard */ coremapnodes__tryextractkeyvalue
    (AMapNode, AKeyValue);
} // func

void /* func */ coremapnodes__extractitems
  (/* out */ mapnode*  /* param */ AMapNode,
   /* out */ pointer** /* param */ AKey,
   /* out */ pointer** /* param */ AValue)
{
  /* discard */ coremapnodes__tryextractitems
    (AMapNode, AKey, AValue);
} // func


// ------------------

 // ...



// ------------------

/* override */ int /* func */ coremapnodes__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coremapnodes__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coremapnodes