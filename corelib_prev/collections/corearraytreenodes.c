/** Module: "corearraytreenodes.c"
 ** Descr.: "predefined library"
 **/
 
// namespace corearraytreenodes {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"
#include "corearraylistnodes.h"
#include "corearraylists.h"

// ------------------

#include "corearraytreenodes.h"
 
// ------------------

/* friend */ bool /* func */ corearraytreenodes__arelinked
  (/* in */ const arraytreenode* /* param */ A,
   /* in */ const arraytreenode* /* param */ B)
{
  bool /* var */ Result = false;
  // ---

  Result = (A != NULL) && (B != NULL);
  if (Result)
  {
      arraytreenodeheader* /* var */ C = NULL;
      arraytreenodeheader* /* var */ D = NULL;

      arraylistnodeheader* /* var */ E = NULL;
      arraylistnodeheader* /* var */ F = NULL;

      C = (arraytreenodeheader*) A;
      D = (arraytreenodeheader*) B;
    
      E = (arraylistnodeheader*) A->ParentListNode;
      F = (arraylistnodeheader*) B->ParentListNode;
    
      Result =
        corearraylistnodes__arelinked
          (E, F);
  } // if
    
  // ---
  return Result;
} // func

/* friend */ bool /* func */ corearraytreenodes__trymoveforward
  (/* inout */ arraytreenode** /* param */ ANode)
{
  bool /* var */ Result = false;
  // ---
  
  Result = (ANode != NULL);
  if (Result)
  {
    arraytreenodeheader* /* var */ ThisTreeNode = NULL;
    arraylistnodeheader* /* var */ ThisListNode = NULL;
 
    ThisTreeNode  = (arraytreenodeheader*) ANode;
    ThisListNode  = (arraylistnode*) ANode->ParentListNode;
// ...
    *ThisListNode = *ThisListNode->Next = B;
  } // if
     
  // ---
  return Result;
} // func

/* friend */ bool /* func */ corearraytreenodes__trymovebackward
  (/* inout */ arraytreenode* /* param */ ANode)
{
  bool /* var */ Result = false;
  // ---
  
  Result = (ANode != NULL);
  if (Result)
  {
    arraytreenodeheader* /* var */ ThisTreeNode = NULL;
    arraylistnodeheader* /* var */ ThisListNode = NULL;
 
    ThisTreeNode  = (arraytreenodeheader*) ANode;
    ThisListNode  = (arraylistnode*) ANode->ParentListNode;
// ...
    *ThisListNode = *ThisListNode->Next = B;
  } // if
     
  // ---
  return Result;
} // func

// ------------------

/* friend */ void /* func */ corearraytreenodes__moveforward
  (/* inout */ arraytreenode* /* param */ ANode)
{
  /* discard */ corearraylistnodes__moveforward(ANode);
} // func

/* friend */ void /* func */ corearraytreenodes__movebackward
  (/* inout */ arraytreenode* /* param */ ANode)
{
  /* discard */ corearraylistnodes__movebackward(ANode);
} // func

// ------------------

arraytreenode* /* func */ corearraytreenodes__createnode
  (/* in */ const arraytree*     /* param */ ATree,
   /* in */ const arraylist*     /* param */ AList,
   /* in */ const arraytreenode* /* param */ AParentNode,
   /* in */ const arraylistnode* /* param */ AListNode,
   /* in */ index_t              /* param */ AIndex,
   /* in */ pointer*             /* param */ AItem)
{
  arraytreenode* /* var */ Result = NULL;
  // ---
  
  bool /* var */ CanContinue = false;
  
  CanContinue =
    (ATree != NULL);
  if (CanContinue)
  {
    arraytreeheader*     /* var */ ThisTree = NULL;
    arraytreenodeheader* /* var */ ThisNode = NULL;
  
    ThisTree =
      (arraytreeheader*)ATree;
      
    // for "root": 
    // "AParentNode", "AList",
    // can be empty
    ThisNode =
      corememory__clearallocate
        (sizeof(arraytreeodeheader);
  
    ThisNode->Tree =
      ATree;
    ThisNode->List =
      AList;
    ThisNode->ListNode =
      AListNode;
      
    ThisNode->Item =
      AItem;

    typecode /* var */ AItemsType = 0;
    size_t   /* var */ AItemsSize = 0;

    AItemsType =
      ThisTree->ItemsType;
    AItemsSize =
      ThisTree->ItemsSize;
   
    ThisNode->Nodes =
      corelists__createlist();
      
    arraylistheader* /* var */ ThisNodeList = NULL;
    ThisNodeList =
      (arraylistheader*) ThisNode->Nodes;

    ThisNodeList->ItemsType =
      AItemsType;
    ThisNodeList->ItemsSize =
      AItemsSize;

    Result =
      corememory__transfer(&ThisNode);
  } // if

  // ---
  return Result;
} // func

pointer* /* func */ corearraytreenodes__dropnode
  (/* out */ arraytreenode** /* param */ ANode)
{
  pointer* /* var */ Result = NULL;
  // ---
     
  bool /* var */ CanContinue = false;
  
  CanContinue =
    (ANode != NULL);
  if (CanContinue)
  {
    arraytreenodeheader* /* var */ ThisNode = NULL;

    ThisNode =
      (arraytreenodeheader**) ANode;

    corearraytreenodes__clear
      (ANode);
    corelists__droplist
     ( (&ThisNode->Nodes));

    corememory__cleardeallocate
      (ANode, sizeof(arraytreenodeheader));
  } // if

  // ---
  return Result;
} // func

// ------------------

count_t /* func */ corearraytreenodes__getcurrentcount
  (/* in */ const arraytreenode* /* param */ ANode)
{
  count_t /* var */ Result = 0;
  // ---
     
  bool /* var */ CanContinue = false;
  
  CanContinue =
    (ANode != NULL);
  if (CanContinue)
  {
    arraytreenodeheader* /* var */ ThisNode = NULL;
    arraylistheader*     /* var */ ThisNodeList = NULL;

    ThisNode =
      (arraytreenodeheader*) ANode;

    ThisNodeList =
      (arraylistheader*) ThisNode->Nodes;

    Result =
      ThisNodeList->ItemsCurrentCount;
  } // if

  // ---
  return Result;
} // func

count_t /* func */ corearraytreenodes__getmaxcount
  (/* in */ const arraytreenode* /* param */ ANode)
{
  count_t /* var */ Result = 0;
  // ---
     
  bool /* var */ CanContinue = false;
  
  CanContinue =
    (ANode != NULL);
  if (CanContinue)
  {
    arraytreenodeheader* /* var */ ThisNode = NULL;
    arraylistheader*     /* var */ ThisNodeList = NULL;

    ThisNode =
      (arraytreenodeheader*) ANode;

    ThisNodeList =
      (arraylistheader*) ThisNode->Nodes;

    Result =
      ThisNodeList->ItemsMaxCount;
  } // if

  // ---
  return Result;
} // func

// ------------------

bool /* func */ corearraytreenodes__isroot
  (/* in */ const arraytreenode* /* param */ ANode)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (ANode != NULL);
  if (Result)
  {
    arraytreeheader*     /* var */ ThisTree = NULL;
    arraytreenodeheader* /* var */ ThisNode = NULL;

    ThisNode =
      (arraytreenodeheader*) ANode;
    ThisTree =
      (arraytreeheader*) 
    Result 
      (ThisTree != NULL);
    if (Result)
    {
       Result =
         (ANode == ThisTree->RootNode);
    } // if
  } // if

  // ---
  return Result;
} // func

bool /* func */ corearraytreenodes__isempty
  (/* in */ const arraytreenode* /* param */ ANode)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (ANode != NULL);
  if (Result)
  {
    arraytreenodeheader* /* var */ ThisNode = NULL;

    ThisNode =
      (arraytreenodeheader*) ANode;
      
    Result =
      corearraylists__getcurrentcount
        (ThisNode->Nodes);
  } // if

  // ---
  return Result;
} // func

bool /* func */ corearraytreenodes__hasitems
  (/* in */ const arraytreenode* /* param */ ANode)
{
  bool /* var */ Result = false;
  // ---
  
  Result = !(corearraytreenodes__isempty(AList));

  // ---
  return Result;
} // func

// ------------------

bool /* func */ corearraytreenodes__tryclear
  (/* inout */ arraytreenode* /* param */ ANode)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (ANode != NULL);
  if (Result)
  {
    arraylistiterator*       /* var */ AIterator = NULL;
    arraylistiteratorheader* /* var */ ThisIterator = NULL;

    arraytreenodeheader*     /* var */ ThisNode = NULL;

    arraytreeheader*         /* var */ AChildNode = NULL;
    arraytreenodeheader*     /* var */ ThisChildNode = NULL;

    arraylistnodeheader*     /* var */ ThisListNode = NULL;

    ThisNode =
      (arraylistnodeheader*) ANode;
    
    // --> drop stored items
    AList = ANode->Nodes;

    AIterator =
      corearraylists__createiteratorfwd(AList);
    ThisIterator =
      (arraylistiteratorheader*) AIterator;

    while 
      (!corearraylists__isdone(AIterator))
    {
      ThisListNode =
        (arraylistnodeheader*) corearraylists__getnode
          (AIterator);
 
      AChildNode =
        ThisListNode->Item;
      ThisChildNode =
        (arraytreenodeheader*) AChildNode;
        
      if (corearraytreenodes__hasitems(ThisChildNode))
      {
        corearraytreenodes__clear(AChildNode);
      }

      /* discard */ corearraytreenodes__dropnode
        (&AChildNode);
      ThisListNode->Item = NULL;
      ATreeNode = NULL;

      corearraylists__movenext(AIterator);
    } // while

    corearraylists__dropiterator(&AList);
    
    // --> remove list links
    corearraylists__clear
      (ANode->Nodes);
  } // if

  // ---
  return Result;
} // func

void /* func */ corearraytreenodes__clear
  (/* inout */ arraytreenode* /* param */ ANode)
{
  /* discard */ corearraytreenodes__tryclear
    (ANode);
} // func

// ------------------

bool /* func */ corearraytreenodes__isvalidindex
  (/* in */ const arraytreenode* /* param */ ANode
   /* in */ const index_t        /* param */ AIndex)
{
  bool /* var */ Result = FALSE;
  // ---
     
  if (ANode != NULL)
  {
    arraylist*           /* var */ ATree = NULL;
    arraylistheader*     /* var */ ThisTree = NULL;
    arraylistnodeheader* /* var */ ThisNode = NULL;

    ThisNode = (arraylistnodeheader*) ANode;
    ATree = ThisNode->Tree;
    ThisTree = (arraylistheader*) ATree;
  	
    Result =
      (AIndex >= 0) &&
      (AIndex < ThisTree->ItemsMaxCount);
  } // if
 
  // ---
  return Result;
} // func

bool /* func */ corearraytreenodes__trygetitemat
  (/* in */  const arraytreenode* /* param */ ANode,
   /* in */  const index_t        /* param */ AIndex,
   /* out */ pointer**            /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (AList != NULL) &&
    (AItem != NULL) &&
    corearraylists__isvalidindex
      (AList, AIndex);
  if (Result)
  {
    arraylistnode*       /* var */ ANode = NULL;
    arraylistnodeheader* /* var */ ThisNode = NULL;
 	
    Result = corearraylists__tryseeknodeatfwd
      (AList, AIndex, &ANode);
    if (Result)
    {
      ThisNode =
        (arraylistnodeheader*) ANode;
      
      *AItem =
        ThisNode->Item;
     } // if
   } // if
    
  // ---
  return Result;
} // func

bool /* func */ corearraytreenodes__tryreaditemat
  (/* in */  const arraytreenode* /* param */ ANode,
   /* in */  const index_t        /* param */ AIndex,
   /* out */ pointer*             /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (AList != NULL) &&
    (AItem != NULL) &&
    corearraytrees__isvalidindex
      (AList, AIndex);
  if (Result)
  {
    arraytreenode*       /* var */ ANode = NULL;
    arraytreenodeheader* /* var */ ThisNode = NULL;
    
    arraytreeheader*     /* var */ ThisList = NULL;
  	
    Result = corearraytrees__tryseeknodeatfwd
      (AList, AIndex, &ANode);
    if (Result)
    {
      ThisNode =
        (arraytreenodeheader*) ANode;
      
      ThisList =
        (arraytreeheader*) AList;
        
      corememory__safecopy
        (AItem, ThisNode->Item, ThisList->ItemsSize);
     } // if
   } // if
    
  // ---
  return Result;
} // func

bool /* func */ corearraytreenodes__trywriteitemat
  (/* in */ const arraytreenode* /* param */ ANode,
   /* in */ const index_t        /* param */ AIndex)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (AList != NULL) &&
    (AItem != NULL) &&
    corearraytrees__isvalidindex
      (AList, AIndex);
  if (Result)
  {
    arraytreenode*       /* var */ ANode = NULL;
    arraytreenodeheader* /* var */ ThisNode = NULL;
    
    arraytreeheader*     /* var */ ThisList = NULL;
  	
    Result = corearraytrees__tryseeknodeatfwd
      (AList, AIndex, &ANode);
    if (Result)
    {
      ThisNode =
        (arraytreenodeheader*) ANode;
      
      ThisList =
        (arraytreeheader*) AList;
        
      corememory__safecopy
        (ThisNode->Item, AItem, ThisList->ItemsSize);
     } // if
   } // if
    
  // ---
  return Result;
} // func

pointer* /* func */ corearraytreenodes__getitemat
  (/* in */  const arraytreenode* /* param */ ANode,
   /* in */  const index_t        /* param */ AIndex)
{
  pointer* /* var */ Result = NULL;
  // ---

  /* discard */ corearraylists__trygetitemat
    (ANode, AIndex, &Result);
    
  // ---
  return Result;
} // func

void /* func */ corearraytreenodes__readitemat
  (/* in */  const arraytreenode* /* param */ ANode,
   /* in */  const index_t        /* param */ AIndex,
   /* out */ pointer*             /* param */ AItem)
{
  /* discard */ corearraytreenodes__tryreaditemat
    (ANode, AIndex, AItem);
} // func

void /* func */ corearraytreenodes__writeitemat
  (/* in */ const arraytreenode* /* param */ ANode,
   /* in */ const index_t        /* param */ AIndex)
{
  /* discard */ corearraytreenodes__trywriteitemat
    (ANode, AIndex, AItem);
} // func

// ------------------

listnode* /* func */ corearraytreenodes__listnodetotreenode
  (/* in  */ const arraytreenode*  /* param */ ANode)
{
  listnode* /* var */ Result = NULL;
  // ---
  
  Result =
    (ANode != NULL);
  if (Result)
  {



  } // if

  // ---
  return Result;
} // func

bool /* func */ corearraytreenodes__tryseeknodeatfwd
  (/* in  */ const arraytreenode*  /* param */ AParentNode,
   /* in  */ const index_t         /* param */ AIndex,
   /* out */ const arraytreenode** /* param */ ADestNode)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (AParentNode != NULL) &&
    (ANode != NULL) &&
     corearraytrees__isvalidindex(AIndex);
  if (Result)
  {
    arraytreeiterator*       /* var */ AIterator = NULL;
    arraytreeiteratorheader* /* var */ ThisIterator = NULL;

    arraytreenode*        /* var */ ANode = NULL;
    arraytreenodeheader*  /* var */ ThisNode = NULL;

    AIterator =
      corearraytrees__createiteratorfwd(AParentNode);
    ThisIterator =
      (arraytreeiteratorheader*) AIterator;

    while 
      (!corearraytrees__isdone(AIterator) &&
        !Result)
    {
      Result =
        (ThisParentNode->Index == AIndex);
      if (Result)
      {
        *ADestNode = 
          ThisNode;
      } // if

      corearraytrees__movenext(AIterator);
    } // while

    corearraytrees__dropiterator(&AIterator);
  } // if

  // ---
  return Result;
} // func

bool /* func */ corearraytreenodes__tryseeknodeatbwd
  (/* in  */ const arraytreenode*  /* param */ AParentNode,
   /* in  */ const index_t         /* param */ AIndex,
   /* out */ const arraytreenode** /* param */ ADestNode)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (AParentNode != NULL) &&
    (ANode != NULL) &&
     corearraytrees__isvalidindex(AIndex);
  if (Result)
  {
    arraytreeiterator*       /* var */ AIterator = NULL;
    arraytreeiteratorheader* /* var */ ThisIterator = NULL;

    arraytreenode*        /* var */ ANode = NULL;
    arraytreenodeheader*  /* var */ ThisNode = NULL;

    AIterator =
      corearraytrees__createiteratorbwd(AParentNode);
    ThisIterator =
      (arraytreeiteratorheader*) AIterator;

    while 
      (!corearraytrees__isdone(AIterator) &&
        !Result)
    {
      Result =
        (ThisParentNode->Index == AIndex);
      if (Result)
      {
        *ADestNode = 
          ThisNode;
      } // if

      corearraytrees__movenext(AIterator);
    } // while

    corearraytrees__dropiterator(&AIterator);
  } // if

  // ---
  return Result;
} // func

arraytreenode* /* func */ corearraytreenodes__seeknodeatfwd
  (/* in */ const arraytreenode* /* param */ AParentNode,
   /* in */ const index_t        /* param */ AIndex)
{
  arraytreenode* /* var */ Result = NULL;
  // ---

  /* discard */ corearraytrees__tryseeknodeatfwd
    (AParentNode, AIndex, &Result);
  
  // ---
  return Result;
} // func

arraytreenode* /* func */ corearraytreenodes__seeknodeatbwd
  (/* in */ const arraytreenode* /* param */ AParentNode,
   /* in */ const index_t        /* param */ AIndex)
{
  arraytreenode* /* var */ Result = NULL;
  // ---

  /* discard */ corearraytrees__tryseeknodeatfwd
    (AParentNode, AIndex, &Result);
  
  // ---
  return Result;
} // func

// ------------------

bool /* func */ corearraytreenodes__tryinsertfirst
  (/* inout */ arraytreenode*  /* param */ AParentNode,
   /* in */    const pointer*  /* param */ AItem,
   /* out */   arraylistnode** /* param */ ANewNode);
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (ATree != NULL) && (AItem != NULL);
  if (Result)
  {
    arraytreeheader*     /* var */ ThisParentNode = NULL;
    arraytreenode*       /* var */ ANewNode       = NULL;
    arraytreenodeheader* /* var*/  ThisNewNode    = NULL;
    arraylistnode*       /* var */ AListNode      = NULL;

    AInsertNode =
      corearraytreenodes__createnode
        (AItem);
    ThisInsertNode =
      (arraytreenodeheader*) corememory__share
        (AInsertNode);

    ThisParentNode = (arraytreeheader*) AParentNode;
    
    AListNode =
      corearraylists__insertfirst
        (ThisParentNode->Nodes, AInsertNode);

    ThisInsertNode->List =
      (arraytreenodeheader*) corememory__share
        (ThisParentNode->Nodes);
  
    ThisInsertNode->Tree =
      (arraytreeheader*) corememory__share
        (ThisParentNode->Tree);

    ThisInsertNode->ParentTreeNode =
      (arraytreeheader*) corememory__share
        (ThisParentNode);
 
    ThisInsertNode->ParentListNode =
      corememory__share(AListNode);

    *ANewNode =
      corememory__transfer
        (&AInsertNode);
  } // if

  // ---
  return Result;
} // func

bool /* func */ corearraytreenodes__tryinsertlast
  (/* inout */ arraytreenode*  /* param */ AParentNode,
   /* in */    const pointer*  /* param */ AItem,
   /* out */   arraylistnode** /* param */ ANewNode);
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (ATree != NULL) && (AItem != NULL);
  if (Result)
  {
    arraytreeheader*     /* var */ ThisParentNode = NULL;
    arraytreenode*       /* var */ ANewNode       = NULL;
    arraytreenodeheader* /* var*/  ThisNewNode    = NULL;
    arraylistnode*       /* var */ AListNode      = NULL;

    AInsertNode =
      corearraytreenodes__createnode
        (AItem);
    ThisInsertNode =
      (arraytreenodeheader*) corememory__share
        (AInsertNode);

    ThisParentNode = (arraytreeheader*) AParentNode;

    AListNode =
      corearraylists__insertlast
        (ThisParentNode->Nodes, AInsertNode);

    ThisInsertNode->List =
      (arraytreenodeheader*) corememory__share
        (ThisParentNode->Nodes);
  
    ThisInsertNode->Tree =
      (arraytreeheader*) corememory__share
        (ThisParentNode->Tree);

    ThisInsertNode->ParentTreeNode =
      (arraytreeheader*) corememory__share
        (ThisParentNode);
 
    ThisInsertNode->ParentListNode =
      corememory__share(AListNode);

    *ANewNode =
      corememory__transfer
        (&AInsertNode);
  } // if

  // ---
  return Result;
} // func

bool /* func */ corearraytreenodes__tryinsertat
  (/* inout */ arraytreenode*  /* param */ AParentNode,
   /* in */    const index_t   /* param */ AIndex,
   /* in */    const pointer*  /* param */ AItem,
   /* out */   arraylistnode** /* param */ ANewNode)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (ATree != NULL) && (AItem != NULL);
  if (Result)
  {
    arraytreeheader*     /* var */ ThisParentNode = NULL;
    arraytreenode*       /* var */ ANewNode       = NULL;
    arraytreenodeheader* /* var*/  ThisNewNode    = NULL;
    arraylistnode*       /* var */ AListNode      = NULL;

    AInsertNode =
      corearraytreenodes__createnode
        (AItem);
    ThisInsertNode =
      (arraytreenodeheader*) corememory__share
        (AInsertNode);

    ThisParentNode = (arraytreeheader*) AParentNode;

    AListNode =
      corearraylists__insertat
        (ThisParentNode->Nodes, AIndex, AInsertNode);

    ThisInsertNode->List =
      (arraytreenodeheader*) corememory__share
        (ThisParentNode->Nodes);
  
    ThisInsertNode->Tree =
      (arraytreeheader*) corememory__share
        (ThisParentNode->Tree);

    ThisInsertNode->ParentTreeNode =
      (arraytreeheader*) corememory__share
        (ThisParentNode);
 
    ThisInsertNode->ParentListNode =
      corememory__share(AListNode);

    *ANewNode =
      corememory__transfer
        (&AInsertNode);
  } // if

  // ---
  return Result;
} // func

arraytreenode* /* func */ corearraytreenodes__insertfirst
  (/* inout */ arraytreenode* /* param */ AParentNode,
   /* in */    const pointer* /* param */ AItem)
{
  arraytreenode* /* var */ Result = NULL;
  // ---

  /* discard */ corearraytrees__tryinsertfirst
    (AParentNode, AItem, &Result);
     
  // ---
  return Result;
} // func

arraytreenode* /* func */ corearraytreenodes__insertlast
  (/* inout */ arraytreenode* /* param */ AParentNode,
   /* in */    const pointer* /* param */ AItem)
{
  arraytreenode* /* var */ Result = NULL;
  // ---

  /* discard */ corearraytrees__tryinsertlast
    (AParentNode, AItem, &Result);
     
  // ---
  return Result;
} // func

arraytreenode* /* func */ corearraytreenodes__insertat
  (/* inout */ arraytreenode* /* param */ AParentNode,
   /* in */    const index_t  /* param */ AIndex,
   /* in */    const pointer* /* param */ AItem)
{
  arraytreenode* /* var */ Result = NULL;
  // ---

  /* discard */ corearraytrees__tryinsertlast
    (AParentNode, AIndex, AItem, &Result);
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ corearraytreenodes__tryextractfirst
  (/* inout */ arraytreenode* /* param */ AParentNode,
   /* out */   pointer*       /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (ANode != NULL);
  if (Result)
  {



  } // if

  // ---
  return Result;
} // func

bool /* func */ corearraytreenodes__tryextractlast
  (/* inout */ arraytreenode* /* param */ AParentNode,
   /* out */   pointer*       /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (ANode != NULL);
  if (Result)
  {



  } // if

  // ---
  return Result;
} // func
   
bool /* func */ corearraytreenodes__tryextractat
  (/* inout */ arraytreenode* /* param */ AParentNode,
   /* in */    const index_t  /* param */ AIndex,
   /* out */   pointer*       /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (ANode != NULL);
  if (Result)
  {



  } // if

  // ---
  return Result;
} // func

bool /* func */ corearraytreenodes__tryremovefirst
  (/* inout */ arraytreenode* /* param */ AParentNode)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (ANode != NULL);
  if (Result)
  {



  } // if

  // ---
  return Result;
} // func
   
bool /* func */ corearraytreenodes__tryremovelast
  (/* inout */ arraytreenode* /* param */ AParentNode)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (ANode != NULL);
  if (Result)
  {



  } // if

  // ---
  return Result;
} // func

pointer* /* func */ corearraytreenodes__extractfirst
  (/* inout */ arraytreenode* /* param */ AParentNode)

pointer* /* func */ corearraytreenodes__extractlast
  (/* inout */ arraytreenode* /* param */ AParentNode)

pointer* /* func */ corearraytreenodes__extractat
  (/* inout */ arraytreenode* /* param */ AParentNode,
   /* in */    const index_t  /* param */ AIndex)
   
void /* func */ corearraytreenodes__removefirst
  (/* inout */ arraytreenode* /* param */ AParentNode)

void /* func */ corearraytreenodes__removelast
  (/* inout */ arraytreenode* /* param */ AParentNode)

// ------------------

bool /* func */ corearraytreenodes__tryexchangeat
  (/* inout */ /* restricted */ arraytreenode* /* param */ AParentNode,
   /* in */    /* restricted */ const index_t  /* param */ AFirstIndex,
   /* in */    /* restricted */ const index_t  /* param */ ASecondIndex)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (ANode != NULL);
  if (Result)
  {



  } // if

  // ---
  return Result;
} // func

void /* func */ corearraytreenodes__exchangeat
  (/* inout */ /* restricted */ arraytreenode* /* param */ ANode,
   /* in */    /* restricted */ const index_t  /* param */ AFirstIndex,
   /* in */    /* restricted */ const index_t  /* param */ ASecondIndex)
   
 // ...
 
// ------------------


 // ...


/* friend */ void /* func */ corearraytreenode__link
  (/* inout */ arraytreenode* /* param */ A,
   /* inout */ arraytreenode* /* param */ B)
{
  // ...
} // func

/* friend */ void /* func */ corearraytreenode__moveforward
  (/* inout */ arraytreenode** /* param */ AListNode)
{
  // ...
} // func

/* friend */ void /* func */ corearraytreenode__movebackward
  (/* inout */ arraytreenode** /* param */ AListNode)
{
  // ...
} // func


// ------------------

/* override */ const ansinullstring* /* func */ corearraytrees__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "corearraytrees";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ corearraytrees__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing()

  // ---
  return Result;
} // func

/* override */ int /* func */ corearraytrees__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing()

  // ---
  return Result;
} // func

// ------------------

 
 // ...

// } // namespace corearraytreenodes