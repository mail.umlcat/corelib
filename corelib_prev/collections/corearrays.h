/** Module: "corecollarrays.h"
 ** Descr.: "Dynamically allocated arrays library."
 **/
 
// namespace corecollarrays {
 
// ------------------
 
#ifndef COREARRAYS__H
#define COREARRAYS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

// opaque type
typedef
  pointer* /* as */ array;
typedef
  pointer* /* as */ corecollarrays__array;

struct matrixheader
{
  // pointer to first item
  pointer* /* var */ ItemsFirst;

  // pointer to last item
  pointer* /* var */ ItemsLast;

  // only applies when items are same type
  // "unknowntype" value means different type
  typecode /* var */ ItemsType;

  // applies instead of "typecode"
  // "0" means not used
  size_t   /* var */ ItemsSize;

  /* readonly */
  count_t  /* var */ DimsCount;

  // restricts how many items can be stored
  count_t  /* var */ ItemsMaxCount;

  // indicates iterator related objects
  count_t  /* var */ IteratorCount;
  
  equality /* var */ MatchFunc;
  
  // ...
} ;

// ------------------

//pointer* /* func */ corecollarrays__malloc
//  (/* in */ size_t /* param */ ASize);
  
//void /* func */ corecollarrays__dealloc
//  (/* out */ pointer** /* param */ ADest,
//   /* in */  size_t    /* param */ ASize);

 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ corecollarrays__modulename
  ( noparams );

/* override */ int /* func */ corecollarrays__setup
  ( noparams );

/* override */ int /* func */ corecollarrays__setoff
  ( noparams );

// ------------------

#endif // COREARRAYS__H

// } // namespace corecollarrays