/** Module: "corearraytrees.c"
 ** Descr.: "predefined library"
 **/
 
// namespace corearraytrees {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"
#include "corearraytreenodes.h"

// ------------------

#include "corearraytrees.h"

// ------------------

arraytree* /* func */ corearraytrees__createtree
  ( noparams )
{
  arraytree* /* var */ Result = NULL;
  // ---
     
  arraytreeheader*     /* var */ ThisTree = NULL;
 
  ThisTree =
    corememory__clearallocate
      (sizeof(arraytreeheader));
      
  ThisTree->RootNode =
    NULL;
 
  ThisTree->ItemsType   = unknowntype;

  ThisTree->ItemsSize   = 0;
  
  ThisTree->ItemsCurrentCount = 0;
  ThisTree->ItemsMaxCount = 0;

  ThisTree->IteratorCount     = 0;

  ThisTree->MatchFunc     = NULL;

  Result =
    corememory__transfer(&ThisTree);

  // ---
  return Result;
} // func

arraytree* /* func */ corearraytrees__createtreebytype
  (/* in */ typecode_t* /* param */ AItemType)
{
  arraylist* /* var */ Result = NULL;
  // ---
     
  arraytreeheader*     /* var */ ThisTree = NULL;
 
  ThisTree =
    corememory__clearallocate
      (sizeof(arraytreeheader));
      
  ThisTree->RootNode =
    NULL;
 
  ThisList->ItemsType   = AItemType;
  ThisList->ItemsSize   =
    corememory__typetosize(AItemType);
   
  ThisTree->ItemsCurrentCount = 0;
  ThisTree->ItemsMaxCount = 0;

  ThisTree->IteratorCount     = 0;

  ThisTree->MatchFunc     = NULL;

  Result =
    corememory__transfer(&ThisTree);

  // ---
  return Result;
} // func

arraytree* /* func */ corearraytrees__createtreebysize
  (/* in */ size_t* /* param */ AItemSize)
{
  arraylist* /* var */ Result = NULL;
  // ---
     
  arraytreeheader*     /* var */ ThisTree = NULL;
 
  ThisTree =
    corememory__clearallocate
      (sizeof(arraytreeheader));
      
  ThisTree->RootNode =
    NULL;
 
  ThisList->ItemsType   = unknowntype;
  ThisList->ItemsSize   =
    AKeySize;
  
  ThisTree->ItemsCurrentCount = 0;
  ThisTree->ItemsMaxCount = 0;

  ThisTree->IteratorCount     = 0;

  ThisTree->MatchFunc     = NULL;

  ThisTree->ItemsCurrentIndex = 0;

  Result =
    corememory__transfer(&ThisTree);

  // ---
  return Result;
} // func

arraytree* /* func */ corearraytrees__createatreerestrictcount
  (/* in */ count_t* /* param */ ACount)
{
  arraytree* /* var */ Result = NULL;
  // ---
     
  arraytreeheader*     /* var */ ThisTree = NULL;
 
  ThisTree =
    corememory__clearallocate
      (sizeof(arraytreeheader));
      
  ThisTree->RootNode =
    NULL;
 
  ThisList->ItemsType   = unknowntype;
  ThisList->ItemsSize   = 0;
  
  ThisList->ItemsCurrentCount = 0;
  ThisList->ItemsMaxCount =
    AMaxCount;

  ThisTree->MatchFunc     = NULL;

  ThisTree->ItemsCurrentIndex = 0;

  Result =
    corememory__transfer(&ThisTree);

  // ---
  return Result;
} // func

void /* func */ corearraytrees__droptree
  (/* inout */ const arraytree** /* param */ ATree)
{
  arraytree* /* var */ Result = NULL;
  // ---
     
  arraytreeheader*     /* var */ ThisTree = NULL;

  if (ATree != NULL)
  {
    ThisTree = (arraytreeheader*) ATree;

    Result =
      ThisTree->RootNode;  	
  } // if

  // ---
  return Result;
} // func

// ------------------

void /* func */ corearraytrees__assignequalityfunc
  (/* inout */ arraytree* /* param */ ATree,
   /* in */    equality   /* param */ AMatchFunc)
{
  bool CanContinue = NULL;

  CanContinue = 
    (ATree != NULL);
  if (CanContinue)
  {
    arraytreeheader* /* var */ ThisTree =  NULL;

    ThisTree =
      (arraytreeheader*) ATree;
    
    ThisTree->MatchFunc =
      AMatchFunc;
  } // if
} // func

// ------------------

arraytreenode* /* func */ corearraytrees__getroot
  (/* in */ const arraytree* /* param */ ATree)
{
  arraytreenodeheader* /* var */ Result = NULL;
  // ---
     
  arraytreeheader*     /* var */ ThisTree = NULL;

  if (ATree != NULL)
  {
    ThisTree = (arraytreeheader*) ATree;

    Result =
      ThisTree->RootNode;  	
  } // if

  // ---
  return Result;
} // func

bool /* func */ corearraytrees__hasroot
  (/* in */ const arraytree* /* param */ ATree)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (AList != NULL);
  if (Result)
  {
    arraytreeheader* /* var */ ThisTree = NULL;
    
    ThisTree = (arraytreeheader*) ATree;

    Result =
      (ThisTree->RootNode != NULL);
  } // if

  // ---
  return Result;
} // func

// ------------------

bool /* func */ corearraytrees__isempty
  (/* in */ const arraytree* /* param */ ATree)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (ATree != NULL);
  if (Result)
  {
    arraytreeheader* /* var */ ThisTree = NULL;

    ThisTree =
      (arraytreeheader*) ATree;

    Result =
      (ThisTree->RootNode != NULL) &&
      (ThisTree->ItemsCurrentCount > 0);
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corearraytrees__isfull
  (/* in */ const arraytree* /* param */ ATree)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (ATree != NULL);
  if (Result)
  {
    arraytreeheader* /* var */ ThisTree = NULL;
    
    ThisTree = (arraytreeheader*) ATree;

    Result =
      (ThisTree->ItemsMaxCount > 0) &&
      (ThisTree->ItemsCurrentCount == ThisTree->ItemsMaxCount));
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corearraytrees__hasitems
  (/* in */ const arraytree* /* param */ ATree)
{
  bool /* var */ Result = false;
  // ---
     
  Result = !(corearraytrees__isempty(ATree));

  // ---
  return Result;
} // func

// ------------------

void /* func */ corearraytrees__insertroot
  (/* inout */ arraytree* /* param */ ATree,
   /* in */    pointer*   /* param */ AItem)
{
  bool /* var */ CanContinue = false;
  
  CanContinue =
    (ATree != NULL);
  if (CanContinue)
  {
    arraytreeheader* /* var */ ThisTree = NULL;
    
    ThisTree = (arraytreeheader*) ATree;

	ThisTree->RootNode =
	  
	
    // ...
  } // if

} // func

pointer* /* func */ corearraytrees__droproot
  (/* in */ const arraytree* /* param */ ATree)
{
  pointer* /* var */ Result = NULL;
  // ---
     




  // ---
  return Result;
} // func

// ------------------

void /* func */ corearraytrees__clear
  (/* inout */ ansinullstring* /* param */ ADest)
{
  // ...
} // func

 // ...

// ------------------

/* override */ const ansinullstring* /* func */ corearraytrees__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "corearraytrees";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ corearraytrees__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corearraytrees__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace corearraytrees