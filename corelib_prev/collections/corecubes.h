/** Module: "corecubes.h"
 ** Descr.: "..."
 **/
 
// namespace corecubes {
 
// ------------------
 
#ifndef CORECUBES__H
#define CORECUBES__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

#include "coreiterators.h"

// ------------------

// opaque type
typedef
  pointer* /* as */ cube;
typedef
  pointer* /* as */ corecubes__cube;

struct cubeheader
{
  // pointer to first item
  pointer* /* var */ ItemsFirst;

  // pointer to last item
  pointer* /* var */ ItemsLast;

  // only applies when items are same type
  // "unknowntype" value means different type
  typecode /* var */ ItemsType;

  // applies instead of "typecode"
  // "0" means not used
  size_t   /* var */ ItemsSize;

  // for a cube is always "3"
  /* readonly */
  count_t  /* var */ DimsCount;

  // restricts how many items can be stored
  count_t  /* var */ ItemsMaxCount;

  // indicates iterator related objects
  count_t   /* var */ IteratorCount;

  // restricts how many rows can be stored
  count_t  /* var */ RowsMaxCount;

  // restricts how many columns can be stored
  count_t  /* var */ ColsMaxCount;

  // restricts how many columns can be stored
  count_t  /* var */ FloorsMaxCount;
  
  equality /* var */ MatchFunc;
  
  // ...
} ;
  
typedef
  cube       /* as */ cube_t;
typedef
  cube*      /* as */ cube_p;

typedef
  cube_t     /* as */ corecubes_cube_t;
typedef
  cube_p     /* as */ corecubes_cube_p;

// ------------------




// ------------------

count_t /* func */ corecubes__getmaxcount
  (/* in */ const cube* /* param */ ACube);

count_t /* func */ corecubes__getdimcount
  (/* in */ const cube* /* param */ ACube);

// ------------------

//pointer* /* func */ corecubes__malloc
//  (/* in */ size_t /* param */ ASize);
  
//void /* func */ corecubes__dealloc
//  (/* out */ pointer** /* param */ ADest,
//   /* in */  size_t    /* param */ ASize);

 // ...
 
// ------------------

/* override */ int /* func */ corecubes__setup
  ( noparams );

/* override */ int /* func */ corecubes__setoff
  ( noparams );

// ------------------

#endif // CORECUBES__H

// } // namespace corecubes