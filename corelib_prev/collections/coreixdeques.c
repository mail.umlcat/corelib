/** Module: "coreixdeques.c"
 ** Descr.: "Indexed Double Linked Deque Items."
 **/

// namespace coreixdeques {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"
#include "coreixdequenodes.h"

// ------------------
 
#include "coreixdeques.h"

// ------------------

ixdeque* /* func */ coreixdeques__createlist
  ( noparams )
{
  ixdeque* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ixdeque* /* func */ coreixdeques__createlistbytype
  (/* in */ typecode_t* /* param */ ADataType)
{
  ixdeque* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

ixdeque* /* func */ coreixdeques__createlistrestrictcount
  (/* in */ count_t* /* param */ ACount)
{
  ixdeque* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func
 
ixdeque* /* func */ coreixdeques__createlistbysize
  (/* in */ size_t* /* param */ ADataSize)
{
  ixdeque* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreixdeques__dropixdeque
  (/* out */ ixdeque** /* param */ AList);
{
  coresystem__nothing();
} // func

// ------------------

bool /* func */ coreixdeques__underiteration
  (/* in */ ixdeque* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
  
  if (AList != NULL)
  {
  	Result = (AList->IteratorCount > 0);
  }
     
  // ---
  return Result;
} // func

ixdequeiterator* /* func */ coreixdeques__createiteratorforward
  (/* in */ ixdeque* /* param */ AList)
{
  ixdequeiterator* /* var */ Result = NULL;
  // ---
     
  if (AList != NULL)
  {
  	Result = (AList->IteratorCount > 0);
  }
  
  // ---
  return Result;
} // func

ixdequeiterator* /* func */ coreixdeques__createiteratorbackward
  (/* in */ ixdeque* /* param */ AList)
{
  ixdequeiterator* /* var */ Result = NULL;
  // ---
     
  Result = (AList != NULL);
  if (Result)
  {
     coresystem__nothing();
  } // if
 
  // ---
  return Result;
} // func

void /* func */ coreixdeques__dropiterator
  (/* out */ ixdequeiterator** /* param */ AIterator)
{
  Result = (AIterator != NULL);
  if (Result)
  {
    coresystem__nothing();
  } // if
} // func

// ------------------

bool /* func */ coreixdeques__isdone
  (/* in */ const ixdequeiterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    coresystem__nothing();
  } // if

  // ---
  return Result;
} // func

bool /* func */ coreixdeques__trymovenext
  (/* inout */ ixdequeiterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (AIterator != NULL);
  if (Result)
  {
    ixdequeiteratorheader** /* var */ ThisIterator = NULL;
    ThisIterator = (ixdequeiteratorheader**) AIterator;

    Result =
      (ThisIterator->CurrentItem == ThisIterator->BottomMarker);
 
      if (Result)
      {
        coreixdequenodes__trymovebackward(&(ThisIterator->CurrentItem));      
      }
  } // if
     
  // ---
  return Result;
} // func

void /* func */ coreixdeques__movenext
  (/* inout */ ixdequeiterator* /* param */ AIterator)
{
  Result = (AIterator != NULL);
  if (Result)
  {
    coresystem__nothing();
  } // if
} // func

bool /* func */ coreixdeques__trygetitem
  (/* inout */ ixdequeiterator* /* param */ AIterator
   /* out */   pointer**      /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      ixdequeiteratorheader* /* var */ ThisIterator = NULL;
      ixdequenodeheader*     /* var */ ThisNode = NULL;

      ThisIterator = (ixdequeiteratorheader*) AIterator;
      ThisNode =
        (ixdequenodeheader*) ThisIterator->CurrentNode;

      corememory__safecopy
        (AItem, ThisNode, ThisIterator->ItemsSize);
    } // if
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coreixdeques__trysetitem
  (/* inout */ ixdequeiterator* /* param */ AIterator
   /* in */ const  pointer*   /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      ixdequeiteratorheader* /* var */ ThisIterator = NULL;
      ixdequenodeheader*     /* var */ ThisNode = NULL;

      ThisIterator = (ixdequeiteratorheader*) AIterator;
      ThisNode =
        (ixdequenodeheader*) ThisIterator->CurrentNode;

      corememory__safecopy
        (ThisNode, AItem, ThisIterator->ItemsSize);
    } // if
  } // if
     
  // ---
  return Result;
} // func

void /* func */ coreixdeques__getitem
  (/* inout */ ixdequeiterator* /* param */ AIterator
   /* out */   pointer*       /* param */ AItem)
{
  /* discard */ coreixdeques__trygetitem
    (AIterator, AItem);
} // func

void /* func */ coreixdeques__setitem
  (/* inout */ ixdequeiterator* /* param */ AIterator
   /* in */ const  pointer*   /* param */ AItem)
{
  /* discard */ coreixdeques__trygetitem
    (AIterator, AItem);
} // func

// ------------------

count_t /* func */ coreixdeques__getcurrentcount
  (/* in */ const ixdeque* /* param */ AList)
{
  count_t /* var */ Result = 0;
  // ---
     
  if (AList != NULL)
  {
    ixdequeheader* /* var */ ThisList = NULL;
    ThisList = (ixdequeheader*) AList;
    Result = ThisList->ItemsCurrentCount;
  } // if
  
  // ---
  return Result;
} // func

count_t /* func */ coreixdeques__getmaxcount
  (/* in */ const ixdeque* /* param */ AList)
{
  count_t /* var */ Result = 0;
  // ---
     
  if (AList != NULL)
  {
    ixdequeheader* /* var */ ThisList = NULL;
    ThisList = (ixdequeheader*) AList;
    Result = ThisList->ItemsMaxCount;
  } // if
  
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreixdeques__tryclear
  (/* inout */ ixdeque* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
     
  if (AList != NULL)
  {
  	Result = (AList->IteratorCount > 0);
  }
   
  // ---
  return Result;
} // func

bool /* func */ coreixdeques__isempty
  (/* in */ const ixdeque* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
     
  if (AList != NULL)
  {
  	Result = (AList->IteratorCount > 0);
  }
  
  // ---
  return Result;
} // func

bool /* func */ coreixdeques__hasitems
  (/* in */ const ixdeque* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
     
  Result = !(coreixdeques__isempty(AList));
     
  // ---
  return Result;
} // func

void /* func */ coreixdeques__clear
  (/* inout */ ixdeque* /* param */ AList)
{
  if (AList != NULL)
  {
  	Result = (AList->IteratorCount > 0);
  }
} // func

// ------------------

/* override */ const ansinullstring* /* func */ coreixdeques__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "coreixdeques";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coreixdeques__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreixdeques__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

 
 // ...

// } // namespace coreixdeques