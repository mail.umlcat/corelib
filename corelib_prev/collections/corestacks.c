/** Module: "corestacks.c"
 ** Descr.: "Last In, First Out, Collection."
 **/
 
// namespace corestacks {
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"
#include "corestacknodes.h"

// ------------------
 
#include "corestacks.h"
 
// ------------------

/* friend */ bool /* func */ corestacks__sameitemsize
  (/* in */ stack* /* param */ AFirstStack,
  (/* in */ stack* /* param */ ASecondStack)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AStack != NULL);
  if (Result)
  {
    stackheader* /* var */ ThisFirstStack = NULL;
    stackheader* /* var */ ThisSecondStack = NULL;
      
    ThisFirstStack  = (stackheader*) AFirstStack;
    ThisSecondStack = (stackheader*) ASecondStack;
    
    Result =
      (ThisFirstStack->ItemsSize == ThisSecondStack->ItemsSize);
  } // if

  // ---
  return Result;
} // func

/* friend */ bool /* func */ corestacks__sameitemtype
  (/* in */ stack* /* param */ AFirstStack,
  (/* in */ stack* /* param */ ASecondStack)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AStack != NULL);
  if (Result)
  {
    stackheader* /* var */ ThisFirstStack = NULL;
    stackheader* /* var */ ThisSecondStack = NULL;
      
    ThisFirstStack  = (stackheader*) AFirstStack;
    ThisSecondStack = (stackheader*) ASecondStack;
    
    Result =
      (ThisFirstStack->ItemsType == ThisSecondStack->ItemsType);
  } // if

  // ---
  return Result;
} // func

// ------------------

stack* /* func */ corestacks__createstack
  ( noparams )
{
  stack* /* var */ Result = NULL;
  // ---

  Result =
    corememory__clearallocate
      (sizeof(stackheader));

  stackheader* /* var */ ThisStack = NULL;
  size_t       /* var */ AItemSize;
  typecode_t   /* var */ AItemType;

  ThisStack =
    (stackheader*) corememory__share
      (Result);

  AItemSize = 0;
  AItemType = unknowntype;
  
  ThisStack->ItemsType =
    AItemType;

  ThisStack->ItemsSize =
    AItemSize;

  ThisStack->ItemsMaxCount =
    0;
    
  ThisStack->ItemsCurrentCount =
    0;

  ThisStack->IteratorCount =
    0;
 
  // add markers
  ThisStack->TopMarker =
    corestacknodes__createnode(NULL);
  ThisStack->BottomMarker =
    corestacknodes__createnode(NULL);
  corestacknodes__link
    (ThisStack->TopMarker, ThisStack->BottomMarker);

  // ---
  return Result;
} // func

stack* /* func */ corestacks__createstackbytype
  (/* in */ typecode_t /* param */ ADataType)
{
  stack* /* var */ Result = NULL;
  // ---
     
  Result =
    corememory__clearallocate
      (sizeof(stackheader));

  stackheader* /* var */ ThisStack = NULL;
  size_t       /* var */ AItemSize;

  ThisStack =
    (stackheader*) corememory__share
      (Result);
  
  AItemSize =
    corememory__typetosize(AItemType);
  
  ThisStack->ItemsType =
    AItemType;

  ThisStack->ItemsSize =
    AItemSize;

  ThisStack->ItemsMaxCount =
    0;
    
  ThisStack->ItemsCurrentCount =
    0;

  ThisStack->IteratorCount =
    0;
 
  // add markers
  ThisStack->TopMarker =
    corestacknodes__createnode(NULL);
  ThisStack->BottomMarker =
    corestacknodes__createnode(NULL);
  corestacknodes__link
    (ThisStack->TopMarker, ThisStack->BottomMarker);
     
  // ---
  return Result;
} // func

stack* /* func */ corestacks__createstackrestrictcount
  (/* in */ count_t /* param */ AMaxCount)
{
  stack* /* var */ Result = NULL;
  // ---

  Result =
    corememory__clearallocate
      (sizeof(stackheader));

  stackheader* /* var */ ThisStack = NULL;
  size_t       /* var */ AItemSize;

  ThisStack =
    (stackheader*) corememory__share
      (Result);
  
  ThisStack->ItemsType =
    unknowntype;

  ThisStack->ItemsSize = 0;

  ThisStack->ItemsMaxCount =
    AMaxCount;
  
  ThisStack->ItemsCurrentCount =
    0;

  ThisStack->IteratorCount =
    0;
 
  // add markers
  ThisStack->TopMarker =
    corestacknodes__createnode(NULL);
  ThisStack->BottomMarker =
    corestacknodes__createnode(NULL);
  corestacknodes__link
    (ThisStack->TopMarker, ThisStack->BottomMarker);
     
  // ---
  return Result;
} // func
 
stack* /* func */ corestacks__createstackbysize
  (/* in */ size_t /* param */ AItemSize)
{
  stack* /* var */ Result = NULL;
  // ---

  Result =
    corememory__clearallocate
      (sizeof(stackheader));

  stackheader* /* var */ ThisStack = NULL;
  size_t       /* var */ AItemSize;

  ThisStack =
    (stackheader*) corememory__share
      (Result);
  
  ThisStack->ItemsType =
    unknowntype;

  ThisStack->ItemsSize =
    AItemSize;

  ThisStack->ItemsMaxCount =
    AMaxCount;
  
  ThisStack->ItemsCurrentCount =
    0;

  ThisStack->IteratorCount =
    0;
 
  // add markers
  ThisStack->TopMarker =
    corestacknodes__createnode(NULL);
  ThisStack->BottomMarker =
    corestacknodes__createnode(NULL);
  corestacknodes__link
    (ThisStack->TopMarker, ThisStack->BottomMarker);
     
  // ---
  return Result;
} // func

void /* func */ corestacks__dropstack
  (/* out */ stack** /* param */ AStack)
{
  Result = (AStack != NULL);
  if (Result)
  {
    stackheader* /* var */ ThisStack = NULL;
    ThisStack = (stackheader*) *AStack;

    corestacks__clear(AStack);

    /* discard */ corestacknodes__dropnode
      (&(ThisStack->BottomMarker));

    /* discard */ corestacknodes__dropnode
      (&(ThisStack->TopMarker));
 
    corememory__cleardeallocate
     (*AStack, sizeof(stackheader);
  } // if
} // func

// ------------------

bool /* func */ corestacks__underiteration
  (/* in */ stack* /* param */ AStack)
{
  bool /* var */ Result = false;
  // ---
     
  if (AStack != NULL)
  {
    stackheader* /* var */ ThisStack = NULL;
    ThisStack = (stackheader*) AStack;

	Result = (ThisStack->IteratorCount > 0);
  } // if
     
  // ---
  return Result;
} // func

stackiterator* /* func */ corestacks__createiterator
  (/* in */ stack* /* param */ AStack)
{
  stackiterator* /* var */ Result = NULL;
  // ---

  if (AStack != NULL)
  {
    stackheader* /* var */ ThisStack =  NULL;
    ThisStack = (stackheader*) AStack;
  	  
    stackiteratorheader* /* var */ AIterator = NULL;

    AIterator =
      corememory__clearallocate
       (sizeof(stackiteratorheader));

    AIterator->Stack =
      AStack;
     
    AIterator->CurrentNode =
      corememory__share
        (ThisStack->TopMarker->Prev);

    AIterator->ItemsCount =
      ThisStack->ItemsMaxCount;

    AIterator->ItemIndex =
      ThisStack->ItemsMaxCount;

    AIterator->Direction =
      listdirections__backward;

    ThisStack->IteratorCount =
      (ThisStack->IteratorCount + 1);

    Result = AIterator;
  } // if

  // ---
  return Result;
} // func

void /* func */ corestacks__dropiterator
  (/* out */ stackiterator** /* param */ AIterator)
{
  Result = (AIterator != NULL);
  if (Result)
  {
    stackiteratorheader** /* var */ ThisIterator = NULL;
    ThisIterator = (stackiteratorheader**) AIterator;

    stackheader** /* var */ ThisStack = NULL;
    
    ThisStack = (stackheader*) ThisIterator->Stack;

    ThisStack->IteratorCount =
      (ThisStack->IteratorCount - 1);
      
    corememory__unshare
      (&(ThisIterator->CurrentNode));

    corememory__cleardeallocate
      (ThisIterator);
      
    *AIterator = NULL;
  } // if
} // func

// ------------------

bool /* func */ corestacks__isdone
  (/* in */ const stackiterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator == NULL);
  if (!Result)
  {
    stackiteratorheader* /* var */ ThisIterator = NULL;

    stackheader* /* var */ ThisIterator = NULL;
    
    ThisIterator = (stackiteratorheader*) AIterator;

    ThisStack = (stackheader*) ThisIterator->Stack;

    Result =
      (ThisIterator->CurrentNode = ThisStack->BottomMarker);
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corestacks__trymovenext
  (/* inout */ stackiterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (AIterator != NULL);
  if (Result)
  {
    stackiteratorheader** /* var */ ThisIterator = NULL;
    ThisIterator = (stackiteratorheader**) AIterator;

    Result =
      (ThisIterator->CurrentNode == ThisIterator->BottomMarker);
 
      if (Result)
      {
        corestacknodes__trymovebackward(&(ThisIterator->CurrentNode));      
      }
  } // if
     
  // ---
  return Result;
} // func

void /* func */ corestacks__movenext
  (/* inout */ stackiterator* /* param */ AIterator)
{
  /* discard */ corestacks__trymovenext(AIterator);
} // func

bool /* func */ corestacks__trygetnode
  (/* inout */ stackiterator* /* param */ AIterator
   /* out */   stacknode*     /* param */ ANode)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      stackheader* /* var */ ThisList = NULL;
      stack*       /* var */ AList  = NULL;

      stackiteratorheader* /* var */ ThisIterator = NULL;
      
      ThisIterator = (stackiteratorheader*) AIterator;
      
      AList    = ThisIterator->MainStack;
      ThisList = (stackheader*) AList;
      
      ANode =
        ThisIterator->CurrentNode;
    } // if
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corestacks__trygetitem
  (/* inout */ stackiterator* /* param */ AIterator
   /* out */   pointer**      /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      stackheader*         /* var */ ThisList = NULL;
      stacknodeheader*     /* var */ ThisNode = NULL;
      stackiteratorheader* /* var */ ThisIterator = NULL;

      ThisIterator = (stackiteratorheader*) AIterator;
      ThisList =
        (stackheader*) ThisIterator->MainStack;
      ThisNode =
        (stacknodeheader*) ThisIterator->CurrentNode;
   
      *AItem =
        ThisNode->Item;
    } // if
  } // if
     
  // ---
  return Result;
} // func
   
bool /* func */ corestacks__tryreaditem
  (/* inout */ stackiterator* /* param */ AIterator
   /* out */   pointer*       /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      stackheader*         /* var */ ThisList = NULL;
      stacknodeheader*     /* var */ ThisNode = NULL;
      stackiteratorheader* /* var */ ThisIterator = NULL;

      ThisIterator = (stackiteratorheader*) AIterator;
      ThisList =
        (stackheader*) ThisIterator->MainStack;
      ThisNode =
        (stacknodeheader*) ThisIterator->CurrentNode;
   	  
      corememory__safecopy
        (AItem, ThisNode->Item, ThisList->ItemsSize);
    } // if
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corestacks__trywriteitem
  (/* inout */ stackiterator* /* param */ AIterator
   /* in */ const  pointer*   /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      stackheader*         /* var */ ThisList = NULL;
      stacknodeheader*     /* var */ ThisNode = NULL;
      stackiteratorheader* /* var */ ThisIterator = NULL;

      ThisIterator = (stackiteratorheader*) AIterator;
      ThisList =
        (stackheader*) ThisIterator->MainStack;
      ThisNode =
        (stacknodeheader*) ThisIterator->CurrentNode;
   	  
      corememory__safecopy
        (ThisNode->Item, AItem, ThisList->ItemsSize);
    } // if
  } // if
     
  // ---
  return Result;
} // func

void /* func */ corestacks__readitem
  (/* inout */ stackiterator* /* param */ AIterator
   /* out */   pointer**      /* param */ AItem)
{
  /* discard */ corestacks__tryreaditem
    (AIterator, AItem);
} // func

void /* func */ corestacks__writeitem
  (/* inout */ stackiterator*  /* param */ AIterator
   /* in */    const  pointer* /* param */ AItem)
{
  /* discard */ corestacks__tryreaditem
    (AIterator, AItem);
} // func

// ------------------

count_t /* func */ corestacks__getcurrentcount
  (/* in */ const stack* /* param */ AStack)
{
  count_t /* var */ Result = 0;
  // ---
     
  if (AStack != NULL)
  {
    stackheader* /* var */ ThisStack = NULL;
    ThisStack = (stackheader*) AStack;
    Result = ThisStack->ItemsCurrentCount;
  } // if
  
  // ---
  return Result;
} // func

count_t /* func */ corestacks__getmaxcount
  (/* in */ const stack* /* param */ AStack)
{
  count_t /* var */ Result = 0;
  // ---
     
  if (AStack != NULL)
  {
    stackheader* /* var */ ThisStack = NULL;
    ThisStack = (stackheader*) AStack;
    Result = ThisStack->ItemsMaxCount;
  } // if
  
  // ---
  return Result;
} // func

// ------------------

bool /* func */ corestacks__tryclear
  (/* inout */ stack* /* param */ AStack)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (AStack != NULL);
  if (Result)
  {
    stackheader*     /* var */ ThisStack = NULL;
    
    stackiterator*   /* var */ AIterator = NULL;
    stackiteratorheader* /* var */ ThisIterator = NULL;
 
    stacknode*       /* var */ AStackNode = NULL;
    stacknodeheader* /* var */ ThisStackNode = NULL;
    
    pointer*         /* var */ AData = NULL;

    ThisStack = (stackheader*) AStack;

    // remove data first
    AIterator =
      corestacks__createiterator(AStack);
    ThisIterator =
      (stackiteratorheader*) AIterator;

    while (!corestacks__isdone(AIterator))
    {
      AStackNode =
        ThisIterator->CurrentNode;
      ThisStackNode = (stacknodeheader*) AStackNode;
      
      AData =
        corememory__transfer(&(ThisStackNode->Data));

      corememory__cleardeallocate
        (AData, ThisStack->ItemsSize);
    
      corestacks__movenext(AIterator);
    } // while

    corestacks__dropiterator(&AStack);

    // remove nodes
    AIterator =
      corestacks__createiterator(AStack);

    while (!corestacks__isdone(AIterator))
    {
      AStackNode =
        ThisIterator->CurrentNode;
      ThisStackNode = (stacknodeheader*) AStackNode;

      corestacks__movenext(AIterator);

      /* discard */ corestacknodes__dropnode
        (&ThisStackNode);
    } // while

    corestacks__dropiterator(&AStack);

    // update other properties states
    corestacknodes__link
      (ThisStack->TopMarker, ThisStack->BottomMarker);

    ThisStack->ItemsMaxCount = 0;
    ThisStack->ItemsCurrentCount = 0;
    ThisStack->IteratorCount = 0;
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corestacks__isempty
  (/* in */ const stack* /* param */ AStack)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (AStack != NULL);
  if (Result)
  {
    stackheader* /* var */ ThisStack = NULL;
    
    ThisStack = (stackheader*) AStack;

    Result =
     (corestacknodes__arelinked(ThisStack->TopMarker, ThisStack->BottomMarker)) ||
      (ThisStack->ItemsCurrentCount == 0));
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corestacks__isfull
  (/* in */ const stack* /* param */ AStack)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (AStack != NULL);
  if (Result)
  {
    stackheader* /* var */ ThisStack = NULL;
    
    ThisStack = (stackheader*) AStack;

    Result =
      (ThisStack->ItemsMaxCount > 0) &&
      (ThisStack->ItemsCurrentCount == ThisStack->ItemsMaxCount));
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corestacks__hasitems
  (/* in */ const stack* /* param */ AStack)
{
  bool /* var */ Result = false;
  // ---

  Result = !(corestacks__isempty(AStack));

  // ---
  return Result;
} // func

void /* func */ corestacks__clear
  (/* inout */ stack* /* param */ AStack)
{
  /* discard */ corestacks__tryclear
    (AStack);
} // func

// ------------------

bool /* func */ corestacks__trypush
  (/* inout */ stack*         /* param */ AStack
   /* in */    const pointer* /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (AStack != NULL) && (AItem != NULL);
  if (Result)
  {
    stackheader*     /* var */ ThisStack = NULL;

    stacknodeheader* /* var */ ThisBeforeNode = NULL;

    stacknode*       /* var */ ABeforeNode = NULL;
    stacknode*       /* var */ AAfterNode  = NULL;
    stacknode*       /* var */ ANewNode    = NULL;

    ANewNode =
      corestacknodes__createnode
        (AItem);

    ThisStack = (stackheader*) AStack;

    ABeforeNode =
      ThisStack->TopMarker;
    ThisBeforeNode =
      (stacknodeheader*) ABeforeNode;

    AAfterNode =
      ThisBeforeNode->Prev;

    corestacknodes__trylink
      (ABeforeNode, ANewNode);
    corestacknodes__trylink
      (ANewNode, AAfterNode);
      
    ThisStack->ItemsCurrentCount =
      (ThisStack->ItemsCurrentCount + 1);
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corestacks__trypop
  (/* inout */ stack*    /* param */ AStack
   /* out */   pointer** /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---

  Result =
    (AStack != NULL) && (AItem != NULL);
  if (Result)
  {
    Result =
      corestacks__hasitems(AStack);
    if (Result)
    {
      stackheader*     /* var */ ThisStack = NULL;

      stacknodeheader* /* var */ ThisBeforeNode = NULL;
      stacknodeheader* /* var */ ThisFreeNode   = NULL;

      stacknode*       /* var */ ABeforeNode = NULL;
      stacknode*       /* var */ AAfterNode  = NULL;
      stacknode*       /* var */ AFreeNode   = NULL;

      ThisStack = (stackheader*) AStack;

      ABeforeNode =
        ThisStack->TopMarker;
      ThisBeforeNode =
        (stacknodeheader*) ABeforeNode;

      AFreeNode =
        ThisBeforeNode->Prev;
      ThisFreeNode =
        (stacknodeheader*) AFreeNode;

      AAfterNode =
        ThisFreeNode->Prev;

      corestacknodes__trylink
        (ABeforeNode, AAfterNode);

      ThisStack->ItemsCurrentCount =
        (ThisStack->ItemsCurrentCount - 1);
    } // if
  } // if
   
  // ---
  return Result;
} // func

bool /* func */ corestacks__tryremove
  (/* inout */ stack* /* param */ AStack)
{
  bool /* var */ Result = false;
  // ---

  pointer*     /* var */ AItem = NULL;
  stackheader* /* var */ ThisStack = NULL;

  Result =
    corestacks__trypop
      (AStack, &AItem);
  if (Result)
  {
    ThisStack = (stackheader*) AStack;
  	
    corememory__deallocate
      (&AItem, ThisStack->ItemsSize);
  } // if

  // ---
  return Result;
} // func

void /* func */ corestacks__push
  (/* inout */ stack*         /* param */ AStack,
   /* in */    const pointer* /* param */ AItem)
{
  /* discard */ corestacks__trypush
    (AStack, AItem);
} // func

void /* func */ corestacks__pop
  (/* inout */ stack**  /* param */ AStack,
   /* out */   pointer* /* param */ AItem)
{
  /* discard */ corestacks__trypop
    (AStack, AItem);
} // func

void /* func */ corestacks__remove
  (/* inout */ stack** /* param */ AStack)
{
  /* discard */ corestacks__tryremove
    (AStack, AItem);
} // func

 
 // ...

// ------------------

/* override */ const ansinullstring* /* func */ corestacks__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "corestacks";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ corestacks__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corestacks__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace corestacks