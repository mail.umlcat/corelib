/** Module: "coredequeuesets.c"
 ** Descr.: "Set implementation using Sets."
 **/

// namespace coredequeuesets {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"
#include "coredequeuenodes.h"

// ------------------
 
#include "coredequeuesets.h"

// ------------------

/* friend */ bool /* func */ coredequesets__sameitemsize
  (/* in */ dequeset* /* param */ A,
  (/* in */ dequeset* /* param */ B)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (ASet != NULL);
  if (Result)
  {
    dequesetheader* /* var */ ThisA = NULL;
    dequesetheader* /* var */ ThisB = NULL;
      
    ThisFirstSet  = (dequesetheader*) AFirstSet;
    ThisSecondSet = (dequesetheader*) ASecondSet;
    
    Result =
      (ThisFirstSet->ItemsSize == ThisSecondSet->ItemsSize);
  } // if

  // ---
  return Result;
} // func

/* friend */ bool /* func */ coredequesets__sameitemtype
  (/* in */ dequeset* /* param */ A,
  (/* in */ dequeset* /* param */ B)
{
  bool /* var */ Result = false;
  // ---
    
  Result = (ASet != NULL);
  if (Result)
  {
    dequesetheader* /* var */ ThisA = NULL;
    dequesetheader* /* var */ ThisB= NULL;
      
    ThisFirstSet  = (dequesetheader*) AFirstSet;
    ThisSecondSet = (dequesetheader*) ASecondSet;
    
    Result =
      (ThisFirstSet->ItemsType == ThisSecondSet->ItemsType);
  } // if

  // ---
  return Result;
} // func

// ------------------

/* friend */ bool /* func */ coredequeuesets__tryunion
  (/* in */  const dequeueset*  /* param */ A,
   /* in */  const dequeueset*  /* param */ B,
   /* out */       dequeueset** /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (A != NULL) &&
    (B != NULL) &&
    (ADest != NULL);
  if (Result)
  {
     Result =
      coredequesets__sameitemsize(A, B);
    if (Result)
    {
      *ADest =
        coredequeuesets__createsetbyschema(A);

      Result =
        coredequesets__tryconcat
          (*ADestSet, A);
      if (Result)
      {
        Result =
          coredequesets__tryconcat
            (*ADestSet, B);
      }
    } // if
  } // if
     
  // ---
  return Result;
} // func

/* friend */ bool /* func */ coredequeuesets__tryintersect
  (/* in */  const dequeueset*  /* param */ A,
   /* in */  const dequeueset*  /* param */ B,
   /* out */       dequeueset** /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (A != NULL) &&
    (B != NULL) &&
    (ADest != NULL);
  if (Result)
  {
     Result =
      coredequesets__sameitemsize(A, B);
    if (Result)
    {
      *ADest =
        coredequeuesets__createsetbyschema(A);

      dequesetheader*         /* var */ ThisSet = NULL;

      pointer*                /* var */ ASourceItem = NULL;
      pointer*                /* var */ ADestItem = NULL;

      bool                /* var */ Found = NULL;

      dequesetiterator*       /* var */ AIterator = NULL;
      dequesetiteratorheader* /* var */ ThisIterator = NULL;
      
      ThisSet = (dequesetheader*) ASet;
    
      AIterator = coredequesets__createiteratorfwd(A);
      ThisIterator =
        (dequesetiteratorheader*) corememory__share(AIterator);
    
      while (coredequesets__isdone(AIterator))
      {
        ASourceItem =
          coredequesets__getcurrentitem(AIterator);
    
        Found =
          coredequesets__ismember
            (B, ASourceItem);
        if (Found)
        {
          ADestItem =
            corememory__duplicate
              (ASourceItem, ThisSet->ItemsSize);

          coredequesets__include
            (*ADestSet, ADestItem);
        }
        
        coredequesets__movenext(AIterator);
      } // while
      
      coredequeuesets__dropiterator
        (&AIterator);
    } // if
  } // if
    
  // ---
  return Result;
} // func

/* friend */ bool /* func */ coredequeuesets__trysubstract
  (/* in */  const dequeueset*  /* param */ A,
   /* in */  const dequeueset*  /* param */ B,
   /* out */       dequeueset** /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (A != NULL) &&
    (B != NULL) &&
    (ADest != NULL);
  if (Result)
  {
     Result =
      coredequesets__sameitemsize(A, B);
    if (Result)
    {
      *ADest =
        coredequeuesets__createsetbyschema(A);

      dequesetheader*         /* var */ ThisSet = NULL;

      pointer*                /* var */ ASourceItem = NULL;
      pointer*                /* var */ ADestItem = NULL;

      bool                /* var */ Found = NULL;

      dequesetiterator*       /* var */ AIterator = NULL;
      dequesetiteratorheader* /* var */ ThisIterator = NULL;
      
      ThisSet = (dequesetheader*) ASet;
    
      AIterator = coredequesets__createiteratorfwd(A);
      ThisIterator =
        (dequesetiteratorheader*) corememory__share(AIterator);
    
      while (coredequesets__isdone(AIterator))
      {
        ASourceItem =
          coredequesets__getcurrentitem(AIterator);
    
        Found =
          coredequesets__ismember
            (B, ASourceItem);
        if (!Found)
        {
          ADestItem =
            corememory__duplicate
              (ASourceItem, ThisSet->ItemsSize);

          coredequesets__include
            (*ADestSet, ADestItem);
        }
        
        coredequesets__movenext(AIterator);
      } // while
      
      coredequeuesets__dropiterator
        (&AIterator);
    } // if
  } // if

  // ---
  return Result;
} // func

/* friend */ bool /* func */ coredequeuesets__trysimmetry
  (/* in */  const dequeueset*  /* param */ A,
   /* in */  const dequeueset*  /* param */ B,
   /* out */       dequeueset** /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (A != NULL) &&
    (B != NULL) &&
    (ADest != NULL);
  if (Result)
  {
     Result =
      coredequesets__sameitemsize(A, B);
    if (Result)
    {
      dequeueset* /* param */ ALeftSet = NULL;
      dequeueset* /* param */ ARightSet = NULL;

      *ADest =
        coredequeuesets__createsetbyschema(A);
  
      ALeftSet =
        coredequeuesets__substract(A, B);
      ARightSet =
        coredequeuesets__substract(B, A);

      coredequeuesets__concat(*ADest, ALeftSet);
      coredequeuesets__concat(*ADest, ARightSet);
   
      coredequeuesets__dropset
        (&ALeftSet);
      coredequeuesets__dropset
        (&ARightSet);
    } // if
  } // if
     
  // ---
  return Result;
} // func

// ------------------

/* friend */ dequeueset* /* func */ coredequeuesets__union
  (/* in */  const dequeueset* /* param */ A,
   /* in */  const dequeueset* /* param */ B);
{
  bool /* var */ Result = false;
  // ---

  /* discard */ coredequeuesets__tryunion
    (A, B, &Result);
     
  // ---
  return Result;
} // func

/* friend */ dequeueset* /* func */ coredequeuesets__intersect
  (/* in */  const dequeueset* /* param */ A,
   /* in */  const dequeueset* /* param */ B);
{
  bool /* var */ Result = false;
  // ---

  /* discard */ coredequeuesets__tryintersect
    (A, B, &Result);
    
  // ---
  return Result;
} // func

/* friend */ dequeueset* /* func */ coredequeuesets__substract
  (/* in */  const dequeueset* /* param */ A,
   /* in */  const dequeueset* /* param */ B);
{
  bool /* var */ Result = false;
  // ---

  /* discard */ coredequeuesets__trysubstract
    (A, B, &Result);
     
  // ---
  return Result;
} // func

/* friend */ dequeueset* /* func */ coredequeuesets__simmetry
  (/* in */  const dequeueset* /* param */ A,
   /* in */  const dequeueset* /* param */ B);
{
  bool /* var */ Result = false;
  // ---

  /* discard */ coredequeuesets__trysimmetry
    (A, B, &Result);
    
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coredequesets__trynodebyitemfwd
  (/* in */  const dequeueset*   /* param */ ASet,
   /* in */  const pointer*      /* param */ AItem,
   /* out */       dequeuenode** /* param */ ANode)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (ASet != NULL) &&
    (AItem != NULL) &&
    (ANode != NULL);
  if (Result)
  {
    *ANode = NULL;
  
    dequesetheader*         /* var */ ThisSet = NULL;
    pointer*                /* var */ ASetItem = NULL;    
    
    dequesetiterator*       /* var */ AIterator = NULL;
    dequesetiteratorheader* /* var */ ThisIterator = NULL;
      
    ThisSet = (dequesetheader*) ASet;
    
    Result =
      (ThisSet->MatchFunc != NULL);
    if (Result)
    {
      AIterator = coredequesets__createiteratorfwd
        (ASet);
      ThisIterator =
        (dequesetiteratorheader*) corememory__share(AIterator);
    
      Found = false;
      while (!Found && coredequesets__isdone(AIterator))
      {
        ASetItem =
          coredequesets__getcurrentitem(AIterator);
    
        Found = 
          ThisSet->MatchFunc(AItem, ASetItem);
    
        if (Found)
        {
          *ANode = coredequesets__getcurrentnode
            (AIterator);
        }
        
        coredequesets__movenext(AIterator);
      } // while
      
      coredequeuesets__dropiterator
        (&AIterator);

      Result = Found;
    } // if
  } // if
     
  // ---
  return Result;
} // func

dequeuenode* /* func */ coredequesets__nodebyitemfwd
  (/* in */ const dequeueset*  /* param */ ASet,
   /* in */ const pointer*     /* param */ AItem)
{
  dequeuenode* /* var */ Result = NULL;
  // ---

  /* discard */ coredequesets__trynodebyitemfwd
    (ASet, &Result);
     
  // ---
  return Result;
} // func

// ------------------

dequeueset* /* func */ coredequeuesets__createset
  ( noparams )
{
  dequeueset* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

dequeueset* /* func */ coredequeuesets__createsetbytype
  (/* in */ typecode_t* /* param */ AItemType)
{
  dequeueset* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

dequeueset* /* func */ coredequeuesets__createsetrestrictcount
  (/* in */ count_t* /* param */ ACount)
{
  dequeueset* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func
 
dequeueset* /* func */ coredequeuesets__createsetbysize
  (/* in */ size_t* /* param */ AItemSize)
{
  dequeueset* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

dequeueset* /* func */ coredequeuesets__createsetbyschema
  (/* in */ const dequeueset* /* param */ ASourceSet)
{
  dequeueset* /* var */ Result = NULL;
  // ---
     
  Result =
    (ASourceSet != NULL);
  if (Result)
  {
    dequenodeheader* /* var */ ATopMarker = NULL;
    dequenodeheader* /* var */ ABottomMarker = NULL;

    dequeuesetheader* /* var */ ThisSourceSet = NULL;
    dequeuesetheader* /* var */ ThisDestSet = NULL;
  
    ThisSourceSet = (dequeuesetheader*) 
      corememory__share(&ASourceSet);
 
    Result =
      corememory__clearallocate(sizeof(dequeuesetheader));
  
    ThisDestSet = (dequeuesetheader*) 
      corememory__share(&Result);

    ATopMarker =
      coredequenodes__createnode(NULL);
    ABottomMarker =
      coredequenodes__createnode(NULL);

    ThisDestSet->TopMarker = (dequenodeheader*)
      corememory__share(&ATopMarker);
    ThisDestSet->BottomMarker = (dequenodeheader*)
      corememory__share(&ABottomMarker);
    coredequenodes__link
      (ATopMarker, ABottomMarker);
  
    ThisDestSet->ItemsType =
      ThisDestSet->ItemsType;
    ThisDestSet->ItemsSize =
      ThisDestSet->ItemsSize;
      
    ThisDestSet->ItemsMaxCount = 0;
    ThisDestSet->ItemsCurrentCount = 0;
    ThisDestSet->IteratorCount = 0;
    
    ThisDestSet->MatchFunc =
      ThisDestSet->MatchFunc;
  } // if
    
  // ---
  return Result;
} // func

dequeueset* /* func */ coredequeuesets__createsetbycopy
  (/* in */ const dequeueset* /* param */ ASourceSet)
{
  dequeueset* /* var */ Result = NULL;
  // ---
     
  Result =
    (ASourceSet != NULL);
  if (Result)
  {
    dequeuesetheader* /* var */ ThisSourceSet = NULL;
    dequeueset* /* var */ ADestSet = NULL;

    dequeuesetiterator* /* var */ AIterator = NULL;
    dequeuesetiteratorheader* /* var */ ThisIterator = NULL;
 
    pointer*          /* var */ ASourceItem = NULL;
    pointer*          /* var */ ADestItem = NULL;

    ThisSourceSet = (dequeuesetheader*) 
      corememory__share(&ASourceSet);
 
    Result =
      coredequeuesets__createsetbyschema(ASourceSet);

    ADestSet = (dequeuesetheader*) 
      corememory__share(&Result);

    AIterator =
      coredequesets__createfwditerator(ASourceSet);
    ThisIterator =
      (dequeiteratorheader*) AIterator;

    while (!coredequesets__isdone(AIterator))
    {
      ASourceItem =
        coredequesets__getcurrentitem(AIterator);
      ADestItem =
        corememory__duplicatecopy
          (&ASourceItem, ThisSourceSet->ItemsSize);

      coredequeuesets__tryinclude
        (ADestSet, ADestItem);

      coredequesets__movenext(AIterator);
    } // while

    coredequedesets__dropiterator(&AIterator);

    Result =
      corememory__share(&ADesSet);
  } // if
    
  // ---
  return Result;
} // func

void /* func */ coredequeuesets__dropset
  (/* out */ dequeueset** /* param */ ASet);
{
  if (ASet != NULL)
  {
    dequeheader*     /* var */ ThisSet = NULL;

    dequenodeheader* /* var */ ThisFirstSetNode = NULL;
    dequenodeheader* /* var */ ThisSecondSetNode = NULL;

    ThisSet =
      (dequeheader*) corememory__share(&ASet);
 
    coredequesets__clear
      (ASet);
 
    corememory__cleardeallocate
      (ASet, sizeof(dequeheader));
  } // if
} // func

// ------------------

bool /* func */ coredequeuesets__underiteration
  (/* in */ dequeueset* /* param */ ASet)
{
  bool /* var */ Result = false;
  // ---
  
  if (ASet != NULL)
  {
    Result = (ASet->IteratorCount > 0);
  }
     
  // ---
  return Result;
} // func

dequeuesetiterator* /* func */ coredequeuesets__createiteratorforward
  (/* in */ dequeueset* /* param */ ASet)
{
  dequeuesetiterator* /* var */ Result = NULL;
  // ---
     
  if (ASet != NULL)
  {
    Result = (ASet->IteratorCount > 0);
  }
  
  // ---
  return Result;
} // func

dequeuesetiterator* /* func */ coredequeuesets__createiteratorbackward
  (/* in */ dequeueset* /* param */ ASet)
{
  dequeuesetiterator* /* var */ Result = NULL;
  // ---
     
  Result = (ASet != NULL);
  if (Result)
  {
     coresystem__nothing();
  } // if
 
  // ---
  return Result;
} // func

void /* func */ coredequeuesets__dropiterator
  (/* out */ dequeuesetiterator** /* param */ AIterator)
{
  Result = (AIterator != NULL);
  if (Result)
  {
    coresystem__nothing();
  } // if
} // func

// ------------------

bool /* func */ coredequeuesets__isdone
  (/* in */ const dequeuesetiterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    coresystem__nothing();
  } // if

  // ---
  return Result;
} // func

bool /* func */ coredequeuesets__trymovenext
  (/* inout */ dequeuesetiterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (AIterator != NULL);
  if (Result)
  {
    dequeuesetiteratorheader** /* var */ ThisIterator = NULL;
    ThisIterator = (dequeuesetiteratorheader**) AIterator;

    Result =
      (ThisIterator->CurrentItem == ThisIterator->BottomMarker);
 
      if (Result)
      {
        coredequeuenodes__trymovebackward(&(ThisIterator->CurrentItem));      
      }
  } // if
     
  // ---
  return Result;
} // func

void /* func */ coredequeuesets__movenext
  (/* inout */ dequeuesetiterator* /* param */ AIterator)
{
  Result = (AIterator != NULL);
  if (Result)
  {
    coresystem__nothing();
  } // if
} // func

bool /* func */ coredequeuesets__trygetitem
  (/* inout */ dequeuesetiterator* /* param */ AIterator
   /* out */   pointer**      /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      dequeuesetiteratorheader* /* var */ ThisIterator = NULL;
      dequeuenodeheader*     /* var */ ThisNode = NULL;

      ThisIterator = (dequeuesetiteratorheader*) AIterator;
      ThisNode =
        (dequeuenodeheader*) ThisIterator->CurrentNode;

      corememory__safecopy
        (AItem, ThisNode, ThisIterator->ItemsSize);
    } // if
  } // if
     
  // ---
  return Result;
} // func

void /* func */ coredequeuesets__getitem
  (/* inout */ dequeuesetiterator* /* param */ AIterator
   /* out */   pointer*       /* param */ AItem)
{
  /* discard */ coredequeuesets__trygetitem
    (AIterator, AItem);
} // func

// ------------------

count_t /* func */ coredequeuesets__getcurrentcount
  (/* in */ const dequeueset* /* param */ ASet)
{
  count_t /* var */ Result = 0;
  // ---
     
  if (ASet != NULL)
  {
    dequeuesetheader* /* var */ ThisSet = NULL;
    ThisSet = (dequeuesetheader*) ASet;
    Result = ThisSet->ItemsCurrentCount;
  } // if
  
  // ---
  return Result;
} // func

count_t /* func */ coredequeuesets__getmaxcount
  (/* in */ const dequeueset* /* param */ ASet)
{
  count_t /* var */ Result = 0;
  // ---
     
  if (ASet != NULL)
  {
    dequeuesetheader* /* var */ ThisSet = NULL;
    ThisSet = (dequeuesetheader*) ASet;
    Result = ThisSet->ItemsMaxCount;
  } // if
  
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coredequeuesets__tryclear
  (/* inout */ dequeueset* /* param */ ASet)
{
  bool /* var */ Result = false;
  // ---

  Result = (ASet != NULL);
  if (Result)
  {
    dequeheader*      /* var */ ThisSet = NULL;

    dequeiterator*    /* var */ AIterator = NULL;
    dequeiteratorheader* /* var */ ThisIterator = NULL;

    dequenode*        /* var */ ASetNode = NULL;
    dequenodeheader*  /* var */ ThisSetNode = NULL;
    
    pointer*          /* var */ AItem = NULL;

    ThisSet =
      (dequeheader*) ASet;

    // remove data first
    AIterator =
      coredequesets__createfwditerator(ASet);
    ThisIterator =
      (dequeiteratorheader*) AIterator;

    while (!coredequesets__isdone(AIterator))
    {
      ASetNode =
        ThisIterator->CurrentNode;
      ThisSetNode = (dequenodeheader*) ASetNode;

      // remove item, leave node
      AItem =
        coredequenodes__extract(ASetNode);
      corememory__cleardeallocate
        (&AItem, ThisSet->ItemsSize);

      coredequesets__movenext(AIterator);
    } // while

    coredequedesets__dropiterator(&ASet);

    // remove nodes
    AIterator =
      coredequesets__createiterator(ASet);

    while (!coredequesets__isdone(AIterator))
    {
      ASetNode =
        ThisIterator->CurrentNode;
      ThisSetNode = (dequenodeheader*) ASetNode;

      coredequesets__movenext(AIterator);

      /* discard */ coredequenodes__dropnode
        (&ThisSetNode);
    } // while

    coredequesets__dropiterator(&ASet);

    // update other properties states
    coredequenodes__link
      (ThisSet->FirstMarker, ThisSet->LastMarker);
      
    ThisSet->ItemsMaxCount = 0;
    ThisSet->ItemsCurrentCount = 0;
    ThisSet->IteratorCount = 0;
  } // if
    
  // ---
  return Result;
} // func

bool /* func */ coredequeuesets__isempty
  (/* in */ const dequeueset* /* param */ ASet)
{
  bool /* var */ Result = false;
  // ---
     
  if (ASet != NULL)
  {
    Result = (ASet->ItemsCount > 0);
  }
  
  // ---
  return Result;
} // func

bool /* func */ coredequeuesets__hasmembers
  (/* in */ const dequeueset* /* param */ ASet)
{
  bool /* var */ Result = false;
  // ---
     
  Result = !(coredequeuesets__isempty(ASet));
     
  // ---
  return Result;
} // func

void /* func */ coredequeuesets__clear
  (/* inout */ dequeueset* /* param */ ASet)
{
  /* discard */ coredequeuesets__tryclear
    (ASet);
} // func

// ------------------

bool /* func */ coredequeuesets__tryismember
  (/* in */    const dequeueset* /* param */ ASet,
   /* in */    const pointer*    /* param */ AItem,
   /* inout */ const bool*       /* param */ AIsMember)
{
  bool /* var */ Result = false;
  // ---

  Result =
    (ASet != NULL) &&
    (AItem != NULL) &&
    (AIsMember != NULL) ;
  if (Result)
  {
    *AIsMember = false;
    
    dequesetheader*         /* var */ ThisSet = NULL;
    dequesetiterator*       /* var */ AIterator = NULL;
    dequesetiteratorheader* /* var */ ThisIterator = NULL;
    bool                    /* var */ Found = false;
    pointer*                /* var */ ASetItem = NULL;

    ThisSet =
      (dequesetheader*) corememory__share(ASet);
      
    Result =
      (ThisSet->MatchFunc);
    if (Result)
    {
      AIterator = coredequesets__createiteratorfwd
        (ASet);
      ThisIterator =
        (dequesetiteratorheader*) corememory__share(AIterator);
    
      Found = false;
      while (!Found && coredequesets__isdone(AIterator))
      {
        ASetItem =
          coredequesets__getcurrentitem(AIterator);
    
        Found = 
          ThisSet->MatchFunc(AItem, ASetItem);
    
        coredequesets__movenext(AIterator);
      } // while
      
      *AResult = Found;
    } // if
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coredequeuesets__ismember
  (/* in */    const dequeueset* /* param */ ASet,
   /* in */    const pointer*    /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
  
  bool                      /* var */ Found = false;
  dequeuesetiterator*       /* var */ AIterator = NULL;
  dequeuesetiteratorheader* /* var */ ThisIterator = NULL;
  dequenode*                /* var */ ANode = NULL;
  dequenodeheader*          /* var */ ANode = NULL;
  dequesetheader*           /* var */ ThisSet = NULL;
     
  Result =
    ((ASet != NULL) && (AItem != NULL));
  if (Result)
  {
    ThisSet = corememory__share
      (ASet);
    
    Result =
      (ThisSet->MatchFunc != NULL);
    if (Result)
    {
      AIterator =
        coredequeueset__createiterator(ASet);
      ThisIterator =
        (dequeuesetiteratorheader*) AIterator;

      while (!Found && !coredequeuesets__isdone(AIterator))
      {
        ANode =
          ThisIterator->CurrentItem;
        ThisNode = (dequeuenodeheader*) ANode;
      
        Found =
          (ThisSet->MatchFunc(AItem, ThisNode->Item));
    
        coredequeuesets__movenext(AIterator);
      } // while

      coredequeuesets__dropiterator(&AIterator);
      
      Result = Found;
    } // if
  } // if
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coredequeuesets__tryconcat
  (/* inout */ dequeueset*       /* param */ ADestSet,
  (/* in */    const dequeueset* /* param */ ASourceSet)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (ASourceSet != NULL) &&
    (ADestSet != NULL);
  if (Result)
  {
    Result =
      coredequesets__sameitemsize(ADestSet, ASourceSet);
    if (Result)
    {
      bool         /* var */ Found = NULL;

      dequesetiterator*       /* var */ AIterator = NULL;
      dequesetiteratorheader* /* var */ ThisIterator = NULL;

      pointer*         /* var */ ASourceItem = NULL;
      pointer*         /* var */ ADestItem = NULL;

      dequesetheader*  /* var */ ThisDestSet = NULL;

      ThisDestSet = (dequesetheader*)
        corememory__share(ADestSet);

      AIterator = coredequesets__createiteratorfwd
        (ASourceSet);
      ThisIterator =
        (dequesetiteratorheader*) corememory__share(AIterator);

      while (coredequesets__isdone(AIterator))
      {
        ASourceItem =
          coredequesets__getcurrentitem(AIterator);

        Found =
          coredequesets__ismember(ASourceSet, ASourceItem);
        if (Found)
        {
          ADestItem =
            corememory_duplicate(ASourceItem, ThisDestSet->ItemsSize);

          coredequesets__include(ADestSet, ADestItem);
        }

        coredequesets__movenext(AIterator);
      } // while

      coredequeuesets__dropiterator
        (&AIterator);
    } // if
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coredequeuesets__tryinclude
  (/* in */    const dequeueset* /* param */ ASet,
   /* in */    const pointer*    /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    ((ASet != NULL) && (AItem != NULL));
  if (Result)
  {
    Result =
      (!coredequesets__ismember(ASet));
    if (Result)
    {
      dequesetheader*  /* var */ ThisSet = NULL;
   	
      dequenode*       /* var */ ABeforeNode = NULL;
      dequenode*       /* var */ AAfterNode = NULL;
      dequenode*       /* var */ AIncludeNode    = NULL;

      dequenodeheader* /* var */ ThisAfterNode = NULL;

      ThisSet = (dequesetheader*) ASet;

      AAfterNode =
        ThisSet->LastMarker;
      ThisAAfterNode =
        (dequenodeheader*) AAfterNode;

      ABeforeNode =
        ThisAAfterNode->Prev;

      AIncludeNode =
        coredequenodes__createnode
          (AItem);
 
      coredequenodes__trylink
        (ABeforeNode, ANewNode);
      coredequenodes__trylink
        (ANewNode, AAfterNode);
      
      ThisSet->ItemsCurrentCount =
        (ThisSet->ItemsCurrentCount + 1);
    } // if
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coredequeuesets__tryexclude
  (/* in */    const dequeueset* /* param */ ASet,
   /* in */    const pointer*    /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    ((ASet != NULL) && (AItem != NULL));
  if (Result)
  {
    dequesetheader*  /* var */ ThisSet = NULL;
   	
    dequenode*       /* var */ ABeforeNode = NULL;
    dequenode*       /* var */ AAfterNode = NULL;
    dequenode*       /* var */ AExcludeNode = NULL;

    dequenodeheader* /* var */ ThisExcludeNode = NULL;
    pointer*         /* var */ AExcludeItem = NULL;

    ThisSet = (dequesetheader*) ASet;
    
    Result = (dequesets__trynodebyitemfwd
      (ASet, AItem, &ThisExcludeNode);
    if (Result)
    {
      ABeforeNode = ThisExcludeNode->Prev;
      AAfterNode  = ThisExcludeNode->Next;
        
      AExcludeItem =
        coredequenodes__dropnode
          (&ThisExcludeNode);
            
      corememory__deallocate
        (&AExcludeItem, ThisSet->ItemsSize);
        
      ThisSet->ItemsCurrentCount =
        (ThisSet->ItemsCurrentCount - 1);
    } // if
  } // if
     
  // ---
  return Result;
} // func

// ------------------

void /* func */ coredequeuesets__include
  (/* in */    const dequeueset* /* param */ ASet,
   /* in */    const pointer*    /* param */ AItem)
{
  /* discard */ coredequeuesets__tryinclude
    (ASet, AItem);
} // func

void /* func */ coredequeuesets__exclude
  (/* in */    const dequeueset* /* param */ ASet,
   /* in */    const pointer*    /* param */ AItem)
{
  /* discard */ coredequeuesets__tryexclude
    (ASet, AItem);
} // func

void /* func */ coredequeuesets__tryconcat
  (/* inout */ dequeueset*       /* param */ ADestSet,
  (/* in */    const dequeueset* /* param */ ASourceSet)
{
  /* discard */ coredequeuesets__tryconcat
    (ASourceSet, ADestSet);
} // func


 // ...
 
// ------------------

 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coredequeuesets__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "coredequeuesets";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coredequeuesets__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coredequeuesets__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

 
 // ...

// } // namespace coredequeuesets