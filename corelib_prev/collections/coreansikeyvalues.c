/** Module: "coreansikeyvalues.c"
 ** Descr.: "String Key, Generic Type Value Pair."
 **/

// namespace coreansikeyvalues {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"
#include "coreansichars.h"
#include "coreansinullstrs.h"

// ------------------
 
#include "coreansikeyvalues.h"
 
// ------------------
 
ansikeyvalue* /* func */ coreansikeyvalues__createansikeyvalue
  ( noparams );
{
  ansikeyvalue /* var */ Result = NULL;
  // ---
     
  Result =
    corememory__createallocate
      (sizeof(ansikeyvalueheader));
  
  // ---
  return Result;
} // func

ansikeyvalue* /* func */ coreansikeyvalues__createansikeyvaluebykey
  (/* in */ pointer* /* param */ AKey)
{
  ansikeyvalue /* var */ Result = NULL;
  // ---
     
  Result =
    corememory__createallocate
      (sizeof(ansikeyvalueheader));
  Result->Key = AKey;
   
  // ---
  return Result;
} // func

ansikeyvalue* /* func */ coreansikeyvalues__createansikeyvaluebyvalue
  (/* in */ pointer* /* param */ AKey
   /* in */ pointer* /* param */ AValue)
{
  ansikeyvalue /* var */ Result = NULL;
  // ---
     
  Result =
    corememory__createallocate
      (sizeof(ansikeyvalueheader));
  Result->Key   = AKey;
  Result->Value = AValue;
  
  // ---
  return Result;
} // func

// ------------------

void /* func */ coreansikeyvalues__dropbysize
 (/* out */ ansikeyvalue** /* param */ AKeyValue,
  /* in */  size_t         /* param */ AKeySize,
  /* in */  size_t         /* param */ AValueSize)
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (AKeyValue != NULL) && (AKeySize > 0) && (AValueSize > 0);
  if (CanContinue)
  {
    ansikeyvalueheader* /* var */ ThisKeyValue = NULL;
  	
    ThisKeyValue = (ansikeyvalueheader*) *AKeyValue;
      
    corememory__cleardeallocate
      (&(ThisKeyValue->Key), AKeySize);
    corememory__cleardeallocate
      (&(ThisKeyValue->Value), AValueSize);
      
    corememory__cleardeallocate
      (AKeyValue, sizeof(ansikeyvalueheader));
  } // if
} // func

void /* func */ coreansikeyvalues__dropextract
 (/* out */ ansikeyvalue** /* param */ AKeyValue)
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (AKeyValue != NULL);
  if (CanContinue)
  {
    ansikeyvalueheader* /* var */ ThisKeyValue = NULL;
  	
    ThisKeyValue = (ansikeyvalueheader*) *AKeyValue;
    *AKey =
      corememory__transfer(&(ThisKeyValue->Key));
    *AValue =
      corememory__transfer(&(ThisKeyValue->Value));

    corememory__cleardeallocate
      (&ThisKeyValue, sizeof(ansikeyvalueheader));
  } // if
} // func

void /* func */ coreansikeyvalues__dropextract
 (/* out */ ansikeyvalue** /* param */ AKeyValue,
  /* out */ pointer**      /* param */ AKey
  /* out */ pointer**      /* param */ AValue)
{
  bool /* var */ CanContinue = false;

  CanContinue =
    (ADest != NULL) && (AKey != NULL) && (AValue != NULL);
  if (CanContinue)
  {
    ansikeyvalueheader* /* var */ ThisKeyValue = NULL;
  	
    ThisKeyValue = (ansikeyvalueheader*) *AKeyValue;
    *AKey =
      corememory__transfer(&(ThisKeyValue->Key));
    *AValue =
      corememory__transfer(&(ThisKeyValue->Value));
      
    corememory__cleardeallocate
      (&ThisKeyValue, sizeof(ansikeyvalueheader));

    *ADestKeyValue = NULL;
  } // if
} // func

// ------------------

pointer* /* func */ coreansikeyvalues__replacekey
  (/* out */ ansikeyvalue* /* param */ AKeyValue,
   /* in */  pointer*      /* param */ ANewKey)
{
  pointer* /* var */ Result = NULL;
  // ---
  
  bool /* var */ CanContinue = FALSE;

  CanContinue =
    (AKeyValue != NULL) &&
    (ANewKey != NULL);
 
  if (CanContinue)
  {
    ansikeyvalueheader* /* var */ ThisKeyValue = NULL;

    ThisKeyValue =
      (ansikeyvalueheader*) corememory__share(AKeyValue);
  	
    Result =
      corememory__transfer(&(ThisKeyValue->Key));

    ThisKeyValue->Key = ANewKey;
  } // if
     
  // ---
  return Result;
} // func

pointer* /* func */ coreansikeyvalues__replacevalue
  (/* out */ ansikeyvalue* /* param */ AKeyValue,
   /* in */  pointer*      /* param */ ANewValue)
{
  pointer* /* var */ Result = NULL;
  // ---
  
  bool /* var */ CanContinue = FALSE;

  CanContinue =
    (AKeyValue != NULL) &&
    (ANewValue != NULL);
 
  if (CanContinue)
  {
    ansikeyvalueheader* /* var */ ThisKeyValue = NULL;

    ThisKeyValue =
      (ansikeyvalueheader*) corememory__share(AKeyValue);
  	
    Result =
      corememory__transfer(&(ThisKeyValue->Value));

    ThisKeyValue->Value = ANewValue;
  } // if
     
  // ---
  return Result;
} // func
 
// ------------------

/* friend */ bool /* func */ coreansikeyvalues__tryexchangevalues
  (/* inout */ /* restricted */ ansikeyvalue* /* param */ A,
   /* inout */ /* restricted */ ansikeyvalue* /* param */ B)
{
  bool /* var */ Result = false;
  // ---
 
  ansikeyvalueheader* /* var */ ThisFirstItem = NULL;
  ansikeyvalueheader* /* var */ ThisSecondItem = NULL;
  
  Result =
    ((A != NULL) && (B != NULL));
  if (Result)
  {
    ThisFirstItem =
      (ansikeyvalueheader*) A;
    ThisSecondItem =
      (ansikeyvalueheader*) B;
      
    corememory__safeexchangeptr
      (&(ThisFirstItem->Value), &(ThisSecondItem->Value));
  } // if

  // ---
  return Result;
} // func

/* friend */ void /* func */ coreansikeyvalues__exchangevalues
  (/* inout */ /* restricted */ ansikeyvalue* /* param */ A,
   /* inout */ /* restricted */ ansikeyvalue* /* param */ B)
{
  /* discard */ coreansikeyvalues__tryexchangevalues
    (A, B);
} // func


 // ...

// ------------------

/* override */ const ansinullstring* /* func */ coreansikeyvalues__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "coreansikeyvalues";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coreansikeyvalues__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreansikeyvalues__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coreansikeyvalues