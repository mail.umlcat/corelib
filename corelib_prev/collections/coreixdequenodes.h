/** Module: "coreixdequenodes.h"
 ** Descr.: "Indexed Double Linked Deque Items."
 **/

// namespace coreixdequenodes {
 
// ------------------
 
#ifndef COREIXDEQUENODENODES__H
#define COREIXDEQUENODENODES__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

typedef
  pointer* /* as */ ixdequenode;

struct ixdequenodeheader
{
  ixdequenode* /* var */ Prev;
  ixdequenode* /* var */ Next;  
  
  pointer*     /* var */ ItemDPtr;

  bool         /* var */ Deleted;
} ;

// ------------------

/* friend */ bool /* func */ coredequenodes__arelinked
  (/* in */ const ixdequenode* /* param */ A,
   /* in */ const ixdequenode* /* param */ B);

/* friend */ bool /* func */ coredequenodes__trylink
  (/* inout */ ixdequenode* /* param */ A,
   /* inout */ ixdequenode* /* param */ B);

/* friend */ bool /* func */ coredequenodes__trymoveforward
  (/* inout */ ixdequenode* /* param */ ADequeNode);

/* friend */ bool /* func */ coredequenodes__trymovebackward
  (/* inout */ ixdequenode* /* param */ ADequeNode);

/* friend */ bool /* func */ coredequenodes__tryexchange
  (/* inout */ ixdequenode* /* param */ A,
   /* inout */ ixdequenode* /* param */ B);

// ------------------

/* friend */ void /* func */ coreixdequenodes__link
  (/* inout */ ixdequenode* /* param */ A,
   /* inout */ ixdequenode* /* param */ B);

/* friend */ void /* func */ coreixdequenodes__moveforward
  (/* inout */ ixdequenode** /* param */ AListNode);

/* friend */ void /* func */ coreixdequenodes__movebackward
  (/* inout */ ixdequenode** /* param */ AListNode);

/* friend */ void /* func */ coreixdequenodes__exchange
  (/* inout */ ixdequenode* /* param */ A,
   /* inout */ ixdequenode* /* param */ B);

// ------------------

ixdequenode* /* func */ coreixdequenodes__createnode
  (/* in */ pointer* /* param */ AItem);

pointer* /* func */ coreixdequenodes__dropnode
  (/* out */ ixdequenode** /* param */ ADequeNode);

// ------------------

bool /* func */ coreixdequenodes__isdeleted
  (/* in */ ixdequenode* /* param */ ADequeNode);

void /* func */ coreixdequenodes__markdeleted
  (/* inout */ ixdequenode*  /* param */ ADequeNode,
   /* in */    const pointer /* param */ AValue);

 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coreixdequenodes__modulename
  ( noparams );

/* override */ int /* func */ coreixdequenodes__setup
  ( noparams );

/* override */ int /* func */ coreixdequenodes__setoff
  ( noparams );

// ------------------

#endif // COREIXDEQUENODENODES__H

// } // namespace coreixdequenodes