/** Module: "corekeyvalues.h"
 ** Descr.: "Generic Type Key Value Pair."
 **/
 
// namespace corekeyvalues {
 
// ------------------
 
#ifndef COREKEYVALUES__H
#define COREKEYVALUES__H
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <float.h"
#include <time.h"
//#include <chrono.h"
#include <stdio.h"
#include <math.h"

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

// opaque type
typedef
  pointer* /* as */ keyvalue;

struct keyvalueheader
{
  pointer* /* var */ Key;
  pointer* /* var */ Value;
} ;


typedef
  keyvalueheader  /* as */ keyvalue_t;
typedef
  keyvalueheader* /* as */ keyvalue_p;

// ------------------

keyvalue* /* func */ corekeyvalues__createkeyvalue
  ( noparams );

keyvalue* /* func */ corekeyvalues__createkeyvaluebykey
  (/* in */ pointer* /* param */ AKey);

keyvalue* /* func */ corekeyvalues__createkeyvaluebyvalue
  (/* in */ pointer* /* param */ AKey
   /* in */ pointer* /* param */ AValue);
 
// ------------------

void /* func */ corekeyvalues__drop
 (/* out */ keyvalue** /* param */ AKeyValue);

void /* func */ corekeyvalues__dropbysize
 (/* out */ keyvalue** /* param */ AKeyValue,
  /* in */  size_t     /* param */ AKeySize,
  /* in */  size_t     /* param */ AValueSize);

void /* func */ corekeyvalues__dropextract
 (/* out */ keyvalue** /* param */ AKeyValue,
  /* out */ pointer**  /* param */ AKey,
  /* out */ pointer**  /* param */ AValue);
 
// ------------------

pointer* /* func */ corekeyvalues__replacekey
  (/* out */ keyvalue* /* param */ AKeyValue,
   /* in */  pointer*  /* param */ ANewKey);

pointer* /* func */ corekeyvalues__replacevalue
  (/* out */ keyvalue* /* param */ AKeyValue,
   /* in */  pointer*  /* param */ ANewValue);
 
// ------------------

/* friend */ bool /* func */ corekeyvalues__tryexchangevalues
  (/* inout */ /* restricted */ keyvalue* /* param */ A,
   /* inout */ /* restricted */ keyvalue* /* param */ B);

/* friend */ void /* func */ corekeyvalues__exchangevalues
  (/* inout */ /* restricted */ keyvalue* /* param */ A,
   /* inout */ /* restricted */ keyvalue* /* param */ B);





 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ corekeyvalues__modulename
  ( noparams );

/* override */ int /* func */ corekeyvalues__setup
  ( noparams );

/* override */ int /* func */ corekeyvalues__setoff
  ( noparams );

// ------------------

#endif // COREKEYVALUES__H

// } // namespace corekeyvalues