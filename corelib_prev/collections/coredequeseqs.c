/** Module: "coredequeseqs.c"
 ** Descr.: "Duplicated items Lists."
 **/
 
// namespace coredequeseqs {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"
#include "coredequeseqnodes.h"

// ------------------
 
#include "coredequeseqs.h"

// ------------------

/* friend */ bool /* func */ coredequeseqs__sameitemsize
  (/* in */ dequeseq* /* param */ A,
  (/* in */ dequeseq* /* param */ B)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (ASet != NULL);
  if (Result)
  {
    dequeseqheader* /* var */ ThisA = NULL;
    dequeseqheader* /* var */ ThisB = NULL;
      
    ThisFirstSet  = (dequeseqheader*) AFirstSet;
    ThisSecondSet = (dequeseqheader*) ASecondSet;
    
    Result =
      (ThisFirstSet->ItemsSize == ThisSecondSet->ItemsSize);
  } // if

  // ---
  return Result;
} // func

/* friend */ bool /* func */ coredequeseqs__sameitemtype
  (/* in */ dequeseq* /* param */ A,
  (/* in */ dequeseq* /* param */ B)
{
  bool /* var */ Result = false;
  // ---
    
  Result = (ASet != NULL);
  if (Result)
  {
    dequeseqheader* /* var */ ThisA = NULL;
    dequeseqheader* /* var */ ThisB= NULL;
      
    ThisFirstSet  = (dequeseqheader*) AFirstSet;
    ThisSecondSet = (dequeseqheader*) ASecondSet;
    
    Result =
      (ThisFirstSet->ItemsType == ThisSecondSet->ItemsType);
  } // if

  // ---
  return Result;
} // func

// ------------------

dequeseq* /* func */ coredequeseqs__createlist
  ( noparams )
{
  dequeseq* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

dequeseq* /* func */ coredequeseqs__createlistbytype
  (/* in */ typecode_t* /* param */ ADataType)
{
  dequeseq* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

dequeseq* /* func */ coredequeseqs__createlistrestrictcount
  (/* in */ count_t* /* param */ ACount)
{
  dequeseq* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func
 
dequeseq* /* func */ coredequeseqs__createlistbysize
  (/* in */ size_t* /* param */ ADataSize)
{
  dequeseq* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coredequeseqs__dropdequeseq
  (/* out */ dequeseq** /* param */ AList);
{
  coresystem__nothing();
} // func

// ------------------

bool /* func */ coredequeseqs__underiteration
  (/* in */ dequeseq* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
  
  if (AList != NULL)
  {
  	Result = (AList->IteratorCount > 0);
  }
     
  // ---
  return Result;
} // func

dequeseqiterator* /* func */ coredequeseqs__createiteratorforward
  (/* in */ dequeseq* /* param */ AList)
{
  dequeseqiterator* /* var */ Result = NULL;
  // ---
     
  if (AList != NULL)
  {
  	Result = (AList->IteratorCount > 0);
  }
  
  // ---
  return Result;
} // func

dequeseqiterator* /* func */ coredequeseqs__createiteratorbackward
  (/* in */ dequeseq* /* param */ AList)
{
  dequeseqiterator* /* var */ Result = NULL;
  // ---
     
  Result = (AList != NULL);
  if (Result)
  {
     coresystem__nothing();
  } // if
 
  // ---
  return Result;
} // func

void /* func */ coredequeseqs__dropiterator
  (/* out */ dequeseqiterator** /* param */ AIterator)
{
  Result = (AIterator != NULL);
  if (Result)
  {
    coresystem__nothing();
  } // if
} // func

// ------------------

bool /* func */ coredequeseqs__isdone
  (/* in */ const dequeseqiterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    coresystem__nothing();
  } // if

  // ---
  return Result;
} // func

bool /* func */ coredequeseqs__trymovenext
  (/* inout */ dequeseqiterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (AIterator != NULL);
  if (Result)
  {
    dequeseqiteratorheader** /* var */ ThisIterator = NULL;
    ThisIterator = (dequeseqiteratorheader**) AIterator;

    Result =
      (ThisIterator->CurrentItem == ThisIterator->BottomMarker);
 
      if (Result)
      {
        coredequeseqnodes__trymovebackward(&(ThisIterator->CurrentItem));      
      }
  } // if
     
  // ---
  return Result;
} // func

void /* func */ coredequeseqs__movenext
  (/* inout */ dequeseqiterator* /* param */ AIterator)
{
  Result = (AIterator != NULL);
  if (Result)
  {
    coresystem__nothing();
  } // if
} // func

bool /* func */ coredequeseqs__trygetitem
  (/* inout */ dequeseqiterator* /* param */ AIterator
   /* out */   pointer**      /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      dequeseqiteratorheader* /* var */ ThisIterator = NULL;
      dequeseqnodeheader*     /* var */ ThisNode = NULL;

      ThisIterator = (dequeseqiteratorheader*) AIterator;
      ThisNode =
        (dequeseqnodeheader*) ThisIterator->CurrentNode;

      corememory__safecopy
        (AItem, ThisNode, ThisIterator->ItemsSize);
    } // if
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coredequeseqs__trysetitem
  (/* inout */ dequeseqiterator* /* param */ AIterator
   /* in */ const  pointer*   /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      dequeseqiteratorheader* /* var */ ThisIterator = NULL;
      dequeseqnodeheader*     /* var */ ThisNode = NULL;

      ThisIterator = (dequeseqiteratorheader*) AIterator;
      ThisNode =
        (dequeseqnodeheader*) ThisIterator->CurrentNode;

      corememory__safecopy
        (ThisNode, AItem, ThisIterator->ItemsSize);
    } // if
  } // if
     
  // ---
  return Result;
} // func

void /* func */ coredequeseqs__getitem
  (/* inout */ dequeseqiterator* /* param */ AIterator
   /* out */   pointer*       /* param */ AItem)
{
  /* discard */ coredequeseqs__trygetitem
    (AIterator, AItem);
} // func

void /* func */ coredequeseqs__setitem
  (/* inout */ dequeseqiterator* /* param */ AIterator
   /* in */ const  pointer*   /* param */ AItem)
{
  /* discard */ coredequeseqs__trygetitem
    (AIterator, AItem);
} // func

// ------------------

count_t /* func */ coredequeseqs__getcurrentcount
  (/* in */ const dequeseq* /* param */ AList)
{
  count_t /* var */ Result = 0;
  // ---
     
  if (AList != NULL)
  {
    dequeseqheader* /* var */ ThisList = NULL;
    ThisList = (dequeseqheader*) AList;
    Result = ThisList->ItemsCurrentCount;
  } // if
  
  // ---
  return Result;
} // func

count_t /* func */ coredequeseqs__getmaxcount
  (/* in */ const dequeseq* /* param */ AList)
{
  count_t /* var */ Result = 0;
  // ---
     
  if (AList != NULL)
  {
    dequeseqheader* /* var */ ThisList = NULL;
    ThisList = (dequeseqheader*) AList;
    Result = ThisList->ItemsMaxCount;
  } // if
  
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coredequeseqs__tryclear
  (/* inout */ dequeseq* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
     
  if (AList != NULL)
  {
  	Result = (AList->ItemsCount > 0);
  }
   
  // ---
  return Result;
} // func

bool /* func */ coredequeseqs__isempty
  (/* in */ const dequeseq* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
     
  if (AList != NULL)
  {
  	Result = (AList->ItemsCount > 0);
  }
  
  // ---
  return Result;
} // func

bool /* func */ coredequeseqs__hasitems
  (/* in */ const dequeseq* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
     
  Result = !(coredequeseqs__isempty(AList));
     
  // ---
  return Result;
} // func

void /* func */ coredequeseqs__clear
  (/* inout */ dequeseq* /* param */ AList)
{
  if (AList != NULL)
  {
  	Result = (AList->IteratorCount > 0);
  }
} // func

// ------------------

/* override */ const ansinullstring* /* func */ coredequeseqs__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "coredequeseqs";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coredequeseqs__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coredequeseqs__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

 
 // ...

// } // namespace coredequeseqs