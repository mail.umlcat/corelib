/** Module: "coredequenodes.h"
 ** Descr.: "Double Linked Deque Items."
 **/

// namespace coredequenodes {
 
// ------------------
 
#ifndef COREDEQUENODES__H
#define COREDEQUENODES__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

typedef
  pointer* /* as */ dequenode;

typedef
  pointer* /* as */ coredequenodes__dequenode;

struct dequenodeheader
{
  dequenode* /* var */ Prev;
  dequenode* /* var */ Next;  
  
  pointer*   /* var */ Item;
} ;

// ------------------

/* friend */ bool /* func */ coredequenodes__arelinked
  (/* in */ const dequenode* /* param */ A,
   /* in */ const dequenode* /* param */ B);

/* friend */ bool /* func */ coredequenodes__trylink
  (/* inout */ dequenode* /* param */ A,
   /* inout */ dequenode* /* param */ B);

/* friend */ bool /* func */ coredequenodes__trymoveforward
  (/* inout */ dequenode* /* param */ ADequeNode);

/* friend */ bool /* func */ coredequenodes__trymovebackward
  (/* inout */ dequenode* /* param */ ADequeNode);

// ------------------

/* friend */ void /* func */ coredequenodes__link
  (/* inout */ dequenode* /* param */ A,
   /* inout */ dequenode* /* param */ B);

/* friend */ void /* func */ coredequenodes__moveforward
  (/* inout */ dequenode* /* param */ ADequeNode);

/* friend */ void /* func */ coredequenodes__movebackward
  (/* inout */ dequenode* /* param */ ADequeNode);

// ------------------

dequenode* /* func */ coredequenodes__createnode
  (/* in */ pointer* /* param */ AItem);

pointer* /* func */ coredequenodes__dropnode
  (/* out */ dequenode** /* param */ ADequeNode);

// ------------------

pointer* /* func */ coredequenodes__extract
  (/* inout */ dequenode* /* param */ ADequeNode);



 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coredequenodes__modulename
  ( noparams );

/* override */ int /* func */ coredequenodes__setup
  ( noparams );

/* override */ int /* func */ coredequenodes__setoff
  ( noparams );

// ------------------
 
#endif // COREDEQUENODES__H

// } // namespace coredequenodes