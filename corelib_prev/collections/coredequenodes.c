/** Module: "coredequenodes.c"
 ** Descr.: "Double Linked Deque Items."
 **/

// namespace coredequenodes {
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coredequenodes.h"
 
// ------------------

/* friend */ bool /* func */ coredequenodes__arelinked
  (/* in */ const dequenode* /* param */ A,
   /* in */ const dequenode* /* param */ B)
{
  bool /* var */ Result = false;
  // ---

  Result = (A != NULL) && (B != NULL);
  if (Result)
  {
      dequenodeheader* /* var */ ThisFirstDequeNode  = NULL;
      dequenodeheader* /* var */ ThisSecondDequeNode = NULL;

      ThisFirstDequeNode  = (dequenodeheader*) A;
      ThisSecondDequeNode = (dequenodeheader*) B;
      
      Result =
        (ThisFirstDequeNode->Next  == ThisSecondDequeNode) ||
        (ThisSecondDequeNode->Prev == ThisFirstDequeNode);
  } // if
    
  // ---
  return Result;
} // func

/* friend */ bool /* func */ coredequenodes__trylink
  (/* inout */ dequenode* /* param */ A,
   /* inout */ dequenode* /* param */ B)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (! ((A == NULL) || (B == NULL)));
  if (Result)
  {
    dequenodeheader* /* var */ ThisFirstNode  = NULL;
    dequenodeheader* /* var */ ThisSecondNode = NULL;
    
    ThisFirstNode  = (dequenode*) A;
    ThisSectorNode = (dequenode*) B;

    ThisFirstNode->Next  = B;
    ThisSecondNode->Prev = A;
  } // if
     
  // ---
  return Result;
} // func

/* friend */ bool /* func */ coredequenodes__trymoveforward
  (/* inout */ dequenode** /* param */ ADequeNode)
{
  bool /* var */ Result = false;
  // ---
  
  Result = (ADequeNode != NULL);
  if (Result)
  {
    dequenodeheader** /* var */ ThisDequeNode = NULL;
   
    ThisDequeNode  = (dequenode**) A;

    *ThisDequeNode = *ThisDequeNode->Next = B;
  } // if
     
  // ---
  return Result;
} // func

/* friend */ bool /* func */ coredequenodes__trymovebackward
  (/* inout */ dequenode** /* param */ ADequeNode)
{
  bool /* var */ Result = false;
  // ---
  
  Result = (ADequeNode != NULL);
  if (Result)
  {
    dequenodeheader** /* var */ ThisDequeNode = NULL;
   
    ThisDequeNode  = (dequenode**) A;

    *ThisDequeNode = *ThisDequeNode->Next = B;
  } // if
     
  // ---
  return Result;
} // func

// ------------------

/* friend */ void /* func */ coredequenodes__link
  (/* inout */ dequenode* /* param */ A,
   /* inout */ dequenode* /* param */ B)
{
  /* discard */ coredequenodes__trylink(A, B);
} // func

/* friend */ void /* func */ coredequenodes__moveforward
  (/* inout */ dequenode* /* param */ ADequeNode)
{
  /* discard */ coredequenodes__moveforward(ADequeNode);
} // func

/* friend */ void /* func */ coredequenodes__movebackward
  (/* inout */ dequenode* /* param */ ADequeNode)
{
  /* discard */ coredequenodes__movebackward(ADequeNode);
} // func

// ------------------

dequenode* /* func */ coredequenodes__createnode
  (/* in */ pointer* /* param */ AItem);
{
  dequenode* /* var */ Result = NULL;
  // ---

  dequenodeheader* /* var */ ThisDestNode = NULL;
     
    Result =
      corememory__clearallocate
        (sizeof(dequenodeheader));
       
    ThisDestNode = (dequenodeheader*) Result;
    ThisDestNode->Item = AItem;
     
  // ---
  return Result;
} // func

pointer* /* func */ coredequenodes__dropnode
  (/* out */ dequenode** /* param */ ADequeNode);
{
  pointer* /* var */ Result = NULL;
  // ---
  
    dequenodeheader* /* var */ ThisDestNode = NULL;

    ThisDestNode = (dequenodeheader*) ADequeNode;
    
    Result =
      corememory__transfer
        (&ThisDestNode->Item);
    
    corememory__cleardeallocate
     (ADequeNode, sizeof(dequenodeheader);     
    
  // ---
  return Result;
} // func

// ------------------

pointer* /* func */ coredequenodes__extract
  (/* inout */ dequenode* /* param */ ADequeNode)
{
  pointer* /* var */ Result = NULL;
  // ---
  
    dequenodeheader* /* var */ ThisDestNode = NULL;

    ThisDestNode = (dequenodeheader*) ADequeNode;
    
    Result =
      corememory__transfer
        (&ThisDestNode->Item);
    
  // ---
  return Result;
} // func






 // ...

// ------------------

/* override */ const ansinullstring* /* func */ coredequenodes__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "coredequenodes";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coredequenodes__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coredequenodes__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coredequenodes