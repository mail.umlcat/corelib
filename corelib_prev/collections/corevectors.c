/** Module: "corevectors.c"
 ** Descr.: "Dynamically allocated one single dimension array."
 **/

// namespace corevectors {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

#include "coreiterators.h"

// ------------------

#include "corevectors.h"
 
// ------------------

/* friend */ bool /* func */ corevectors__sameitemsize
  (/* in */ vector* /* param */ AFirstVector,
  (/* in */ vector* /* param */ ASecondVector)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AVector != NULL);
  if (Result)
  {
    vectorheader* /* var */ ThisFirstVector = NULL;
    vectorheader* /* var */ ThisSecondVector = NULL;
      
    ThisFirstVector  = (vectorheader*) AFirstVector;
    ThisSecondVector = (vectorheader*) ASecondVector;
    
    Result =
      (ThisFirstVector->ItemsSize == ThisSecondVector->ItemsSize);
  } // if

  // ---
  return Result;
} // func

/* friend */ bool /* func */ corevectors__sameitemtype
  (/* in */ vector* /* param */ AFirstVector,
  (/* in */ vector* /* param */ ASecondVector)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AVector != NULL);
  if (Result)
  {
    vectorheader* /* var */ ThisFirstVector = NULL;
    vectorheader* /* var */ ThisSecondVector = NULL;
      
    ThisFirstVector  = (vectorheader*) AFirstVector;
    ThisSecondVector = (vectorheader*) ASecondVector;
    
    Result =
      (ThisFirstVector->ItemsType == ThisSecondVector->ItemsType);
  } // if

  // ---
  return Result;
} // func

/* friend */ bool /* func */ corevectors__sameitemcount
  (/* in */ vector* /* param */ AFirstVector,
  (/* in */ vector* /* param */ ASecondVector)
{
  bool /* var */ Result = false;
  // ---
     
  Result = 
    ((AFirstVector != NULL) &&
     (ASecondVector != NULL));
  if (Result)
  {
    vectorheader* /* var */ ThisFirstVector  = NULL;
    vectorheader* /* var */ ThisSecondVector = NULL;
       
    ThisFirstVector  = (vectorheader*) AFirstVector;
    ThisSecondVector = (vectorheader*) ASecondVector;
    
    Result =
      (ThisFirstVector->ItemsMaxCount == ThisSecondVector->ItemsMaxCount);
  } // if
 
  // ---
  return Result;
} // func

// ------------------

/* friend */ bool /* func */ corevectors__trycopyempty
  (/* in */  vector*  /* param */ ASourceVector,
  (/* out */ vector** /* param */ ADestVector)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (ASourceVector != NULL);
  if (Result)
  {
    vectorheader* /* var */ ThisSourceVector = NULL;
    ThisSourceVector = (vectorheader*) ASourceVector;

    ADestVector =
      corevectors__createvector();
    ThisDestVector = (vectorheader*) ADestVector;
    
    ThisDestVector->ItemsType =
      ThisDestVector->ItemsType;
      
    ThisDestVector->ItemsSize =
      ThisDestVector->ItemsSize;
      
    ThisDestVector->ItemsMaxCount =
      ThisDestVector->ItemsMaxCount;
  
    ThisDestVector->IteratorCount =
      0;

    ThisDestVector->ItemsFirst =
      corememory__clearallocatearray  
        (ThisDestVector->ItemsSize,
         ThisDestVector->ItemsMaxCount);
         
    ThisDestVector->ItemsLast =
      corememory__itematbysize
        (ThisDestVector->ItemsFirst,
         ThisDestVector->ItemsMaxCount - 1,
         ThisDestVector->ItemsSize);
  } // if
  
  // ---
  return Result;
} // func

/* friend */ bool /* func */ corevectors__trycopyall
  (/* in */  vector*  /* param */ ASourceVector,
  (/* out */ vector** /* param */ ADestVector)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (ASourceVector != NULL);
  if (Result)
  {
    vectorheader* /* var */ ThisSourceVector = NULL;
    ThisSourceVector = (vectorheader*) ASourceVector;

    ADestVector =
      corevectors__createvector();
    ThisDestVector = (vectorheader*) ADestVector;
    
    ThisDestVector->ItemsType =
      ThisDestVector->ItemsType;
      
    ThisDestVector->ItemsSize =
      ThisDestVector->ItemsSize;
      
    ThisDestVector->ItemsMaxCount =
      ThisDestVector->ItemsMaxCount;
  
    ThisDestVector->IteratorCount =
      0;

    ThisDestVector->ItemsFirst =
      corememory__duplicatecopyarray
        (ThisSourceVector->ItemsFirst,
         ThisDestVector->ItemsSize,
         ThisDestVector->ItemsMaxCount);
         
    ThisDestVector->ItemsLast =
         corememory__itematbysize
        (ThisDestVector->ItemsFirst,
         ThisDestVector->ItemsMaxCount - 1,
         ThisDestVector->ItemsSize);
  } // if

  // ---
  return Result;
} // func

/* friend */ bool /* func */ corevectors__trycopyreverse
  (/* in */  vector*  /* param */ ASourceVector,
  (/* out */ vector** /* param */ ADestVector)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (ASourceVector != NULL);
  if (Result)
  {
    vectorheader* /* var */ ThisSourceVector = NULL;
    ThisSourceVector = (vectorheader*) ASourceVector;

    ADestVector =
      corevectors__createvector();
    ThisDestVector = (vectorheader*) ADestVector;
    
    ThisDestVector->ItemsType =
      ThisDestVector->ItemsType;
      
    ThisDestVector->ItemsSize =
      ThisDestVector->ItemsSize;
      
    ThisDestVector->ItemsMaxCount =
      ThisDestVector->ItemsMaxCount;
  
    ThisDestVector->IteratorCount =
      0;

    ThisDestVector->ItemsFirst =
      corememory__clearallocatearray  
        (ThisDestVector->ItemsSize,
         ThisDestVector->ItemsMaxCount);
         
    ThisDestVector->ItemsLast =
         corememory__itematbysize
        (ThisDestVector->ItemsFirst,
         ThisDestVector->ItemsMaxCount - 1,
         ThisDestVector->ItemsSize);
    
    corememory__safereversecopyarray
      (ThisDestVector->ItemsLast,
       ThisSourceVector->ItemsFirst,
       ThisDestVector->ItemsSize,
       ThisDestVector->ItemsMaxCount);       
  } // if

  // ---
  return Result;
} // func

/* friend */ bool /* func */ corevectors__trycopyconcat
  (/* in */  vector*  /* param */ AFirstVector,
   /* in */  vector*  /* param */ ASecondVector,
   /* out */ vector** /* param */ ADestVector)
{
  bool /* var */ Result = false;
  // ---
     
  Result = 
    ((AFirstVector != NULL) &&
     (ASecondVector != NULL));
  if (Result)
  {
    Result =
      corevectors__sameitemsize
        (AFirstVector, ASecondVector) &&
      corevectors__sameitemtype
        (AFirstVector, ASecondVector);
    if (Result)
    {
      vectorheader* /* var */ ThisFirstVector = NULL;
      vectorheader* /* var */ ThisSecondVector = NULL;
       
      ThisFirstVector  = (vectorheader*) AFirstVector;
      ThisSecondVector = (vectorheader*) ASecondVector;
    
      count_t /* var */ NewCount =
        (ThisFirstVector->ItemsMaxCount + ThisSecondVector->ItemsMaxCount);
        
      ADestVector =
        corevectors__createvector();
      ThisDestVector = (vectorheader*) ADestVector;
    
      ThisDestVector->ItemsType =
        ThisFirstVector->ItemsType;
      
      ThisDestVector->ItemsSize =
        ThisFirstVector->ItemsSize;
      
      ThisDestVector->ItemsMaxCount =
        ThisFirstVector->ItemsMaxCount;
  
      ThisDestVector->IteratorCount =
        NewCount;
        
      ThisDestVector->ItemsFirst =
        corememory__clearallocatearray  
          (ThisDestVector->ItemsSize,
           ThisDestVector->ItemsMaxCount);

      // copy first array items
      corememory__safecopyarray
        (ThisDestVector->ItemsFirst,
         ThisFirstVector->ItemsFirst,
         ThisFirstVector->ItemsSize,
         ThisFirstVector->ItemsMaxCount);
      
      // prepare location
      ThisDestVector->ItemsLast =
        corememory__itematbysize
          (ThisDestVector->ItemsFirst,
           ThisDestVector->ItemsMaxCount,
           ThisDestVector->ItemsSize);

      // copy second array items
      corememory__safecopyarray
        (ThisDestVector->ItemsLast,
         ThisSecondVector->ItemsFirst,
         ThisSecondVector->ItemsSize,
         ThisSecondVector->ItemsMaxCount);

      // update last item pointer   
      ThisDestVector->ItemsLast =
        corememory__itematbysize
          (ThisDestVector->ItemsFirst,
           ThisDestVector->ItemsMaxCount - 1,
           ThisDestVector->ItemsSize);
    } // if
  } // if

  // ---
  return Result;
} // func

// ------------------

/* friend */ void /* func */ corevectors__copyempty
  (/* in */  vector*  /* param */ ASourceVector,
  (/* out */ vector** /* param */ ADestVector)
{
  /* discard */ corevectors__trycopyempty
    (ASourceVector, ADestVector);
} // func

/* friend */ void /* func */ corevectors__copyall
  (/* in */  vector*  /* param */ ASourceVector,
  (/* out */ vector** /* param */ ADestVector)
{
  /* discard */ corevectors__trycopyall
    (ASourceVector, ADestVector);
} // func

/* friend */ void /* func */ corevectors__copyreverse
  (/* in */  vector*  /* param */ ASourceVector,
  (/* out */ vector** /* param */ ADestVector)
{
  /* discard */ corevectors__trycopyreverse
    (ASourceVector, ADestVector);
} // func

/* friend */ void /* func */ corevectors__copyconcat
  (/* in */  vector*  /* param */ AFirstVector,
   /* in */  vector*  /* param */ ASecondVector,
   /* out */ vector** /* param */ ADestVector)
{
  /* discard */ corevectors__trycopyconcat
    (AFirstVector, ASecondVector, ADestVector);
} // func

// ------------------

vector* /* func */ corevectors__createvector
  ( noparams )
{
  vector* /* var */ Result = NULL;
  // ---

  vectorheader* /* var */ AVector = NULL;

  Result =
    corememory__clearallocate
      (sizeof(vectorheader));
      
  Result->DimsCount = 1;
           
  AVector = Result;
 
  // ---
  return Result;
} // func

vector* /* func */ corevectors__createvectorbyitemtype
  (/* in */ typecode_t /* param */ AItemType)
{
  vector* /* var */ Result = NULL;
  // ---

  vectorheader* /* var */ AVector = NULL;

  AVector =
    corememory__clearallocate
      (sizeof(vectorheader));

  Result->DimsCount = 1;
 
  size_t  /* param */ AItemSize;

  AItemSize =
    corememory__typetosize(AItemType);
  
  AVector->ItemsType =
    AItemType;

  AVector->ItemsSize =
    AItemSize;

  AVector->ItemsMaxCount =
    1024;
    
  AVector->IteratorCount =
    0;

  AVector->ItemsFirst =
    corememory__clearallocatearray
      (sizeof(pointer*), AItemMaxCount);

  AVector->ItemsLast =
    &(AVector->ItemsFirst[AItemMaxCount]);

  Result = AVector;
   
  // ---
  return Result;
} // func

vector* /* func */ corevectors__createvectorbyitemtypecount
  (/* in */ typecode_t /* param */ AItemType,
   /* in */ count_t    /* param */ AItemMaxCount)
{
  vector* /* var */ Result = NULL;
  // ---

  vectorheader* /* var */ AVector = NULL;
 
  AVector =
    corememory__clearallocate
      (sizeof(vectorheader));

  Result->DimsCount = 1;

  size_t  /* param */ AItemSize;

  AItemSize =
    corememory__typetosize(AItemType);
  
  AVector->ItemsType =
    AItemType;

  AVector->ItemsSize =
    AItemSize;

  AVector->ItemsMaxCount =
    AItemMaxCount;

  AVector->IteratorCount =
    0;

  AVector->ItemsFirst =
    corememory__clearallocatearray
      (sizeof(pointer*), AItemMaxCount);

  AVector->ItemsLast =
    &(AVector->ItemsFirst[AItemMaxCount]);

  Result = AVector;

  // ---
  return Result;
} // func

vector* /* func */ corevectors__createvectorbyitemsizecount
  (/* in */ size_t  /* param */ AItemSize,
   /* in */ count_t /* param */ AItemMaxCount)
{
  vector* /* var */ Result = NULL;
  // ---

  vectorheader* /* var */ AVector = NULL;

  Result->DimsCount = 1;

  AVector =
    corememory__clearallocate
      (sizeof(vectorheader));

  AVector->ItemsType =
    unknowntype;

  AVector->ItemsSize =
    AItemSize;

  AVector->ItemsMaxCount =
    AItemMaxCount;

  AVector->IteratorCount =
    0;

  AVector->ItemsFirst =
    corememory__clearallocatearray
      (sizeof(pointer*), AItemMaxCount);

  AVector->ItemsLast =
    &(AVector->ItemsFirst[AItemMaxCount]);

  Result = AVector;
  // ---
  return Result;
} // func

vector* /* func */ corevectors__createvectorbycopy
  (/* in */ const vector* /* param */ ASourceVector)
{
  vector* /* var */ Result = NULL;
  // ---

  if (ASourceVector != NULL)
  {
    vectorheader* /* var */ ThisSourceVector = NULL;
    vectorheader* /* var */ ThisDestVector   = NULL;

    Result =
      corememory__clearallocate
        (sizeof(vectorheader));

    Result->DimsCount = 1;

    ThisSourceVector = (vectorheader*) ASourceVector;
    ThisDestVector   = (vectorheader*) Result;
  
    ThisDestVector->ItemsType =
      ThisDestVector->ItemsType;
      
    ThisDestVector->ItemsSize =
      ThisDestVector->ItemsSize;
      
    ThisDestVector->ItemsMaxCount =
      ThisDestVector->ItemsMaxCount;
  
    ThisDestVector->IteratorCount =
      0;

    ThisDestVector->ItemsFirst =
      corememory__duplicatecopyarray
        (ThisSourceVector->ItemsFirst,
         ThisDestVector->ItemsSize,
         ThisDestVector->ItemsMaxCount);
         
    ThisDestVector->ItemsLast =
         corememory__itematbysize
        (ThisDestVector->ItemsFirst,
         ThisDestVector->ItemsMaxCount - 1,
         ThisDestVector->ItemsSize);
  } // if

  // ---
  return Result;
} // func

void /* func */ corevectors__dropvector
  (/* out */ vector** /* param */ AVector)
{
  Result = (AVector != NULL);
  if (Result)
  {
    vectorheader* /* var */ ThisVector = NULL;
    ThisVector = (vectorheader*) AVector;

    corememory__unshare
      (&ThisVector->ItemsLast);

    corememory__cleardeallocatearray
     (AVector->ItemsFirst, AVector->ItemsSize, AVector->ItemsMaxCount);   
  
    corememory__cleardeallocate
     (AVector, sizeof(vectorheader);     
  } // if
} // func

// ------------------

bool /* func */ corevectors__underiteration
  (/* in */ vector* /* param */ AVector)
{
  bool /* var */ Result = false;
  // ---
  
  if (AVector != NULL)
  {
    vectorheader* /* var */ ThisVector = NULL;
    ThisVector = (vectorheader*) AVector;

	Result = (ThisVector->IteratorCount > 0);
  }
     
  // ---
  return Result;
} // func

vectoriterator* /* func */ corevectors__createiteratorfwd
  (/* in */ vector* /* param */ AVector)
{
  vectoriterator* /* var */ Result = NULL;
  // ---
     
  if (AVector != NULL)
  {
    vectorheader* /* var */ ThisVector = NULL;
    vectoriteratorheader* /* var */ ThisIterator = NULL;
    
    ThisVector =
      (vectorheader*) corememory__share(AVector);

    ThisIterator =
      corememory__clearallocate
       (sizeof(vectoriteratorheader));

    ThisIterator->Vector =
      corememory__share(AVector);
  
    ThisIterator->ItemsFirst =
      corememory__share
        (ThisVector->ItemsFirst);
  
    ThisIterator->CurrentItem =
      corememory__share
        (ThisVector->ItemsFirst);

    ThisIterator->ItemsMaxCount =
      ThisVector->ItemsMaxCount;

    ThisIterator->ItemIndex =
      0;

    ThisIterator->Direction =
      listdirections__fwd;
    
    ThisVector->IteratorCount =
      (ThisVector->IteratorCount + 1);

    Result = ThisIterator;
  } // if
    
  // ---
  return Result;
} // func

vectoriterator* /* func */ corevectors__createiteratorback
  (/* in */ vector* /* param */ AVector)
{
  vectoriterator* /* var */ Result = NULL;
  // ---

  if (AVector != NULL)
  {
    vectorheader* /* var */ ThisVector = NULL;
    vectoriteratorheader* /* var */ ThisIterator = NULL;
    
    ThisVector =
      (vectorheader*) corememory__share(AVector);

    ThisIterator =
      corememory__clearallocate
       (sizeof(vectoriteratorheader));

    ThisIterator->Vector =
      corememory__share(AVector);

    ThisIterator->ItemsFirst =
      corememory__share
        (ThisVector->ItemsFirst);
         
    ThisIterator->CurrentItem =
      corememory__share
        (ThisVector->ItemsLast);

    ThisIterator->ItemsMaxCount =
      ThisVector->ItemsMaxCount;

    ThisIterator->ItemIndex =
      ThisVector->ItemsMaxCount;

    ThisIterator->Direction =
      listdirections__back;
      
    ThisVector->IteratorCount =
      (ThisVector->IteratorCount + 1);
  
    Result = ThisIterator;
  } // if

  // ---
  return Result;
} // func

void /* func */ corevectors__dropiterator
  (/* out */ vectoriterator** /* param */ AIterator)
{
  Result = (AIterator != NULL);
  if (Result)
  {
    vectoriteratorheader** /* var */ ThisIterator = NULL;
    vectorheader*          /* var */ ThisVector =  NULL;

    ThisIterator = (vectoriteratorheader**) AIterator;

    ThisVector = (vectorheader*) ThisIterator->Vector;
      
    ThisVector->IteratorCount =
      (ThisVector->IteratorCount - 1);

    corememory__unshare
      (&(ThisIterator->ItemsFirst));

    corememory__unshare
      (&(ThisIterator->CurrentItem));

    corememory__cleardeallocate
      (ThisIterator);
      
    *AIterator = NULL;
  } // if
} // func

// ------------------

bool /* func */ corevectors__isdone
  (/* in */ const vectoriterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator == NULL);
  if (!Result)
  {
    vectoriteratorheader* /* var */ ThisIterator = NULL;
    ThisIterator =
      (vectoriteratorheader*) AIterator;
    
    if (ThisIterator->Direction == listdirections__fwd)
    {
      Result =
        (ThisIterator->ItemIndex >= ThisIterator->IteratorCount);
    }
    else if (ThisIterator->Direction == listdirections__back)
    {
      Result =
        (ThisIterator->ItemIndex < 0);
    }
  } // if

  // ---
  return Result;
} // func

bool /* func */ corevectors__trymovenext
  (/* inout */ vectoriterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    vectoriteratorheader* /* var */ ThisIterator = NULL;
    ThisIterator =
      (vectoriteratorheader*) AIterator;
  
    if (ThisIterator->Direction == listdirections__fwd)
    {
      Result =
        (ThisIterator->ItemIndex >= ThisIterator->IteratorCount);
      if (Result)
      {
        ThisIterator->ItemIndex =
          (ThisIterator->ItemIndex + 1);
        
        ThisIterator->CurrentItem =
          corememory__itematbysize
            (ThisIterator->ItemsFirst, ThisIterator->ItemIndex, ThisIterator->ItemsSize);
      }
    }
    else if (ThisIterator->Direction == listdirections__back)
    {
      Result =
        (ThisIterator->ItemIndex < 0);
        
      if (Result)
      {
        ThisIterator->ItemIndex =
          (ThisIterator->ItemIndex - 1);
          
        ThisIterator->CurrentItem =
          corememory__itematbysize
            (ThisIterator->ItemsFirst, ThisIterator->ItemIndex, ThisIterator->ItemsSize);
      }
    }
  } // if
     
  // ---
  return Result;
} // func

void /* func */ corevectors__movenext
  (/* inout */ vectoriterator* /* param */ AIterator)
{
  /* discard */ corevectors__trymovenext(AIterator);
} // func

bool /* func */ corevectors__tryreaditem
  (/* inout */ vectoriterator* /* param */ AIterator
   /* out */   pointer*        /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      vectoriteratorheader** /* var */ ThisIterator = NULL;
      ThisIterator = (vectoriteratorheader**) AIterator;

      corememory__safecopy
        (AItem, ThisIterator->CurrentItem, ThisIterator->ItemsSize);
    } // if
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corevectors__trywriteitem
  (/* inout */ vectoriterator* /* param */ AIterator
   /* in */    const  pointer* /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      vectoriteratorheader** /* var */ ThisIterator = NULL;
      ThisIterator = (vectoriteratorheader**) AIterator;

      corememory__safecopy
        (ThisIterator->CurrentItem, AItem, ThisIterator->ItemsSize);
    } // if
  } // if
     
  // ---
  return Result;
} // func

void /* func */ corevectors__readitem
  (/* inout */ vectoriterator* /* param */ AIterator
   /* out */   pointer*        /* param */ AItem)
{
  /* discard */ corevectors__tryreaditem
    (AIterator, AItem);
} // func

void /* func */ corevectors__writeitem
  (/* inout */ vectoriterator* /* param */ AIterator
   /* in */    const  pointer* /* param */ AItem)
{
  /* discard */ corevectors__trywriteitem
    (AIterator, AItem);
} // func

// ------------------

bool /* func */ corevectors__isvalidindex
  (/* in */ const vector* /* param */ AVector,
   /* in */ const index_t /* param */ AIndex)
{
  bool /* var */ Result = FALSE;
  // ---
     
  if (AVector != NULL)
  {
    vectorheader* /* var */ ThisVector = NULL;
    ThisVector = (vectorheader*) AVector;
  	
    Result =
      (AIndex >= 0) &&
      (AIndex < ThisVector->ItemsMaxCount);
  } // if
 
  // ---
  return Result;
} // func

typecode_t /* func */ corevectors__readitemtype
  (/* in */ const vector* /* param */ AVector)
{
  typecode_t /* var */ Result = 0;
  // ---
     
  if (AVector != NULL)
  {
    vectorheader* /* var */ ThisVector = NULL;
    ThisVector = (vectorheader*) AVector;
    Result = ThisVector->ItemsType;
  } // if

  // ---
  return Result;
} // func

size_t /* func */ corevectors__readitemsize
  (/* in */ const vector* /* param */ AVector);
{
  size_t /* var */ Result = 0;
  // ---
     
  if (AVector != NULL)
  {
    vectorheader* /* var */ ThisVector = NULL;
    ThisVector = (vectorheader*) AVector;
    Result = ThisVector->ItemsSize;
  } // if
   
  // ---
  return Result;
} // func

// ------------------

count_t /* func */ corevectors__getmaxcount
  (/* in */ const vector* /* param */ AVector)
{
  count_t /* var */ Result = 0;
  // ---
     
  if (AVector != NULL)
  {
    vectorheader* /* var */ ThisVector = NULL;
    ThisVector = (vectorheader*) AVector;
    Result = ThisVector->ItemsMaxCount;
  } // if
  
  // ---
  return Result;
} // func

count_t /* func */ corevectors__getdimcount
  (/* in */ const vector* /* param */ AVector)
{
  count_t /* var */ Result = 0;
  // ---
     
  if (AVector != NULL)
  {
    vectorheader* /* var */ ThisVector = NULL;
    ThisVector = (vectorheader*) AVector;
    Result = ThisVector->DimsCount;
  } // if
  
  // ---
  return Result;
} // func

// ------------------

bool /* func */ corevectors__tryclear
  (/* inout */ vector* /* param */ AVector)
{
  bool /* var */ Result = false;
  // ---
 
  Result = (AVector != NULL);
  if (Result)
  {
    vectorheader* /* var */ ThisVector = NULL;
    ThisVector = (vectorheader*) AVector;
    
    corememory__cleararray
      (ThisVector->ItemsFirst, ThisVector->ItemsSize, ThisVector->ItemsMaxCount);
  } // if
  
  // ---
  return Result;
} // func

void /* func */ corevectors__clear
  (/* inout */ vector* /* param */ AVector)
{
  /* discard */ corevectors__tryclear
    (AVector);
} // func

// ------------------

bool /* func */ corevectors__trygetitemat
  (/* in */  /* restricted */ const vector* /* param */ AVector,
   /* in */  /* restricted */ const index_t /* param */ AIndex,
   /* out */ /* restricted */ pointer**     /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AVector != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      Result =
        (corevectors__isvalidindex(AVector, AIndex));
      if (Result)
      {
        vectorheader* /* var */ ThisVector = NULL;
        ThisVector = (vectorheader*) AVector;
    
        *AItem =
          corememory__itematbysize
            (ThisVector->ItemsFirst, AIndex, ThisVector->ItemsFirst);
      }
    } // if
  } // if
   
  // ---
  return Result;
} // func

bool /* func */ corevectors__tryreaditemat
  (/* in */  /* restricted */ const vector* /* param */ AVector,
   /* in */  /* restricted */ const index_t /* param */ AIndex,
   /* out */ /* restricted */ pointer*      /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AVector != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      Result =
        (corevectors__isvalidindex(AVector, AIndex));
      if (Result)
      {
        vectorheader* /* var */ ThisVector = NULL;
        ThisVector = (vectorheader*) AVector;
   	
        pointer* /* var */ ASourceItem = NULL;
      
        ADestItem =
          corememory__itematbysize
            (ThisVector->ItemsFirst, AIndex, ThisVector->ItemsSize);
          
        corememory__overlappedcopy
          (AItem, ADestItem, AIterator->ItemsSize);
      }
    } // if
  } // if
   
  // ---
  return Result;
} // func

bool /* func */ corevectors__trywriteitemat
  (/* in */ /* restricted */ const vector*  /* param */ AVector,
   /* in */ /* restricted */ const index_t  /* param */ AIndex,
   /* in */ /* restricted */ const pointer* /* param */ AItem);
{
  bool /* var */ Result = false;
  // ---
   
  Result = (AVector != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      Result =
        (corevectors__isvalidindex(AVector, AIndex));
      if (Result)
      {
        vectorheader* /* var */ ThisVector = NULL;
        ThisVector = (vectorheader*) AVector;
   	
        pointer* /* var */ ADestItem = NULL;
      
        ADestItem =
          corememory__itematbysize
            (ThisVector->ItemsFirst, AIndex, ThisVector->ItemsSize);
          
        corememory__overlappedcopy
          ( ADestItem, AItem, AIterator->ItemsSize);
      }
    } // if
  } // if

  // ---
  return Result;
} // func

pointer* /* func */ corevectors__getitemat
  (/* in */ /* restricted */ const vector* /* param */ AVector,
   /* in */ /* restricted */ const index_t /* param */ AIndex);
{
  pointer* /* var */ Result = false;
  // ---

  /* discard */ corevectors__trygetitemat
    (AVector, &Result, AItem);

  // ---
  return Result;
} // func

void /* func */ corevectors__readitemat
  (/* in */  /* restricted */ const vector* /* param */ AVector,
   /* in */  /* restricted */ const index_t /* param */ AIndex,
   /* out */ /* restricted */ pointer*      /* param */ AItem)
{
  /* discard */ coresystem__tryreaditemat
    (AVector, AIndex, AItem);
} // func

void /* func */ corevectors__writeitemat
  (/* in */ /* restricted */ const vector*  /* param */ AVector,
   /* in */ /* restricted */ const index_t  /* param */ AIndex,
   /* in */ /* restricted */ const pointer* /* param */ AItem);
{
  /* discard */ coresystem__trywriteitemat
    (AVector, AIndex, AItem);
} // func

// ------------------

bool /* func */ corevectors__tryclearat
  (/* inout */ /* restricted */ vector*       /* param */ AVector,
   /* in */    /* restricted */ const index_t /* param */ AIndex)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AVector != NULL);
  if (Result)
  {
    Result =
        (corevectors__isvalidindex(AVector, AIndex));
    if (Result)
    {
      vectorheader* /* var */ ThisVector = NULL;
      ThisVector = (vectorheader*) AVector;
   	
      pointer* /* var */ ADestItem = NULL;
      
      ADestItem =
        corememory__itematbysize
          (ThisVector->ItemsFirst, AIndex, ThisVector->ItemsSize);
          
      corememory__clear
        (ADestItem, AIterator->ItemsSize);
    }
  } // if
   
  // ---
  return Result;
} // func

bool /* func */ corevectors__tryexchangeat
  (/* inout */ /* restricted */ vector*       /* param */ AVector,
   /* in */    /* restricted */ const index_t /* param */ AFirstIndex,
   /* in */    /* restricted */ const index_t /* param */ ASecondIndex)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AVector != NULL);
  if (Result)
  {
    Result =
        (corevectors__isvalidindex(AVector, AFirstIndex) &&
        (corevectors__isvalidindex(AVector, ASecondIndex));
    if (Result)
    {
      vectorheader* /* var */ ThisVector = NULL;
      ThisVector = (vectorheader*) AVector;
   	
      pointer* /* var */ AFirstItem  = NULL;
      pointer* /* var */ ASecondItem = NULL;
      
      AFirstItem =
        corememory__itematbysize
          (ThisVector->ItemsFirst, AFirstIndex, ThisVector->ItemsSize);
      ASecondItem =
        corememory__itematbysize
          (ThisVector->ItemsFirst, ASecondIndex, ThisVector->ItemsSize);
          
      corememory__overlappedexchange
        (AFirstItem, ASecondItem, AIterator->ItemsSize);
    }
  } // if
     
  // ---
  return Result;
} // func

void /* func */ corevectors__clearat
  (/* inout */ /* restricted */ vector*       /* param */ AVector,
   /* in */    /* restricted */ const index_t /* param */ AIndex)
{
  /* discard */ coresystem__tryclearat
    (AVector, AIndex, AItem);
} // func

void /* func */ corevectors__exchangeat
  (/* inout */ /* restricted */ vector*       /* param */ AVector,
   /* in */    /* restricted */ const index_t /* param */ AFirstIndex,
   /* in */    /* restricted */ const index_t /* param */ ASecondIndex)
{
  /* discard */ coresystem__tryexchangeat
    (AVector, AIndex, AItem);
} // func

// ------------------




 
 // ...

// ------------------

/* override */ const ansinullstring* /* func */ corevectors__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "corevectors";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ corevectors__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corevectors__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace corevectors