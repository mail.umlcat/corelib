/** Module: "corequeuenodes.h"
 ** Descr.: "..."
 **/
 
// namespace corequeuenodes {
 
// ------------------
 
#ifndef COREQUEUENODES__H
#define COREQUEUENODES__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

typedef
  pointer* /* as */ queuenode;

struct queuenodeheader
{
  queuenode* /* var */ Prev;  
  
  pointer*   /* var */ Item;
} ;

// ------------------

/* friend */ bool /* func */ corequeuenodes__arelinked
  (/* in */ const queuenode* /* param */ A,
   /* in */ const queuenode* /* param */ B);

/* friend */ bool /* func */ corequeuenodes__trylink
  (/* inout */ queuenode* /* param */ A,
   /* inout */ queuenode* /* param */ B);

/* friend */ bool /* func */ corequeuenode__trymoveforward
  (/* inout */ queuenode* /* param */ AQueueNode);

// -----------------

/* friend */ void /* func */ corequeuenodes__link
  (/* inout */ queuenode* /* param */ A,
   /* inout */ queuenode* /* param */ B);

/* friend */ void /* func */ corequeuenodes__moveforward
  (/* inout */ queuenode** /* param */ AQueueNode);

// ------------------

queuenode* /* func */ corequeuenodes__createnode
  (/* in */ pointer* /* param */ AItem);

pointer* /* func */ corequeuenodes__dropnode
  (/* out */ queuenode** /* param */ AQueueNode);

// ------------------


 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ corequeuenodes__modulename
  ( noparams );

/* override */ int /* func */ corequeuenodes__setup
  ( noparams );

/* override */ int /* func */ corequeuenodes__setoff
  ( noparams );

// ------------------

#endif // COREQUEUENODES__H

// } // namespace corequeuenodes