/** Module: "corearraytrees.h"
 ** Descr.: "..."
 **/
 
// namespace corearraytrees {
 
// ------------------
 
#ifndef COREARRAYTREENODES__H
#define COREARRAYTREENODES__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"
#include "corearraytreenodes.h"

// ------------------

typedef
  pointer* /* as */ corearraytrees__arraytree;

struct arraytreeheader
{
  // pointer to first node
  arraytreenode* /* var */ RootNode;

  // only applies when items are same type
  // "unknowntype" value means different type
  typecode /* var */ ItemsType;

  // applies instead of "typecode"
  // "0" means not used
  size_t   /* var */ ItemsSize;

  // restricts how many items can be stored
  // "0" means not used
  count_t  /* var */ ItemsMaxCount;

  // how many items are currently stored
  // "global" count
  count_t  /* var */ ItemsCurrentCount;
  
  // indicates iterator related objects
  count_t  /* var */ IteratorCount;
  
  equality /* var */ MatchFunc;

  // ...
} ;

struct arraytreeiteratorheader
{
  arraytree*          /* var */ MainTree;

  arraytreenode*      /* var */ CurrentNode;

  //arraytreedirections /* var */ Direction; 
} ;

// ------------------

arraytree* /* func */ corearraytrees__createtree
  ( noparams );

arraytree* /* func */ corearraytrees__createtreebytype
  (/* in */ typecode_t* /* param */ AItemType);
  
arraytree* /* func */ corearraytrees__createtreebysize
  (/* in */ size_t* /* param */ AItemSize);

arraytree* /* func */ corearraytrees__createatreerestrictcount
  (/* in */ count_t* /* param */ ACount);

void /* func */ corearraytrees__droptree
  (/* inout */ arraytree** /* param */ ATree);

// ------------------

void /* func */ corearraytrees__assignequalityfunc
  (/* inout */ arraytree* /* param */ ATree,
   /* in */    equality   /* param */ AMatchFunc);

// ------------------

arraytreenode* /* func */ corearraytrees__getroot
  (/* in */ const arraytree* /* param */ ATree);

bool /* func */ corearraytrees__hasroot
  (/* in */ const arraytree* /* param */ ATree);

// ------------------

bool /* func */ corearraytrees__isempty
  (/* in */ const arraytree* /* param */ ATree);

bool /* func */ corearraytrees__isfull
  (/* in */ const arraytree* /* param */ ATree);

bool /* func */ corearraytrees__hasitems
  (/* in */ const arraytree* /* param */ ATree);

// ------------------

void /* func */ corearraytrees__insertroot
  (/* inout */ arraytree* /* param */ ATree,
   /* in */    pointer*   /* param */ AItem);

pointer* /* func */ corearraytrees__droproot
  (/* in */ const arraytree* /* param */ ATree);

// ------------------

index_t /* func */ corearraytrees__generateindex
  (/* inout */ const arraytree* /* param */ ATree);

// ------------------


 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ corearraytrees__modulename
  ( noparams );

/* override */ int /* func */ corearraytrees__setup
  ( noparams );

/* override */ int /* func */ corearraytrees__setoff
  ( noparams );

// ------------------

#endif // COREARRAYTREES__H

// } // namespace corearraytrees