/** Module: "coremapnodes.h"
 ** Descr.: "Unique Key and duplicated value,"
 **         "list items."
 **/

// namespace coremapnodes {
 
// ------------------
 
#ifndef COREMAPNODES__H
#define COREMAPNODES__H
 
// ------------------
 
#include <stdlib.h"
#include <stddef.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "corekeyvalues.h"

// ------------------

// opaque type
typedef
  pointer* /* as */ mapnode;

struct mapnodeheader
{
  mapnode*  /* var */ Prev;
  mapnode*  /* var */ Next;  

  keyvalue* /* var */ KeyValue;
} ;

typedef
  mapnodeheader  /* as */ mapnode_t;
typedef
  mapnodeheader* /* as */ mapnode_p;

// ------------------

/* friend */ bool /* func */ coremapnodes__arelinked
  (/* in */ const mapnode* /* param */ A,
   /* in */ const mapnode* /* param */ B);


/* friend */ bool /* func */ coremapnodes__trylink
  (/* inout */ mapnode* /* param */ A,
   /* inout */ mapnode* /* param */ B);

/* friend */ bool /* func */ coremapnodes__trymoveforward
  (/* inout */ mapnode* /* param */ AListNode);

/* friend */ bool /* func */ coremapnodes__trymovebackward
  (/* inout */ mapnode* /* param */ AListNode);

// ------------------

/* friend */ void /* func */ coremapnodes__link
  (/* inout */ mapnode* /* param */ A,
   /* inout */ mapnode* /* param */ B);

/* friend */ void /* func */ coremapnodes__moveforward
  (/* inout */ mapnode** /* param */ AMapNode);

/* friend */ void /* func */ coremapnodes__movebackward
  (/* inout */ mapnode** /* param */ AMapNode);

// ------------------

mapnode* /* func */ coremapnodes__createnodeempty
  ( noparams );

mapnode* /* func */ coremapnodes__createnodebykey
  (/* in */ pointer* /* param */ AKey);

mapnode* /* func */ coremapnodes__createnodebyvalue
  (/* in */ pointer* /* param */ AKey,
   /* in */ pointer* /* param */ AValue);

mapnode* /* func */ coremapnodes__createnodebykeyvalue
  (/* in */ pointer* /* param */ AKeyValue);

// ------------------

void /* func */ coremapnodes__dropnodebysize
  (/* out */ mapnode** /* param */ AMapNode,
   /* in */  size_t    /* param */ AKeySize,
   /* in */  size_t    /* param */ AValueSize);

void /* func */ coremapnodes__dropnodeextractkeyvalue
  (/* out */ mapnode**  /* param */ AMapNode,
   /* out */ keyvalue** /* param */ AKeyValue);

void /* func */ coremapnodes__dropnodeextract
  (/* out */ mapnode** /* param */ AMapNode,
   /* out */ pointer** /* param */ AKey,
   /* out */ pointer** /* param */ AValue);

// ------------------

pointer* /* func */ coremapnodes__getkey
  (/* inout */ mapnode* /* param */ AMapNode);

pointer* /* func */ coremapnodes__getvalue
  (/* inout */ mapnode* /* param */ AMapNode);

// ------------------

bool /* func */ coremapnodes__MatchKey
  (/* in */ mapnode* /* param */ AMapNode,
   /* in */ pointer* /* param */ AKey);
   /* in */ equality /* param */ MatchKey);

// ------------------

pointer* /* func */ coremapnodes__replacekey
  (/* out */ mapnode* /* param */ AMapNode,
   /* in */  pointer* /* param */ ANewKey);

pointer* /* func */ coremapnodes__replacevalue
  (/* out */ mapnode* /* param */ AMapNode,
   /* in */  pointer* /* param */ ANewValue);
 

// ------------------

bool /* func */ coremapnodes__tryextractkeyvalue
  (/* out */ mapnode*   /* param */ AMapNode,
   /* out */ keyvalue** /* param */ AKeyValue);

bool /* func */ coremapnodes__tryextractitems
  (/* out */ mapnode*  /* param */ AMapNode,
   /* out */ pointer** /* param */ AKey,
   /* out */ pointer** /* param */ AValue);

void /* func */ coremapnodes__extractkeyvalue
  (/* out */ mapnode*   /* param */ AMapNode,
   /* out */ keyvalue** /* param */ AKeyValue);

void /* func */ coremapnodes__extractitems
  (/* out */ mapnode*  /* param */ AMapNode,
   /* out */ pointer** /* param */ AKey,
   /* out */ pointer** /* param */ AValue);


// ------------------

 // ...

// ------------------

/* override */ int /* func */ coremapnodes__setup
  ( noparams );

/* override */ int /* func */ coremapnodes__setoff
  ( noparams );

// ------------------

#endif // COREMAPNODES__H

// } // namespace coremapnodes