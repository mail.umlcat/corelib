/** Module: "corearraylistnodes.c"
 ** Descr.: "Double Linked Indexed List"
 **         "Node."
 **/

// namespace corearraylistnodes {
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "corearraylistnodes.h"
 
// ------------------

/* friend */ bool /* func */ corearraylistnodes__arelinked
  (/* in */ const arraylistnode* /* param */ A,
   /* in */ const arraylistnode* /* param */ B);
{
  bool /* var */ Result = false;
  // ---

  Result = (A != NULL) && (B != NULL);
  if (Result)
  {
      arraylistnodeheader* /* var */ ThisFirstArrayListNodes  = NULL;
      arraylistnodeheader* /* var */ ThisSecondArrayListNodes = NULL;

      ThisFirstArrayListNodes  = (arraylistnodeheader*) A;
      ThisSecondArrayListNodes = (arraylistnodeheader*) B;
      
      Result =
        (ThisFirstArrayListNodes->Next  == ThisSecondArrayListNodes) &&
        (ThisSecondArrayListNodes->Prev == ThisFirstArrayListNodes);
  } // if
    
  // ---
  return Result;
} // func

/* friend */ bool /* func */ corearraylistnodes__trylink
  (/* inout */ arraylistnode* /* param */ A,
   /* inout */ arraylistnode* /* param */ B);
{
  bool /* var */ Result = false;
  // ---

  Result = (A != NULL) && (B != NULL);
  if (Result)
  {
      arraylistnodeheader* /* var */ ThisFirstArrayListNodes  = NULL;
      arraylistnodeheader* /* var */ ThisSecondArrayListNodes = NULL;

      ThisFirstArrayListNodes  = (arraylistnodeheader*) A;
      ThisSecondArrayListNodes = (arraylistnodeheader*) B;
      
      ThisFirstArrayListNodes->Next  = ThisSecondArrayListNodes;
      ThisSecondArrayListNodes->Prev = ThisFirstArrayListNodes;
  } // if
    
  // ---
  return Result;
} // func

/* friend */ bool /* func */ corearraylistnodes__trymoveforward
  (/* inout */ arraylistnode** /* param */ AListNode)
{
  bool /* var */ Result = false;
  // ---
  
  Result = (AListNode != NULL);
  if (Result)
  {
    listnodeheader** /* var */ ThisListNode = NULL;
   
    ThisListNode  = (listnode**) A;

    *ThisListNode = *ThisListNode->Next = B;
  } // if
     
  // ---
  return Result;
} // func

/* friend */ bool /* func */ corearraylistnodes__trymovebackward
  (/* inout */ arraylistnode** /* param */ AListNode)
{
  bool /* var */ Result = false;
  // ---
  
  Result = (AListNode != NULL);
  if (Result)
  {
    listnodeheader** /* var */ ThisListNode = NULL;
   
    ThisListNode  = (listnode**) A;

    *ThisListNode = *ThisListNode->Next = B;
  } // if
     
  // ---
  return Result;
} // func

// ------------------

/* friend */ void /* func */ corearraylistnodes__link
  (/* inout */ arraylistnode* /* param */ A,
   /* inout */ arraylistnode* /* param */ B)
{
  /* discard */ corearraylistnodes__trylink(A, B);
} // func

/* friend */ void /* func */ corearraylistnodes__moveforward
  (/* inout */ arraylistnode** /* param */ AListNode)
{
  /* discard */ corearraylistnodes__moveforward(AListNode);
} // func

/* friend */ void /* func */ corearraylistnodes__movebackward
  (/* inout */ arraylistnode** /* param */ AListNode)
{
  /* discard */ corearraylistnodes__movebackward(AListNode);
} // func

// ------------------

arraylistnode* /* func */ corearraylistnodes__createnode
  (/* in */ index_t  /* param */ AIndex,
   /* in */ pointer* /* param */ AItem)
{
  arraylistnode* /* var */ Result = NULL;
  // ---

  arraylistnodeheader* /* var */ ThisDestNode = NULL;
     
    Result =
      corememory__clearallocate
        (sizeof(arraylistnodeheader));
       
    ThisDestNode = (arraylistnodeheader*) Result;
    
    ThisDestNode->Index = AIndex;
    ThisDestNode->Item  = AItem;
     
  // ---
  return Result;
} // func

pointer* /* func */ corearraylistnodes__dropnode
  (/* out */ arraylistnode** /* param */ AListNode);
{
  pointer* /* var */ Result = NULL;
  // ---

    if (AListNode != NULL)
    {
      arraylistnodeheader* /* var */ ThisDestNode = NULL;

      ThisDestNode = (arraylistnodeheader*) AListNode;

      Result =
        corememory__transfer
          (&ThisDestNode->Item);
    
      corememory__cleardeallocate
        (AListNode, sizeof(arraylistnodeheader);     
    } // if
    
  // ---
  return Result;
} // func

 // ...

// ------------------

/* override */ const ansinullstring* /* func */ corearraylistnodes__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "corearraylistnodes";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ corearraylistnodes__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corearraylistnodes__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace corearraylistnodes