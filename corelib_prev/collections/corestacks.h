/** Module: "corestacks.h"
 ** Descr.: "Last In, First Out, Collection."
 **/

// namespace corestacks {
 
// ------------------
 
#ifndef CORESTACKS__H
#define CORESTACKS__H
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"
#include "corestacknodes.h"

// ------------------

typedef
  pointer* /* as */ stack;
typedef
  pointer* /* as */ corestacks__stack;

struct stackheader
{
  // pointer to node before first node
  stacknode* /* var */ TopMarker;

  // pointer to node before first node
  stacknode* /* var */ BottomMarker;

  // only applies when items are same type
  // "unknowntype" value means different type
  typecode /* var */ ItemsType;

  // applies instead of "typecode"
  // "0" means not used
  size_t   /* var */ ItemsSize;

  // restricts how many items can be stored
  // "0" means not used
  count_t  /* var */ ItemsMaxCount;

  // how many items are currently stored
  count_t  /* var */ ItemsCurrentCount;
  
  // indicates iterator related objects
  count_t  /* var */ IteratorCount;
  
  equality /* var */ MatchFunc;
  
  // ...
} ;

typedef
  stackheader  /* as */ stack_t;
typedef
  stackheader* /* as */ stack_p;

typedef
  stack_t      /* as */ corestacks__stack_t;
typedef
  stack_p      /* as */ corestacks__stack_p;

// ------------------

typedef
  stackiteratorheader /* as */ stackiterator;

struct stackiteratorheader
{
  // backreference to collection
  stack*         /* var */ Stack;
	
  stacknode*     /* var */ CurrentNode;
  
  count_t        /* var */ ItemsCount;

  listdirections /* var */ Direction;
} ;

typedef
  stackiteratorheader   /* as */ stackiterator_t;
typedef
  stackiteratorheader*  /* as */ stackiterator_p;

typedef
  stackiteratorheader   /* as */ corestacks__stackiterator_t;
typedef
  stackiteratorheader*  /* as */ corestacks__stackiterator_p;

// ------------------

/* friend */ bool /* func */ corestacks__sameitemsize
  (/* in */ stack* /* param */ AFirstStack,
  (/* in */ stack* /* param */ ASecondStack);

/* friend */ bool /* func */ corestacks__sameitemtype
  (/* in */ stack* /* param */ AFirstStack,
  (/* in */ stack* /* param */ ASecondStack);

// ------------------

stack* /* func */ corestacks__createstack
  ( noparams );

stack* /* func */ corestacks__createstackbytype
  (/* in */ typecode_t /* param */ ADataType);

stack* /* func */ corestacks__createstackrestrictcount
  (/* in */ count_t /* param */ AMaxCount);
  
stack* /* func */ corestacks__createstackbysize
  (/* in */ size_t /* param */ AItemSize);

void /* func */ corestacks__dropstack
  (/* out */ stack** /* param */ AStack);

// ------------------

bool /* func */ corestacks__underiteration
  (/* in */ stack* /* param */ AStack);

stackiterator* /* func */ corestacks__createiterator
  (/* in */ stack* /* param */ AStack);

void /* func */ corestacks__dropiterator
  (/* out */ stackiterator** /* param */ AIterator);

// ------------------

bool /* func */ corestacks__isdone
  (/* in */ const stackiterator* /* param */ AIterator);
  
bool /* func */ corestacks__trymovenext
  (/* inout */ stackiterator* /* param */ AIterator);

void /* func */ corestacks__movenext
  (/* inout */ stackiterator* /* param */ AIterator);

bool /* func */ corestacks__trygetnode
  (/* inout */ stackiterator* /* param */ AIterator
   /* out */   stacknode*     /* param */ ANode);

bool /* func */ corestacks__trygetitem
  (/* inout */ stackiterator* /* param */ AIterator
   /* out */   pointer**      /* param */ AItem);

bool /* func */ corestacks__tryreaditem
  (/* inout */ stackiterator* /* param */ AIterator
   /* out */   pointer*       /* param */ AItem);

bool /* func */ corestacks__trywriteitem
  (/* inout */ stackiterator* /* param */ AIterator
   /* in */ const  pointer*   /* param */ AItem);

void /* func */ corestacks__readitem
  (/* inout */ stackiterator* /* param */ AIterator
   /* out */   pointer*       /* param */ AItem);

void /* func */ corestacks__writeitem
  (/* inout */ stackiterator* /* param */ AIterator
   /* in */ const pointer*    /* param */ AItem);

// ------------------

count_t /* func */ corestacks__getcurrentcount
  (/* in */ const stack* /* param */ AStack);

count_t /* func */ corestacks__getmaxcount
  (/* in */ const stack* /* param */ AStack);

// ------------------

bool /* func */ corestacks__tryclear
  (/* inout */ stack* /* param */ AStack);

bool /* func */ corestacks__isempty
  (/* in */ const stack* /* param */ AStack);

bool /* func */ corestacks__isfull
  (/* in */ const stack* /* param */ AStack);

bool /* func */ corestacks__hasitems
  (/* in */ const stack* /* param */ AStack);

void /* func */ corestacks__clear
  (/* inout */ stack* /* param */ AStack);

// ------------------

bool /* func */ corestacks__trypush
  (/* inout */ stack*         /* param */ AStack
   /* in */    const pointer* /* param */ AItem);

bool /* func */ corestacks__trypop
  (/* inout */ stack*    /* param */ AStack
   /* out */   pointer** /* param */ AItem);

bool /* func */ corestacks__tryremove
  (/* inout */ stack* /* param */ AStack);

void /* func */ corestacks__push
  (/* inout */ stack*         /* param */ AStack,
   /* in */    const pointer* /* param */ AItem);

void /* func */ corestacks__pop
  (/* inout */ stack**  /* param */ AStack,
   /* out */   pointer* /* param */ AItem);

void /* func */ corestacks__remove
  (/* inout */ stack** /* param */ AStack);

// ------------------

 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ corestacks__modulename
  ( noparams );

/* override */ int /* func */ corestacks__setup
  ( noparams );

/* override */ int /* func */ corestacks__setoff
  ( noparams );

// ------------------

#endif // CORESTACKS__H

// } // namespace corestacks