/** Module: "corestacknodes.h"
 ** Descr.: "Last In, First Out, Collection Items."
 **/
 
// namespace corestacknodes {
 
// ------------------
 
#ifndef CORESTACKSNODES__H
#define CORESTACKSNODES__H
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

// opaque type
typedef
  pointer* /* as */ stacknode;

struct stacknodeheader
{
  stacknode* /* var */ Prev;
  
  pointer*   /* var */ Data;
} ;

// ------------------

/* friend */ bool /* func */ corestacknodes__arelinked
  (/* in */ const stacknode* /* param */ A,
   /* in */ const stacknode* /* param */ B);

/* friend */ bool /* func */ corestacknodes__trylink
  (/* inout */ stacknode* /* param */ A,
   /* inout */ stacknode* /* param */ B);

/* friend */ bool /* func */ corestacknode__trymovebackward
  (/* inout */ stacknode** /* param */ AStackNode);

// ------------------

/* friend */ void /* func */ corestacknodes__link
  (/* inout */ stacknode* /* param */ A,
   /* inout */ stacknode* /* param */ B);

/* friend */ void /* func */ corestacknodes__movebackward
  (/* inout */ stacknode* /* param */ AStackNode);

// ------------------

stacknode* /* func */ corestacknodes__createnode
  (/* in */ pointer* /* param */ AItem);

pointer* /* func */ corestacknodes__dropnode
  (/* out */ stacknode** /* param */ AStackNode);

// ------------------



 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ corestacknodes__modulename
  ( noparams );

/* override */ int /* func */ corestacknodes__setup
  ( noparams );

/* override */ int /* func */ corestacknodes__setoff
  ( noparams );

// ------------------
 
#endif // CORESTACKSNODES__H

// } // namespace corestacknodes