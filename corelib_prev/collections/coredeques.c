/** Module: "coredeques.c"
 ** Descr.: "Double Linked Deque Collection."
 **/

// namespace coredeques {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"

// ------------------

#include "coreiterators.h"
#include "coredequenodes.h"

// ------------------
 
#include "coredeques.h"

// ------------------

/* friend */ bool /* func */ coredeques__sameitemsize
  (/* in */ deque* /* param */ AFirstDeque,
  (/* in */ deque* /* param */ ASecondDeque)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (ADeque != NULL);
  if (Result)
  {
    dequeheader* /* var */ ThisFirstDeque = NULL;
    dequeheader* /* var */ ThisSecondDeque = NULL;
      
    ThisFirstDeque  = (dequeheader*) AFirstDeque;
    ThisSecondDeque = (dequeheader*) ASecondDeque;
    
    Result =
      (ThisFirstDeque->ItemsSize == ThisSecondDeque->ItemsSize);
  } // if

  // ---
  return Result;
} // func

/* friend */ bool /* func */ coredeques__sameitemtype
  (/* in */ deque* /* param */ AFirstDeque,
  (/* in */ deque* /* param */ ASecondDeque)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (ADeque != NULL);
  if (Result)
  {
    dequeheader* /* var */ ThisFirstDeque = NULL;
    dequeheader* /* var */ ThisSecondDeque = NULL;
      
    ThisFirstDeque  = (dequeheader*) AFirstDeque;
    ThisSecondDeque = (dequeheader*) ASecondDeque;
    
    Result =
      (ThisFirstDeque->ItemsType == ThisSecondDeque->ItemsType);
  } // if

  // ---
  return Result;
} // func

// ------------------

deque* /* func */ coredeques__createdeque
  ( noparams )
{
  deque* /* var */ Result = NULL;
  // ---
     
  dequeheader*     /* var */ ThisDeque = NULL;

  dequenodeheader* /* var */ ThisFirstDequeNode = NULL;
  dequenodeheader* /* var */ ThisSecondDequeNode = NULL;
 
  ThisDeque =
    corememory__clearallocate
      (sizeof(dequeheader));
  ThisFirstDequeNode =
    coredequenodes__createnodeempty();
  ThisSecondDequeNode =
    coredequenodes__createnodeempty();

  coredequenodes__link
    (ThisFirstDequeNode, ThisSecondDequeNode);

  ThisDeque->FirstMarker =
    corememory__transfer(&ThisFirstDequeNode);
  ThisDeque->LastMarker =
    corememory__transfer(&ThisSecondDequeNode);
 
  ThisDeque->ItemsType   = unknowntype;

  ThisDeque->ItemsSize   = 0;
  
  ThisDeque->ItemsCurrentCount = 0;
  ThisDeque->ItemsMaxCount = 0;

  ThisDeque->IteratorCount     = 0;

  Result =
    corememory__transfer(&ThisDeque);

  // ---
  return Result;
} // func

deque* /* func */ coredeques__createdequebytype
  (/* in */ typecode_t* /* param */ AItemType)
{
  deque* /* var */ Result = NULL;
  // ---
     
  dequeheader*     /* var */ ThisDeque = NULL;

  dequenodeheader* /* var */ ThisFirstDequeNode = NULL;
  dequenodeheader* /* var */ ThisSecondDequeNode = NULL;
 
  ThisDeque =
    corememory__clearallocate
      (sizeof(dequeheader));
  ThisFirstDequeNode =
    coredequenodes__createnodeempty();
  ThisSecondDequeNode =
    coredequenodes__createnodeempty();

  coredequenodes__link
    (ThisFirstDequeNode, ThisSecondDequeNode);

  ThisDeque->FirstMarker =
    corememory__transfer(&ThisFirstDequeNode);
  ThisDeque->LastMarker =
    corememory__transfer(&ThisSecondDequeNode);
 
  ThisDeque->ItemsType   = AItemType;

  ThisDeque->ItemsSize   =
    corememory__typetosize(AItemType);
   
  ThisDeque->ItemsCurrentCount = 0;
  ThisDeque->ItemsMaxCount = 0;

  ThisDeque->IteratorCount     = 0;

  Result =
    corememory__transfer(&ThisDeque);

  // ---
  return Result;
} // func

deque* /* func */ coredeques__createdequebysize
  (/* in */ size_t* /* param */ AItemSize)
{
  deque* /* var */ Result = NULL;
  // ---
     
  dequeheader*     /* var */ ThisDeque = NULL;

  dequenodeheader* /* var */ ThisFirstDequeNode = NULL;
  dequenodeheader* /* var */ ThisSecondDequeNode = NULL;
 
  ThisDeque =
    corememory__clearallocate
      (sizeof(dequeheader));
  ThisFirstDequeNode =
    coredequenodes__createnodeempty();
  ThisSecondDequeNode =
    coredequenodes__createnodeempty();

  coredequenodes__link
    (ThisFirstDequeNode, ThisSecondDequeNode);

  ThisDeque->FirstMarker =
    corememory__transfer(&ThisFirstDequeNode);
  ThisDeque->LastMarker =
    corememory__transfer(&ThisSecondDequeNode);
 
  ThisDeque->ItemsType   = unknowntype;

  ThisDeque->ItemsSize   =
    AKeySize;
  
  ThisDeque->ItemsCurrentCount = 0;
  ThisDeque->ItemsMaxCount = 0;

  ThisDeque->IteratorCount     = 0;

  Result =
    corememory__transfer(&ThisDeque);

  // ---
  return Result;
} // func

deque* /* func */ coredeques__createdequerestrictcount
  (/* in */ count_t* /* param */ AMaxCount)
{
  deque* /* var */ Result = NULL;
  // ---
     
  dequeheader*     /* var */ ThisDeque = NULL;

  dequenodeheader* /* var */ ThisFirstDequeNode = NULL;
  dequenodeheader* /* var */ ThisSecondDequeNode = NULL;
 
  ThisDeque =
    corememory__clearallocate
      (sizeof(dequeheader));
  ThisFirstDequeNode =
    coredequenodes__createnodeempty();
  ThisSecondDequeNode =
    coredequenodes__createnodeempty();

  coredequenodes__link
    (ThisFirstDequeNode, ThisSecondDequeNode);

  ThisDeque->FirstMarker =
    corememory__transfer(&ThisFirstDequeNode);
  ThisDeque->LastMarker =
    corememory__transfer(&ThisSecondDequeNode);
 
  ThisDeque->ItemsType   = unknowntype;

  ThisDeque->ItemsSize   = 0;
  
  ThisDeque->ItemsCurrentCount = 0;
  ThisDeque->ItemsMaxCount =
    AMaxCount;

  ThisDeque->IteratorCount     = 0;

  Result =
    corememory__transfer(&ThisDeque);

  // ---
  return Result;
} // func

void /* func */ coredeques__dropdeque
  (/* out */ deque** /* param */ ADeque)
{
  if (ADeque != NULL)
  {
    dequeheader*     /* var */ ThisDeque = NULL;

    dequenodeheader* /* var */ ThisFirstDequeNode = NULL;
    dequenodeheader* /* var */ ThisSecondDequeNode = NULL;

    ThisDeque =
      (dequeheader*) corememory__share(&ADeque);
 
    coredeques__clear
      (ADeque);
 
    corememory__cleardeallocate
      (ADeque, sizeof(dequeheader));
  } // if
} // func

// ------------------

bool /* func */ coredeques__underiteration
  (/* in */ deque* /* param */ ADeque)
{
  bool /* var */ Result = false;
  // ---
  
  if (ADeque != NULL)
  {
  	Result = (ADeque->IteratorCount > 0);
  } // if
     
  // ---
  return Result;
} // func

dequeiterator* /* func */ coredeques__createiteratorfwd
  (/* in */ deque* /* param */ ADeque)
{
  dequeiterator* /* var */ Result = NULL;
  // ---
     
  if (ADeque != NULL)
  {
    dequeheader*         /* var */ ThisDeque = NULL;
    dequeiteratorheader* /* var */ ThisIterator = NULL;
  	
    ThisDeque =
      (dequeheader*) corememory__share(ADeque);
 
    ThisIterator =
      corememory__clearallocate
        (sizeof(dequeiteratorheader));
 
    ThisIterator->Deque =
      ADeque;
  
    ThisIterator->CurrentNode =
      corememory__share
        (ThisDeque->FirstMarker);

    ThisIterator->ItemsMaxCount =
      ThisDeque->ItemsMaxCount;

    ThisIterator->ItemIndex =
      0;

    ThisIterator->Direction =
      dequedirections__fwd;
    
    ThisDeque->IteratorCount =
      (ThisDeque->IteratorCount + 1);

    Result =
      corememory__transfer(&ThisIterator);
  } // if
     
  // ---
  return Result;
} // func

dequeiterator* /* func */ coredeques__createiteratorback
  (/* in */ deque* /* param */ ADeque)
{
  dequeiterator* /* var */ Result = NULL;
  // ---
     
  if (ADeque != NULL)
  {
    dequeheader*         /* var */ ThisDeque = NULL;
    dequeiteratorheader* /* var */ ThisIterator = NULL;
  	
    ThisDeque =
      (dequeheader*) corememory__share(ADeque);
 
    ThisIterator =
      corememory__clearallocate
        (sizeof(dequeiteratorheader));
 
    ThisIterator->MainDeque =
      ADeque;
  
    ThisIterator->CurrentNode =
      corememory__share
        (ThisDeque->FirstMarker);

    ThisIterator->ItemsMaxCount =
      ThisDeque->ItemsMaxCount;

    ThisIterator->ItemIndex =
      ThisDeque->ItemsMaxCount;

    ThisIterator->Direction =
      dequedirections__back;
    
    ThisDeque->IteratorCount =
      (ThisDeque->IteratorCount + 1);

    Result =
      corememory__transfer(&ThisIterator);
  } // if
     
  // ---
  return Result;
} // func

void /* func */ coredeques__dropiterator
  (/* out */ dequeiterator** /* param */ AIterator)
{
  Result = (AIterator != NULL);
  if (Result)
  {
    dequeiteratorheader** /* var */ ThisIterator = NULL;
    dequeheader*          /* var */ ThisDeque =  NULL;
      
    ThisIterator = (dequeiteratorheader**) AIterator;

    ThisDeque =
      (dequeheader*) corememory__share
        (ThisIterator->MainDeque);
      
    ThisDeque->IteratorCount =
      (ThisDeque->IteratorCount - 1);

    corememory__unshare
      (&(ThisIterator->CurrentNode));

    corememory__cleardeallocate
      (ThisIterator);
      
    *AIterator = NULL;
  } // if
} // func

// ------------------

void /* func */ coredeques__assignequalityfunc
  (/* inout */ deque*    /* param */ ADeque,
   /* in */    equality /* param */ AMatchFunc)
{
  bool CanContinue = NULL;

  CanContinue = 
    (ADeque != NULL);
  if (CanContinue)
  {
    dequeheader* /* var */ ThisDeque =  NULL;

    ThisDeque =
      (dequeheader*) ADeque;
    
    ThisDeque->MatchFunc =
      AMatchFunc;
  } // if
} // func

// ------------------

bool /* func */ coredeques__isdone
  (/* in */ const dequeiterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator == NULL);
  if (!Result)
  {
    dequeiteratorheader* /* var */ ThisIterator = NULL;
    dequeheader*         /* var */ ThisDeque =  NULL;

    ThisIterator =
      (dequeiteratorheader*) corememory__share
        (AIterator);
    ThisDeque =
      (dequeheader*) corememory__share
        (ThisIterator->MainDeque);

    if (ThisIterator->Direction == dequedirections__fwd)
    {
      Result =
        (ThisIterator->CurrentNode == ThisDeque->LastMarker);
    }
    else if (ThisIterator->Direction == dequedirections__back)
    {
      Result =
        (ThisIterator->CurrentNode == ThisDeque->FirstMarker);
    }
  } // if

  // ---
  return Result;
} // func

bool /* func */ coredeques__trymovenext
  (/* inout */ dequeiterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator == NULL);
  if (!Result)
  {
    ThisIterator =
      (dequeiteratorheader*) corememory__share
        (AIterator);
    ThisDeque =
      (dequeheader*) corememory__share
        (ThisIterator->MainDeque);
  	
    if (ThisIterator->Direction == dequedirections__fwd)
    {
      Result =
        (ThisIterator->CurrentNode != ThisDeque->LastMarker);
      if (Result)
      {
        ThisIterator->ItemIndex =
          (ThisIterator->ItemIndex + 1);
        coredequenodes__movefwd
          (&(ThisIterator->CurrentNode));
      }
    }
    else if (ThisIterator->Direction == dequedirections__back)
    {
      Result =
        (ThisIterator->CurrentNode != ThisDeque->FirstMarker);
        
      if (Result)
      {
        ThisIterator->ItemIndex =
          (ThisIterator->ItemIndex - 1);

        coredequenodes__moveback
          (&(ThisIterator->CurrentNode));
      }
    }
  } // if

  // ---
  return Result;
} // func

void /* func */ coredeques__movenext
  (/* inout */ dequeiterator* /* param */ AIterator)
{
  /* discard */ coredeques__trymovenext
    (AIterator);
} // func

// ------------------

bool /* func */ coredeques__trygetnode
  (/* inout */ dequeiterator* /* param */ AIterator
   /* out */   dequenode*     /* param */ ANode)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      dequeiteratorheader* /* var */ ThisIterator = NULL;
      
      ThisIterator = (dequeiteratorheader*) AIterator;
      
      ANode =
        ThisIterator->CurrentNode;
    } // if
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coredeques__trygetitem
  (/* inout */ dequeiterator* /* param */ AIterator,
   /* out */   pointer**     /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      dequeheader*         /* var */ ThisDeque = NULL;
      dequenodeheader*     /* var */ ThisNode = NULL;
      dequeiteratorheader* /* var */ ThisIterator = NULL;
      
      ThisIterator = (dequeiteratorheader*) AIterator;
      ThisDeque = (dequeheader*) ThisIterator->MainDeque;
      ThisNode = (dequenodeheader*) ThisIterator->CurrentNode;
      	  
      *AItem =
        ThisNode->Item;
    } // if
  } // if
    
  // ---
  return Result;
} // func

bool /* func */ coredeques__tryreaditem
  (/* inout */ dequeiterator* /* param */ AIterator,
   /* out */   pointer*      /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      dequeheader*         /* var */ ThisDeque = NULL;
      dequenodeheader*     /* var */ ThisNode = NULL;
      dequeiteratorheader* /* var */ ThisIterator = NULL;
      
      ThisIterator = (dequeiteratorheader*) AIterator;
      ThisDeque = (dequeheader*) ThisIterator->MainDeque;
      ThisNode = (dequenodeheader*) ThisIterator->CurrentNode;
      	  
      corememory__safecopy
        (AItem, ThisNode->Item, ThisDeque->ItemsSize);
    } // if
  } // if
    
  // ---
  return Result;
} // func

bool /* func */ coredeques__trywriteitem
  (/* inout */ dequeiterator* /* param */ AIterator,
   /* out */   pointer*      /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      dequeheader*         /* var */ ThisDeque = NULL;
      dequenodeheader*     /* var */ ThisNode = NULL;
      dequeiteratorheader* /* var */ ThisIterator = NULL;
      
      ThisIterator = (dequeiteratorheader*) AIterator;
      ThisDeque = (dequeheader*) ThisIterator->MainDeque;
      ThisNode = (dequenodeheader*) ThisIterator->CurrentNode;
      	  
      corememory__safecopy
        (ThisNode->Item, AItem, ThisDeque->ItemsSize);
    } // if
  } // if
     
  // ---
  return Result;
} // func

dequenode* /* func */ coredeques__getnode
  (/* inout */ dequeiterator* /* param */ AIterator)
{
  dequenode* /* var */ Result = NULL;
  // ---
     
  /* discard */ coredeques__trygetnode
    (ADeque, Result);
     
  // ---
  return Result;
} // func

pointer* /* func */ coredeques__getnode
  (/* inout */ dequeiterator* /* param */ AIterator)
{
  pointer* /* var */ Result = NULL;
  // ---
     
  /* discard */ coredeques__trygetitem
    (ADeque, &Result);
     
  // ---
  return Result;
} // func

void /* func */ coredeques__readitem
  (/* inout */ dequeiterator* /* param */ AIterator
   /* out */   pointer*      /* param */ AItem)
{
  /* discard */ coredeques__tryreaditem
    (ADeque, AItem);
} // func

void /* func */ coredeques__writeitem
  (/* inout */ dequeiterator*   /* param */ AIterator
   /* in */    const  pointer* /* param */ AItem)
{
  /* discard */ coredeques__trywriteitem
    (ADeque, AItem);
} // func

// ------------------

count_t /* func */ coredeques__getcurrentcount
  (/* in */ const deque* /* param */ ADeque)
{
  count_t /* var */ Result = 0;
  // ---
  
  bool /* var */ CanContinue = false;
     
  CanContinue = (AIterator != NULL);
  if (CanContinue)
  {
    CanContinue = (AItem != NULL);
    if (CanContinue)
    {
      dequeiteratorheader** /* var */ ThisIterator = NULL;
      dequeheader*          /* var */ ThisDeque      = NULL;

      ThisIterator = (dequeiteratorheader**) AIterator;
          
      ThisDeque =
        (dequeheader*) corememory__share
          (ThisIterator->Deque);

      Result =
        ThisDeque->ItemsCurrentCount;
    } // if
  } // if
     
  // ---
  return Result;
} // func

count_t /* func */ coredeques__getmaxcount
  (/* in */ const deque* /* param */ ADeque)
{
  count_t /* var */ Result = 0;
  // ---
  
  bool /* var */ CanContinue = false;
     
  CanContinue = (AIterator != NULL);
  if (CanContinue)
  {
    CanContinue = (AItem != NULL);
    if (CanContinue)
    {
      dequeiteratorheader** /* var */ ThisIterator = NULL;
      dequeheader*          /* var */ ThisDeque      = NULL;

      ThisIterator = (dequeiteratorheader**) AIterator;
          
      ThisDeque =
        (dequeheader*) corememory__share
          (ThisIterator->Deque);

      Result =
        ThisDeque->ItemsCurrentCount;
    } // if
  } // if
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coredeques__tryclear
  (/* inout */ deque* /* param */ ADeque)
{
  bool /* var */ Result = false;
  // ---

  Result = (ADeque != NULL);
  if (Result)
  {
    dequeheader*      /* var */ ThisDeque = NULL;

    dequeiterator*    /* var */ AIterator = NULL;
    dequeiteratorheader* /* var */ ThisIterator = NULL;

    dequenode*        /* var */ ADequeNode = NULL;
    dequenodeheader*  /* var */ ThisDequeNode = NULL;
    
    pointer*          /* var */ AItem = NULL;

    ThisDeque =
      (dequeheader*) ADeque;

    // remove data first
    AIterator =
      coredeques__createfwditerator(ADeque);
    ThisIterator =
      (dequeiteratorheader*) AIterator;

    while (!coredeques__isdone(AIterator))
    {
      ADequeNode =
        ThisIterator->CurrentNode;
      ThisDequeNode = (dequenodeheader*) ADequeNode;

      AItem =
        coredequenodes__dropnode
          (ADequeNode);

      coredeques__movenext(AIterator);
    } // while

    coredeques__dropiterator(&ADeque);

    // remove nodes
    AIterator =
      coredeques__createiterator(ADeque);

    while (!coredeques__isdone(AIterator))
    {
      ADequeNode =
        ThisIterator->CurrentNode;
      ThisDequeNode = (dequenodeheader*) ADequeNode;

      coredeques__movenext(AIterator);

      /* discard */ coredequenodes__dropnode
        (&ThisDequeNode);
    } // while

    coredeques__dropiterator(&ADeque);

    // update other properties states
    coredequenodes__link
      (ThisDeque->FirstMarker, ThisDeque->LastMarker);
      
    ThisDeque->ItemsMaxCount = 0;
    ThisDeque->ItemsCurrentCount = 0;
    ThisDeque->IteratorCount = 0;
  } // if
    
  // ---
  return Result;
} // func

void /* func */ coredeques__clear
  (/* inout */ deque* /* param */ ADeque)
{
  /* discard */ coredeques__tryclear
    (ADeque);
} // func

// ------------------

bool /* func */ coredeques__isempty
  (/* in */ const deque* /* param */ ADeque)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (ADeque != NULL);
  if (Result)
  {
    dequeheader*      /* var */ ThisDeque = NULL;

    ThisDeque =
      (dequeheader*) ADeque;

    Result =
      coredequenodes__arelinked
        (ThisDeque->FirstMarker, ThisDeque->LastMarker) ||
        (ThisDeque->ItemsCurrentCount == 0);
  } // if
    
  // ---
  return Result;
} // func

bool /* func */ coredeques__isfull
  (/* in */ const deque* /* param */ ADeque)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (ADeque != NULL);
  if (Result)
  {
    dequeheader* /* var */ ThisDeque = NULL;
    
    ThisDeque = (dequeheader*) ADeque;

    Result =
      (ThisDeque->ItemsMaxCount > 0) &&
      (ThisDeque->ItemsCurrentCount == ThisDeque->ItemsMaxCount));
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coredeques__hasitems
  (/* in */ const deque* /* param */ ADeque)
{
  bool /* var */ Result = false;
  // ---
     
  Result = !(coredeques__isempty(ADeque));
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coredeques__tryinsertfirst
  (/* inout */ deque*          /* param */ ADeque,
   /* in */    const pointer* /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (ADeque != NULL) && (AItem != NULL);
  if (Result)
  {
    dequeheader*     /* var */ ThisDeque = NULL;

    dequenodeheader* /* var */ ThisBeforeNode = NULL;

    dequenode*       /* var */ ABeforeNode = NULL;
    dequenode*       /* var */ AAfterNode  = NULL;
    dequenode*       /* var */ ANewNode    = NULL;

    ANewNode =
      coredequenodes__createnode
        (AItem);

    ThisDeque = (dequeheader*) ADeque;

    ABeforeNode =
      ThisDeque->FirstMarker;
    ThisBeforeNode =
      (dequenodeheader*) ABeforeNode;

    AAfterNode =
      ThisBeforeNode->Prev;

    coredequenodes__trylink
      (ABeforeNode, ANewNode);
    coredequenodes__trylink
      (ANewNode, AAfterNode);
      
    ThisDeque->ItemsCurrentCount =
      (ThisDeque->ItemsCurrentCount + 1);
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coredeques__tryinsertlast
  (/* inout */ deque*          /* param */ ADeque,
   /* in */    const pointer* /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (ADeque != NULL) && (AItem != NULL);
  if (Result)
  {
    dequeheader*     /* var */ ThisDeque = NULL;

    dequenodeheader* /* var */ ThisAfterNode = NULL;

    dequenode*       /* var */ ABeforeNode = NULL;
    dequenode*       /* var */ AAfterNode  = NULL;
    dequenode*       /* var */ ANewNode    = NULL;

    ANewNode =
      coredequenodes__createnode
        (AItem);

    ThisDeque = (dequeheader*) ADeque;

    AAfterNode =
      ThisDeque->LastMarker;
    ThisAAfterNode =
      (dequenodeheader*) AAfterNode;

    ABeforeNode =
      ThisAAfterNode->Prev;
 
    coredequenodes__trylink
      (ABeforeNode, ANewNode);
    coredequenodes__trylink
      (ANewNode, AAfterNode);
      
    ThisDeque->ItemsCurrentCount =
      (ThisDeque->ItemsCurrentCount + 1);
  } // if
     
  // ---
  return Result;
} // func

void /* func */ coredeques__insertfirst
  (/* inout */ deque*          /* param */ ADeque,
   /* in */    const pointer* /* param */ AItem)
{
  /* discard */ coredeques__tryinsertfirst
    (ADeque, AItem);
} // func

void /* func */ coredeques__insertlast
  (/* inout */ deque*          /* param */ ADeque,
   /* in */    const pointer* /* param */ AItem)
{
  /* discard */ coredeques__tryinsertlast
    (ADeque, AItem);
} // func

// ------------------

bool /* func */ coredeques__tryextractfirst
  (/* inout */ deque*    /* param */ ADeque,
   /* out */   pointer** /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---

  Result =
    (ADeque != NULL) && (AItem != NULL);
  if (Result)
  {
    Result =
      coredeques__hasitems(ADeque);
    if (Result)
    {
      dequeheader*     /* var */ ThisDeque = NULL;

      dequenodeheader* /* var */ ThisBeforeNode = NULL;
      dequenodeheader* /* var */ ThisDropNode   = NULL;

      dequenode*       /* var */ ABeforeNode = NULL;
      dequenode*       /* var */ AAfterNode  = NULL;
      dequenode*       /* var */ ADropNode   = NULL;

      ThisDeque = (dequeheader*) ADeque;

      ABeforeNode =
        ThisDeque->FirstMarker;
      ThisBeforeNode =
        (dequenodeheader*) ABeforeNode;

      ADropNode =
        ThisBeforeNode->Prev;
      ThisDropNode =
        (dequenodeheader*) ADropNode;

      AAfterNode =
        ThisDropNode->Prev;

      coredequenodes__trylink
        (ABeforeNode, AAfterNode);

      ThisDeque->ItemsCurrentCount =
        (ThisDeque->ItemsCurrentCount - 1);
    } // if
  } // if
   
  // ---
  return Result;
} // func

bool /* func */ coredeques__tryextractlast
  (/* inout */ deque*    /* param */ ADeque,
   /* out */   pointer** /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (ADeque != NULL) && (AItem != NULL);
  if (Result)
  {
    Result =
      coredeques__hasitems(ADeque);
    if (Result)
    {
      dequeheader*     /* var */ ThisDeque = NULL;

      dequenodeheader* /* var */ ThisBeforeNode = NULL;
      dequenodeheader* /* var */ ThisDropNode   = NULL;

      dequenode*       /* var */ ABeforeNode = NULL;
      dequenode*       /* var */ AAfterNode  = NULL;
      dequenode*       /* var */ ADropNode   = NULL;

      ThisDeque = (dequeheader*) ADeque;

      AAfterNode =
        ThisDeque->FirstMarker;
      ThisAfterNode =
        (dequenodeheader*) AAfterNode;

      ADropNode =
        ThisBeforeNode->Next;
      ThisDropNode =
        (dequenodeheader*) ADropNode;

      coredequenodes__trylink
        (ABeforeNode, AAfterNode);

      ThisDeque->ItemsCurrentCount =
        (ThisDeque->ItemsCurrentCount - 1);
    } // if
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coredeques__tryremovefirst
  (/* inout */ deque* /* param */ ADeque)
{
  bool /* var */ Result = false;
  // ---

  pointer*    /* var */ AItem = NULL;
  dequeheader* /* var */ ThisDeque = NULL;

  Result =
    coredeques__tryextractfirst
      (ADeque, &AItem);
  if (Result)
  {
    ThisDeque = (dequeheader*) ADeque;
  	
    corememory__deallocate
      (&AItem, ThisDeque->ItemsSize);
  } // if

  // ---
  return Result;
} // func

bool /* func */ coredeques__tryremovelast
  (/* inout */ deque* /* param */ ADeque)
{
  bool /* var */ Result = false;
  // ---

  pointer*    /* var */ AItem = NULL;
  dequeheader* /* var */ ThisDeque = NULL;

  Result =
    coredeques__tryextractlast
      (ADeque, &AItem);
  if (Result)
  {
    ThisDeque = (dequeheader*) ADeque;
  	
    corememory__deallocate
      (&AItem, ThisDeque->ItemsSize);
  } // if

  // ---
  return Result;
} // func

pointer* /* func */ coredeques__extractfirst
  (/* inout */ deque* /* param */ ADeque)
{
  /* discard */ coredeques__tryextractfirst
    (ADeque, AItem);
} // func

pointer* /* func */ coredeques__extractlast
  (/* inout */ deque* /* param */ ADeque)
{
  pointer* /* var */ Result = NULL;
  // ---
     
  /* discard */ coredeques__tryextractlast
    (ADeque, Result);
     
  // ---
  return Result;
} // func

void /* func */ coredeques__removefirst
  (/* inout */ deque* /* param */ ADeque)
{
  /* discard */ coredeques__tryremovefirst
    (ADeque);
} // func

void /* func */ coredeques__removelast
  (/* inout */ deque* /* param */ ADeque)
{
  /* discard */ coredeques__tryremovelast
    (ADeque);
} // func

// ------------------

/* override */ const ansinullstring* /* func */ coredeques__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "coredeques";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coredeques__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coredeques__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coredeques