/** Module: "coreiterators.h"
 ** Descr.: "Definitions for traversing Data Structures."
 **/

// namespace coreiterators {
 
// ------------------
 
#ifndef COREITERATORS__H
#define COREITERATORS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

enum listdirections
{
  listdirections__none,
  listdirections__forward,
  listdirections__backward,
} ;

enum treedirections
{
  treedirections__none,
  treedirections__prefix,
  treedirections__posfix,
  treedirections__infix,
} ;

#define treedirections__suffix treedirections__posfix

// ------------------

/* override */ const ansinullstring* /* func */ coreiterators__modulename
  ( noparams );

/* override */ int /* func */ coreiterators__setup
  ( noparams );

/* override */ int /* func */ coreiterators__setoff
  ( noparams );

// ------------------
 
#endif // COREITERATORS__H

// } // namespace coreiterators