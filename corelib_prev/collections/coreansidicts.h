/** Module: "coreansidicts.h"
 ** Descr.: "Unique ANSI String Key and duplicated value,"
 **         "list (Dictionary)."
 **/
 
// namespace coreansidicts {
 
// ------------------
 
#ifndef CORECOLLANSIDICTS__H
#define CORECOLLANSIDICTS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"
#include "coreiterators.h"

// ------------------

#include "coreansikeyvalues.h"
#include "coreansidictnodes.h"

// ------------------

struct ansidictionaryheader
{
  // pointer to node before first node
  ansidictionarynode* /* var */ FirstMarker;
  
  // pointer to node before last node
  ansidictionarynode* /* var */ LastMarker;

  // always [ansinullstring]
  typecode /* var */ ItemsKeyType;

  // only applies when items are same type
  // "unknowntype" value means different type
  typecode /* var */ ItemsValueType;

  // maybe fixed, maybe variable
  size_t   /* var */ ItemsKeySize;

  // applies instead of "typecode"
  // "0" means not used
  size_t   /* var */ ItemsValueSize;

  // restricts how many items can be stored
  // "0" means not used
  count_t  /* var */ ItemsMaxCount;

  // how many items are currently stored
  count_t  /* var */ ItemsCurrentCount;
  
  // indicates iterator related objects
  count_t  /* var */ IteratorCount;
  
  equality /* var */ MatchFunc;

  stringcasemodes /* var */ CaseMode;

  // ...
} ;

typedef
  ansidictionaryheader /* as */ ansidictionary;
typedef
  ansidictionary       /* as */ ansidictionary_t;
typedef
  ansidictionary*      /* as */ ansidictionary_p;

// ------------------

ansidictionary* /* func */ coreansidicts__createdictionary
  ( noparams );

ansidictionary* /* func */ coreansidicts__createdictbytype
  (/* in */ const typecode_t /* param */ AValueType);

ansidictionary* /* func */ coreansidicts__createdictbysize
  (/* in */ size_t* /* param */ AValueSize);

void /* func */ coreansidicts__dropdictionary
  (/* out */ ansidictionary** /* param */ ADict);

// ------------------

count_t /* func */ coreansidicts__getcount
  (/* in */ const ansidictionary* /* param */ ADict);

// ------------------

bool /* func */ coreansidicts__tryclear
  (/* inout */ ansidictionary* /* param */ ADict);

bool /* func */ coreansidicts__isempty
  (/* in */ const ansidictionary* /* param */ ADict);

bool /* func */ coreansidicts__hasitems
  (/* in */ const ansidictionary* /* param */ ADict);

void /* func */ coreansidicts__clear
  (/* inout */ ansidictionary* /* param */ ADict);

// ------------------

bool /* func */ coreansidicts__trykeyfound
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey,
   /* out */   /* restricted */ bool*                 /* param */ AKeyFound);

bool /* func */ coreansidicts__tryitembykey
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey,
   /* out */   /* restricted */ pointer*              /* param */ AValue);

bool /* func */ coreansidicts__keyfound
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey);

pointer* /* func */ coreansidicts__itembykey
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey);

// ------------------

bool /* func */ coreansidicts__tryinsert
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey,
   /* in */    /* restricted */ pointer*              /* param */ AValue);

bool /* func */ coreansidicts__tryupdate
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey,
   /* in */    /* restricted */ pointer*              /* param */ AValue);

bool /* func */ coreansidicts__tryreinsert
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey,
   /* in */    /* restricted */ pointer*              /* param */ AValue);

void /* func */ coreansidicts__insert
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey,
   /* in */    /* restricted */ pointer*              /* param */ AValue);

void /* func */ coreansidicts__update
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey,
   /* in */    /* restricted */ pointer*              /* param */ AValue);

void /* func */ coreansidicts__reinsert
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey,
   /* in */    /* restricted */ pointer*              /* param */ AValue);

// ------------------

bool /* func */ coreansidicts__tryextract
  (/* inout */ /* restricted */ ansidictionary*                  /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey,
   /* out */   /* restricted */ pointer*              /* param */ AValue);

pointer* /* func */ coreansidicts__extract
  (/* inout */ /* restricted */ ansidictionary*                  /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey,
   /* out */   /* restricted */ pointer*              /* param */ AValue);

bool /* func */ coreansidicts__trydelete
  (/* inout */ /* restricted */ ansidictionary*                  /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey);

pointer* /* func */ coreansidicts__delete
  (/* inout */ /* restricted */ ansidictionary*                  /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey);

// ------------------

bool /* func */ coreansidicts__tryexchange
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AFirstKey,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASecondKey);

void /* func */ coreansidicts__exchange
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AFirstKey,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASecondKey);

// ------------------



 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ corestacks__modulename
  ( noparams );

/* override */ int /* func */ corestacks__setup
  ( noparams );

/* override */ int /* func */ corestacks__setoff
  ( noparams );

// ------------------

#endif // CORECOLLANSIDICTS__H

// } // namespace coreansidicts