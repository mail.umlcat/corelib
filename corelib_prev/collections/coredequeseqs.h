/** Module: "coredequeseqs.h"
 ** Descr.: "Duplicated items Lists."
 **/
 
// namespace coredequeseqs {
 
// ------------------
 
#ifndef COREDEQUESEQS__H
#define COREDEQUESEQS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"
#include "coredequeseqnodes.h"

// ------------------

typedef
  pointer* /* as */ dequeseq;

struct dequeseqheader
{
  // pointer to node before first node
  dequeuenode* /* var */ TopMarker;
  
  // pointer to node before last node
  dequeuenode* /* var */ BottomMarker;

  // only applies when items are same type
  // "unknowntype" value means different type
  typecode /* var */ ItemsType;

  // applies instead of "typecode"
  // "0" means not used
  size_t   /* var */ ItemsSize;

  // restricts how many items can be stored
  // "0" means not used
  count_t  /* var */ ItemsMaxCount;

  // how many items are currently stored
  count_t  /* var */ ItemsCurrentCount;
  
  // indicates iterator related objects
  count_t  /* var */ IteratorCount;
  
  // indicates wheter the set can be ordered
  bool     /* var */ WantSorted;

  areequalfunctor /* var */ MatchFunc;
  
  // ...
} ;

typedef
  dequeseqheader  /* as */ dequeseq_t;
typedef
  dequeseqheader* /* as */ dequeseq_p;
  
typedef
  dequeseqheader  /* as */ coredequeseqs__dequeseq_t;
typedef
  dequeseqheader* /* as */ coredequeseqs__dequeseq_p;

// ------------------

typedef
  pointer* /* as */ dequeseqiterator;

struct dequeseqiteratorheader
{
  // backreference to collection
  dequeseq*     /* var */ MainList

  dequeseqnode* /* var */ CurrentItem;

  count_t               /* var */ ItemsCount;
  size_t                /* var */ ItemsSize;

  listdirections        /* var */ Direction;
  
  // ...
} ;

typedef
  dequeseqiteratorheader  /* as */ dequeseqiterator_t;
typedef
  dequeseqiteratorheader* /* as */ dequeseqiterator_p;

typedef
  dequeseqiteratorheader  /* as */ coredequeseqs__dequeseqiterator_t;
typedef
  dequeseqiteratorheader* /* as */ coredequeseqs__dequeseqiterator_p;

// ------------------

/* friend */ bool /* func */ coredequeseqs__sameitemsize
  (/* in */ dequeseq* /* param */ A,
  (/* in */ dequeseq* /* param */ B);

/* friend */ bool /* func */ coredequeseqs__sameitemtype
  (/* in */ dequeseq* /* param */ A,
  (/* in */ dequeseq* /* param */ B);

// ------------------

dequeseq* /* func */ coredequeseqs__createdequeseq
  ( noparams );

dequeseq* /* func */ coredequeseqs__createdequeseqbytype
  (/* in */ typecode_t* /* param */ ADataType);

dequeseq* /* func */ coredequeseqs__createdequeseqrestrictcount
  (/* in */ count_t* /* param */ ACount);
  
dequeseq* /* func */ coredequeseqs__createdequeseqbysize
  (/* in */ size_t* /* param */ ADataSize);

void /* func */ coredequeseqs__dropdequeseq
  (/* out */ dequeseq** /* param */ AList);

// ------------------

bool /* func */ coredequeseqs__underiteration
  (/* in */ dequeseq* /* param */ AList)

dequeseqiterator* /* func */ coredequeseqs__createiteratorforward
  (/* in */ dequeseq* /* param */ AList);

dequeseqiterator* /* func */ coredequeseqs__createiteratorbackward
  (/* in */ dequeseq* /* param */ AList);

void /* func */ coredequeseqs__dropiterator
  (/* out */ dequeseqiterator** /* param */ AIterator);

// ------------------

bool /* func */ coredequeseqs__isdone
  (/* in */ const dequeseqiterator* /* param */ AIterator);

bool /* func */ coredequeseqs__trymovenext
  (/* inout */ dequeseqiterator* /* param */ AIterator);

void /* func */ coredequeseqs__movenext
  (/* inout */ dequeseqiterator* /* param */ AIterator);

bool /* func */ coredequeseqs__trygetitem
  (/* inout */ dequeseqiterator* /* param */ AIterator
   /* out */   pointer*        /* param */ AItem);

bool /* func */ coredequeseqs__trysetitem
  (/* inout */ dequeseqiterator* /* param */ AIterator
   /* in */ const pointer*     /* param */ AItem);

void /* func */ coredequeseqs__getitem
  (/* inout */ dequeseqiterator* /* param */ AIterator
   /* out */   pointer*        /* param */ AItem);

void /* func */ coredequeseqs__setitem
  (/* inout */ dequeseqiterator* /* param */ AIterator
   /* in */ const pointer*     /* param */ AItem);

// ------------------

void /* func */ coredequeseqs__assignequalityfunc
  (/* inout */ dequeseq*  /* param */ AList,
   /* in */    equality /* param */ AMatchFunc);

// ------------------

count_t /* func */ coredequeseqs__getcurrentcount
  (/* in */ const dequeseq* /* param */ AStack);

count_t /* func */ coredequeseqs__getmaxcount
  (/* in */ const dequeseq* /* param */ AStack);

// ------------------

bool /* func */ coredequeseqs__tryclear
  (/* inout */ dequeseq* /* param */ AList);

bool /* func */ coredequeseqs__isempty
  (/* in */ const dequeseq* /* param */ AList);

bool /* func */ coredequeseqs__hasitems
  (/* in */ const dequeseq* /* param */ AList);

void /* func */ coredequeseqs__clear
  (/* inout */ dequeseq* /* param */ AList);

// ------------------


 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coredequeseqs__modulename
  ( noparams );

/* override */ int /* func */ coredequeseqs__setup
  ( noparams );

/* override */ int /* func */ coredequeseqs__setoff
  ( noparams );

// ------------------

#endif // COREDEQUESEQS__H

// } // namespace coredequeseqs