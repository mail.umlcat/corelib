/** Module: "corearraylists.c"
 ** Descr.: "Double Linked Indexed List"
 **         "Data Structure."
 **/

// namespace corearraylists {
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"

// ------------------

#include "coreiterators.h"
#include <arraylistnodes.h"

// ------------------

#include "corearraylists.h"

// ------------------

/* friend */ bool /* func */ corearraylists__sameitemsize
  (/* in */ arraylist* /* param */ AFirstList,
  (/* in */ arraylist* /* param */ ASecondList)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AList != NULL);
  if (Result)
  {
    listheader* /* var */ ThisFirstList = NULL;
    listheader* /* var */ ThisSecondList = NULL;
      
    ThisFirstList  = (arraylistheader*) AFirstList;
    ThisSecondList = (arraylistheader*) ASecondList;
    
    Result =
      (ThisFirstList->ItemsSize == ThisSecondList->ItemsSize);
  } // if

  // ---
  return Result;
} // func

/* friend */ bool /* func */ corearraylists__sameitemtype
  (/* in */ arraylist* /* param */ AFirstList,
  (/* in */ arraylist* /* param */ ASecondList)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AList != NULL);
  if (Result)
  {
    arraylistheader* /* var */ ThisFirstList = NULL;
    arraylistheader* /* var */ ThisSecondList = NULL;
      
    ThisFirstList  = (arraylistheader*) AFirstList;
    ThisSecondList = (arraylistheader*) ASecondList;
    
    Result =
      (ThisFirstList->ItemsType == ThisSecondList->ItemsType);
  } // if

  // ---
  return Result;
} // func

// ------------------

arraylist* /* func */ corearraylists__createlist
  ( noparams )
{
  arraylist* /* var */ Result = NULL;
  // ---
     
  arraylistheader*     /* var */ ThisList = NULL;

  arraylistnodeheader* /* var */ ThisFirstListNode = NULL;
  arraylistnodeheader* /* var */ ThisSecondListNode = NULL;
 
  ThisList =
    corememory__clearallocate
      (sizeof(arraylistheader));
  ThisFirstListNode =
    corearraylistnodes__createnodeempty();
  ThisSecondListNode =
    corearraylistnodes__createnodeempty();

  corearraylistnodes__link
    (ThisFirstListNode, ThisSecondListNode);

  ThisList->FirstMarker =
    corememory__transfer(&ThisFirstListNode);
  ThisList->LastMarker =
    corememory__transfer(&ThisSecondListNode);
 
  ThisList->ItemsType   = unknowntype;

  ThisList->ItemsSize   = 0;
  
  ThisList->ItemsCurrentCount = 0;
  ThisList->ItemsMaxCount = 0;

  ThisList->IteratorCount     = 0;

  Result =
    corememory__transfer(&ThisList);

  // ---
  return Result;
} // func

arraylist* /* func */ corearraylists__createlistbytype
  (/* in */ typecode_t* /* param */ ADataType)
{
  arraylist* /* var */ Result = NULL;
  // ---
     
  arraylistheader*     /* var */ ThisList = NULL;

  arraylistnodeheader* /* var */ ThisFirstListNode = NULL;
  arraylistnodeheader* /* var */ ThisSecondListNode = NULL;
 
  ThisList =
    corememory__clearallocate
      (sizeof(arraylistheader));
  ThisFirstListNode =
    corearraylistnodes__createnodeempty();
  ThisSecondListNode =
    corearraylistnodes__createnodeempty();

  corearraylistnodes__link
    (ThisFirstListNode, ThisSecondListNode);

  ThisList->FirstMarker =
    corememory__transfer(&ThisFirstListNode);
  ThisList->LastMarker =
    corememory__transfer(&ThisSecondListNode);
 
  ThisList->ItemsType   = AItemType;
  ThisList->ItemsSize   =
    corememory__typetosize(AItemType);
  
  ThisList->ItemsCurrentCount = 0;
  ThisList->ItemsMaxCount = 0;

  ThisList->IteratorCount     = 0;

  Result =
    corememory__transfer(&ThisList);

  // ---
  return Result;
} // func

arraylist* /* func */ corearraylists__createlistbysize
  (/* in */ size_t* /* param */ ADataSize)
{
  arraylist* /* var */ Result = NULL;
  // ---
     
  arraylistheader*     /* var */ ThisList = NULL;

  arraylistnodeheader* /* var */ ThisFirstListNode = NULL;
  arraylistnodeheader* /* var */ ThisSecondListNode = NULL;
 
  ThisList =
    corememory__clearallocate
      (sizeof(arraylistheader));
  ThisFirstListNode =
    corearraylistnodes__createnodeempty();
  ThisSecondListNode =
    corearraylistnodes__createnodeempty();

  corearraylistnodes__link
    (ThisFirstListNode, ThisSecondListNode);

  ThisList->FirstMarker =
    corememory__transfer(&ThisFirstListNode);
  ThisList->LastMarker =
    corememory__transfer(&ThisSecondListNode);
 
  ThisList->ItemsType   = unknowntype;
  ThisList->ItemsSize   =
    AKeySize;

  ThisList->ItemsCurrentCount = 0;
  ThisList->ItemsMaxCount = 0;

  ThisList->IteratorCount     = 0;

  Result =
    corememory__transfer(&ThisList);

  // ---
  return Result;
} // func

arraylist* /* func */ corearraylists__createlistrestrictcount
  (/* in */ count_t* /* param */ ACount)
{
  arraylist* /* var */ Result = NULL;
  // ---
     
  arraylistheader*     /* var */ ThisList = NULL;

  arraylistnodeheader* /* var */ ThisFirstListNode = NULL;
  arraylistnodeheader* /* var */ ThisSecondListNode = NULL;
 
  ThisList =
    corememory__clearallocate
      (sizeof(listheader));
  ThisFirstListNode =
    corearraylistnodes__createnodeempty();
  ThisSecondListNode =
    corearraylistnodes__createnodeempty();

  corearraylistnodes__link
    (ThisFirstListNode, ThisSecondListNode);

  ThisList->FirstMarker =
    corememory__transfer(&ThisFirstListNode);
  ThisList->LastMarker =
    corememory__transfer(&ThisSecondListNode);
 
  ThisList->ItemsType   = unknowntype;
  ThisList->ItemsSize   = 0;
  
  ThisList->ItemsCurrentCount = 0;
  ThisList->ItemsMaxCount =
    AMaxCount;

  ThisList->IteratorCount     = 0;

  Result =
    corememory__transfer(&ThisList);

  // ---
  return Result;
} // func

void /* func */ corearraylists__droplist
  (/* out */ arraylist** /* param */ AList)
{
  if (AList != NULL)
  {
    arraylistheader*     /* var */ ThisList = NULL;

    arraylistnodeheader* /* var */ ThisFirstListNode = NULL;
    arraylistnodeheader* /* var */ ThisSecondListNode = NULL;

    ThisList =
      (arraylistheader*) corememory__share(&AList);
 
    corearraylists__clear
      (AList);
 
    corememory__cleardeallocate
      (AList, sizeof(arraylistheader));
  } // if
} // func

// ------------------

bool /* func */ corearraylists__isdone
  (/* in */ const arraylistiterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
  
  if (AList != NULL)
  {
  	Result = (AList->IteratorCount > 0);
  } // if
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ corearraylists__underiteration
  (/* in */ arraylist* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
  
  if (AList != NULL)
  {
  	Result = (AList->IteratorCount > 0);
  } // if
     
  // ---
  return Result;
} // func

arraylistiterator* /* func */ corearraylists__createiteratorfwd
  (/* in */ arraylist* /* param */ AList)
{
  arraylistiterator* /* var */ Result = NULL;
  // ---
     
  if (AList != NULL)
  {
    arraylistheader*         /* var */ ThisList = NULL;
    arraylistiteratorheader* /* var */ ThisIterator = NULL;
  	
    ThisList =
      (arraylistheader*) corememory__share(AList);
 
    ThisIterator =
      corememory__clearallocate
        (sizeof(arraylistiteratorheader));
 
    ThisIterator->List =
      AList;
  
    ThisIterator->CurrentItem =
      corememory__share
        (ThisList->FirstMarker);

    ThisIterator->ItemsMaxCount =
      ThisList->ItemsMaxCount;

    ThisIterator->ItemIndex =
      0;

    ThisIterator->Direction =
      listdirections__fwd;
    
    ThisList->IteratorCount =
      (ThisList->IteratorCount + 1);

    Result =
      corememory__transfer(&ThisIterator);
  } // if
     
  // ---
  return Result;
} // func

arraylistiterator* /* func */ corearraylists__createiteratorback
  (/* in */ arraylist* /* param */ AList)
{
  arraylistiterator* /* var */ Result = NULL;
  // ---
     
  if (AList != NULL)
  {
    arraylistheader*         /* var */ ThisList = NULL;
    arraylistiteratorheader* /* var */ ThisIterator = NULL;
  	
    ThisList =
      (arraylistheader*) corememory__share(AList);
 
    ThisIterator =
      corememory__clearallocate
        (sizeof(arraylistiteratorheader));
 
    ThisIterator->MainList =
      AList;
  
    ThisIterator->CurrentItem =
      corememory__share
        (ThisList->FirstMarker);

    ThisIterator->ItemsMaxCount =
      ThisList->ItemsMaxCount;

    ThisIterator->ItemIndex =
      ThisList->ItemsMaxCount;

    ThisIterator->Direction =
      listdirections__back;
    
    ThisList->IteratorCount =
      (ThisList->IteratorCount + 1);

    Result =
      corememory__transfer(&ThisIterator);
  } // if
     
  // ---
  return Result;
} // func

void /* func */ corearraylists__dropiterator
  (/* out */ arraylistiterator** /* param */ AIterator)
{
  Result = (AIterator != NULL);
  if (Result)
  {
    listiteratorheader** /* var */ ThisIterator = NULL;
    listheader*          /* var */ ThisList =  NULL;
      
    ThisIterator = (listiteratorheader**) AIterator;

    ThisList =
      (listheader*) corememory__share
        (ThisIterator->MainList);
      
    ThisList->IteratorCount =
      (ThisList->IteratorCount - 1);

    corememory__unshare
      (&(ThisIterator->CurrentItem));

    corememory__cleardeallocate
      (ThisIterator);
      
    *AIterator = NULL;
  } // if
} // func

// ------------------

void /* func */ corearraylists__assignequalityfunc
  (/* inout */ arraylist* /* param */ AList,
   /* in */    equality   /* param */ AMatchFunc)
{
  bool CanContinue = NULL;

  CanContinue = 
    (AList != NULL);
  if (CanContinue)
  {
    arraylistheader* /* var */ ThisList =  NULL;

    ThisList =
      (arraylistheader*) AList;
    
    ThisList->MatchFunc =
      AMatchFunc;
  } // if
} // func

// ------------------

bool /* func */ corearraylists__isdone
  (/* in */ const arraylistiterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator == NULL);
  if (!Result)
  {
    arraylistiteratorheader* /* var */ ThisIterator = NULL;
    arraylistheader*         /* var */ ThisList =  NULL;

    ThisIterator =
      (arraylistiteratorheader*) corememory__share
        (AIterator);
    ThisList =
      (arraylistheader*) corememory__share
        (ThisIterator->MainList);

    if (ThisIterator->Direction == listdirections__fwd)
    {
      Result =
        (ThisIterator->CurrentItem == ThisList->LastMarker);
    }
    else if (ThisIterator->Direction == listdirections__back)
    {
      Result =
        (ThisIterator->CurrentItem == ThisList->FirstMarker);
    }
  } // if

  // ---
  return Result;
} // func

bool /* func */ corearraylists__trymovenext
  (/* inout */ arraylistiterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator == NULL);
  if (!Result)
  {
    ThisIterator =
      (arraylistiteratorheader*) corememory__share
        (AIterator);
    ThisList =
      (arraylistheader*) corememory__share
        (ThisIterator->MainList);
  	
    if (ThisIterator->Direction == listdirections__fwd)
    {
      Result =
        (ThisIterator->CurrentItem != ThisList->LastMarker);
      if (Result)
      {
        ThisIterator->ItemIndex =
          (ThisIterator->ItemIndex + 1);
        corearraylistnodes__movefwd
          (&(ThisIterator->CurrentItem));
      }
    }
    else if (ThisIterator->Direction == listdirections__back)
    {
      Result =
        (ThisIterator->CurrentItem != ThisList->FirstMarker);
        
      if (Result)
      {
        ThisIterator->ItemIndex =
          (ThisIterator->ItemIndex - 1);

        corearraylistnodes__moveback
          (&(ThisIterator->CurrentItem));
      }
    }
  } // if

  // ---
  return Result;
} // func

void /* func */ corearraylists__movenext
  (/* inout */ arraylistiterator* /* param */ AIterator)
{
  /* discard */ corearraylists__trymovenext
    (AIterator);
} // func

// ------------------

bool /* func */ corearraylists__trygetnode
  (/* inout */ arraylistiterator* /* param */ AIterator
   /* out */   arraylistnode*     /* param */ ANode)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      arraylistiteratorheader* /* var */ ThisIterator = NULL;
      
      ThisIterator = (arraylistiteratorheader*) AIterator;
      
      ANode =
        ThisIterator->CurrentNode;
    } // if
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corearraylists__trygetitem
  (/* inout */ arraylistiterator* /* param */ AIterator
   /* out */   pointer**          /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      arraylistheader*         /* var */ ThisList = NULL;
      arraylistnodeheader*     /* var */ ThisNode = NULL;
      arraylistiteratorheader* /* var */ ThisIterator = NULL;
      
      ThisIterator = (arraylistiteratorheader*) AIterator;
      ThisList = (arraylistheader*) ThisIterator->MainList;
      ThisNode = (arraylistnodeheader*) ThisIterator->CurrentNode;
      	 
      *AItem =
        ThisNode->Item;
    } // if
  } // if
    
  // ---
  return Result;
} // func

bool /* func */ corearraylists__tryreaditem
  (/* inout */ arraylistiterator* /* param */ AIterator
   /* out */   pointer*           /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      arraylistheader*         /* var */ ThisList = NULL;
      arraylistnodeheader*     /* var */ ThisNode = NULL;
      arraylistiteratorheader* /* var */ ThisIterator = NULL;
      
      ThisIterator = (arraylistiteratorheader*) AIterator;
      ThisList = (arraylistheader*) ThisIterator->MainList;
      ThisNode = (arraylistnodeheader*) ThisIterator->CurrentNode;
      	  
      corememory__safecopy
        (AItem, ThisNode->Item, ThisList->ItemsSize);
    } // if
  } // if
    
  // ---
  return Result;
} // func

bool /* func */ corearraylists__trywriteitem
  (/* inout */ arraylistiterator* /* param */ AIterator
   /* out */   pointer*           /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      arraylistheader*         /* var */ ThisList = NULL;
      arraylistnodeheader*     /* var */ ThisNode = NULL;
      arraylistiteratorheader* /* var */ ThisIterator = NULL;
      
      ThisIterator = (arraylistiteratorheader*) AIterator;
      ThisList = (arraylistheader*) ThisIterator->MainList;
      ThisNode = (arraylistnodeheader*) ThisIterator->CurrentNode;
      	  
      corememory__safecopy
        (ThisNode->Item, AItem, ThisList->ItemsSize);
    } // if
  } // if
     
  // ---
  return Result;
} // func

arraylistnode* /* func */ corearraylists__getnode
  (/* inout */ arraylistiterator* /* param */ AIterator)
{
  arraylistnode* /* var */ Result = NULL;
  // ---
     
  /* discard */ corearraylists__trygetnode
    (AList, Result);
     
  // ---
  return Result;
} // func

pointer* /* func */ corearraylists__getitem
  (/* inout */ arraylistiterator* /* param */ AIterator)
{
  pointer* /* var */ Result = NULL;
  // ---
     
  /* discard */ corearraylists__trygetitem
    (AList, &Result);
     
  // ---
  return Result;
} // func

void /* func */ corearraylists__readitem
  (/* inout */ arraylistiterator* /* param */ AIterator
   /* out */   pointer*           /* param */ AItem)
{
  /* discard */ corearraylists__tryreaditem
    (AList, AItem);
} // func

void /* func */ corearraylists__writeitem
  (/* inout */ arraylistiterator* /* param */ AIterator
   /* in */    const  pointer*    /* param */ AItem)
{
  /* discard */ corearraylists__trywriteitem
    (AList, AItem);
} // func

// ------------------

void /* func */ corearraylists__assignequalityfunc
  (/* inout */ arraylist* /* param */ AList,
   /* in */    equality   /* param */ AMatchFunc)
{
  bool CanContinue = NULL;

  CanContinue = 
    (AList != NULL);
  if (CanContinue)
  {
    arraylistheader* /* var */ ThisList =  NULL;

    ThisList =
      (arraylistheader*) AList;
    
    ThisList->MatchFunc =
      AMatchFunc;
  } // if
} // func

// ------------------

count_t /* func */ corearraylists__getcurrentcount
  (/* in */ const arraylist* /* param */ AList);
{
  count_t /* var */ Result = 0;
  // ---
  
  bool /* var */ CanContinue = false;
     
  CanContinue = (AIterator != NULL);
  if (CanContinue)
  {
    CanContinue = (AItem != NULL);
    if (CanContinue)
    {
      arraylistiteratorheader** /* var */ ThisIterator = NULL;
      arraylistheader*          /* var */ ThisList      = NULL;

      ThisIterator = (arraylistiteratorheader**) AIterator;
          
      ThisList =
        (arraylistheader*) corememory__share
          (ThisIterator->List);

      Result =
        ThisList->ItemsCurrentCount;
    } // if
  } // if
     
  // ---
  return Result;
} // func

count_t /* func */ corearraylists__getmaxcount
  (/* in */ const arraylist* /* param */ AList);
{
  count_t /* var */ Result = 0;
  // ---
  
  bool /* var */ CanContinue = false;
     
  CanContinue = (AIterator != NULL);
  if (CanContinue)
  {
    CanContinue = (AItem != NULL);
    if (CanContinue)
    {
      arraylistiteratorheader** /* var */ ThisIterator = NULL;
      arraylistheader*          /* var */ ThisList      = NULL;

      ThisIterator = (arraylistiteratorheader**) AIterator;
          
      ThisList =
        (arraylistheader*) corememory__share
          (ThisIterator->List);

      Result =
        ThisList->ItemsCurrentCount;
    } // if
  } // if
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ corearraylists__tryreindex
  (/* inout */ arraylist* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
  
  Result = (AList)
  if (Result)
  {
    arraylistiterator*       /* var */ AIterator = NULL;
    arraylistiteratorheader* /* var */ ThisIterator = NULL;

    arraylistheader* /* var */ ThisList = NULL;

    arraylistnode*       /* var */ ANode = NULL;
    arraylistnodeheader* /* var */ ThisNode = NULL;

    index_t /* var */ ThisIndex = 0;

    AIterator =
      corearraylists__createiteratorfwd
        (AList);
    
    while (! corearraylists__isdone(AIterator))
    {
      ANode =
        corearraylists__getnode
          (AIterator);
      ThisNode = (arraylistnodeheader*) ANode;

      ThisNode->Index = ThisIndex;
      
      ThisIndex++;

      corearraylists__movenext
        (AList);	
    } // while
    
    corearraylists__droplist
      (AList);	
  } // if
  
  // ---
  return Result;
} // func

void /* func */ corearraylists__reindex
  (/* inout */ arraylist* /* param */ AList)
{
  /* discard */ corearraylists__tryreindex
    (AList);
} // func

// ------------------

bool /* func */ corearraylists__isempty
  (/* in */ const arraylist* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AList != NULL);
  if (Result)
  {
    arraylistheader* /* var */ ThisList = NULL;

    ThisList =
      (arraylistheader*) AList;

    Result =
      corearraylistnodes__arelinked
        (ThisList->FirstMarker, ThisList->LastMarker) ||
        (ThisList->ItemsCurrentCount == 0);
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corearraylists__isfull
  (/* in */ const arraylist* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (AList != NULL);
  if (Result)
  {
    arraylistheader* /* var */ ThisList = NULL;
    
    ThisList = (arraylistheader*) AList;

    Result =
      (ThisList->ItemsMaxCount > 0) &&
      (ThisList->ItemsCurrentCount == ThisList->ItemsMaxCount));
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corearraylists__hasitems
  (/* in */ const arraylist* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---
     
  Result = !(corearraylists__isempty(AList));

  // ---
  return Result;
} // func

// ------------------

bool /* func */ corearraylists__tryclear
  (/* inout */ arraylist* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---

  Result = (AList != NULL);
  if (Result)
  {
    arraylistiterator*       /* var */ AIterator = NULL;
    arraylistiteratorheader* /* var */ ThisIterator = NULL;

    arraylistnode*        /* var */ AListNode = NULL;
    arraylistnodeheader*  /* var */ ThisListNode = NULL;

    pointer*              /* var */ AItem = NULL;

    ThisList =
      (arraylistheader*) AList;

    // remove data first
    AIterator =
      corearraylists__createiteratorfwd(AList);
    ThisIterator =
      (arraylistiteratorheader*) AIterator;

    while (!corearraylists__isdone(AIterator))
    {
      AListNode =
        ThisIterator->CurrentNode;
      ThisListNode = (arraylistnodeheader*) AListNode;

      AItem =
        corememory__transfer(&(ThisListNode->Item));

      corememory__cleardealloc
        (AItem, ThisList->ItemsSize);

      corearraylists__movenext(AIterator);
    } // while

    corearraylists__dropiterator(&AList);

    // remove nodes
    AIterator =
      corearraylists__createiterator(AList);
      
    while (!corearraylists__isdone(AIterator))
    {
      AListNode =
        ThisIterator->CurrentNode;
      ThisListNode = (arraylistnodeheader*) AListNode;

      corearraylists__movenext(AIterator);

      /* discard */ corearraylistnodes__dropnode
        (&ThisListNode);
    } // while

    corearraylists__dropiterator(&AList);

    // update other properties states
    corearraylistnodes__link
      (ThisList->FirstMarker, ThisList->LastMarker);
      
    ThisList->ItemsMaxCount = 0;
    ThisList->ItemsCurrentCount = 0;
    ThisList->IteratorCount = 0;
  } // if
    
  // ---
  return Result;
} // func

void /* func */ corearraylists__clear
  (/* inout */ list* /* param */ AList)
{
  /* discard */ corearraylists__tryclear
    (AList);
} // func

// ------------------

bool /* func */ corearraylists__isvalidindex
  (/* in */ const arraylist* /* param */ AList
   /* in */ const index_t    /* param */ AIndex)
{
  bool /* var */ Result = FALSE;
  // ---
     
  if (AList != NULL)
  {
    arrayheader* /* var */ ThisList = NULL;
    ThisList = (arrayheader*) AList;
  	
    Result =
      (AIndex >= 0) &&
      (AIndex < ThisList->ItemsMaxCount);
  } // if
 
  // ---
  return Result;
} // func

bool /* func */ corearraylists__trygetitemat
  (/* in */  const arraylist* /* param */ AList,
   /* in */  const index_t    /* param */ AIndex,
   /* out */ pointer**        /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---

  Result =
    (AList != NULL) &&
    (AItem != NULL) &&
    corearraylists__isvalidindex
      (AList, AIndex);
  if (Result)
  {
    arraylistnode*       /* var */ ANode = NULL;
    arraylistnodeheader* /* var */ ThisNode = NULL;
 	
    Result = corearraylists__tryseeknodeatfwd
      (AList, AIndex, &ANode);
    if (Result)
    {
      ThisNode =
        (arraylistnodeheader*) ANode;
      
      *AItem =
        ThisNode->Item;
     } // if
   } // if
    
  // ---
  return Result;
} // func

bool /* func */ corearraylists__tryreaditemat
  (/* in */  const arraylist* /* param */ AList,
   /* in */  const index_t    /* param */ AIndex,
   /* out */ pointer*         /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (AList != NULL) &&
    (AItem != NULL) &&
    corearraylists__isvalidindex
      (AList, AIndex);
  if (Result)
  {
    arraylistnode*       /* var */ ANode = NULL;
    arraylistnodeheader* /* var */ ThisNode = NULL;
    
    arraylistheader*     /* var */ ThisList = NULL;
  	
    Result = corearraylists__tryseeknodeatfwd
      (AList, AIndex, &ANode);
    if (Result)
    {
      ThisNode =
        (arraylistnodeheader*) ANode;
      
      ThisList =
        (arraylistheader*) AList;
        
      corememory__safecopy
        (AItem, ThisNode->Item, ThisList->ItemsSize);
     } // if
   } // if
    
  // ---
  return Result;
} // func

bool /* func */ corearraylists__trywriteitemat
  (/* in */ const arraylist* /* param */ AList,
   /* in */ const index_t    /* param */ AIndex)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (AList != NULL) &&
    (AItem != NULL) &&
    corearraylists__isvalidindex
      (AList, AIndex);
  if (Result)
  {
    arraylistnode*       /* var */ ANode = NULL;
    arraylistnodeheader* /* var */ ThisNode = NULL;
    
    arraylistheader*     /* var */ ThisList = NULL;
  	
    Result = corearraylists__tryseeknodeatfwd
      (AList, AIndex, &ANode);
    if (Result)
    {
      ThisNode =
        (arraylistnodeheader*) ANode;
      
      ThisList =
        (arraylistheader*) AList;
        
      corememory__safecopy
        (ThisNode->Item, AItem, ThisList->ItemsSize);
     } // if
   } // if
    
  // ---
  return Result;
} // func

pointer* /* func */ corearraylists__getitemat
  (/* in */ /* restricted */ const arraylist* /* param */ AList,
   /* in */ /* restricted */ const index_t    /* param */ AIndex)
{
  pointer* /* var */ Result = false;
  // ---

  /* discard */ corearraylists__trygetitemat
    (AList, &Result, AItem);

  // ---
  return Result;
} // func

void /* func */ corearraylists__readitemat
  (/* in */  const arraylist* /* param */ AList,
   /* in */  const index_t    /* param */ AIndex,
   /* out */ pointer*         /* param */ AItem)
{
  /* discard */ corearraylists__tryreaditemat
    (AList, AIndex, AItem);
} // func

void /* func */ corearraylists__writeitemat
  (/* in */ const arraylist* /* param */ AList,
   /* in */ const index_t    /* param */ AIndex,
   /* in */ pointer*         /* param */ AItem)
{
  /* discard */ corearraylists__trywriteitemat
    (AList, AIndex, AItem);
} // func

// ------------------

bool /* func */ corearraylists__tryseeknodeatfwd
  (/* in  */ const arraylist*      /* param */ AList,
   /* in  */ const index_t         /* param */ AIndex,
   /* out */ const arraylistnode** /* param */ ADestNode)
{
  bool /* var */ Result = false;
  // ---

  Result =
    (AList != NULL) &&
    (ANode != NULL) &&
     corearraylists__isvalidindex(AIndex);
  if (Result)
  {
    arraylistiterator*       /* var */ AIterator = NULL;
    arraylistiteratorheader* /* var */ ThisIterator = NULL;

    arraylistnode*        /* var */ AListNode = NULL;
    arraylistnodeheader*  /* var */ ThisListNode = NULL;

    AIterator =
      corearraylists__createiteratorfwd(AList);
    ThisIterator =
      (arraylistiteratorheader*) AIterator;

    while 
      (!corearraylists__isdone(AIterator) &&
        !Result)
    {
      Result =
        (ThisListNode->Index == AIndex);
      if (Result)
      {
        *ADestNode = 
          ThisListNode;
      } // if

      corearraylists__movenext(AIterator);
    } // while

    corearraylists__dropiterator(&AIterator);
  } // if
 
  // ---
  return Result;
} // func

bool /* func */ corearraylists__tryseeknodeatbwd
  (/* in  */ const arraylist*      /* param */ AList,
   /* in  */ const index_t         /* param */ AIndex,
   /* out */ const arraylistnode** /* param */ ADestNode)
{
  bool /* var */ Result = false;
  // ---

  *ANode = NULL;

  Result =
    (AList != NULL) &&
    (ANode != NULL) &&
     corearraylists__isvalidindex(AIndex);
  if (Result)
  {
    arraylistiterator*       /* var */ AIterator = NULL;
    arraylistiteratorheader* /* var */ ThisIterator = NULL;

    arraylistnode*        /* var */ AListNode = NULL;
    arraylistnodeheader*  /* var */ ThisListNode = NULL;
	
    AIterator =
      corearraylists__createiteratorback(AList);
    ThisIterator =
      (arraylistiteratorheader*) AIterator;

    while 
      (!corearraylists__isdone(AIterator) &&
        !Result)
    {
      Result =
        (ThisListNode->Index == AIndex);
      if (Result)
      {
        *ADestNode = 
          ThisListNode;
      } // if

      corearraylists__movenext(AIterator);
    } // while

    corearraylists__dropiterator(&AIterator);
  } // if
 
  // ---
  return Result;
} // func

arraylistnode* /* func */ corearraylists__seeknodeatfwd
  (/* in */ const arraylist* /* param */ AList,
   /* in */ const index_t    /* param */ AIndex)
{
  arraylistnode* /* var */ Result = NULL;
  // ---

  /* discard */ corearraylists__tryseeknodeatfwd
    (AList, AIndex, &Result);
  
  // ---
  return Result;
} // func

arraylistnode* /* func */ corearraylists__seeknodeatbwd
  (/* in */ const arraylist* /* param */ AList,
   /* in */ const index_t    /* param */ AIndex)
{
  arraylistnode* /* var */ Result = NULL;
  // ---

  /* discard */ corearraylists__tryseeknodeatbwd
    (AList, AIndex, &Result);
  
  // ---
  return Result;
} // func

arraylistnode* /* func */ corearraylists__firstnodethat
  (/* in */ const arraylist* /* param */ AList,
   /* in */ matchesfunctor   /* param */ MatchesFunc,
   /* in */ pointer*         /* param */ AData)
{
  arraylistnode* /* var */ Result = NULL;
  // ---

  bool /* var */ CanContinue = false;
  bool /* var */ Found = false;
  
  CanContinue =
    (
      (AList != NULL) &&
      (MatchesFunc != NULL)
    );
 
  if (CanContinue)
  {
    arraylistiterator*       /* var */ AIterator = NULL;
    arraylistiteratorheader* /* var */ ThisIterator = NULL;

    arraylistnode*        /* var */ AListNode = NULL;
    arraylistnodeheader*  /* var */ ThisListNode = NULL;
  	
    AIterator =
      corearraylists__createiteratorfwd(AList);
    ThisIterator =
      (arraylistiteratorheader*) AIterator;

    while 
      (!corearraylists__isdone(AIterator) &&
        !Found)
    {
      AListNode =
        ThisIterator->CurrentNode;
      ThisListNode = (arraylistnodeheader*) AListNode;

      Found =
        MatchesFunc
          (ThisListNode->Item, AData, NULL);
      if (Found)
      {
        Result = 
          ThisListNode;
      } // if

      corearraylists__movenext(AIterator);
    } // while

    corearraylists__dropiterator(&AList);
  } // if

  // ---
  return Result;
} // func

arraylistnode* /* func */ corearraylists__lastnodethat
  (/* in */ const arraylist* /* param */ AList,
   /* in */ matchesfunctor   /* param */ MatchesFunc,
   /* in */ pointer*         /* param */ AData)
{
  arraylistnode* /* var */ Result = NULL;
  // ---

  bool /* var */ CanContinue = false;
  bool /* var */ Found = false;
  
  CanContinue =
    (
      (AList != NULL) &&
      (MatchesFunc != NULL)
    );
 
  if (CanContinue)
  {
    arraylistiterator*       /* var */ AIterator = NULL;
    arraylistiteratorheader* /* var */ ThisIterator = NULL;

    arraylistnode*        /* var */ AListNode = NULL;
    arraylistnodeheader*  /* var */ ThisListNode = NULL;
  	
    AIterator =
      corearraylists__createiteratorback(AList);
    ThisIterator =
      (arraylistiteratorheader*) AIterator;

    while 
      (!corearraylists__isdone(AIterator) &&
        !Found)
    {
      AListNode =
        ThisIterator->CurrentNode;
      ThisListNode = (arraylistnodeheader*) AListNode;

      Found =
        MatchesFunc
          (ThisListNode->Item, AData, NULL);
      if (Found)
      {
        Result = 
          ThisListNode;
      } // if

      corearraylists__movenext(AIterator);
    } // while

    corearraylists__dropiterator(&AList);
  } // if

  // ---
  return Result;
} // func

// ------------------

bool /* func */ corearraylists__tryinsertfirst
  (/* inout */ arraylist*      /* param */ AList,
   /* in */    const pointer*  /* param */ AItem,
   /* out */   arraylistnode** /* param */ ANewNode)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (AList != NULL) && (AItem != NULL);
  if (Result)
  {
    arraylistheader*     /* var */ ThisList = NULL;

    arraylistnodeheader* /* var */ ThisBeforeNode = NULL;

    arraylistnode*       /* var */ ABeforeNode = NULL;
    arraylistnode*       /* var */ AAfterNode  = NULL;
    arraylistnode*       /* var */ AInsertNode = NULL;

    AInsertNode =
      corearraylistnodes__createnode
        (AItem);

    ThisList = (arraylistheader*) AList;

    ABeforeNode =
      ThisList->FirstMarker;
    ThisBeforeNode =
      (arraylistnodeheader*) ABeforeNode;

    AAfterNode =
      ThisBeforeNode->Prev;

    corearraylistnodes__trylink
      (ABeforeNode, AInsertNode);
    corearraylistnodes__trylink
      (AInsertNode, AAfterNode);
      
    ThisList->ItemsCurrentCount =
      (ThisList->ItemsCurrentCount + 1);
        
    *ANewNode =
      corememory__transfer
        (&AInsertNode);
  } // if
    
  // ---
  return Result;
} // func

bool /* func */ corearraylists__tryinsertlast
  (/* inout */ arraylist*      /* param */ AList,
   /* in */    const pointer*  /* param */ AItem,
   /* out */   arraylistnode** /* param */ ANewNode)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (AList != NULL) && (AItem != NULL);
  if (Result)
  {
    arraylistheader*     /* var */ ThisList = NULL;

    arraylistnodeheader* /* var */ ThisAfterNode = NULL;

    arraylistnode*       /* var */ ABeforeNode = NULL;
    arraylistnode*       /* var */ AAfterNode  = NULL;
    arraylistnode*       /* var */ AInsertNode = NULL;

    AInsertNode =
      corearraylistnodes__createnode
        (AItem);

    ThisList = (arraylistheader*) AList;

    AAfterNode =
      ThisList->LastMarker;
    ThisAAfterNode =
      (arraylistnodeheader*) AAfterNode;

    ABeforeNode =
      ThisAAfterNode->Prev;
 
    corearraylistnodes__trylink
      (ABeforeNode, AInsertNode);
    corearraylistnodes__trylink
      (AInsertNode, AAfterNode);
      
    ThisList->ItemsCurrentCount =
      (ThisList->ItemsCurrentCount + 1);
    
    *ANewNode =
      corememory__transfer
        (&AInsertNode);
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corearraylists__tryinsertat
  (/* inout */ arraylist*      /* param */ AList,
   /* in */    const index_t   /* param */ AIndex,
   /* in */    const pointer*  /* param */ AItem,
   /* out */   arraylistnode** /* param */ ANewNode)
{
  bool /* var */ Result = false;
  // ---

  Result =
    (AList != NULL) &&
    (AItem != NULL) &&
    corearraylists__isvalidindex(AIndex);
  if (Result)
  {
    arraylistheader*     /* var */ ThisList = NULL;

    arraylistnodeheader* /* var */ ThisAfterNode = NULL;

    arraylistnode*       /* var */ ABeforeNode = NULL;
    arraylistnode*       /* var */ AAfterNode  = NULL;
    arraylistnode*       /* var */ AInsertNode = NULL;

    arraylistheadernode* /* var */ ThisAfterNode  = NULL;
    arraylistheadernode* /* var */ ThisBeforeNode = NULL;

    Result =
      corearraylists__tryseeknodeatfwd
        (AList, AIndex, &AAfterNode);
    if (Result)
    {
      ThisAfterNode =
        (arraylistheadernode*) AAfterNode;
      ABeforeNode =
        ThisAfterNode->Prev;
      ThisBeforeNode =
        (arraylistheadernode*) ABeforeNode;
      
      AInsertNode =
        corearraylistnodes__createnode
          (AItem);
          
      corearraylistnodes__trylink
        (ABeforeNode, AInsertNode);
      corearraylistnodes__trylink
        (AInsertNode, AAfterNode);
      
      ThisList->ItemsCurrentCount =
        (ThisList->ItemsCurrentCount + 1);

      corearraylists__reindex
        (AList);
    
    *ANewNode =
      corememory__transfer
        (&AInsertNode);
  } // if
  
  // ---
  return Result;
} // func

arraylistnode* /* func */ corearraylists__insertfirst
  (/* inout */ arraylist*     /* param */ AList,
   /* in */    const pointer* /* param */ AItem)
{
  arraylistnode* /* var */ Result = NULL;
  // ---

  /* discard */ corearraylists__tryinsertfirst
    (AList, AItem, &Result);
     
  // ---
  return Result;
} // func

arraylistnode* /* func */ corearraylists__insertlast
  (/* inout */ arraylist*     /* param */ AList,
   /* in */    const pointer* /* param */ AItem)
{
  arraylistnode* /* var */ Result = NULL;
  // ---

  /* discard */ corearraylists__tryinsertlast
    (AList, AItem, &Result);
     
  // ---
  return Result;
} // func

arraylistnode* /* func */ corearraylists__insertat
  (/* inout */ arraylist*     /* param */ AList,
   /* in */    const index_t  /* param */ AIndex,
   /* in */    const pointer* /* param */ AItem)
{
  arraylistnode* /* var */ Result = NULL;
  // ---

  /* discard */ corearraylists__tryinsertat
    (AList, AIndex, AItem, &Result);
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ corearraylists__tryextractfirst
  (/* inout */ arraylist* /* param */ AList,
   /* out */   pointer**  /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---

  Result =
    (AList != NULL) && (AItem != NULL);
  if (Result)
  {
    Result =
      corearraylists__hasitems(AList);
    if (Result)
    {
      arraylistheader*     /* var */ ThisList = NULL;

      arraylistnodeheader* /* var */ ThisBeforeNode = NULL;
      arraylistnodeheader* /* var */ ThisExtractNode   = NULL;

      arraylistnode*       /* var */ ABeforeNode = NULL;
      arraylistnode*       /* var */ AAfterNode  = NULL;
      arraylistnode*       /* var */ AExtractNode   = NULL;

      ThisList = (arraylistheader*) AList;

      ABeforeNode =
        ThisList->FirstMarker;
      ThisBeforeNode =
        (arraylistnodeheader*) ABeforeNode;

      AExtractNode =
        ThisBeforeNode->Prev;
      ThisExtractNode =
        (arraylistnodeheader*) AExtractNode;

      AAfterNode =
        ThisExtractNode->Prev;

      corearraylistnodes__trylink
        (ABeforeNode, AAfterNode);

      ThisList->ItemsCurrentCount =
        (ThisList->ItemsCurrentCount - 1);
    } // if
  } // if
   
  // ---
  return Result;
} // func

bool /* func */ corearraylists__tryextractlast
  (/* inout */ arraylist* /* param */ AList,
   /* out */   pointer**  /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (AList != NULL) && (AItem != NULL);
  if (Result)
  {
    Result =
      corearraylists__hasitems(AList);
    if (Result)
    {
      arraylistheader*     /* var */ ThisList = NULL;

      arraylistnodeheader* /* var */ ThisBeforeNode = NULL;
      arraylistnodeheader* /* var */ ThisExtractNode   = NULL;

      arraylistnode*       /* var */ ABeforeNode = NULL;
      arraylistnode*       /* var */ AAfterNode  = NULL;
      arraylistnode*       /* var */ AExtractNode   = NULL;

      ThisList = (arraylistheader*) AList;

      AAfterNode =
        ThisList->FirstMarker;
      ThisAfterNode =
        (arraylistnodeheader*) AAfterNode;

      AExtractNode =
        ThisBeforeNode->Next;
      ThisExtractNode =
        (arraylistnodeheader*) AExtractNode;

      corearraylistnodes__trylink
        (ABeforeNode, AAfterNode);

      ThisList->ItemsCurrentCount =
        (ThisList->ItemsCurrentCount - 1);
    } // if
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corearraylists__tryextractat
  (/* inout */ arraylist*    /* param */ AList,
   /* in */    const index_t /* param */ AIndex,
   /* out */   pointer**     /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (AList != NULL) &&
    (AItem != NULL) &&
    corearraylists__isvalidindex(AIndex);
  if (Result)
  {
    arraylistheader*     /* var */ ThisList = NULL;

    arraylistnodeheader* /* var */ ThisAfterNode = NULL;

    arraylistnode*       /* var */ ABeforeNode  = NULL;
    arraylistnode*       /* var */ AAfterNode   = NULL;
    arraylistnode*       /* var */ AExtractNode = NULL;

    arraylistheadernode* /* var */ ThisAfterNode   = NULL;
    arraylistheadernode* /* var */ ThisExtractNode = NULL;
    arraylistheadernode* /* var */ ThisBeforeNode  = NULL;

    Result =
      corearraylists__tryseeknodeatfwd
        (AList, AIndex, &AAfterNode);
    if (Result)
    {
      ThisAfterNode =
        (arraylistnodeheader*) AAfterNode;
 
      AExtractNode =
        ThisAfterNode->Prev;
      ThisExtractNode =
        (arraylistnodeheader*) ABeforeNode;
   	
      ABeforeNode =
        ThisExtractNode->Prev;
      ThisBeforeNode =
        (arraylistnodeheader*) ABeforeNode;

      corearraylistnodes__trylink
        (ABeforeNode, AAfterNode);

      ThisList->ItemsCurrentCount =
        (ThisList->ItemsCurrentCount - 1);
 
  
  
     
      corearraylists__reindex
        (AList);
    } //if
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corearraylists__tryremovefirst
  (/* inout */ arraylist* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---

  pointer*         /* var */ AItem = NULL;
  arraylistheader* /* var */ ThisList = NULL;

  Result =
    corearraylists__tryextractfirst
      (AList, &AItem);
  if (Result)
  {
    ThisList = (arraylistheader*) AList;
  	
    corememory__deallocate
      (&AItem, ThisList->ItemsSize);
  } // if

  // ---
  return Result;
} // func
   
bool /* func */ corearraylists__tryremovelast
  (/* inout */ arraylist* /* param */ AList)
{
  bool /* var */ Result = false;
  // ---

  pointer*         /* var */ AItem = NULL;
  arraylistheader* /* var */ ThisList = NULL;

  Result =
    corearraylists__tryextractlast
      (AList, &AItem);
  if (Result)
  {
    ThisList = (arraylistheader*) AList;
  	
    corememory__deallocate
      (&AItem, ThisList->ItemsSize);
  } // if

  // ---
  return Result;
} // func

pointer* /* func */ corearraylists__extractfirst
  (/* inout */ list* /* param */ AList)
{
  /* discard */ corearraylists__tryextractfirst
    (AList, AItem);
} // func

pointer* /* func */ corearraylists__extractlast
  (/* inout */ list* /* param */ AList)
{
  /* discard */ corearraylists__tryextractlast
    (AList, AItem);
} // func

pointer* /* func */ corearraylists__extractat
  (/* inout */ arraylist*    /* param */ AList,
   /* in */    const index_t /* param */ AIndex)
{
  pointer* /* var */ Result = NULL;
  // ---
     
  /* discard */ corelists__tryextractlast
    (AList, Result);
     
  // ---
  return Result;
} // func

void /* func */ corearraylists__removefirst
  (/* inout */ list* /* param */ AList)
{
  /* discard */ corearraylists__tryremovefirst
    (AList);
} // func

void /* func */ corearraylists__removelast
  (/* inout */ list* /* param */ AList)
{
  /* discard */ corearraylists__tryremovelast
    (AList);
} // func

// ------------------

bool /* func */ corearraylists__tryexchangeat
  (/* inout */ /* restricted */ arraylist*    /* param */ AList,
   /* in */    /* restricted */ const index_t /* param */ AFirstIndex,
   /* in */    /* restricted */ const index_t /* param */ ASecondIndex)
{
  bool /* var */ Result = false;
  // ---
 
  arraylistnodeheader* /* var */ ThisFirstNode = NULL;
  arraylistnodeheader* /* var */ ThisSecondNode = NULL;
  
  Result =
    corearraylists__tryseeknodeatfwd
      (AList, AFirstIndex, &ThisFirstNode) &&
    corearraylists__tryseeknodeatfwd
      (AList, ASecondIndex, &ThisSecondNode);
     
  if (Result)
  {
    corememory__safeexchangeptr
      (&(ThisFirstNode->Item), &(ThisSecondNode->Item));
  } // if

  // ---
  return Result;
} // func

void /* func */ corearraylists__exchangeat
  (/* inout */ /* restricted */ arraylist*    /* param */ AList,
   /* in */    /* restricted */ const index_t /* param */ AFirstIndex,
   /* in */    /* restricted */ const index_t /* param */ ASecondIndex)
{
  /* discard */ corearraylists__tryexchangeat
    (AList, AFirstIndex, ASecondIndex);
} // func

 // ...

// ------------------

/* override */ const ansinullstring* /* func */ corearraylists__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "corearraylists";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ corearraylists__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corearraylists__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace corearraylists