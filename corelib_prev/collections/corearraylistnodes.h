/** Module: "corearraylistnodes.h"
 ** Descr.: "Double Linked Indexed List"
 **         "Node."
 **/

// namespace corearraylistnodes {
 
// ------------------
 
#ifndef COREARRAYLISTNODES__H
#define COREARRAYLISTNODES__H
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

// opaque type
typedef
  pointer* /* as */ arraylistnode;
typedef
  pointer* /* as */ corearraylistnodes__arraylistnode;

struct arraylistnodeheader
{
  arraylistnode* /* var */ Prev;
  arraylistnode* /* var */ Next;  
  
  pointer*       /* var */ Item;

  index_t        /* var */ Index;
	
} ;

typedef
  arraylistnodeheader  /* as */ arraylistnode_t;
typedef
  arraylistnodeheader* /* as */ arraylistnode_p;
  
typedef
  arraylistnodeheader  /* as */ corearraylistnodes__arraylistnode_t;
typedef
  arraylistnodeheader* /* as */ corearraylistnodes__arraylistnode_p;

// ------------------

/* friend */ bool /* func */ corearraylistnodes__arelinked
  (/* in */ const arraylistnode* /* param */ A,
   /* in */ const arraylistnode* /* param */ B);

/* friend */ bool /* func */ corearraylistnodes__trylink
  (/* inout */ arraylistnode* /* param */ A,
   /* inout */ arraylistnode* /* param */ B);

/* friend */ bool /* func */ corearraylistnodes__trymoveforward
  (/* inout */ arraylistnode** /* param */ AListNode);

/* friend */ bool /* func */ corearraylistnodes__trymovebackward
  (/* inout */ arraylistnode** /* param */ AListNode);

// ------------------

/* friend */ void /* func */ corearraylistnodes__link
  (/* inout */ arraylistnode /* param */ A,
   /* inout */ arraylistnode /* param */ B);

/* friend */ void /* func */ corearraylistnodes__moveforward
  (/* inout */ arraylistnode** /* param */ AListNode);

/* friend */ void /* func */ corearraylistnodes__movebackward
  (/* inout */ arraylistnode** /* param */ AListNode);

// ------------------

arraylistnode* /* func */ corearraylistnodes__createnode
  (/* in */ index_t  /* param */ AIndex,
   /* in */ pointer* /* param */ AItem);

pointer* /* func */ corearraylistnodes__dropnode
  (/* out */ arraylistnode** /* param */ AListNode);

 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ corearraylistnodes__modulename
  ( noparams );

/* override */ int /* func */ corearraylistnodes__setup
  ( noparams );

/* override */ int /* func */ corearraylistnodes__setoff
  ( noparams );

// ------------------

#endif // COREARRAYLISTNODES__H

// } // namespace corearraylistnodes