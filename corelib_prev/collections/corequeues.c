/** Module: "corequeues.c"
 ** Descr.: "predefined library"
 **/
 
// namespace corequeues {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"
#include "corequeuenodes.h"

// ------------------
 
#include "corequeues.h"

// ------------------

/* friend */ bool /* func */ corequeues__sameitemsize
  (/* in */ queue* /* param */ AFirstQueue,
  (/* in */ queue* /* param */ ASecondQueue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AQueue != NULL);
  if (Result)
  {
    queueheader* /* var */ ThisFirstQueue = NULL;
    queueheader* /* var */ ThisSecondQueue = NULL;
      
    ThisFirstQueue  = (queueheader*) AFirstQueue;
    ThisSecondQueue = (queueheader*) ASecondQueue;
    
    Result =
      (ThisFirstQueue->ItemsSize == ThisSecondQueue->ItemsSize);
  } // if

  // ---
  return Result;
} // func

/* friend */ bool /* func */ corequeues__sameitemtype
  (/* in */ queue* /* param */ AFirstQueue,
  (/* in */ queue* /* param */ ASecondQueue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AQueue != NULL);
  if (Result)
  {
    queueheader* /* var */ ThisFirstQueue = NULL;
    queueheader* /* var */ ThisSecondQueue = NULL;
      
    ThisFirstQueue  = (queueheader*) AFirstQueue;
    ThisSecondQueue = (queueheader*) ASecondQueue;
    
    Result =
      (ThisFirstQueue->ItemsType == ThisSecondQueue->ItemsType);
  } // if

  // ---
  return Result;
} // func

// ------------------

queue* /* func */ corequeues__createqueue
  ( noparams )
{
  queue* /* var */ Result = NULL;
  // ---

  Result =
    corememory__clearallocate
      (sizeof(queueheader));

  queueheader* /* var */ ThisQueue = NULL;
  size_t       /* var */ AItemSize;
  typecode_t   /* var */ AItemType;

  ThisQueue =
    (queueheader*) corememory__share
      (Result);

  AItemSize = 0;
  AItemType = unknowntype;
  
  ThisQueue->ItemsType =
    AItemType;

  ThisQueue->ItemsSize =
    AItemSize;

  ThisQueue->ItemsMaxCount =
    0;
    
  ThisQueue->ItemsCurrentCount =
    0;

  ThisQueue->IteratorCount =
    0;
 
  // add markers
  ThisQueue->FirstMarker =
    corequeuenode__createnode(NULL);
  ThisQueue->LastMarker =
    corequeuenode__createnode(NULL);
  corequeuenodes__link
    (ThisQueue->FirstMarker, ThisQueue->LastMarker);

  // ---
  return Result;
} // func

queue* /* func */ corequeues__createqueuebytype
  (/* in */ typecode_t* /* param */ AItemType)
{
  queue* /* var */ Result = NULL;
  // ---

  Result =
    corememory__clearallocate
      (sizeof(queueheader));

  queueheader* /* var */ ThisQueue = NULL;
  size_t       /* var */ AItemSize;

  ThisQueue =
    (queueheader*) corememory__share
      (Result);

  AItemSize =
    corememory__typetosize(AItemType);
  
  ThisQueue->ItemsType =
    AItemType;

  ThisQueue->ItemsSize =
    AItemSize;

  ThisQueue->ItemsMaxCount =
    0;
    
  ThisQueue->ItemsCurrentCount =
    0;

  ThisQueue->IteratorCount =
    0;
 
  // add markers
  ThisQueue->FirstMarker =
    corequeuenode__createnode(NULL);
  ThisQueue->LastMarker =
    corequeuenode__createnode(NULL);
  corequeuenodes__link
    (ThisQueue->FirstMarker, ThisQueue->LastMarker);

  // ---
  return Result;
} // func

queue* /* func */ corequeues__createqueuerestrictcount
  (/* in */ count_t* /* param */ ACount)
{
  queue* /* var */ Result = NULL;
  // ---

  Result =
    corememory__clearallocate
      (sizeof(queueheader));

  queueheader* /* var */ ThisQueue = NULL;
  size_t       /* var */ AItemSize;
  typecode_t   /* var */ AItemType;

  ThisQueue =
    (queueheader*) corememory__share
      (Result);

  AItemSize = 0;
  AItemType = unknowntype;
  
  ThisQueue->ItemsType =
    AItemType;

  ThisQueue->ItemsSize =
    AItemSize;

  ThisQueue->ItemsMaxCount =
    AMaxCount;
    
  ThisQueue->ItemsCurrentCount =
    0;

  ThisQueue->IteratorCount =
    0;
 
  // add markers
  ThisQueue->FirstMarker =
    corequeuenode__createnode(NULL);
  ThisQueue->LastMarker =
    corequeuenode__createnode(NULL);
  corequeuenodes__link
    (ThisQueue->FirstMarker, ThisQueue->LastMarker);

  // ---
  return Result;
} // func

queue* /* func */ corequeues__createqueuebysize
  (/* in */ size_t* /* param */ AItemSize)
{
  queue* /* var */ Result = NULL;
  // ---

  Result =
    corememory__clearallocate
      (sizeof(queueheader));

  queueheader* /* var */ ThisQueue = NULL;
  typecode_t   /* var */ AItemType;

  ThisQueue =
    (queueheader*) corememory__share
      (Result);

  AItemType = unknowntype;
  
  ThisQueue->ItemsType =
    AItemType;

  ThisQueue->ItemsSize =
    AItemSize;

  ThisQueue->ItemsMaxCount =
    AMaxCount;
    
  ThisQueue->ItemsCurrentCount =
    0;

  ThisQueue->IteratorCount =
    0;
 
  // add markers
  ThisQueue->FirstMarker =
    corequeuenode__createnode(NULL);
  ThisQueue->LastMarker =
    corequeuenode__createnode(NULL);
  corequeuenodes__link
    (ThisQueue->FirstMarker, ThisQueue->LastMarker);

  // ---
  return Result;
} // func

void /* func */ corequeues__dropqueue
  (/* out */ queue** /* param */ AQueue);
{
  Result = (AQueue != NULL);
  if (Result)
  {
    queueheader* /* var */ ThisQueue = NULL;
    ThisQueue = (queueheader*) *AQueue;

    corequeues__clear(AQueue);

    /* discard */ corequeuenodes__dropnode
      (&(ThisQueue->LastMarker));

    /* discard */ corequeuenodes__dropnode
      (&(ThisQueue->FirstMarker));
 
    corememory__cleardeallocate
     (*AQueue, sizeof(queueheader);
  } // if
} // func

// ------------------

bool /* func */ corequeues__underiteration
  (/* in */ queue* /* param */ AQueue)
{
  bool /* var */ Result = false;
  // ---
  
  if (AQueue != NULL)
  {
    queueheader* /* var */ ThisQueue = NULL;
    ThisQueue = (queueheader*) AQueue;

	Result = (ThisQueue->IteratorCount > 0);
  } // if
     
  // ---
  return Result;
} // func

queueiterator* /* func */ corequeues__createiterator
  (/* in */ queue* /* param */ AQueue)
{
  queueiterator* /* var */ Result = NULL;
  // ---

  if (AQueue != NULL)
  {
    queueheader* /* var */ ThisQueue =  NULL;
    ThisQueue = (queueheader*) AQueue;
  	  
    queueiteratorheader* /* var */ AIterator = NULL;

    AIterator =
      corememory__clearallocate
       (sizeof(queueiteratorheader));

    AIterator->Queue =
      AQueue;
     
    AIterator->CurrentItem =
      corememory__share
        (ThisQueue->FirstMarker->Next);

    AIterator->ItemsCount =
      ThisQueue->ItemsMaxCount;

    AIterator->ItemIndex =
      ThisQueue->ItemsMaxCount;

    AIterator->Direction =
      queuedirections__forward;

    ThisQueue->IteratorCount =
      (ThisQueue->IteratorCount + 1);

    Result = AIterator;
  } // if

  // ---
  return Result;
} // func

void /* func */ corequeues__dropiterator
  (/* out */ queueiterator** /* param */ AIterator)
{
  Result = (AIterator != NULL);
  if (Result)
  {
    queueiteratorheader** /* var */ ThisIterator = NULL;
    ThisIterator = (queueiteratorheader**) AIterator;

    queueheader** /* var */ ThisQueue = NULL;
    
    ThisQueue = (queueheader*) ThisIterator->Queue;

    ThisQueue->IteratorCount =
      (ThisQueue->IteratorCount - 1);
      
    corememory__unshare
      (&(ThisIterator->CurrentItem));

    corememory__cleardeallocate
      (ThisIterator);
      
    *AIterator = NULL;
  } // if
} // func

// ------------------

bool /* func */ corequeues__isdone
  (/* in */ const queueiterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator == NULL);
  if (!Result)
  {
    queueiteratorheader* /* var */ ThisIterator = NULL;

    queueheader* /* var */ ThisIterator = NULL;
    
    ThisIterator = (queueiteratorheader*) AIterator;

    ThisQueue = (queueheader*) ThisIterator->Queue;

    Result =
      (ThisIterator->CurrentItem = ThisQueue->BottomMarker);
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corequeues__trymovenext
  (/* inout */ queueiterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (AIterator != NULL);
  if (Result)
  {
    queueiteratorheader** /* var */ ThisIterator = NULL;
    ThisIterator = (queueiteratorheader**) AIterator;

    Result =
      (ThisIterator->CurrentItem == ThisIterator->LastMarker);
 
      if (Result)
      {
        corequeuenodes__trymoveforward(&(ThisIterator->CurrentItem));      
      }
  } // if
     
  // ---
  return Result;
} // func

void /* func */ corequeues__movenext
  (/* inout */ queueiterator* /* param */ AIterator)
{
  /* discard */ corequeues__trymovenext(AIterator);
} // func

// ------------------

bool /* func */ corequeues__trygetnode
  (/* inout */ queueiterator* /* param */ AIterator
   /* out */   queuenode*     /* param */ ANode)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      queueiteratorheader* /* var */ ThisIterator = NULL;
      
      ThisIterator = (queueiteratorheader*) AIterator;
      
      ANode =
        ThisIterator->CurrentNode;
    } // if
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corequeues__trygetitem
  (/* inout */ queueiterator* /* param */ AIterator
   /* out */   pointer*       /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      queueheader*         /* var */ ThisList = NULL;
      queuenodeheader*     /* var */ ThisNode = NULL;
      queueiteratorheader* /* var */ ThisIterator = NULL;
      
      ThisIterator = (queueiteratorheader*) AIterator;
      ThisList = (queueheader*) ThisIterator->MainList;
      ThisNode = (queuenodeheader*) ThisIterator->CurrentNode;
      	  
      corememory__safecopy
        (AItem, ThisNode->Item, ThisList->ItemsSize);
    } // if
  } // if
    
  // ---
  return Result;
} // func

bool /* func */ corequeues__trysetitem
  (/* inout */ queueiterator*  /* param */ AIterator
   /* in */    const  pointer* /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      queueheader*         /* var */ ThisList = NULL;
      queuenodeheader*     /* var */ ThisNode = NULL;
      queueiteratorheader* /* var */ ThisIterator = NULL;
      
      ThisIterator = (queueiteratorheader*) AIterator;
      ThisList = (queueheader*) ThisIterator->MainList;
      ThisNode = (queuenodeheader*) ThisIterator->CurrentNode;
      	  
      corememory__safecopy
        (ThisNode->Item, AItem, ThisList->ItemsSize);
    } // if
  } // if
     
  // ---
  return Result;
} // func

void /* func */ corequeues__getnode
  (/* inout */ queueiterator* /* param */ AIterator
   /* out */   pointer*       /* param */ ANode)
{
  /* discard */ corequeues__trygetnode
    (AList, AItem);
} // func

void /* func */ corequeues__getitem
  (/* inout */ queueiterator* /* param */ AIterator
   /* out */   pointer*       /* param */ AItem)
{
  /* discard */ corequeues__trygetitem
    (AList, AItem);
} // func

void /* func */ corequeues__setitem
  (/* inout */ queueiterator* /* param */ AIterator
   /* in */    const pointer* /* param */ AItem)
{
  /* discard */ corequeues__trysetitem
    (AList, AItem);
} // func

// ------------------

count_t /* func */ corequeues__getcurrentcount
  (/* in */ const queue* /* param */ AQueue)
{
  count_t /* var */ Result = 0;
  // ---
     
  if (AQueue != NULL)
  {
    queueheader* /* var */ ThisQueue = NULL;
    ThisQueue = (queueheader*) AQueue;
    Result = ThisQueue->ItemsCurrentCount;
  } // if
  
  // ---
  return Result;
} // func

count_t /* func */ corequeues__getmaxcount
  (/* in */ const queue* /* param */ AQueue)
{
  count_t /* var */ Result = 0;
  // ---
     
  if (AQueue != NULL)
  {
    queueheader* /* var */ ThisQueue = NULL;
    ThisQueue = (queueheader*) AQueue;
    Result = ThisQueue->ItemsMaxCount;
  } // if
  
  // ---
  return Result;
} // func

// ------------------

bool /* func */ corequeues__tryclear
  (/* inout */ queue* /* param */ AQueue)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (AQueue != NULL);
  if (Result)
  {
    queueheader*     /* var */ ThisQueue = NULL;
    
    queueiterator*   /* var */ AIterator = NULL;
    queueiteratorheader* /* var */ ThisIterator = NULL;
 
    queuenode*       /* var */ AQueueNode = NULL;
    queuenodeheader* /* var */ ThisQueueNode = NULL;
    
    pointer*         /* var */ AData = NULL;

    ThisQueue = (queueheader*) AQueue;

    // remove data first
    AIterator =
      corequeues__createiterator(AQueue);
    ThisIterator =
      (queueiteratorheader*) AIterator;

    while (!corequeues__isdone(AIterator))
    {
      AQueueNode =
        ThisIterator->CurrentItem;
      ThisQueueNode = (queuenodeheader*) AQueueNode;
      
      AData =
        corememory__transfer(&(ThisQueueNode->Data));

      corememory__cleardeallocate
        (AData, ThisQueue->ItemsSize);
    
      corequeues__movenext(AIterator);
    } // while

    corequeues__dropiterator(&AQueue);

    // remove nodes
    AIterator =
      corequeues__createiterator(AQueue);

    while (!corequeues__isdone(AIterator))
    {
      AQueueNode =
        ThisIterator->CurrentItem;
      ThisQueueNode = (queuenodeheader*) AQueueNode;

      corequeues__movenext(AIterator);

      /* discard */ corequeuenodes__dropnode
        (&ThisQueueNode);
    } // while

    corequeues__dropiterator(&AQueue);

    // update other properties states
    corequeuenodes__link
      (ThisQueue->FirstMarker, ThisQueue->LastMarker);

    ThisQueue->ItemsMaxCount = 0;
    ThisQueue->ItemsCurrentCount = 0;
    ThisQueue->IteratorCount = 0;
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corequeues__isempty
  (/* in */ const queue* /* param */ AQueue)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (AQueue != NULL);
  if (Result)
  {
    queueheader* /* var */ ThisQueue = NULL;
    
    ThisQueue = (queueheader*) AQueue;

    Result =
     (corequeuenodes__arelinked(ThisQueue->TopMarker, ThisQueue->BottomMarker)) ||
      (ThisQueue->ItemsCurrentCount == 0));
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corequeues__isfull
  (/* in */ const queue* /* param */ AQueue);
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (AQueue != NULL);
  if (Result)
  {
    queueheader* /* var */ ThisQueue = NULL;
    
    ThisQueue = (queueheader*) AQueue;

    Result =
      (ThisQueue->ItemsMaxCount > 0) &&
      (ThisQueue->ItemsCurrentCount == ThisQueue->ItemsMaxCount));
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corequeues__hasitems
  (/* in */ const queue* /* param */ AQueue)
{
  bool /* var */ Result = false;
  // ---
     
  Result = !(corequeues__isempty(AQueue));
     
  // ---
  return Result;
} // func

void /* func */ corequeues__clear
  (/* inout */ queue* /* param */ AQueue)
{
  /* discard */ corequeues__tryclear
    (AQueue);
} // func

// ------------------

bool /* func */ corequeues__tryinsert
  (/* inout */ queue*         /* param */ AQueue
   /* in */    const pointer* /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (AQueue != NULL) && (AItem != NULL);
  if (Result)
  {
    queueheader*     /* var */ ThisQueue = NULL;

    queuenodeheader* /* var */ ThisBeforeNode = NULL;

    queuenode*       /* var */ ABeforeNode = NULL;
    queuenode*       /* var */ AAfterNode  = NULL;
    queuenode*       /* var */ ANewNode    = NULL;

    ANewNode =
      corequeuenodes__createnode
        (AItem);

    ThisQueue = (queueheader*) AQueue;

    ABeforeNode =
      ThisQueue->FirstMarker;
    ThisBeforeNode =
      (queuenodeheader*) ABeforeNode;

    AAfterNode =
      ThisBeforeNode->Next;
  
    corequeuenodes__trylink
      (ABeforeNode, ANewNode);
    corequeuenodes__trylink
      (ANewNode, AAfterNode);
      
    ThisQueue->ItemsCurrentCount =
      (ThisQueue->ItemsCurrentCount + 1);
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corequeues__tryextract
  (/* inout */ queue*    /* param */ AQueue
   /* out */   pointer** /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (AQueue != NULL) && (AItem != NULL);
  if (Result)
  {
    Result =
      corequeues__hasitems(AQueue);
    if (Result)
    {
      queueheader*     /* var */ ThisQueue = NULL;

      queuenodeheader* /* var */ ThisBeforeNode = NULL;
      queuenodeheader* /* var */ ThisFreeNode   = NULL;

      queuenode*       /* var */ ABeforeNode = NULL;
      queuenode*       /* var */ AAfterNode  = NULL;
      queuenode*       /* var */ AFreeNode   = NULL;

      ThisQueue = (queueheader*) AQueue;

      AAfterNode =
        ThisQueue->FirstMarker;
      ThisAfterNode =
        (queuenodeheader*) AAfterNode;

      AFreeNode =
        ThisBeforeNode->Next;
      ThisFreeNode =
        (queuenodeheader*) AFreeNode;

      corequeuenodes__trylink
        (ABeforeNode, AAfterNode);

      ThisQueue->ItemsCurrentCount =
        (ThisQueue->ItemsCurrentCount - 1);
    } // if
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ corequeues__tryremove
  (/* inout */ queue* /* param */ AQueue)
{
  bool /* var */ Result = false;
  // ---

  pointer*     /* var */ AItem = NULL;
  queueheader* /* var */ ThisQueue = NULL;

  Result =
    corequeues__trypop
      (AQueue, &AItem);
  if (Result)
  {
    ThisQueue = (queueheader*) AQueue;
  	
    corememory__deallocate
      (&AItem, ThisQueue->ItemsSize);
  } // if

  // ---
  return Result;
} // func

void /* func */ corequeues__insert
  (/* inout */ queue*         /* param */ AQueue,
   /* in */    const pointer* /* param */ AItem)
{
  /* discard */ corequeues__tryinsert
    (AQueue, AItem);
} // func

void /* func */ corequeues__extract
  (/* inout */ queue**  /* param */ AQueue,
   /* out */   pointer* /* param */ AItem)
{
  /* discard */ corequeues__tryextract
    (AQueue, AItem);
} // func

void /* func */ corequeues__remove
  (/* inout */ queue** /* param */ AQueue)
{
  /* discard */ corequeues__tryremove
    (AQueue, AItem);
} // func

// ------------------

/* override */ const ansinullstring* /* func */ corequeues__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "corequeues";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ corequeues__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corequeues__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

 
 // ...

// } // namespace corequeues