/** Module: "corecubes.c"
 ** Descr.: "predefined library"
 **/
 
// namespace corecubes {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------
 
#include "corecubes.h"
 
// ------------------

count_t /* func */ corecubes__getmaxcount
  (/* in */ const cube* /* param */ ACube)
{
  count_t /* var */ Result = 0;
  // ---
     
  if (ACube != NULL)
  {
    cubeheader* /* var */ ThisCube = NULL;
    ThisCube = (cubeheader*) ACube;
    Result = ThisCube->ItemsMaxCount;
  } // if
  
  // ---
  return Result;
} // func

count_t /* func */ corecubes__getdimcount
  (/* in */ const cube* /* param */ ACube)
{
  count_t /* var */ Result = 0;
  // ---
     
  if (ACube != NULL)
  {
    cubeheader* /* var */ ThisCube = NULL;
    ThisCube = (cubeheader*) ACube;
    Result = ThisCube->DimsCount;
  } // if
  
  // ---
  return Result;
} // func

// ------------------

comparison /* func */ corecubes__compare
  (/* in */ ansinullstring* /* param */ A, B)
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func
   
 // ------------------
 
void /* func */ corecubes__clear
  (/* inout */ ansinullstring* /* param */ ADest)
{
  // ...
} // func
 
 // ...

// ------------------

/* override */ int /* func */ corecubes__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corecubes__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------


// } // namespace corecubes