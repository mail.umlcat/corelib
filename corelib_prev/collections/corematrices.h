/** Module: "corematrices.h"
 ** Descr.: "Dynamically allocated two dimension array."
 **/
 
// namespace corematrices {
 
// ------------------
 
#ifndef COREMATRICES__H
#define COREMATRICES__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

// opaque type
typedef
  pointer* /* as */ matrix;
typedef
  pointer* /* as */ corematrices__matrix;

struct matrixheader
{
  // pointer to first item
  pointer* /* var */ ItemsFirst;

  // pointer to last item
  pointer* /* var */ ItemsLast;

  // only applies when items are same type
  // "unknowntype" value means different type
  typecode /* var */ ItemsType;

  // applies instead of "typecode"
  // "0" means not used
  size_t   /* var */ ItemsSize;

  // for a matrix is always "2"
  /* readonly */
  count_t  /* var */ DimsCount;

  // restricts how many items can be stored
  count_t  /* var */ ItemsMaxCount;

  // indicates iterator related objects
  count_t  /* var */ IteratorCount;

  // restricts how many rows can be stored
  count_t  /* var */ RowsMaxCount;

  // restricts how many columns can be stored
  count_t  /* var */ ColsMaxCount;
  
  equality /* var */ MatchFunc;
  
  // ...
} ;

typedef
  matrixheader       /* as */ matrix_t;
typedef
  matrixheader*      /* as */ matrix_p;

typedef
  matrix_t     /* as */ corematrices_matrix_t;
typedef
  matrix_p     /* as */ corematrices_matrix_p;

// ------------------

/* friend */ bool /* func */ corematrices__sameitemtype
  (/* in */  matrix* /* param */ AFirstMatrix,
  (/* out */ matrix* /* param */ ASecondMatrix);

/* friend */ bool /* func */ corematrices__sameitemsize
  (/* in */  matrix* /* param */ AFirstMatrix,
  (/* out */ matrix* /* param */ ASecondMatrix);

/* friend */ bool /* func */ corematrices__sameitemcount
  (/* in */  matrix* /* param */ AFirstMatrix,
  (/* out */ matrix* /* param */ ASecondMatrix);

/* friend */ bool /* func */ corematrices__sameitemrowcount
  (/* in */  matrix* /* param */ AFirstMatrix,
  (/* out */ matrix* /* param */ ASecondMatrix);

/* friend */ bool /* func */ corematrices__sameitemcolcount
  (/* in */  matrix* /* param */ AFirstMatrix,
  (/* out */ matrix* /* param */ ASecondMatrix);

// ------------------

/* friend */ bool /* func */ corematrices__trycopyempty
  (/* in */  matrix* /* param */ ASourceMatrix,
  (/* out */ matrix* /* param */ ADestMatrix);

/* friend */ bool /* func */ corematrices__trycopyall
  (/* in */  matrix* /* param */ ASourceMatrix,
  (/* out */ matrix* /* param */ ADestMatrix);

/* friend */ bool /* func */ corematrices__trycopyfliphorizontal
  (/* in */  matrix* /* param */ ASourceMatrix,
  (/* out */ matrix* /* param */ ADestMatrix);
  
/* friend */ bool /* func */ corematrices__trycopyflipvertical
  (/* in */  matrix* /* param */ ASourceMatrix,
  (/* out */ matrix* /* param */ ADestMatrix);

/* friend */ bool /* func */ corematrices__trycopyconcathorizontal
  (/* in */  matrix* /* param */ AFirstMatrix,
   /* in */  matrix* /* param */ ASecondMatrix,
   /* out */ matrix* /* param */ ADestMatrix);
   
/* friend */ bool /* func */ corematrices__trycopyconcatvertical
  (/* in */  matrix* /* param */ AFirstMatrix,
   /* in */  matrix* /* param */ ASecondMatrix,
   /* out */ matrix* /* param */ ADestMatrix);

/* friend */ void /* func */ corematrices__copyempty
  (/* in */  matrix* /* param */ ASourceMatrix,
  (/* out */ matrix* /* param */ ADestMatrix);

/* friend */ void /* func */ corematrices__copyall
  (/* in */  matrix* /* param */ ASourceMatrix,
  (/* out */ matrix* /* param */ ADestMatrix);
  
/* friend */ void /* func */ corematrices__copyfliphorizontal
  (/* in */  matrix* /* param */ ASourceMatrix,
  (/* out */ matrix* /* param */ ADestMatrix);
  
/* friend */ void /* func */ corematrices__copyflipvertical
  (/* in */  matrix* /* param */ ASourceMatrix,
  (/* out */ matrix* /* param */ ADestMatrix);

/* friend */ void /* func */ corematrices__copyconcathorizontal
  (/* in */  matrix* /* param */ AFirstMatrix,
   /* in */  matrix* /* param */ ASecondMatrix,
   /* out */ matrix* /* param */ ADestMatrix);
   
/* friend */ void /* func */ corematrices__copyconcatvertical
  (/* in */  matrix* /* param */ AFirstMatrix,
   /* in */  matrix* /* param */ ASecondMatrix,
   /* out */ matrix* /* param */ ADestMatrix);

// ------------------

matrix* /* func */ corematrices__creatematrix
  ( noparams );

matrix* /* func */ corematrices__creatematrixbyitemtype
  (/* in */ typecode_t /* param */ AItemType);

matrix* /* func */ corematrices__creatematrixbyitemtypecount
  (/* in */ typecode_t /* param */ AItemType,
   /* in */ count_t    /* param */ AItemMaxRowCount,
   /* in */ count_t    /* param */ AItemMaxColCount);

matrix* /* func */ corematrices__creatematrixsquarebyitemtype
  (/* in */ count_t    /* param */ AItemMaxCount,
   /* in */ typecode_t /* param */ AItemType);

matrix* /* func */ corematrices__creatematrixbyitemsizecount
  (/* in */ size_t  /* param */ AItemSize,
   /* in */ count_t /* param */ AItemMaxRowCount,
   /* in */ count_t /* param */ AItemMaxColCount);

matrix* /* func */ corematrices__creatematrixsquarebyitemsize
  (/* in */ count_t /* param */ AItemMaxCount,
   /* in */ size_t  /* param */ AItemSize);

void /* func */ corematrices__dropmatrix
  (/* out */ matrix** /* param */ AMatrix);

// ------------------

typecode_t /* func */ corematrices__getitemtype
  (/* in */ const matrix* /* param */ AMatrix);

size_t /* func */ corematrices__getitemsize
  (/* in */ const matrix* /* param */ AMatrix);

// ------------------

count_t /* func */ corematrices__getmaxcount
  (/* in */ const matrix* /* param */ AMatrix);

count_t /* func */ corematrices__getdimcount
  (/* in */ const matrix* /* param */ AMatrix);

// ------------------

count_t /* func */ corematrices__getmaxcolcount
  (/* in */ const matrix* /* param */ AMatrix);
  
count_t /* func */ corematrices__getmaxrowcount
  (/* in */ const matrix* /* param */ AMatrix);

bool /* func */ corematrices__issquare
  (/* inout */ matrix* /* param */ AMatrix);

// ------------------

bool /* func */ corematrices__tryclear
  (/* inout */ matrix* /* param */ AMatrix);

void /* func */ corematrices__clear
  (/* inout */ matrix* /* param */ AMatrix);

// ------------------

bool /* func */ corematrices__trygetitemat
  (/* in */  /* restricted */ const matrix* /* param */ AMatrix,
   /* in */  /* restricted */ const index_t /* param */ ARowIndex,
   /* in */  /* restricted */ const index_t /* param */ AColIndex,
   /* out */ /* restricted */ pointer*      /* param */ AItem);

bool /* func */ corematrices__trysetitemat
  (/* in */  /* restricted */ const matrix*  /* param */ AMatrix,
   /* in */  /* restricted */ const index_t  /* param */ ARowIndex,
   /* in */  /* restricted */ const index_t  /* param */ AColIndex,
   /* out */ /* restricted */ const pointer* /* param */ AItem);

void /* func */ corematrices__getitemat
  (/* in */  /* restricted */ const matrix* /* param */ AMatrix,
   /* in */  /* restricted */ const index_t /* param */ ARowIndex,
   /* in */  /* restricted */ const index_t /* param */ AColIndex,
   /* out */ /* restricted */ pointer*      /* param */ AItem);

void /* func */ corematrices__setitemat
  (/* in */  /* restricted */ const matrix*  /* param */ AMatrix,
   /* in */  /* restricted */ const index_t  /* param */ ARowIndex,
   /* in */  /* restricted */ const index_t  /* param */ AColIndex,
   /* out */ /* restricted */ const pointer* /* param */ AItem);

// ------------------

bool /* func */ corematrices__tryclearat
  (/* inout */ /* restricted */ matrix*       /* param */ AMatrix,
   /* in */    /* restricted */ const index_t /* param */ ARowIndex,
   /* in */    /* restricted */ const index_t /* param */ AColIndex);

bool /* func */ corematrices__tryexchangeat
  (/* inout */ /* restricted */ matrix*       /* param */ AMatrix,
   /* in */    /* restricted */ const index_t /* param */ AFirstRowIndex,
   /* in */    /* restricted */ const index_t /* param */ AFirstColIndex,
   /* in */    /* restricted */ const index_t /* param */ ASecondRowIndex,
   /* in */    /* restricted */ const index_t /* param */ ASecondColIndex);

void /* func */ corematrices__clearat
  (/* inout */ /* restricted */ matrix*       /* param */ AMatrix,
   /* in */    /* restricted */ const index_t /* param */ ARowIndex,
   /* in */    /* restricted */ const index_t /* param */ AColIndex);

void /* func */ corematrices__exchangeat
  (/* inout */ /* restricted */ matrix*       /* param */ AMatrix,
   /* in */    /* restricted */ const index_t /* param */ AFirstRowIndex,
   /* in */    /* restricted */ const index_t /* param */ AFirstColIndex,
   /* in */    /* restricted */ const index_t /* param */ ASecondRowIndex,
   /* in */    /* restricted */ const index_t /* param */ ASecondColIndex);

// ------------------



 // ...
 
// ------------------
 
#endif // COREMATRICES__H

// } // namespace corematrices