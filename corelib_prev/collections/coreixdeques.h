/** Module: "coreixdeques.h"
 ** Descr.: "Indexed Double Linked Deque Items."
 **/

// namespace coreixdeques {
 
// ------------------
 
#ifndef COREIXDEQUES__H
#define COREIXDEQUES__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"
#include "coreixdequenodes.h"

// ------------------

// opaque type
typedef
  pointer* /* as */ ixdeque;
typedef
  pointer* /* as */ coreixdeques__ixdeque;

struct ixdequeheader
{
  // pointer to node before first node
  ixdequenode* /* var */ FirstMarker;
  
  // pointer to node before last node
  ixdequenode* /* var */ LastMarker;

  // only applies when items are same type
  // "unknowntype" value means different type
  typecode /* var */ ItemsType;

  // applies instead of "typecode"
  // "0" means not used
  size_t   /* var */ ItemsSize;

  // restricts how many items can be stored
  // "0" means not used
  count_t  /* var */ ItemsMaxCount;

  // how many items are currently stored
  count_t  /* var */ ItemsCurrentCount;
  
  // indicates iterator related objects
  count_t  /* var */ IteratorCount;
  
  areequalfunctor /* var */ MatchFunc;
  
  // ...
} ;

typedef
  ixdequeheader  /* as */ ixdeque_t;
typedef
  ixdequeheader* /* as */ ixdeque_p;
  
typedef
  ixdequeheader  /* as */ coreixdeques__ixdeque_t;
typedef
  ixdequeheader* /* as */ coreixdeques__ixdeque_p;

// ------------------

typedef
  pointer* /* as */ ixdequeiterator;

struct ixdequeiteratorheader
{
  // backreference to collection
  ixdeque*     /* var */ MainList

  ixdequenode* /* var */ CurrentItem;

  count_t               /* var */ ItemsCount;
  size_t                /* var */ ItemsSize;

  listdirections        /* var */ Direction;
  
  // ...
} ;

typedef
  ixdequeiteratorheader  /* as */ ixdequeiterator_t;
typedef
  ixdequeiteratorheader* /* as */ ixdequeiterator_p;

typedef
  ixdequeiteratorheader  /* as */ coreixdeques__ixdequeiterator_t;
typedef
  ixdequeiteratorheader* /* as */ coreixdeques__ixdequeiterator_p;

// ------------------

ixdeque* /* func */ coreixdeques__createixdeque
  ( noparams );

ixdeque* /* func */ coreixdeques__createixdequebytype
  (/* in */ typecode_t* /* param */ ADataType);

ixdeque* /* func */ coreixdeques__createixdequerestrictcount
  (/* in */ count_t* /* param */ ACount);
  
ixdeque* /* func */ coreixdeques__createixdequebysize
  (/* in */ size_t* /* param */ ADataSize);

void /* func */ coreixdeques__dropixdeque
  (/* out */ ixdeque** /* param */ AList);

// ------------------

bool /* func */ coreixdeques__underiteration
  (/* in */ ixdeque* /* param */ AList)

ixdequeiterator* /* func */ coreixdeques__createiteratorforward
  (/* in */ ixdeque* /* param */ AList);

ixdequeiterator* /* func */ coreixdeques__createiteratorbackward
  (/* in */ ixdeque* /* param */ AList);

void /* func */ coreixdeques__dropiterator
  (/* out */ ixdequeiterator** /* param */ AIterator);

// ------------------

bool /* func */ coreixdeques__isdone
  (/* in */ const ixdequeiterator* /* param */ AIterator);

bool /* func */ coreixdeques__trymovenext
  (/* inout */ ixdequeiterator* /* param */ AIterator);

void /* func */ coreixdeques__movenext
  (/* inout */ ixdequeiterator* /* param */ AIterator);

bool /* func */ coreixdeques__trygetitem
  (/* inout */ ixdequeiterator* /* param */ AIterator
   /* out */   pointer*        /* param */ AItem);

bool /* func */ coreixdeques__trysetitem
  (/* inout */ ixdequeiterator* /* param */ AIterator
   /* in */ const pointer*     /* param */ AItem);

void /* func */ coreixdeques__getitem
  (/* inout */ ixdequeiterator* /* param */ AIterator
   /* out */   pointer*        /* param */ AItem);

void /* func */ coreixdeques__setitem
  (/* inout */ ixdequeiterator* /* param */ AIterator
   /* in */ const pointer*     /* param */ AItem);

// ------------------

void /* func */ coreixdeques__assignequalityfunc
  (/* inout */ ixdeque*  /* param */ AList,
   /* in */    equality /* param */ AMatchFunc);

// ------------------

count_t /* func */ coreixdeques__getcurrentcount
  (/* in */ const ixdeque* /* param */ AStack);

count_t /* func */ coreixdeques__getmaxcount
  (/* in */ const ixdeque* /* param */ AStack);

// ------------------

bool /* func */ coreixdeques__tryclear
  (/* inout */ ixdeque* /* param */ AList);

bool /* func */ coreixdeques__isempty
  (/* in */ const ixdeque* /* param */ AList);

bool /* func */ coreixdeques__hasitems
  (/* in */ const ixdeque* /* param */ AList);

void /* func */ coreixdeques__clear
  (/* inout */ ixdeque* /* param */ AList);

// ------------------


 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coreixdeques__modulename
  ( noparams );

/* override */ int /* func */ coreixdeques__setup
  ( noparams );

/* override */ int /* func */ coreixdeques__setoff
  ( noparams );

// ------------------

#endif // COREIXDEQUES__H

// } // namespace coreixdeques