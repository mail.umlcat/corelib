/** Module: "coredequeuesets.h"
 ** Descr.: "Set implementation using Deques."
 **/

// namespace coredequeuesets {
 
// ------------------
 
#ifndef COREDEQUEUESETS__H
#define COREDEQUEUESETS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"
#include "coredequeuenodes.h"

// ------------------

typedef
  pointer* /* as */ dequeueset;

struct dequeuesetheader
{
  // pointer to node before first node
  dequeuenode* /* var */ TopMarker;
  
  // pointer to node before last node
  dequeuenode* /* var */ BottomMarker;

  // only applies when items are same type
  // "unknowntype" value means different type
  typecode /* var */ ItemsType;

  // applies instead of "typecode"
  // "0" means not used
  size_t   /* var */ ItemsSize;

  // restricts how many items can be stored
  // "0" means not used
  count_t  /* var */ ItemsMaxCount;

  // how many items are currently stored
  count_t  /* var */ ItemsCurrentCount;
  
  // indicates iterator related objects
  count_t  /* var */ IteratorCount;
  
  // indicates wheter the set can be ordered
  bool     /* var */ WantSorted;

  areequalfunctor /* var */ MatchFunc;
  
  // ...
} ;

typedef
  dequeuesetheader  /* as */ dequeueset_t;
typedef
  dequeuesetheader* /* as */ dequeueset_p;
  
typedef
  dequeuesetheader  /* as */ coredequeuesets__dequeueset_t;
typedef
  dequeuesetheader* /* as */ coredequeuesets__dequeueset_p;

// ------------------

typedef
  pointer* /* as */ dequeuesetiterator;

struct dequeuesetiteratorheader
{
  // backreference to collection
  dequeueset*     /* var */ MainList

  dequeuenode* /* var */ CurrentItem;

  count_t               /* var */ ItemsCount;
  size_t                /* var */ ItemsSize;

  listdirections        /* var */ Direction;
  
  // ...
} ;

typedef
  dequeuesetiteratorheader  /* as */ dequeuesetiterator_t;
typedef
  dequeuesetiteratorheader* /* as */ dequeuesetiterator_p;

typedef
  dequeuesetiteratorheader  /* as */ coredequeuesets__dequeuesetiterator_t;
typedef
  dequeuesetiteratorheader* /* as */ coredequeuesets__dequeuesetiterator_p;
  
// ------------------

/* friend */ bool /* func */ coredequesets__sameitemsize
  (/* in */ dequeset* /* param */ A,
  (/* in */ dequeset* /* param */ B);

/* friend */ bool /* func */ coredequesets__sameitemtype
  (/* in */ dequeset* /* param */ A,
  (/* in */ dequeset* /* param */ B);

// ------------------

/* friend */ bool /* func */ coredequeuesets__tryunion
  (/* in */  const dequeueset*  /* param */ A,
   /* in */  const dequeueset*  /* param */ B,
   /* out */       dequeueset** /* param */ ADest);

/* friend */ bool /* func */ coredequeuesets__tryintersect
  (/* in */  const dequeueset*  /* param */ A,
   /* in */  const dequeueset*  /* param */ B,
   /* out */       dequeueset** /* param */ ADest);

/* friend */ bool /* func */ coredequeuesets__trysubstract
  (/* in */  const dequeueset*  /* param */ A,
   /* in */  const dequeueset*  /* param */ B,
   /* out */       dequeueset** /* param */ ADest);

/* friend */ bool /* func */ coredequeuesets__trysimmetry
  (/* in */  const dequeueset*  /* param */ A,
   /* in */  const dequeueset*  /* param */ B,
   /* out */       dequeueset** /* param */ ADest);

// ------------------

/* friend */ dequeueset* /* func */ coredequeuesets__union
  (/* in */  const dequeueset* /* param */ A,
   /* in */  const dequeueset* /* param */ B);

/* friend */ dequeueset* /* func */ coredequeuesets__intersect
  (/* in */  const dequeueset* /* param */ A,
   /* in */  const dequeueset* /* param */ B);

/* friend */ dequeueset* /* func */ coredequeuesets__substract
  (/* in */  const dequeueset* /* param */ A,
   /* in */  const dequeueset* /* param */ B);

/* friend */ dequeueset* /* func */ coredequeuesets__simmetry
  (/* in */  const dequeueset* /* param */ A,
   /* in */  const dequeueset* /* param */ B);

// ------------------

bool /* func */ coredequesets__trynodebyitemfwd
  (/* in */  const dequeueset*   /* param */ ASet,
   /* in */  const pointer*      /* param */ AItem,
   /* out */       dequeuenode** /* param */ ANode);

dequeuenode* /* func */ coredequesets__nodebyitemfwd
  (/* in */ const dequeueset*  /* param */ ASet,
   /* in */ const pointer*     /* param */ AItem);

// ------------------

dequeueset* /* func */ coredequeuesets__createset
  ( noparams );

dequeueset* /* func */ coredequeuesets__createsetbytype
  (/* in */ typecode_t* /* param */ AItemType);

dequeueset* /* func */ coredequeuesets__createsetrestrictcount
  (/* in */ count_t* /* param */ ACount);
  
dequeueset* /* func */ coredequeuesets__createsetbysize
  (/* in */ size_t* /* param */ AItemSize);
  
dequeueset* /* func */ coredequeuesets__createsetbyschema
  (/* in */ const dequeueset* /* param */ ASourceSet);
  
dequeueset* /* func */ coredequeuesets__createsetbycopy
  (/* in */ const dequeueset* /* param */ ASourceSet);

void /* func */ coredequeuesets__dropset
  (/* out */ dequeueset** /* param */ ASet);

// ------------------

bool /* func */ coredequeuesets__underiteration
  (/* in */ dequeueset* /* param */ ASet)

dequeuesetiterator* /* func */ coredequeuesets__createiteratorforward
  (/* in */ dequeueset* /* param */ ASet);

dequeuesetiterator* /* func */ coredequeuesets__createiteratorbackward
  (/* in */ dequeueset* /* param */ ASet);

void /* func */ coredequeuesets__dropiterator
  (/* out */ dequeuesetiterator** /* param */ AIterator);

// ------------------

bool /* func */ coredequeuesets__isdone
  (/* in */ const dequeuesetiterator* /* param */ AIterator);

bool /* func */ coredequeuesets__trymovenext
  (/* inout */ dequeuesetiterator* /* param */ AIterator);

void /* func */ coredequeuesets__movenext
  (/* inout */ dequeuesetiterator* /* param */ AIterator);

bool /* func */ coredequeuesets__trygetitem
  (/* inout */ dequeuesetiterator* /* param */ AIterator
   /* out */   pointer*        /* param */ AItem);

bool /* func */ coredequeuesets__trysetitem
  (/* inout */ dequeuesetiterator* /* param */ AIterator
   /* in */ const pointer*     /* param */ AItem);

void /* func */ coredequeuesets__getitem
  (/* inout */ dequeuesetiterator* /* param */ AIterator
   /* out */   pointer*        /* param */ AItem);

// ------------------

void /* func */ coredequeuesets__assignequalityfunc
  (/* inout */ dequeueset*  /* param */ ASet,
   /* in */    equality /* param */ AMatchFunc);

// ------------------

count_t /* func */ coredequeuesets__getcurrentcount
  (/* in */ const dequeueset* /* param */ ASet);

count_t /* func */ coredequeuesets__getmaxcount
  (/* in */ const dequeueset* /* param */ ASet);

// ------------------

bool /* func */ coredequeuesets__tryclear
  (/* inout */ dequeueset* /* param */ ASet);

bool /* func */ coredequeuesets__isempty
  (/* in */ const dequeueset* /* param */ ASet);

bool /* func */ coredequeuesets__hasmembers
  (/* in */ const dequeueset* /* param */ ASet);

void /* func */ coredequeuesets__clear
  (/* inout */ dequeueset* /* param */ ASet);

// ------------------

bool /* func */ coredequeuesets__tryismember
  (/* in */    const dequeueset* /* param */ ASet,
   /* in */    const pointer*    /* param */ AItem,
   /* inout */ const bool        /* param */ AResult);

bool /* func */ coredequeuesets__ismember
  (/* in */    const dequeueset* /* param */ ASet,
   /* in */    const pointer*    /* param */ AItem);

 // ...
 
// ------------------

bool /* func */ coredequeuesets__tryconcat
  (/* inout */ dequeueset*       /* param */ ADestSet,
  (/* in */    const dequeueset* /* param */ ASourceSet);

bool /* func */ coredequeuesets__tryinclude
  (/* in */    const dequeueset* /* param */ ASet,
   /* in */    const pointer*    /* param */ AItem);

bool /* func */ coredequeuesets__tryexclude
  (/* in */    const dequeueset* /* param */ ASet,
   /* in */    const pointer**   /* param */ AItem);

void /* func */ coredequeuesets__tryconcat
  (/* inout */ dequeueset*       /* param */ ADestSet,
  (/* in */    const dequeueset* /* param */ ASourceSet);

// ------------------

void /* func */ coredequeuesets__include
  (/* in */    const dequeueset* /* param */ ASet,
   /* in */    const pointer*    /* param */ AItem);

void /* func */ coredequeuesets__exclude
  (/* in */    const dequeueset* /* param */ ASet,
   /* in */    const pointer**   /* param */ AItem);




 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coredequeuesets__modulename
  ( noparams );

/* override */ int /* func */ coredequeuesets__setup
  ( noparams );

/* override */ int /* func */ coredequeuesets__setoff
  ( noparams );

// ------------------

#endif // COREDEQUEUESETS__H

// } // namespace coredequeuesets