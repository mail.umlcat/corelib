/** Module: "coremaps.h"
 ** Descr.: "Unique Key and duplicated value,"
 **         "list."
 **/

// namespace coremaps {
 
// ------------------
 
#ifndef COREMAPS_H
#define COREMAPS_H
 
// ------------------
 
#include <stdlib.h"
#include <stddef.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

#include "corekeyvalues.h"
#include "coremapnodes.h"

// ------------------

typedef
  pointer* /* as */ map;
typedef
  map      /* as */ coremaps__map;
  
struct mapheader
{
  // pointer to node before first node
  mapnode* /* var */ FirstMarker;
  
  // pointer to node before last node
  mapnode* /* var */ LastMarker;

  // only applies when items are same type
  // "unknowntype" value means different type
  typecode /* var */ ItemsKeyType;
  typecode /* var */ ItemsValueType;

  // applies instead of "typecode"
  // "0" means not used
  size_t   /* var */ ItemsKeySize;
  size_t   /* var */ ItemsValueSize;

  // restricts how many items can be stored
  // "0" means not used
  count_t  /* var */ ItemsMaxCount;

  // how many items are currently stored
  count_t  /* var */ ItemsCurrentCount;
  
  // indicates iterator related objects
  count_t  /* var */ IteratorCount;
  
  equality /* var */ MatchFunc;
  
  // ...
} ;

typedef
  mapheader  /* as */ map_t;
typedef
  mapheader* /* as */ map_p;

typedef
  map_t      /* as */ coremaps__map_t;
typedef
  map_p      /* as */ coremaps__map_p;

// ------------------

typedef
  pointer*       /* as */ mapiterator;

struct mapiteratorheader
{
  map*           /* var */ MainMap;

  mapnode*       /* var */ CurrentNode;

  index_t        /* var */ ItemsIndex;

  count_t        /* var */ ItemsCount;
  size_t         /* var */ ItemsSize;

  mapdirections /* var */ Direction;
  
  // ...
} ;

typedef
  mapiterator       /* as */ mapiterator_t;
typedef
  mapiterator*      /* as */ mapiterator_p;

typedef
  mapiterator_t     /* as */ coremaps__mapiterator_t;
typedef
  mapiterator_p     /* as */ coremaps__mapiterator_p;

// ------------------

map* /* func */ coremaps__createmap
  ( noparams );

map* /* func */ coremaps__createmapbytype
  (/* in */ const typecode_t /* param */ AKeyType,
   /* in */ const typecode_t /* param */ AValueType);

map* /* func */ coremaps__createmapbysize
  (/* in */ size_t* /* param */ AKeySize,
   /* in */ size_t* /* param */ AValueSize);

void /* func */ coremaps__dropmap
  (/* out */ map** /* param */ AMap);

// ------------------

bool /* func */ coremaps__underiteration
  (/* in */ map* /* param */ AMap);

mapiterator* /* func */ coremaps__createiteratorforward
  (/* in */ map* /* param */ AMap);

mapiterator* /* func */ coremaps__createiteratorbackward
  (/* in */ map* /* param */ AMap);

void /* func */ coremaps__dropiterator
  (/* out */ mapiterator** /* param */ AIterator);

// ------------------

void /* func */ coremaps__assignequalityfunc
  (/* inout */ map*     /* param */ AMap,
   /* in */    equality /* param */ AMatchFunc);

// ------------------

bool /* func */ coremaps__isdone
  (/* in */ const mapiterator* /* param */ AIterator);

bool /* func */ coremaps__trymovenext
  (/* inout */ mapiterator* /* param */ AIterator);

void /* func */ coremaps__movenext
  (/* inout */ mapiterator* /* param */ AIterator);

// ------------------

bool /* func */ coremaps__trygetnode
  (/* inout */ mapiterator* /* param */ AIterator
   /* out */   mapnode*     /* param */ ANode);

bool /* func */ coremaps__trygetitem
  (/* inout */ mapiterator* /* param */ AIterator
   /* out */   keyvalue**   /* param */ AItem);

bool /* func */ coremaps__trygetkey
  (/* inout */ mapiterator* /* param */ AIterator
   /* out */   pointer**    /* param */ AKey);

bool /* func */ coremaps__trygetvalue
  (/* inout */ mapiterator* /* param */ AIterator
   /* out */   pointer**    /* param */ AValue);

bool /* func */ coremaps__trysetvalue
  (/* inout */ mapiterator* /* param */ AIterator
   /* out */   pointer*     /* param */ AValue);

mapnode* /* func */ coremaps__getnode
  (/* inout */ mapiterator* /* param */ AIterator);

void /* func */ coremaps__getitem
  (/* inout */ mapiterator* /* param */ AIterator
   /* out */   keyvalue**   /* param */ AItem);

void /* func */ coremaps__getkey
  (/* inout */ mapiterator* /* param */ AIterator,
   /* out */   pointer**    /* param */ AKey);

void /* func */ coremaps__getvalue
  (/* inout */ mapiterator* /* param */ AIterator,
   /* out */   pointer**    /* param */ AValue);

void /* func */ coremaps__setvalue
  (/* inout */ mapiterator* /* param */ AIterator
   /* out */   pointer*     /* param */ AValue);

// ------------------

count_t /* func */ coremaps__getcurrentcount
  (/* in */ const map* /* param */ AMap);

count_t /* func */ coremaps__getmaxcount
  (/* in */ const map* /* param */ AMap);

// ------------------

void /* func */ coremaps__dropkeyvalue
  (/* inout */ map*     /* param */ AMap,
   /* in */    mapnode* /* param */ AMapNode);

bool /* func */ coremaps__tryclear
  (/* inout */ map* /* param */ AMap);

bool /* func */ coremaps__isempty
  (/* in */ const map* /* param */ AMap);

bool /* func */ coremaps__isfull
  (/* in */ const map* /* param */ AStack);

bool /* func */ coremaps__hasitems
  (/* in */ const map* /* param */ AMap);

void /* func */ coremaps__clear
  (/* inout */ map* /* param */ AMap);

// ------------------

bool /* func */ coremaps__trykeyfound
  (/* inout */ map*           /* param */ AMap,
   /* in */    const pointer* /* param */ AKey,
   /* out */   bool*          /* param */ AKeyFound);

bool /* func */ coremaps__tryitembykey
  (/* inout */ map*           /* param */ AMap,
   /* in */    const pointer* /* param */ AKey,
   /* out */   pointer*       /* param */ AValue);

bool /* func */ coremaps__keyfound
  (/* inout */ map*           /* param */ AMap,
   /* in */    const pointer* /* param */ AKey);

pointer* /* func */ coremaps__itembykey
  (/* inout */ map*           /* param */ AMap,
   /* in */    const pointer* /* param */ AKey);

// ------------------

bool /* func */ coremaps__trydropkey
  (/* inout */ map*     /* param */ AMap,
   /* out */   pointer* /* param */ AKey);

bool /* func */ coremaps__trydropvalue
  (/* inout */ map*     /* param */ AMap,
   /* out */   pointer* /* param */ AValue);

// ------------------

bool /* func */ coremaps__tryinsert
  (/* inout */ map*     /* param */ AMap,
   /* in */    pointer* /* param */ AKey,
   /* in */    pointer* /* param */ AValue);

bool /* func */ coremaps__tryupdate
  (/* inout */ map*     /* param */ AMap,
   /* in */    pointer* /* param */ AKey,
   /* in */    pointer* /* param */ AValue);

bool /* func */ coremaps__tryreinsert
  (/* inout */ map*     /* param */ AMap,
   /* in */    pointer* /* param */ AKey,
   /* in */    pointer* /* param */ AValue);

void /* func */ coremaps__insert
  (/* inout */ map*     /* param */ AMap,
   /* in */    pointer* /* param */ AKey,
   /* in */    pointer* /* param */ AValue);

void /* func */ coremaps__update
  (/* inout */ map*     /* param */ AMap,
   /* in */    pointer* /* param */ AKey,
   /* in */    pointer* /* param */ AValue);

void /* func */ coremaps__reinsert
  (/* inout */ map*     /* param */ AMap,
   /* in */    pointer* /* param */ AKey,
   /* in */    pointer* /* param */ AValue);

// ------------------

bool /* func */ coremaps__tryextract
  (/* inout */ /* restricted */ map*           /* param */ AMap,
   /* in */    /* restricted */ const pointer* /* param */ AKey,
   /* out */   /* restricted */ pointer*       /* param */ AValue);

pointer* /* func */ coremaps__extract
  (/* inout */ /* restricted */ map*           /* param */ AMap,
   /* in */    /* restricted */ const pointer* /* param */ AKey,
   /* out */   /* restricted */ pointer*       /* param */ AValue);

bool /* func */ coremaps__trydelete
  (/* inout */ /* restricted */ map*           /* param */ AMap,
   /* in */    /* restricted */ const pointer* /* param */ AKey);

pointer* /* func */ coremaps__delete
  (/* inout */ /* restricted */ map*           /* param */ AMap,
   /* in */    /* restricted */ const pointer* /* param */ AKey);

// ------------------

bool /* func */ coremaps__tryexchange
  (/* inout */ /* restricted */ map*           /* param */ AMap,
   /* in */    /* restricted */ const pointer* /* param */ AFirstKey,
   /* in */    /* restricted */ const pointer* /* param */ ASecondKey);

void /* func */ coremaps__exchange
  (/* inout */ /* restricted */ map*           /* param */ AMap,
   /* in */    /* restricted */ const pointer* /* param */ AFirstKey,
   /* in */    /* restricted */ const pointer* /* param */ ASecondKey);

// ------------------


 // ...
 
// ------------------

/* override */ int /* func */ coremaps__setup
  ( noparams );

/* override */ int /* func */ coremaps__setoff
  ( noparams );

// ------------------

#endif // COREMAPS_H

// } // namespace coremaps