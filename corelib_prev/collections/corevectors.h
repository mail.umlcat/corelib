/** Module: "corevectors.h"
 ** Descr.: "Dynamically allocated one single dimension array."
 **/
 
// namespace corevectors {
 
// ------------------
 
#ifndef COREVECTORS__H
#define COREVECTORS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

// opaque type
typedef
  pointer* /* as */ vector;
typedef
  pointer* /* as */ corevectors__vector;

struct vectorheader
{
  // pointer to first item
  pointer* /* var */ ItemsFirst;

  // pointer to last item
  pointer* /* var */ ItemsLast;

  // only applies when items are same type
  // "unknowntype" value means different type
  typecode  /* var */ ItemsType;

  // applies instead of "typecode"
  // "0" means not used
  size_t    /* var */ ItemsSize;

  // for a vector is always "1"
  /* readonly */
  count_t   /* var */ DimsCount;

  // how many items can be stored
  count_t   /* var */ ItemsMaxCount;

  // indicates iterator related objects
  count_t   /* var */ IteratorCount;

  equality /* var */ MatchFunc;
  
  // ...
} ;

typedef
  vector   /* as */ vector_t;
typedef
  vector*  /* as */ vector_p;

typedef
  vector_t /* as */ corevectors__vector_t;
typedef
  vector_p /* as */ corevectors__vector_p;

// ------------------

struct vectoriteratorheader
{
  vector*        /* var */ Vector;

  pointer*       /* var */ CurrentItem;
  
  // pointer to first item
  pointer*       /* var */ ItemsFirst;

  count_t        /* var */ ItemsCount;
  size_t         /* var */ ItemsSize;

  listdirections /* var */ Direction;
  
  // ...
} ;

typedef
  vectoriteratorheader /* as */ vectoriterator;
typedef
  vectoriterator       /* as */ vectoriterator_t;
typedef
  vectoriterator*      /* as */ vectoriterator_p;

typedef
  vectoriterator_t     /* as */ corevectors__vectoriterator_t;
typedef
  vectoriterator_p     /* as */ corevectors__vectoriterator_p;

// ------------------

/* friend */ bool /* func */ corevectors__sameitemsize
  (/* in */ vector* /* param */ AFirstVector,
  (/* in */ vector* /* param */ ASecondVector);

/* friend */ bool /* func */ corevectors__sameitemtype
  (/* in */ vector* /* param */ AFirstVector,
  (/* in */ vector* /* param */ ASecondVector);

/* friend */ bool /* func */ corevectors__sameitemcount
  (/* in */ vector* /* param */ AFirstVector,
  (/* in */ vector* /* param */ ASecondVector);

// ------------------

/* friend */ bool /* func */ corevectors__trycopy
  (/* in */  vector*  /* param */ ASourceVector,
  (/* out */ vector** /* param */ ADestVector);

/* friend */ bool /* func */ corevectors__trycopyall
  (/* in */  vector*  /* param */ ASourceVector,
  (/* out */ vector** /* param */ ADestVector);

/* friend */ bool /* func */ corevectors__trycopyreverse
  (/* in */  vector*  /* param */ ASourceVector,
  (/* out */ vector** /* param */ ADestVector);

/* friend */ bool /* func */ corevectors__trycopyconcat
  (/* in */  vector*  /* param */ AFirstVector,
   /* in */  vector*  /* param */ ASecondVector,
   /* out */ vector** /* param */ ADestVector);
 
// ------------------

/* friend */ void /* func */ corevectors__copyempty
  (/* in */  vector*  /* param */ ASourceVector,
  (/* out */ vector** /* param */ ADestVector);

/* friend */ void /* func */ corevectors__copyall
  (/* in */  vector*  /* param */ ASourceVector,
  (/* out */ vector** /* param */ ADestVector);
  
/* friend */ void /* func */ corevectors__copyreverse
  (/* in */  vector*  /* param */ ASourceVector,
  (/* out */ vector** /* param */ ADestVector);

/* friend */ void /* func */ corevectors__copyconcat
  (/* in */  vector*  /* param */ AFirstVector,
   /* in */  vector*  /* param */ ASecondVector,
   /* out */ vector** /* param */ ADestVector);

// ------------------

vector* /* func */ corevectors__createvector
  ( noparams );

vector* /* func */ corevectors__createvectorbyitemtype
  (/* in */ typecode_t /* param */ AItemType);
  
vector* /* func */ corevectors__createvectorbyitemtypecount
  (/* in */ typecode_t /* param */ AItemType,
   /* in */ count_t    /* param */ AItemMaxCount);

vector* /* func */ corevectors__createvectorbyitemsizecount
  (/* in */ size_t  /* param */ AItemSize,
   /* in */ count_t /* param */ AItemMaxCount);

vector* /* func */ corevectors__createvectorbycopy
  (/* in */ const vector* /* param */ ASourceVector);

void /* func */ corevectors__dropvector
  (/* out */ vector** /* param */ AVector);

// ------------------

bool /* func */ corevectors__underiteration
  (/* in */ vector* /* param */ AVector)

vectoriterator* /* func */ corevectors__createiteratorfwd
  (/* in */ vector* /* param */ AVector);

vectoriterator* /* func */ corevectors__createiteratorback
  (/* in */ vector* /* param */ AVector);

void /* func */ corevectors__dropiterator
  (/* out */ vectoriterator** /* param */ AIterator);

// ------------------

bool /* func */ corevectors__isdone
  (/* in */ const vectoriterator* /* param */ AIterator);

void /* func */ corevectors__movenext
  (/* inout */ vectoriterator* /* param */ AIterator);

void /* func */ corevectors__readitem
  (/* inout */ vectoriterator* /* param */ AIterator
   /* out */   pointer*        /* param */ AItem);

void /* func */ corevectors__writeitem
  (/* inout */ vectoriterator* /* param */ AIterator
   /* in */    const pointer*  /* param */ AItem);

// ------------------

bool /* func */ corevectors__isvalidindex
  (/* in */ const vector* /* param */ AVector,
   /* in */ const index_t /* param */ AIndex);

typecode_t /* func */ corevectors__readitemtype
  (/* in */ const vector* /* param */ AVector);

size_t /* func */ corevectors__readitemsize
  (/* in */ const vector* /* param */ AVector);
  
// ------------------

count_t /* func */ corevectors__getmaxcount
  (/* in */ const vector* /* param */ AVector);

count_t /* func */ corevectors__getdimcount
  (/* in */ const vector* /* param */ AVector);

// ------------------

bool /* func */ corevectors__tryclear
  (/* inout */ vector* /* param */ AVector);

void /* func */ corevectors__clear
  (/* inout */ vector* /* param */ AVector);

// ------------------

bool /* func */ corevectors__trygetitemat
  (/* in */  /* restricted */ const vector* /* param */ AVector,
   /* in */  /* restricted */ const index_t /* param */ AIndex,
   /* out */ /* restricted */ pointer**     /* param */ AItem);

bool /* func */ corevectors__tryreaditemat
  (/* in */  /* restricted */ const vector* /* param */ AVector,
   /* in */  /* restricted */ const index_t /* param */ AIndex,
   /* out */ /* restricted */ pointer*      /* param */ AItem);

bool /* func */ corevectors__trywriteitemat
  (/* in */ /* restricted */ const vector*  /* param */ AVector,
   /* in */ /* restricted */ const index_t  /* param */ AIndex,
   /* in */ /* restricted */ const pointer* /* param */ AItem);

pointer* /* func */ corevectors__readitemat
  (/* in */ /* restricted */ const vector* /* param */ AVector,
   /* in */ /* restricted */ const index_t /* param */ AIndex);

void /* func */ corevectors__readitemat
  (/* in */  /* restricted */ const vector* /* param */ AVector,
   /* in */  /* restricted */ const index_t /* param */ AIndex,
   /* out */ /* restricted */ pointer*      /* param */ AItem);

void /* func */ corevectors__writeitemat
  (/* in */ /* restricted */ const vector*  /* param */ AVector,
   /* in */ /* restricted */ const index_t  /* param */ AIndex,
   /* in */ /* restricted */ const pointer* /* param */ AItem);

// ------------------

bool /* func */ corevectors__tryclearat
  (/* inout */ /* restricted */ vector*       /* param */ AVector,
   /* in */    /* restricted */ const index_t /* param */ AIndex);

bool /* func */ corevectors__tryexchangeat
  (/* inout */ /* restricted */ vector*       /* param */ AVector,
   /* in */    /* restricted */ const index_t /* param */ AFirstIndex,
   /* in */    /* restricted */ const index_t /* param */ ASecondIndex);

void /* func */ corevectors__clearat
  (/* inout */ /* restricted */ vector*       /* param */ AVector,
   /* in */    /* restricted */ const index_t /* param */ AIndex);
  
void /* func */ corevectors__exchangeat
  (/* inout */ /* restricted */ vector*       /* param */ AVector,
   /* in */    /* restricted */ const index_t /* param */ AFirstIndex,
   /* in */    /* restricted */ const index_t /* param */ ASecondIndex);

// ------------------



 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ corevectors__modulename
  ( noparams );

/* override */ int /* func */ corevectors__setup
  ( noparams );

/* override */ int /* func */ corevectors__setoff
  ( noparams );

// ------------------

#endif // COREVECTORS__H

// } // namespace corevectors