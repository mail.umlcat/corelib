/** Module: "coreansidictnodes.c"
 ** Descr.: "Unique Key and duplicated value,"
 **         "list items."
 **/

// namespace coreansidictnodes {
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansikeyvalues.h"

// ------------------

#include "coreansidictnodes.h"
 
// ------------------

/* friend */ bool /* func */ coreansidictnodes__arelinked
  (/* in */ const ansidictionarynode* /* param */ A,
   /* in */ const ansidictionarynode* /* param */ B)
{
  bool /* var */ Result = false;
  // ---

  Result = (A != NULL) && (B != NULL);
  if (Result)
  {
      ansidictionarynodeheader* /* var */ ThisFirstDictNode  = NULL;
      ansidictionarynodeheader* /* var */ ThisSecondDictNode = NULL;

      ThisFirstDictNode  = (ansidictionarynodeheader*) A;
      ThisSecondDictNode = (ansidictionarynodeheader*) B;
      
      Result =
        (ThisFirstDictNode->Next  == ThisSecondDictNode) &&
        (ThisSecondDictNode->Prev == ThisFirstDictNode);
  } // if
    
  // ---
  return Result;
} // func

/* friend */ bool /* func */ coreansidictnodes__trylink
  (/* inout */ ansidictionarynode* /* param */ A,
   /* inout */ ansidictionarynode* /* param */ B);
{
  bool /* var */ Result = false;
  // ---

  Result = (A != NULL) && (B != NULL);
  if (Result)
  {
      ansidictionarynodeheader* /* var */ ThisFirstDictNode  = NULL;
      ansidictionarynodeheader* /* var */ ThisSecondDictNode = NULL;

      ThisFirstDictNode  = (ansidictionarynodeheader*) A;
      ThisSecondDictNode = (ansidictionarynodeheader*) B;
      
      ThisFirstDictNode->Next  = ThisSecondDictNode;
      ThisSecondDictNode->Prev = ThisFirstDictNode;
  } // if
} // func

/* friend */ bool /* func */ coreansidictnodes__trymoveforward
  (/* inout */ ansidictionarynode* /* param */ ADictNode)
{
  bool /* var */ Result = false;
  // ---
  
  Result = (ADictNode != NULL);
  if (Result)
  {
    listnodeheader** /* var */ ThisDictNode = NULL;
   
    ThisDictNode  = (listnode**) A;

    *ThisDictNode = *ThisDictNode->Next = B;
  } // if
     
  // ---
  return Result;
} // func

/* friend */ bool /* func */ coreansidictnodes__trymovebackward
  (/* inout */ ansidictionarynode* /* param */ ADictNode)
{
  bool /* var */ Result = false;
  // ---
  
  Result = (ADictNode != NULL);
  if (Result)
  {
    listnodeheader** /* var */ ThisDictNode = NULL;
   
    ThisDictNode  = (listnode**) A;

    *ThisDictNode = *ThisDictNode->Next = B;
  } // if
     
  // ---
  return Result;
} // func

// ------------------

/* friend */ void /* func */ coreansidictnodes__link
  (/* inout */ ansidictionarynode* /* param */ A,
   /* inout */ ansidictionarynode* /* param */ B)
{
  /* discard */ coreansidictnodes__trylink(A, B);
} // func

/* friend */ void /* func */ coreansidictnodes__moveforward
  (/* inout */ ansidictionarynode** /* param */ ADictNode)
{
  /* discard */ coreansidictnodes__moveforward(ADictNode);
} // func

/* friend */ void /* func */ coreansidictnodes__movebackward
  (/* inout */ ansidictionarynode** /* param */ ADictNode)
{
  /* discard */ coreansidictnodes__movebackward(ADictNode);
} // func

// ------------------

ansidictionarynode* /* func */ coreansidictnodes__createnodeempty
  ( noparams )
{
  ansidictionarynode* /* var */ Result = NULL;
  // ---
  
  if (AKey != NULL)
  {
    ansidictionarynodeheader* /* var */ ThisDictNode = NULL;
  	  
    ThisDictNode =
      corememory__clearallocate
        (sizeof(ansidictionarynodeheader));
    ThisDictNode->KeyValue = NULL;
    
    Result =
      corememory__transfer(&ThisDictNode);
  } // if
     
  // ---
  return Result;
} // func

ansidictionarynode* /* func */ coreansidictnodes__createnodebykey
  (/* in */ pointer* /* param */ AKey)
{
  ansidictionarynode* /* var */ Result = NULL;
  // ---
  
  if (AKey != NULL)
  {
    ansidictionarynodeheader* /* var */ ThisDictNode = NULL;
  	 
    ThisDictNode =
      corememory__clearallocate
        (sizeof(ansidictionarynodeheader));
    ThisDictNode->KeyValue =
      corekeyvalues__createkeyvaluebykey
        (AKey);
        
    Result =
      corememory__transfer(&ThisDictNode);
  } // if
     
  // ---
  return Result;
} // func

ansidictionarynode* /* func */ coreansidictnodes__createnodebyvalue
  (/* in */ pointer* /* param */ AKey,
   /* in */ pointer* /* param */ AValue)
{
  ansidictionarynode* /* var */ Result = NULL;
  // ---
  
  if (AKey != NULL)
  {
    ansidictionarynodeheader* /* var */ ThisDictNode = NULL;
  	  
    ThisDictNode =
      corememory__clearallocate
        (sizeof(ansidictionarynodeheader));
    ThisDictNode->KeyValue =
      corekeyvalues__createkeyvaluebyvalue
        (AKey, AValue);
        
    Result =
      corememory__transfer(&ThisDictNode);
  } // if
         
  // ---
  return Result;
} // func

ansidictionarynode* /* func */ coreansidictnodes__createnodebykeyvalue
  (/* in */ pointer* /* param */ AKeyValue)
{
  ansidictionarynode* /* var */ Result = NULL;
  // ---
   
  if (AKeyValue != NULL)
  {
    ansidictionarynodeheader* /* var */ ThisDictNode = NULL;
  	
    ThisDictNode =
      corememory__clearallocate
        (sizeof(ansidictionarynodeheader));
            
    ThisDictNode->KeyValue =
        (AKeyValue);
        
    Result =
      corememory__transfer(&ThisDictNode);
  } // if
     
  // ---
  return Result;
} // func

// ------------------

void /* func */ coreansidictnodes__dropnode
  (/* out */ ansidictionarynode**  /* param */ ADictNode)
{
  bool /* var */ CanContinue = FALSE;

  CanContinue =
    (ADictNode != NULL);
 
  if (CanContinue)
  {
    corememory__cleardeallocate
      (ADictNode, sizeof(ansidictionarynodeheader));
  } // if
} // func

void /* func */ coreansidictnodes__dropnodebysize
  (/* out */ ansidictionarynode** /* param */ ADictNode,
   /* in */  size_t               /* param */ AKeySize,
   /* in */  size_t               /* param */ AValueSize)
{
  bool /* var */ CanContinue = FALSE;

  CanContinue =
    (AKeyValue != NULL) && (AKeySize > 0) && (AValueSize > 0);
  if (CanContinue)
  {
    ansidictionarynodeheader* /* var */ ThisDictNode = NULL;
    keyvalue*      /* var */ AKeyValue = NULL;
   
    ThisDictNode =
      (ansidictionarynodeheader*) ADictNode;

    AKeyValue =
      ThisDictNode->KeyValue;
      
    corekeyvalues__dropbysize
      (&AKeyValue, AKeySize, AValueSize);
      
    corememory__cleardeallocate
      (ADictNode, sizeof(ansidictionarynodeheader));
  } // if
} // func

void /* func */ coreansidictnodes__dropnodeextractkeyvalue
  (/* out */ ansidictionarynode** /* param */ ADictNode,
   /* out */ keyvalue**           /* param */ AKeyValue)
{
  bool /* var */ CanContinue = FALSE;

  CanContinue =
    (AKeyValue != NULL) &&
    (ADictNode != NULL);
 
  if (CanContinue)
  {
    ansidictionarynodeheader* /* var */ ThisDictNode = NULL;
    
    ThisDictNode =
      (ansidictionarynodeheader*) ADictNode;

    AKeyValue =
      corememory__transfer(&(ThisDictNode->KeyValue));
 
    corememory__cleardeallocate
      (ADictNode, sizeof(ansidictionarynodeheader));
  } // if
} // func

void /* func */ coreansidictnodes__dropnodeextract
  (/* out */ ansidictionarynode** /* param */ ADictNode,
   /* out */ pointer**            /* param */ AKey,
   /* out */ pointer**            /* param */ AValue)
{
  bool /* var */ CanContinue = FALSE;

  CanContinue =
    (ADictNode != NULL) &&
    (AKey != NULL) &&
    (AValue != NULL);
 
  if (CanContinue)
  {
    ansidictionarynodeheader* /* var */ ThisDictNode = NULL;
    
    ThisDictNode =
      (ansidictionarynodeheader*) ADictNode;

    AKeyValue =
      corememory__transfer(&(ThisDictNode->KeyValue));
 
    corememory__cleardeallocate
      (ADictNode, sizeof(ansidictionarynodeheader));
  } // if
} // func

// ------------------

pointer* /* func */ coreansidictnodes__getkey
  (/* inout */ ansidictionarynode* /* param */ ADictNode)
{
  pointer* /* var */ Result = NULL;
  // ---

  Result = (ADictNode != NULL);
  if (Result)
  {
    ansidictionarynodeheader*  /* var */ ThisDictNode  = NULL;
    keyvalueheader* /* var */ ThisKeyValue = NULL;

    ThisDictNode =
      (ansidictionarynodeheader*) corememory__share(ADictNode);

    ThisKeyValue =
      (keyvalueheader*) corememory__share
        (ThisDictNode->KeyValue);
      
    Result =
      ThisKeyValue->Key;
  } // if
    
  // ---
  return Result;
} // func

pointer* /* func */ coreansidictnodes__getvalue
  (/* inout */ ansidictionarynode* /* param */ ADictNode)

// ------------------

bool /* func */ coreansidictnodes__MatchKey
  (/* in */ ansidictionarynode* /* param */ ADictNode,
   /* in */ pointer*            /* param */ AKey,
   /* in */ equality            /* param */ MatchFunc)
{
  bool /* var */ Result = FALSE;


  Result =
    (ADictNode != NULL) &&
    (AKey != NULL) &&
    (MatchFunc != NULL);
 
  if (Result)
  {
    ansidictionarynodeheader*  /* var */ ThisDictNode  = NULL;
    keyvalueheader* /* var */ ThisKeyValue = NULL;
    keyvalue*       /* var */ AKeyValue = NULL;

    ThisDictNode =
      (ansidictionarynodeheader*) ADictNode;

    Result =
      MatchFunc(ThisDictNode->Key, AKey);
  } // if
  
  
} // func

// ------------------

pointer* /* func */ coreansidictnodes__replacekey
  (/* out */ ansidictionarynode* /* param */ ADictNode,
   /* in */  pointer*            /* param */ AKey)
{
  pointer* /* var */ Result = NULL;
  // ---
  
  bool /* var */ CanContinue = FALSE;

  CanContinue =
    (ADictNode != NULL) &&
    (AKey != NULL);
 
  if (CanContinue)
  {
    ansidictionarynodeheader*  /* var */ ThisDictNode  = NULL;
    keyvalueheader* /* var */ ThisKeyValue = NULL;

    ThisDictNode =
      (ansidictionarynodeheader*) corememory__share(ADictNode);

    Result =
      corekeyvalues__replacekey
        (ThisDictNode->KeyValue, AKey);
  } // if
     
  // ---
  return Result;
} // func

pointer* /* func */ coreansidictnodes__replacevalue
  (/* out */ ansidictionarynode* /* param */ ADictNode,
   /* in */  pointer*            /* param */ AValue)
{
  pointer* /* var */ Result = NULL;
  // ---
  
  bool /* var */ CanContinue = FALSE;

  CanContinue =
    (ADictNode != NULL) &&
    (AValue != NULL);
 
  if (CanContinue)
  {
    ansidictionarynodeheader*  /* var */ ThisDictNode  = NULL;
    keyvalueheader* /* var */ ThisKeyValue = NULL;

    ThisDictNode =
      (ansidictionarynodeheader*) corememory__share(ADictNode);

    Result =
      corekeyvalues__replacevalue
        (ThisDictNode->KeyValue, AValue);
  } // if
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreansidictnodes__tryextractkeyvalue
  (/* out */ ansidictionarynode* /* param */ ADictNode,
   /* out */ keyvalue**          /* param */ AKeyValue)
{
  bool /* var */ Result = NULL;
  // ---
  
  Result =
    (ADictNode != NULL) &&
    (AKeyValue != NULL);
 
  if (Result)
  {
    ansidictionarynodeheader*  /* var */ ThisDictNode  = NULL;

    ThisDictNode =
      (ansidictionarynodeheader*) corememory__share(ADictNode);

    AKeyValue =
      corememory__transfer(&(ThisDictNode->KeyValue));
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coreansidictnodes__tryextractitems
  (/* out */ ansidictionarynode* /* param */ ADictNode,
   /* out */ pointer**           /* param */ AKey,
   /* out */ pointer**           /* param */ AValue)
{
  bool /* var */ Result = NULL;
  // ---
  
  Result =
    (ADictNode != NULL) &&
    (AKey != NULL) &&
    (AValue != NULL);
 
  if (Result)
  {
    ansidictionarynodeheader*  /* var */ ThisDictNode  = NULL;
    keyvalue*       /* var */ AKeyValue = NULL;
    
    ThisDictNode =
      (ansidictionarynodeheader*) corememory__share(ADictNode);

    AKeyValue =
      corememory__transfer(&(ThisDictNode->KeyValue));
    
    corekeyvalues__dropextract
      (&AKeyValue, AKey, AValue);
  } // if
     
  // ---
  return Result;
} // func
   
void /* func */ coreansidictnodes__extractkeyvalue
  (/* out */ ansidictionarynode* /* param */ ADictNode,
   /* out */ keyvalue**          /* param */ AKeyValue)
{
  /* discard */ coreansidictnodes__tryextractkeyvalue
    (ADictNode, AKeyValue);
} // func

void /* func */ coreansidictnodes__extractitems
  (/* out */ ansidictionarynode* /* param */ ADictNode,
   /* out */ pointer**           /* param */ AKey,
   /* out */ pointer**           /* param */ AValue)
{
  /* discard */ coreansidictnodes__tryextractitems
    (ADictNode, AKey, AValue);
} // func


// ------------------

 // ...



// ------------------

/* override */ const ansinullstring* /* func */ coreansidictnodes__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "coreansidictnodes";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coreansidictnodes__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreansidictnodes__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coreansidictnodes