/** Module: "coreiterators.c"
 ** Descr.: "Definitions for traversing Data Structures."
 **/

// namespace coreiterators {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------
 
#include "coreiterators.h"
 
// ------------------


/* override */ const ansinullstring* /* func */ coreiterators__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "coreiterators";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coreiterators__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreiterators__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coreiterators