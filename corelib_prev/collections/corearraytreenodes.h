/** Module: "corearraytreenodes.h"
 ** Descr.: "..."
 **/
 
// namespace corearraytreenodes {
 
// ------------------
 
#ifndef COREARRAYTREENODES__H
#define COREARRAYTREENODES__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"
#include "corearraylistnodes.h"
#include "corearraylists.h"

// ------------------

// opaque type
typedef
  pointer* /* as */ arraytreenode;
typedef
  pointer* /* as */ arraytreelist;
typedef
  pointer* /* as */ arraytreeiterator;

// ------------------

struct arraytreenodeheader
{
  // inverse reference to tree,
  // that manages this node
  arraytreelist* /* var */ Tree;

  // inverse reference to list,
  // that manages this node
  arraylist*     /* var */ List;

  // inverse reference to listnode,
  // that manages this node
  arraylistnode* /* var */ ParentListNode;

  // inverse reference to treenode,
  // that manages this node
  arraytreenode* /* var */ ParentTreeNode;

  // reference to list,
  // that manages subordinate nodes
  arraylist*     /* var */ Nodes;

  // polymorphic data to be contained,
  // by the node
  pointer*       /* var */ Item;
} ;

typedef
  arraytreeodeheader   /* as */ arraytreenode_t;
typedef
  arraytreenodeheader* /* as */ arraytreenode_p;
  
typedef
  arraytreenodeheader  /* as */ corearraytreenodes__arraytreenode_t;
typedef
  arraytreenodeheader* /* as */ corearraytreenodes__arraytreenode_p;

// ------------------

/* friend */ bool /* func */ corearraytreenodes__arelinked
  (/* in */ const arraytreenode* /* param */ A,
   /* in */ const arraytreenode* /* param */ B);

/* friend */ bool /* func */ corearraytreenodes__trymoveforward
  (/* inout */ arraytreenode** /* param */ ANode);

/* friend */ bool /* func */ corearraytreenodes__trymovebackward
  (/* inout */ arraytreenode** /* param */ ANode);

// ------------------

/* friend */ void /* func */ corearraytreenodes__moveforward
  (/* inout */ arraytreenode** /* param */ ANode);

/* friend */ void /* func */ corearraytreenodes__movebackward
  (/* inout */ arraytreenode** /* param */ ANode);

// ------------------

arraytreenode* /* func */ corearraytreenodes__createnode
  (/* in */ const arraytree*     /* param */ ATree,
   /* in */ const arraylist*     /* param */ AList,
   /* in */ const arraytreenode* /* param */ AParentNode,
   /* in */ const arraylistnode* /* param */ AListNode,
   /* in */ index_t              /* param */ AIndex,
   /* in */ pointer*             /* param */ AItem)

pointer* /* func */ corearraytreenodes__dropnode
  (/* out */ arraytreenode** /* param */ ANode);

// ------------------

count_t /* func */ corearraytreenodes__getcurrentcount
  (/* in */ const arraytreenode* /* param */ ANode);

count_t /* func */ corearraytreenodes__getmaxcount
  (/* in */ const arraytreenode* /* param */ ANode);
  
index_t /* func */ corearraytreenodes__getindex
  (/* in */ const arraytreenode* /* param */ ANode);

// ------------------

bool /* func */ corearraytreenodes__isroot
  (/* in */ const arraytreenode* /* param */ ANode);

bool /* func */ corearraytreenodes__isempty
  (/* in */ const arraytreenode* /* param */ ANode);

bool /* func */ corearraytreenodes__hasitems
  (/* in */ const arraytreenode* /* param */ ANode);

// ------------------

bool /* func */ corearraytreenodes__tryclear
  (/* inout */ arraytreenode* /* param */ ANode);

void /* func */ corearraytreenodes__clear
  (/* inout */ arraytreenode* /* param */ ANode);
  
// ------------------

bool /* func */ corearraytreenodes__isvalidindex
  (/* in */ const arraytreenode* /* param */ ANode
   /* in */ const index_t        /* param */ AIndex);

bool /* func */ corearraytreenodes__trygetitemat
  (/* in */  const arraytreenode* /* param */ ANode,
   /* in */  const index_t        /* param */ AIndex,
   /* out */ pointer**            /* param */ AItem);

bool /* func */ corearraytreenodes__tryreaditemat
  (/* in */  const arraytreenode* /* param */ ANode,
   /* in */  const index_t        /* param */ AIndex,
   /* out */ pointer*             /* param */ AItem);

bool /* func */ corearraytreenodes__trywriteitemat
  (/* in */ const arraytreenode* /* param */ ANode,
   /* in */ const index_t        /* param */ AIndex);

pointer* /* func */ corearraytreenodes__getitemat
  (/* in */  const arraytreenode* /* param */ ANode,
   /* in */  const index_t        /* param */ AIndex);

void /* func */ corearraytreenodes__writeitemat
  (/* in */  const arraytreenode* /* param */ ANode,
   /* in */  const index_t        /* param */ AIndex,
   /* out */ pointer*             /* param */ AItem);

void /* func */ corearraytreenodes__readitemat
  (/* in */ const arraytreenode* /* param */ ANode,
   /* in */ const index_t        /* param */ AIndex);

// ------------------

listnode* /* func */ corearraytreenodes__listnodetotreenode
  (/* in  */ const arraytreenode*  /* param */ ANode);

bool /* func */ corearraytreenodes__tryseeknodeatfwd
  (/* in  */ const arraytreenode*  /* param */ AParentNode,
   /* in  */ const index_t         /* param */ AIndex,
   /* out */ const arraytreenode** /* param */ ADestNode);

bool /* func */ corearraytreenodes__tryseeknodeatbwd
  (/* in  */ const arraytreenode*  /* param */ AParentNode,
   /* in  */ const index_t         /* param */ AIndex,
   /* out */ const arraytreenode** /* param */ ADestNode);

arraytreenode* /* func */ corearraytreenodes__seeknodeatfwd
  (/* in */ const arraytreenode* /* param */ AParentNode,
   /* in */ const index_t        /* param */ AIndex);

arraytreenode* /* func */ corearraytreenodes__seeknodeatbwd
  (/* in */ const arraytreenode* /* param */ AParentNode,
   /* in */ const index_t        /* param */ AIndex);

// ------------------

bool /* func */ corearraytreenodes__tryinsertfirst
  (/* inout */ arraytreenode* /* param */ AParentNode,
   /* in */    const pointer*  /* param */ AItem,
   /* out */   arraylistnode** /* param */ ANewNode);

bool /* func */ corearraytreenodes__tryinsertlast
  (/* inout */ arraytreenode*  /* param */ AParentNode,
   /* in */    const pointer*  /* param */ AItem,
   /* out */   arraylistnode** /* param */ ANewNode);

bool /* func */ corearraytreenodes__tryinsertat
  (/* inout */ arraytreenode*  /* param */ AParentNode,
   /* in */    const index_t   /* param */ AIndex,
   /* in */    const pointer*  /* param */ AItem,
   /* out */   arraylistnode** /* param */ ANewNode);

arraytreenode* /* func */ corearraytreenodes__insertfirst
  (/* inout */ arraytreenode*  /* param */ AParentNode,
   /* in */    const pointer*  /* param */ AItem);

arraytreenode* /* func */ corearraytreenodes__insertlast
  (/* inout */ arraytreenode* /* param */ AParentNode,
   /* in */    const pointer* /* param */ AItem);

arraytreenode* /* func */ corearraytreenodes__insertat
  (/* inout */ arraytreenode* /* param */ AParentNode,
   /* in */    const index_t  /* param */ AIndex,
   /* in */    const pointer* /* param */ AItem);

// ------------------

bool /* func */ corearraytreenodes__tryextractfirst
  (/* inout */ arraytreenode* /* param */ AParentNode,
   /* out */   pointer*       /* param */ AItem);

bool /* func */ corearraytreenodes__tryextractlast
  (/* inout */ arraytreenode* /* param */ AParentNode,
   /* out */   pointer*       /* param */ AItem);
   
bool /* func */ corearraytreenodes__tryextractat
  (/* inout */ arraytreenode* /* param */ AParentNode,
   /* in */    const index_t  /* param */ AIndex,
   /* out */   pointer*       /* param */ AItem);

bool /* func */ corearraytreenodes__tryremovefirst
  (/* inout */ arraytreenode* /* param */ AParentNode);
   
bool /* func */ corearraytreenodes__tryremovelast
  (/* inout */ arraytreenode* /* param */ AParentNode);

pointer* /* func */ corearraytreenodes__extractfirst
  (/* inout */ arraytreenode* /* param */ AParentNode);

pointer* /* func */ corearraytreenodes__extractlast
  (/* inout */ arraytreenode* /* param */ AParentNode);

pointer* /* func */ corearraytreenodes__extractat
  (/* inout */ arraytreenode* /* param */ AParentNode,
   /* in */    const index_t  /* param */ AIndex);
   
void /* func */ corearraytreenodes__removefirst
  (/* inout */ arraytreenode* /* param */ AParentNode);

void /* func */ corearraytreenodes__removelast
  (/* inout */ arraytreenode* /* param */ AParentNode);

// ------------------

bool /* func */ corearraytreenodes__tryexchangeat
  (/* inout */ /* restricted */ arraytreenode* /* param */ AParentNode,
   /* in */    /* restricted */ const index_t  /* param */ AFirstIndex,
   /* in */    /* restricted */ const index_t  /* param */ ASecondIndex);

void /* func */ corearraytreenodes__exchangeat
  (/* inout */ /* restricted */ arraytreenode* /* param */ AParentNode,
   /* in */    /* restricted */ const index_t  /* param */ AFirstIndex,
   /* in */    /* restricted */ const index_t  /* param */ ASecondIndex);
   
 // ...
 
// ------------------


 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ corearraytreenodes__modulename
  ( noparams );

/* override */ int /* func */ corearraytreenodes__setup
  ( noparams );

/* override */ int /* func */ corearraytreenodes__setoff
  ( noparams );

// ------------------

#endif // COREARRAYTREENODES__H

// } // namespace corearraytreenodes