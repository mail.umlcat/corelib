/** Module: "coredeques.h"
 ** Descr.: "Double Linked Deque Collection."
 **/
 
// namespace coredeques {
 
// ------------------
 
#ifndef COREDEQUES__H
#define COREDEQUES__H
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"

// ------------------

#include "coreiterators.h"
#include "coredequenodes.h"

// ------------------

// opaque type
typedef
  pointer* /* as */ deque;
typedef
  pointer* /* as */ coredeques__deque;

struct dequeheader
{
  // pointer to node before first node
  dequenode* /* var */ FirstMarker;
  
  // pointer to node before last node
  dequenode* /* var */ LastMarker;

  // only applies when items are same type
  // "unknowntype" value means different type
  typecode /* var */ ItemsType;

  // applies instead of "typecode"
  // "0" means not used
  size_t   /* var */ ItemsSize;

  // restricts how many items can be stored
  // "0" means not used
  count_t  /* var */ ItemsMaxCount;

  // how many items are currently stored
  count_t  /* var */ ItemsCurrentCount;
  
  // indicates iterator related objects
  count_t  /* var */ IteratorCount;
  
  areequalfunctor /* var */ MatchFunc;
  
  // ...
} ;

typedef
  dequeheader  /* as */ deque_t;
typedef
  dequeheader* /* as */ deque_p;

typedef
  dequeheader  /* as */ coredeques__deque_t;
typedef
  dequeheader* /* as */ coredeques__deque_p;

// ------------------

typedef
  pointer*       /* as */ dequeiterator;

struct dequeiteratorheader
{
  deque*          /* var */ MainDeque;

  dequenode*      /* var */ CurrentNode;

  count_t        /* var */ ItemsCount;
  size_t         /* var */ ItemsSize;
  
  listdirections /* var */ Direction;
} ;

typedef
  dequeiteratorheader  /* as */ dequeiterator_t;
typedef
  dequeiteratorheader* /* as */ dequeiterator_p;

typedef
  dequeiteratorheader  /* as */ coredeques__dequeiterator_t;
typedef
  dequeiteratorheader* /* as */ coredeques__dequeiterator_p;

// ------------------

/* friend */ bool /* func */ coredeques__sameitemsize
  (/* in */ deque* /* param */ AFirstDeque,
  (/* in */ deque* /* param */ ASecondDeque);

/* friend */ bool /* func */ coredeques__sameitemtype
  (/* in */ deque* /* param */ AFirstDeque,
  (/* in */ deque* /* param */ ASecondDeque);

// ------------------

deque* /* func */ coredeques__createdeque
  ( noparams );

deque* /* func */ coredeques__createdequerestrictcount
  (/* in */ count_t* /* param */ AMaxCount);

deque* /* func */ coredeques__createdequebytype
  (/* in */ typecode_t* /* param */ AItemType);

deque* /* func */ coredeques__createdequebysize
  (/* in */ size_t* /* param */ AItemSize);

void /* func */ coredeques__dropdeque
  (/* out */ deque** /* param */ ADeque);

// ------------------

bool /* func */ coredeques__underiteration
  (/* in */ deque* /* param */ ADeque);

dequeiterator* /* func */ coredeques__createiteratorfwd
  (/* in */ deque* /* param */ ADeque);

dequeiterator* /* func */ coredeques__createiteratorback
  (/* in */ deque* /* param */ ADeque);

void /* func */ coredeques__dropiterator
  (/* out */ dequeiterator** /* param */ AIterator);

// ------------------

void /* func */ coredeques__assignequalityfunc
  (/* inout */ deque*    /* param */ ADeque,
   /* in */    equality /* param */ AMatchFunc);

// ------------------

bool /* func */ coredeques__isdone
  (/* in */ const dequeiterator* /* param */ AIterator);

bool /* func */ coredeques__trymovenext
  (/* inout */ dequeiterator* /* param */ AIterator);

void /* func */ coredeques__movenext
  (/* inout */ dequeiterator* /* param */ AIterator);
  
// ------------------

bool /* func */ coredeques__trygetnode
  (/* inout */ dequeiterator* /* param */ AIterator
   /* out */   pointer**     /* param */ AItem);

bool /* func */ coredeques__trygetitem
  (/* in */  const dequeiterator* /* param */ AIterator,
   /* out */ pointer**           /* param */ AItem);

bool /* func */ coredeques__tryreaditem
  (/* inout */ dequeiterator* /* param */ AIterator,
   /* out */   pointer*      /* param */ AItem);

bool /* func */ coredeques__trywriteitem
  (/* inout */ dequeiterator* /* param */ AIterator
   /* out */   pointer*      /* param */ AItem);

dequenode* /* func */ coredeques__getnode
  (/* inout */ dequeiterator* /* param */ AIterator);

pointer* /* func */ coredeques__getitem
  (/* inout */ dequeiterator* /* param */ AIterator);

void /* func */ coredeques__readitem
  (/* inout */ dequeiterator* /* param */ AIterator
   /* out */   pointer*      /* param */ AItem);

void /* func */ coredeques__writeitem
  (/* inout */ dequeiterator*  /* param */ AIterator
   /* in */    const pointer* /* param */ AItem);

// ------------------

count_t /* func */ coredeques__getcurrentcount
  (/* in */ const deque* /* param */ ADeque);

count_t /* func */ coredeques__getmaxcount
  (/* in */ const deque* /* param */ ADeque);

// ------------------

bool /* func */ coredeques__tryclear
  (/* inout */ deque* /* param */ ADeque);

bool /* func */ coredeques__isempty
  (/* in */ const deque* /* param */ ADeque);

bool /* func */ coredeques__isfull
  (/* in */ const deque* /* param */ AStack);

bool /* func */ coredeques__hasitems
  (/* in */ const deque* /* param */ ADeque);

void /* func */ coredeques__clear
  (/* inout */ deque* /* param */ ADeque);

// ------------------

bool /* func */ coredeques__tryinsertfirst
  (/* inout */ deque*       /* param */ ADeque,
   /* in */ const pointer* /* param */ AItem);

bool /* func */ coredeques__tryinsertlast
  (/* inout */ deque*       /* param */ ADeque,
   /* in */ const pointer* /* param */ AItem);

void /* func */ coredeques__insertfirst
  (/* inout */ deque*       /* param */ ADeque,
   /* in */ const pointer* /* param */ AItem);

void /* func */ coredeques__insertlast
  (/* inout */ deque*       /* param */ ADeque,
   /* in */ const pointer* /* param */ AItem);

// ------------------

bool /* func */ coredeques__tryextractfirst
  (/* inout */ deque*     /* param */ ADeque,
   /* out */   pointer** /* param */ AItem);

bool /* func */ coredeques__tryextractlast
  (/* inout */ deque*     /* param */ ADeque,
   /* out */   pointer** /* param */ AItem);
   
bool /* func */ coredeques__tryremovefirst
  (/* inout */ deque* /* param */ ADeque);
   
bool /* func */ coredeques__tryremovelast
  (/* inout */ deque* /* param */ ADeque);

pointer* /* func */ coredeques__extractfirst
  (/* inout */ deque* /* param */ ADeque);

pointer* /* func */ coredeques__extractlast
  (/* inout */ deque* /* param */ ADeque);

void /* func */ coredeques__removefirst
  (/* inout */ deque* /* param */ ADeque);

void /* func */ coredeques__removelast
  (/* inout */ deque* /* param */ ADeque);

// ------------------

/* override */ const ansinullstring* /* func */ coredeques__modulename
  ( noparams );

/* override */ int /* func */ coredeques__setup
  ( noparams );

/* override */ int /* func */ coredeques__setoff
  ( noparams );
 
// ------------------
 
#endif // COREDEQUES__H

// } // namespace coredeques