/** Module: "coreansidictnodes.h"
 ** Descr.: "Unique Key and duplicated value,"
 **         "list items."
 **/

// namespace coreansidictnodes {
 
// ------------------
 
#ifndef COREANSIDICTNODES__H
#define COREANSIDICTNODES__H
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansikeyvalues.h"

// ------------------

// opaque type
typedef
  pointer* /* as */ ansidictionarynode;

struct ansidictionarynodeheader
{
  ansidictionarynode*  /* var */ Prev;
  ansidictionarynode*  /* var */ Next;  

  keyvalue* /* var */ KeyValue;
} ;

typedef
  ansidictionarynodeheader  /* as */ ansidictionarynode_t;
typedef
  ansidictionarynodeheader* /* as */ ansidictionarynode_p;

// ------------------

/* friend */ bool /* func */ coreansidictnodes__arelinked
  (/* in */ const ansidictionarynode* /* param */ A,
   /* in */ const ansidictionarynode* /* param */ B);

/* friend */ bool /* func */ coreansidictnodes__trylink
  (/* inout */ ansidictionarynode* /* param */ A,
   /* inout */ ansidictionarynode* /* param */ B);

/* friend */ bool /* func */ coreansidictnodes__trymoveforward
  (/* inout */ ansidictionarynode* /* param */ AListNode);

/* friend */ bool /* func */ coreansidictnodes__trymovebackward
  (/* inout */ ansidictionarynode* /* param */ AListNode);

// ------------------

/* friend */ void /* func */ coreansidictnodes__link
  (/* inout */ ansidictionarynode* /* param */ A,
   /* inout */ ansidictionarynode* /* param */ B);

/* friend */ void /* func */ coreansidictnodes__moveforward
  (/* inout */ ansidictionarynode** /* param */ ADictNode);

/* friend */ void /* func */ coreansidictnodes__movebackward
  (/* inout */ ansidictionarynode** /* param */ ADictNode);

// ------------------

ansidictionarynode* /* func */ coreansidictnodes__createnodeempty
  ( noparams );

ansidictionarynode* /* func */ coreansidictnodes__createnodebykey
  (/* in */ pointer* /* param */ AKey);

ansidictionarynode* /* func */ coreansidictnodes__createnodebyvalue
  (/* in */ pointer* /* param */ AKey,
   /* in */ pointer* /* param */ AValue);

ansidictionarynode* /* func */ coreansidictnodes__createnodebykeyvalue
  (/* in */ pointer* /* param */ AKeyValue);

// ------------------

void /* func */ coreansidictnodes__dropnodebysize
  (/* out */ ansidictionarynode** /* param */ ADictNode,
   /* in */  size_t               /* param */ AKeySize,
   /* in */  size_t               /* param */ AValueSize);

void /* func */ coreansidictnodes__dropnodeextractkeyvalue
  (/* out */ ansidictionarynode** /* param */ ADictNode,
   /* out */ keyvalue**           /* param */ AKeyValue);

void /* func */ coreansidictnodes__dropnodeextract
  (/* out */ ansidictionarynode** /* param */ ADictNode,
   /* out */ pointer**            /* param */ AKey,
   /* out */ pointer**            /* param */ AValue);

// ------------------

pointer* /* func */ coreansidictnodes__getkey
  (/* inout */ ansidictionarynode* /* param */ ADictNode);

pointer* /* func */ coreansidictnodes__getvalue
  (/* inout */ ansidictionarynode* /* param */ ADictNode);

// ------------------

bool /* func */ coreansidictnodes__MatchKey
  (/* in */ ansidictionarynode* /* param */ ADictNode,
   /* in */ pointer*            /* param */ AKey);
   /* in */ equality            /* param */ MatchKey);

// ------------------

pointer* /* func */ coreansidictnodes__replacekey
  (/* out */ ansidictionarynode* /* param */ ADictNode,
   /* in */  pointer*            /* param */ ANewKey);

pointer* /* func */ coreansidictnodes__replacevalue
  (/* out */ ansidictionarynode* /* param */ ADictNode,
   /* in */  pointer*            /* param */ ANewValue);
 

// ------------------

bool /* func */ coreansidictnodes__tryextractkeyvalue
  (/* out */ ansidictionarynode* /* param */ ADictNode,
   /* out */ keyvalue**          /* param */ AKeyValue);

bool /* func */ coreansidictnodes__tryextractitems
  (/* out */ ansidictionarynode* /* param */ ADictNode,
   /* out */ pointer**           /* param */ AKey,
   /* out */ pointer**           /* param */ AValue);

void /* func */ coreansidictnodes__extractkeyvalue
  (/* out */ ansidictionarynode* /* param */ ADictNode,
   /* out */ keyvalue**          /* param */ AKeyValue);

void /* func */ coreansidictnodes__extractitems
  (/* out */ ansidictionarynode* /* param */ ADictNode,
   /* out */ pointer**           /* param */ AKey,
   /* out */ pointer**           /* param */ AValue);


// ------------------

 // ...

// ------------------

/* override */ const ansinullstring* /* func */ coreansidictnodes__modulename
  ( noparams );

/* override */ int /* func */ coreansidictnodes__setup
  ( noparams );

/* override */ int /* func */ coreansidictnodes__setoff
  ( noparams );

// ------------------

#endif // COREANSIDICTNODES__H

// } // namespace coreansidictnodes