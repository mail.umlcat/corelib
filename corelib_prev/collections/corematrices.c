/** Module: "corematrices.c"
 ** Descr.: "Dynamically allocated two dimension array."
 **/

// namespace corematrices {
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

#include "corematrices.h"
 
// ------------------

/* friend */ bool /* func */ corematrices__sameitemsize
  (/* in */  matrix* /* param */ AFirstMatrix,
  (/* out */ matrix* /* param */ ASecondMatrix)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AMatrix != NULL);
  if (Result)
  {
    matrixheader* /* var */ ThisFirstMatrix = NULL;
    matrixheader* /* var */ ThisSecondMatrix = NULL;
      
    ThisFirstMatrix  = (matrixheader*) AFirstMatrix;
    ThisSecondMatrix = (matrixheader*) ASecondMatrix;
    
    Result =
      (ThisFirstMatrix->ItemsSize == ThisSecondMatrix->ItemsSize);
  } // if

  // ---
  return Result;
} // func

/* friend */ bool /* func */ corematrices__sameitemtype
  (/* in */  matrix* /* param */ AFirstMatrix,
  (/* out */ matrix* /* param */ ASecondMatrix)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AMatrix != NULL);
  if (Result)
  {
    matrixheader* /* var */ ThisFirstMatrix = NULL;
    matrixheader* /* var */ ThisSecondMatrix = NULL;
      
    ThisFirstMatrix  = (matrixheader*) AFirstMatrix;
    ThisSecondMatrix = (matrixheader*) ASecondMatrix;
    
    Result =
      (ThisFirstMatrix->ItemsType == ThisSecondMatrix->ItemsType);
  } // if

  // ---
  return Result;
} // func

/* friend */ bool /* func */ corematrices__sameitemcount
  (/* in */  matrix* /* param */ AFirstMatrix,
  (/* out */ matrix* /* param */ ASecondMatrix)
{
  bool /* var */ Result = false;
  // ---
     
  Result = 
    ((AFirstMatrix != NULL) &&
     (ASecondMatrix != NULL));
  if (Result)
  {
    matrixheader* /* var */ ThisFirstMatrix = NULL;
    matrixheader* /* var */ ThisSecondMatrix = NULL;
       
    ThisFirstMatrix  = (matrixheader*) AFirstMatrix;
    ThisSecondMatrix = (matrixheader*) ASecondMatrix;
    
    Result =
     ((ThisFirstMatrix->RowsMaxCount == ThisSecondMatrix->RowsMaxCount) &&
      (ThisFirstMatrix->ColsMaxCount == ThisSecondMatrix->ColsMaxCount));
  } // if
 
  // ---
  return Result;
} // func

/* friend */ bool /* func */ corematrices__sameitemrowcount
  (/* in */  matrix* /* param */ AFirstMatrix,
  (/* out */ matrix* /* param */ ASecondMatrix)
{
  bool /* var */ Result = false;
  // ---
     
  Result = 
    ((AFirstMatrix != NULL) &&
     (ASecondMatrix != NULL));
  if (Result)
  {
    matrixheader* /* var */ ThisFirstMatrix = NULL;
    matrixheader* /* var */ ThisSecondMatrix = NULL;
       
    ThisFirstMatrix  = (matrixheader*) AFirstMatrix;
    ThisSecondMatrix = (matrixheader*) ASecondMatrix;
    
    Result =
      (ThisFirstMatrix->RowsMaxCount == ThisSecondMatrix->RowsMaxCount);
  } // if
 
  // ---
  return Result;
} // func

/* friend */ bool /* func */ corematrices__sameitemcolcount
  (/* in */  matrix* /* param */ AFirstMatrix,
  (/* out */ matrix* /* param */ ASecondMatrix)
{
  bool /* var */ Result = false;
  // ---
     
  Result = 
    ((AFirstMatrix != NULL) &&
     (ASecondMatrix != NULL));
  if (Result)
  {
    matrixheader* /* var */ ThisFirstMatrix = NULL;
    matrixheader* /* var */ ThisSecondMatrix = NULL;
       
    ThisFirstMatrix  = (matrixheader*) AFirstMatrix;
    ThisSecondMatrix = (matrixheader*) ASecondMatrix;
    
    Result =
      (ThisFirstMatrix->ColsMaxCount == ThisSecondMatrix->ColsMaxCount);
  } // if
 
  // ---
  return Result;
} // func

// ------------------

/* friend */ bool /* func */ corematrixs__trycopyempty
  (/* in */  matrix*  /* param */ ASourceMatrix,
  (/* out */ matrix** /* param */ ADestMatrix)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (ASourceMatrix != NULL);
  if (Result)
  {
    matrixheader* /* var */ ThisSourceMatrix = NULL;
    ThisSourceMatrix = (matrixheader*) ASourceMatrix;

    ADestMatrix =
      corematrixs__creatematrix();
    ThisDestMatrix = (matrixheader*) ADestMatrix;
    
    ThisDestMatrix->ItemsType =
      ThisDestMatrix->ItemsType;
      
    ThisDestMatrix->ItemsSize =
      ThisDestMatrix->ItemsSize;
      
    ThisDestMatrix->ItemsMaxCount =
      ThisDestMatrix->ItemsMaxCount;

    ThisDestMatrix->RowsMaxCount =
      ThisDestMatrix->RowsMaxCount;

    ThisDestMatrix->ColsMaxCount =
      ThisDestMatrix->ColsMaxCount;
  
    ThisDestMatrix->IteratorCount =
      0;

    ThisDestMatrix->ItemsFirst =
      corememory__clearallocatearray  
        (ThisDestMatrix->ItemsSize,
         ThisDestMatrix->ItemsMaxCount);
         
    ThisDestMatrix->ItemsLast =
      corememory__itematbysize
        (ThisDestMatrix->ItemsFirst,
         ThisDestMatrix->ItemsMaxCount - 1,
         ThisDestMatrix->ItemsSize);
  } // if
  
  // ---
  return Result;
} // func

/* friend */ bool /* func */ corematrixs__trycopyall
  (/* in */  matrix*  /* param */ ASourceMatrix,
  (/* out */ matrix** /* param */ ADestMatrix)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (ASourceMatrix != NULL);
  if (Result)
  {
    matrixheader* /* var */ ThisSourceMatrix = NULL;
    ThisSourceMatrix = (matrixheader*) ASourceMatrix;

    ADestMatrix =
      corematrixs__creatematrix();
    ThisDestMatrix = (matrixheader*) ADestMatrix;
    
    ThisDestMatrix->ItemsType =
      ThisDestMatrix->ItemsType;
      
    ThisDestMatrix->ItemsSize =
      ThisDestMatrix->ItemsSize;
      
    ThisDestMatrix->ItemsMaxCount =
      ThisDestMatrix->ItemsMaxCount;

    ThisDestMatrix->RowsMaxCount =
      ThisDestMatrix->RowsMaxCount;

    ThisDestMatrix->ColsMaxCount =
      ThisDestMatrix->ColsMaxCount;
  
    ThisDestMatrix->IteratorCount =
      0;

    ThisDestMatrix->ItemsFirst =
      corememory__duplicatecopyarray
        (ThisSourceMatrix->ItemsFirst,
         ThisDestMatrix->ItemsSize,
         ThisDestMatrix->ItemsMaxCount);
         
    ThisDestMatrix->ItemsLast =
         corememory__itematbysize
        (ThisDestMatrix->ItemsFirst,
         ThisDestMatrix->ItemsMaxCount - 1,
         ThisDestMatrix->ItemsSize);
  } // if

  // ---
  return Result;
} // func

/* friend */ bool /* func */ corematrixs__trycopyreverse
  (/* in */  matrix*  /* param */ ASourceMatrix,
  (/* out */ matrix** /* param */ ADestMatrix)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (ASourceMatrix != NULL);
  if (Result)
  {
    matrixheader* /* var */ ThisSourceMatrix = NULL;
    ThisSourceMatrix = (matrixheader*) ASourceMatrix;

    ADestMatrix =
      corematrixs__creatematrix();
    ThisDestMatrix = (matrixheader*) ADestMatrix;
    
    ThisDestMatrix->ItemsType =
      ThisDestMatrix->ItemsType;
      
    ThisDestMatrix->ItemsSize =
      ThisDestMatrix->ItemsSize;
      
    ThisDestMatrix->ItemsMaxCount =
      ThisDestMatrix->ItemsMaxCount;

    ThisDestMatrix->RowsMaxCount =
      ThisDestMatrix->RowsMaxCount;

    ThisDestMatrix->ColsMaxCount =
      ThisDestMatrix->ColsMaxCount;
  
    ThisDestMatrix->IteratorCount =
      0;

    ThisDestMatrix->ItemsFirst =
      corememory__clearallocatearray  
        (ThisDestMatrix->ItemsSize,
         ThisDestMatrix->ItemsMaxCount);
         
    ThisDestMatrix->ItemsLast =
         corememory__itematbysize
        (ThisDestMatrix->ItemsFirst,
         ThisDestMatrix->ItemsMaxCount - 1,
         ThisDestMatrix->ItemsSize);
    
    corememory__safereversecopyarray
      (ThisDestMatrix->ItemsLast,
       ThisSourceMatrix->ItemsFirst,
       ThisDestMatrix->ItemsSize,
       ThisDestMatrix->ItemsMaxCount);       
  } // if

  // ---
  return Result;
} // func

/* friend */ bool /* func */ corematrixs__trycopyconcat
  (/* in */  matrix*  /* param */ AFirstMatrix,
   /* in */  matrix*  /* param */ ASecondMatrix,
   /* out */ matrix** /* param */ ADestMatrix)
{
  bool /* var */ Result = false;
  // ---
     
  Result = 
    ((AFirstMatrix != NULL) &&
     (ASecondMatrix != NULL));
  if (Result)
  {
    Result =
      corematrixs__sameitemsize
        (AFirstMatrix, ASecondMatrix) &&
      corematrixs__sameitemtype
        (AFirstMatrix, ASecondMatrix);
    if (Result)
    {
      matrixheader* /* var */ ThisFirstMatrix = NULL;
      matrixheader* /* var */ ThisSecondMatrix = NULL;
       
      ThisFirstMatrix  = (matrixheader*) AFirstMatrix;
      ThisSecondMatrix = (matrixheader*) ASecondMatrix;
    
      count_t /* var */ NewCount =
        (ThisFirstMatrix->ItemsMaxCount + ThisSecondMatrix->ItemsMaxCount);
        
      ADestMatrix =
        corematrixs__creatematrix();
      ThisDestMatrix = (matrixheader*) ADestMatrix;
    
      ThisDestMatrix->ItemsType =
        ThisFirstMatrix->ItemsType;
      
      ThisDestMatrix->ItemsSize =
        ThisFirstMatrix->ItemsSize;
      
      ThisDestMatrix->ItemsMaxCount =
        ThisDestMatrix->ItemsMaxCount;

      ThisDestMatrix->RowsMaxCount =
        ThisDestMatrix->RowsMaxCount;

      ThisDestMatrix->ColsMaxCount =
        ThisDestMatrix->ColsMaxCount;
  
      ThisDestMatrix->IteratorCount =
        NewCount;
        
      ThisDestMatrix->ItemsFirst =
        corememory__clearallocatearray  
          (ThisDestMatrix->ItemsSize,
           ThisDestMatrix->ItemsMaxCount);

      // copy first array items
      corememory__safecopyarray
        (ThisDestMatrix->ItemsFirst,
         ThisFirstMatrix->ItemsFirst,
         ThisFirstMatrix->ItemsSize,
         ThisFirstMatrix->ItemsMaxCount);
      
      // prepare location
      ThisDestMatrix->ItemsLast =
        corememory__itematbysize
          (ThisDestMatrix->ItemsFirst,
           ThisDestMatrix->ItemsMaxCount,
           ThisDestMatrix->ItemsSize);

      // copy second array items
      corememory__safecopyarray
        (ThisDestMatrix->ItemsLast,
         ThisSecondMatrix->ItemsFirst,
         ThisSecondMatrix->ItemsSize,
         ThisSecondMatrix->ItemsMaxCount);

      // update last item pointer   
      ThisDestMatrix->ItemsLast =
        corememory__itematbysize
          (ThisDestMatrix->ItemsFirst,
           ThisDestMatrix->ItemsMaxCount - 1,
           ThisDestMatrix->ItemsSize);
    } // if
  } // if

  // ---
  return Result;
} // func

// ------------------

/* friend */ void /* func */ corematrixs__copyempty
  (/* in */  matrix*  /* param */ ASourceMatrix,
  (/* out */ matrix** /* param */ ADestMatrix)
{
  /* discard */ corematrixs__trycopyempty
    (ASourceMatrix, ADestMatrix);
} // func

/* friend */ void /* func */ corematrixs__copyall
  (/* in */  matrix*  /* param */ ASourceMatrix,
  (/* out */ matrix** /* param */ ADestMatrix)
{
  /* discard */ corematrixs__trycopyall
    (ASourceMatrix, ADestMatrix);
} // func

/* friend */ void /* func */ corematrixs__copyreverse
  (/* in */  matrix*  /* param */ ASourceMatrix,
  (/* out */ matrix** /* param */ ADestMatrix)
{
  /* discard */ corematrixs__trycopyreverse
    (ASourceMatrix, ADestMatrix);
} // func

/* friend */ void /* func */ corematrixs__copyconcat
  (/* in */  matrix*  /* param */ AFirstMatrix,
   /* in */  matrix*  /* param */ ASecondMatrix,
   /* out */ matrix** /* param */ ADestMatrix)
{
  /* discard */ corematrixs__trycopyconcat
    (AFirstMatrix, ASecondMatrix, ADestMatrix);
} // func

// ------------------

matrix* /* func */ corematrices__creatematrix
  ( noparams )
{
  matrix* /* var */ Result = NULL;
  // ---

  matrixheader* /* var */ AMatrix = NULL;

  Result =
    corememory__clearallocate
      (sizeof(matrixheader));
  AMatrix = Result;
 
  // ---
  return Result;
} // func

matrix* /* func */ corematrices__creatematrixbyitemtype
  (/* in */ typecode_t /* param */ AItemType)
{
  matrix* /* var */ Result = NULL;
  // ---

  matrixheader* /* var */ AMatrix = NULL;

  AMatrix =
    corememory__clearallocate
      (sizeof(matrixheader));
      
  size_t  /* param */ AItemSize;

  AItemSize =
    corememory__typetosize(AItemType);
  
  AMatrix->ItemsType =
    AItemType;

  AMatrix->ItemsSize =
    AItemSize;

  AMatrix->ItemsMaxCount =
    0;
    
  AMatrix->IteratorCount =
    0;

  AMatrix->ItemsFirst =
    NULL;

  AMatrix->ItemsLast =
    NULL;

  Result = AMatrix;
   
  // ---
  return Result;
} // func

matrix* /* func */ corematrices__creatematrixbyitemtypecount
  (/* in */ typecode_t /* param */ AItemType,
   /* in */ count_t    /* param */ AItemMaxRowCount,
   /* in */ count_t    /* param */ AItemMaxColCount);
{
  matrix* /* var */ Result = NULL;
  // ---

  matrixheader* /* var */ AMatrix = NULL;
 
  AMatrix =
    corememory__clearallocate
      (sizeof(matrixheader));

  size_t  /* param */ AItemSize;

  AItemSize =
    corememory__typetosize(AItemType);
  
  AMatrix->ItemsType =
    AItemType;

  AMatrix->ItemsSize =
    AItemSize;

  AMatrix->ItemsRowsMaxCount =
    AItemRowsMaxCount;

  AMatrix->ItemsColsMaxCount =
    AItemColsMaxCount;

  AMatrix->ItemsMaxCount =
    (AItemRowsMaxCount * AItemColsMaxCount);

  AMatrix->IteratorCount =
    0;

  AMatrix->ItemsFirst =
    corememory__clearallocatearray
      (sizeof(pointer*), AItemMaxCount);

  AMatrix->ItemsLast =
    &(AMatrix->ItemsFirst[AItemMaxCount]);

  Result = AMatrix;

  // ---
  return Result;
} // func

matrix* /* func */ corematrices__creatematrixsquarebyitemtype
  (/* in */ count_t    /* param */ AItemMaxCount,
   /* in */ typecode_t /* param */ AItemType);
{
  matrix* /* var */ Result = NULL;
  // ---

  matrixheader* /* var */ AMatrix = NULL;
 
  AMatrix =
    corememory__clearallocate
      (sizeof(matrixheader));

  size_t  /* param */ AItemSize;

  AItemSize =
    corememory__typetosize(AItemType);
  
  AMatrix->ItemsType =
    AItemType;

  AMatrix->ItemsSize =
    AItemSize;

  AMatrix->ItemsRowsMaxCount =
    AItemMaxCount;

  AMatrix->ItemsColsMaxCount =
    AItemMaxCount;

  AMatrix->ItemsMaxCount =
    (AItemMaxCount * AItemMaxCount);

  AMatrix->IteratorCount =
    0;

  AMatrix->ItemsFirst =
    corememory__clearallocatearray
      (sizeof(pointer*), AItemMaxCount);

  AMatrix->ItemsLast =
    &(AMatrix->ItemsFirst[AItemMaxCount]);

  Result = AMatrix;

  // ---
  return Result;
} // func

matrix* /* func */ corematrices__creatematrixbyitemsizecount
  (/* in */ size_t  /* param */ AItemSize,
   /* in */ count_t /* param */ AItemMaxRowCount,
   /* in */ count_t /* param */ AItemMaxColCount)
{
  matrix* /* var */ Result = NULL;
  // ---

  matrixheader* /* var */ AMatrix = NULL;

  AMatrix =
    corememory__clearallocate
      (sizeof(matrixheader));

  AMatrix->ItemsType =
    unknowntype;

  AMatrix->ItemsSize =
    AItemSize;

  AMatrix->ItemsRowsMaxCount =
    AItemRowsMaxCount;

  AMatrix->ItemsColsMaxCount =
    AItemColsMaxCount;

  AMatrix->ItemsMaxCount =
    (AItemRowsMaxCount * AItemColsMaxCount);

  AMatrix->IteratorCount =
    0;

  AMatrix->ItemsFirst =
    corememory__clearallocatearray
      (sizeof(pointer*), AItemMaxCount);

  AMatrix->ItemsLast =
    &(AMatrix->ItemsFirst[AItemMaxCount]);

  Result = AMatrix;
  // ---
  return Result;
} // func

matrix* /* func */ corematrices__creatematrixsquarebyitemsize
  (/* in */ count_t /* param */ AItemMaxCount,
   /* in */ size_t  /* param */ AItemSize)
{
  matrix* /* var */ Result = NULL;
  // ---

  matrixheader* /* var */ AMatrix = NULL;

  AMatrix =
    corememory__clearallocate
      (sizeof(matrixheader));

  AMatrix->ItemsType =
    unknowntype;

  AMatrix->ItemsSize =
    AItemSize;

  AMatrix->ItemsRowsMaxCount =
    AItemMaxCount;

  AMatrix->ItemsColsMaxCount =
    AItemMaxCount;

  AMatrix->ItemsMaxCount =
    (AItemMaxCount * AItemMaxCount);

  AMatrix->IteratorCount =
    0;

  AMatrix->ItemsFirst =
    corememory__clearallocatearray
      (sizeof(pointer*), AItemMaxCount);

  AMatrix->ItemsLast =
    &(AMatrix->ItemsFirst[AItemMaxCount]);

  Result = AMatrix;
  // ---
  return Result;
} // func

void /* func */ corematrices__dropmatrix
  (/* out */ matrix** /* param */ AMatrix)
{
  Result = (AMatrix != NULL);
  if (Result)
  {
    matrixheader* /* var */ ThisMatrix = NULL;
    ThisMatrix = (matrixheader*) AMatrix;

    corememory__unshare
      (&ThisMatrix->ItemsLast);

    corememory__cleardeallocatearray
     (AMatrix->ItemsFirst, AMatrix->ItemsSize, AMatrix->ItemsMaxCount);   
  
    corememory__cleardeallocate
     (AMatrix, sizeof(matrixheader);     
  } // if
} // func

// ------------------

bool /* func */ corematrixs__underiteration
  (/* in */ matrix* /* param */ AMatrix)
{
  bool /* var */ Result = false;
  // ---
  
  if (AMatrix != NULL)
  {
    matrixheader* /* var */ ThisMatrix = NULL;
    ThisMatrix = (matrixheader*) AMatrix;

	Result = (ThisMatrix->IteratorCount > 0);
  }
     
  // ---
  return Result;
} // func

matrixiterator* /* func */ corematrixs__createiteratorforward
  (/* in */ matrix* /* param */ AMatrix)
{
  matrixiterator* /* var */ Result = NULL;
  // ---
     
  matrixheader* /* var */ ThisMatrix =  NULL;
  ThisMatrix = (matrixheader*) AMatrix;

  if (ThisMatrix != NULL)
  {
    matrixiteratorheader* /* var */ AIterator = NULL;

    AIterator =
      corememory__clearallocate
       (sizeof(matrixiteratorheader));
     
    AIterator->CurrentItem =
      corememory__share
        (ThisMatrix->ItemsFirst);

    AIterator->IteratorCount =
      ThisMatrix->IteratorCount;

    AIterator->ItemIndex =
      0;

    AIterator->Direction =
      listdirections__forward;
      
    Result = AIterator;
  } // if
    
  // ---
  return Result;
} // func

matrixiterator* /* func */ corematrixs__createiteratorbackward
  (/* in */ matrix* /* param */ AMatrix)
{
  matrixiterator* /* var */ Result = NULL;
  // ---

  matrixheader* /* var */ ThisMatrix =  NULL;
  ThisMatrix = (matrixheader*) AMatrix;

  if (ThisMatrix != NULL)
  {
    matrixiteratorheader* /* var */ AIterator = NULL;

    AIterator =
      corememory__clearallocate
       (sizeof(matrixiteratorheader));
     
    AIterator->CurrentItem =
      corememory__share
        (ThisMatrix->ItemsLast);

    AIterator->IteratorCount =
      ThisMatrix->ItemsMaxCount;

    AIterator->ItemIndex =
      ThisMatrix->ItemsMaxCount;

    AIterator->Direction =
      listdirections__backward;
      
    Result = AIterator;
  } // if

  // ---
  return Result;
} // func

void /* func */ corematrixs__dropiterator
  (/* out */ matrixiterator** /* param */ AIterator)
{
  Result = (AIterator != NULL);
  if (Result)
  {
    matrixiteratorheader** /* var */ ThisIterator = NULL;
    ThisIterator = (matrixiteratorheader**) AIterator;

    corememory__unshare
      (&(ThisIterator->CurrentItem));

    corememory__cleardeallocate
      (ThisIterator);
      
    *AIterator = NULL;
  } // if
} // func

// ------------------

typecode_t /* func */ corematrices__getitemtype
  (/* in */ const matrix* /* param */ AMatrix)
{
  typecode_t /* var */ Result = unknowntype;
  // ---
     
     
  // ---
  return Result;
} // func

size_t /* func */ corematrices__getitemsize
  (/* in */ const matrix* /* param */ AMatrix)
{
  size_t /* var */ Result = 0;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

count_t /* func */ corematrices__getmaxcount
  (/* in */ const matrix* /* param */ AMatrix)
{
  count_t /* var */ Result = 0;
  // ---
     
  if (AMatrix != NULL)
  {
    matrixheader* /* var */ ThisMatrix = NULL;
    ThisMatrix = (matrixheader*) AMatrix;
    Result = ThisMatrix->ItemsMaxCount;
  } // if
  
  // ---
  return Result;
} // func

count_t /* func */ corematrices__getdimcount
  (/* in */ const matrix* /* param */ AMatrix)
{
  count_t /* var */ Result = 0;
  // ---
     
  if (AMatrix != NULL)
  {
    matrixheader* /* var */ ThisMatrix = NULL;
    ThisMatrix = (matrixheader*) AMatrix;
    Result = ThisMatrix->DimsCount;
  } // if
  
  // ---
  return Result;
} // func

// ------------------

count_t /* func */ corematrices__getmaxcolcount
  (/* in */ const matrix* /* param */ AMatrix)
{
  count_t /* var */ Result = 0;
  // ---
     
     
  // ---
  return Result;
} // func

count_t /* func */ corematrices__getmaxrowcount
  (/* in */ const matrix* /* param */ AMatrix)
{
  count_t /* var */ Result = 0;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ corematrices__issquare
  (/* inout */ matrix* /* param */ AMatrix)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ corematrices__tryclear
  (/* inout */ matrix* /* param */ AMatrix)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ corematrices__clear
  (/* inout */ matrix* /* param */ AMatrix)
{
  coresystem__nothing();
} // func

// ------------------

bool /* func */ corematrices__trygetitemat
  (/* in */  /* restricted */ const matrix* /* param */ AMatrix,
   /* in */  /* restricted */ const index_t /* param */ ARowIndex,
   /* in */  /* restricted */ const index_t /* param */ AColIndex,
   /* out */ /* restricted */ pointer*      /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ corematrices__trysetitemat
  (/* in */  /* restricted */ const matrix*  /* param */ AMatrix,
   /* in */  /* restricted */ const index_t  /* param */ ARowIndex,
   /* in */  /* restricted */ const index_t  /* param */ AColIndex,
   /* out */ /* restricted */ const pointer* /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ corematrices__getitemat
  (/* in */  /* restricted */ const matrix* /* param */ AMatrix,
   /* in */  /* restricted */ const index_t /* param */ ARowIndex,
   /* in */  /* restricted */ const index_t /* param */ AColIndex,
   /* out */ /* restricted */ pointer*      /* param */ AItem)
{
  coresystem__nothing();
} // func

void /* func */ corematrices__setitemat
  (/* in */  /* restricted */ const matrix*  /* param */ AMatrix,
   /* in */  /* restricted */ const index_t  /* param */ ARowIndex,
   /* in */  /* restricted */ const index_t  /* param */ AColIndex,
   /* out */ /* restricted */ const pointer* /* param */ AItem)
{
  coresystem__nothing();
} // func

// ------------------

bool /* func */ corematrices__tryclearat
  (/* inout */ /* restricted */ matrix*       /* param */ AMatrix,
   /* in */    /* restricted */ const index_t /* param */ ARowIndex,
   /* in */    /* restricted */ const index_t /* param */ AColIndex)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ corematrices__tryexchangeat
  (/* inout */ /* restricted */ matrix*       /* param */ AMatrix,
   /* in */    /* restricted */ const index_t /* param */ AFirstRowIndex,
   /* in */    /* restricted */ const index_t /* param */ AFirstColIndex,
   /* in */    /* restricted */ const index_t /* param */ ASecondRowIndex,
   /* in */    /* restricted */ const index_t /* param */ ASecondColIndex)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ corematrices__clearat
  (/* inout */ /* restricted */ matrix*       /* param */ AMatrix,
   /* in */    /* restricted */ const index_t /* param */ ARowIndex,
   /* in */    /* restricted */ const index_t /* param */ AColIndex)
{
  coresystem__nothing();
} // func

void /* func */ corematrices__exchangeat
  (/* inout */ /* restricted */ matrix*       /* param */ AMatrix,
   /* in */    /* restricted */ const index_t /* param */ AFirstRowIndex,
   /* in */    /* restricted */ const index_t /* param */ AFirstColIndex,
   /* in */    /* restricted */ const index_t /* param */ ASecondRowIndex,
   /* in */    /* restricted */ const index_t /* param */ ASecondColIndex)
{
  coresystem__nothing();
} // func

// ------------------




 // ...

// } // namespace corematrices