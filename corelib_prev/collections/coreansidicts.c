/** Module: "coreansidicts.c"
 ** Descr.: "Unique ANSI String Key and duplicated value,"
 **         "list (Dictionary)."
 **/
 
// namespace coreansidicts {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"
#include "coreiterators.h"

// ------------------

#include "coreansikeyvalues.h"
#include "coreansidictnodes.h"

// ------------------

#include "coreansidicts.h"
 
// ------------------

ansidictionary* /* func */ coreansidicts__createdictionary
  ( noparams )
{
  ansidictionary* /* var */ Result = NULL;
  // ---
     
  ansidictionaryheader*     /* var */ ThisDict = NULL;

  ansidictionarynodeheader* /* var */ ThisFirstDictNode = NULL;
  ansidictionarynodeheader* /* var */ ThisSecondDictNode = NULL;
 
  ThisDict =
    corememory__clearallocate
      (sizeof(ansidictionaryheader));
  ThisFirstDictNode =
    coreansidictionarynodes__createnodeempty();
  ThisSecondDictNode =
    coreansidictionarynodes__createnodeempty();

  coreansidictionarynodes__link
    (ThisFirstDictNode, ThisSecondDictNode);

  ThisDict->FirstMarker =
    corememory__transfer(&ThisFirstDictNode);
  ThisDict->LastMarker =
    corememory__transfer(&ThisSecondDictNode);
 
  ThisDict->ItemsKeyType   = unknowntype;
  ThisDict->ItemsValueType = unknowntype;

  ThisDict->ItemsKeySize   = 0;
  ThisDict->ItemsValueSize = 0;
  
  ThisDict->ItemsCurrentCount = 0;
  ThisDict->ItemsMaxCount = 0;
  
  ThisDict->IteratorCount     = 0;

  Result =
    corememory__transfer(&ThisDict);

  // ---
  return Result;
} // func

ansidictionary* /* func */ coreansidicts__createdictbytype
  (/* in */ const typecode_t /* param */ AValueType)
{
  ansidictionary* /* var */ Result = NULL;
  // ---
     
  ansidictionaryheader*     /* var */ ThisDict = NULL;

  ansidictionarynodeheader* /* var */ ThisFirstDictNode = NULL;
  ansidictionarynodeheader* /* var */ ThisSecondDictNode = NULL;
 
  ThisDict =
    corememory__clearallocate
      (sizeof(ansidictionaryheader));
  ThisFirstDictNode =
    coreansidictionarynodes__createnodeempty();
  ThisSecondDictNode =
    coreansidictionarynodes__createnodeempty();

  coreansidictionarynodes__link
    (ThisFirstDictNode, ThisSecondDictNode);

  ThisDict->FirstMarker =
    corememory__transfer(&ThisFirstDictNode);
  ThisDict->LastMarker =
    corememory__transfer(&ThisSecondDictNode);
 
  ThisDict->ItemsKeyType   = AKeyType;
  ThisDict->ItemsValueType = AValueType;

  ThisDict->ItemsKeySize   =
    corememory__typetosize(AKeyType);
  ThisDict->ItemsValueSize =
    corememory__typetosize(AValueType);
   
  ThisDict->ItemsCurrentCount = 0;
  ThisDict->ItemsMaxCount = 0;

  ThisDict->IteratorCount     = 0;

  Result =
    corememory__transfer(&ThisDict);

  // ---
  return Result;
} // func

ansidictionary* /* func */ coreansidicts__createdictbysize
  (/* in */ size_t* /* param */ AValueSize);
{
  ansidictionary* /* var */ Result = NULL;
  // ---
     
  ansidictionaryheader*     /* var */ ThisDict = NULL;

  ansidictionarynodeheader* /* var */ ThisFirstDictNode = NULL;
  ansidictionarynodeheader* /* var */ ThisSecondDictNode = NULL;
 
  ThisDict =
    corememory__clearallocate
      (sizeof(ansidictionaryheader));
  ThisFirstDictNode =
    coreansidictionarynodes__createnodeempty();
  ThisSecondDictNode =
    coreansidictionarynodes__createnodeempty();

  coreansidictionarynodes__link
    (ThisFirstDictNode, ThisSecondDictNode);

  ThisDict->FirstMarker =
    corememory__transfer(&ThisFirstDictNode);
  ThisDict->LastMarker =
    corememory__transfer(&ThisSecondDictNode);
 
  ThisDict->ItemsKeyType   = unknowntype;
  ThisDict->ItemsValueType = unknowntype;

  ThisDict->ItemsKeySize   =
    AKeySize;
  ThisDict->ItemsValueSize =
    AValueSize;
   
  ThisDict->ItemsCurrentCount = 0;
  ThisDict->ItemsMaxCount = 0;

  ThisDict->IteratorCount     = 0;

  Result =
    corememory__transfer(&ThisDict);

  // ---
  return Result;
} // func

void /* func */ coreansidicts__dropdictionary
  (/* out */ ansidictionary** /* param */ ADict)
{
  if (ADict != NULL)
  {
    ansidictionaryheader*     /* var */ ThisDict = NULL;

    ansidictionarynodeheader* /* var */ ThisFirstDictNode = NULL;
    ansidictionarynodeheader* /* var */ ThisSecondDictNode = NULL;

    ThisDict =
      (ansidictionaryheader*) corememory__share(&ADict);
 
    coreansidictionarys__clear
      (ADict);
 
    corememory__cleardeallocate
      (ADict, sizeof(ansidictionaryheader));
  } // if
} // func

// ------------------

count_t /* func */ coreansidicts__getcount
  (/* in */ const ansidictionary* /* param */ ADict)
{
  count_t /* var */ Result = 0;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreansidicts__tryclear
  (/* inout */ ansidictionary* /* param */ ADict)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreansidicts__isempty
  (/* in */ const ansidictionary* /* param */ ADict)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreansidicts__hasitems
  (/* in */ const ansidictionary* /* param */ ADict)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreansidicts__clear
  (/* inout */ ansidictionary* /* param */ ADict)
{
  // ...
} // func

// ------------------

bool /* func */ coreansidicts__trykeyfound
  (/* inout */ /* restricted */ ansidictionary*       i/* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey,
   /* out */   /* restricted */ bool*                 /* param */ AKeyFound)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreansidicts__tryitembykey
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey,
   /* out */   /* restricted */ pointer*              /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreansidicts__keyfound
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

pointer* /* func */ coreansidicts__itembykey
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey)
{
  pointer* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreansidicts__tryinsert
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey,
   /* in */    /* restricted */ pointer*              /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreansidicts__tryupdate
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey,
   /* in */    /* restricted */ pointer*              /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreansidicts__tryreinsert
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey,
   /* in */    /* restricted */ pointer*              /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreansidicts__insert
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey,
   /* in */    /* restricted */ pointer*              /* param */ AValue)
{
  // ...
} // func

void /* func */ coreansidicts__update
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey,
   /* in */    /* restricted */ pointer*              /* param */ AValue)
{
  // ...
} // func

void /* func */ coreansidicts__reinsert
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey,
   /* in */    /* restricted */ pointer*              /* param */ AValue)
{
  // ...
} // func

// ------------------

bool /* func */ coreansidicts__tryextract
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey,
   /* out */   /* restricted */ pointer*              /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

pointer* /* func */ coreansidicts__extract
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey,
   /* out */   /* restricted */ pointer*              /* param */ AValue)
{
  pointer* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreansidicts__trydelete
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

pointer* /* func */ coreansidicts__delete
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AKey)
{
  pointer* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreansidicts__tryexchange
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AFirstKey,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASecondKey)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreansidicts__exchange
  (/* inout */ /* restricted */ ansidictionary*       /* param */ ADict,
   /* in */    /* restricted */ const ansinullstring* /* param */ AFirstKey,
   /* in */    /* restricted */ const ansinullstring* /* param */ ASecondKey)
{
  // ...
} // func

// ------------------

/* override */ const ansinullstring* /* func */ coreansidicts__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "coreansidicts";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coreansidicts__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreansidicts__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coreansidicts