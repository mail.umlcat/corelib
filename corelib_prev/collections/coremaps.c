/** Module: "coremaps.c"
 ** Descr.: "Unique Key and duplicated value,"
 **         "list."
 **/

// namespace coremaps {
 
// ------------------
 
#include <stdlib.h"
#include <stddef.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "corekeyvalues.h"
#include "coremapnodes.h"

// ------------------
 
#include "coremaps.h"

// ------------------

map* /* func */ coremaps__createmap
  ( noparams )
{
  map* /* var */ Result = NULL;
  // ---
     
  mapheader*     /* var */ ThisMap = NULL;

  mapnodeheader* /* var */ ThisFirstMapNode = NULL;
  mapnodeheader* /* var */ ThisSecondMapNode = NULL;
 
  ThisMap =
    corememory__clearallocate
      (sizeof(mapheader));
  ThisFirstMapNode =
    coremapnodes__createnodeempty();
  ThisSecondMapNode =
    coremapnodes__createnodeempty();

  coremapnodes__link
    (ThisFirstMapNode, ThisSecondMapNode);

  ThisMap->FirstMarker =
    corememory__transfer(&ThisFirstMapNode);
  ThisMap->LastMarker =
    corememory__transfer(&ThisSecondMapNode);
 
  ThisMap->ItemsKeyType   = unknowntype;
  ThisMap->ItemsValueType = unknowntype;

  ThisMap->ItemsKeySize   = 0;
  ThisMap->ItemsValueSize = 0;
  
  ThisMap->ItemsCurrentCount = 0;
  ThisMap->ItemsMaxCount = 0;
  
  ThisMap->IteratorCount     = 0;

  Result =
    corememory__transfer(&ThisMap);

  // ---
  return Result;
} // func

map* /* func */ coremaps__createmapbytype
  (/* in */ const typecode_t /* param */ AKeyType,
   /* in */ const typecode_t /* param */ AValueType)
{
  map* /* var */ Result = NULL;
  // ---
     
  mapheader*     /* var */ ThisMap = NULL;

  mapnodeheader* /* var */ ThisFirstMapNode = NULL;
  mapnodeheader* /* var */ ThisSecondMapNode = NULL;
 
  ThisMap =
    corememory__clearallocate
      (sizeof(mapheader));
  ThisFirstMapNode =
    coremapnodes__createnodeempty();
  ThisSecondMapNode =
    coremapnodes__createnodeempty();

  coremapnodes__link
    (ThisFirstMapNode, ThisSecondMapNode);

  ThisMap->FirstMarker =
    corememory__transfer(&ThisFirstMapNode);
  ThisMap->LastMarker =
    corememory__transfer(&ThisSecondMapNode);
 
  ThisMap->ItemsKeyType   = AKeyType;
  ThisMap->ItemsValueType = AValueType;

  ThisMap->ItemsKeySize   =
    corememory__typetosize(AKeyType);
  ThisMap->ItemsValueSize =
    corememory__typetosize(AValueType);
   
  ThisMap->ItemsCurrentCount = 0;
  ThisMap->ItemsMaxCount = 0;

  ThisMap->IteratorCount     = 0;

  Result =
    corememory__transfer(&ThisMap);

  // ---
  return Result;
} // func

map* /* func */ coremaps__createmapbysize
  (/* in */ size_t* /* param */ AKeySize,
   /* in */ size_t* /* param */ AValueSize);
{
  map* /* var */ Result = NULL;
  // ---
     
  mapheader*     /* var */ ThisMap = NULL;

  mapnodeheader* /* var */ ThisFirstMapNode = NULL;
  mapnodeheader* /* var */ ThisSecondMapNode = NULL;
 
  ThisMap =
    corememory__clearallocate
      (sizeof(mapheader));
  ThisFirstMapNode =
    coremapnodes__createnodeempty();
  ThisSecondMapNode =
    coremapnodes__createnodeempty();

  coremapnodes__link
    (ThisFirstMapNode, ThisSecondMapNode);

  ThisMap->FirstMarker =
    corememory__transfer(&ThisFirstMapNode);
  ThisMap->LastMarker =
    corememory__transfer(&ThisSecondMapNode);
 
  ThisMap->ItemsKeyType   = unknowntype;
  ThisMap->ItemsValueType = unknowntype;

  ThisMap->ItemsKeySize   =
    AKeySize;
  ThisMap->ItemsValueSize =
    AValueSize;
   
  ThisMap->ItemsCurrentCount = 0;
  ThisMap->ItemsMaxCount = 0;

  ThisMap->IteratorCount     = 0;

  Result =
    corememory__transfer(&ThisMap);

  // ---
  return Result;
} // func

void /* func */ coremaps__dropmap
  (/* out */ map** /* param */ AMap)
{
  if (AMap != NULL)
  {
    mapheader*     /* var */ ThisMap = NULL;

    mapnodeheader* /* var */ ThisFirstMapNode = NULL;
    mapnodeheader* /* var */ ThisSecondMapNode = NULL;

    ThisMap =
      (mapheader*) corememory__share(&AMap);
 
    coremaps__clear
      (AMap);
 
    corememory__cleardeallocate
      (AMap, sizeof(mapheader));
  } // if
} // func

// ------------------

bool /* func */ coremaps__underiteration
  (/* in */ map* /* param */ AMap)
{
  bool /* var */ Result = false;
  // ---
  
  if (AMap != NULL)
  {
  	Result = (AMap->IteratorCount > 0);
  } // if
     
  // ---
  return Result;
} // func

mapiterator* /* func */ coremaps__createiteratorforward
  (/* in */ map* /* param */ AMap)
{
  mapiterator* /* var */ Result = NULL;
  // ---
     
  if (AMap != NULL)
  {
    mapheader*         /* var */ ThisMap = NULL;
    mapiteratorheader* /* var */ ThisIterator = NULL;
  	
    ThisMap =
      (mapheader*) corememory__share(AMap);
 
    ThisIterator =
      corememory__clearallocate
        (sizeof(mapiteratorheader));
 
    ThisIterator->Map =
      AMap;
  
    ThisIterator->CurrentNode =
      corememory__share
        (ThisMap->FirstMarker);

    ThisIterator->ItemsMaxCount =
      ThisMap->ItemsMaxCount;

    ThisIterator->ItemIndex =
      0;

    ThisIterator->Direction =
      listdirections__forward;
    
    ThisMap->IteratorCount =
      (ThisMap->IteratorCount + 1);

    Result =
      corememory__transfer(&ThisIterator);
  } // if
     
  // ---
  return Result;
} // func

mapiterator* /* func */ coremaps__createiteratorbackward
  (/* in */ map* /* param */ AMap)
{
  mapiterator* /* var */ Result = NULL;
  // ---
     
  if (AMap != NULL)
  {
    mapheader*         /* var */ ThisMap = NULL;
    mapiteratorheader* /* var */ ThisIterator = NULL;
  	
    ThisMap =
      (mapheader*) corememory__share(AMap);
 
    ThisIterator =
      corememory__clearallocate
        (sizeof(mapiteratorheader));
 
    ThisIterator->Map =
      AMap;
  
    ThisIterator->CurrentNode =
      corememory__share
        (ThisMap->FirstMarker);

    ThisIterator->ItemsMaxCount =
      ThisMap->ItemsMaxCount;

    ThisIterator->ItemIndex =
      ThisMap->ItemsMaxCount;

    ThisIterator->Direction =
      listdirections__backward;
    
    ThisMap->IteratorCount =
      (ThisMap->IteratorCount + 1);

    Result =
      corememory__transfer(&ThisIterator);
  } // if
     
  // ---
  return Result;
} // func

void /* func */ coremaps__dropiterator
  (/* out */ mapiterator** /* param */ AIterator)
{
  Result = (AIterator != NULL);
  if (Result)
  {
    mapiteratorheader** /* var */ ThisIterator = NULL;
    mapheader*          /* var */ ThisMap =  NULL;
      
    ThisIterator = (mapiteratorheader**) AIterator;

    ThisMap =
      (mapheader*) corememory__share
        (ThisIterator->Map);
      
    ThisMap->IteratorCount =
      (ThisMap->IteratorCount - 1);

    corememory__unshare
      (&(ThisIterator->CurrentNode));

    corememory__cleardeallocate
      (ThisIterator);
      
    *AIterator = NULL;
  } // if
} // func

// ------------------

void /* func */ coremaps__assignequalityfunc
  (/* inout */ map*     /* param */ AMap,
   /* in */    equality /* param */ AMatchFunc)
{
  bool CanContinue = NULL;

  CanContinue = 
    (AMap != NULL);
  if (CanContinue)
  {
    mapheader* /* var */ ThisMap =  NULL;

    ThisMap =
      (mapheader*) AMap;
    
    ThisMap->MatchFunc =
      AMatchFunc;
  } // if
} // func

// ------------------

bool /* func */ coremaps__isdone
  (/* in */ const mapiterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator == NULL);
  if (!Result)
  {
    mapiteratorheader* /* var */ ThisIterator = NULL;
    mapheader*         /* var */ ThisMap =  NULL;

    ThisIterator =
      (mapiteratorheader*) corememory__share
        (AIterator);
    ThisMap =
      (mapheader*) corememory__share
        (ThisIterator->Map);

    if (ThisIterator->Direction == listdirections__forward)
    {
      Result =
        (ThisIterator->CurrentNode == ThisMap->LastMarker);
    }
    else if (ThisIterator->Direction == listdirections__backward)
    {
      Result =
        (ThisIterator->CurrentNode == ThisMap->FirstMarker);
    }
  } // if

  // ---
  return Result;
} // func

bool /* func */ coremaps__trymovenext
  (/* inout */ mapiterator* /* param */ AIterator)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    mapiteratorheader* /* var */ ThisIterator = NULL;
    mapheader*         /* var */ ThisMap =  NULL;

    ThisIterator =
      (mapiteratorheader*) corememory__share
        (AIterator);
    ThisMap =
      (mapheader*) corememory__share
        (ThisIterator->Map);
  
    if (ThisIterator->Direction == listdirections__forward)
    {
      Result =
        (ThisIterator->CurrentNode != ThisMap->LastMarker);
      if (Result)
      {
        ThisIterator->ItemIndex =
          (ThisIterator->ItemIndex + 1);
        coremapnodes__moveforward
          (&(ThisIterator->CurrentNode));
      }
    }
    else if (ThisIterator->Direction == listdirections__backward)
    {
      Result =
        (ThisIterator->CurrentNode != ThisMap->FirstMarker);
        
      if (Result)
      {
        ThisIterator->ItemIndex =
          (ThisIterator->ItemIndex - 1);

        coremapnodes__movebackward
          (&(ThisIterator->CurrentNode));
      }
    }
  } // if
     
  // ---
  return Result;
} // func

void /* func */ coremaps__movenext
  (/* inout */ mapiterator* /* param */ AIterator)
{
  /* discard */ coremaps__trymovenext(AIterator);
} // func

// ------------------

bool /* func */ coremaps__trygetnode
  (/* inout */ mapiterator* /* param */ AIterator
   /* out */   mapnode*     /* param */ ANode)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      mapiteratorheader* /* var */ ThisIterator = NULL;
      
      ThisIterator = (mapiteratorheader*) AIterator;
      
      ANode =
        ThisIterator->CurrentNode;
    } // if
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coremaps__trygetitem
  (/* inout */ mapiterator* /* param */ AIterator
   /* out */   keyvalue**   /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---

  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      mapiteratorheader* /* var */ ThisIterator = NULL;
      mapnodeheader*     /* var */ ThisMapNode  = NULL;

      mapheader* /* var */ ThisMap = NULL;
      map*       /* var */ AMap  = NULL;

      ThisIterator = (mapiteratorheader**) AIterator;

      AMap    = ThisIterator->MainMap;
      ThisMap = (mapheader*) AMap;
      ThisMapNode =
        (mapnodeheader*) ThisIterator->CurrentNode;

      corememory__safecopy
        (*AItem, ThisMapNode->KeyValue, ThisMap->ItemsSize);
    } // if
  } // if
    
  // ---
  return Result;
} // func

bool /* func */ coremaps__trygetkey
  (/* inout */ mapiterator* /* param */ AIterator
   /* out */   pointer**    /* param */ AKey)
{
  bool /* var */ Result = false;
  // ---

  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      mapiteratorheader** /* var */ ThisIterator = NULL;
      mapnodeheader*      /* var */ ThisMapNode  = NULL;
      keyvalueheader*     /* var */ ThisKeyValue = NULL;

      ThisIterator = (mapiteratorheader**) AIterator;

      ThisMapNode =
        (mapnodeheader*) corememory__share
          (ThisIterator->CurrentNode);

      ThisKeyValue =
        (keyvalueheader*) corememory__share
          (ThisMapNode->KeyValue);

      corememory__safecopy
        (AItem, ThisKeyValue->Key, ThisMap->ItemsSize);
    } // if
  } // if
    
  // ---
  return Result;
} // func

bool /* func */ coremaps__trygetvalue
  (/* inout */ mapiterator* /* param */ AIterator
   /* out */   pointer**    /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---

  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      mapiteratorheader** /* var */ ThisIterator = NULL;
      mapnodeheader*      /* var */ ThisMapNode  = NULL;
      keyvalueheader*     /* var */ ThisKeyValue = NULL;

      ThisIterator = (mapiteratorheader**) AIterator;

      ThisMapNode =
        (mapnodeheader*) corememory__share
          (ThisIterator->CurrentNode);

      ThisKeyValue =
        (keyvalueheader*) corememory__share
          (ThisMapNode->KeyValue);

      corememory__safecopy
        (ThisKeyValue->Key, AItem, ThisMap->ItemsSize);
    } // if
  } // if
    
  // ---
  return Result;
} // func

bool /* func */ coremaps__trysetvalue
  (/* inout */ mapiterator* /* param */ AIterator
   /* out */   pointer*     /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---

  Result = (AIterator != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      mapiteratorheader** /* var */ ThisIterator = NULL;
      mapnodeheader*      /* var */ ThisMapNode  = NULL;
      mapheader*          /* var */ ThisMap      = NULL;

      keyvalueheader*     /* var */ ThisKeyValue  = NULL;
      pointer*            /* var */ ThisPrevValue = NULL;

      ThisIterator = (mapiteratorheader**) AIterator;

      ThisMapNode =
        (mapnodeheader*) corememory__share
          (ThisIterator->CurrentNode);
          
      ThisMap =
        (mapheader*) corememory__share
          (ThisIterator->Map);

      ThisPrevValue =
        coremapnodes__replacevalue
          (ThisMapNode, AValue);

      corememory__deallocate
        (&ThisPrevValue, ThisMap->ItemsValueSize);
   
      ThisPrevValue =
        corekeyvalues__replacevalue
          (ThisMapNode->KeyValue, AValue);
    } // if
  } // if
    
  // ---
  return Result;
} // func

mapnode* /* func */ coremaps__getnode
  (/* inout */ mapiterator* /* param */ AIterator)
{
  mapnode* /* var */ Result = NULL;
  // ---
     
  /* discard */ coremaps__trygetnode
    (AList, Result);
     
  // ---
  return Result;
} // func

void /* func */ coremaps__getitem
  (/* inout */ mapiterator* /* param */ AIterator
   /* out */   keyvalue**   /* param */ AItem);
{
  /* discard */ coremaps__trygetitem
    (AIterator, AItem);
} // func

void /* func */ coremaps__getkey
  (/* inout */ mapiterator* /* param */ AIterator,
   /* out */   pointer**    /* param */ AKey)
{
  /* discard */ coremaps__trygetkey
    (AIterator, AKey);
} // func

void /* func */ coremaps__getvalue
  (/* inout */ mapiterator* /* param */ AIterator,
   /* out */   pointer**    /* param */ AValue)
{
  /* discard */ coremaps__trygetkey
    (AIterator, AValue);
} // func

void /* func */ coremaps__setvalue
  (/* inout */ mapiterator* /* param */ AIterator
   /* out */   pointer*     /* param */ AValue);
{
  /* discard */ coremaps__trysetvalue
    (AIterator, AValue);
} // func

// ------------------

count_t /* func */ coremaps__getcurrentcount
  (/* in */ const map* /* param */ AMap)
{
  count_t /* var */ Result = 0;
  // ---
  
  bool /* var */ CanContinue = false;
     
  CanContinue = (AIterator != NULL);
  if (CanContinue)
  {
    CanContinue = (AItem != NULL);
    if (CanContinue)
    {
      mapiteratorheader** /* var */ ThisIterator = NULL;
      mapheader*          /* var */ ThisMap      = NULL;

      ThisIterator = (mapiteratorheader**) AIterator;
          
      ThisMap =
        (mapheader*) corememory__share
          (ThisIterator->Map);

      Result =
        ThisMap->ItemsCurrentCount;
    } // if
  } // if
     
  // ---
  return Result;
} // func

count_t /* func */ coremaps__getmaxcount
  (/* in */ const map* /* param */ AMap)
{
  count_t /* var */ Result = 0;
  // ---
  
  bool /* var */ CanContinue = false;
     
  CanContinue = (AIterator != NULL);
  if (CanContinue)
  {
    CanContinue = (AItem != NULL);
    if (CanContinue)
    {
      mapiteratorheader** /* var */ ThisIterator = NULL;
      mapheader*          /* var */ ThisMap      = NULL;

      ThisIterator = (mapiteratorheader**) AIterator;
          
      ThisMap =
        (mapheader*) corememory__share
          (ThisIterator->Map);

      Result =
        ThisMap->ItemsCurrentCount;
    } // if
  } // if
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coremaps__tryclear
  (/* inout */ map* /* param */ AMap)
{
  bool /* var */ Result = false;
  // ---

  Result = (AMap != NULL);
  if (Result)
  {
    mapheader*      /* var */ ThisMap = NULL;

    mapiterator*    /* var */ AIterator = NULL;
    mapiteratorheader* /* var */ ThisIterator = NULL;

    mapnode*        /* var */ AMapNode = NULL;
    mapnodeheader*  /* var */ ThisMapNode = NULL;
    
    keyvalue*       /* var */ AKeyValue  = NULL;

    ThisMap =
      (mapheader*) AMap;

    // remove data first
    AIterator =
      coremaps__createforwarditerator(AMap);
    ThisIterator =
      (mapiteratorheader*) AIterator;

    while (!coremaps__isdone(AIterator))
    {
      AMapNode =
        ThisIterator->CurrentNode;
      ThisMapNode = (mapnodeheader*) AMapNode;

      coremapnodes__extractkeyvalue
        (AMapNode, &AKeyValue);
        
      corekeyvalues__dropbysize
        (&AMapNode, ThisMap->ItemsKeySize, ThisMap->ItemsValueSize);

      coremaps__movenext(AIterator);
    } // while

    coremaps__dropiterator(&AMap);

    // remove nodes
    AIterator =
      coremaps__createiterator(AMap);

    while (!coremaps__isdone(AIterator))
    {
      AMapNode =
        ThisIterator->CurrentNode;
      ThisMapNode = (mapnodeheader*) AMapNode;

      coremaps__movenext(AIterator);

      /* discard */ coremapnodes__dropnode
        (&ThisMapNode);
    } // while

    coremaps__dropiterator(&AMap);

    // update other properties states
    coremapnodes__link
      (ThisMap->FirstMarker, ThisMap->LastMarker);
      
    ThisMap->ItemsMaxCount = 0;
    ThisMap->ItemsCurrentCount = 0;
    ThisMap->IteratorCount = 0;
  } // if
    
  // ---
  return Result;
} // func

void /* func */ coremaps__clear
  (/* inout */ map* /* param */ AMap)
{
  /* discard */ coremaps__tryclear
    (AMap);
} // func

// ------------------

bool /* func */ coremaps__isempty
  (/* in */ const map* /* param */ AMap)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AMap != NULL);
  if (Result)
  {
    mapheader*      /* var */ ThisMap = NULL;

    ThisMap =
      (mapheader*) AMap;

    Result =
      coremapnodes__arelinked
        (ThisMap->FirstMarker, ThisMap->LastMarker) ||
        (ThisMap->ItemsCurrentCount == 0);
  } // if
    
  // ---
  return Result;
} // func

bool /* func */ coremaps__isfull
  (/* in */ const map* /* param */ AMap)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (AMap != NULL);
  if (Result)
  {
    mapheader* /* var */ ThisMap = NULL;
    
    ThisMap = (mapheader*) AMap;

    Result =
      (ThisMap->ItemsMaxCount > 0) &&
      (ThisMap->ItemsCurrentCount == ThisMap->ItemsMaxCount));
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coremaps__hasitems
  (/* in */ const map* /* param */ AMap)
{
  bool /* var */ Result = false;
  // ---
     
  Result = !(coremaps__isempty(AMap));
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coremaps__trykeyfound
  (/* inout */ map*           /* param */ AMap,
   /* in */    const pointer* /* param */ AKey,
   /* out */   bool*          /* param */ AKeyFound)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coremaps__tryitembykey
  (/* inout */ map*           /* param */ AMap,
   /* in */    const pointer* /* param */ AKey,
   /* out */   pointer*       /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coremaps__keyfound
  (/* inout */ map*           /* param */ AMap,
   /* in */    const pointer* /* param */ AKey)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

mapnode* /* func */ coremaps__mapnodebykey
  (/* inout */ map*           /* param */ AMap,
   /* in */    const pointer* /* param */ AKey)
{
  mapnode* /* var */ Result = NULL;
  // ---

  bool /* var */ CanContinue = FALSE;
  bool /* var */ Found       = FALSE;

  CanContinue = (AMap != NULL);
  if (Result)
  {
    mapheader*         /* var */ ThisMap = NULL;

    ThisMap =
      (mapheader*) AMap;

    mapiterator*       /* var */ AIterator = NULL;
    mapiteratorheader* /* var */ ThisIterator = NULL;

    AIterator =
      coremaps__createforwarditerator(AMap);
    ThisIterator =
      (mapiteratorheader*) AIterator;

    CanContinue =
      (!coremaps__isdone(AIterator));
    while (CanContinue)
    {
      AMapNode =
        ThisIterator->CurrentNode;
      ThisMapNode = (mapnodeheader*) AMapNode;



      coremaps__movenext(AIterator);
      
      CanContinue =
        (!coremaps__isdone(AIterator)) && Found;
      
    } // while

    coremaps__dropiterator(&AMap);
  } // if




 
    mapnode*        /* var */ AMapNode = NULL;
    mapnodeheader*  /* var */ ThisMapNode = NULL;
    
    keyvalue*       /* var */ AKeyValue  = NULL;

        
  
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coremaps__tryinsert
  (/* inout */ map*     /* param */ AMap,
   /* in */    pointer* /* param */ AKey,
   /* in */    pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (AMap != NULL) &&
    (AKey != NULL) &&
    (AValue != NULL);
  if (Result)
  {
    Result =
      (! coremaps__underiteration(AMap));  	
    if (Result)
    {
      Result =
        (! coremaps__keyfound(AMap, AKey));  	
      if (Result)
      {
        mapnode* /* var */ ABeforeNode = NULL;
        mapnode* /* var */ AAfterNode  = NULL;
        mapnode* /* var */ ANewNode    = NULL;
           
        mapheader* /* var */ ThisMap = NULL;

        mapnodeheader* /* var */ ThisBeforeNode = NULL;
        mapnodeheader* /* var */ ThisAfterNode  = NULL;
        mapnodeheader* /* var */ ThisNewNode    = NULL;

        ThisMap = (mapheader*) AMap;
      
        AAfterNode = ThisMap->LastMarker;
        ThisAfterNode = (mapnodeheader*) AAfterNode;
 
        ABeforeNode = ThisAfterNode->FirstMarker;
        ThisBeforeNode = (mapnodeheader*) ABeforeNode;

        ANewNode = coremapnodes__createnodebyvalue
          (AKey, AValue);
        ThisNewNode = (mapnodeheader*) ANewNode;
      
        mapnodes__link(ABeforeNode, ANewNode);
        mapnodes__link(ANewNode, AAfterNode);

        ThisMap->ItemsCurrentCount =
          (ThisMap->ItemsCurrentCount + 1);
      } // if  
    } // if  
  } // if  
       
  // ---
  return Result;
} // func

bool /* func */ coremaps__tryupdate
  (/* inout */ map*     /* param */ AMap,
   /* in */    pointer* /* param */ AKey,
   /* in */    pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coremaps__tryreinsert
  (/* inout */ map*     /* param */ AMap,
   /* in */    pointer* /* param */ AKey,
   /* in */    pointer* /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coremaps__insert
  (/* inout */ map*     /* param */ AMap,
   /* in */    pointer* /* param */ AKey,
   /* in */    pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

void /* func */ coremaps__update
  (/* inout */ map*     /* param */ AMap,
   /* in */    pointer* /* param */ AKey,
   /* in */    pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

void /* func */ coremaps__reinsert
  (/* inout */ map*     /* param */ AMap,
   /* in */    pointer* /* param */ AKey,
   /* in */    pointer* /* param */ AValue)
{
  coresystem__nothing();
} // func

// ------------------

bool /* func */ coremaps__tryextract
  (/* inout */ /* restricted */ map*           /* param */ AMap,
   /* in */    /* restricted */ const pointer* /* param */ AKey,
   /* out */   /* restricted */ pointer*       /* param */ AValue)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

pointer* /* func */ coremaps__extract
  (/* inout */ /* restricted */ map*           /* param */ AMap,
   /* in */    /* restricted */ const pointer* /* param */ AKey,
   /* out */   /* restricted */ pointer*       /* param */ AValue)
{
  pointer* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coremaps__trydelete
  (/* inout */ /* restricted */ map*           /* param */ AMap,
   /* in */    /* restricted */ const pointer* /* param */ AKey)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

pointer* /* func */ coremaps__delete
  (/* inout */ /* restricted */ map*           /* param */ AMap,
   /* in */    /* restricted */ const pointer* /* param */ AKey)
{
  pointer* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coremaps__tryexchange
  (/* inout */ /* restricted */ map*           /* param */ AMap,
   /* in */    /* restricted */ const pointer* /* param */ AFirstKey,
   /* in */    /* restricted */ const pointer* /* param */ ASecondKey)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coremaps__exchange
  (/* inout */ /* restricted */ map*           /* param */ AMap,
   /* in */    /* restricted */ const pointer* /* param */ AFirstKey,
   /* in */    /* restricted */ const pointer* /* param */ ASecondKey)
{
  coresystem__nothing();
} // func

// ------------------

 
 // ...

// } // namespace coremaps