/** Module: "coreixdequenodes.c"
 ** Descr.: "Indexed Double Linked Deque Items."
 **/

// namespace coreixdequenodes {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "coreixdequenodes.h"
 
// ------------------

/* friend */ void /* func */ coreixdequenode__link
  (/* inout */ ixdequenode* /* param */ A,
   /* inout */ ixdequenode* /* param */ B)
{
  // ...
} // func

/* friend */ void /* func */ coreixdequenode__moveforward
  (/* inout */ ixdequenode** /* param */ AListNode)
{
  // ...
} // func

/* friend */ void /* func */ coreixdequenode__movebackward
  (/* inout */ ixdequenode** /* param */ AListNode)
{
  // ...
} // func

// ------------------

ixdequenode* /* func */ coreixdequenode__createnode
  (/* in */ pointer* /* param */ AData);
{
  ixdequenode* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

pointer* /* func */ coreixdequenode__dropnode
  (/* out */ ixdequenode** /* param */ AListNode);
{
  pointer* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* override */ const ansinullstring* /* func */ coreixdequenodes__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "coreixdequenodes";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coreixdequenodes__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreixdequenodes__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

 
 // ...

// } // namespace coreixdequenodes