/** Module: "corecplparsers.h"
 ** Descr.: "..."
 **/
 
// namespace corecplparsers {
 
// ------------------
 
#ifndef CORECPLPARSERS__H
#define CORECPLPARSERS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

//$define void noparams

// ------------------
 
//typedef
//  sometype         /* as */ sometype;
   
// ------------------

//struct failure
//{
//  ansichar /* var */ Mesage;
//  pointer* /* var */ Parent;
//} ;

// ------------------

//pointer* /* func */ corecplparsers__malloc
//  (/* in */ size_t /* param */ ASize);
  
//void /* func */ corecplparsers__dealloc
//  (/* out */ pointer** /* param */ ADest,
//   /* in */  size_t    /* param */ ASize);

 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ corecplparsers__modulename
  ( noparams );

/* override */ int /* func */ corecplparsers__setup
  ( noparams );

/* override */ int /* func */ corecplparsers__setoff
  ( noparams );

// ------------------

#endif // CORECPLPARSERS__H

// } // namespace corecplparsers