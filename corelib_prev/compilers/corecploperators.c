/** Module: "corecploperators.c"
 ** Descr.: "Basic Operator Declaration Library."
 **/

// namespace corecploperators {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------
 
#include "corecploperators.h"
 
// ------------------



 // ...

// ------------------

/* override */ const ansinullstring* /* func */ corecploperators__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "corecploperators";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ corecploperators__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corecploperators__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace corecploperators