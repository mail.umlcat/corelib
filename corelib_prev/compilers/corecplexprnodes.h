/** Module: "coreexprnodes.h"
 ** Descr.: "Operation Expression Item Data Structures."
 **/

// namespace coreexprnodes {
 
// ------------------
 
#ifndef COREEXPRNODES__H
#define COREEXPRNODES__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"
#include "corecploperators.h"
#include "corecplexprnodes.h"

// ------------------

typedef
  pointer* /* as */ exprnode;

// ------------------

struct leafnodeheader
{
  pointer* /* var */ EmptyNode;
} ;

struct agroupatornodeheader
{
  exprnode*       /* var */ SubExprParam;
  enum groupmodes /* var */ GroupMode;
} ;

struct indexernodeheader
{
  exprnode*       /* var */ SingleParam;
  enum groupmodes /* var */ GroupMode;
} ;

struct unarynodeheader
{
  exprnode*       /* var */ SingleParam;
  enum unarymodes /* var */ UnaryMode;
} ;

struct binarynodeheader
{
  exprnode* /* var */ LeftParam;
  exprnode* /* var */ RightParam;
} ;

struct ternarynodeheader
{
  exprnode* /* var */ PredicateParam;
  exprnode* /* var */ ThenParam;
  exprnode* /* var */ ElseParam;
} ;

struct listnodeheader
{
  pointer* /* var */ ListParam;
} ;

union unionexprnode
{
  struct leafnodeheader       /* var */ AsOperand;

  struct agroupatornodeheader /* var */ AsGroup;
  struct indexernodeheader    /* var */ AsIndex;

  struct unarynodeheader      /* var */ AsUnary;
  struct binarynodeheader     /* var */ AsBinary;
  struct ternarynodeheader    /* var */ AsTernary;

  struct listnodeheader       /* var */ AsList;
} ;

// ------------------

struct exprnodeheader
{
  union unionexprnode  /* var */ ItemNode;

  /* virtual */
  operatortokens       /* var */ OperatorToken;

  // additional data
  pointer*             /* var */ Data;
 
  size_t               /* var */ TextSize;
  ansinullstring*      /* var */ TextBuffer;

  enum expressionmodes /* var */ ExpressionMode;
  enum operatormodes   /* var */ OperatorMode;
  enum assocmodes      /* var */ Associativity;
  uint8_t              /* var */ Precedence;

} ;

// ------------------

enum expressionmodes /* func */ coreexprnodes__getExpressionMode
  (/* in */ const exprnode* /* param */ ANode);

enum operatormodes /* func */ coreexprnodes__getOperatorMode
  (/* in */ const exprnode* /* param */ ANode);

enum assocmodes /* func */ coreexprnodes__getAssociativity
  (/* in */ const exprnode* /* param */ ANode);

uint8_t /* func */ coreexprnodes__getPrecedence
  (/* in */ const exprnode* /* param */ ANode);

pointer* /* func */ coreexprnodes__getData
  (/* in */ const exprnode* /* param */ ANode);

size_t /* func */ coreexprnodes__getTextSize
  (/* in */ const exprnode* /* param */ ANode);

void /* func */ coreexprnodes__getTextBuffer
  (/* in */  const exprnode* /* param */ ANode,
   /* out */ pointer*        /* param */ AText);

// ------------------

void /* func */ coreexprnodes__setExpressionMode
  (/* in */ const exprnode*            /* param */ ANode,
   /* in */ const enum expressionmodes /* param */ ANewValue);

void /* func */ coreexprnodes__setOperatorMode
  (/* in */ const exprnode*          /* param */ ANode,
   /* in */ const enum operatormodes /* param */ ANewValue);

void /* func */ coreexprnodes__setAssociativity
  (/* in */ const exprnode*       /* param */ ANode,
   /* in */ const enum assocmodes /* param */ ANewValue);

void /* func */ coreexprnodes__setPrecedence
  (/* in */ const exprnode* /* param */ ANode,
   /* in */ const uint8_t   /* param */ ANewValue);

void /* func */ coreexprnodes__setData
  (/* in */ const exprnode* /* param */ ANode,
   /* in */ const pointer*  /* param */ ANewValue);

void /* func */ coreexprnodes__setTextSize
  (/* in */ const exprnode* /* param */ ANode,
   /* in */ const size_t    /* param */ ANewValue);

void /* func */ coreexprnodes__setTextBuffer
  (/* in */ const exprnode*        /* param */ ANode,
   /* in */ const ansinullstring*  /* param */ ANewValue);

// ------------------

exprnode* /* func */ coreexprnodes__createleaf
  (/* in */ pointer* /* param */ AData);

exprnode* /* func */ coreexprnodes__createlist
  (/* in */ pointer* /* param */ AData);

exprnode* /* func */ coreexprnodes__creategroup
  (/* in */ pointer* /* param */ AData);

exprnode* /* func */ coreexprnodes__createindexer
  (/* in */ pointer* /* param */ AData);

exprnode* /* func */ coreexprnodes__createprefix
  (/* in */ pointer* /* param */ AData);

exprnode* /* func */ coreexprnodes__createposfix
  (/* in */ pointer* /* param */ AData);

exprnode* /* func */ coreexprnodes__createbinary
  (/* in */ pointer* /* param */ AData);

exprnode* /* func */ coreexprnodes__createnode
  (/* in */ pointer* /* param */ AData);

// ------------------

pointer* /* func */ coreexprnodes__dropleafnode
  (/* out */ exprnode** /* param */ ANode);

pointer* /* func */ coreexprnodes__dropunarynode
  (/* out */ exprnode** /* param */ ANode);

pointer* /* func */ coreexprnodes__dropbinarynode
  (/* out */ exprnode** /* param */ ANode);

pointer* /* func */ coreexprnodes__dropternarynode
  (/* out */ exprnode** /* param */ ANode);

pointer* /* func */ coreexprnodes__droplistnode
  (/* out */ exprnode** /* param */ ANode);

pointer* /* func */ coreexprnodes__dropagroupatornode
  (/* out */ exprnode** /* param */ ANode);

pointer* /* func */ coreexprnodes__dropindexernode
  (/* out */ exprnode** /* param */ ANode);

pointer* /* func */ coreexprnodes__dropnode
  (/* out */ exprnode** /* param */ ANode);

// ------------------

pointer* /* func */ coreexprnodes__extractdata
  (/* inout */ exprnode* /* param */ ANode);



 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coreexprnodes__modulename
  ( noparams );

/* override */ int /* func */ coreexprnodes__setup
  ( noparams );

/* override */ int /* func */ coreexprnodes__setoff
  ( noparams );

// ------------------

#endif // COREEXPRNODES__H

// } // namespace coreexprnodes