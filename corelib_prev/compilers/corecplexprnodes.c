/** Module: "coreexprnodes.c"
 ** Descr.: "Operation Expression Item Data Structures."
 **/

// namespace coreexprnodes {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"
#include "corecploperators.h"
#include "corecplexprnodes.h"

// ------------------

#include "coreexprnodes.h"
 
// ------------------

enum expressionmodes /* func */ coreexprnodes__getExpressionMode
  (/* in */ const exprnode* /* param */ ANode)
{
  enum expressionmodes /* var */ Result = operatormodes__none;
  // ---

  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;

    ThisNode =
      (struct exprtreeheader*) ANode;

    Result =
      ThisNode->ExpressionMode;
  } // if

  Result = ThisNode;

  // ---
  return Result;
} // func

enum operatormodes /* func */ coreexprnodes__getOperatorMode
  (/* in */ const exprnode* /* param */ ANode)
{
  enum operatormodes /* var */ Result = operatormodes__none;
  // ---

  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;

    ThisNode =
      (struct exprtreeheader*) ANode;

    Result =
      ThisNode->OperatorMode;
  } // if

  Result = ThisNode;

  // ---
  return Result;
} // func

enum assocmodes /* func */ coreexprnodes__getAssociativity
  (/* in */ const exprnode* /* param */ ANode)
{
  enum assocmodes /* var */ Result = assocmodes__none;
  // ---

  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;

    ThisNode =
      (struct exprtreeheader*) ANode;

    Result =
      ThisNode->Associativity;
  } // if

  Result = ThisNode;

  // ---
  return Result;
} // func

uint8_t /* func */ coreexprnodes__getPrecedence
  (/* in */ const exprnode* /* param */ ANode)
{
  uint8 /* var */ Result = 0;
  // ---

  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;

    ThisNode =
      (struct exprtreeheader*) ANode;

    Result =
      ThisNode->Precedence;
  } // if

  Result = ThisNode;

  // ---
  return Result;
} // func

pointer* /* func */ coreexprnodes__getData
  (/* in */ const exprnode* /* param */ ANode)
{
  pointer* /* var */ Result = NULL;
  // ---

  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;

    ThisNode =
      (struct exprtreeheader*) ANode;

    Result =
      ThisNode->Data;
  } // if

  Result = ThisNode;

  // ---
  return Result;
} // func

size_t /* func */ coreexprnodes__getTextSize
  (/* in */ const exprnode* /* param */ ANode)
{
  size_t /* var */ Result = 0;
  // ---

  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;

    ThisNode =
      (struct exprtreeheader*) ANode;

    Result =
      coreansinullstrs__getlength(ThisNode->Text);
  } // if

  Result = ThisNode;

  // ---
  return Result;
} // func

void /* func */ coreexprnodes__getText
  (/* in */  const exprnode* /* param */ ANode,
   /* out */ pointer*        /* param */ AText)
{
  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;

    ansinullstring* /* var */ ThisSourceText = NULL;
    ansinullstring* /* var */ ThisDestText = NULL;

    ThisNode =
      (struct exprtreeheader*) ANode;

    ThisSourceText = (ansinullstring*) ThisNode->Text;
    ThisDestText   = (ansinullstring*) AText;

    if (coreansinullstrs__isempty(ThisText))
    {
      *ThisDestText = ansinullchar;
    }
    else
    {
      coreansinullstrs__assign
        (ThisDestText, ThisSourceText);
    }
  } // if
} // func





// ------------------

void /* func */ coreexprnodes__setExpressionMode
  (/* in */ const exprnode*            /* param */ ANode,
   /* in */ const enum expressionmodes /* param */ ANewValue)
{
  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;

    ThisNode =
      (struct exprtreeheader*) ANode;

    ThisNode->ExpressionMode = ANewValue;
  } // if
} // func

void /* func */ coreexprnodes__setOperatorMode
  (/* in */ const exprnode*          /* param */ ANode,
   /* in */ const enum operatormodes /* param */ ANewValue)
{
  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;

    ThisNode =
      (struct exprtreeheader*) ANode;

    ThisNode->OperatorMode = ANewValue;
  } // if
} // func

void /* func */ coreexprnodes__setAssociativity
  (/* in */ const exprnode*       /* param */ ANode,
   /* in */ const enum assocmodes /* param */ ANewValue)
{
  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;

    ThisNode =
      (struct exprtreeheader*) ANode;

    ThisNode->Associativity = ANewValue;
  } // if
} // func

void /* func */ coreexprnodes__setPrecedence
  (/* in */ const exprnode* /* param */ ANode,
   /* in */ const uint8_t   /* param */ ANewValue)
{
  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;

    ThisNode =
      (struct exprtreeheader*) ANode;

    ThisNode->Precedence = ANewValue;
  } // if
} // func

void /* func */ coreexprnodes__setData
  (/* in */ const exprnode* /* param */ ANode,
   /* in */ const pointer*  /* param */ ANewValue)
{
  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;

    ThisNode =
      (struct exprtreeheader*) ANode;

    ThisNode->Data = ANewValue;
  } // if
} // func

void /* func */ coreexprnodes__setTextSize
  (/* in */ const exprnode* /* param */ ANode,
   /* in */ const size_t    /* param */ ANewValue)
{
  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;

    ThisNode =
      (struct exprtreeheader*) ANode;

    ThisNode->TextSize =
      ANewValue;
  } // if
} // func

void /* func */ coreexprnodes__setTextBuffer
  (/* in */ const exprnode*        /* param */ ANode,
   /* in */ const ansinullstring*  /* param */ ANewValue)
{
  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;

    ThisNode =
      (struct exprtreeheader*) ANode;

    // release previous buffer
    if (! coreansinullstrs__isempty(ThisNode->Text))
    {
      coreansinullstrs__dropstr
        (&(ThisNode->TextBuffer), ThisNode->TextSize);
    } // if

    // adquire new buffer
    ThisNode->TextBuffer =
      coreansinullstrs__compactcopy(ANewValue);
  } // if
} // func

// ------------------

exprnode* /* func */ coreexprnodes__createleaf
  (/* in */ pointer* /* param */ AData)
{
  exprnode* /* var */ Result = NULL;
  // ---

  struct exprnodeheader* /* var */ ThisNode = NULL;

  ThisNode =
    corememory__clearallocate
      (sizeof(struct exprnodeheader)); 

  ThisNode->ExpressionMode =
    expressionmodes__operand;
  ThisNode->OperatorMode =
    operatormodes__none;

  ThisNode->Associativity =
    assocmodes__none;
  ThisNode->Precedence =
    0;
  ThisNode->Data=
    NULL;

  // ---

  Result = (exprnode*) ThisNode;

  // ---
  return Result;
} // func

exprnode* /* func */ coreexprnodes__createlist
  (/* in */ pointer* /* param */ AData)
{
  exprnode* /* var */ Result = NULL;
  // ---

  struct exprnodeheader* /* var */ ThisNode = NULL;

  ThisNode =
    corememory__clearallocate
      (sizeof(struct exprnodeheader)); 

  ThisNode->ExpressionMode =
    expressionmodes__operand;
  ThisNode->OperatorMode =
    operatormodes__list;

  ThisNode->Associativity =
    assocmodes__none;
  ThisNode->Precedence =
    0;
  ThisNode->Data=
    NULL;

  // ---

  Result = (exprnode*) ThisNode;

  // ---
  return Result;
} // func

exprnode* /* func */ coreexprnodes__creategroup
  (/* in */ pointer* /* param */ AData)
{
  exprnode* /* var */ Result = NULL;
  // ---

  struct exprnodeheader* /* var */ ThisNode = NULL;

  ThisNode =
    corememory__clearallocate
      (sizeof(struct exprnodeheader)); 

  ThisNode->ExpressionMode =
    expressionmodes__operator;
  ThisNode->OperatorMode =
    operatormodes__agroupator;

  ThisNode->Associativity =
    assocmodes__none;
  ThisNode->Precedence =
    0;
  ThisNode->Data=
    NULL;

  // ---

  ThisNode->ItemNode.AsGroup.SubExprParam = NULL;
  ThisNode->ItemNode.AsGroup.GroupMode    = groupmodes__opened;

  // ---

  Result = (exprnode*) ThisNode;

  // ---
  return Result;
} // func

exprnode* /* func */ coreexprnodes__createindexer
  (/* in */ pointer* /* param */ AData)
{
  exprnode* /* var */ Result = NULL;
  // ---

  struct exprnodeheader* /* var */ ThisNode = NULL;

  ThisNode =
    corememory__clearallocate
      (sizeof(struct exprnodeheader)); 

  ThisNode->ExpressionMode =
    expressionmodes__operator;
  ThisNode->OperatorMode =
    operatormodes__indexer;

  ThisNode->Associativity =
    assocmodes__none;
  ThisNode->Precedence =
    0;
  ThisNode->Data=
    NULL;

  // ---

  ThisNode->ItemNode.AsIndex.SingleParam = NULL;
  ThisNode->ItemNode.AsIndex.GroupMode   = groupmodes__opened;

  // ---

  Result = (exprnode*) ThisNode;

  // ---
  return Result;
} // func

exprnode* /* func */ coreexprnodes__createprefix
  (/* in */ pointer* /* param */ AData)
{
  exprnode* /* var */ Result = NULL;
  // ---

  struct exprnodeheader* /* var */ ThisNode = NULL;

  ThisNode =
    corememory__clearallocate
      (sizeof(struct exprnodeheader)); 

  ThisNode->ExpressionMode =
    expressionmodes__operator;
  ThisNode->OperatorMode =
    operatormodes__unary;

  ThisNode->Associativity =
    assocmodes__none;
  ThisNode->Precedence =
    0;
  ThisNode->Data=
    NULL;

  // ---

  ThisNode->ItemNode.AsUnary.SingleParam = NULL;
  ThisNode->ItemNode.AsUnary.UnaryMode   = unarymodes__prefix;

  // ---

  Result = (exprnode*) ThisNode;

  // ---
  return Result;
} // func

exprnode* /* func */ coreexprnodes__createposfix
  (/* in */ pointer* /* param */ AData)
{
  exprnode* /* var */ Result = NULL;
  // ---

  struct exprnodeheader* /* var */ ThisNode = NULL;

  ThisNode =
    corememory__clearallocate
      (sizeof(struct exprnodeheader)); 

  ThisNode->ExpressionMode =
    expressionmodes__operator;
  ThisNode->OperatorMode =
    operatormodes__unary;

  ThisNode->Associativity =
    assocmodes__none;
  ThisNode->Precedence =
    0;
  ThisNode->Data=
    NULL;

  // ---

  ThisNode->ItemNode.AsUnary.SingleParam = NULL;
  ThisNode->ItemNode.AsUnary.UnaryMode   = unarymodes__posfix;

  // ---

  Result = (exprnode*) ThisNode;

  // ---
  return Result;
} // func

exprnode* /* func */ coreexprnodes__createbinary
  (/* in */ pointer* /* param */ AData)
{
  exprnode* /* var */ Result = NULL;
  // ---

  struct exprnodeheader* /* var */ ThisNode = NULL;

  ThisNode =
    corememory__clearallocate
      (sizeof(struct exprnodeheader)); 

  ThisNode->ExpressionMode =
    expressionmodes__operator;
  ThisNode->OperatorMode =
    operatormodes__binary;

  ThisNode->Associativity =
    assocmodes__none;
  ThisNode->Precedence =
    0;
  ThisNode->Data=
    NULL;

  // ---

  ThisNode->ItemNode.AsBinary.SingleParam = NULL;
  ThisNode->ItemNode.AsBinary.UnaryMode   = unarymodes__none;

  // ---

  Result = (exprnode*) ThisNode;

  // ---
  return Result;
} // func

exprnode* /* func */ coreexprnode__createnode
  (/* in */ pointer* /* param */ AData);
{
  exprnode* /* var */ Result = NULL;
  // ---

  struct exprnodeheader* /* var */ ThisNode = NULL;

  ThisNode =
    corememory__clearallocate
      (sizeof(struct exprnodeheader)); 




  // ---

  Result = (exprnode*) ThisNode;

  // ---
  return Result;
} // func

// ------------------

pointer* /* func */ coreexprnodes__dropleafnode
  (/* out */ exprnode** /* param */ ANode)
{
  pointer* /* var */ Result = NULL;
  // ---

  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;

    ThisNode =
      (struct exprnodeheader*) *ANode;

    if (ThisNode->OperatorMode == operatormodes__leaf)
    {
      //coreexprnodes__dropnode
        //(&(ThisNode->ItemNode.AsUnary.SingleParam));
    } // if
  } // if

  // ---
  return Result;
} // func

pointer* /* func */ coreexprnodes__dropunarynode
  (/* out */ exprnode** /* param */ ANode)
{
  pointer* /* var */ Result = NULL;
  // ---

  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;

    ThisNode =
      (struct exprnodeheader*) *ANode;

    if (ThisNode->OperatorMode == operatormodes__unary)
    {
      coreexprnodes__dropnode
        (&(ThisNode->ItemNode.AsUnary.SingleParam));
    } // if
  } // if

  // ---
  return Result;
} // func

pointer* /* func */ coreexprnodes__dropbinarynode
  (/* out */ exprnode** /* param */ ANode)
{
  pointer* /* var */ Result = NULL;
  // ---

  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;

    ThisNode =
      (struct exprnodeheader*) *ANode;

    if (ThisNode->OperatorMode == operatormodes__binary)
    {
      coreexprnodes__dropnode
        (&(ThisNode->ItemNode.AsBinary.LeftParam));
      coreexprnodes__dropnode
        (&(ThisNode->ItemNode.AsBinary.RightParam));
    } // if
  } // if

  // ---
  return Result;
} // func

pointer* /* func */ coreexprnodes__dropternarynode
  (/* out */ exprnode** /* param */ ANode)
{
  pointer* /* var */ Result = NULL;
  // ---

  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;

    ThisNode =
      (struct exprnodeheader*) *ANode;

    if (ThisNode->OperatorMode == operatormodes__ternary)
    {
      coreexprnodes__dropnode
        (&(ThisNode->ItemNode.AsTernary.PredicateParam));
      coreexprnodes__dropnode
        (&(ThisNode->ItemNode.AsTernary.ThenParam));
      coreexprnodes__dropnode
        (&(ThisNode->ItemNode.AsTernary.ElseParam));
    } // if
  } // if

  // ---
  return Result;
} // func

pointer* /* func */ coreexprnodes__droplistnode
  (/* out */ exprnode** /* param */ ANode)
{
  pointer* /* var */ Result = NULL;
  // ---

  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;

    ThisNode =
      (struct exprnodeheader*) *ANode;

    if (ThisNode->OperatorMode == operatormodes__list)
    {
      //coreexprnodes__dropnode
        //(&(ThisNode->ItemNode.AsList.PredicateParam));
    } // if
  } // if

  // ---
  return Result;
} // func

pointer* /* func */ coreexprnodes__dropagroupatornode
  (/* out */ exprnode** /* param */ ANode)
{
  pointer* /* var */ Result = NULL;
  // ---

  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;

    ThisNode =
      (struct exprnodeheader*) *ANode;

    if (ThisNode->OperatorMode == operatormodes__agroupator)
    {
      coreexprnodes__dropnode
        (&(ThisNode->ItemNode.AsGroup.SubExprParam));
    } // if
  } // if

  // ---
  return Result;
} // func

pointer* /* func */ coreexprnodes__dropindexernode
  (/* out */ exprnode** /* param */ ANode)
{
  pointer* /* var */ Result = NULL;
  // ---

  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;

    ThisNode =
      (struct exprnodeheader*) *ANode;

    if (ThisNode->OperatorMode == operatormodes__indexer)
    {
      coreexprnodes__dropnode
        (&(ThisNode->ItemNode.AsIndex.SingleParam));
    } // if
  } // if

  // ---
  return Result;
} // func

pointer* /* func */ coreexprnode__dropnode
  (/* out */ exprnode** /* param */ ANode);
{
  pointer* /* var */ Result = NULL;
  // ---

  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;
    size_t                 /* var */ ASize;

    ThisNode =
      (struct exprnodeheader*) ANode;

    // remove parameters, first
    switch (ThisNode->OperatorMode)
    {
      operatormodes__none:
        coresystem__donothing();
      break;

      operatormodes__leaf:
        coreexprnodes__dropleafnode
          (ANode);
      break;

      operatormodes__unary:
        coreexprnodes__dropunarynode
          (ANode);
      break;

      operatormodes__binary:
        coreexprnodes__dropbinarynode
          (ANode);
      break;

      operatormodes__ternary:
        coreexprnodes__dropternarynode
          (ANode);
      break;

      operatormodes__agroupator:
        coreexprnodes__dropagroupatornode
          (ANode);
      break;

      operatormodes__indexer:
        coreexprnodes__dropindexernode
          (ANode);
      break;
    } // switch

    Result = 
      ThisNode->Data;

    // remove text
    ASize =
      coreansinullstrs__getlength(ThisNode->Text) + 1;
    coreansinullstrs__dropstr
      (&(ThisNode->Text, ASize));
  } // if

  // ---
  return Result;
} // func

// ------------------

pointer* /* func */ coreexprnodes__extractdata
  (/* inout */ exprnode* /* param */ ANode)
{
  pointer* /* var */ Result = NULL;
  // ---
    
  if (ANode != NULL)
  {
    struct exprnodeheader* /* var */ ThisNode;

    ThisNode =
      (struct exprexprheader*) ANode;

    Result =
      corememory__transfer(&ThisNode->Data);
  } // if

  // ---
  return Result;
} // func

// ------------------

/* override */ const ansinullstring* /* func */ coreexprnodes__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "corecplexprnodes";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coreexprnodes__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreexprnodes__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

 
 // ...

// } // namespace coreexprnodes