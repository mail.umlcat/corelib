/** Module: "coreexprtrees.c"
 ** Descr.: "Operation Expression Data Structures."
 **/

// namespace coreexprtrees {
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "corevisitors.h"
#include "corecploperators.h"
#include "corecplexprnodes.h"

// ------------------

#include "corecplexprtrees.h"
 
// ------------------

bool /* func */ coreexprtrees__hasroot
  (/* in */ const exprtree* /* param */ AExpr)
{
  bool /* var */ Result = false;
  // ---
     
  if (AExpr != NULL)
  {
    struct exprtreeheader* /* var */ ThisExpr;

    ThisExpr =
      (struct exprtreeheader*) AExpr;

    Result = (ThisExpr->RootNode != NULL);
  } // if

  // ---
  return Result;
} // func

void /* func */ coreexprtrees__insertroot
  (/* inout */ exprtree* /* param */ AExpr,
   /* in */    exprnode* /* param */ ANode)
{
  if (AExpr != NULL) &&  (ANode != NULL)
  {
    struct exprtreeheader* /* var */ ThisExpr;

    ThisExpr =
      (struct exprtreeheader*) AExpr;

    // remove potential prev root
    coreexprtrees__droproot(AExpr);

    ThisExpr->RootNode = ANode;
  } // if
} // func

void /* func */ coreexprtrees__droproot
  (/* inout */ exprtree* /* param */ AExpr)
{
  if (AExpr != NULL)
  {
    struct exprtreeheader* /* var */ ThisExpr;

    ThisExpr =
      (struct exprtreeheader*) AExpr;

    if (coreexprtrees__hasroot(AExpr))
    {
      coreexprnodes__dropnode(&(ThisExpr->RootNode));
    } // if
  } // if
} // func

// ------------------

exprtree* /* func */ coreexprtrees__createexpr
  ( noparams )
{
  exprtree* /* var */ Result = comparison__equal;
  // ---
     
  Result =
    corememory__allocate(sizeof(struct exprtreeheader));

  Result->RootNode = NULL;

  // ---
  return Result;
} // func

void /* func */ coreexprtrees__dropexpr
  (/* out */ exprtree** /* param */ AExpr)
{
  if (AExpr != NULL)
  {



  } // if
} // func

// ------------------

bool /* func */ coreexprtrees__underiteration
  (/* in */ exprtree* /* param */ AExprTree)
{
  bool /* var */ Result = false;
  // ---
  
  if (AExprTree != NULL)
  {
  	Result = (AExprTree->VisitorCount > 0);
  }
     
  // ---
  return Result;
} // func

exprtreevisitor* /* func */ coreexprtrees__createvisitor
  (/* in */ exprtree*           /* param */ AExprTree,
   /* in */ enum listdirections /* param */ AListDir,
   /* in */ enum treedirections /* param */ ATreeDir)
{
  exprtreevisitor* /* var */ Result = NULL;
  // ---
     
  if (AExprTree != NULL)
  {
  	Result = (AExprTree->VisitorCount > 0);
  } // if
  
  // ---
  return Result;
} // func

void /* func */ coreexprtrees__dropvisitor
  (/* out */ exprtreevisitor** /* param */ AVisitor)
{
  Result = (AVisitor != NULL);
  if (Result)
  {
    coresystem__nothing();
  } // if
} // func

// ------------------

bool /* func */ coreexprtrees__isdone
  (/* in */ const exprtreevisitor* /* param */ AVisitor)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AVisitor != NULL);
  if (Result)
  {
    coresystem__nothing();
  } // if

  // ---
  return Result;
} // func

// ------------------



bool /* func */ coreexprtrees__trymovenext
  (/* inout */ exprtreevisitor* /* param */ AVisitor)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    (AVisitor != NULL);
  if (Result)
  {
    exprtreevisitorheader** /* var */ ThisVisitor = NULL;
    ThisVisitor = (exprtreevisitorheader**) AVisitor;

    Result =
      (ThisVisitor->CurrentItem == ThisVisitor->BottomMarker);
 
      if (Result)
      {
        coreexprtreenodes__trymovebackward(&(ThisVisitor->CurrentItem));      
      }
  } // if
     
  // ---
  return Result;
} // func

void /* func */ coreexprtrees__movenext
  (/* inout */ exprtreevisitor* /* param */ AVisitor)
{
  Result = (AVisitor != NULL);
  if (Result)
  {
    coresystem__nothing();
  } // if
} // func

// ------------------

bool /* func */ coreexprtrees__trygetnode
  (/* inout */ exprtreevisitor* /* param */ AVisitor
   /* out */   exprnode*        /* param */ ANode)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AVisitor != NULL);
  if (Result)
  {
    Result = (ANode != NULL);
    if (Result)
    {
      exprtreevisitorheader* /* var */ ThisVisitor = NULL;
      exprtreenodeheader*    /* var */ ThisNode = NULL;

      ThisVisitor = (exprtreevisitorheader*) AVisitor;
      ANode =
        ThisVisitor->CurrentNode;
    } // if
  } // if
     
  // ---
  return Result;
} // func

void /* func */ coreexprtrees__getnode
  (/* inout */ exprtreevisitor* /* param */ AVisitor
   /* out */   exprnode*        /* param */ ANode)
{
  /* discard */ coreexprtrees__trygetnode
    (AVisitor, ANode);
} // func

// ------------------

bool /* func */ coreexprtrees__trygetitem
  (/* inout */ exprtreevisitor* /* param */ AVisitor
   /* out */   pointer**        /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AVisitor != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      exprtreevisitorheader* /* var */ ThisVisitor = NULL;
      exprtreenodeheader*    /* var */ ThisNode = NULL;

      ThisVisitor = (exprtreevisitorheader*) AVisitor;
      ThisNode =
        (exprtreenodeheader*) ThisVisitor->CurrentNode;

      corememory__safecopy
        (AItem, ThisNode, ThisVisitor->ItemsSize);
    } // if
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coreexprtrees__trysetitem
  (/* inout */ exprtreevisitor* /* param */ AVisitor
   /* in */ const  pointer*     /* param */ AItem)
{
  bool /* var */ Result = false;
  // ---
     
  Result = (AVisitor != NULL);
  if (Result)
  {
    Result = (AItem != NULL);
    if (Result)
    {
      exprtreevisitorheader* /* var */ ThisVisitor = NULL;
      exprtreenodeheader*    /* var */ ThisNode = NULL;

      ThisVisitor = (exprtreevisitorheader*) AVisitor;
      ThisNode =
        (exprtreenodeheader*) ThisVisitor->CurrentNode;

      corememory__safecopy
        (ThisNode, AItem, ThisVisitor->ItemsSize);
    } // if
  } // if
     
  // ---
  return Result;
} // func

void /* func */ coreexprtrees__getitem
  (/* inout */ exprtreevisitor* /* param */ AVisitor
   /* out */   pointer*         /* param */ AItem)
{
  /* discard */ coreexprtrees__trygetitem
    (AVisitor, AItem);
} // func

void /* func */ coreexprtrees__setitem
  (/* inout */ exprtreevisitor* /* param */ AVisitor
   /* in */    const  pointer*  /* param */ AItem)
{
  /* discard */ coreexprtrees__trygetitem
    (AVisitor, AItem);
} // func

// ------------------

/* override */ const ansinullstring* /* func */ coreexprtrees__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "corecplexprtrees";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coreexprtrees__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreexprtrees__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coreexprtrees