/** Module: "corecplafnds.h"
 ** Descr.: "Deterministic Finite Automata Library,"
 **         "for Compiler related tools."
 **/

// namespace corecplafnds {
 
// ------------------
 
#ifndef CORECPLAFNDS__H
#define CORECPLAFNDS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "corecplstates.h"
 
// ------------------

struct dfatransition
{
  ansichar /* var */ Text;
  state_t  /* var */ NextState;
} ;

// ------------------

//pointer* /* func */ corecplafnds__malloc
//  (/* in */ size_t /* param */ ASize);
  
//void /* func */ corecplafnds__dealloc
//  (/* out */ pointer** /* param */ ADest,
//   /* in */  size_t    /* param */ ASize);

 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ corecplafnds__modulename
  ( noparams );

/* override */ int /* func */ corecplafnds__setup
  ( noparams );

/* override */ int /* func */ corecplafnds__setoff
  ( noparams );

// ------------------

#endif // CORECPLAFNDS__H

// } // namespace corecplafnds