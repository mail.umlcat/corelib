/** Module: "corecplndfas.c"
 ** Descr.: "Non Deterministic Finite Automata Library,"
 **         "for Compiler related tools."
 **/

// namespace corecplndfas {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "corecplstates.h"
 
// ------------------

#include "corecplndfas.h"
 
// ------------------


 // ...

// ------------------

/* override */ const ansinullstring* /* func */ corecplndfas__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "corecplndfas";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ corecplndfas__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corecplndfas__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace corecplndfas