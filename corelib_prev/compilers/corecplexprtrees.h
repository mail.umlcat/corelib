/** Module: "coreexprtrees.h"
 ** Descr.: "Operation Expression Data Structures."
 **/

// namespace coreexprtrees {
 
// ------------------
 
#ifndef COREEXPRTREES__H
#define COREEXPRTREES__H
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "corevisitors.h"
#include "corecploperators.h"
#include "corecplexprnodes.h"

// ------------------

typedef
  pointer* /* as */ exprtree;

// ------------------

struct exprtreeheader
{
  exprnode* /* var */ RootNode;

  // additional data
  pointer*  /* var */ Data;
  
  // indicates iterator related objects
  count_t  /* var */ VisitorCount;
  
  // ...
} ;

// ------------------

typedef
  pointer* /* as */ exprtreevisitor;

struct exprtreevisitorheader
{
  exprtree*           /* var */ Expression;

  exprnode*           /* var */ CurrentNode;

  // additional data
  pointer*            /* var */ Data;

  enum listdirections /* var */ ListDirection;
  enum treedirections /* var */ TreeDirection;

  // ...
} ;

// ------------------

bool /* func */ coreexprtrees__hasroot
  (/* in */ const exprtree* /* param */ AExpr);

void /* func */ coreexprtrees__insertroot
  (/* inout */ exprtree* /* param */ AExpr,
   /* in */    exprnode* /* param */ ANode);

void /* func */ coreexprtrees__droproot
  (/* inout */ exprtree* /* param */ AExpr);

// ------------------

bool /* func */ coreexprtrees__underiteration
  (/* in */ exprtree* /* param */ AExprTree)

exprtreevisitor* /* func */ coreexprtrees__createvisitor
  (/* in */ exprtree*           /* param */ AExprTree,
   /* in */ enum listdirections /* param */ AListDir,
   /* in */ enum treedirections /* param */ ATreeDir);

void /* func */ coreexprtrees__dropvisitor
  (/* out */ exprtreevisitor** /* param */ AVisitor);

// ------------------

bool /* func */ coreexprtrees__isdone
  (/* in */ const exprtreevisitor* /* param */ AVisitor);

bool /* func */ coreexprtrees__trymovenext
  (/* inout */ exprtreevisitor* /* param */ AVisitor);

void /* func */ coreexprtrees__movenext
  (/* inout */ exprtreevisitor* /* param */ AVisitor);

// ------------------

bool /* func */ coreexprtrees__trygetnode
  (/* inout */ exprtreevisitor* /* param */ AVisitor
   /* out */   exprnode*        /* param */ ANode);

void /* func */ coreexprtrees__getnode
  (/* inout */ exprtreevisitor* /* param */ AVisitor
   /* out */   exprnode*        /* param */ ANode);

// ------------------

bool /* func */ coreexprtrees__trygetitem
  (/* inout */ exprtreevisitor* /* param */ AVisitor
   /* out */   pointer*         /* param */ AItem);

bool /* func */ coreexprtrees__trysetitem
  (/* inout */ exprtreevisitor* /* param */ AVisitor
   /* in */ const pointer*      /* param */ AItem);

void /* func */ coreexprtrees__getitem
  (/* inout */ exprtreevisitor* /* param */ AVisitor
   /* out */   pointer*         /* param */ AItem);

void /* func */ coreexprtrees__setitem
  (/* inout */ exprtreevisitor* /* param */ AVisitor
   /* in */ const pointer*      /* param */ AItem);

// ------------------

exprtree* /* func */ coreexprtrees__createexpr
  ( noparams );
  
void /* func */ coreexprtrees__dropexpr
  (/* out */ exprtree** /* param */ AExpr);
 
// ------------------


 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coreexprtrees__modulename
  ( noparams );

/* override */ int /* func */ coreexprtrees__setup
  ( noparams );

/* override */ int /* func */ coreexprtrees__setoff
  ( noparams );

// ------------------

#endif // COREEXPRTREES__H

// } // namespace coreexprtrees