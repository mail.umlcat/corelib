/** Module: "corecplndfas.h"
 ** Descr.: "Non Deterministic Finite Automata Library,"
 **         "for Compiler related tools."
 **/

// namespace corecplndfas {
 
// ------------------
 
#ifndef CORECPLNDFAS__H
#define CORECPLNDFAS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------
 
#include "corecplstates.h"
 
// ------------------

// associativity modes
enum nfdatransmodes
{
  // mode not assigned
  nfdatransmodes__none,
  nfdatransmodes__deterministic,
  nfdatransmodes__nondeterministic,
} ;

struct ndfatransition
{
  enum nfdatransmodes /* var */ TransMode;
  ansichar            /* var */ Text;
  state_t             /* var */ NextState;
} ;

// ------------------


 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ corecplndfas__modulename
  ( noparams );

/* override */ int /* func */ corecplndfas__setup
  ( noparams );

/* override */ int /* func */ corecplndfas__setoff
  ( noparams );

// ------------------

#endif // CORECPLNDFAS__H

// } // namespace corecplndfas