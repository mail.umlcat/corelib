/** Module: "corecploperators.h"
 ** Descr.: "Basic Operator Declaration Library."
 **/

// namespace corecploperators {
 
// ------------------
 
#ifndef CORECPLOPERATORS__H
#define CORECPLOPERATORS__H

// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

/* virtual enum */
typedef
  uint16_t /* as */ operatortokens;

// ------------------

enum expressionmodes
{
  expressionmodes__none,
  expressionmodes__operator,
  expressionmodes__operand,
} ;

enum operatormodes
{
  operatormodes__none,

  operatormodes__leaf,

  operatormodes__unary,
  operatormodes__binary,
  operatormodes__ternary,

  operatormodes__list,

  operatormodes__agroupator,
  operatormodes__indexer,
} ;

// associativity modes
enum assocmodes
{
  // mode not assigned
  assocmodes__none,
  assocmodes__lefttoright,
  assocmodes__righttoleft,
} ;

enum unarymodes
{
  // mode not assigned
  unarymodes__none,
  unarymodes__prefix,
  unarymodes__posfix,
} ;

#define unarymodes__suffix unarymodes__posfix

enum agrupatormodes
{
  // mode not assigned
  unarymodes__none,
  unarymodes__opened,
  unarymodes__closed,
} ;

// ------------------

 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ corecploperators__modulename
  ( noparams );

/* override */ int /* func */ corecploperators__setup
  ( noparams );

/* override */ int /* func */ corecploperators__setoff
  ( noparams );

// ------------------

#endif // CORECPLOPERATORS__H

// } // namespace corecploperators