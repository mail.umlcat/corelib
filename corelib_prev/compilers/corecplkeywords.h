/** Module: "corecplkeywords.h"
 ** Descr.: "..."
 **/
 
// namespace corecplkeywords {
 
// ------------------
 
#ifndef CORECPLKEYWORDS__H
#define CORECPLKEYWORDS__H
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <float.h"
#include <time.h"
#include <chrono.h"
#include <stdio.h"
#include <math.h"

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

//$define void noparams

// ------------------
 
//typedef
//  sometype         /* as */ sometype;
   
// ------------------

//struct failure
//{
//  ansichar /* var */ Mesage;
//  pointer* /* var */ Parent;
//} ;

// ------------------

//pointer* /* func */ corecplkeywords__malloc
//  (/* in */ size_t /* param */ ASize);
  
//void /* func */ corecplkeywords__dealloc
//  (/* out */ pointer** /* param */ ADest,
//   /* in */  size_t    /* param */ ASize);

 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ corecplkeywords__modulename
  ( noparams );

/* override */ int /* func */ corecplkeywords__setup
  ( noparams );

/* override */ int /* func */ corecplkeywords__setoff
  ( noparams );

// ------------------

#endif // CORECPLKEYWORDS__H

// } // namespace corecplkeywords