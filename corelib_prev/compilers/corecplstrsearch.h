/** Module: "corecplstrsearch.h"
 ** Descr.: "String search operations."
 **/

// namespace corecplstrsearch {
 
// ------------------
 
#ifndef CORECPLSTRSEARCH__H
#define CORECPLSTRSEARCH__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

struct ansinstridxptr
{
  ansichar* /* var */ Ptr;
  index_t*  /* var */ Offset;
} ;

// ------------------

ansinstridxptr* /* func */ corecplstrsearch__packptr
  (/* in */ ansichar* /* param */ AHaystackBuffer,
   /* in */ index_t   /* param */ AHaystackIndex);

void /* func */ corecplstrsearch__dropptr
  (/* out */ ansinstridxptr** /* param */ APtr);

// ------------------

void /* func */ corecplstrsearch__inc
  (/* in */ ansinstridxptr* /* param */ APtr);

void /* func */ corecplstrsearch__dec
  (/* in */ ansinstridxptr* /* param */ APtr);

void /* func */ corecplstrsearch__inccount
  (/* in */ ansinstridxptr* /* param */ APtr,
   /* in */ count_t         /* param */ ACount);

void /* func */ corecplstrsearch__deccount
  (/* in */ ansinstridxptr* /* param */ APtr,
   /* in */ count_t         /* param */ ACount);

// ------------------

ansinullstring* /* func */ coreansinullstrs__firstptrofchar
  (/* in */ /* restricted */ ansichar*       /* param */ AHaystackBuffer,
   /* in */ const size_t                     /* param */ AHaystackSize,
   /* in */ /* restricted */ const ansichar* /* param */ ANeedle,
   /* in */ /* restricted */ ansichar**      /* param */ ADestPtr,
   /* in */ const size_t*                    /* param */ ADestSize,

 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ corecplstrsearch__modulename
  ( noparams );

/* override */ int /* func */ corecplstrsearch__setup
  ( noparams );

/* override */ int /* func */ corecplstrsearch__setoff
  ( noparams );

// ------------------

#endif // CORECPLSTRSEARCH__H

// } // namespace corecplstrsearch