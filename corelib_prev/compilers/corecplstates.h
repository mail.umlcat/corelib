/** Module: "corecplstates.h"
 ** Descr.: "Automatas & Compiler related,"
 **         "library."
 **/

// namespace corecplstates {
 
// ------------------
 
#ifndef CORECPLSTATES__H
#define CORECPLSTATES__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

// Automaton's state definition
// Note: States can be negative !!!
typedef
  sint32_t /* as */ state_t;
typedef
  state_t* /* as */ state_p;

// ------------------




// ------------------

 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ corecplstates__modulename
  ( noparams );

/* override */ int /* func */ corecplstates__setup
  ( noparams );

/* override */ int /* func */ corecplstates__setoff
  ( noparams );

// ------------------

#endif // CORECPLSTATES__H

// } // namespace corecplstates