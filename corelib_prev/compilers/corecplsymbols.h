/** Module: "corecplsymbols.h"
 ** Descr.: "..."
 **/
 
// namespace corecplsymbols {
 
// ------------------
 
#ifndef CORECPLSYMBOLS__H
#define CORECPLSYMBOLS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

//$define void noparams

// ------------------
 
//typedef
//  sometype         /* as */ sometype;
   
// ------------------

//struct failure
//{
//  ansichar /* var */ Mesage;
//  pointer* /* var */ Parent;
//} ;

// ------------------

//pointer* /* func */ corecplsymbols__malloc
//  (/* in */ size_t /* param */ ASize);
  
//void /* func */ corecplsymbols__dealloc
//  (/* out */ pointer** /* param */ ADest,
//   /* in */  size_t    /* param */ ASize);

 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ corecplsymbols__modulename
  ( noparams );

/* override */ int /* func */ corecplsymbols__setup
  ( noparams );

/* override */ int /* func */ corecplsymbols__setoff
  ( noparams );

// ------------------

#endif // CORECPLSYMBOLS__H

// } // namespace corecplsymbols