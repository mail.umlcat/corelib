/** Module: "corecplstates.c"
 ** Descr.: "Automatas & Compiler related,"
 **         "library."
 **/

// namespace corecplstates {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------
 
#include "corecplstates.h"
 
// ------------------


 // ...

// ------------------

/* override */ const ansinullstring* /* func */ corecplstates__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "corecplstates";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ corecplstates__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corecplstates__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace corecplstates