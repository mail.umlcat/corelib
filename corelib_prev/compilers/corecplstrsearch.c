/** Module: "corecplstrsearch.c"
 ** Descr.: "String search operations."
 **/

// namespace corecplstrsearch {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------
 
#include "corecplstrsearch.h"
 
// ------------------

ansinstridxptr* /* func */ corecplstrsearch__packptridx
  (/* in */ ansichar* /* param */ AHaystackBuffer,
   /* in */ index_t   /* param */ AHaystackIndex)
{
  ansinstridxptr* /* var */ Result = NULL;
  // ---
     
  Result =
    corememory__allocate
      (sizeof(ansinstridxptr));
  Result->Ptr    = AHaystackBuffer;
  Result->Offset = AHaystackIndex;

  // ---
  return Result;
} // func

void /* func */ corecplstrsearch__dropptr
  (/* out */ ansinstridxptr** /* param */ APtr)
{
  if (APtr != NULL)
  {
    corememory__deallocate
      (APtr, sizeof(ansinstridxptr));
  } // if
} // func

// ------------------

void /* func */ corecplstrsearch__inc
  (/* in */ ansinstridxptr* /* param */ APtr)
{
  if (APtr != NULL)
  {
    (APtr->Ptr)++;
    (APtr->Offset)++;
  } // if
} // func

void /* func */ corecplstrsearch__dec
  (/* in */ ansinstridxptr* /* param */ APtr)
{
  if (APtr != NULL)
  {
    (APtr->Ptr)--;
    (APtr->Offset)--;
  } // if
} // func

void /* func */ corecplstrsearch__inccount
  (/* in */ ansinstridxptr* /* param */ APtr,
   /* in */ count_t         /* param */ ACount)
{
  if ((APtr != NULL) && (ACount > 0))
  {
    APtr->Ptr    = (APtr->Ptr + ACount);
    APtr->Offset = (APtr->Offset + ACount);
  } // if
} // func

void /* func */ corecplstrsearch__deccount
  (/* in */ ansinstridxptr* /* param */ APtr,
   /* in */ count_t         /* param */ ACount)
{
  if ((APtr != NULL) && (ACount < 0))
  {
    APtr->Ptr    = (APtr->Ptr - ACount);
    APtr->Offset = (APtr->Offset - ACount);
  } // if
} // func

// ------------------

 // ...

// ------------------

/* override */ const ansinullstring* /* func */ corecplstrsearch__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "corecplstrsearch";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ corecplstrsearch__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corecplstrsearch__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace corecplstrsearch