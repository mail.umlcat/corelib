/** Module: "corecpllexers.h"
 ** Descr.: "..."
 **/
 
// namespace corecpllexers {
 
// ------------------
 
#ifndef CORECPLLEXERS__H
#define CORECPLLEXERS__H
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"
#include "corevectors.h"
#include "corematrices.h"

// ------------------

enum statemodes
{
  // mode not assigned
  statemodemodes__none,
  
  // read character,
  // do not move to next character,
  // symbol not fully recognized
  statemodemodes__nonterminalread,
  
  // read character,
  // move to next character
  // symbol not fully recognized
  statemodemodes__nonterminalreadnext,
  
  // read character,
  // do not move to next character
  // symbol recognized,
  // restart for next symbol
  statemodemodes__terminalread,

  // read character,
  // move to next character
  // symbol recognized,
  // restart for next symbol
  statemodemodes__terminalnext,

  // start recognition
  statemodemodes__start,

  // read character,
  // do not move to next character
  // special symbol recognized,
  // stop recognition
  statemodemodes__finalsuccess,

  // read character,
  // do not move to next character
  // special symbol recognized,
  // stop recognition
  statemodemodes__finalerror,

} ;

struct lexerheader
{
  int /* var */ CurrentState;

  int /* var */ PrevState;
  
  
  //pointer* /* var */ Parent;
} ;

typedef
  lexerheader /* as */ lexer;
typedef
  lexer       /* as */ lexer_t;
typedef
  lexer*      /* as */ lexer_p;


 // ...
 
// ------------------

/* override */ int /* func */ corecpllexers__setup
  ( noparams );

/* override */ int /* func */ corecpllexers__setoff
  ( noparams );

// ------------------

#endif // CORECPLLEXERS__H

// } // namespace corecpllexers