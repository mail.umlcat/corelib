/** Module: "corecplcompilers.h"
 ** Descr.: "..."
 **/
 
// namespace corecplcompilers {
 
// ------------------
 
#ifndef CORECPLCOMPILERS__H
#define CORECPLCOMPILERS__H
 
// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <float.h"
#include <time.h"
#include <chrono.h"
#include <stdio.h"
#include <math.h"

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

//$define void noparams

// ------------------
 
//typedef
//  sometype         /* as */ sometype;
   
// ------------------

//struct failure
//{
//  ansichar /* var */ Mesage;
//  pointer* /* var */ Parent;
//} ;

// ------------------

//pointer* /* func */ corecplcompilers__malloc
//  (/* in */ size_t /* param */ ASize);
  
//void /* func */ corecplcompilers__dealloc
//  (/* out */ pointer** /* param */ ADest,
//   /* in */  size_t    /* param */ ASize);

 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ corecplcompilers__modulename
  ( noparams );

/* override */ int /* func */ corecplcompilers__setup
  ( noparams );

/* override */ int /* func */ corecplcompilers__setoff
  ( noparams );

// ------------------

#endif // CORECPLCOMPILERS__H

// } // namespace corecplcompilers