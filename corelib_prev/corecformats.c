/** Module: "corecformats.c"
 ** Descr.: "Implements 'C' style string formatting".
 **/

// namespace corecformats {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include <coreansichars.h>
#include <coreansinullstrs.h>
#include <coredlinklists.h>
#include <coreptrparams.h>
#include <coreuint32s.h>
#include <corebooleans.h>
#include <corefloats.h>
#include <coredatetimes.h>

// ------------------
 
#include <corecformats.h>
 
// ------------------

enum formatspecifiers /* func */ corecformats__char2specifier
  (/* in */ const ansichar /* param */ AChar)
{
  enum formatspecifiers /* var */ Result = formatspecifiers__None;
  // ---
  
  switch (AChar)
  {
    case '%' : 
      Result = formatspecifiers__PercentSymbol;
    break;

    case 's' : 
      Result = formatspecifiers__NullString;
    break;

    case 'c' : 
      Result = formatspecifiers__Char;
    break;

    case 'd' : 
      Result = formatspecifiers__DecimalInt;
    break;
   
    case 'f' : 
      Result = formatspecifiers__Float;
    break;

    case 'p' :
      Result = formatspecifiers__Pointer;
    break;
    
    case 'h' : 
      Result = formatspecifiers__HexadecimalInt;
    break;
   
    case 'o' :
      Result = formatspecifiers__OctalInt;
    break;
    
    case 'u' : 
      Result = formatspecifiers__UnsignedInt;
    break;

    case 'e' : 
    case 'E' : 
    case 'g' : 
    case 'G' : 
      Result = formatspecifiers__ScientificFloat;
    break;

    case 'l' : 
    case 'L' : 
      Result = formatspecifiers__SignedLongInteger;
    break;

    // non standard
    case ':' :
      Result = formatspecifiers__BinaryInteger;
    break;
    
    case '?' : 
      Result = formatspecifiers__Boolean;
    break;
   
    case '!' :
      Result = formatspecifiers__Time;
    break;
    
    case '#' : 
      Result = formatspecifiers__Date;
    break;

  } // switch

  // ---
  return Result;
} // func

ansichar /* func */ corecformats__specifier2char
  (/* in */ const enum formatspecifiers /* param */ ASpecifier)
{
  ansichar /* var */ Result = ansinullchar;
  // ---
  
  switch (ASpecifier)
  {
    case formatspecifiers__PercentSymbol : 
      Result = '%';
    break;

    case formatspecifiers__NullString : 
      Result = 's';
    break;

    case formatspecifiers__Char : 
      Result = 'c';
    break;

    case formatspecifiers__DecimalInt : 
      Result = 'd';
    break;
   
    case formatspecifiers__Float : 
      Result = 'f';
    break;

    case formatspecifiers__Pointer :
      Result = 'p';
    break;
    
    case formatspecifiers__HexadecimalInt : 
      Result = 'h';
    break;
   
    case formatspecifiers__OctalInt :
      Result = 'o';
    break;
    
    case formatspecifiers__UnsignedInt : 
      Result = 'u';
    break;

    case formatspecifiers__ScientificFloat : 
      Result = 'e';
    break;

    case formatspecifiers__SignedLongInteger : 
      Result = 'l';
    break;

    // non standard
    case formatspecifiers__BinaryInteger :
      Result = ':';
    break;
    
    case formatspecifiers__Boolean : 
      Result = '?';
    break;
   
    case formatspecifiers__Time :
      Result = '!';
    break;
    
    case formatspecifiers__Date : 
      Result = '#';
    break;

  } // switch

  // ---
  return Result;
} // func

// ------------------

bool /* func */ corecformats__trymerge
  (/* in */  const ansinullstring* /* param */ ASourceMsg,
   /* in */  const coreptrparams   /* param */ ASourceParams,
   /* out */ formatlist*           /* param */ ADestFormatList)
{
  bool /* var */ Result = false;
  // ---

  size_t /* var */ ASourceSize = 1024;
  
  Result = corecformats__trymergesize
    (ASourceMsg, ASourceSize, ADestFormatList);

  // ---
  return Result;
} // func

bool /* func */ corecformats__trymergesize
  (/* in */  const ansinullstring* /* param */ ASourceMsg,
   /* in */  const size_t          /* param */ ASourceSize,
   /* in */  const coreptrparams   /* param */ ASourceParams,
   /* out */ formatlist*           /* param */ ADestFormatList)
{
  bool /* var */ Result = false;
  // ---
  ADestFormatList = NULL;
  // ---

  doublelinkedlist*  /* var */ AList = NULL;
  struct formatitem* /* var */ AItem = NULL;

  AList =
    coredlinklists__createlistbysize(sizeof(struct formatitem));

  Result =
    (! coreansinullstrs__isempty(ASourceMsg)); 
  if (Result)
  {
    bool     /* var */ SpecExpected = FALSE;
    index_t  /* var */ EachIndex = 0;
    index_t  /* var */ EachParamIndex = 0;
    pointer* /* var */ EachParamPtr = NULL;

    ansichar /* var */ p = ASourceMsg;
    while (p != ansinullchar)
    {
      if (SpecExpected)
      {
        EachParamPtr =
          coreptrparams__itemat
            (ASourceParams, EachParamIndex);
           
        AItem =
          corecformats__newparamitem
            (formatspecifiers__Char, EachParamPtr);
        coredlinklists__insertlast
          (AList, AItem);         
     
        // reset flag
        SpecExpected = FALSE;
      }
      else
      {
        SpecExpected = (*p == '%');
        if (!SpecExpected)
        {
          // add text to list
      	AItem =
            corecformats__newcharitem(*p);
          coredlinklists__insertlast
            (AList, AItem);
        }
      }
    
      EachIndex++;
      p++;
    } // for
  } // if
  ADestFormatList = AList;

  // ---
  return Result;
} // func

void /* func */ corecformats__merge
  (/* in */  const ansinullstring* /* param */ ASourceMsg,
   /* in */  const ptrparams*      /* param */ ASourceParams,
   /* out */ formatlist*           /* param */ ADestFormatList)
{
  corecformats__trymerge
    (ASourceMsg, ADestFormatList);
} // func

void /* func */ corecformats__mergesize
  (/* in */  const ansinullstring* /* param */ ASourceMsg,
   /* in */  const size_t          /* param */ ASourceSize,
   /* in */  const ptrparams*      /* param */ ASourceParams,
   /* out */ formatlist*           /* param */ ADestFormatList)
{
  corecformats__trymergesize
    (ASourceMsg, ASourceSize, ADestFormatList);
} // func

// ------------------

bool /* func */ corecformats__trysplit
  (/* in */  const formatlist*     /* param */ ASourceFormatList,
   /* out */ const ansinullstring* /* param */ ADestMsg,
   /* out */ const ptrparams*      /* param */ ADestParams)
{
  bool /* var */ Result = false;
  // ---
     
  size_t /* var */ ADestSize = 1024;

  corecformats__trysplitsize
    (ASourceFormatList, ADestMsg, ADestSize);

  // ---
  return Result;
} // func

bool /* func */ corecformats__trysplitsize
  (/* in */  const formatlist*     /* param */ ASourceFormatList,
   /* out */ const ansinullstring* /* param */ ADestMsg,
   /* in */  const size_t          /* param */ ADestSize,
   /* out */ const ptrparams*      /* param */ ADestParams)
{
  bool /* var */ Result = false;
  // ---

  coreansinullstrs__clear(ADestMsg);
  // (ADestParams);
  // ---
  
  pointer* /* var */ EachParamPtr = NULL;
  index_t  /* var */ LastParamIndex =
    coreptrparams__getcount(ASourceFormatList);

  for (int /* var */ EachIndex; 
    EachIndex < LastParamIndex;
    EachIndex++)
  {
  	EachParamPtr =
        coreptrparams__itemat(ASourceFormatList, EachIndex);
      
      if (EachParamPtr->Mode == formatmodes__PlainText)
      {
      	coreansinullstrs__concat
            (ADestMsg = EachParamPtr->Text);
      }
      else if (EachParamPtr->Mode == formatmodes__FormatSpecifier)
      {
      	coreptrparams__add
            (ADestParams, EachParamPtr->Param);
      }
  } // for

  // ---
  return Result;
} // func

void /* func */ corecformats__split
  (/* in */  const formatlist*     /* param */ ASourceFormatList,
   /* out */ const ansinullstring* /* param */ ADestMsg)
{
  corecformats__trysplit
    (ASourceFormatList, ADestMsg);
} // func

void /* func */ corecformats__splitsize
  (/* in */  const formatlist*     /* param */ ASourceFormatList,
   /* out */ const ansinullstring* /* param */ ADestMsg,
   /* in */  const size_t          /* param */ ADestSize)
{
  corecformats__trysplitsize
    (ASourceFormatList, ADestMsg, ADestSize);
} // func

// ------------------

struct formatitem* /* func */ corecformats__newchartextitem
  (/* in */  const ansichar /* param */ ASource)
{
  struct formatitem* /* var */ Result = NULL;
  // ---
     
  AResult = (struct formatitem*)
    corememory__allocate(sizeof(struct formatitem));
    
  AResult->Mode = formatmodes__PlainText;
  AResult->Specifier = formatspecifiers__None;
  AResult->Text =
   coreansinullstrs__charcompactcopy(*p);

  // ---
  return Result;
} // func

struct formatitem* /* func */ corecformats__newstrtextitem
  (/* in */  const ansinullstring* /* param */ ASource)
{
  struct formatitem* /* var */ Result = NULL;
  // ---
     
  AResult = (struct formatitem*)
    corememory__allocate(sizeof(struct formatitem));
    
  AResult->Mode = formatmodes__PlainText;
  AResult->Specifier = formatspecifiers__None;
  AResult->Text =
   coreansinullstrs__compactcopy(*p);

  // ---
  return Result;
} // func

struct formatitem* /* func */ corecformats__newstrtextitemsize
  (/* in */  const ansinullstring* /* param */ ASourceBuffer,
   /* in */  const size_t          /* param */ ASourceSize)
{
  struct formatitem* /* var */ Result = NULL;
  // ---
     
  AResult = (struct formatitem*)
    corememory__allocate(sizeof(struct formatitem));
    
  AResult->Mode = formatmodes__PlainText;
  AResult->Specifier = formatspecifiers__None;
  AResult->Text =
   coreansinullstrs__compactcopy(*p);

  // ---
  return Result;
} // func

// ------------------

struct formatitem* /* func */ corecformats__newparamitem
  (/* in */  const enum formatspecifiers /* param */ AFormatSpecifier,
   /* in */  const pointer*         /* param */ AItem)
{
  struct formatitem* /* var */ Result = NULL;
  // ---
     
  AResult = (struct formatitem*)
    corememory__allocate(sizeof(struct formatitem));
    
  AResult->Mode      = formatmodes__FormatSpecifier;
  AResult->Specifier = AFormatSpecifier;
  AResult->Param     = AItem;
 
  AResult->Text      = NULL;

  // ---
  return Result;
} // func

// ------------------

/* override */ int /* func */ corecformats__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corecformats__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------


// } // namespace corecformats