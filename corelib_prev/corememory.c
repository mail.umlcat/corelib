/** Module: "corememory.c"
 ** Descr.: "Memory Management Library."
 **/
 
// namespace corememory {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>
 
// ------------------

#include "coresystem.h"
//#include "corefunctors.h"
//#include "coreuuids.h"
//#include "coreoswarnings.h"

// ------------------
 
#include "corememory.h"
 
// ------------------

/* inline */ bool /* func */ corememory__isassigned
  (/* in */ const pointer* /* param */ ASource)
{
  bool /* var */ Result = NULL;
  // ---

  Result =
    (ASourceBuffer != NULL); 
     
  // ---
  return Result;
} // func

/* inline */ bool /* func */ corememory__isassignedsize
  (/* in */ const pointer* /* param */ ASourceBuffer, 
   /* in */ const size_t   /* param */ ASourceSize)
{
  bool /* var */ Result = NULL;
  // ---

  Result =
    (ASourceBuffer != NULL) && (ASourceSize > 0); 

  // ---
  return Result;
} // func

/* inline */ bool /* func */ corememory__isassignedarray
  (/* in */ const pointer* /* param */ AArrayBuffer, 
   /* in */ const count_t  /* param */ AItemCount)
{
  bool /* var */ Result = NULL;
  // ---

  Result =
    (AArrrayBuffer != NULL) && (AItemCount > 0); 

  // ---
  return Result;
} // func

/* inline */ bool /* func */ corememory__isassignedarraysize
  (/* in */ const pointer* /* param */ AArrayBuffer, 
   /* in */ const size_t   /* param */ AItemSize,
   /* in */ const count_t  /* param */ AItemCount)
{
  bool /* var */ Result = NULL;
  // ---

  Result =
    (AArrrayBuffer != NULL) && (AItemSize > 0); && (AItemCount > 0);

  // ---
  return Result;
} // func

// ------------------

/* inline */ size_t /* func */ corememory__minsize
  (/* in */ const size_t /* param */ A,
   /* in */ const size_t /* param */ B)
{
  size_t /* var */ Result = 0;
  // ---
  
  Result = (A < B) ? A : B; 
     
  // ---
  return Result;
} // func

/* inline */ size_t /* func */ corememory__maxsize
  (/* in */ const size_t /* param */ A,
   /* in */ const size_t /* param */ B)
{
  size_t /* var */ Result = 0;
  // ---
     
  Result = (A > B) ? A : B; 
    
  // ---
  return Result;
} // func

/* inline */ size_t /* func */ corememory__zerominsize
  (/* in */ const size_t /* param */ A,
   /* in */ const size_t /* param */ B)
{
  size_t /* var */ Result = 0;
  // ---
     
  A = (A > 0) ? A : 0; 
  B = (B > 0) ? B : 0; 
  Result = (A > B) ? A : B; 
     
  // ---
  return Result;
} // func

/* inline */ size_t /* func */ corememory__zeromaxsize
  (/* in */ const size_t /* param */ A,
   /* in */ const size_t /* param */ B)
{
  size_t /* var */ Result = 0;
  // ---
     
  A = (A > 0) ? A : 0; 
  B = (B > 0) ? B : 0; 
  Result = (A < B) ? A : B; 
     
  // ---
  return Result;
} // func

/* inline */ size_t /* func */ corememory__oneminsize
  (/* in */ const size_t /* param */ A,
   /* in */ const size_t /* param */ B)
{
  size_t /* var */ Result = 0;
  // ---
     
  A = (A > 1) ? A : 1; 
  B = (B > 1) ? B : 1; 
  Result = (A > B) ? A : B; 
     
  // ---
  return Result;
} // func

/* inline */ size_t /* func */ corememory__onemaxsize
  (/* in */ const size_t /* param */ A,
   /* in */ const size_t /* param */ B)
{
  size_t /* var */ Result = 0;
  // ---
     
  A = (A > 1) ? A : 1; 
  B = (B > 1) ? B : 1; 
  Result = (A < B) ? A : B; 
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ corememory__areoverlapped
  (/* in */ const /* nonrestricted */ pointer* /* param */ A,
   /* in */ const /* nonrestricted */ pointer* /* param */ B,
   /* in */ const size_t                       /* param */ ACount)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ corememory__sametype
  (/* in */  const typecode /* param */ A,
   /* in */  const typecode /* param */ B)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// -------------------

size_t /* func */ corememory__typetosize
  (/* in */ const typecode /* param */ ADataType)
{
  size_t /* var */ Result = 0;
  // ---
     
     
  // ---
  return Result;
} // func

pointer* /* func */ corememory__new
  (/* in */ const typecode /* param */ ADataType)
{
  pointer* /* var */ Result = NULL;
  // ---
  
  //Result =
    //malloc(ASize);
     
  // ---
  return Result;
} // func

void /* func */ corememory__drop
  (/* out */ pointer**      /* param */ ADest,
   /* in */  const typecode /* param */ ADataType)
{
  //memset(ADest, 0, ASize);
  //dealloc(*ADest, ASize);
}

pointer* /* func */ corememory__newarray
  (/* in */ const typecode /* param */ ADataType,
   /* in */ const count_t  /* param */ AItemCount)
{
  pointer* /* var */ Result = NULL;
  // ---
  
  //Result =
    //malloc(ASize);
     
  // ---
  return Result;
} // func

void /* func */ corememory__droparray
  (/* out */ pointer**      /* param */ ADest,
   /* in */  const typecode /* param */ ADataType,
   /* in */  const count_t  /* param */ AItemCount)
{
  //memset(ADest, 0, ASize);
  //dealloc(*ADest, ASize);
} // func

// -------------------

/* inline */ pointer* /* func */ corememory__itemat
  (/* inout */ pointer*      /* param */ ABufferPtr,
   /* in */    const index_t /* param */ AItemIndex)
{
  pointer* /* var */ Result = NULL;
  // ---
  
  uint8_t* /* var */ ThisBufferPtr = NULL;

  ThisBufferPtr = (uint8_t*) ABufferPtr;
    
  Result =
    (pointer*) &(ThisBufferPtr[AItemIndex]);

  // ---
  return Result;
} // func

/* inline */ pointer* /* func */ corememory__itematbysize
  (/* inout */ pointer*      /* param */ ABufferPtr,
   /* in */    const index_t /* param */ AItemIndex,
   /* in */    const size_t  /* param */ AItemSize)
{
  pointer* /* var */ Result = NULL;
  // ---
  
  index_t  /* var */ ThisIndex;
  uint8_t* /* var */ ThisBufferPtr = NULL;

  ThisIndex =
    (AItemSize * AItemIndex);
  ThisBufferPtr = (uint8_t*) ABufferPtr;
    
  Result =
    (pointer*) &(ThisBufferPtr[ThisIndex]);
     
  // ---
  return Result;
} // func

/* inline */ pointer* /* func */ corememory__itematbytype
  (/* inout */ pointer*       /* param */ ABufferPtr,
   /* in */    const index_t  /* param */ AItemIndex,
   /* in */    const typecode /* param */ AItemType)
{
  pointer* /* var */ Result = NULL;
  // ---

  size_t  /* var */ AItemSize =
    corememory__typetosize(AItemType);

  index_t  /* var */ ThisIndex;
  uint8_t* /* var */ ThisBufferPtr = NULL;

  ThisIndex =
    (AItemSize * AItemIndex);
  ThisBufferPtr = (pointer*) ABufferPtr;
    
  Result =
    (pointer*) &(ThisBufferPtr[ThisIndex]);
     
  // ---
  return Result;
} // func

// -------------------

void /* func */ corememory__insertat
  (/* inout */ pointer*      /* param */ ADestPtr,
   /* in */    const size_t  /* param */ ADestSize,
   /* in */    const index_t /* param */ AIndex,
   /* in */    const count_t /* param */ AItemCount)
{
  coresystem__nothing( );
} // func

void /* func */ corememory__deleteat
  (/* inout */ pointer*      /* param */ ADestPtr,
   /* in */    const size_t  /* param */ ADestSize,
   /* in */    const index_t /* param */ AIndex,
   /* in */    const count_t /* param */ AItemCount)
{
  coresystem__nothing( );
} // func

// -------------------

void /* func */ corememory__insertatarray
  (/* inout */ pointer*      /* param */ ADestPtr,
   /* in */    const size_t  /* param */ ADestSize,
   /* in */    const size_t  /* param */ AItemSize,
   /* in */    const index_t /* param */ AIndex,
   /* in */    const count_t /* param */ AItemCount)
{
  coresystem__nothing( );
} // func

void /* func */ corememory__deleteatarray
  (/* inout */ pointer*      /* param */ ADestPtr,
   /* in */    const size_t  /* param */ ADestSize,
   /* in */    const size_t  /* param */ AItemSize,
   /* in */    const index_t /* param */ AIndex,
   /* in */    const count_t /* param */ AItemCount)
{
  coresystem__nothing( );
} // func

// -------------------

/* inline */ bool /* func */ corememory__assigned
  (/* inout */ pointer* /* param */ APointer)
{
  return (APointer != NULL);
} // func

/* inline */ pointer* /* func */ corememory__transfer
  (/* inout */ pointer** /* param */ APointer)
{
  pointer* /* var */ Result = NULL;
  // ---
  
  // backup value in another place
  Result = *APointer;
  // clear variable
  APointer = NULL;
     
  // ---
  return Result;
} // func

/* inline */ pointer* /* func */ corememory__share
  (/* inout */ pointer** /* param */ APointer)
{
  pointer* /* var */ Result = NULL;
  // ---
  
  // backup value in another place
  Result = *APointer;
     
  // ---
  return Result;
} // func

/* inline */ void /* func */ corememory__unshare
  (/* inout */ pointer** /* param */ APointer)
{
  APointer = NULL;
} // func

// -------------------

pointer* /* func */ corememory__allocate
  (/* in */ const size_t /* param */ ASize)
{
  pointer* /* var */ Result = NULL;
  // ---
  
  Result =
    (pointer*) malloc(ASize);
     
  // ---
  return Result;
} // func

void /* func */ corememory__deallocate
  (/* out */ pointer**    /* param */ ADest,
   /* in */  const size_t /* param */ ASize)
{
  memset(*ADest, 0, ASize);
  free(*ADest);
} // func

pointer* /* func */ corememory__allocatearray
  (/* in */ const size_t  /* param */ AItemSize,
   /* in */ const count_t /* param */ AItemCount)
{
  pointer* /* var */ Result = NULL;
  // ---
  
  size_t /* var */ SizeInBytes =
    (AItemSize * AItemCount);
  Result =
    (pointer*) malloc(SizeInBytes);
     
  // ---
  return Result;
} // func

void /* func */ corememory__deallocaterray
  (/* out */ pointer**     /* param */ ADest,
   /* in */  const size_t  /* param */ AItemSize,
   /* in */  const count_t /* param */ AItemCount)
{
  size_t /* var */ SizeInBytes =
    (AItemSize * AItemCount);
  memset(*ADest, 0, SizeInBytes);
  free(*ADest);
}

// -------------------

pointer* /* func */ corememory__clearallocate
  (/* in */ const size_t /* param */ ASize)
{
  pointer* /* var */ Result = NULL;
  // ---
  
  Result =
    calloc(ASize, 1);
     
  // ---
  return Result;
} // func

void /* func */ corememory__cleardeallocate
  (/* out */ pointer**    /* param */ ADest,
   /* in */  const size_t /* param */ ASize)
{
  memset(*ADest, 0, ASize);
  free(*ADest);
  ADest = NULL;
}

// -------------------

pointer* /* func */ corememory__clearallocatearray
  (/* in */ const size_t  /* param */ AItemSize,
   /* in */ const count_t /* param */ AItemCount)
{
  pointer* /* var */ Result = NULL;
  // ---
  
  //size_t /* var */ SizeInBytes =
  //  (AItemSize * AItemCount);

  Result =
    calloc(AItemSize, AItemCount);
     
  // ---
  return Result;
} // func

void /* func */ corememory__cleardeallocatearray
  (/* out */ pointer**    /* param */ ADest,
   /* in */ const size_t  /* param */ AItemSize,
   /* in */ const count_t /* param */ AItemCount)
{
  size_t /* var */ SizeInBytes =
    (AItemSize * AItemCount);

  memset(*ADest, 0, SizeInBytes);
  free(*ADest);
  ADest = NULL;
}

// -------------------

void /* func */ corememory__clear
  (/* out */ pointer*     /* param */ ADest,
   /* in */  const size_t /* param */ ASize)
{
  memset(ADest, 0, ASize);
}

void /* func */ corememory__cleararray
  (/* out */ pointer*      /* param */ ADest,
   /* in */  const size_t  /* param */ AItemSize,
   /* in */  const count_t /* param */ AItemCount)
{
  size_t /* var */ SizeInBytes =
    (AItemSize * AItemCount);

  memset(ADest, 0, SizeInBytes);
}

void /* func */ corememory__fill
  (/* out */ pointer*     /* param */ ADest,
   /* in */  const size_t /* param */ ASize,
   /* in */  const byte_t /* param */ AValue)
{
  memset(ADest, AValue, ASize);
} // func

void /* func */ corememory__fillarray
  (/* out */ pointer*      /* param */ ADest,
   /* in */  const size_t  /* param */ AItemSize,
   /* in */  const count_t /* param */ AItemCount,
   /* in */  const byte_t  /* param */ AValue)
{
  size_t /* var */ SizeInBytes =
    (AItemSize * AItemCount);

  memset(ADest, AValue, SizeInBytes);
} // func

// -------------------

bool /* func */ corememory__safeisclear
  (/* in */ /* restricted */ pointer* /* param */ ASource,
   /* in */ const size_t              /* param */ ACount)
{
    bool /* var */ Result = false;
    // ---
    
    Result =
      (ASource) && (ACount > 0);
    if (Result)
    {
      byte_t* /* var */ P = NULL;
    
      int  /* var */ i = 0;
      bool /* var */ CanContinue = false;
      bool /* var */ Found = false;
    
      P = (pointer*) ASource;
      
      while (CanContinue)
      {
         Found =
          (*P != 0);
      	
         CanContinue =
          (i < ACount) && (!Found);
         i++;
      } // while
      
      Result = (!Found);
    } // if
     
    // ---
    return Result;
} // func

bool /* func */ corememory__safeiscleararray
  (/* in */ /* restricted */ pointer* /* param */ ASource,
   /* in */  const size_t             /* param */ AItemSize,
   /* in */  const count_t            /* param */ AItemCount)
{
    bool /* var */ Result = false;
    // ---
    
    Result =
      (ASource != NULL) && (AItemCount > 0);
    if (Result)
    {
      byte_t* /* var */ P = NULL;
 
      size_t  /* var */ ASize = 0;
   
      int  /* var */ i = 0;
      bool /* var */ CanContinue = false;
      bool /* var */ Found = false;
      
      ASize = (AItemSize * AItemCount);
      P = (pointer*) ASource;
      
      while (CanContinue)
      {
         Found =
          (*P != 0);
      	
         CanContinue =
          (i < ASize) && (!Found);
         i++;
      } // while
      
      Result = (!Found);
    } // if
     
    // ---
    return Result;
} // func

// -------------------

bool /* func */ corememory__safeequal
  (/* in */ /* restricted */ pointer* /* param */ A,
   /* in */ /* restricted */ pointer* /* param */ B,
   /* in */ const size_t              /* param */ ACount)
{
    bool /* var */ Result = false;
    // ---
    
    Result =
      (A != NULL) && (B != NULL) && (ACount > 0);
    if (Result)
    {
      byte_t* /* var */ C = NULL;
      byte_t* /* var */ D = NULL;
    
      int  /* var */ i = 0;
      bool /* var */ CanContinue = false;
      bool /* var */ Found = false;
    
      C = (pointer*) A;
      D = (pointer*) B;
      
      while (CanContinue)
      {
         Found =
          (*C != *D);
      	
         CanContinue =
          (i < ACount) && (!Found);
         i++;
      } // while
      
      Result = (!Found);
    } // if
     
    // ---
    return Result;
} // func

bool /* func */ corememory__safeequalarray
  (/* in */ /* restricted */ pointer* /* param */ A,
   /* in */ /* restricted */ pointer* /* param */ B,
   /* in */  const size_t             /* param */ AItemSize,
   /* in */  const count_t            /* param */ AItemCount)
{
    bool /* var */ Result = false;
    // ---
    
    Result =
      (A != NULL) && (B != NULL) && (AItemCount > 0);
    if (Result)
    {
      byte_t* /* var */ C = NULL;
      byte_t* /* var */ D = NULL;

      size_t  /* var */ ASize = 0;
    
      int  /* var */ i = 0;
      bool /* var */ CanContinue = false;
      bool /* var */ Found = false;
    
      ASize = (AItemSize * AItemCount);
      C = (pointer*) A;
      D = (pointer*) B;
      
      while (CanContinue)
      {
         Found =
          (*C != *D);
      	
         CanContinue =
          (i < ASize) && (!Found);
         i++;
      } // while
      
      Result = (!Found);
    } // if
     
    // ---
    return Result;
} // func

// -------------------

void /* func */ corememory__safecopy
  (/* out */ /* restricted */ pointer*       /* param */ ADest,
   /* in */  /* restricted */ const pointer* /* param */ ASource,
   /* in */  const size_t                    /* param */ ACount)
{
  memmove((pointer*) ADest, (pointer*) ASource, ACount);
} // func

void /* func */ corememory__safecopyarray
  (/* out */ /* restricted */ pointer*       /* param */ ADest,
   /* in */  /* restricted */ const pointer* /* param */ ASource,
   /* in */  const size_t                    /* param */ AItemSize,
   /* in */  const count_t                   /* param */ AItemCount)
{
  size_t /* var */ SizeInBytes =
    (AItemSize * AItemCount);

  corememory__safecopy
    (ADest, ASource, SizeInBytes);
} // func

void /* func */ corememory__safereversecopy
  (/* out */ /* restricted */ pointer*       /* param */ ADest,
   /* in */  /* restricted */ const pointer* /* param */ ASource,
   /* in */  const size_t                    /* param */ ACount)
{
  uint8_t* /* var */ s = NULL;
  uint8_t* /* var */ d = NULL;
  
  s = (pointer*) ASource;
  d = (pointer*) ADest;

  for (int /* var */ i = 0; i < ACount; i++)
  {
    *d = *s;
    s++; d++;
  } // for
} // func

void /* func */ corememory__safereversecopyarray
  (/* out */ /* restricted */ pointer*       /* param */ ADest,
   /* in */  /* restricted */ const pointer* /* param */ ASource,
   /* in */  const size_t                    /* param */ AItemSize,
   /* in */  const count_t                   /* param */ AItemCount)
{
  size_t /* var */ SizeInBytes =
    (AItemSize * AItemCount);
    
  corememory__safereversecopy
    (ADest, ASource, SizeInBytes);
} // func

void /* func */ corememory__safemove
  (/* out */ /* restricted */ pointer* /* param */ ADest,
   /* out */ /* restricted */ pointer* /* param */ ASource,
   /* in */  const size_t              /* param */ ACount)
{
	//memset(ADest, AValue, ASize);
} // func

void /* func */ corememory__safemovearray
  (/* out */ /* restricted */ pointer* /* param */ ADest,
   /* out */ /* restricted */ pointer* /* param */ ASource,
   /* in */  const size_t              /* param */ AItemSize,
   /* in */  const count_t             /* param */ AItemCount)
{
  size_t /* var */ SizeInBytes =
    (AItemSize * AItemCount);

  corememory__safecopy
    (ADest, ASource, SizeInBytes);
} // func

void /* func */ corememory__overlappedcopy
  (/* out */ /* nonrestricted */ pointer*       /* param */ ADest,
   /* in */  /* nonrestricted */ const pointer* /* param */ ASource,
   /* in */  const size_t                       /* param */ ACount)
{
	//memmove(ADest, AValue, ASize);
} // func

void /* func */ corememory__overlappedcopyarray
  (/* out */ /* nonrestricted */ pointer*       /* param */ ADest,
   /* in */  /* nonrestricted */ const pointer* /* param */ ASource,
   /* in */  const size_t                       /* param */ AItemSize,
   /* in */  const count_t                      /* param */ AItemCount)
{
  size_t /* var */ SizeInBytes =
    (AItemSize * AItemCount);

  corememory__overlappedcopy
    (ADest, ASource, SizeInBytes);
} // func

void /* func */ corememory__overlappedmove
  (/* out */ /* nonrestricted */ pointer* /* param */ ADest,
   /* out */ /* nonrestricted */ pointer* /* param */ ASource,
   /* in */  const size_t                 /* param */ ACount)
{
  //memmove(ADest, AValue, ASize);
} // func

void /* func */ corememory__overlappedmovearray
  (/* out */ /* nonrestricted */ pointer* /* param */ ADest,
   /* out */ /* nonrestricted */ pointer* /* param */ ASource,
   /* in */  const size_t                 /* param */ AItemSize,
   /* in */  const count_t                /* param */ AItemCount)
{
  size_t /* var */ SizeInBytes =
    (AItemSize * AItemCount);

  corememory__overlappedmove
    (ADest, ASource, SizeInBytes);
} // func

// ------------------

/* inline*/ void /* func */ corememory__safeexchangeptr
  (/* inout */ /* restricted */ pointer** /* param */ A,
   /* inout */ /* restricted */ pointer** /* param */ B)
{
  pointer* /* var */ C = NULL;
  C  = *A;
  *A = *B;
  *B = C;
} // if

void /* func */ corememory__safeexchange
  (/* out */ /* restricted */ pointer* /* param */ A,
   /* out */ /* restricted */ pointer* /* param */ B,
   /* in */  const size_t              /* param */ ACount)
{
  //memset(ADest, AValue, ASize);
} // if

void /* func */ corememory__safeexchangearray
  (/* out */ /* restricted */ pointer* /* param */ A,
   /* out */ /* restricted */ pointer* /* param */ B,
   /* in */  const size_t              /* param */ AItemSize,
   /* in */  const count_t             /* param */ AItemCount)
{
  size_t /* var */ SizeInBytes =
    (AItemSize * AItemCount);

  corememory__safeexchange
    (A, B, SizeInBytes);
} // if

void /* func */ corememory__overlappedexchange
  (/* out */ /* nonrestricted */ pointer* /* param */ A,
   /* out */ /* nonrestricted */ pointer* /* param */ B,
   /* in */  const size_t                 /* param */ ACount)
{
	//memset(ADest, AValue, ASize);
} // if

void /* func */ corememory__overlappedexchangearray
  (/* out */ /* nonrestricted */ pointer* /* param */ A,
   /* out */ /* nonrestricted */ pointer* /* param */ B,
   /* in */  const size_t                 /* param */ AItemSize,
   /* in */  const count_t                /* param */ AItemCount)
{
  size_t /* var */ SizeInBytes =
    (AItemSize * AItemCount);

  corememory__overlappedexchange
    (A, B, SizeInBytes);
} // if

// ------------------

pointer* /* func */ corememory__duplicatecopy
  (/* in */ pointer*     /* param */ ASourceBuffer,
   /* in */ const size_t /* param */ ASourceSize)
{
  pointer* /* var */ Result = NULL;
  // ---
  
  Result =
    (pointer*) corememory__allocate(ASourceSize);
  corememory__safecopy
    (Result, ASourceBuffer, ASourceSize);
     
  // ---
  return Result;
} // func

pointer* /* func */ corememory__duplicatecopyarray
  (/* in */ pointer*      /* param */ ASourceBuffer,
   /* in */ const size_t  /* param */ AItemSize,
   /* in */ const count_t /* param */ AItemCount)
{
  pointer* /* var */ Result = NULL;
  // ---
  
  size_t /* var */ SizeInBytes =
    (AItemSize * AItemCount);

  Result =
    corememory__duplicatecopy
      (ASourceBuffer, SizeInBytes);
     
  // ---
  return Result;
} // func

// ------------------

struct typedptr* /* func */ corememory__packtypedptr
  (/* in */ pointer* /* param */ ASourcePtr,
   /* in */ typecode /* param */ ASourceType)
{
  struct typedptr* /* var */ Result = NULL;
  // ---
  
  Result =
    (struct typedptr*) corememory__allocate(sizeof(struct typedptr));
  
  memmove(Result->itemtype, ASourceType, sizeof(typecode));
  
  Result->itemptr  = ASourcePtr;
 
  // ---
  return Result;
} // func

struct sizedptr* /* func */ corememory__packsizedptr
  (/* in */ pointer* /* param */ ASourcePtr,
   /* in */ size_t   /* param */ ASourceSize)
{
  struct sizedptr* /* var */ Result = NULL;
  // ---
  
  Result =
    (struct sizedptr*) corememory__allocate
      (sizeof(struct sizedptr));
    
  Result->itemsize = ASourceSize;
  Result->itemptr  = ASourcePtr;
 
  // ---
  return Result;
} // func

struct indexedptr* /* func */ corememory__packindexedptr
  (/* in */ pointer* /* param */ ASourcePtr,
   /* in */ size_t   /* param */ ASourceIndex)
{
  struct indexedptr* /* var */ Result = NULL;
  // ---
  
  Result =
    (struct indexedptr*) corememory__allocate
      (sizeof(struct indexedptr));

  Result->itemindex = ASourceIndex;
  Result->itemptr   = ASourcePtr;
 
  // ---
  return Result;
} // func


 // ...

// ------------------

/* override */ const ansinullstring* /* func */ corememory__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "corememory";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ corememory__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corememory__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------


// } // namespace corememory