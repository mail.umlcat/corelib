/** Module: "corerecstreams.h"
 ** Descr.: "..."
 **/
 
// namespace corerecstreams {
 
// ------------------
 
#ifndef CORERECSTREAMS__H
#define CORERECSTREAMS__H
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"
#include "coreansiospaths.h"
#include "coreansiosfilesys.h"
#include "corestreams.h"

// ------------------

typedef
  stream /* as */ recordstream;

// ------------------

bool /* func */ corerecordstreams__isclosed
  (/* in */ const recordstream* /* param */ AStream);

// ------------------

void /* func */ corerecordstreams__assignpath
   (/* inout */ recordstream*         /* param */ ADest,
    /* in */    const ansinullstring* /* param */ AFullPath);
    /* in */    const size_t          /* param */ AIndex);

// ------------------

bool /* func */ corerecordstreams__tryclose
  (/* inout */ recordstream* /* param */ AStream);

bool /* func */ corerecordstreams__tryopenreadonly
  (/* inout */ recordstream* /* param */ AStream);

bool /* func */ corerecordstreams__tryopenrewrite
  (/* inout */ recordstream* /* param */ AStream);

bool /* func */ corerecordstreams__tryopenappend
  (/* inout */ recordstream* /* param */ AStream);

// ------------------

//typedef
//  sometype         /* as */ sometype;
   
// ------------------

//struct failure
//{
//  ansichar /* var */ Message;
//  pointer* /* var */ Parent;
//} ;

// ------------------

//pointer* /* func */ corerecstreams__malloc
//  (/* in */ size_t /* param */ ASize);
  
//void /* func */ corerecstreams__dealloc
//  (/* out */ pointer** /* param */ ADest,
//   /* in */  size_t    /* param */ ASize);

 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coreansidictnodes__modulename
  ( noparams );

/* override */ int /* func */ coreansidictnodes__setup
  ( noparams );

/* override */ int /* func */ coreansidictnodes__setoff
  ( noparams );

// ------------------

#endif // CORERECSTREAMS__H

// } // namespace corerecstreams