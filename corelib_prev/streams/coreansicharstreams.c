/** Module: "coreansicharstreams.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coreansicharstreams {
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"
#include "coreansiospaths.h"
#include "coreansiosfilesys.h"
#include "corestreams.h"

// ------------------

#include "coreansicharstreams.h"

// ------------------

bool /* func */ coreansicharstreams__isclosed
  (/* in */ const ansicharstream* /* param */ AStream)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreansicharstreams__tryclose
  (/* inout */ ansicharstream* /* param */ AStream)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreansicharstreams__tryopenreadonly
  (/* inout */ ansicharstream* /* param */ AStream)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func
 
bool /* func */ coreansicharstreams__tryopenrewrite
  (/* inout */ ansicharstream* /* param */ AStream)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func
  
bool /* func */ coreansicharstreams__tryopenappend
  (/* inout */ ansicharstream* /* param */ AStream)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreansicharstreams__trygetchar
  (/* inout */ ansicharstream* /* param */ ASource,
   /* out */ ansichar*         /* param */ ABuffer)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func
  
bool /* func */ coreansicharstreams__tryputchar
  (/* inout */ ansicharstream* /* param */ ADest,
   /* in */ const ansichar*    /* param */ ABuffer)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreansicharstreams__trymovenext
  (/* inout */ ansicharstream* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreansicharstreams__tryreadchar
  (/* inout */ ansicharstream* /* param */ ASource,
   /* out */ ansichar*         /* param */ ABuffer)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func
  
bool /* func */ coreansicharstreams__trywritechar
  (/* inout */ ansicharstream* /* param */ ADest,
   /* in */ const ansichar*    /* param */ ABuffer)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

ansichar /* func */ coreansicharstreams__getchar
  (/* inout */ ansicharstream* /* param */ ASource)
{
  ansichar /* var */ Result = nullchar;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreansicharstreams__putchar
  (/* inout */ ansicharstream* /* param */ ADest,
   /* in */ const ansichar     /* param */ ABuffer)
{
  // ...
} // func

void /* func */ coreansicharstreams__movenext
  (/* inout */ ansicharstream* /* param */ ASource)
{
  // ...
} // func

ansichar /* func */ coreansicharstreams__readchar
  (/* inout */ ansicharstream* /* param */ ASource)
{
  ansichar /* var */ Result = nullchar;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coreansicharstreams__writechar
  (/* inout */ ansicharstream* /* param */ ADest,
   /* in */ const ansichar     /* param */ ABuffer)
{
  // ...
} // func

// ------------------

bool /* func */ coreansiconsoles__tryreadstr
  (/* inout */ ansicharstream* /* param */ ASource,
   /* out */ ansinullstring*   /* param */ ABuffer)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreansiconsoles__trywritestr
  (/* inout */ ansicharstream*    /* param */ ADest,
   /* in */ const ansinullstring* /* param */ ABuffer)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreansiconsoles__trywriteeoln
  (/* inout */ ansicharstream* /* param */ ADestStream)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreansiconsoles__tryreadstrcount
  (/* inout */ ansicharstream* /* param */ ASource,
   /* out */ ansinullstring*   /* param */ ADestBuffer,
   /* in */ const size_t       /* param */ ADestSize)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* func */ coreansiconsoles__trywritestrcount
  (/* inout */ ansicharstream*    /* param */ ADest,
   /* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ASourceSize)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

void /* func */ coreansiconsoles__readstr
  (/* inout */ ansicharstream*       /* param */ ASourceStream,
   /* out */ ansinullstring* /* param */ ADestBuffer)
{
  // ...
} // func

void /* func */ coreansiconsoles__writestr
  (/* inout */ ansicharstream*            /* param */ ADestStream,
   /* in */ const ansinullstring* /* param */ ASourceBuffer)
{
  // ...
} // func

void /* func */ coreansicharstreams__readstrcount
  (/* inout */ ansicharstream* /* param */ ASource,
   /* out */ ansinullstring*   /* param */ ADestBuffer,
   /* in */ const size_t       /* param */ ADestSize)
{
  // ...
} // func

void /* func */ coreansicharstreams__writestrcount
  (/* inout */ ansicharstream*    /* param */ ADest,
   /* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ASourceSize)
{
  // ...
} // func

 
 // ...
// ------------------

/* override */ const ansinullstring* /* func */ coreansidictnodes__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "coreansicharstreams";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coreansidictnodes__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreansidictnodes__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coreansicharstreams