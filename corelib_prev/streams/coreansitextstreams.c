/** Module: "coreansitextstreams.c"
 ** Descr.: "Predefined (ANSI) Text files library."
 **/

// namespace coreansitextstreams {
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"
#include "coreansiospaths.h"
#include "coreansiosfilesys.h"
#include "corestreams.h"

// ------------------

#include "coreansitextstreams.h"
 
// ------------------

bool /* func */ coreansitextstreams__tryisclosed
  (/* inout */ ansitextstream* /* param */ ASource,
   /* out */   bool*           /* param */ AResult)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    ((ASource != NULL) &&
     (AResult != NULL));
  if (Result)
  {
    streamheader* /* var */ ThisSource = NULL;
  
    ThisSource = (streamheader*) ASource;
    AResult =
      (ThisSource->AccessModes == streamaccessmodes__Closed);
  } // if
   
  // ---
  return Result;
} // func

bool /* func */ coreansitextstreams__isclosed
  (/* in */ const ansitextstream* /* param */ AStream)
{
  bool /* var */ Result = false;
  // ---
  
  /* discard */ coreansitextstreams__tryisclosed
    (AStream, &Result);
    
  // ---
  return Result;
} // func

bool /* func */ coreansitextstreams__trygetpos
  (/* inout */ ansitextstream* /* param */ ASource,
   /* out */   index_t*        /* param */ AResult)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    ((ASource != NULL) &&
     (AResult != NULL));
  if (Result)
  {
    streamheader* /* var */ ThisSource = NULL;
  
    ThisSource = (streamheader*) ASource;
    AResult =
      ftell(ThisSource->InternalFile);
  } // if
   
  // ---
  return Result;
} // func

index_t /* func */ coreansitextstreams__getpos
  (/* in */ const ansitextstream* /* param */ AStream)
{
  index_t /* var */ Result = 0;
  // ---
  
  /* discard */ coreansitextstreams__trygetpos
    (AStream, &Result);
    
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreansitextstreams__tryclose
  (/* inout */ ansitextstream* /* param */ AStream)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (ASource != NULL);
  if (Result)
  {
    streamheader* /* var */ ThisSource = NULL;
  
    ThisSource = (streamheader*) ASource;
    
    Result =
      (ThisSource->AccessModes != streamaccessmodes__Undefined) &&
      (ThisSource->AccessModes != streamaccessmodes__Closed);
    if (Result)
    {
      // execute OS command
      fclose(ThisSource->InternalFile);
      ThisSource->InternalFile = NULL;
     
      // clear "flag"
      ThisSource->AccessModes = streamaccessmodes__Closed;
      ThisSource->DataType    = streamdatatypes__Unknown;
      ThisSource->Ownership   = streamownerships__Undefined;
      
      ThisSource->RecIndex    = 0;
    }
  } // if
   
  // ---
  return Result;
} // func

bool /* func */ coreansitextstreams__tryopenreadonly
  (/* inout */ ansitextstream* /* param */ AStream)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (ASource != NULL);
  if (Result)
  {
    streamheader* /* var */ ThisSource = NULL;
  
    ThisSource = (streamheader*) ASource;
    
    ThisSource->InternalFile = NULL;
    ThisSource->InternalFile =
      fopen(ThisSource->FullPath, "r");
        
    Result =
      (ThisSource->InternalFile != NULL);
    if (Result)
    {
      ThisSource->AccessModes = streamaccessmodes__OpenReadOnly;
      ThisSource->DataType    = streamdatatypes__Text;
      ThisSource->Ownership   = streamownerships__User;
     
      ThisSource->RecIndex    = 0;
    }
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coreansitextstreams__tryopenrewrite
  (/* inout */ ansitextstream* /* param */ AStream)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (ASource != NULL);
  if (Result)
  {
    streamheader* /* var */ ThisSource = NULL;
  
    ThisSource = (streamheader*) ASource;
    
    ThisSource->InternalFile = NULL;
    ThisSource->InternalFile =
      fopen(ThisSource->FullPath, "w");
        
    Result =
      (ThisSource->InternalFile != NULL);
    if (Result)
    {
      ThisSource->AccessModes = streamaccessmodes__OpenWriteOnly;
      ThisSource->DataType    = streamdatatypes__Text;
      ThisSource->Ownership   = streamownerships__User;
      
      ThisSource->RecIndex    = 0;
    }
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coreansitextstreams__tryopenappend
  (/* inout */ ansitextstream* /* param */ AStream)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (ASource != NULL);
  if (Result)
  {
    streamheader* /* var */ ThisSource = NULL;
  
    ThisSource = (streamheader*) ASource;
    
    ThisSource->InternalFile = NULL;
    ThisSource->InternalFile =
      fopen(ThisSource->FullPath, "a");
        
    Result =
      (ThisSource->InternalFile != NULL);
    if (Result)
    {
      ThisSource->AccessModes = streamaccessmodes__Append;
      ThisSource->DataType    = streamdatatypes__Text;
      ThisSource->Ownership   = streamownerships__User;
      
      ThisSource->RecIndex    = 0;
    }
  } // if
    
  // ---
  return Result;
} // func

// ------------------

bool /* func */ coreansitextstreams__trygetchar
  (/* inout */ ansitextstream* /* param */ ASource,
   /* out */   ansichar*       /* param */ ABuffer)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (ASource != NULL) && (ABuffer!= NULL);
  if (Result)
  {
    streamheader* /* var */ ThisSource = NULL;
  
    ThisSource = (streamheader*) ASource;
    
    Result =
      (ThisSource->AccessModes == streamaccessmodes__OpenReadOnly) ||
      (ThisSource->AccessModes == streamaccessmodes__OpenReadWrite);
    if (Result)
    {
      *ABuffer = ansinullchar;
    
      int /* var */ AIntValue = 0;
    
      AIntValue = fgetchar(ThisSource->InternalFile);
      Result = (AIntValue != EOF);
      if (Result)
      {
        *ABuffer = (ansichar) AIntValue;
      }
    }
  } // if
    
  // ---
  return Result;
} // func

bool /* func */ coreansitextstreams__tryputchar
  (/* inout */ ansitextstream* /* param */ ADest,
   /* in */    const ansichar* /* param */ ABuffer)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (ASource != NULL);
  if (Result)
  {
    streamheader* /* var */ ThisSource = NULL;
  
    ThisSource = (streamheader*) ASource;
    
    Result =
      (ThisSource->AccessModes == streamaccessmodes__OpenWriteOnly) ||
      (ThisSource->AccessModes == streamaccessmodes__Append) ||
      (ThisSource->AccessModes == streamaccessmodes__OpenReadWrite);

    if (Result)
    {
      putchar(ThisSource->InternalFile, AValue);
    }
  } // if
    
  // ---
  return Result;
} // func

bool /* func */ coreansitextstreams__tryreadchar
  (/* inout */ ansitextstream* /* param */ ASource,
   /* out */   ansichar*       /* param */ ABuffer)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    coreansitextstreams__trygetchar(ASource);
  if (Result)
  {
    Result =
      coreansitextstreams__trymovenext(ASource);
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coreansitextstreams__trywritechar
  (/* inout */ ansitextstream* /* param */ ADest,
   /* in */    const ansichar* /* param */ ABuffer)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    coreansitextstreams__tryputchar(ASource, ABuffer);
  if (Result)
  {
    Result =
      coreansitextstreams__trymovenext(ASource);
  } // if
     
  // ---
  return Result;
} // func

bool /* func */ coreansitextstreams__trymovenext
  (/* inout */ ansitextstream* /* param */ ADest)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (ASource != NULL);
  if (Result)
  {
    streamheader* /* var */ ThisSource = NULL;
  
    ThisSource = (streamheader*) ASource;
    
    // update pointer
    ThisSource->RecIndex =
      (ThisSource->RecIndex + 1);
    
    fseek(ThisSource->InternalFile, ThisSource->RecIndex, SEEK_SET);
  } // if
    
  // ---
  return Result;
} // func

// ------------------

ansichar /* func */ coreansitextstreams__getchar
  (/* inout */ ansitextstream* /* param */ ASource)
{
  ansichar /* var */ Result = nullchar;
  // ---
     
  /* discard */ coreansitextstreams__trygetchar
    (ASource, &Result);
 
  // ---
  return Result;
} // func

void /* func */ coreansitextstreams__putchar
  (/* inout */ ansitextstream* /* param */ ADest,
   /* in */ const ansichar     /* param */ ABuffer)
{
  /* discard */ coreansitextstreams__tryputchar
    (ASource);
} // func

ansichar /* func */ coreansitextstreams__readchar
  (/* inout */ ansitextstream* /* param */ ASource)
{
  ansichar /* var */ Result = nullchar;
  // ---
     
  /* discard */ coreansitextstreams__tryreadchar
    (ASource, &Result);
     
  // ---
  return Result;
} // func

void /* func */ coreansitextstreams__writechar
  (/* inout */ ansitextstream* /* param */ ADest,
   /* in */ const ansichar     /* param */ ABuffer)
{
  /* discard */ coreansitextstreams__writechar
    (ASource, ABuffer);
} // func

void /* func */ coreansitextstreams__movenext
  (/* inout */ ansitextstream* /* param */ ASource)
{
  /* discard */ coreansitextstreams__trymovenext
    (ASource);
} // func

// ------------------

bool /* func */ coreansitextstreams__tryreadstr
  (/* inout */ ansitextstream* /* param */ ASource,
   /* out */   ansinullstring* /* param */ ABuffer)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (ASource != NULL) && (ABuffer!= NULL);
  if (Result)
  {
    streamheader* /* var */ ThisSource = NULL;
    size_t        /* var */ Delta = 0;
 
    ThisSource = (streamheader*) ASource;
    
    Result =
      (ThisSource->AccessModes == streamaccessmodes__OpenReadOnly) ||
      (ThisSource->AccessModes == streamaccessmodes__OpenReadWrite);
    if (Result)
    {
      *ABuffer = ansinullchar;
      Delta = 1024;
    
      fgets(ABuffer, Delta, ThisSource->InternalFile);
      
      Delta = strlen(ASource) + 1;
           
      // update pointer
      ThisSource->RecIndex =
        (ThisSource->RecIndex + Delta);
    } // if
  } // if
    
  // ---
  return Result;
} // func

bool /* func */ coreansitextstreams__trywritestr
  (/* inout */ ansitextstream*       /* param */ ADest,
   /* in */    const ansinullstring* /* param */ ABuffer)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (ASource != NULL) && (ABuffer!= NULL);
  if (Result)
  {
    streamheader* /* var */ ThisSource = NULL;
    size_t        /* var */ Delta = 0;
 
    ThisSource = (streamheader*) ASource;
    
    Result =
      (ThisSource->AccessModes == streamaccessmodes__OpenWriteOnly) ||
      (ThisSource->AccessModes == streamaccessmodes__Append) ||
      (ThisSource->AccessModes == streamaccessmodes__OpenReadWrite);

    if (Result)
    {
      Delta = strlen(ASource);

      Result = (Delta > 1);
      if (Delta > 1)
      {
        fputs(ABuffer, ThisSource->InternalFile);
      
        // update pointer
        ThisSource->RecIndex =
          (ThisSource->RecIndex + Delta);
      } // if
    }
  } // if
    
  // ---
  return Result;
} // func

bool /* func */ coreansitextstreams__trywriteeoln
  (/* inout */ ansitextstream* /* param */ ADestStream)
{
  bool /* var */ Result = false;
  // ---
  
  Result =
    coreansitextstreams__trywritestr
      ("\n");
      
  // ---
  return Result;
} // func

bool /* func */ coreansitextstreams__tryreadstrcount
  (/* inout */ ansitextstream* /* param */ ASource,
   /* out */   ansinullstring* /* param */ ADestBuffer,
   /* in */    const size_t    /* param */ ADestSize)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (ASource != NULL) && (ABuffer!= NULL);
  if (Result)
  {
    streamheader* /* var */ ThisSource = NULL;
    size_t        /* var */ Delta = 0;
 
    ThisSource = (streamheader*) ASource;
    
    Result =
      (ThisSource->AccessModes == streamaccessmodes__OpenReadOnly) ||
      (ThisSource->AccessModes == streamaccessmodes__OpenReadWrite);
    if (Result)
    {
      *ABuffer = ansinullchar;
      Delta = ADestSize;
    
      fgets(ABuffer, Delta, ThisSource->InternalFile);
      
      Delta = strlen(ASource) + 1;
           
      // update pointer
      ThisSource->RecIndex =
        (ThisSource->RecIndex + Delta);
    } // if
  } // if
    
  // ---
  return Result;
} // func

bool /* func */ coreansitextstreams__trywritestrcount
  (/* inout */ ansitextstream*       /* param */ ADest,
   /* in */    const ansinullstring* /* param */ ASourceBuffer,
   /* in */    const size_t          /* param */ ASourceSize)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (ASource != NULL) && (ABuffer!= NULL);
  if (Result)
  {
    streamheader* /* var */ ThisSource = NULL;
    size_t        /* var */ Delta = 0;
 
    ThisSource = (streamheader*) ASource;
    
    Result =
      (ThisSource->AccessModes == streamaccessmodes__OpenWriteOnly) ||
      (ThisSource->AccessModes == streamaccessmodes__Append) ||
      (ThisSource->AccessModes == streamaccessmodes__OpenReadWrite);

    if (Result)
    {
      Delta = ASourceSize;

      Result = (Delta > 1);
      if (Delta > 1)
      {
        fputs(ABuffer, ThisSource->InternalFile);
      
        // update pointer
        ThisSource->RecIndex =
          (ThisSource->RecIndex + Delta);
      } // if
    }
  } // if
    
  // ---
  return Result;
} // func
  
// ------------------

void /* func */ coreansitextstreams__readstr
  (/* inout */ ansitextstream* /* param */ ASourceStream,
   /* out */   ansinullstring* /* param */ ADestBuffer)
{
  /* discard */ coreansitextstreams__tryreadstr
    (ASource, ADestBuffer);
} // func

void /* func */ coreansitextstreams__writestr
  (/* inout */ ansitextstream*       /* param */ ADestStream,
   /* in */    const ansinullstring* /* param */ ASourceBuffer)
{
  /* discard */ coreansitextstreams__trywritestr
    (ASource, ADestBuffer);
} // func

void /* func */ coreansitextstreams__writeeoln
  (/* inout */ ansitextstream* /* param */ ADestStream)
{
  /* discard */ coreansitextstreams__trywriteeoln();
} // func

 
 // ...
 
 
// ------------------

/* override */ int /* func */ coresystem__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
  
  streamheader* /* var */ AInputStream =
    coresystem__clearallocate(sizeof(ansitextstream));
    AInputStream->InternalFile = stdin; // <-- !!!
    AInputStream->RecSize      = sizeof(ansichar);
    AInputStream->AccessModes  = streamaccessmodes__OpenReadOnly;
    AInputStream->DataType     = streamdatatypes__Text;
    AInputStream->Ownership    = streamorigin__System;

  streamheader* /* var */ AOutputStream =
    coresystem__clearallocate(sizeof(ansitextstream));
    AOutputStream->InternalFile = stdout; // <-- !!!
    AOutputStream->RecSize      = sizeof(ansichar);
    AOutputStream->AccessModes  = streamaccessmodes__OpenReadOnly;
    AOutputStream->DataType     = streamdatatypes__Text;
    AOutputStream->Ownership    = streamorigin__System;
   
  streamheader* /* var */ AErrorStream =
    coresystem__clearallocate(sizeof(ansitextstream));
    AErrorStream->InternalFile = stderr; // <-- !!!
    AErrorStream->RecSize      = sizeof(ansichar);
    AErrorStream->AccessModes  = streamaccessmodes__OpenReadOnly;
    AErrorStream->DataType     = streamdatatypes__Text;
    AErrorStream->Ownership    = streamorigin__System;

  inputstream = 
    corememory__transfer(*AInputStream);
  ouputstream =
    corememory__transfer(*AOutputStream);
  errorstream =
    corememory__transfer(*AErrorStream);

  // ---

  coresystem__inputstream
    corememory__share(inputstream);
  
  coresystem__ouputstream
    corememory__share(ouputstream);
  
  coresystem__errorstream
    corememory__share(errorstream);
    
  // ---
  return Result;
} // func

/* override */ int /* func */ coresystem__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---

  corememory__unshare
    (&coresystem__inputstream);
  
  corememory__unshare
    (&coresystem__ouputstream);
  
  corememory__unshare
    (&coresystem__errorstream);
    
  // ---
     
  coresystem__cleardeallocate
    (&errorstream, sizeof(ansitextstream));
  coresystem__cleardeallocate
    (&ouputstream, sizeof(ansitextstream));
  coresystem__cleardeallocate
    (&inputstream, sizeof(ansitextstream));
    
  // ---
  return Result;
} // func

// ------------------

/* override */ const ansinullstring* /* func */ coreansidictnodes__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "coreansitextstreams";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coreansidictnodes__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreansidictnodes__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coreansitextstreams