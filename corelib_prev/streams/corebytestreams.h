/** Module: "corebytestreams.h"
 ** Descr.: "..."
 **/
 
// namespace corebytestreams {
 
// ------------------
 
#ifndef COREBYTESTREAMS__H
#define COREBYTESTREAMS__H
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"
#include "coreansiospaths.h"
#include "coreansiosfilesys.h"
#include "corestreams.h"

// ------------------

typedef
  stream /* as */ bytestream;

// ------------------

enum StreamAccessModes /* func */ corebytestreams__getAccessMode
  (/* in */ const bytestream* /* param */ AStream);

enum StreamDataTypes /* func */ corebytestreams__getDataType
  (/* in */ bytestream* /* param */ AStream);

enum StreamOwnerships /* func */ corebytestreams_getOwnership
  (/* in */ bytestream* /* param */ AStream);

// ------------------

bool /* func */ corebytestreams__isdone
  (/* in */ const bytestream* /* param */ AStream);

bool /* func */ corebytestreams__isclosed
  (/* in */ const bytestream* /* param */ AStream);

// ------------------

bool /* func */ corebytestreams__canread
  (/* in */ const bytestream* /* param */ AStream);
  
bool /* func */ corebytestreams__canwrite
  (/* in */ const bytestream* /* param */ AStream);
  
bool /* func */ corebytestreams__canseek
  (/* in */ const bytestream* /* param */ AStream);

// ------------------

bool /* func */ corebytestreams__tryclose
  (/* inout */ bytestream* /* param */ AStream);

bool /* func */ corebytestreams__tryopenreadonly
  (/* inout */ bytestream* /* param */ AStream);

bool /* func */ corebytestreams__tryopenrewrite
  (/* inout */ bytestream* /* param */ AStream);

bool /* func */ corebytestreams__tryopenappend
  (/* inout */ bytestream* /* param */ AStream);

bool /* func */ corebytestreams__tryopenreadwrite
  (/* inout */ bytestream* /* param */ AStream);

// ------------------

bool /* func */ corebytestreams__trygetbyte
  (/* inout */ bytestream* /* param */ ASource,
   /* out */   byte_t*     /* param */ ADestRec);

bool /* func */ corebytestreams__tryputbyte
  (/* inout */ bytestream*   /* param */ ADest,
   /* in */    const byte_t* /* param */ ASourceRec);

bool /* func */ corebytestreams__trymovenext
  (/* inout */ bytestream* /* param */ ADest);

bool /* func */ corebytestreams__tryreadbyte
  (/* inout */ bytestream* /* param */ ASource,
   /* out */   byte_t*     /* param */ ABuffer);

bool /* func */ corebytestreams__trywritebyte
  (/* inout */ bytestream*   /* param */ ADest,
   /* in */    const byte_t* /* param */ ABuffer);

bool /* func */ corebytestreams__tryseekat
  (/* inout */ bytestream*    /* param */ ADest,
   /* in */    const index_t* /* param */ AIndex);

// ------------------

bool /* func */ corebytestreams__tryreadpath
   (/* inout */ bytestream*     /* param */ AStream,
    /* out */   ansinullstring* /* param */ AFullPath);

bool /* func */ corebytestreams__trywritepath
   (/* inout */ bytestream*           /* param */ AStream,
    /* in */    const ansinullstring* /* param */ AFullPath);

void /* func */ corebytestreams__readpath
   (/* inout */ bytestream*     /* param */ AStream,
    /* out */   ansinullstring* /* param */ AFullPath);

void /* func */ corebytestreams__writepath
   (/* inout */ bytestream*           /* param */ AStream,
    /* in */    const ansinullstring* /* param */ AFullPath);

// ------------------

byte_t /* func */ corebytestreams__getbyte
  (/* inout */ bytestream* /* param */ AStream);

void /* func */ corebytestreams__putbyte
  (/* inout */ bytestream*  /* param */ AStream,
   /* in */    const byte_t /* param */ ARecBuffer);

void /* func */ corebytestreams__movenext
  (/* inout */ bytestream* /* param */ AStream);

byte_t /* func */ corebytestreams__readbyte
  (/* inout */ bytestream* /* param */ AStream);

void /* func */ corebytestreams__writebyte
  (/* inout */ bytestream*  /* param */ AStream,
   /* in */    const byte_t /* param */ ARecBuffer);

void /* func */ corebytestreams__seekat
  (/* inout */ bytestream*    /* param */ AStream,
   /* in */    const index_t* /* param */ AIndex);

// ------------------

bool /* func */ corebytestreams__tryreadbytecount
  (/* inout */ bytestream*  /* param */ AStream,
   /* out */   byte_t*      /* param */ ARecBuffer,
   /* in */    const size_t /* param */ ARecSize);

bool /* func */ corebytestreams__trywritebytecount
  (/* inout */ bytestream*   /* param */ AStream,
   /* in */    const byte_t* /* param */ ARecBuffer,
   /* in */    const size_t  /* param */ ARecSize);

void /* func */ corebytestreams__readbytecount
  (/* inout */ bytestream*  /* param */ AStream,
   /* out */   byte_t*      /* param */ ARecBuffer,
   /* in */    const size_t /* param */ ARecSize);

void /* func */ corebytestreams__writebytecount
  (/* inout */ bytestream*   /* param */ AStream,
   /* in */    const byte_t* /* param */ ARecBuffer,
   /* in */    const size_t  /* param */ ARecSize);

// ------------------

/* override */ const ansinullstring* /* func */ coreansidictnodes__modulename
  ( noparams );

/* override */ int /* func */ corebytestreams__setup
  ( noparams );

/* override */ int /* func */ corebytestreams__setoff
  ( noparams );

// ------------------

#endif // COREBYTESTREAMS__H

// } // namespace corebytestreams