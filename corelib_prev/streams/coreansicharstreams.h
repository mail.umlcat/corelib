/** Module: "coreansicharstreams.h"
 ** Descr.: "..."
 **/
 
// namespace coreansicharstreams {
 
// ------------------
 
#ifndef COREANSICHARSTREAMS__H
#define COREANSICHARSTREAMS__H
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"
#include "coreansiospaths.h"
#include "coreansiosfilesys.h"
#include "corestreams.h"

// ------------------

typedef
  stream /* as */ ansicharstream;
   
// ------------------

bool /* func */ coreansicharstreams__isclosed
  (/* in */ const ansicharstream* /* param */ AStream);

// ------------------

bool /* func */ coreansicharstreams__tryclose
  (/* inout */ ansicharstream* /* param */ AStream);

bool /* func */ coreansicharstreams__tryopenreadonly
  (/* inout */ ansicharstream* /* param */ AStream);

bool /* func */ coreansicharstreams__tryopenrewite
  (/* inout */ ansicharstream* /* param */ AStream);

bool /* func */ coreansicharstreams__tryopenappend
  (/* inout */ ansicharstream* /* param */ AStream);

// ------------------

bool /* func */ coreansicharstreams__trygetchar
  (/* inout */ ansicharstream* /* param */ ASource,
   /* out */ ansichar*         /* param */ ABuffer);

bool /* func */ coreansicharstreams__tryputchar
  (/* inout */ ansicharstream* /* param */ ADest,
   /* in */ const ansichar*    /* param */ ABuffer);

bool /* func */ coreansicharstreams__trymovenext
  (/* inout */ ansicharstream* /* param */ ADest);

bool /* func */ coreansicharstreams__tryreadchar
  (/* inout */ ansicharstream* /* param */ ASource,
   /* out */ ansichar*         /* param */ ABuffer);

bool /* func */ coreansicharstreams__trywritechar
  (/* inout */ ansicharstream* /* param */ ADest,
   /* in */ const ansichar*    /* param */ ABuffer);

bool /* func */ coreansicharstreams__tryseek
  (/* inout */ ansicharstream* /* param */ ADest,
   /* in */ const index_t*     /* param */ AIndex);

// ------------------

ansichar /* func */ coreansicharstreams__getchar
  (/* inout */ ansicharstream* /* param */ ASource);

void /* func */ coreansicharstreams__putchar
  (/* inout */ ansicharstream* /* param */ ADest,
   /* in */ const ansichar     /* param */ ABuffer);

void /* func */ coreansicharstreams__movenext
  (/* inout */ ansicharstream* /* param */ ASource);

ansichar /* func */ coreansicharstreams__readchar
  (/* inout */ ansicharstream* /* param */ ASource);

void /* func */ coreansicharstreams__writechar
  (/* inout */ ansicharstream* /* param */ ADest,
   /* in */ const ansichar     /* param */ ABuffer);
   
void /* func */ coreansicharstreams__seek
  (/* inout */ ansicharstream* /* param */ ADest,
   /* in */ const index_t* /* param */ AIndex);

// ------------------

bool /* func */ coreansicharstreams__tryreadstr
  (/* inout */ ansicharstream* /* param */ ASource,
   /* out */ ansinullstring*   /* param */ ABuffer);

bool /* func */ coreansicharstreams__trywritestr
  (/* inout */ ansicharstream*    /* param */ ADest,
   /* in */ const ansinullstring* /* param */ ABuffer);

bool /* func */ coreansicharstreams__tryreadstrcount
  (/* inout */ ansicharstream* /* param */ ASource,
   /* out */ ansinullstring*   /* param */ ADestBuffer,
   /* in */ const size_t       /* param */ ADestSize);

bool /* func */ coreansicharstreams__trywritestrcount
  (/* inout */ ansicharstream*    /* param */ ADest,
   /* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ASourceSize);

// ------------------

void /* func */ coreansicharstreams__readstr
  (/* inout */ ansicharstream* /* param */ ASourceStream,
   /* out */ ansinullstring*   /* param */ ADestBuffer);

void /* func */ coreansicharstreams__writestr
  (/* inout */ ansicharstream*    /* param */ ADestStream,
   /* in */ const ansinullstring* /* param */ ASourceBuffer);
 
void /* func */ coreansicharstreams__readstrcount
  (/* inout */ ansicharstream* /* param */ ASource,
   /* out */ ansinullstring*   /* param */ ADestBuffer,
   /* in */ const size_t       /* param */ ADestSize);

void /* func */ coreansicharstreams__writestrcount
  (/* inout */ ansicharstream*    /* param */ ADest,
   /* in */ const ansinullstring* /* param */ ASourceBuffer,
   /* in */ const size_t          /* param */ ASourceSize);

 // ...
 
 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coreansidictnodes__modulename
  ( noparams );

/* override */ int /* func */ coreansitextstreams__setup
  ( noparams );

/* override */ int /* func */ coreansitextstreams__setoff
  ( noparams );

// ------------------

#endif // COREANSICHARSTREAMS__H

// } // namespace coreansicharstreams