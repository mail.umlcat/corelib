/** Module: "corebytestreams.c"
 ** Descr.: "predefined library"
 **/
 
// namespace corebytestreams {
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"
#include "coreansiospaths.h"
#include "coreansiosfilesys.h"
#include "corestreams.h"

// ------------------
 
#include "corebytestream.h"
 
// ------------------

enum StreamAccessModes /* func */ corebytestreams__getAccessMode
  (/* in */ const bytestream* /* param */ AStream)
{
  enum StreamAccessModes /* var */ Result = streamaccessmodes__Undefined;
  // ---
  
  struct streamheader* /* var */ ThisStream = false;

  if (AStream != NULL)
  {
    ThisStream =
      (struct streamheader*) AStream;
  
    Result = 
      ThisStream->AccessMode;
  } //if
  
  // ---
  return Result;
} // func

enum StreamDataTypes /* func */ corebytestreams__getDataType
  (/* in */ bytestream* /* param */ AStream)
{
  enum StreamDataTypes /* var */ Result = streamdatatypes__Unknown;
  // ---
  
  struct streamheader* /* var */ ThisStream = false;

  if (AStream != NULL)
  {
    ThisStream =
      (struct streamheader*) AStream;
  
    Result = 
      ThisStream->DataType;
  } //if
  
  // ---
  return Result;
} // func

enum StreamOwnerships /* func */ corebytestreams_getOwnership
  (/* in */ bytestream* /* param */ AStream)
{
  enum StreamOwnerships /* var */ Result = streamownerships__Undefined;
  // ---
  
  struct streamheader* /* var */ ThisStream = false;

  if (AStream != NULL)
  {
    ThisStream =
      (struct streamheader*) AStream;
  
    Result = 
      ThisStream->Ownership;
  } //if
  
  // ---
  return Result;
} // func

// ------------------

bool /* func */ corebytestreams__isdone
  (/* in */ const bytestream* /* param */ AStream)
{
  bool /* var */ Result = false;
  // ---
  
  struct streamheader* /* var */ ThisStream = false;
  errorcode_t /* var */ ErrorCode = 0;

  Result =
    (AStream != NULL);
  if (Result)
  {
    Result =
      (corebytestreams__canread(AStream)
    if (Result)
    {
      ThisStream =
        (struct streamheader*) AStream;
    	
      ErrorCode =
       feof(AStream);
       
      Result =
        (ErrorCode != 0);
    }
  } //if
  
  // ---
  return Result;
} // func

bool /* func */ corebytestreams__isclosed
  (/* in */ const bytestream* /* param */ AStream)
{
  bool /* var */ Result = false;
  // ---
  
  struct streamheader* /* var */ ThisStream = false;

  if (AStream != NULL)
  {
    ThisStream =
      (struct streamheader*) AStream;
  
    Result = 
      (ThisStream->AccessMode == streamaccessmodes__Closed) ||
      (ThisStream->InternalFile == NULL);
  } //if
  
  // ---
  return Result;
} // func

bool /* func */ corebytestreams__canread
  (/* in */ const bytestream* /* param */ AStream)
{
  bool /* var */ Result = false;
  // ---
  
  struct streamheader* /* var */ ThisStream = false;

  if (AStream != NULL)
  {
    ThisStream =
      (struct streamheader*) AStream;
  
    Result = 
      (ThisStream->InternalFile != NULL) &&
      ((ThisStream->AccessMode == streamaccessmodes__OpenReadOnly) ||
       (ThisStream->AccessMode == streamaccessmodes__OpenReadWrite));
  } //if
  
  // ---
  return Result;
} // func

bool /* func */ corebytestreams__canwrite
  (/* in */ const bytestream* /* param */ AStream)
{
  bool /* var */ Result = false;
  // ---
  
  struct streamheader* /* var */ ThisStream = false;

  if (AStream != NULL)
  {
    ThisStream =
      (struct streamheader*) AStream;
  
    Result = 
      (ThisStream->InternalFile != NULL) &&
      ((ThisStream->AccessMode == streamaccessmodes__OpenWriteOnly) ||
       (ThisStream->AccessMode == streamaccessmodes__OpenReadWrite));
  } //if
  
  // ---
  return Result;
} // func

bool /* func */ corebytestreams__canseek
  (/* in */ const bytestream* /* param */ AStream)
{
  bool /* var */ Result = false;
  // ---
  
  struct streamheader* /* var */ ThisStream = false;

  if (AStream != NULL)
  {
    ThisStream =
      (struct streamheader*) AStream;
  
    Result = 
      ((ThisStream->InternalFile != NULL) &&
       (ThisStream->AccessMode == streamaccessmodes__OpenReadWrite));
  } //if
  
  // ---
  return Result;
} // func

// ------------------

bool /* func */ corebytestreams__tryclose
  (/* inout */ bytestream* /* param */ AStream)
{
  bool /* var */ Result = false;
  // ---
     
  struct streamheader* /* var */ ThisStream = false;

  Result = (AStream != NULL);
  if (Result);
  {
    ThisStream =
      (struct streamheader*) AStream;
    
    Result =
      (ThisStream->InternalFile != NULL);
    if (Result)
    {
      ThisStream->AccessMode = streamaccessmodes__Closed;
      ThisStream->DataType   = streamdatatypes__Unknown;
      ThisStream->Ownership  = streamownerships__Undefined;
    } 
  } //if

  // ---
  return Result;
} // func

bool /* func */ corebytestreams__tryopenreadonly
  (/* inout */ bytestream* /* param */ AStream)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    corebytestreams__isclosed(AStream);
  if (Result);
  {
    ThisStream =
      (struct streamheader*) AStream;
    
    ThisStream->InternalFile =
      fopen(FullPath, "rb");
    Result =
      (ThisStream->InternalFile != NULL);
    if (Result)
    {
      ThisStream->AccessMode = streamaccessmodes__OpenReadOnly;
      ThisStream->DataType   = streamdatatypes__Binary;
      ThisStream->Ownership  = streamownerships__User;
    } 
  } //if
     
  // ---
  return Result;
} // func

bool /* func */ corebytestreams__tryopenrewrite
  (/* inout */ bytestream* /* param */ AStream)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    corebytestreams__isclosed(AStream);
  if (Result);
  {
    ThisStream =
      (struct streamheader*) AStream;
    
    ThisStream->InternalFile =
      fopen(FullPath, "wb");
    Result =
      (ThisStream->InternalFile != NULL);
    if (Result)
    {
      ThisStream->AccessMode = streamaccessmodes__OpenWriteOnly;
      ThisStream->DataType   = streamdatatypes__Binary;
      ThisStream->Ownership  = streamownerships__User;
    } 
  } //if
 
  // ---
  return Result;
} // func

bool /* func */ corebytestreams__tryopenappend
  (/* inout */ bytestream* /* param */ AStream)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    corebytestreams__isclosed(AStream);
  if (Result);
  {
    ThisStream =
      (struct streamheader*) AStream;
    
    ThisStream->InternalFile =
      fopen(FullPath, "ab");
    Result =
      (ThisStream->InternalFile != NULL);
    if (Result)
    {
      ThisStream->AccessMode = streamaccessmodes__Append;
      ThisStream->DataType   = streamdatatypes__Binary;
      ThisStream->Ownership  = streamownerships__User;
    } 
  } //if
    
  // ---
  return Result;
} // func

bool /* func */ corebytestreams__tryopenreadwrite
  (/* inout */ bytestream* /* param */ AStream)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    corebytestreams__isclosed(AStream);
  if (Result);
  {
    ThisStream =
      (struct streamheader*) AStream;
    
    ThisStream->InternalFile =
      fopen(FullPath, "r+b");
    Result =
      (ThisStream->InternalFile != NULL);
    if (Result)
    {
      ThisStream->AccessMode = streamaccessmodes__Append;
      ThisStream->DataType   = streamdatatypes__Binary;
      ThisStream->Ownership  = streamownerships__User;
    } 
  } //if
  
  // ---
  return Result;
} // func

// ------------------

bool /* func */ corebytestreams__tryreadpath
   (/* inout */ bytestream*     /* param */ AStream,
    /* out */   ansinullstring* /* param */ AFullPath)
{
  bool /* var */ Result = false;
  // ---

  int /* var */ AValue = 0;

  Result =
    ((AStream != NULL) && (AFullPath != NULL));
  if (Result);
  {
    **ARecBuffer = ansinullchar;
    
    Result =
     (corebytestreams__canread(AStream));
    if (Result);
    {
      ThisStream =
        (struct streamheader*) AStream;
    
      AValue =
        fgetc(ThisStream->InternalFile);
    
      Result =
        (AValue != EOF);
      if (Result)
      {
        *ARecBuffer =
          (byte_t) AValue;
      } 
    } //if
  } //if
     
  // ---
  return Result;
} // func

bool /* func */ corebytestreams__trywritepath
   (/* inout */ bytestream*           /* param */ AStream,
    /* in */    const ansinullstring* /* param */ AFullPath)
{
  bool /* var */ Result = false;
  // ---

  int /* var */ AValue = 0;

  Result =
    ((AStream != NULL) && (AFullPath != NULL));
  if (Result);
  {
    *ARecBuffer = NULL;
    
    Result =
     (corebytestreams__canread(AStream));
    if (Result);
    {
      ThisStream =
        (struct streamheader*) AStream;
    
      AValue =
        fgetc(ThisStream->InternalFile);
    
      Result =
        (AValue != EOF);
      if (Result)
      {
        *ARecBuffer =
          (byte_t) AValue;
      } 
    } //if
  } //if
     
  // ---
  return Result;
} // func

void /* func */ corebytestreams__readpath
   (/* inout */ bytestream*     /* param */ AStream,
    /* out */   ansinullstring* /* param */ AFullPath)
{
  /* discard */ corebytestreams__tryreadpath
    (AStream, AFullPath);
} // func

void /* func */ corebytestreams__writepath
   (/* inout */ bytestream*           /* param */ AStream,
    /* in */    const ansinullstring* /* param */ AFullPath)
{
  /* discard */ corebytestreams__trywritepath
    (AStream, AFullPath);
} // func

// ------------------

bool /* func */ corebytestreams__trygetbyte
  (/* inout */ bytestream* /* param */ ASource,
   /* out */   byte_t*     /* param */ ADestRec)
{
  bool /* var */ Result = false;
  // ---

  int /* var */ AValue = 0;

  Result =
    ((AStream != NULL) && (ARecBuffer));
  if (Result);
  {
    *ARecBuffer = NULL;
    
    Result =
     (corebytestreams__canread(AStream));
    if (Result);
    {
      ThisStream =
        (struct streamheader*) AStream;
    
      AValue =
        fgetc(ThisStream->InternalFile);
    
      Result =
        (AValue != EOF);
      if (Result)
      {
        *ARecBuffer =
          (byte_t) AValue;
      } 
    } //if
  } //if
     
  // ---
  return Result;
} // func

bool /* func */ corebytestreams__tryputbyte
  (/* inout */ bytestream*   /* param */ AStream,
   /* in */    const byte_t* /* param */ ARecBuffer)
{
  bool /* var */ Result = false;
  // ---

  struct streamheader* /* var */ ThisStream = 0;
  int                  /* var */ AValue = 0;
  errorcode_t          /* var */ ThisErrorCode = 0;

  Result =
    ((AStream != NULL) && (ARecBuffer));
  if (Result);
  {
    Result =
     (corebytestreams__canwrite(AStream));
    if (Result);
    {
      ThisStream =
        (struct streamheader*) AStream;

      AValue = (int) *ARecBuffer;
      ThisErrorCode =
        fputc(AValue, ThisStream->Internalfile);
     
      Result =
        (ThisErrorCode != 0);
    } //if
  } //if

  // ---
  return Result;
} // func

bool /* func */ corebytestreams__trymovenext
  (/* inout */ bytestream* /* param */ AStream)
{
  bool /* var */ Result = false;
  // ---
     
  Result =
    (AStream != NULL);
  if (Result);
  {
     
     
     
  } //if
  
  // ---
  return Result;
} // func

bool /* func */ corebytestreams__tryreadbyte
  (/* inout */ bytestream* /* param */ AStream,
   /* out */   byte_t*     /* param */ ARecBuffer)
{
  bool /* var */ Result = false;
  // ---

  int /* var */ AValue = 0;

  Result =
    ((AStream != NULL) && (ARecBuffer));
  if (Result);
  {
    *ARecBuffer = NULL;
    
    Result =
     (corebytestreams__canread(AStream));
    if (Result);
    {
      ThisStream =
        (struct streamheader*) AStream;
    
      AValue =
        fgetc(ThisStream->InternalFile);
    
      Result =
        (AValue != EOF);
      if (Result)
      {
        *ARecBuffer =
          (byte_t) AValue;
      } 
    } //if
  } //if
     
  // ---
  return Result;
} // func

bool /* func */ corebytestreams__trywritebyte
  (/* inout */ bytestream*   /* param */ AStream,
   /* in */    const byte_t* /* param */ ARecBuffer)
{
  bool /* var */ Result = false;
  // ---

  struct streamheader* /* var */ ThisStream = 0;
  int                  /* var */ AValue = 0;
  errorcode_t          /* var */ ThisErrorCode = 0;

  Result =
    ((AStream != NULL) && (ARecBuffer));
  if (Result);
  {
    Result =
     (corebytestreams__canwrite(AStream));
    if (Result);
    {
      ThisStream =
        (struct streamheader*) AStream;

      AValue = (int) *ARecBuffer;
      ThisErrorCode =
        fputc(AValue, ThisStream->Internalfile);
     
      Result =
        (ThisErrorCode != 0);
    } //if
  } //if

  // ---
  return Result;
} // func

bool /* func */ corebytestreams__tryseekat
  (/* inout */ bytestream*    /* param */ AStream,
   /* in */    const index_t* /* param */ AIndex)
{
  bool /* var */ Result = false;
  // ---

  errorcode_t /* var */ ThisErrorCode = 0;

  Result =
    corebytestreams__canseek(AStream);
  if (Result)
  {
    ThisErrorCode = fseek
      (AStream, SEEK_SET, AIndex);
    Result =
      (ThisErrorCode != 0);
  } //if
    
  // ---
  return Result;
} // func

// ------------------

byte_t /* func */ corebytestreams__getbyte
  (/* inout */ bytestream* /* param */ AStream)
{
  byte_t /* var */ Result = 0;
  // ---
     
  /* discard */ corebytestreams__trygetbyte
    (AStream, &Result);
  
  // ---
  return Result;
} // func

void /* func */ corebytestreams__putbyte
  (/* inout */ bytestream*  /* param */ AStream,
   /* in */    const byte_t /* param */ ARecBuffer)
{
  /* discard */ corebytestreams__tryputbyte
    (AStream, ARecBuffer);
} // func

void /* func */ corebytestreams__movenext
  (/* inout */ bytestream* /* param */ AStream)
{
  /* discard */ corebytestreams__trymovenext
    (AStream);
} // func

byte_t /* func */ corebytestreams__readbyte
  (/* inout */ bytestream* /* param */ AStream)
{
  byte_t /* var */ Result = 0;
  // ---
     
  /* discard */ corebytestreams__tryreadbyte
    (AStream, &Result);
     
  // ---
  return Result;
} // func

void /* func */ corebytestreams__writebyte
  (/* inout */ bytestream*  /* param */ AStream,
   /* in */    const byte_t /* param */ ARecBuffer)
{
  /* discard */ corebytestreams__trywritebyte
    (AStream, &ARecBuffer);
} // func

void /* func */ corebytestreams__seekat
  (/* inout */ bytestream*   /* param */ AStream,
   /* in */    const index_t /* param */ AIndex)
{
  /* discard */ corebytestreams__tryseekat
    (AStream, &AIndex);
} // func

// ------------------

bool /* func */ corebytestreams__tryreadbytecount
  (/* inout */ bytestream*   /* param */ AStream,
   /* out */   byte_t*       /* param */ ARecBuffer,
   /* in */    const count_t /* param */ ARecCount)
{
  bool /* var */ Result = false;
  // ---

  count_t     /* var */ ReadCount = 0;
  struct streamheader* /* var */ ThisStream = 0;

  Result =
    corebytestreams__canread(AStream);
  if (Result)
  {
    ThisStream =
     (struct streamheader*) AStream;
    ReadCount = (size_t)
      fread((void*) ARecBuffer, 1, ThisStream->RecSize);
  
    Result =
      (ReadCount == ThisStream->RecSize);
  } //if
    
  // ---
  return Result;
} // func

bool /* func */ corebytestreams__trywritebytecount
  (/* inout */ bytestream*   /* param */ AStream,
   /* in */    const byte_t* /* param */ ARecBuffer,
   /* in */    const size_t  /* param */ ARecSize)
{
  bool /* var */ Result = false;
  // ---
     
  count_t     /* var */ WrittenCount = 0;
  struct streamheader* /* var */ ThisStream = 0;

  Result =
    corebytestreams__canwrite(AStream);
  if (Result)
  {
    ThisStream =
     (struct streamheader*) AStream;
    WrittenCount = (size_t)
      fwrite((void*) ARecBuffer, 1, ThisStream->RecSize);
  
    Result =
      (WrittenCount == ThisStream->RecSize);
  } //if

  // ---
  return Result;
} // func

void /* func */ corebytestreams__readbytecount
  (/* inout */ bytestream*  /* param */ AStream,
   /* out */   byte_t*      /* param */ ARecBuffer,
   /* in */    const size_t /* param */ ARecSize)
{
  /* discard */ corebytestreams__tryreadbytecount
    (AStream, ARecBuffer, ARecSize);
} // func

void /* func */ corebytestreams__writebytecount
  (/* inout */ bytestream*   /* param */ ADest,
   /* in */    const byte_t* /* param */ ASourceBuffer,
   /* in */    const size_t  /* param */ ASourceSize)
{
  /* discard */ corebytestreams__trywritebytecount
    (AStream, ARecBuffer, ARecSize);
} // func

// ------------------

/* override */ const ansinullstring* /* func */ coreansidictnodes__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "corebytestreams";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ corebytestreams__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corebytestreams__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace corebytestreams