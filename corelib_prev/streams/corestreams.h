/** Module: "corestreams.h"
 ** Descr.: "Input and Output base library."
 **/

// namespace corestreams {
 
// ------------------
 
#ifndef CORESTREAMS__H
#define CORESTREAMS__H
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"
#include "coreansiospaths.h"
#include "coreansiosfilesys.h"
#include "coreintegers.h"

// ------------------

#define corestreams__EOF EOF

#define corestreams__eof EOF

// ------------------

enum StreamAccessModes
{
   streamaccessmodes__Undefined,
   streamaccessmodes__Closed,
   streamaccessmodes__OpenReadOnly,
   streamaccessmodes__OpenWriteOnly,
   streamaccessmodes__Append,
   streamaccessmodes__OpenReadWrite,
} ;
 
enum StreamDataTypes
{
   streamdatatypes__Unknown,
   streamdatatypes__Binary,
   streamdatatypes__Text,
} ;

enum StreamOwnerships
{
   streamownerships__Undefined,
   streamownerships__System,
   streamownerships__User,
   streamownerships__Temporal,
} ;

struct streamheader
{
   FILE*                  /* var */ InternalFile;
   
   enum StreamAccessModes /* var */ AccessMode;
   enum StreamDataTypes   /* var */ DataType;
   enum StreamOwnerships  /* var */ Ownership;

   //byte_t           /* var */ BufferPtr*;
   //size_t           /* var */ BufferCount;

   size_t           /* var */ RecSize;
   size_t           /* var */ RecCount;

   index_t          /* var */ RecIndex;

   ansichar         /* var */ FullPath[1024];
   //ansichar         /* var */ Name[256];
   //ansichar         /* var */ Ext[16];

   // ....
 } ;

typedef
  struct streamheader  /* as */ stream_t;
typedef
  struct streamheader* /* as */ stream_p;

typedef
  stream_t      /* as */ corestreams__stream_t;
typedef
  stream_p      /* as */ corestreams__stream_p;

// ------------------

bool /* func */ corestreams__isclosed
  (/* in */ const stream* /* param */ AStream);

enum StreamAccessModes /* func */ corestreams__getAccessMode
  (/* in  */ stream* /* param */ AStream);

enum StreamDataTypes /* func */ corestreams__getDataType
  (/* in */ stream* /* param */ AStream);

enum StreamOwnerships /* func */ corestreams_getOwnership
  (/* in */ stream* /* param */ AStream);

// ------------------

void /* func */ corestreams__assignpath
   (/* inout */ stream*               /* param */ AStream,
    /* in */    const ansinullstring* /* param */ AFullPath);

// ------------------

bool /* func */ corestreams__tryclose
  (/* inout */ stream* /* param */ AStream);

 void /* func */ corestreams__close
   (/* inout */ stream* /* param */ AStream);

// ------------------

/* override */ const ansinullstring* /* func */ coreansidictnodes__modulename
  ( noparams );

/* override */ int /* func */ corestreams__setup
  ( noparams );

/* override */ int /* func */ corestreams__setoff
  ( noparams );

// ------------------


 // ...
 
// ------------------
 
#endif // CORESTREAMS__H

// } // namespace corestreams