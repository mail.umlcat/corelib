/** Module: "coreansitextstreams.h"
 ** Descr.: "Predefined (ANSI) Text files library."
 **/
 
// namespace coreansitextstreams {
 
// ------------------
 
#ifndef COREANSITEXTSTREAMS__H
#define COREANSITEXTSTREAMS__H
 
// ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"
#include "coreansiospaths.h"
#include "coreansiosfilesys.h"
#include "corestreams.h"

// ------------------

typedef
  stream /* as */ ansitextstream;

// ------------------

bool /* func */ coreansitextstreams__tryisclosed
  (/* inout */ ansitextstream* /* param */ ASource,
   /* out */   bool*           /* param */ AResult);

bool /* func */ coreansitextstreams__isclosed
  (/* in */ const ansitextstream* /* param */ AStream);

bool /* func */ coreansitextstreams__trygetpos
  (/* inout */ ansitextstream* /* param */ ASource,
   /* out */   index_t*        /* param */ AResult);

index_t /* func */ coreansitextstreams__getpos
  (/* in */ const ansitextstream* /* param */ AStream);

// ------------------

bool /* func */ coreansitextstreams__tryclose
  (/* inout */ ansitextstream* /* param */ AStream);

bool /* func */ coreansitextstreams__tryopenreadonly
  (/* inout */ ansitextstream* /* param */ AStream);

bool /* func */ coreansitextstreams__tryopenrewrite
  (/* inout */ ansitextstream* /* param */ AStream);

bool /* func */ coreansitextstreams__tryopenappend
  (/* inout */ ansitextstream* /* param */ AStream);

// ------------------

bool /* func */ coreansitextstreams__trygetchar
  (/* inout */ ansitextstream* /* param */ ASource,
   /* out */   ansichar*       /* param */ ABuffer);

bool /* func */ coreansitextstreams__tryputchar
  (/* inout */ ansitextstream* /* param */ ADest,
   /* in */    const ansichar* /* param */ ABuffer);

bool /* func */ coreansitextstreams__tryreadchar
  (/* inout */ ansitextstream* /* param */ ASource,
   /* out */   ansichar*       /* param */ ABuffer);

bool /* func */ coreansitextstreams__trywritechar
  (/* inout */ ansitextstream* /* param */ ADest,
   /* in */    const ansichar* /* param */ ABuffer);

bool /* func */ coreansitextstreams__trymovenext
  (/* inout */ ansitextstream* /* param */ ADest);

// ------------------

ansichar /* func */ coreansitextstreams__getchar
  (/* inout */ ansitextstream* /* param */ ASource);

void /* func */ coreansitextstreams__putchar
  (/* inout */ ansitextstream* /* param */ ADest,
   /* in */    const ansichar  /* param */ ABuffer);

ansichar /* func */ coreansitextstreams__readchar
  (/* inout */ ansitextstream* /* param */ ASource);

void /* func */ coreansitextstreams__writechar
  (/* inout */ ansitextstream* /* param */ ADest,
   /* in */    const ansichar  /* param */ ABuffer);

void /* func */ coreansitextstreams__movenext
  (/* inout */ ansitextstream* /* param */ ASource);

// ------------------

bool /* func */ coreansitextstreams__tryreadstr
  (/* inout */ ansitextstream* /* param */ ASource,
   /* out */   ansinullstring* /* param */ ABuffer);

bool /* func */ coreansitextstreams__trywritestr
  (/* inout */ ansitextstream*       /* param */ ADest,
   /* in */    const ansinullstring* /* param */ ABuffer);

bool /* func */ coreansitextstreams__trywriteeoln
  (/* inout */ ansitextstream* /* param */ ADestStream);

bool /* func */ coreansitextstreams__tryreadstrcount
  (/* inout */ ansitextstream* /* param */ ASource,
   /* out */   ansinullstring* /* param */ ADestBuffer,
   /* in */    const size_t    /* param */ ADestSize);

bool /* func */ coreansitextstreams__trywritestrcount
  (/* inout */ ansitextstream*       /* param */ ADest,
   /* in */    const ansinullstring* /* param */ ASourceBuffer,
   /* in */    const size_t          /* param */ ASourceSize);
   
// ------------------

void /* func */ coreansitextstreams__readstr
  (/* inout */ ansitextstream* /* param */ ASourceStream,
   /* out */   ansinullstring* /* param */ ADestBuffer);

void /* func */ coreansitextstreams__writestr
  (/* inout */ ansitextstream*       /* param */ ADestStream,
   /* in */    const ansinullstring* /* param */ ASourceBuffer);

void /* func */ coreansitextstreams__writeeoln
  (/* inout */ ansitextstream* /* param */ ADestStream);


 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coreansidictnodes__modulename
  ( noparams );

/* override */ int /* func */ coreansitextstreams__setup
  ( noparams );

/* override */ int /* func */ coreansitextstreams__setoff
  ( noparams );

// ------------------

#endif // COREANSITEXTSTREAMS__H

// } // namespace coreansitextstreams