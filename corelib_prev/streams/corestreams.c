/** Module: "corestreams.c"
 ** Descr.: "Input and Output base library."
 **/

// namespace corestreams {
 
 // ------------------
 
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreansichars.h"
#include "coreansinullstrs.h"
#include "coreansiospaths.h"
#include "coreansiosfilesys.h"

// ------------------
 
#include "corestreams.h"
 
// ------------------

bool /* func */ corestreams__isclosed
  (/* in */ const stream* /* param */ AStream)
{
  stream* /* var */ Result = NULL;
  // ---
   
  if (AStream != NULL)
  {
  	
  } // if
     
  // ---
  return Result;
} // func

enum StreamAccessModes /* func */ corestreams__getAccessMode
  (/* in  */ stream* /* param */ AStream)
{
  enum StreamAccessModes /* var */ Result = streamaccessmodes__Undefined;
  // ---
   
  if (AStream != NULL)
  {
  	
  } // if
     
  // ---
  return Result;
} // func

enum StreamDataTypes /* func */ corestreams__getDataType
  (/* in */ stream* /* param */ AStream)
{
  enum StreamDataTypes /* var */ Result = streamdatatypes__Unknown;
  // ---
   
  if (AStream != NULL)
  {
  	
  } // if
     
  // ---
  return Result;
} // func

enum StreamOwnerships /* func */ corestreams_getOwnership
  (/* in */ stream* /* param */ AStream)
{
  enum StreamOwnerships /* var */ Result = streamownerships__Undefined;
  // ---
   
  if (AStream != NULL)
  {
  	
  } // if
     
  // ---
  return Result;
} // func

// ------------------

void /* func */ corestreams__assignpath
  (/* inout */ stream* /* param */ AStream,
   /* in */    const ansinullstring* /* path */ AFullPath)
{
  stream* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func
  
// ------------------

bool /* func */ corestreams__tryclose
  (/* inout */ stream* /* param */ AStream)
{
  bool /* var */ Result = NULL;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

void /* func */ corestreams__close
  (/* inout */ stream* /* param */ AStream)
{
  corestreams__tryclose(AStream);
} // func

// ------------------

/* override */ const ansinullstring* /* func */ coreansidictnodes__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "corestreams";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ corestreams__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corestreams__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------


 // ...

// } // namespace corestreams