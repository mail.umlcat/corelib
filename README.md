# corelib

The "corelib" projects is an Open Source "C" library,
aimed to help developers to built cross-platform applications.

It started in 1994, as a school project. Have received, several ideas,
from PHP and Delphi (Free Pascal), code migrations.

It was lost, due to a hard drive crash, but rebuilt from scratch.

The library project had to be restarted, to be used for a cross-platform application,
due to existing libraries, doesn't met the requirements, of the project.

This library emphazises design over efficency, efficency comes automatically,
identifiers used are longer than usual, and some operations, are duplicated,
and lower memory-efficient than the existing libraries.

Design Example:

The "coreansinullstrs.h" library replaces the "string.h" standard library,
with the "coreansinullstrs.h" library, which internally uses "string.h" .

And "wraps" several standard functions, with a more defined identifer,
the "strcmp" in "string.h", is replaced by the "coreansinullstrs__compare" function,
from the "coreansinullstrs.h" library.

This feature may seem unefficient, for some developers,
but useful, for others.

Also, adds special comments, to allow the source code to be easier to read,
as "/* function */" between the return type,
and the identifier of a function declaration, similar to Javascript or Delphi.

Code Example:

`/** Module: "mapdemo01.c"`
` ** Descr.: "Map using integers demo."`
` **/`
``
`// ------------------`
``
`#include <stdlib.h>`
`#include <stddef.h>`
`#include <setjmp.h>`
`#include <string.h>`
`#include <limits.h>`
`#include <stdbool.h>`
`#include <stdint.h>`
`#include <inttypes.h>`
`#include <stdio.h>`
`#include <math.h>`
``
`// ------------------`
``
`#include "coresystem.h"`
`#include "coresysmem.h"`
`#include "corefunctors.h"`
`#include "coreuuids.h"`
`#include "coreoswarnings.h"`
``
`// ------------------`
``
`#include "corecollkeyvalues.h"`
`#include "coremapnodes.h"`
`#include "corecollmaps.h"`
``
`// ------------------`
``
`// namespace mapdemo01 {`
` `
`// ------------------`
``
`EXTERN map*  /* var */ AMap;`
``
`coresys__uint16_t     /* var */ Keys[5];`
`coresys__uint16_t     /* var */ OldValues[5];`
`coresys__uint16_t     /* var */ NewValues[5];`
` `
`// ------------------`
``
`bool /* func */ mapdemo01__areequal`
`  (coresys__pointer* /* param */ A, `
`   coresys__pointer* /* param */ B)`
`{`
`  bool /* var */ Result = false;`
`  // ---`
`     `
`  coresys__uint16_t* /* var */ C;`
`  coresys__uint16_t* /* var */ D;`
``
`  // ---`
``
`  C =`
`    (coresys__uint16_t*) A;`
`  D =`
`    (coresys__uint16_t*) B;`
``
`  Result =`
`    (C == D);`
``
`  // ---`
`  return Result;`
`} // func`
``
`void /* func */ mapdemo01__start`
`  ( noparams )`
`{`
`  AMap =`
`    corecollmaps__createbysize`
`      (sizeof(coresys__uint16_t), sizeof(coresys__uint16_t));`
`  AMap->MatchFunc =`
`    &mapdemo01__areequal;`
``
`  // ---`
``
`  // values where added in reverse order,`
`  // purpousely`
``
`  Keys[0] = 2010;`
`  Keys[1] = 2000;`
`  Keys[2] = 1990;`
`  Keys[3] = 1980;`
`  Keys[4] = 1970;`
``
`  // ---`
``
`  // values where added in reverse order,`
`  // purpousely`
``
`  OldValues[0] = 1;`
`  OldValues[1] = 2;`
`  OldValues[2] = 3;`
`  OldValues[3] = 4;`
`  OldValues[4] = 5;`
``
`  // ---`
``
`  // values where added in reverse order,`
`  // purpousely`
``
`  NewValues[0] = 90;`
`  NewValues[1] = 80;`
`  NewValues[2] = 70;`
`  NewValues[3] = 60;`
`  NewValues[4] = 50;`
`} // func`
``
`void /* func */ mapdemo01__finish`
`  ( noparams )`
`{`
`  // remove values references`
``
`  for (coresys__int /* var */ i; i < 4; i++)`
`  {`
`    NewValues[i] = 0;`
`  } // for`
``
`  // ---`
``
`  // remove values references`
``
`  for (coresys__int /* var */ i; i < 4; i++)`
`  {`
`    OldValues[i] = 0;`
`  } // for`
``
`  // ---`
``
`  // remove keys references`
``
`  for (coresys__int /* var */ i; i < 4; i++)`
`  {`
`    Keys[i] = 0;`
`  } // for`
``
`  // ---`
``
`  corecollmaps__drop(&AMap);`
`} // func`
``
`void /* func */ mapdemo01__show`
`  ( noparams )`
`{`
`  coresys__pointer* /* var */ AKey;`
`  coresys__pointer* /* var */ AValue;`
``
`  coresys__ansichar /* var */ AStrKey[32];`
`  coresys__ansichar /* var */ AStrValue[32];`
``
`  mapiterator* /* var */ AIterator;`
``
`  // ---`
`  puts("Prcoresys__int coresys__uint16_t Map\n\n");`
``
`  AIterator =`
`    corecollmaps__createiteratorforward`
`      (AMap);`
``
`  while (! corecollmaps__isdone(AMap))`
`  {`
`    corecollmaps__getkeycopy(&AKey);`
`    corecollmaps__getvaluecopy(&AValue);`
``
`    AStrKey =`
`      itoa(*AKey);`
`    AStrValue =`
`      itoa(*AValue);`
``
`    puts("Key: [\"");`
`    puts(AStrKey);`
`    puts("\"]\n");`
``
`    puts("Value: [\"");`
`    puts(AStrValue);`
`    puts("\"]\n\n");`
``
`    corecollmaps__movenext(AIterator);`
`  } // while`
``
`  corecollmaps__dropiterator`
`    (&AIterator);`
``
`  puts("\n\n");`
`} // func`
``
`void /* func */ mapdemo01__before`
`  ( noparams )`
`{`
`  coresys__pointer* /* var */ AKey;`
`  coresys__pointer* /* var */ AValue;`
``
`  coresys__pointer* /* var */ AOriginalKey;`
`  coresys__pointer* /* var */ AOriginalValue;`
``
`  // ---`
``
`  // insert items`
``
`  for (coresys__int /* var */ i; i < 4; i++)`
`  {`
`    AOriginalKey = (coresys__pointer*)`
`      &(Keys[i]);`
`    AOriginalValue = (coresys__pointer*)`
`      &(OldValues[i]);`
``
`    AKey = (coresys__pointer*)`
`      coresysmem__duplicatecopy`
`        (AOriginalKey, sizeof(coresys__uint16_t));`
`    AValue = (coresys__pointer*)`
`      coresysmem__duplicatecopy`
`        (AOriginalValue, sizeof(coresys__uint16_t));`
``
`    corecollmaps__insert`
`      (AMap, AKey, AValue);`
`  } // for`
``
`  // ---`
``
`  mapdemo01__show();`
`} // func`
``
`void /* func */ mapdemo01__after`
`  ( noparams )`
`{`
`  corecollkeyvalues__keyvalue* /* var */ AKeyValue;`
``
`  coresys__pointer*  /* var */ AOriginalKey;`
``
`  coresys__pointer*  /* var */ ANewValue;`
`  coresys__pointer*  /* var */ AOldValue;`
``
`  // ---`
``
`  for (coresys__int /* var */ i; i < 4; i++)`
`  {`
`    ANewValue =`
`      coresysmem__duplicatecopy`
`        (NewValues[i], sizeof(coresys__uint16_t));`
``
`    AOriginalKey =`
`      &(Keys[i]);`
``
`    corecollmaps__updateextract`
`      (AMap, AOriginalKey, ANewValue, &AOldValue);`
``
`    coresysmem__deallocate`
`      (&AOldValue, sizeof(coresys__uint16_t));`
`  } // for`
``
`  // ---`
``
`  mapdemo01__show();`
`} // func`
``
`void /* func */ mapdemo01__run`
`  ( noparams )`
`{`
`  mapdemo01__before();`
`  mapdemo01__after();`
`} // func`
``
`/* override */ coresys__int /* func */ main`
`  ( noparams )`
`{`
`  coresys__int /* var */ Result = 0;`
`  // ---`
`     `
`  mapdemo01__start();`
`  mapdemo01__run();`
`  mapdemo01__finish();`
``
`  // ---`
`  return Result;`
`} // func`
``
`// ------------------`
``
`// } // namespace mapdemo01`

Features

* The Open Source License is LGPL 2.1, allowed to be used both,
in open source and commercial programs.

* All, non inclusion, source code files, start with the lowercase "core" prefix,
indicating the main library project.

* The main feature is to add a quick modular, namespace replacement.

* Each, non inclusion, source code file, is considered a namespace.

* All, non inclusion, source code files, start with the lowercase "core" prefix,
followed by with a lowercase identifier.

* "Class and Object Oriented Programming" is not used,
altought can be emulated with several programing techniques.

This library it's implemented in procedural "C", not Object and Class Oriented "C++",
altought a "C++" compiler, may be used.

* All functions start with the same lowercase identifier, as the filename,
followed by two consecutive underscores.

Those two consecutive underscores, should be treated as the "::" scope,
character of "C++" namespaces.

Example:

The "corestreams" file has functions like "corestreams__openonlyreadfile",
and "corestreams__closefile". They should be read as "corestreams::openonlyreadfile",
and "corestreams::closefile".

* Other identifiers, for variables, functions, types,
may vary wheter use the "core" prefix, or wheter should use lowercase,
uppercase, or mixed case. That's open to other developers to decide.

Is it possible to have an identifier,
with the lowercase "core*__" prefix, followed by a mixed case text.

Many commonly used, simple types, avoid using the "core*__" prefix,
to avoid making the sourcecode too complicated.

* The multiple usages of the keyword "void" are removed. as macro "noparams" is defined,
and used instead of "void", for functions that doesn't have any parameters. 

The macro or typedef "pointer", borrowed from "pascal", it's used instead of "void*".
There fore "void" is only used to indicate a function does not have a return type.

* Each library has a "*__setup" loader function, and "*__setoff" unloader function,
that must be called explicitly. In some cases these methods are empty.

* Several libraries are "wrappers" to existing, sometimes, standard libraries,
and its respective functions.

The "coreansinullstrs.h" library replaces the "string.h" standard library,
and "wraps" several standard functions, with a more defined identifer,
the "strcmp" in "string.h", is replaced by the "coreansinullstrs__compare" function,
from the "coreansinullstrs.h" library.

A developer may use both "string.h" and "coreansinullstrs.h" libraries,
in its application.

* Some libraries are intended to avoid confusion, function overloading,
(several functions with different parameters and same identifier), are avoided,
as possible.

* In functions, with pointers, there are cases wheter the pointer addresses a structure.

In some cases, an operation requires to modify an existing structure,
in others, a new copy to be generated.

For example, in order to get a lowercase-only version of a string,
two functions are provided:

"coreansinullstrs__lowercasecopy(...)" for a new generated copy,
that must be deallocated from memory, by the programmer,
and "coreansinullstrs__lowercasechange(...)",
that may use a local character array, that is disposed by a function, automtically.

* Other features in process.





