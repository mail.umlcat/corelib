/** Module: "oswarningdemo.c"
 ** Descr.: "executable demo."
 **/

// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

// namespace oswarningdemo {
 
// ------------------

#DEFINE UUIDPtrExpected "5cda5b6f-c072-47c0-8937-2b864249feb7"
#DEFINE UUIDivByZero    "32b4ec0b-a101-467a-9819-ade09cb87452"

// ------------------

oswarning* /* func */ intdiv
  (int  /* var */ A,
   int  /* var */ B,
   int* /* var */ ADest,
   )
{
  oswarning* /* var */ Result = NULL;
  // ---
  
  if (ADest != NULL)
  {
  	
  } else
  {
  	
  }
  
  // ---
  return Result;
} // func

/* override */ int /* func */ main
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
  
  int /* var */ A = 123;
  int /* var */ B = 0;
  int /* var */ C = 0;

  oswarning* /* var */ AWarning = NULL;

  printf("intdiv & oswarning demo\n");
  printf("A:[%d], B:[%d], ", &A, &B);
  
  AWarning =
    safediv(A, B, &C);

  if (AWarning == NULL)
  {
    printf("A / B:[%d], ", &C);
  }
  else
  {
  	
  }
  
  // ---
  return Result;
} // func

// ------------------

// } // namespace oswarningdemo