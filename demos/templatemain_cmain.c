/** Module: "<name>.c"
 ** Descr.: "executable demo."
 **/

// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <integers.h>
#include <float.h>
#include <time.h>
#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include <coresystem.h>
#include <corememory.h>
#include <corefunctors.h>
#include <coreuuids.h>
#include <coreoswarnings.h>

// ------------------

// namespace <name> {
 
// ------------------

/* override */ int /* func */ main
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func


// ------------------

// } // namespace <name>