/** Module: "uuiddemo.c"
 ** Descr.: "UUID demo."
 **/

// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
//#include "coreoswarnings.h"

// ------------------

#include "coretxtshared.h"
#include "coretxtuuids.h"

// ------------------

// namespace uuiddemo {
 
// ------------------

/* override */ int /* func */ main
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func


// ------------------

// } // namespace uuiddemo