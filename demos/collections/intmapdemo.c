/** Module: "intmapdemo.c"
 ** Descr.: "executable demo."
 **/

// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <integers.h"
#include <float.h"
#include <time.h"
#include <chrono.h"
#include <stdio.h"
#include <math.h"

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreintegers.h"
#include "coreuint8s.h"
#include "coresint8s.h"

// ------------------

#include "coremapnodes.h"
#include "coremaps.h"

// ------------------

// namespace intmapdemo {
 
// ------------------

/* override */ int /* func */ main
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  map* /* var */ AMap =
    coremaps__createmapbysize
      (sizeof(uint8_t), sizeof(sint8_t));
    coremaps__assignequalityfunc
      (AMap, coreuint8s__areequalbyptr);
    
    coremaps__insert
      (coreuint8s__uint8toptrcopy(10), coresint8s__sint8toptrcopy(-99));
    coremaps__insert
      (coreuint8s__uint8toptrcopy(20), coresint8s__sint8toptrcopy(-98));
    coremaps__insert
      (coreuint8s__uint8toptrcopy(30), coresint8s__sint8toptrcopy(-97));
    coremaps__insert
      (coreuint8s__uint8toptrcopy(40), coresint8s__sint8toptrcopy(-96));
    coremaps__insert
      (coreuint8s__uint8toptrcopy(50), coresint8s__sint8toptrcopy(-95));
    coremaps__insert
      (coreuint8s__uint8toptrcopy(60), coresint8s__sint8toptrcopy(-94));
    coremaps__insert
      (coreuint8s__uint8toptrcopy(70), coresint8s__sint8toptrcopy(-93));

    mapiterator*       /* var */ AIterator = NULL;
    mapiteratorheader* /* var */ ThisIterator = NULL;
    
    uint8_t*           /* var */ AKey = NULL;
    sint8_t*           /* var */ AValue = NULL;
    
    AIterator =
      coremaps__createforwarditerator(AMap);
    ThisIterator =
      (mapiteratorheader*) AIterator;

    while (!coremaps__isdone(AIterator))
    {
      AMapNode =
        ThisIterator->CurrentItem;

      AKey   = coremapnodes__getkey(AMapNode);
      AValue = coremapnodes__getvalue(AMapNode);

      printf("{Key: %d, Value:%d}\n", *AKey, *AValue);
      printf("\n");

      coremaps__movenext(AIterator);
    } // while

    coremaps__dropiterator(&AMap);

  coremaps__dropmap(&AMap);

  // ---
  return Result;
} // func


// ------------------

// } // namespace intmapdemo