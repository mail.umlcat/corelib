/** Module: "intqueuedemo.c"
 ** Descr.: "executable demo."
 **/

// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <integers.h"
#include <float.h"
#include <time.h"
#include <chrono.h"
#include <stdio.h"
#include <math.h"

// ------------------

#include "coresystem.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreintegers.h"
#include "coreuint32s.h"
#include "coresint32s.h"

// ------------------

#include "corequeuenodes.h"
#include "corequeues.h"

// ------------------

// namespace intqueuedemo {
 
// ------------------

/* override */ int /* func */ main
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  queue* /* var */ AQueue =
    corequeues__createqueuebysize
      (sizeof(uint32_t);
    corequeues__assignequalityfunc
      (AQueue, coreuint32s__areequalbyptr);
    
    corequeues__insert
      (coreuint32s__uint32toptrcopy(10));
    corequeues__insert
      (coreuint32s__uint32toptrcopy(20));
    corequeues__insert
      (coreuint32s__uint32toptrcopy(30));
    corequeues__insert
      (coreuint32s__uint32toptrcopy(40));
    corequeues__insert
      (coreuint32s__uint32toptrcopy(50));
    corequeues__insert
      (coreuint32s__uint32toptrcopy(60));
    corequeues__insert
      (coreuint32s__uint32toptrcopy(70));

    queueiterator*       /* var */ AIterator = NULL;
    queueiteratorheader* /* var */ ThisIterator = NULL;
    
    uint32_t*            /* var */ AItem = NULL;
    
    AIterator =
      corequeues__createiterator(AQueue);
    ThisIterator =
      (queueiteratorheader*) AIterator;

    while (!corequeues__isdone(AIterator))
    {
      AQueueNode =
        ThisIterator->CurrentItem;

      AItem = corequeuenodes__getkey(AQueueNode);

      printf("{Item: %d}\n", *AItem);
      printf("\n");

      corequeues__movenext(AIterator);
    } // while

    corequeues__dropiterator(&AQueue);

    corequeues__remove(AQueue);
    corequeues__remove(AQueue);
    corequeues__remove(AQueue);

    AIterator =
      corequeues__createiterator(AQueue);
    ThisIterator =
      (queueiteratorheader*) AIterator;

    while (!corequeues__isdone(AIterator))
    {
      AQueueNode =
        ThisIterator->CurrentItem;

      AItem = corequeuenodes__getkey(AQueueNode);

      printf("{Item: %d}\n", *AItem);
      printf("\n");

      corequeues__movenext(AIterator);
    } // while

    corequeues__dropiterator(&AQueue);

  corequeues__dropqueue(&AQueue);

  // ---
  return Result;
} // func


// ------------------

// } // namespace intqueuedemo