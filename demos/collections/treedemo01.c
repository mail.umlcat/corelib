/** Module: "treedemo01.c"
 ** Descr.: "executable demo."
 **/

// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <float.h"
#include <time.h"
//#include <chrono.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"
#include "corearraytreenodes.h"
#include "corearraytrees.h"
 
// ------------------

#include "coreintegers.h"
#include "coreuint48s.h"
#include "coresint48s.h"

// ------------------

// namespace treedemo01 {
 
// ------------------

/* override */ int /* func */ main
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
  
  arraytreenode* /* var */ ATree =
    corearraytrees__createtreebysize
      (sizeof(uint48_t);
    corearraytrees__assignequalityfunc
      (ATree, coreuint48s__areequalbyptr);
     
  coresystem__nothing();
  
  corearraytrees__dropdlinklist(&ATree);

  // ---
  return Result;
} // func


// ------------------

// } // namespace treedemo01