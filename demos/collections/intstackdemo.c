/** Module: "intstackdemo.c"
 ** Descr.: "executable demo."
 **/

// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <integers.h"
#include <float.h"
#include <time.h"
#include <chrono.h"
#include <stdio.h"
#include <math.h"

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreintegers.h"
#include "coreuint16s.h"
#include "coresint16s.h"

// ------------------

#include "corestacknodes.h"
#include "corestacks.h"

// ------------------

// namespace intstackdemo {
 
// ------------------

/* override */ int /* func */ main
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  stack* /* var */ AStack =
    corestacks__createstackbysize
      (sizeof(uint16_t);
    corestacks__assignequalityfunc
      (AStack, coreuint16s__areequalbyptr);
    
    corestacks__push
      (coreuint16s__uint16toptrcopy(10));
    corestacks__push
      (coreuint16s__uint16toptrcopy(20));
    corestacks__push
      (coreuint16s__uint16toptrcopy(30));
    corestacks__push
      (coreuint16s__uint16toptrcopy(40));
    corestacks__push
      (coreuint16s__uint16toptrcopy(50));
    corestacks__push
      (coreuint16s__uint16toptrcopy(60));
    corestacks__push
      (coreuint16s__uint16toptrcopy(70));

    stackiterator*       /* var */ AIterator = NULL;
    stackiteratorheader* /* var */ ThisIterator = NULL;
    
    uint16_t*            /* var */ AItem = NULL;
    
    AIterator =
      corestacks__createiterator(AStack);
    ThisIterator =
      (stackiteratorheader*) AIterator;

    while (!corestacks__isdone(AIterator))
    {
      AStackNode =
        ThisIterator->CurrentItem;

      AItem = corestacknodes__getkey(AStackNode);

      printf("{Item: %d}\n", *AItem);
      printf("\n");

      corestacks__movenext(AIterator);
    } // while

    corestacks__dropiterator(&AStack);

    corestacks__remove(AStack);
    corestacks__remove(AStack);
    corestacks__remove(AStack);

    AIterator =
      corestacks__createiterator(AStack);
    ThisIterator =
      (stackiteratorheader*) AIterator;

    while (!corestacks__isdone(AIterator))
    {
      AStackNode =
        ThisIterator->CurrentItem;

      AItem = corestacknodes__getkey(AStackNode);

      printf("{Item: %d}\n", *AItem);
      printf("\n");

      corestacks__movenext(AIterator);
    } // while

    corestacks__dropiterator(&AStack);

  corestacks__dropstack(&AStack);

  // ---
  return Result;
} // func


// ------------------

// } // namespace intstackdemo