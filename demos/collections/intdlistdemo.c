/** Module: "intdlinklistdemo.c"
 ** Descr.: "executable demo."
 **/

// ------------------

#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <integers.h"
#include <float.h"
#include <time.h"
#include <chrono.h"
#include <stdio.h"
#include <math.h"

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreintegers.h"
#include "coreuint64s.h"
#include "coresint64s.h"

// ------------------

#include "coredlinklistnodes.h"
#include "coredlinklists.h"

// ------------------

// namespace intdlinklistdemo {
 
// ------------------

/* override */ int /* func */ main
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  dlinklist* /* var */ AQueue =
    coredlinklists__createdlinklistbysize
      (sizeof(uint64_t);
    coredlinklists__assignequalityfunc
      (AQueue, coreuint64s__areequalbyptr);
    
    coredlinklists__insertfirst
      (coreuint64s__uint64toptrcopy(10));
    coredlinklists__insertfirst
      (coreuint64s__uint64toptrcopy(20));
    coredlinklists__insertfirst
      (coreuint64s__uint64toptrcopy(30));
    coredlinklists__insertlast
      (coreuint64s__uint64toptrcopy(40));
    coredlinklists__insertlast
      (coreuint64s__uint64toptrcopy(50));
    coredlinklists__insertlast
      (coreuint64s__uint64toptrcopy(60));
    coredlinklists__insertlast
      (coreuint64s__uint64toptrcopy(70));

    dlinklistiterator*       /* var */ AIterator = NULL;
    dlinklistiteratorheader* /* var */ ThisIterator = NULL;
    
    uint64_t*            /* var */ AItem = NULL;
    
    AIterator =
      coredlinklists__createiterator(AQueue);
    ThisIterator =
      (dlinklistiteratorheader*) AIterator;

    while (!coredlinklists__isdone(AIterator))
    {
      AQueueNode =
        ThisIterator->CurrentItem;

      AItem = coredlinklistnodes__getkey(AQueueNode);

      printf("{Item: %d}\n", *AItem);
      printf("\n");

      coredlinklists__movenext(AIterator);
    } // while

    coredlinklists__dropiterator(&AQueue);

    coredlinklists__removefirst(AQueue);
    coredlinklists__removelast(AQueue);
    coredlinklists__removefirst(AQueue);

    AIterator =
      coredlinklists__createiterator(AQueue);
    ThisIterator =
      (dlinklistiteratorheader*) AIterator;

    while (!coredlinklists__isdone(AIterator))
    {
      AQueueNode =
        ThisIterator->CurrentItem;

      AItem = coredlinklistnodes__getkey(AQueueNode);

      printf("{Item: %d}\n", *AItem);
      printf("\n");

      coredlinklists__movenext(AIterator);
    } // while

    coredlinklists__dropiterator(&AQueue);

  coredlinklists__dropdlinklist(&AQueue);

  // ---
  return Result;
} // func


// ------------------

// } // namespace intdlinklistdemo