/** Module: "cformat_demo_01.c"
 ** Descr.: "'cformat' library demo."
 **/

// ------------------
 
#include <stdlib.h"
#include <stddef.h"
#include <setjmp.h"
#include <string.h"
#include <limits.h"
#include <stdbool.h"
#include <stdint.h"
#include <inttypes.h"
#include <stdio.h"
#include <math.h"
 
// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

#include "coreiterators.h"
#include "corevarargs.h"
#include "corecformats.h"

#include "coreansichars.h"
#include "coreansinullstrs.h"
#include "coreuint32s.h"
#include "corebooleans.h"
#include "corefloats.h"
#include "coredatetimes.h"

// ------------------

// namespace cformat_demo_01 {

// ------------------

varargs* /* func */ getvarargs
  ( noparams )
{
  varargs* /* var */ Result = NULL;
  // ---

  // "varargs" alike
  // ("John Doe", 35, TRUE, 1.90);
  Result =
    corevarargs__create(4);
  
  corevarargs__add
    (Result, coreansinullstrs__compactcopy("John Doe"));
  corevarargs__add
    (Result, coreuint32s__uint32ptrcopy(35));
  corevarargs__add
    (Result, corebooleans__boolptrcopy(TRUE));
  corevarargs__add
    (Result, corefloats__sfptrcopy(1.90));

  // ---
  return Result;
} // func

/* override */ int /* func */ main
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
  
  // "varargs" alike
  // ("John Doe", 35, TRUE, 1.90);
  varargs*   /* var */ Params =
    getvarargs();
  
  ansichar     /* var */ TemplateMessage[255];
  coreansinullstrs__assign
    (TemplateMessage,
       "Name: %s, Age:%d, Married:%?, Height: %f");
  
  ansichar     /* var */ FormatedMsg[512];
  
  formatlist*  /* var */ FormatedList = NULL;
  
  corecformats__mergesize
    (FormatedMsg, sizeof(FormatedMsg), Params, FormatedList);
    
  // ...
  
  corevarargs__drop(&Params);
  
  // ---
  return Result;
} // func


// ------------------

// } // namespace cformat_demo_01