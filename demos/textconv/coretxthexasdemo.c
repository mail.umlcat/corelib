/** Module: "txthexasdemo.c"
 ** Descr.: "executable demo."
 **/

// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
//#include "coreuuids.h"
//#include "coreoswarnings.h"

// ------------------

// namespace coretxthexasdemo {
 
// ------------------

void /* func */ hexchartodecbytebyptrdemo
  ( noparams )
{
  bool     /* var */ WasOk = false;
  ansichar /* var */ AChar = '.';
  byte     /* var */ AByte = 0;

  printf("hexchartodecbytedemo\n\n");

  AChar = 'F';
  WasOk =
    coretxthexas__tryhexchartodecbyte
     (&AChar, &AByte);
  if (WasOk)
  {
    printf("Char:[%c], Byte:[%d]\n\n");
  }
 
  AChar = '?';
  WasOk =
    coretxthexas__tryhexchartodecbyte
     (&AChar, &AByte);
  if (!WasOk)
  {
    printf("Char:[%c], Byte:[<error>]\n\n");
  }
    
  getchar();
} // func

void /* func */ hexstrtodecbytebyptrdemo
  ( noparams )
{
  bool     /* var */ WasOk = false;
  byte     /* var */ AByte = 0;
  ansichar /* var */ AStr[2 + 1];

  printf("hexstrtodecbytedemo\n\n");

  AChar = 'F5';
  WasOk =
    coretxthexas__tryhexstrtodecbyte
     (&AStr, &AByte);
  if (WasOk)
  {
    printf("Text:[%c], Byte:[%d]\n\n");
  }
 
  AChar = '?';
  WasOk =
    coretxthexas__tryhexstrtodecbyte
     (&AStr, &AByte);
  if (!WasOk)
  {
    printf("Text:[%c], Byte:[<error>]\n\n");
  }
    
  getchar();
} // func

/* override */ int /* func */ main
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  hexchartodecbytebyptrdemo();
  
  hexstrtodecbytebyptrdemo();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coretxthexasdemo