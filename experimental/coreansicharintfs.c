/** Module: "coreansicharintfs.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coreansicharintfs {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
//#include "coreuuids.h"
//#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------
 
#include "coreansicharintfs.h"
 
// ------------------

struct ansicharintf* /* func */ coreansicharintfs__createdefault
  ( noparams )
{
  struct ansicharintf* /* var */ Result = NULL;
  // ---
     
  Result =
    corememory__clearallocate
      (sizeof(struct ansicharintf));
      
  Result->chartolower = coreansichars__chartolower;
  Result->chartoupper = coreansichars__chartoupper;

  Result->chartodec = coreansichars__chartodec
  Result->dectochar = coreansichars__dectochar;
  
  Result->islowercase = coreansichars__islowercase;
  Result->isuppercase = coreansichars__isuppercase;
  
  Result->isspace = coreansichars__isspace;
  Result->isblank = coreansichars__isblank;
  
  Result->isdigit = coreansichars__isdigit;
  Result->isalpha = coreansichars__isalpha;
  Result->isalphanum = coreansichars__isalphanum;
  
  Result->ispunct = coreansichars__ispunct;
  Result->iscontrol = coreansichars__iscontrol;
  Result->isgraph = coreansichars__isgraph;
  
  // ---
  return Result;
} // func

struct ansicharintf* /* func */ coreansicharintfs__createschema
  ( noparams )
{
  struct ansicharintf* /* var */ Result = NULL;
  // ---
     
  Result =
    corememory__clearallocate
      (sizeof(struct ansicharintf));
      
  // ---
  return Result;
} // func

void /* func */ coreansicharintfs__dropdeafault
 (/* inout */ struct ansicharintf** /* param */ ADest)
{
  if ((ADest != NULL) && (*ADest != NULL))
  {
    corememory__cleardeallocate
      (ADest, sizeof(struct ansicharintf));
  }  // if
} // func

void /* func */ coreansicharintfs__dropschema
 (/* inout */ struct ansicharintf** /* param */ ADest)
{
  if ((ADest != NULL) && (*ADest != NULL))
  {
    corememory__cleardeallocate
      (ADest, sizeof(struct ansicharintf));
  }  // if
} // func

// ------------------

 // ...

// ------------------

/* override */ const ansinullstring* /* func */ coreansicharintfs__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "coreansicharintfs";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coreansicharintfs__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coreansicharintfs__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coreansicharintfs