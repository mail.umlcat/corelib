/** Module: "coretypes.h"
 ** Descr.: "Data Dictionary for Types,"
 **         "Reflection Libray."
 **/

// namespace coretypes {
 
// ------------------
 
#ifndef CORETYPES__H
#define CORETYPES__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

// identifier for types, not pointer
// "typeid" is already taken
typedef
  typecode   /* as */ coretypes__typecode;

typedef
  typecode_t /* as */ coretypes__typecode_t;
typedef
  typecode_p /* as */ coretypes__typecode_p;

// ------------------

struct typeheader
{
  typecode*   /* var */ TypeID;

  modulecode* /* var */ ModuleID;

  typecode*   /* var */ ParentTypeID;

  bool        /* var */ IsCategory;

  // "ansinullstring*", by now
  pointer*    /* var */ TypeName;
 
  size_t      /* var */ TypeSize;

  // ...

  bool        /* var */ HasDefaultValue;

  isdefaultvaluefunctor       /* var */ IsDefaultValueFunc;
  tryapplydefaultvaluefunctor /* var */ TryApplyDefaultValueFunc;

  // ...

  // are two items equal
  areequalfunctor    /* var */ AreEqualFunc;
  // make a copy of an item
  duplicatefunctor   /* var */ DuplicateFunc;
  // return the default text representation
  trybintotxtbyptrcountfunctor /* var */ DuplicateFunc;
  // return the binary item from the default representation
  trytxttobinbyptrcountfunctor /* var */ DuplicateFunc;

  // ...

} ;

// ------------------

/* friend */ bool /* func */ coretypes__samesize
  (/* in */ const typecode* /* param */ A,
  (/* in */ const typecode* /* param */ B);

/* friend */ bool /* func */ coretypes__sametype
  (/* in */ const typecode* /* param */ A,
  (/* in */ const typecode* /* param */ B);
  
/* friend */ const pointer* /* func */ coretypes__typetometadata
  (/* in */ const typecode* /* param */ ATypeID);

/* friend */ bool /* func */ coretypes__seektype
  (/* in */ const typecode* /* param */ ATypeID);

// ------------------

pointer* /* func */ coretypes__new
  (/* in */ const typecode* /* param */ ATypeID);

pointer* /* func */ coretypes__newarray
  (/* in */ const typecode* /* param */ ATypeID,
   /* in */ const count_t   /* param */ AItemCount);

pointer* /* func */ coretypes__clearnew
  (/* in */ const typecode* /* param */ ATypeID);

pointer* /* func */ coretypes__clearnewarray
  (/* in */ const typecode* /* param */ ATypeID,
   /* in */ const count_t   /* param */ AItemCount);

// ------------------

void /* func */ coretypes__drop
  (/* in */ const pointer** /* param */ ADest,
   /* in */ const typecode* /* param */ ATypeID);

void /* func */ coretypes__droparray
  (/* inout */ pointer**       /* param */ ADest,
   /* in */    const typecode* /* param */ ATypeID,
   /* in */    const count_t   /* param */ AItemCount);

void /* func */ coretypes__cleardrop
  (/* in */ const pointer** /* param */ ADest,
   /* in */ const typecode* /* param */ ATypeID);

void /* func */ coretypes__droparray
  (/* inout */ pointer**       /* param */ ADest,
   /* in */    const typecode* /* param */ ATypeID,
   /* in */    const count_t   /* param */ AItemCount);

// ------------------

pointer* /* func */ coretypes__registertype
  (/* in */ const typecode*   /* param */ ATypeID,
   /* in */ const modulecode* /* param */ AModuleID,
   /* in */ const typecode*   /* param */ AParentTypeID,
   /* in */ const bool        /* param */ AIsCategory,

   // "ansinullstring*", by now
   /* in */ const pointer*    /* param */ ATypeName,
   /* in */ const size_t      /* param */ ATypeSize);
 
pointer* /* func */ coretypes__registerdefaultvalue
  (/* in */ const typecode*             /* param */ ATypeID,
   /* in */ isdefaultvaluefunctor       /* param */ AIsDefaultFunc,
   /* in */ tryapplydefaultvaluefunctor /* param */ ATryApplyDefaultFunc);

// ------------------

size_t /* func */ coretypes__typetosize
  (/* in */ const typecode* /* param */ ATypeID);

const pointer* /* func */ coretypes__typetotext
  (/* in */ const typecode* /* param */ ATypeID);

void /* func */ coretypes__moduleoftype
  (/* in */  const typecode* /* param */ ATypeID,
   /* out */ modulecode**    /* param */ AModuleCode);
 
// ------------------

bool /* func */ coretypes__hasdefaultvalue
  (/* in */ const typecode* /* param */ ATypeI);



// ------------------



 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coretypes__modulename
  ( noparams );

/* override */ int /* func */ coretypes__setup
  ( noparams );

/* override */ int /* func */ coretypes__setoff
  ( noparams );

// ------------------

#endif // CORETYPES__H

// } // namespace coretypes