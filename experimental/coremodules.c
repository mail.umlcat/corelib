/** Module: "coremodules.c"
 ** Descr.: "Data Dictionary for Modules,"
 **         "Reflection Libray."
 **/

// namespace coremodules {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------
 
#include "coremodules.h"
 
// ------------------

modulecode* /* func */ coremodules__registermodule
  (/* in */ modulecode* /* param */ AParentModuleID,
   /* in */ pointer*    /* param */ AModuleName)
{
  modulecode* /* var */ Result = NULL;
  // ---
  
  bool /* var */ CanContinue = false;
  
  CanContinue =
    (AParentModuleID != NULL) &&
    (AModuleName != NULL);

  if (CanContinue)
  {
    moduleheader* /* var */ ThisModule = NULL;
  
    ThisModule =
      corememory__clearallocate
        (sizeof(moduleheader));

    // ...

    ThisModule->ParentModuleID = AParentModuleID;

    // ...

 

  
    // ...

    Result =
      corememory__transfer
        (&(ThisType));

  } //if

  // ---
  return Result;
} // func

 
 // ...

// ------------------

/* override */ const ansinullstring* /* func */ coremodules__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "coremodules";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coremodules__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coremodules__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coremodules