/** Module: "coremodules.h"
 ** Descr.: "Data Dictionary for Modules,"
 **         "Reflection Libray."
 **/

// namespace coremodules {
 
// ------------------
 
#ifndef COREMODULES__H
#define COREMODULES__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

// identifier for modules, not pointer
typedef
  modulecode   /* as */ coremodules__modulecode;

typedef
  modulecode_t /* as */ coremodules__module_t;
typedef
  modulecode_p /* as */ coremodules__module_p;

// ------------------

struct moduleheader
{
  modulecode* /* var */ ModuleID;
  modulecode* /* var */ ParentModuleID;

  // "ansinullstring*", by now
  pointer*    /* var */ ModuleName;

  // ...




  // ...

} ;

// ------------------

modulecode* /* func */ coremodules__registermodule
  (/* in */ modulecode* /* param */ AParentModuleID,
   /* in */ pointer*    /* param */ AModuleName);




 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ coremodules__modulename
  ( noparams );

/* override */ int /* func */ coremodules__setup
  ( noparams );

/* override */ int /* func */ coremodules__setoff
  ( noparams );

// ------------------

#endif // COREMODULES__H

// } // namespace coremodules