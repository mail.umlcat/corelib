/** Module: "coreansicharptrs.h"
 ** Descr.: "..."
 **/
 
// namespace coreansicharptrs {
 
// ------------------
 
#ifndef COREANSICHARPTRS__H
#define COREANSICHARPTRS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

typedef
  ansichar /* functor */ (*chartocharfunctor) /* as */
    (/* in */ const ansichar /* param */ AValue);
 
// ------------------

struct ansicharintf
{
  chartocharfunctor /* var */ chartolower;
  chartocharfunctor /* var */ chartoupper;
} ;

// ------------------

//pointer* /* func */ coreansicharptrs__malloc
//  (/* in */ size_t /* param */ ASize);
  
//void /* func */ coreansicharptrs__dealloc
//  (/* out */ pointer** /* param */ ADest,
//   /* in */  size_t    /* param */ ASize);

 // ...

// ------------------

/* override */ const ansinullstring* /* func */ coreansicharptrs__modulename
  ( noparams );

/* override */ int /* func */ coreansicharptrs__setup
  ( noparams );

/* override */ int /* func */ coreansicharptrs__setoff
  ( noparams );

// ------------------

#endif // COREANSICHARPTRS__H

// } // namespace coreansicharptrs