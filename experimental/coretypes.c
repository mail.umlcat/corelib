/** Module: "coretypes.c"
 ** Descr.: "Data Dictionary for Types,"
 **         "Reflection Libray."
 **/

// namespace coretypes {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------
 
#include "coretypes.h"
 
// ------------------

/* friend */ bool /* func */ coretypes__samesize
  (/* in */ const typecode* /* param */ A,
   /* in */ const typecode* /* param */ B);
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

/* friend */ bool /* func */ coretypes__sametype
  (/* in */ const typecode* /* param */ A,
   /* in */ const typecode* /* param */ B)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func
  
/* friend */ const pointer* /* func */ coretypes__typetometadata
  (/* in */ const typecode* /* param */ ATypeID)
{
  pointer* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

/* friend */ bool /* func */ coretypes__seektype
  (/* in */ const typecode* /* param */ ATypeID)
{
  bool /* var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

pointer* /* func */ coretypes__new
  (/* in */ const typecode* /* param */ ATypeID)
{
  pointer* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

pointer* /* func */ coretypes__newarray
  (/* in */ const typecode* /* param */ ATypeID,
   /* in */ const count_t   /* param */ AItemCount)
{
  pointer* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

pointer* /* func */ coretypes__clearnew
  (/* in */ const typecode* /* param */ ATypeID)
{
  pointer* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

pointer* /* func */ coretypes__clearnewarray
  (/* in */ const typecode* /* param */ ATypeID,
   /* in */ const count_t   /* param */ AItemCount)
{
  pointer* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

void /* func */ coretypes__drop
  (/* in */ const pointer** /* param */ ADest,
   /* in */ const typecode* /* param */ ATypeID)
{
  coresystem__nothing();
} // func

void /* func */ coretypes__droparray
  (/* inout */ pointer**       /* param */ ADest,
   /* in */    const typecode* /* param */ ATypeID,
   /* in */    const count_t   /* param */ AItemCount)
{
  coresystem__nothing();
} // func

void /* func */ coretypes__cleardrop
  (/* in */ const pointer** /* param */ ADest,
   /* in */ const typecode* /* param */ ATypeID)
{
  coresystem__nothing();
} // func

void /* func */ coretypes__droparray
  (/* inout */ pointer**       /* param */ ADest,
   /* in */    const typecode* /* param */ ATypeID,
   /* in */    const count_t   /* param */ AItemCount)
{
  coresystem__nothing();
} // func

// ------------------

pointer* /* func */ coretypes__registertype
  (/* in */ const typecode*   /* param */ ATypeID,
   /* in */ const modulecode* /* param */ AModuleID,
   /* in */ const typecode*   /* param */ AParentTypeID,
   /* in */ const bool        /* param */ AIsCategory,

   // "ansinullstring*", by now
   /* in */ const pointer*    /* param */ ATypeName,
   /* in */ const size_t      /* param */ ATypeSize)
{
  pointer* /* var */ Result = NULL;
  // ---
  
  bool /* var */ CanContinue = false;
  
  CanContinue =
    (ATypeID != NULL) &&
    (AModuleID != NULL) &&
    (AParentTypeID != NULL) &&
    (ATypeName != NULL) &&
    (ATypeSize > 0);

  if (CanContinue)
  {
    typeheader* /* var */ ThisType = NULL;
  
    ThisType =
      corememory__clearallocate
        (sizeof(typeheader));

    // ...

    ThisType->TypeID = ATypeID;
    ThisType->ModuleID = AModuleID;
    ThisType->ParentTypeID = AParentTypeID;
    ThisType->TypeName = ATypeName;
    ThisType->TypeSize = ATypeSize;

    // ...

    ThisType->HasDefaultValue = false;

    ThisType->IsDefaultValueFunc = NULL;
    ThisType->TryApplyDefaultValueFunc = NULL;
   
    // ...

    Result =
      corememory__transfer
        (&(ThisType));

  } //if

  // ---
  return Result;
} // func

pointer* /* func */ coretypes__registerdefaultvalue
  (/* in */ const typecode*             /* param */ ATypeID,
   /* in */ isdefaultvaluefunctor       /* param */ AIsDefaultFunc,
   /* in */ tryapplydefaultvaluefunctor /* param */ ATryApplyDefaultFunc)
{
  pointer* /* var */ Result = NULL;
  // ---
  
  bool /* var */ CanContinue = false;
  
  CanContinue =
    (ATypeID != NULL) &&
    (AIsDefaultFunc != NULL) &&
    (ATryApplyDefaultFunc != NULL);

  if (CanContinue)
  {
    typeheader* /* var */ ThisType = NULL;

    ThisType = 
      coretypes__seektype(ATypeID);
    CanContinue =
      (ThisType != NULL);
     if (CanContinue)
     {
       ThisType->HasDefaultValue = false;
       ThisType->IsDefaultValueFunc =
         AIsDefaultFunc;
       ThisType->TryApplyDefaultValueFunc =
         ATryApplyDefaultFunc;
     } //if
  } //if

  // ---
  return Result;
} // func

// ------------------

size_t /* func */ coretypes__typetosize
  (/* in */ const typecode* /* param */ ATypeID)
{
  size_t /* var */ Result = 0;
  // ---
     
     
  // ---
  return Result;
} // func

const pointer* /* func */ coretypes__typetotext
  (/* in */ const typecode* /* param */ ATypeID)
{
  pointer* /* var */ Result = NULL;
  // ---
     
     
  // ---
  return Result;
} // func

void /* func */ coretypes__moduleoftype
  (/* in */  const typecode* /* param */ ATypeID,
   /* out */ modulecode**    /* param */ AModuleCode)
{
  coresystem__nothing();
} // func

// ------------------



// ------------------

 // ...

// ------------------

/* override */ const ansinullstring* /* func */ coretypes__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer =
    "coretypes";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ coretypes__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coretypes__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coretypes