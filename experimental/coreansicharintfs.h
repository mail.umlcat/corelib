/** Module: "coreansicharintfs.h"
 ** Descr.: "..."
 **/
 
// namespace coreansicharintfs {
 
// ------------------
 
#ifndef COREANSICHARINTFS__H
#define COREANSICHARINTFS__H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
//#include "coreuuids.h"
//#include "coreoswarnings.h"

// ------------------

#include "coreiterators.h"

// ------------------

typedef
  ansichar /* functor */ (*chartocharfunctor) /* as */
    (/* in */ const ansichar /* param */ AValue);

  ansichar /* functor */ (*chartocharfunctor) /* as */
    (/* in */ const ansichar /* param */ AValue);

typedef
  byte_t /* functor */ (*chartobyte) /* as */
    (/* in */ const ansichar /* param */ AValue);
  
typedef
  ansichar /* functor */ (*bytetochar) /* as */
    (/* in */ const byte_t /* param */ AValue);

typedef
  bool /* functor */ (*isfunctor) /* as */
    (/* in */ const ansichar /* param */ AValue);


// ------------------

struct ansicharintf
{
  chartocharfunctor /* var */ chartolower;
  chartocharfunctor /* var */ chartoupper;

  chartobytefunctor /* var */ chartodec;
  bytetocharfunctor /* var */ dectochar;
  
  isfunctor /* var */ islowercase;
  isfunctor /* var */ isuppercase;
  
  isfunctor /* var */ isspace;
  isfunctor /* var */ isblank;
  
  isfunctor /* var */ isdigit;
  isfunctor /* var */ isalpha;
  isfunctor /* var */ isalphanum;
  
  isfunctor /* var */ ispunct;
  isfunctor /* var */ iscontrol;
  isfunctor /* var */ isgraph;
  
} ;

// ------------------

struct ansicharintf* /* func */ coreansicharintfs__createdefault
  ( noparams );

struct ansicharintf* /* func */ coreansicharintfs__createschema
  ( noparams );

void /* func */ coreansicharintfs__dropdeafault
 (/* inout */ struct ansicharintf** /* param */ ADest);

void /* func */ coreansicharintfs__dropschema
 (/* inout */ struct ansicharintf** /* param */ ADest);

// ------------------

/* override */ const ansinullstring* /* func */ coreansicharintfs__modulename
  ( noparams );

/* override */ int /* func */ coreansicharintfs__setup
  ( noparams );

/* override */ int /* func */ coreansicharintfs__setoff
  ( noparams );

// ------------------

#endif // COREANSICHARINTFS__H

// } // namespace coreansicharintfs