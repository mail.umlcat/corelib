#include <stdio.h>
#include <stdlib.h>

// ---

#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"
#include "C:\softdev\corelib\corelib\src\core\sys\conv\coresysconv.h"

// ------------------

/* override */ int /* func */ demo01__start
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysbase__start();
  coresysmemints__start();
  coresyschars__start();
  coresysstrs__start();
  coresysconv__start();

  // ---
  return Result;
} // func

/* override */ int /* func */ demo01__finish
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysconv__finish();
  coresysstrs__finish();
  coresyschars__finish();
  coresysmemints__finish();
  coresysbase__finish();

  // ---
  return Result;
} // func

/* override */ int /* func */ demo01__run
  ( noparams )
{
  int /* var */ Result = 0;
  // ---

  printf("core::sys::uints::uint8byptrsdemo01\n\n", NULL);

  pointer* /* var */ P = NULL;
  pointer* /* var */ Q = NULL;
  
  int /* var */ A = 0;
  int /* var */ B = 0;
  
  // ---

  P =
    coresysuintsuint8byptrs__consttoptr(5);
  Q =
    coresysuintsuint8byptrs__consttoptr(7);
  
  // compiler automatic casting
  A = *((uint8_t*) P);
  B = *((uint8_t*) Q);
  
  printf("P: [%i]\n", A);
  printf("Q: [%i]\n", B);
  printf("\n", NULL);
    
  if (coresysuintsuint8byptrs__different((const pointer*) P, (const pointer*) Q))
  {
    printf("Are different.\n", NULL);
  }
  else
  {
    printf("Are Equal.\n", NULL);
  };
    
  coresysuintsuint8byptrs__dropptr
    (&Q);
  coresysuintsuint8byptrs__dropptr
    (&P);

  // ---

  //printf("Press [RETURN] to continue ...\n\n", NULL);
  system("pause");

  // ---
  return Result;
} // func

// ------------------

int main(int argc, char *argv[])
{
  int /* var */ Result = 0;
  // ---
  
  demo01__start();
    Result =
      demo01__run();
  demo01__finish();

  // ---
  return Result;
}