#include <stdio.h>
#include <stdlib.h>

// ---

#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"
#include "C:\softdev\corelib\corelib\src\core\sys\io\coresysio.h"

// ------------------

/* override */ int /* func */ demo01__start
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysbase__start();

  // ---
  return Result;
} // func

/* override */ int /* func */ demo01__finish
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysbase__finish();

  // ---
  return Result;
} // func

/* override */ int /* func */ demo01__run
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
  
  ansichar /* var */ BufferData[256];
  // ---
 
  bool /* var */ CanContinue = false;
  // ---
 
  coresysiofiles__textfile* /* var */ AFile;
 
  size_t /* var */ l;
  
  AFile =
    coresysioansitxtfiles__openwriteonly
      ("C:\\softdev\\corelib\\corelib\\demos\\core\\sys\\coresysioansitextfilesdemo01\\test.txt");
  
  strcpy(BufferData, "Spring");
  l = strlen(BufferData);
  for (int i = 0; i < l; i++)
  {
    coresysioansitxtfiles__putchar
      (AFile, BufferData[i]);
  } // for
  coresysioansitxtfiles__putnewline
    (AFile);
  
  strcpy(BufferData, "Summer");
  l = strlen(BufferData);
  for (int i = 0; i < l; i++)
  {
    coresysioansitxtfiles__putchar
      (AFile, BufferData[i]);
  } // for
  coresysioansitxtfiles__putnewline
    (AFile);
  
  strcpy(BufferData, "Fall");
  l = strlen(BufferData);
  for (int i = 0; i < l; i++)
  {
    coresysioansitxtfiles__putchar
      (AFile, BufferData[i]);
  } // for
  coresysioansitxtfiles__putnewline
    (AFile);
  
  strcpy(BufferData, "Winter");
  l = strlen(BufferData);
  for (int i = 0; i < l; i++)
  {
    coresysioansitxtfiles__putchar
      (AFile, BufferData[i]);
  } // for
  coresysioansitxtfiles__putnewline
    (AFile);   
    
  coresysioansitxtfiles__close
    (AFile);
    
  // ---
  
  AFile =
    coresysioansitxtfiles__openreadonly
      ("C:\\softdev\\corelib\\corelib\\demos\\core\\sys\\coresysioansitextfilesdemo01\\test.txt");

  ansichar /* var */ AChar;
  // ---

  while (coresysioansitxtfiles__trygetchar(AFile, &AChar))
  {
      printf("%c", AChar);
  } // while
    
  coresysioansitxtfiles__close
    (AFile);
    
  // ---

  printf("Press [RETURN] to continue ...\n\n", NULL);
  system("pause");

  // ---
  return Result;
} // func

// ------------------

int main(int argc, char *argv[])
{
  int /* var */ Result = 0;
  // ---
  
  demo01__start();
  Result =
    demo01__run();
  demo01__finish();

  // ---
  return Result;
}
