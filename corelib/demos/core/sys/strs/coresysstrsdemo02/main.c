#include <stdio.h>
#include <stdlib.h>

// ---

#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"
#include "C:\softdev\corelib\corelib\src\core\sys\io\coresysio.h"

// ------------------

/* override */ int /* func */ demo01__start
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysbase__start();

  // ---
  return Result;
} // func

/* override */ int /* func */ demo01__finish
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysbase__finish();

  // ---
  return Result;
} // func

/* override */ int /* func */ demo01__run
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  ansichar /* var */ SourceBuffer[512];
  ansichar /* var */ DestBuffer[512];
     
  ansichar* /* var */ SourcePtr;
  ansichar* /* var */ DestPtr;
     
  size_t /* var */ SourceSize;
  size_t /* var */ DestSize;
  
  // ---
   
  SourcePtr =
    /* & */ SourceBuffer;
  DestPtr =
    /* & */ DestBuffer;
    
  SourceSize =
    sizeof(SourceBuffer);
  DestSize =
    sizeof(DestBuffer);
    
  coresysstrsansinullstrs__clearsize
    (SourcePtr, SourceSize);
    
  coresysstrsansinullstrs__assign
    (SourcePtr, "whitehouse");
    
  // ---
  
  coresysioansitxtfiles__stdputstr
    ("SourceBuffer: ");
  coresysioansitxtfiles__stdputchar
    ('[');
  coresysioansitxtfiles__stdputstrsize
    (SourcePtr, SourceSize);
  coresysioansitxtfiles__stdputchar
    (']');
  coresysioansitxtfiles__stdputnewline();
  coresysioansitxtfiles__stdputnewline();
    
  // ---
   
  coresysstrsansinullstrs__clearsize
    (DestPtr, DestSize);
  coresysstrsansinullstrs__assignleftsize
    (DestPtr, DestSize, SourcePtr, SourceSize, 5);
    
  // ---
   
  coresysioansitxtfiles__stdputstr
    ("DestBuffer: ");
  coresysioansitxtfiles__stdputchar
    ('[');
  coresysioansitxtfiles__stdputstrsize
    (DestPtr, DestSize);
  coresysioansitxtfiles__stdputchar
    (']');
  coresysioansitxtfiles__stdputnewline();
  coresysioansitxtfiles__stdputnewline();
    
  // ---
   
  coresysstrsansinullstrs__clearsize
    (DestPtr, DestSize);
  coresysstrsansinullstrs__assignrightsize
    (DestPtr, DestSize, SourcePtr, SourceSize, 5);
    
  // ---
   
  coresysioansitxtfiles__stdputstr
    ("DestBuffer: ");
  coresysioansitxtfiles__stdputchar
    ('[');
  coresysioansitxtfiles__stdputstrsize
    (DestPtr, DestSize);
  coresysioansitxtfiles__stdputchar
    (']');
  coresysioansitxtfiles__stdputnewline();
  coresysioansitxtfiles__stdputnewline();
    
  // ---
   
  printf("Press [RETURN] to continue ...\n\n", NULL);
  system("pause");

  // ---
  return Result;
} // func

// ------------------

int main(int argc, char *argv[])
{
  int /* var */ Result = 0;
  // ---
  
  demo01__start();
  Result =
    demo01__run();
  demo01__finish();

  // ---
  return Result;
}