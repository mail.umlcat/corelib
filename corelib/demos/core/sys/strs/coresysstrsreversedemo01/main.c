#include <stdio.h>
#include <stdlib.h>

// ---

#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

/* override */ int /* func */ demo01__start
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysbase__start();
  coresysmemints__start();
  coresyschars__start();
  coresysstrs__start();

  // ---
  return Result;
} // func

/* override */ int /* func */ demo01__finish
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysstrs__finish();
  coresyschars__finish();
  coresysmemints__finish();
  coresysbase__finish();

  // ---
  return Result;
} // func

/* override */ int /* func */ demo01__run
  ( noparams )
{
  int /* var */ Result = 0;
  // ---

  ansichar /* var */ SourceFileName[512];
  ansichar /* var */ DestFileName[512];
    
  struct ansicharsizedptr* /* var */ FatS = NULL;
  struct ansicharsizedptr* /* var */ FatD = NULL;
   
  bool /* var */ Found = false;
    
  // ---

  coresysstrsansinullstrs__clearsize
    (SourceFileName, sizeof(SourceFileName));
  coresysstrsansinullstrs__clearsize
    (DestFileName, sizeof(DestFileName));
    
  // ---

  coresysstrsansinullstrs__assign
    (SourceFileName, "C:\\softdev\\corelib\\corelib\\apps\\semicolon2under02\\data\\coresysstrsansinullstrs.xc");
    
  // ---

  FatS =
    coresysstrsansicharfatptrs__new();
  FatD =
    coresysstrsansicharfatptrs__new();
    
  // ---

  coresysstrsansicharfatptrs__pack
    (&FatS, SourceFileName, sizeof(SourceFileName));
  coresysstrsansicharfatptrs__pack
    (&FatD, DestFileName, sizeof(DestFileName));
    
  // ---

  Found =
    coresysstrsansicharfatptrs__tryreverse
     (&FatD, FatS); 
  if (Found)
  {
	printf("D: [%s] \n\n", /* & */ DestFileName);
  } // if (Found)

  // ---

  coresysstrsansicharfatptrs__drop(&FatD);
  coresysstrsansicharfatptrs__drop(&FatS);

  // ---

  //printf("Press [RETURN] to continue ...\n\n", NULL);
  system("pause");

  // ---
  return Result;
} // func

// ------------------

int main(int argc, char *argv[])
{
  int /* var */ Result = 0;
  // ---
  
  demo01__start();
    Result =
      demo01__run();
  demo01__finish();

  // ---
  return Result;
}