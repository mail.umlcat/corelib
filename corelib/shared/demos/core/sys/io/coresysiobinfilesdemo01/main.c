#include <stdio.h>
#include <stdlib.h>

// ---

#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"
#include "C:\softdev\corelib\corelib\src\core\sys\io\coresysio.h"

// ------------------

/* override */ int /* func */ demo01__start
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysbase__start();

  // ---
  return Result;
} // func

/* override */ int /* func */ demo01__finish
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysbase__finish();

  // ---
  return Result;
} // func

/* override */ int /* func */ demo01__run
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
 
  bool /* var */ CanContinue = false;
  // ---
 
  coresysiofiles__binfile* /* param */ AFile = NULL;
  
  AFile =
    coresysiobinfiles__openwriteonly
      ("C:\\softdev\\corelib\\corelib\\demos\\core\\sys\\io\\coresysiobinfilesdemo01\\test.dat");
  
  for (memint8_t /* var */ i = 0; i < 5; i++)
  {
    coresysiobinfiles__writesize
      (AFile, (pointer*) &i, sizeof(i));
  } // for  
    
  coresysiobinfiles__close
    (intoref AFile);
    
  AFile = NULL;
    
  // ---
  
  AFile =
    coresysiobinfiles__openreadonly
      ("C:\\softdev\\corelib\\corelib\\demos\\core\\sys\\io\\coresysiobinfilesdemo01\\test.dat");
  
  memint8_t /* var */ i = 0;
  
  do
  {
    CanContinue =
    	coresysiobinfiles__tryreadsize
        (AFile, (pointer*) &i, sizeof(i));
    if (CanContinue)
    {
      printf("Data: [%i]", i);
      puts("\n");
	  } // if
  } // do
  while (CanContinue);
  puts("\n");
  
//  while (coresysiobinfiles__tryreadsize(AFile, BufferData, sizeof(BufferData) - 1))
//  {
//      puts(BufferData);
//      puts("\n");
//  } // while
    
  coresysiobinfiles__close
    (intoref AFile);
    
  // ---

  printf("Press [RETURN] to continue ...\n\n", NULL);
  system("pause");

  // ---
  return Result;
} // func

// ------------------

int main(int argc, char *argv[])
{
  int /* var */ Result = 0;
  // ---
  
  demo01__start();
  Result =
    demo01__run();
  demo01__finish();

  // ---
  return Result;
}