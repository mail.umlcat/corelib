#include <stdio.h>
#include <stdlib.h>

// ---

#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"
#include "C:\softdev\corelib\corelib\src\core\sys\io\coresysio.h"

// ------------------

/* override */ int /* func */ demo01__start
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysbase__start();
  coresysmemints__start();
  coresyschars__start();
  coresysstrs__start();
  coresysio__start();

  // ---
  return Result;
} // func

/* override */ int /* func */ demo01__finish
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysio__finish();
  coresysstrs__finish();
  coresyschars__finish();
  coresysmemints__finish();
  coresysbase__finish();

  // ---
  return Result;
} // func

/* override */ int /* func */ demo01__run
  ( noparams )
{
  int /* var */ Result = 0;
  // ---

  ansichar /* var */ SourceFileName[512];
  ansichar /* var */ DestFileName[512];
  
  ansinullstring* /* var */ D;

  coresysstrsansinullstrs__clearsize
    (SourceFileName, sizeof(SourceFileName));
  coresysstrsansinullstrs__clearsize
    (DestFileName, sizeof(DestFileName));

  coresysstrsansinullstrs__assign
    (SourceFileName, "C:\\softdev\\corelib\\corelib\\apps\\semicolon2under02\\data\\coresysstrsansinullstrs.txt");

  coresysioansitxtfiles__stdputstr
    ("SourceFileName: ");
  coresysioansitxtfiles__stdputstrsize
    (SourceFileName, sizeof(SourceFileName));
  coresysioansitxtfiles__stdputnewline
    ();
    
//  coresysioansipaths__changefileext
//    (ref DestFileName, SourceFileName, "*.c");
    
  D = DestFileName;
    
  coresysioansipaths__changefileext
    (ref D, (ansinullstring*) SourceFileName, (const ansinullstring*) "xc");
    

  coresysioansitxtfiles__stdputstr
    ("DestFileName: ");
  coresysioansitxtfiles__stdputstrsize
    (DestFileName, sizeof(DestFileName));
  coresysioansitxtfiles__stdputnewline
    ();
    
  // ---

  printf("Press [RETURN] to continue ...\n\n", NULL);
  system("pause");

  // ---
  return Result;
} // func

// ------------------

int main(int argc, char *argv[])
{
  int /* var */ Result = 0;
  // ---
  
  demo01__start();
    Result =
      demo01__run();
  demo01__finish();

  // ---
  return Result;
}
