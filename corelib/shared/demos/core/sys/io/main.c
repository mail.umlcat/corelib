#include <stdio.h>
#include <stdlib.h>

// ---

#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"
#include "C:\softdev\corelib\corelib\src\core\sys\io\coresysio.h"

// ------------------

/* override */ int /* func */ demo01__start
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysbase__start();

  // ---
  return Result;
} // func

/* override */ int /* func */ demo01__finish
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysbase__finish();

  // ---
  return Result;
} // func

/* override */ int /* func */ demo01__run
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
  
  ansichar /* var */ BufferData[256];
  // ---
 
  bool /* var */ CanContinue = false;
  // ---
 
  coresysiofiles__textfile* /* param */ AFile;
  
  AFile =
    coresysioansitxtfiles__openwriteonly
      ("C:\\softdev\\corelib\\corelib\\demos\\core\\sys\\coresysioansitextfilesdemo01\\test.txt");
  
  strcpy(BufferData, "Spring");
  coresysioansitxtfiles__putstr
    (AFile, BufferData, strlen(BufferData));
  coresysioansitxtfiles__putnewline
    (AFile);
  
  strcpy(BufferData, "Summer");
  coresysioansitxtfiles__putstr
    (AFile, BufferData, strlen(BufferData));
  coresysioansitxtfiles__putnewline
    (AFile);
  
  strcpy(BufferData, "Fall");
  coresysioansitxtfiles__putstr
    (AFile, BufferData, strlen(BufferData));
  coresysioansitxtfiles__putnewline
    (AFile);
  
  strcpy(BufferData, "Winter");
  coresysioansitxtfiles__putstr
    (AFile, BufferData, strlen(BufferData));
  coresysioansitxtfiles__putnewline
    (AFile);    
    
  coresysioansitxtfiles__close
    (AFile);
    
  // ---
  
  AFile =
    coresysioansitxtfiles__openreadonly
      ("C:\\softdev\\corelib\\corelib\\demos\\core\\sys\\coresysioansitextfilesdemo01\\test.txt");
  
//  do
//  {
//  	coresysioansitxtfiles__getstr(AFile, BufferData, sizeof(BufferData) - 1);
//  	
//    CanContinue =
//      (coresysioansitxtfiles__geterrorcode(AFile) == 0);
//    if (CanContinue)
//    {
//      puts(BufferData);
//      puts("\n");
//	} // if
//  } // do
//  while (CanContinue);
  
  while (coresysioansitxtfiles__getstr(AFile, BufferData, sizeof(BufferData) - 1) != NULL)
  {
      puts(BufferData);
      puts("\n");
  } // while
    
  coresysioansitxtfiles__close
    (AFile);
    
  // ---

  printf("Press [RETURN] to continue ...\n\n", NULL);
  system("pause");

  // ---
  return Result;
} // func

// ------------------

int main(int argc, char *argv[])
{
  int /* var */ Result = 0;
  // ---
  
  demo01__start();
  Result =
    demo01__run();
  demo01__finish();

  // ---
  return Result;
}
