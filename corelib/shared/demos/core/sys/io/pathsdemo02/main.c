#include <stdio.h>
#include <stdlib.h>

// ---

#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"
#include "C:\softdev\corelib\corelib\src\core\sys\io\coresysio.h"

// ------------------

/* override */ int /* func */ demo01__start
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysbase__start();
  coresysmemints__start();
  coresyschars__start();
  coresysstrs__start();
  coresysio__start();

  // ---
  return Result;
} // func

/* override */ int /* func */ demo01__finish
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysio__finish();
  coresysstrs__finish();
  coresyschars__finish();
  coresysmemints__finish();
  coresysbase__finish();

  // ---
  return Result;
} // func

/* override */ int /* func */ demo01__run
  ( noparams )
{
  int /* var */ Result = 0;
  // ---

  ansichar /* var */ ASourceFullPath[512];
  
  ansinullstring* /* var */ ASourceFullPathBuffer;
  size_t          /* var */ ASourceFullPathSize;
  
  ansichar /* var */ ADestPath[512];
  
  ansinullstring* /* var */ ADestPathBuffer;
  size_t          /* var */ ADestPathSize;

  coresysioansipaths__scanpath /* var */ AScanPathStruct;
  
  // ---
  
  ASourceFullPathBuffer = /* & */ ASourceFullPath;
  ASourceFullPathSize = sizeof(ASourceFullPath);
  
  ADestPathBuffer = /* & */ ADestPath;
  ADestPathSize = sizeof(ADestPath);
  
  // ---
  
  coresysstrsansinullstrs__clearsize
    (ASourceFullPathBuffer, ASourceFullPathSize);

  coresysstrsansinullstrs__assign
    (ASourceFullPath, "C:\\doc.txt");
    
    //(ASourceFullPath, "C:\\softdev\\corelib\\corelib\\apps\\semicolon2under02\\data\\coresysstrsansinullstrs.txt");

  // ---
  
  coresysioansitxtfiles__stdputstr
    ("ASourceFullPath: ");
  coresysioansitxtfiles__stdputstrsize
    (ASourceFullPath, sizeof(ASourceFullPath));
  coresysioansitxtfiles__stdputnewline
    ();
  coresysioansitxtfiles__stdputnewline
    ();
    
  // ---
  
  coresysioansipaths__tryparsestartsize
    (ref AScanPathStruct, ASourceFullPathBuffer, ASourceFullPathSize);

  while (coresysioansipaths__tryparsenextsize
    (ref AScanPathStruct, (ansinullstring*) ADestPathBuffer, ADestPathSize))
  {
    coresysioansitxtfiles__stdputstr
      ("Element: ");
    coresysioansitxtfiles__stdputstrsize
      (ADestPathBuffer, ADestPathSize);
    coresysioansitxtfiles__stdputnewline
      ();
  } // while

  coresysioansipaths__tryparsefinish
    (ref AScanPathStruct);
    
  coresysioansitxtfiles__stdputnewline
    ();
    
  // ---

  printf("Press [RETURN] to continue ...\n\n", NULL);
  system("pause");

  // ---
  return Result;
} // func

// ------------------

int main(int argc, char *argv[])
{
  int /* var */ Result = 0;
  // ---
  
  demo01__start();
    Result =
      demo01__run();
  demo01__finish();

  // ---
  return Result;
}
