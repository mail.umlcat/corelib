#include <stdio.h>
#include <stdlib.h>

// ---

#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

/* override */ int /* func */ demo01__start
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysbase__start();
  coresysmemints__start();
  coresyschars__start();
  coresysstrs__start();

  // ---
  return Result;
} // func

/* override */ int /* func */ demo01__finish
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysstrs__finish();
  coresyschars__finish();
  coresysmemints__finish();
  coresysbase__finish();

  // ---
  return Result;
} // func

/* override */ int /* func */ demo01__run
  ( noparams )
{
  int /* var */ Result = 0;
  // ---

  printf("core::sys::base::params02(...)\n\n", NULL);
  
  // ---

  ansinullstring* /* var */ EachItem = NULL;

  size_t /* var */ EachItemSize = 0;
  
  constparams  /* var */ AParams = NULL;
  
  // ---
  
  ansichar s0[] = "Unknown";
  ansichar s1[] = "Spring";
  ansichar s2[] = "Summer";
  ansichar s3[] = "Fall";
  ansichar s4[] = "Winter";
  
  // ---
  
  AParams =
    coresysbaseparams__paramscreate(5);

  EachItem = NULL;
  
  // ---

  EachItem = 
    coresysstrsansinullstrs__compactcopy
        (s0);
  coresysbaseparams__paramsadd
    (AParams, (constparam) EachItem);

  EachItem = 
    coresysstrsansinullstrs__compactcopy
        (s1);
  coresysbaseparams__paramsadd
    (AParams, (constparam) EachItem);

  EachItem = 
    coresysstrsansinullstrs__compactcopy
        (s2);
  coresysbaseparams__paramsadd
    (AParams, (constparam) EachItem);
    
  EachItem = 
    coresysstrsansinullstrs__compactcopy
        (s3);
  coresysbaseparams__paramsadd
    (AParams, (constparam) EachItem);
    
  EachItem = 
    coresysstrsansinullstrs__compactcopy
        (s4);
  coresysbaseparams__paramsadd
    (AParams, (constparam) EachItem);
    
  // ---

  printf("\n", NULL);
  // ---

  for (int /* var */ i = 0; i < 5; i++)
  {
    EachItem =
      (ansichar*) coresysbaseparams__paramsreadparamat
        (AParams, i);

    printf("Item[%i]: %s\n", i, EachItem);
  } // for

  printf("\n", NULL);

  // ---

  for (int /* var */ i = 0; i < 5; i++)
  {
    EachItem =
      (ansichar*) coresysbaseparams__paramsreadparamat
        (AParams, i);

    EachItemSize =
      coresysstrsansinullstrs__length
        (EachItem);

  coresysstrsansinullstrs__dropstr
    ((ansinullstring* asref) &EachItem, EachItemSize);
  } // for

  // ---

  coresysbaseparams__paramsdrop
    (&AParams);

  // ---

  //printf("Press [RETURN] to continue ...\n\n", NULL);
  system("pause");

  // ---
  return Result;
} // func

// ------------------

int main(int argc, char *argv[])
{
  int /* var */ Result = 0;
  // ---
  
  demo01__start();
    Result =
      demo01__run();
  demo01__finish();

  // ---
  return Result;
}