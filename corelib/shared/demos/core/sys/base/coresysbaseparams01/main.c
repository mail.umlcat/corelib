#include <stdio.h>
#include <stdlib.h>

// ---

#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

/* override */ int /* func */ demo01__start
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysbase__start();
  coresysmemints__start();
  coresyschars__start();
  coresysstrs__start();

  // ---
  return Result;
} // func

/* override */ int /* func */ demo01__finish
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysstrs__finish();
  coresyschars__finish();
  coresysmemints__finish();
  coresysbase__finish();

  // ---
  return Result;
} // func

/* override */ int /* func */ demo01__run
  ( noparams )
{
  int /* var */ Result = 0;
  // ---

  printf("core::sys::base::params01(...)\n\n", NULL);
  
  // ---

  ansichar* /* var */ EachItem = NULL;
  ansichar  /* var */ EachChar = 'a';
  
  constparams  /* var */ AParams = NULL;
  
  // ---
  
  AParams =
    coresysbaseparams__paramscreate(5);

  EachItem = NULL;
  
  for (int /* var */ i = 0; i < 5; i++)
  {
    EachItem =
      (ansichar*) coresysbasemem__allocate(sizeof(ansichar));
    *EachItem =
      EachChar;
    
    coresysbaseparams__paramsadd
      (AParams, (constparam) EachItem);

    EachItem = NULL;
  
    EachChar++;
  } // for

  printf("\n", NULL);
  // ---

  for (int /* var */ i = 0; i < 5; i++)
  {
    EachItem =
      (ansichar*) coresysbaseparams__paramsreadparamat
        (AParams, i);

    printf("Item[%i]: %c\n", i, *EachItem);
  } // for

  printf("\n", NULL);

  // ---

  for (int /* var */ i = 0; i < 5; i++)
  {
    EachItem =
      (ansichar*) coresysbaseparams__paramsreadparamat
        (AParams, i);

    coresysbasemem__deallocate
      ((pointer**) &EachItem, sizeof(ansichar));
  } // for

  // ---

  coresysbaseparams__paramsdrop
    (&AParams);

  // ---

  //printf("Press [RETURN] to continue ...\n\n", NULL);
  system("pause");

  // ---
  return Result;
} // func

// ------------------

int main(int argc, char *argv[])
{
  int /* var */ Result = 0;
  // ---
  
  demo01__start();
    Result =
      demo01__run();
  demo01__finish();

  // ---
  return Result;
}