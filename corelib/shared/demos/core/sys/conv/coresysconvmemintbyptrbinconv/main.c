#include <stdio.h>
#include <stdlib.h>

// ---

#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"
#include "C:\softdev\corelib\corelib\src\core\sys\conv\coresysconv.h"

// ------------------

/* override */ int /* func */ demo01__start
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysbase__start();
  coresysmemints__start();
  coresyschars__start();
  coresysstrs__start();
  coresysconv__start();

  // ---
  return Result;
} // func

/* override */ int /* func */ demo01__finish
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysconv__finish();
  coresysstrs__finish();
  coresyschars__finish();
  coresysmemints__finish();
  coresysbase__finish();

  // ---
  return Result;
} // func

/* override */ int /* func */ demo01__run
  ( noparams )
{
  int /* var */ Result = 0;
  // ---

  printf("core::sys::uints::memintbyptransibinconv(...)\n\n", NULL);

  pointer* /* var */ P = NULL;
  
  int /* var */ A = 0;
  
  ansichar   /* var */ BufferArray[256];
  ansichar*  /* var */ ADestPtr;
  size_t     /* var */ ADestSize;

  // ---

  ADestSize =
    sizeof(BufferArray);
  ADestPtr =
    /* & */ BufferArray;
    
  coresysstrsansinullstrs__clearsize
    (BufferArray, ADestSize);

  // ---

  P =
    coresysmemintsmint8byptrs__consttoptr(8);
  
  // compiler automatic casting
  A = *((memint8_t*) P);
  
  printf("printf\n", NULL);
  printf("P: [%i]\n", A);
  printf("\n", NULL);
    
  coresysconvmemintbyptransibinconv__concatmemint8ptrtobinstr
    (ADestPtr, ADestSize, P);
    
  printf("core::sys::conv::memintbyptrbinconv::concatmemint8ptrtoansibinstr(...)\n", NULL);
  printf("P: [%s]\n", ADestPtr);
  printf("\n", NULL);
    
  coresysmemintsmint8byptrs__dropptr
    (&P);

  // ---

  //printf("Press [RETURN] to continue ...\n\n", NULL);
  system("pause");

  // ---
  return Result;
} // func

// ------------------

int main(int argc, char *argv[])
{
  int /* var */ Result = 0;
  // ---
  
  demo01__start();
    Result =
      demo01__run();
  demo01__finish();

  // ---
  return Result;
}