/** Module: "core<modulename>s.h"
 ** Descr.:
 ** "<put description here>"
 ** "<put description here>"
 ** "<put description here>"
 ** "<put description here>"
 **/

// namespace core<modulename>s {
 
// ------------------
 
#ifndef CORE<MODULENAME>S_H
#define CORE<MODULENAME>S_H
 
// ------------------

// "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// basic libraries here
#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

// other "corelib" libraries here
#include <put your library here>
#include <put your library here>
#include <put your library here>

// ------------------

// other NON "corelib" libraries here
#include <put your library here>
#include <put your library here>
#include <put your library here>

// ------------------

//$define void noparams

// ------------------
 
//typedef
//  sometype         /* as */ sometype;
   
// ------------------

//struct failure
//{
//  ansichar /* var */ Mesage;
//  pointer* /* var */ Parent;
//} ;

// ------------------

//pointer* /* func */ core<modulename>s__malloc
//  (/* in */ size_t /* param */ ASize);
  
//void /* func */ core<modulename>s__dealloc
//  (/* out */ pointer** /* param */ ADest,
//   /* in */  size_t    /* param */ ASize);

 // ...
 
// ------------------

/* override */ const ansinullstring* /* func */ core<modulename>s__modulename
  ( noparams );

/* override */ int /* func */ core<modulename>s__setup
  ( noparams );

/* override */ int /* func */ core<modulename>s__setoff
  ( noparams );

// ------------------

#endif // CORE<MODULENAME>S_H

// } // namespace core<modulename>s
