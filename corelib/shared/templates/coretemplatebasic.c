/** Module: "core<modulename>s.c"
 ** Descr.:
 ** "<put description here>"
 ** "<put description here>"
 ** "<put description here>"
 ** "<put description here>"
 **/

// namespace core<modulename>s {
 
// ------------------

// "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// basic libraries here
#include "coresystem.h"
#include "corememory.h"
#include "corefunctors.h"
#include "coreuuids.h"
#include "coreoswarnings.h"

// ------------------

// other "corelib" libraries here
#include <put your library here>
#include <put your library here>
#include <put your library here>

// ------------------

// other NON "corelib" libraries here
#include <put your library here>
#include <put your library here>
#include <put your library here>

// ------------------

// this module own header goes here
#include "core<modulename>s.h"
 
// ------------------

comparison /* func */ core<modulename>s__compare
  (/* in */ ansinullstring* /* param */ A, B)
{
  comparison /* var */ Result = comparison__equal;
  // ---
     
     
  // ---
  return Result;
} // func
   
// ------------------
 
void /* func */ core<modulename>s__clear
  (/* inout */ ansinullstring* /* param */ ADest)
{
  // ...
} // func
 
 // ...

// ------------------

/* override */ const ansinullstring* /* func */ core<modulename>s__modulename
  ( noparams )
{
  static ansichar /* var */ setbuffer[] =
    "core<modulename>s";
  
  return (const ansinullstring*) setbuffer;
} // func

/* override */ int /* func */ core<modulename>s__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ core<modulename>s__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace core<modulename>s
