/** Module: "coresyscharsansicharptrs.c"
 ** Descr.: "A.N.S.I. Character Encoding"
 **         "Individual characters, accessed by pointer,"
 **         "operations library."
 **/

// $$ namespace coresyscharsansicharptrs
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

// --> same package libraries here
#include "coresyscharsansichars.h"

// ------------------

// --> this module own header goes here
#include "coresyscharsansicharptrs.h"
 
// ------------------

/* functions */


/* $$ inline */ bool /* $$ func */ coresyscharsansicharptrs_isempty
  (/* $$ in */ const ansichar* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
  
  Result =
    (AValue != NULL) && (*AValue != ANSINULLCHAR);

  // ---
  return Result;
} // func

// ------------------

ansichar /* $$ func */ coresyscharsansicharptrs_chartolower
  (/* $$ in */ const ansichar* /* $$ param */ AValue)
{
  ansichar /* $$ var */ Result = ANSINULLCHAR;
  // ---
  
  Result = tolower(*AValue);

  // ---
  return Result;
} // func

ansichar /* $$ func */ coresyscharsansicharptrs_chartoupper
  (/* $$ in */ const ansichar* /* $$ param */ AValue)
{
  ansichar /* $$ var */ Result = ANSINULLCHAR;
  // ---
     
  Result = toupper(*AValue);
     
  // ---
  return Result;
} // func

ansichar /* $$ func */ coresyscharsansicharptrs_chartorevcase
  (/* $$ in */ const ansichar* /* $$ param */ AValue)
{
  ansichar /* $$ var */ Result = ANSINULLCHAR;
  // ---
    
  if (coresyscharsansicharptrs_isalpha(AValue))
  {
    if (coresyscharsansicharptrs_isuppercase(AValue))
    {
      Result = tolower(*AValue);
    }
    else
    {
      Result = toupper(*AValue);
    }
  }
     
  // ---
  return Result;
} // func


// ------------------

/* $$ inline */ byte_t /* $$ func */ coresyscharsansicharptrs_chartodec
  (/* $$ in */ const ansichar* /* $$ param */ AValue)
{
  byte_t /* $$ var */ Result = 0;
  // ---
     
  Result =
     ((byte_t) *AValue);
     
  // ---
  return Result;
} // func

/* $$ inline */ ansichar /* $$ func */ coresyscharsansicharptrs_dectochar
  (/* $$ in */ const byte_t /* $$ param */ AValue)
{
  ansichar /* $$ var */ Result = ANSINULLCHAR;
  // ---
     
  Result =
     ((unsigned char) AValue);

  // ---
  return Result;
} // func


// ------------------

/* $$ inline */ bool /* $$ func */ coresyscharsansicharptrs_isnullmarker
  (/* $$ in */ const ansichar* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result =
    (AValue != NULL) &&
    (*AValue == ANSINULLCHAR);

  // ---
  return Result;
} // func

// ------------------

bool /* $$ func */ coresyscharsansicharptrs_islowercase
  (/* $$ in */ const ansichar* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = islower(*AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharsansicharptrs_isuppercase
  (/* $$ in */ const ansichar* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = isupper(*AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharsansicharptrs_isspace
  (/* $$ in */ const ansichar* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = isspace(*AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharsansicharptrs_isblank
  (/* $$ in */ const ansichar* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = isblank(*AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharsansicharptrs_isdigit
  (/* $$ in */ const ansichar* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = isdigit(*AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharsansicharptrs_isalpha
  (/* $$ in */ const ansichar* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = isalpha(*AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharsansicharptrs_isalphanum
  (/* $$ in */ const ansichar* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = isalnum(*AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharsansicharptrs_ispunct
  (/* $$ in */ const ansichar* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = ispunct(*AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharsansicharptrs_iscontrol
  (/* $$ in */ const ansichar* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = iscntrl(*AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharsansicharptrs_isgraph
  (/* $$ in */ const ansichar* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = isgraph(*AValue);
  
  // ---
  return Result;
} // func

// ------------------

bool /* $$ func */ coresyscharsansicharptrs_isid
  (/* $$ in */ const ansichar* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result =
    (isalnum(*AValue) || (*AValue == '_'));
  
  // ---
  return Result;
} // func

// ------------------

/* procedures */

/* $$ inline */ void /* $$ func */ coresyscharsansicharptrs_clear
  (/* $$ out */ nonconst ansichar* /* $$ param */ ADest)
{
  (*ADest) = ANSINULLCHAR;
} // func


// ------------------

/* operators */

/* $$ inline */ enum comparison /* $$ func */ coresyscharsansicharptrs_compare
  (/* $$ in */ /* restricted */ const ansichar* /* $$ param */ A,
   /* $$ in */ /* restricted */ const ansichar* /* $$ param */ B)
{
  enum comparison /* $$ var */ Result = comparison_equal;
  // ---
  
  if (*A < *B)
  {
    Result = comparison_lesser;
  }
  else if (*A > *B)
  {
    Result = comparison_greater;
  }
  else
  {
    Result = comparison_equal;
  }

  // ---
  return Result;
} // func

/* $$ inline */ bool /* $$ func */ coresyscharsansicharptrs_equal
  (/* $$ in */ /* restricted */ const ansichar* /* $$ param */ A,
   /* $$ in */ /* restricted */ const ansichar* /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
  
  Result =
    (A != NULL) &&
    (B != NULL) &&
    (*A == *B);

  // ---
  return Result;
} // func

/* $$ inline */ bool /* $$ func */ coresyscharsansicharptrs_same
  (/* $$ in */ /* restricted */ const ansichar* /* $$ param */ A,
   /* $$ in */ /* restricted */ const ansichar* /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
  
  Result =
    (A != NULL) && (B != NULL);
  if (Result)
  {
    Result =
      (tolower(*A) == tolower(*B));
  } // if

  // ---
  return Result;
} // func

// ------------------

/* rtti */


// ------------------



 // ...

// ------------------

/* $$ override */ int /* $$ func */ coresyscharsansicharptrs_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresyscharsansicharptrs_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // $$ namespace coresyscharsansicharptrs