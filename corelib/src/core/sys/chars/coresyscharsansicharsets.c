/** Module: "coresyscharsansicharsets.c"
 ** Descr.: "ANSI Character Set implemented,"
 **         "with NULL marker terminated strings, library."
 **/

// $$ namespace coresyscharsansicharsets
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

// --> same package libraries here
#include "coresyscharsansichars.h"

// ------------------

// --> this same module header goes here
#include "coresyscharsansicharsets.h"

// ------------------

/* functions */

/* $$ inline */ bool /* $$ func */ coresyscharsansicharsets_isempty
  (/* $$ in */ const ansicharset* /* $$ param */ ASet)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result =
    ((ASet == NULL) || ((*ASet)[0] == ANSINULLCHAR));

  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharsansicharsets_ismember
  (/* $$ in */ const ansicharset* /* $$ param */ ASet,
   /* $$ in */ const ansichar     /* $$ param */ AItem)
{
  bool /* $$ var */ Result = false;
  // ---
  
  Result =
    (! coresyscharsansicharsets_isempty(ASet));
  if (Result)
  {
    bool      /* $$ var */ CanContinue;
    bool      /* $$ var */ Found;

    ansichar* /* $$ var */ EachChar;
    index_t   /* $$ var */ EachIndex;

    EachChar    = (ansichar*) ASet;
    EachIndex   = 0;
    CanContinue = true;
    Found       = false;

    while (CanContinue && !Found)
    {
      Found =
        (*EachChar == AItem);

      CanContinue =
        ((*EachChar != ANSINULLCHAR) &&
         (EachIndex < ANSICHARSETMAXSIZE));
    
      if (!CanContinue)
      {
        EachIndex++;
        EachChar++;
      }
    } // while
  } // if

  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharsansicharsets_ismemberptr
  (/* $$ in */ const ansicharset* /* $$ param */ ASet,
   /* $$ in */ const ansichar*    /* $$ param */ AItem)
{
  bool /* $$ var */ Result = false;
  // ---
  
  Result =
    (! coresyscharsansicharsets_isempty(ASet));
  if (Result)
  {
    bool      /* $$ var */ CanContinue;
    bool      /* $$ var */ Found;

    ansichar* /* $$ var */ EachChar;
    index_t   /* $$ var */ EachIndex;

    EachChar    = (ansichar*) ASet;
    EachIndex   = 0;
    CanContinue = true;
    Found       = false;

    while (CanContinue && !Found)
    {
      Found =
        (*EachChar == *AItem);

      CanContinue =
        ((*EachChar != ANSINULLCHAR) &&
         (EachIndex < ANSICHARSETMAXSIZE));
    
      if (!CanContinue)
      {
        EachIndex++;
        EachChar++;
      }
    } // while
  } // if

  // ---
  return Result;
} // func

// ------------------

/* procedures */

ansicharset* /* $$ func */ coresyscharsansicharsets_createset
 ( noparams )
{
  ansicharset* /* $$ var */ Result = NULL;
  // ---
     
  Result =
    (ansicharset*) coresysbasemem_allocateclear(sizeof(ansicharset));

  // ---
  return Result;
} // func

void /* $$ func */ coresyscharsansicharsets_dropset
 (/* $$ out */ nonconst ansicharset** /* $$ param */ ASet)
{
  bool /* $$ var */ CanContinue = false;
  // ---

  CanContinue =
    ((ASet != NULL) && ((*ASet) != NULL));
  if (CanContinue)
  {
    coresysbasemem_deallocateclear
	  ((pointer**)ASet, sizeof(ansicharset));
	//*ASet = NULL;
  }
} // func

void /* $$ func */ coresyscharsansicharsets_clear
  (/* $$ inout */ nonconst ansicharset* /* $$ param */ ASet)
{
  bool /* $$ var */ CanContinue = false;
  // ---

  CanContinue =
    ((ASet != NULL) && ((*ASet) != NULL));
  if (CanContinue)
  {
    coresysbasemem_clear
      ((pointer*) ASet, ANSICHARSETMAXSIZE);
  }
} // func
   
void /* $$ func */ coresyscharsansicharsets_include
  (/* $$ in */ const ansicharset* /* $$ param */ ASet,
   /* $$ in */ const ansichar     /* $$ param */ AItem)
{
  bool /* $$ var */ CanContinue = false;
  // ---

  CanContinue =
    (ASet != NULL);
  if (CanContinue)
  {
    // coresysbasemem_clear
      // (ASet, ANSICHARSETMAXSIZE);
  }
} // func 
   
void /* $$ func */ coresyscharsansicharsets_exclude
  (/* $$ in */ const ansicharset* /* $$ param */ ASet,
   /* $$ in */ const ansichar     /* $$ param */ AItem)
{
  bool /* $$ var */ CanContinue = false;
  // ---

  CanContinue =
    (ASet != NULL);
  if (CanContinue)
  {
    // coresysbasemem_clear
      // (ASet, ANSICHARSETMAXSIZE);
  }
} // func 

// ------------------

/* operators */

// ------------------

/* rtti */



 
// ------------------

 // ...

// ------------------

/* $$ override */ int /* $$ func */ coresyscharsansicharsets_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresyscharsansicharsets_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // $$ namespace coresyscharsansicharsets