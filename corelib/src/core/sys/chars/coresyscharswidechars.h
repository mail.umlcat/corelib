/** Module: "coresyscharswidechars.h"
 ** Descr.: "Individual Wide / Long Character"
 **         "operations library."
 **/

// $$ namespace coresyscharswidechars
// $$ {
 
// ------------------
 
#ifndef CORESYSCHARSWIDECHARS_H
#define CORESYSCHARSWIDECHARS_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>
#include <ctype.h>
//.include <cwtype.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

/* types */

typedef
  widechar /* $$ as */ coresyscharswidechars_widechar;

// ------------------

/* functions */

widechar /* $$ func */ coresyscharswidechars_chartolower
  (/* $$ in */ const widechar /* $$ param */ AValue);

widechar /* $$ func */ coresyscharswidechars_chartoupper
  (/* $$ in */ const widechar /* $$ param */ AValue);

widechar /* $$ func */ coresyscharswidechars_chartorevcase
  (/* $$ in */ const widechar /* $$ param */ AValue);

// ------------------

/* $$ inline */ word_t /* $$ func */ coresyscharswidechars_chartodec
  (/* $$ in */ const widechar /* $$ param */ AValue);

/* $$ inline */ widechar /* $$ func */ coresyscharswidechars_dectochar
  (/* $$ in */ const word_t /* $$ param */ AValue);

// ------------------

/* $$ inline */ bool /* $$ func */ coresyscharswidechars_isnullmarker
  (/* $$ in */ const widechar /* $$ param */ AValue);

bool /* $$ func */ coresyscharswidechars_islowercase
  (/* $$ in */ const widechar /* $$ param */ AValue);
 
bool /* $$ func */ coresyscharswidechars_isuppercase
  (/* $$ in */ const widechar /* $$ param */ AValue);

bool /* $$ func */ coresyscharswidechars_isspace
  (/* $$ in */ const widechar /* $$ param */ AValue);

bool /* $$ func */ coresyscharswidechars_isblank
  (/* $$ in */ const widechar /* $$ param */ AValue);

bool /* $$ func */ coresyscharswidechars_isdigit
  (/* $$ in */ const widechar /* $$ param */ AValue);
  
bool /* $$ func */ coresyscharswidechars_isalpha
  (/* $$ in */ const widechar /* $$ param */ AValue);
  
bool /* $$ func */ coresyscharswidechars_isalphanum
  (/* $$ in */ const widechar /* $$ param */ AValue);
  
bool /* $$ func */ coresyscharswidechars_ispunct
  (/* $$ in */ const widechar /* $$ param */ AValue);
  
bool /* $$ func */ coresyscharswidechars_iscontrol
  (/* $$ in */ const widechar /* $$ param */ AValue);
  
bool /* $$ func */ coresyscharswidechars_isgraph
  (/* $$ in */ const widechar /* $$ param */ AValue);

// ------------------

bool /* $$ func */ coresyscharswidechars_isid
  (/* $$ in */ const widechar /* $$ param */ AValue);


// ------------------

/* procedures */

/* $$ inline */ void /* $$ func */ coresyscharswidechars_clear
  (/* $$ out */ nonconst widechar* /* $$ param */ ADest);

/* $$ inline */ void /* $$ func */ coresyscharswidechars_assign
  (/* $$ out */ nonconst widechar* /* $$ param */ ADest,
   /* $$ in */  const    widechar  /* $$ param */ ASource);
   
// ------------------

/* operators */

/* $$ inline */ enum comparison /* $$ func */ coresyscharswidechars_compare
  (/* $$ in */ const widechar /* $$ param */ A,
   /* $$ in */ const widechar /* $$ param */ B);
   
bool /* $$ func */ coresyscharswidechars_equal
  (/* $$ in */ const widechar /* $$ param */ A,
   /* $$ in */ const widechar /* $$ param */ B);
   
bool /* $$ func */ coresyscharswidechars_different
  (/* $$ in */ const widechar /* $$ param */ A,
   /* $$ in */ const widechar /* $$ param */ B);

// ------------------

/* rtti */ 

bool /* $$ func */ coresyscharswidechars_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ ASource);

bool /* $$ func */ coresyscharswidechars_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ ADest);

void /* $$ func */ coresyscharswidechars_rttiapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ ADest);

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresyscharswidechars_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresyscharswidechars_finish
  ( noparams );

// ------------------

#endif // CORESYSCHARSWIDECHARS_H

// $$ } // $$ namespace coresyscharswidechars