/** Module: "coresyscharswidechars.c"
 ** Descr.: "Individual Wide / Long Character"
 **         "operations library."
 **/

// $$ namespace coresyscharswidechars
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>
#include <ctype.h>
//.include <cwtype.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

// --> this same module header goes here
#include "coresyscharswidechars.h"

// ------------------

/* functions */

widechar /* $$ func */ coresyscharswidechars_chartolower
  (/* $$ in */ const widechar /* $$ param */ AValue)
{
  widechar /* $$ var */ Result = WIDENULLCHAR;
  // ---
  
  Result = towlower(AValue);

  // ---
  return Result;
} // func

widechar /* $$ func */ coresyscharswidechars_chartoupper
  (/* $$ in */ const widechar /* $$ param */ AValue)
{
  widechar /* $$ var */ Result = WIDENULLCHAR;
  // ---
     
  Result = towupper(AValue);
     
  // ---
  return Result;
} // func

widechar /* $$ func */ coresyscharswidechars_chartorevcase
  (/* $$ in */ const widechar /* $$ param */ AValue)
{
  widechar /* $$ var */ Result = WIDENULLCHAR;
  // ---
    
  if (coresyscharswidechars_isalpha(AValue))
  {
    if (coresyscharswidechars_isuppercase(AValue))
    {
      Result = towlower(AValue);
    }
    else
    {
      Result = towupper(AValue);
    }
  }
     
  // ---
  return Result;
} // func
// ------------------

/* $$ inline */ word_t /* $$ func */ coresyscharswidechars_chartodec
  (/* $$ in */ const widechar /* $$ param */ AValue)
{
  word_t /* $$ var */ Result = 0;
  // ---
     
  Result =
     ((word_t) AValue);
     
  // ---
  return Result;
} // func

/* $$ inline */ widechar /* $$ func */ coresyscharswidechars_dectochar
  (/* $$ in */ const word_t /* $$ param */ AValue)
{
  widechar /* $$ var */ Result = WIDENULLCHAR;
  // ---
     
  Result =
     ((widechar) AValue);

  // ---
  return Result;
} // func

// ------------------

/* $$ inline */ bool /* $$ func */ coresyscharswidechars_isnullmarker
  (/* $$ in */ const widechar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = (AValue == WIDENULLCHAR);
  
  // ---
  return Result;
} // func

// ------------------

bool /* $$ func */ coresyscharswidechars_islowercase
  (/* $$ in */ const widechar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = iswlower(AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharswidechars_isuppercase
  (/* $$ in */ const widechar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = iswupper(AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharswidechars_isspace
  (/* $$ in */ const widechar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = iswspace(AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharswidechars_isblank
  (/* $$ in */ const widechar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = iswblank(AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharswidechars_isdigit
  (/* $$ in */ const widechar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = iswdigit(AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharswidechars_isalpha
  (/* $$ in */ const widechar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = iswalpha(AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharswidechars_isalphanum
  (/* $$ in */ const widechar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = iswalnum(AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharswidechars_ispunct
  (/* $$ in */ const widechar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = iswpunct(AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharswidechars_iscontrol
  (/* $$ in */ const widechar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = iswcntrl(AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharswidechars_isgraph
  (/* $$ in */ const widechar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = iswgraph(AValue);
  
  // ---
  return Result;
} // func

// ------------------

bool /* $$ func */ coresyscharswidechars_isid
  (/* $$ in */ const widechar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result =
    (iswalnum(AValue) || (AValue == '_'));
  
  // ---
  return Result;
} // func

// ------------------

/* procedures */

/* $$ inline */ void /* $$ func */ coresyscharswidechars_clear
  (/* $$ out */ nonconst widechar* /* $$ param */ ADest)
{
  (*ADest) = WIDENULLCHAR;
} // func

/* $$ inline */ void /* $$ func */ coresyscharswidechars_assign
  (/* $$ out */ nonconst widechar* /* $$ param */ ADest,
   /* $$ in */  const    widechar  /* $$ param */ ASource)
{
  *ADest = ASource;
} // func

// ------------------

/* operators */

/* $$ inline */ enum comparison /* $$ func */ coresyscharswidechars_compare
  (/* $$ in */ const widechar /* $$ param */ A,
   /* $$ in */ const widechar /* $$ param */ B)
{
  enum comparison /* $$ var */ Result = comparison_equal;
  // ---
  
  if (A < B)
  {
    Result = comparison_lesser;
  }
  else if (A > B)
  {
    Result = comparison_greater;
  }
  else
  {
    Result = comparison_equal;
  }

  // ---
  return Result;
} // func
   
bool /* $$ func */ coresyscharswidechars_equal
  (/* $$ in */ const widechar /* $$ param */ A,
   /* $$ in */ const widechar /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
  
  Result =
    (A == B);

  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharswidechars_different
  (/* $$ in */ const widechar /* $$ param */ A,
   /* $$ in */ const widechar /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
  
  Result =
    (A != B);

  // ---
  return Result;
} // func

// ------------------

/* rtti */ 

bool /* $$ func */ coresyscharswidechars_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = (ASource == (pointer*) WIDENULLCHAR);
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharswidechars_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ ADest)
{
  bool /* $$ var */ Result = false;
  // ---
  
  Result = (*ADest != NULL);
  if (Result)
  {
    (*ADest) = WIDENULLCHAR;
  }
     
  // ---
  return Result;
} // func

void /* $$ func */ coresyscharswidechars_rttiapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ ADest)
{
  /* discard */ coresyscharswidechars_rttitryapplydefaultvalue
    (ADest);
} // func

// ------------------

 // ...

// ------------------

/* $$ override */ int /* $$ func */ coresyscharswidechars_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresyscharswidechars_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // $$ namespace coresyscharswidechars