/** Module: "coresyscharsansicharptrs.h"
 ** Descr.: "A.N.S.I. Character Encoding"
 **         "Individual characters, accessed by pointer,"
 **         "operations library."
 **/

// $$ namespace coresyscharsansicharptrs
// $$ {
 
// ------------------
 
#ifndef CORESYSCHARSANSICHARPTRS_H
#define CORESYSCHARSANSICHARPTRS_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

// --> same package libraries here
#include "coresyscharsansichars.h"

// ------------------

/* functions */

/* $$ inline */ bool /* $$ func */ coresyscharsansicharptrs_isempty
  (/* $$ in */ const ansichar* /* $$ param */ AValue);
  
// ------------------

ansichar /* $$ func */ coresyscharsansicharptrs_chartolower
  (/* $$ in */ const ansichar* /* $$ param */ AValue);

ansichar /* $$ func */ coresyscharsansicharptrs_chartoupper
  (/* $$ in */ const ansichar* /* $$ param */ AValue);

ansichar /* $$ func */ coresyscharsansicharptrs_chartorevcase
  (/* $$ in */ const ansichar* /* $$ param */ AValue);


// ------------------

/* $$ inline */ byte_t /* $$ func */ coresyscharsansicharptrs_chartodec
  (/* $$ in */ const ansichar* /* $$ param */ AValue);

/* $$ inline */ ansichar /* $$ func */ coresyscharsansicharptrs_dectochar
  (/* $$ in */ const byte_t /* $$ param */ AValue);
// ------------------


/* $$ inline */ bool /* $$ func */ coresyscharsansicharptrs_isnullmarker
  (/* $$ in */ const ansichar* /* $$ param */ AValue);


// ------------------

bool /* $$ func */ coresyscharsansicharptrs_islowercase
  (/* $$ in */ const ansichar* /* $$ param */ AValue);
 
bool /* $$ func */ coresyscharsansicharptrs_isuppercase
  (/* $$ in */ const ansichar* /* $$ param */ AValue);

bool /* $$ func */ coresyscharsansicharptrs_isspace
  (/* $$ in */ const ansichar* /* $$ param */ AValue);

bool /* $$ func */ coresyscharsansicharptrs_isblank
  (/* $$ in */ const ansichar* /* $$ param */ AValue);

bool /* $$ func */ coresyscharsansicharptrs_isdigit
  (/* $$ in */ const ansichar* /* $$ param */ AValue);
  
bool /* $$ func */ coresyscharsansicharptrs_isalpha
  (/* $$ in */ const ansichar* /* $$ param */ AValue);
  
bool /* $$ func */ coresyscharsansicharptrs_isalphanum
  (/* $$ in */ const ansichar* /* $$ param */ AValue);

bool /* $$ func */ coresyscharsansicharptrs_ispunct
  (/* $$ in */ const ansichar* /* $$ param */ AValue);
  
bool /* $$ func */ coresyscharsansicharptrs_iscontrol
  (/* $$ in */ const ansichar* /* $$ param */ AValue);
  
bool /* $$ func */ coresyscharsansicharptrs_isgraph
  (/* $$ in */ const ansichar* /* $$ param */ AValue);

// ------------------

bool /* $$ func */ coresyscharsansicharptrs_isid
  (/* $$ in */ const ansichar* /* $$ param */ AValue);

// ------------------

/* procedures */

/* $$ inline */ void /* $$ func */ coresyscharsansicharptrs_clear
  (/* $$ out */ nonconst ansichar* /* $$ param */ ADest);


// ------------------

/* operators */

/* $$ inline */ enum comparison /* $$ func */ coresyscharsansicharptrs_compare
  (/* $$ in */ /* restricted */ const ansichar* /* $$ param */ A,
   /* $$ in */ /* restricted */ const ansichar* /* $$ param */ B);
   
/* $$ inline */ bool /* $$ func */ coresyscharsansicharptrs_equal
  (/* $$ in */ /* restricted */ const ansichar* /* $$ param */ A,
   /* $$ in */ /* restricted */ const ansichar* /* $$ param */ B);

/* $$ inline */ bool /* $$ func */ coresyscharsansicharptrs_same
  (/* $$ in */ /* restricted */ const ansichar* /* $$ param */ A,
   /* $$ in */ /* restricted */ const ansichar* /* $$ param */ B);


// ------------------

/* rtti */


// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresyscharsansicharptrs_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresyscharsansicharptrs_finish
  ( noparams );

// ------------------

#endif // CORESYSCHARSANSICHARPTRS_H

// $$ } // $$ namespace coresyscharsansicharptrs