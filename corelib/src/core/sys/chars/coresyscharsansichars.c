/** Module: "coresyscharsansichars.c"
 ** Descr.: "Individual A.N.S.I. Character"
 **         "operations library."
 **/

// $$ namespace coresyscharsansichars
// $$ {
 
// ------------------
 
// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
#include <ctype.h>
//.include <cwtype.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

// --> this same module header goes here
#include "coresyscharsansichars.h"

// ------------------

/* functions */

ansichar /* $$ func */ coresyscharsansichars_chartolower
  (/* $$ in */ const ansichar /* $$ param */ AValue)
{
  ansichar /* $$ var */ Result = ANSINULLCHAR;
  // ---
  
  Result = tolower(AValue);

  // ---
  return Result;
} // func

ansichar /* $$ func */ coresyscharsansichars_chartoupper
  (/* $$ in */ const ansichar /* $$ param */ AValue)
{
  ansichar /* $$ var */ Result = ANSINULLCHAR;
  // ---
     
  Result = toupper(AValue);
     
  // ---
  return Result;
} // func

ansichar /* $$ func */ coresyscharsansichars_chartorevcase
  (/* $$ in */ const ansichar /* $$ param */ AValue)
{
  ansichar /* $$ var */ Result = ANSINULLCHAR;
  // ---
    
  if (coresyscharsansichars_isalpha(AValue))
  {
    if (coresyscharsansichars_isuppercase(AValue))
    {
      Result = tolower(AValue);
    }
    else
    {
      Result = toupper(AValue);
    }
  }
     
  // ---
  return Result;
} // func
// ------------------

/* $$ inline */ byte_t /* $$ func */ coresyscharsansichars_chartodec
  (/* $$ in */ const ansichar /* $$ param */ AValue)
{
  byte_t /* $$ var */ Result = 0;
  // ---
     
  Result =
     ((byte_t) AValue);
     
  // ---
  return Result;
} // func

/* $$ inline */ ansichar /* $$ func */ coresyscharsansichars_dectochar
  (/* $$ in */ const byte_t /* $$ param */ AValue)
{
  ansichar /* $$ var */ Result = ANSINULLCHAR;
  // ---
     
  Result =
     ((unsigned char) AValue);

  // ---
  return Result;
} // func

// ------------------

/* $$ inline */ bool /* $$ func */ coresyscharsansichars_isnullmarker
  (/* $$ in */ const ansichar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = (AValue == ANSINULLCHAR);
  
  // ---
  return Result;
} // func

// ------------------

bool /* $$ func */ coresyscharsansichars_islowercase
  (/* $$ in */ const ansichar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = islower(AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharsansichars_isuppercase
  (/* $$ in */ const ansichar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = isupper(AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharsansichars_isspace
  (/* $$ in */ const ansichar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = isspace(AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharsansichars_isblank
  (/* $$ in */ const ansichar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = isblank(AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharsansichars_isdigit
  (/* $$ in */ const ansichar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = isdigit(AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharsansichars_isalpha
  (/* $$ in */ const ansichar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = isalpha(AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharsansichars_isalphanum
  (/* $$ in */ const ansichar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = isalnum(AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharsansichars_ispunct
  (/* $$ in */ const ansichar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = ispunct(AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharsansichars_iscontrol
  (/* $$ in */ const ansichar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = iscntrl(AValue);
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharsansichars_isgraph
  (/* $$ in */ const ansichar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = isgraph(AValue);
  
  // ---
  return Result;
} // func

// ------------------

bool /* $$ func */ coresyscharsansichars_isid
  (/* $$ in */ const ansichar /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result =
    (isalnum(AValue) || (AValue == '_'));
  
  // ---
  return Result;
} // func

// ------------------

/* procedures */

/* $$ inline */ void /* $$ func */ coresyscharsansichars_clear
  (/* $$ out */ nonconst ansichar* /* $$ param */ ADest)
{
  (*ADest) = ANSINULLCHAR;
} // func

/* $$ inline */ void /* $$ func */ coresyscharsansichars_assign
  (/* $$ out */ nonconst ansichar* /* $$ param */ ADest,
   /* $$ in */  const    ansichar  /* $$ param */ ASource)
{
  *ADest = ASource;
} // func
   
// ------------------

/* operators */

/* $$ inline */ enum comparison /* $$ func */ coresyscharsansichars_compare
  (/* $$ in */ const ansichar /* $$ param */ A,
   /* $$ in */ const ansichar /* $$ param */ B)
{
  enum comparison /* $$ var */ Result = comparison_equal;
  // ---
  
  if (A < B)
  {
    Result = comparison_lesser;
  }
  else if (A > B)
  {
    Result = comparison_greater;
  }
  else
  {
    Result = comparison_equal;
  }

  // ---
  return Result;
} // func
   
bool /* $$ func */ coresyscharsansichars_equal
  (/* $$ in */ const ansichar /* $$ param */ A,
   /* $$ in */ const ansichar /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
  
  Result =
    (A == B);

  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharsansichars_different
  (/* $$ in */ const ansichar /* $$ param */ A,
   /* $$ in */ const ansichar /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
  
  Result =
    (A != B);

  // ---
  return Result;
} // func
   
// ------------------

/* rtti */ 

bool /* $$ func */ coresyscharsansichars_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = (ASource == (pointer*) ANSINULLCHAR);
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresyscharsansichars_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ ADest)
{
  bool /* $$ var */ Result = false;
  // ---
  
  Result = (*ADest != NULL);
  if (Result)
  {
    (*ADest) = ANSINULLCHAR;
  }
     
  // ---
  return Result;
} // func

void /* $$ func */ coresyscharsansichars_rttiapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ ADest)
{
  /* discard */ coresyscharsansichars_rttitryapplydefaultvalue
    (ADest);
} // func

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresyscharsansichars_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresyscharsansichars_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // $$ namespace coresyscharsansichars