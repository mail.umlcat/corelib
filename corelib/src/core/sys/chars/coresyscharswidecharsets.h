/** Module: "coresyscharswidecharsets.h"
 ** Descr.: "Wide / Long Character Encoded Sets Library,"
 **         "implemented with NULL marker terminated strings."
 **/

// $$ namespace coresyscharswidecharsets
// $$ {
 
// ------------------
 
#ifndef CORESYSCHARSWIDECHARSETS_H
#define CORESYSCHARSWIDECHARSETS_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

// --> same package libraries here
#include "coresyscharswidechars.h"

// ------------------

/* functions */

/* $$ inline */ bool /* $$ func */ coresyscharswidecharsets_isempty
  (/* $$ in */ const widecharset* /* $$ param */ ASet);
  
// ------------------

/* procedures */

// ------------------

/* operators */

// ------------------

/* rtti */

 
// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresyscharswidecharsets_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresyscharswidecharsets_finish
  ( noparams );

// ------------------

#endif // CORESYSCHARSWIDECHARSETS_H

// $$ } // $$ namespace coresyscharswidecharsets