/** Module: "coresyschars.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresyschars
// $$ {
 
// ------------------
 
#ifndef CORESYSCHARS_H
#define CORESYSCHARS_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

// --> contained modules here
#include "coresyscharsansichars.h"
#include "coresyscharswidechars.h"
#include "coresyscharsansicharsets.h"
#include "coresyscharswidecharsets.h"

// ------------------

/* rtti */

#define MOD_coresyschars
// $$ {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresyschars_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresyschars_finish
  ( noparams );

// ------------------

#endif // CORESYSCHARS_H

// ------------------

// $$ } // namespace coresyschars