/** Module: "coresyscharsansichars.h"
 ** Descr.: "Individual A.N.S.I. Character"
 **         "operations library."
 **/

// $$ namespace coresyscharsansichars
// $$ {
 
// ------------------
 
#ifndef CORESYSCHARSANSICHARS_H
#define CORESYSCHARSANSICHARS_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
#include <ctype.h>
//.include <cwtype.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

/* types */

typedef
  ansichar /* $$ as */ coresyscharsansichars_ansichar;

// ------------------

/* functions */

ansichar /* $$ func */ coresyscharsansichars_chartolower
  (/* $$ in */ const ansichar /* $$ param */ AValue);

ansichar /* $$ func */ coresyscharsansichars_chartoupper
  (/* $$ in */ const ansichar /* $$ param */ AValue);

ansichar /* $$ func */ coresyscharsansichars_chartorevcase
  (/* $$ in */ const ansichar /* $$ param */ AValue);

// ------------------

/* $$ inline */ byte_t /* $$ func */ coresyscharsansichars_chartodec
  (/* $$ in */ const ansichar /* $$ param */ AValue);

/* $$ inline */ ansichar /* $$ func */ coresyscharsansichars_dectochar
  (/* $$ in */ const byte_t /* $$ param */ AValue);

// ------------------

/* $$ inline */ bool /* $$ func */ coresyscharsansichars_isnullmarker
  (/* $$ in */ const ansichar /* $$ param */ AValue);

bool /* $$ func */ coresyscharsansichars_islowercase
  (/* $$ in */ const ansichar /* $$ param */ AValue);
 
bool /* $$ func */ coresyscharsansichars_isuppercase
  (/* $$ in */ const ansichar /* $$ param */ AValue);

bool /* $$ func */ coresyscharsansichars_isspace
  (/* $$ in */ const ansichar /* $$ param */ AValue);

bool /* $$ func */ coresyscharsansichars_isblank
  (/* $$ in */ const ansichar /* $$ param */ AValue);

bool /* $$ func */ coresyscharsansichars_isdigit
  (/* $$ in */ const ansichar /* $$ param */ AValue);
  
bool /* $$ func */ coresyscharsansichars_isalpha
  (/* $$ in */ const ansichar /* $$ param */ AValue);
  
bool /* $$ func */ coresyscharsansichars_isalphanum
  (/* $$ in */ const ansichar /* $$ param */ AValue);
  
bool /* $$ func */ coresyscharsansichars_ispunct
  (/* $$ in */ const ansichar /* $$ param */ AValue);
  
bool /* $$ func */ coresyscharsansichars_iscontrol
  (/* $$ in */ const ansichar /* $$ param */ AValue);
  
bool /* $$ func */ coresyscharsansichars_isgraph
  (/* $$ in */ const ansichar /* $$ param */ AValue);

// ------------------

bool /* $$ func */ coresyscharsansichars_isid
  (/* $$ in */ const ansichar /* $$ param */ AValue);

// ------------------

/* procedures */

/* $$ inline */ void /* $$ func */ coresyscharsansichars_clear
  (/* $$ out */ nonconst ansichar* /* $$ param */ ADest);

/* $$ inline */ void /* $$ func */ coresyscharsansichars_assign
  (/* $$ out */ nonconst ansichar* /* $$ param */ ADest,
   /* $$ in */  const    ansichar  /* $$ param */ ASource);
  
// ------------------

/* operators */

/* $$ inline */ enum comparison /* $$ func */ coresyscharsansichars_compare
  (/* $$ in */ const ansichar /* $$ param */ A,
   /* $$ in */ const ansichar /* $$ param */ B);
   
bool /* $$ func */ coresyscharsansichars_equal
  (/* $$ in */ const ansichar /* $$ param */ A,
   /* $$ in */ const ansichar /* $$ param */ B);
   
bool /* $$ func */ coresyscharsansichars_different
  (/* $$ in */ const ansichar /* $$ param */ A,
   /* $$ in */ const ansichar /* $$ param */ B);
   
// ------------------

/* rtti */ 

bool /* $$ func */ coresyscharsansichars_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ ASource);

bool /* $$ func */ coresyscharsansichars_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ ADest);

void /* $$ func */ coresyscharsansichars_rttiapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ ADest);

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresyscharsansichars_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresyscharsansichars_finish
  ( noparams );

// ------------------

#endif // CORESYSCHARSANSICHARS_H

// $$ } // $$ namespace coresyscharsansichars