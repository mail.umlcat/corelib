/** Module: "coresyscharswidecharptrs.h"
 ** Descr.: "Wide / Long Character Encoding"
 **         "Individual characters, accessed by pointer,"
 **         "operations library."
 **/

// $$ namespace coresyscharswidecharptrs
// $$ {
 
// ------------------
 
#ifndef CORESYSCHARSWIDECHARPTRS_H
#define CORESYSCHARSWIDECHARPTRS_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

// --> same package libraries here
#include "coresyscharswidechars.h"

// ------------------

/* functions */



// ------------------

/* procedures */



// ------------------

/* operators */



// ------------------

/* rtti */



// ------------------

//typedef
//  sometype         /* $$ as */ sometype;
   
// ------------------

//struct failure
//{
//  widechar /* $$ var */ Mesage;
//  pointer* /* $$ var */ Parent;
//} ;

// ------------------

//pointer* /* $$ func */ coresyscharswidecharptrs_malloc
//  (/* $$ in */ size_t /* $$ param */ ASize);
  
//void /* $$ func */ coresyscharswidecharptrs_dealloc
//  (/* $$ out */ pointer** /* $$ param */ ADest,
//   /* $$ in */  size_t    /* $$ param */ ASize);

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresyscharswidecharptrs_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresyscharswidecharptrs_finish
  ( noparams );

// ------------------

#endif // CORESYSCHARSWIDECHARPTRS_H

// $$ } // $$ namespace coresyscharswidecharptrs