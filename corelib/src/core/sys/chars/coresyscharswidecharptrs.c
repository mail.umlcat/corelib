/** Module: "coresyscharswidecharptrs.c"
 ** Descr.: "Wide / Long Character Encoding"
 **         "Individual characters, accessed by pointer,"
 **         "operations library."
 **/

// $$ namespace coresyscharswidecharptrs
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

// --> same package libraries here
#include "coresyscharswidechars.h"

// ------------------

// --> this module own header goes here
#include "coresyscharswidecharptrs.h"
 
// ------------------

/* functions */



// ------------------

/* procedures */



// ------------------

/* operators */



// ------------------

/* rtti */


   
// ------------------

 
 // ...

// ------------------

/* $$ override */ int /* $$ func */ coresyscharswidecharptrs_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresyscharswidecharptrs_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // $$ namespace coresyscharswidecharptrs