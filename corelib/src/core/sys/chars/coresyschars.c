/** Module: "coresyschars.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresyschars
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

// --> contained modules here
#include "coresyscharsansichars.h"
#include "coresyscharswidechars.h"
#include "coresyscharsansicharsets.h"
#include "coresyscharswidecharsets.h"

// ------------------

// --> this same module header goes here
#include "coresyschars.h"
 
// ------------------


// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresyschars_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresyschars_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresyschars