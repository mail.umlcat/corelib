/** Module: "coresyscharswidecharsets.c"
 ** Descr.: "Wide / Long Character Encoded Sets Library,"
 **         "implemented with NULL marker terminated strings."
 **/

// $$ namespace coresyscharswidecharsets
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

// --> same package libraries here
#include "coresyscharswidechars.h"

// ------------------

// --> this same module header goes here
#include "coresyscharswidecharsets.h"

// ------------------

/* functions */

/* $$ inline */ bool /* $$ func */ coresyscharswidecharsets_isempty
  (/* $$ in */ const widecharset* /* $$ param */ ASet)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result =
    ((ASet == NULL) || ((*ASet)[0] == WIDENULLCHAR));

  // ---
  return Result;
} // func

// ------------------

/* procedures */

// ------------------

/* operators */

// ------------------

/* rtti */

 
// ------------------

 // ...

// ------------------

/* $$ override */ int /* $$ func */ coresyscharswidecharsets_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresyscharswidecharsets_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // $$ namespace coresyscharswidecharsets