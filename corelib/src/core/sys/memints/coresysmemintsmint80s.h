/** Module: "coresysmemintsmint80s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace  coresysmemintsmint80s
// $$ {
 
// ------------------
 
#ifndef CORESYSMEMINTSMINT80S_H
#define CORESYSMEMINTSMINT80S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"


// ------------------

/* functions */

bool /* func*/ coresysmemintsmint80s_isempty
  (/* in */ const memint80_t /* $$ param */ ASource);


// ------------------

/* procedures */

void /* $$ func */ coresysmemintsmint80s_clear
  (/* $$ inout */ memint80_t /* $$ param */ ADest);
  
void /* $$ func */ coresysmemintsmint80s_assign
  (/* $$ inout */ memint80_t* /* $$ param */ ADest,
   /* in */    const memint80_t  /* $$ param */ ASource);
   
// ------------------

/* operators */

bool /* $$ func */ coresysmemintsmint80s_equal
  (/* in */ const memint80_t /* $$ param */ A,
   /* in */ const memint80_t /* $$ param */ B);



bool /* $$ func */ coresysmemintsmint80s_different
  (/* in */ const memint80_t /* $$ param */ A,
   /* in */ const memint80_t /* $$ param */ B);


// ------------------

bool /* $$ func */ coresysmemintsmint80s_rttiisdefaultvalue
  (/* in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysmemintsmint80s_rttitryapplydefaultvalue
  (/* $$ inout */ pointer* /* $$ param */ AValue);

void /* $$ func */ coresysmemintsmint80s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysmemintsmint80s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysmemintsmint80s_finish
  ( noparams );

// ------------------

#endif // CORESYSMEMINTSMINT80S_H

// $$ } // namespace  coresysmemintsmint80s