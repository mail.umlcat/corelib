/** Module: "coresysmemintsmint128s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace  coresysmemintsmint128s
// $$ {
 
// ------------------
 
#ifndef CORESYSMEMINTSMINT128S_H
#define CORESYSMEMINTSMINT128S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

/* functions */

bool /* func*/ coresysmemintsmint128s_isempty
  (/* in */ const memint128_t /* $$ param */ ASource);

// ------------------

/* procedures */

void /* $$ func */ coresysmemintsmint128s_clear
  (/* $$ inout */ memint128_t /* $$ param */ ADest);
  
void /* $$ func */ coresysmemintsmint128s_assign
  (/* $$ inout */ memint128_t* /* $$ param */ ADest,
   /* in */    const memint128_t  /* $$ param */ ASource);
   
// ------------------

/* operators */

bool /* $$ func */ coresysmemintsmint128s_equal
  (/* in */ const memint128_t /* $$ param */ A,
   /* in */ const memint128_t /* $$ param */ B);


bool /* $$ func */ coresysmemintsmint128s_different
  (/* in */ const memint128_t /* $$ param */ A,
   /* in */ const memint128_t /* $$ param */ B);
   
// ------------------

bool /* $$ func */ coresysmemintsmint128s_rttiisdefaultvalue
  (/* in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysmemintsmint128s_rttitryapplydefaultvalue
  (/* $$ inout */ pointer* /* $$ param */ AValue);

void /* $$ func */ coresysmemintsmint128s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysmemintsmint128s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysmemintsmint128s_finish
  ( noparams );

// ------------------

#endif // CORESYSMEMINTSMINTS128S_H

// $$ } // namespace  coresysmemintsmint128s