/** Module: "coresysmemintsmint16s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace  coresysmemintsmint16s
// $$ {
 
// ------------------
 
#ifndef CORESYSMEMINTSMINT16S_H
#define CORESYSMEMINTSMINT16S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"


// ------------------

/* functions */

bool /* func*/ coresysmemintsmint16s_isempty
  (/* in */ const memint16_t /* $$ param */ ASource);


// ------------------

/* procedures */

void /* $$ func */ coresysmemintsmint16s_clear
  (/* $$ inout */ nonconst memint16_t* /* $$ param */ ADest);
  
void /* $$ func */ coresysmemintsmint16s_assign
  (/* $$ inout */ memint16_t* /* $$ param */ ADest,
   /* in */    const memint16_t  /* $$ param */ ASource);
   
// ------------------

/* operators */

bool /* $$ func */ coresysmemintsmint16s_equal
  (/* in */ const memint16_t /* $$ param */ A,
   /* in */ const memint16_t /* $$ param */ B);


bool /* $$ func */ coresysmemintsmint16s_different
  (/* in */ const memint16_t /* $$ param */ A,
   /* in */ const memint16_t /* $$ param */ B);

  
// ------------------

void /* $$ func */ coresysmemintsmint16s_encode8to16
  (/* out */ nonconst memint16_t* /* $$ param */ ADest,
   /* in */  const    memint8_t   /* $$ param */ AHiValue,
   /* in */  const    memint8_t   /* $$ param */ ALoValue);
   
void /* $$ func */ coresysmemintsmint16s_decode16to8
  (/* in */  const    memint16_t /* $$ param */ ASource,
   /* out */ nonconst memint8_t* /* $$ param */ AHiValue,
   /* out */ nonconst memint8_t* /* $$ param */ ALoValue);



// ------------------

bool /* $$ func */ coresysmemintsmint16s_rttiisdefaultvalue
  (/* in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysmemintsmint16s_rttitryapplydefaultvalue
  (/* $$ inout */ pointer* /* $$ param */ AValue);

void /* $$ func */ coresysmemintsmint16s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);


 
// ------------------

/* $$ override */ int /* $$ func */ coresysmemintsmint16s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysmemintsmint16s_finish
  ( noparams );

// ------------------

#endif // CORESYSMEMINTSMINT16S_H

// $$ } // namespace  coresysmemintsmint16s