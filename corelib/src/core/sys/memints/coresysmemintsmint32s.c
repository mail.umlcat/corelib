/** Module: "coresysmemintsmint32s.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace  coresysmemintsmint32s
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

// --> this module own header goes here
#include "coresysmemintsmint32s.h"
 

// ------------------

/* functions */

bool /* func*/ coresysmemintsmint32s_isempty
  (/* in */ const memint32_t /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
  
  Result = (ASource == 0);
     
  // ---
  return Result;
} // func

// ------------------

/* procedures */

void /* $$ func */ coresysmemintsmint32s_clear
  (/* $$ inout */ memint32_t* /* $$ param */ ADest)
{
  if (ADest != NULL)
  {
    *ADest = 0;
  } // if
} // func

void /* $$ func */ coresysmemintsmint32s_assign
  (/* $$ inout */ memint32_t* /* $$ param */ ADest,
   /* in */    const memint32_t  /* $$ param */ ASource)
{
  *ADest = ASource;
} // func 

// ------------------

/* operators */

bool /* $$ func */ coresysmemintsmint32s_equal
  (/* in */ const memint32_t /* $$ param */ A,
   /* in */ const memint32_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = (A == B);
     
  // ---
  return Result;
} // func


bool /* $$ func */ coresysmemintsmint32s_different
  (/* in */ const memint32_t /* $$ param */ A,
   /* in */ const memint32_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

void /* $$ func */ coresysmemintsmint32s_encode8to32
  (/* out */ nonconst memint32_t* /* $$ param */ ADest,
   /* in */  const    memint8_t   /* $$ param */ A,
   /* in */  const    memint8_t   /* $$ param */ B,
   /* in */  const    memint8_t   /* $$ param */ C,
   /* in */  const    memint8_t   /* $$ param */ D)
{
  if (ADest != NULL)
  {
    //*ADest = ((AHiValue << 16) | (ALoValue));
  } // if
} // func

void /* $$ func */ coresysmemintsmint32s_encode16to32
  (/* out */ nonconst memint32_t* /* $$ param */ ADest,
   /* in */  const    memint16_t  /* $$ param */ AHiValue,
   /* in */  const    memint16_t  /* $$ param */ ALoValue)
{
  if (ADest != NULL)
  {
    *ADest = ((AHiValue << 16) | (ALoValue));
  } // if
} // func

void /* $$ func */ coresysmemintsmint32s_decode32to16
  (/* in */  const    memint32_t  /* $$ param */ ASource,
   /* out */ nonconst memint16_t* /* $$ param */ AHiValue,
   /* out */ nonconst memint16_t* /* $$ param */ ALoValue)
{
  bool /* $$ var */ CanContinue = false;

  CanContinue =
    (AHiValue != NULL) &&
    (ALoValue != NULL);
  if (CanContinue)
  {
    *AHiValue = ((ASource >> 16) & 0xffff0000);
    *ALoValue = (ASource & 0x0000ffff);
  } // if
} // func

void /* $$ func */ coresysmemintsmint32s_decode32to8
  (/* in */  const    memint32_t  /* $$ param */ ASource,
   /* out */ nonconst memint16_t* /* $$ param */ A,
   /* out */ nonconst memint16_t* /* $$ param */ B,
   /* out */ nonconst memint16_t* /* $$ param */ C,
   /* out */ nonconst memint16_t* /* $$ param */ D)
{
  bool /* $$ var */ CanContinue = false;
  CanContinue =
    (A != NULL) &&
    (B != NULL) &&
    (C != NULL) &&
    (D != NULL);
  if (CanContinue)
  {
//    *AHiValue = ((ASource >> 16) & 0xffff0000);
//    *ALoValue = (ASource & 0x0000ffff);
  } // if
} // func


// ------------------

bool /* $$ func */ coresysmemintsmint32s_rttiisdefaultvalue
  (/* in */ const pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result =
    (AValue != NULL);
  if (Result)
  {

  } // if
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysmemintsmint32s_rttitryapplydefaultvalue
  (/* $$ inout */ pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result =
    (AValue != NULL);
  if (Result)
  {

  } // if
     
  // ---
  return Result;
} // func

void /* $$ func */ coresysmemintsmint32s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue)
{
  coresysbasefuncs_nothing();
} // func


// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysmemintsmint32s_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysmemintsmint32s_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace  coresysmemintsmint32s