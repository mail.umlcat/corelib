/** Module: "coresysmemintsmint64s.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace  coresysmemintsmint64s
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

// --> this module own header goes here
#include "coresysmemintsmint64s.h"
 

// ------------------

/* functions */

bool /* func*/ coresysmemintsmint64s_isempty
  (/* in */ const memint64_t /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* procedures */

void /* $$ func */ coresysmemintsmint64s_clear
  (/* $$ inout */ memint64_t /* $$ param */ ADest)
{
  coresysbasefuncs_nothing();
} // func

void /* $$ func */ coresysmemintsmint64s_assign
  (/* $$ inout */ memint64_t* /* $$ param */ ADest,
   /* in */    const memint64_t  /* $$ param */ ASource)
{
  coresysbasefuncs_nothing();
} // func 

// ------------------

/* operators */

bool /* $$ func */ coresysmemintsmint64s_equal
  (/* in */ const memint64_t /* $$ param */ A,
   /* in */ const memint64_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func


bool /* $$ func */ coresysmemintsmint64s_different
  (/* in */ const memint64_t /* $$ param */ A,
   /* in */ const memint64_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

void /* $$ func */ coresysmemintsmint64s_encode8to64
  (/* out */ nonconst memint64_t* /* $$ param */ ADest,
   /* in */  const    memint8_t*  /* $$ param */ ASource)
{
  bool /* $$ var */ CanContinue = false;

  CanContinue =
    (ADest != NULL) &&
    (ASource != NULL);
  if (CanContinue)
  {
//    *AHiValue = ((ASource >> 16) & 0xffff0000);
//    *ALoValue = (ASource & 0x0000ffff);
  } // if
} // func

void /* $$ func */ coresysmemintsmint64s_encode32to64
  (/* out */ nonconst memint64_t* /* $$ param */ ADest,
   /* in */  const    memint32_t  /* $$ param */ AHiValue,
   /* in */  const    memint32_t  /* $$ param */ ALoValue)
{
  bool /* $$ var */ CanContinue = false;

//  CanContinue =
//    (AHiValue != NULL) &&
//    (ALoValue != NULL);
  if (CanContinue)
  {
//    *AHiValue = ((ASource >> 16) & 0xffff0000);
//    *ALoValue = (ASource & 0x0000ffff);
  } // if
} // func

void /* $$ func */ coresysmemintsmint64s_decode64to8
  (/* in */  const    memint64_t /* $$ param */ ASource,
   /* out */ nonconst memint8_t* /* $$ param */ ADest)
{
  bool /* $$ var */ CanContinue = false;

  CanContinue =
    (ADest != NULL);
  if (CanContinue)
  {
//    *AHiValue = ((ASource >> 16) & 0xffff0000);
//    *ALoValue = (ASource & 0x0000ffff);
  } // if
} // func

void /* $$ func */ coresysmemintsmint64s_decode64to32
  (/* in */  const    memint64_t  /* $$ param */ ASource,
   /* out */ nonconst memint32_t* /* $$ param */ AHiValue,
   /* out */ nonconst memint32_t* /* $$ param */ ALoValue)
{
  bool /* $$ var */ CanContinue = false;

  CanContinue =
    (AHiValue != NULL) &&
    (ALoValue != NULL);
  if (CanContinue)
  {
//    *AHiValue = ((ASource >> 16) & 0xffff0000);
//    *ALoValue = (ASource & 0x0000ffff);
  } // if
} // func


// ------------------

bool /* $$ func */ coresysmemintsmint64s_rttiisdefaultvalue
  (/* in */ const pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysmemintsmint64s_rttitryapplydefaultvalue
  (/* $$ inout */ pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* $$ func */ coresysmemintsmint64s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue)
{
  coresysbasefuncs_nothing();
} // func

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysmemintsmint64s_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysmemintsmint64s_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------


 // ...

// $$ } // namespace  coresysmemintsmint64s