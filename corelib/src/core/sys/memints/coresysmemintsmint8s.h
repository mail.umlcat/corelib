/** Module: "coresysmemintsmint8s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace  coresysmemintsmint8s
// $$ {
 
// ------------------
 
#ifndef CORESYSMEMINTSMINT8S_H
#define CORESYSMEMINTSMINT8S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"


// ------------------

/* functions */

bool /* func*/ coresysmemintsmint8s_isempty
  (/* in */ const memint8_t /* $$ param */ ASource);


// ------------------

/* procedures */

void /* $$ func */ coresysmemintsmint8s_clear
  (/* $$ inout */ nonconst memint8_t* /* $$ param */ ADest);
  
void /* $$ func */ coresysmemintsmint8s_assign
  (/* $$ inout */ nonconst memint8_t* /* $$ param */ ADest,
   /* in */    const    memint8_t  /* $$ param */ ASource);
  
// ------------------

/* operators */

bool /* $$ func */ coresysmemintsmint8s_equal
  (/* in */ const memint8_t /* $$ param */ A,
   /* in */ const memint8_t /* $$ param */ B);

bool /* $$ func */ coresysmemintsmint8s_different
  (/* in */ const memint8_t /* $$ param */ A,
   /* in */ const memint8_t /* $$ param */ B);

// ------------------

bool /* $$ func */ coresysmemintsmint8s_rttiisdefaultvalue
  (/* in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysmemintsmint8s_rttitryapplydefaultvalue
  (/* $$ inout */ pointer* /* $$ param */ AValue);

void /* $$ func */ coresysmemintsmint8s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysmemintsmint8_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysmemintsmint8_finish
  ( noparams );

// ------------------

#endif // CORESYSMEMINTSMINT8S_H

// $$ } // namespace  coresysmemintsmint8s