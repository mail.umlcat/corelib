/** Module: "coresysmemintsmint256s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace  coresysmemintsmint256s
// $$ {
 
// ------------------
 
#ifndef CORESYSMEMINTSMINT256S_H
#define CORESYSMEMINTSMINT256S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"


// ------------------

/* functions */

bool /* func*/ coresysmemintsmint256s_isempty
  (/* in */ const memint256_t /* $$ param */ ASource);


// ------------------

/* procedures */


void /* $$ func */ coresysmemintsmint256s_clear
  (/* $$ inout */ nonconst memint256_t /* $$ param */ ADest);
  
void /* $$ func */ coresysmemintsmint256s_assign
  (/* $$ inout */ nonconst memint256_t* /* $$ param */ ADest,
   /* in */    const memint256_t  /* $$ param */ ASource);
   
// ------------------

/* operators */

bool /* $$ func */ coresysmemintsmint256s_equal
  (/* in */ const memint256_t /* $$ param */ A,
   /* in */ const memint256_t /* $$ param */ B);



bool /* $$ func */ coresysmemintsmint256s_different
  (/* in */ const memint256_t /* $$ param */ A,
   /* in */ const memint256_t /* $$ param */ B);


// ------------------

bool /* $$ func */ coresysmemintsmint256s_rttiisdefaultvalue
  (/* in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysmemintsmint256s_rttitryapplydefaultvalue
  (/* $$ inout */ pointer* /* $$ param */ AValue);

void /* $$ func */ coresysmemintsmint256s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysmemintsmint256s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysmemintsmint256s_finish
  ( noparams );

// ------------------

#endif // CORESYSMEMINTSMINTS256S_H

// $$ } // namespace  coresysmemintsmint256s