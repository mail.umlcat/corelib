/** Module: "coresysmemintsmint8s.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace  coresysmemintsmint8s
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

// --> this module own header goes here
#include "coresysmemintsmint8s.h"
 

// ------------------

/* functions */

bool /* func*/ coresysmemintsmint8s_isempty
  (/* in */ const memint8_t /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* procedures */

void /* $$ func */ coresysmemintsmint8s_clear
  (/* $$ inout */ nonconst memint8_t* /* $$ param */ ADest)
{
  coresysbasefuncs_nothing();
} // func

void /* $$ func */ coresysmemintsmint8s_assign
  (/* $$ inout */ nonconst memint8_t* /* $$ param */ ADest,
   /* in */    const    memint8_t  /* $$ param */ ASource)
{
  coresysbasefuncs_nothing();
} // func 
   
// ------------------

/* operators */

bool /* $$ func */ coresysmemintsmint8s_equal
  (/* in */ const memint8_t /* $$ param */ A,
   /* in */ const memint8_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysmemintsmint8s_different
  (/* in */ const memint8_t /* $$ param */ A,
   /* in */ const memint8_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

 

bool /* $$ func */ coresysmemintsmint8s_rttiisdefaultvalue
  (/* in */ const pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysmemintsmint8s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* $$ func */ coresysmemintsmint8s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue)
{
  coresysbasefuncs_nothing();
} // func

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysmemintsmint8_start
  ( noparams )
{
  coresysbasefuncs_nothing();
  
  return 0;
} // func

/* $$ override */ int /* $$ func */ coresysmemintsmint8_finish
  ( noparams )
{
  coresysbasefuncs_nothing();
  
  return 0;
} // func

// ------------------


 // ...

// $$ } // namespace  coresysmemintsmint8s