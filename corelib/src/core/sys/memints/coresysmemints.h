/** Module: "coresysmemints"
 ** Filename: "coresysmemints.h"
 ** Qualified Identifier:
 ** "core::system::memints" 
 ** Descr.:
 ** "System Package Module"
 **/

// $$ namespace  coresysmemints
// $$ {
 
// ------------------
 
#ifndef CORESYSMEMINTS_H
#define CORESYSMEMINTS_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> outer corelib packages here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

// --> inner corelib package libraries here

#include "coresysmemintsmint8s.h"
#include "coresysmemintsmint16s.h"
#include "coresysmemintsmint32s.h"
#include "coresysmemintsmint64s.h"
#include "coresysmemintsmint128s.h"
#include "coresysmemintsmint256s.h"
#include "coresysmemintsmint24s.h"
#include "coresysmemintsmint48s.h"
#include "coresysmemintsmint80s.h"

// ---

#include "coresysmemintsmint8byptrs.h"

// ------------------

// --> other NON "corelib" libraries here
// #include <put your library here>
// #include <put your library here>
// #include <put your library here>

// ------------------

#define MOD_coresysmemints
// $$ {0x02,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------




 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysmemints_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysmemints_finish
  ( noparams );

// ------------------

#endif // CORESYSMEMINTS_H

// ------------------

// $$ } // namespace coresysmemints