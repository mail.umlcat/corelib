/** Module: "coresysmemintstypes"
 ** Filename: "coresysmemintstypes.h"
 ** Qualified Identifier:
 ** "core::system::memints" 
 ** Descr.:
 ** "System Package Module"
 **/

// $$ namespace  coresysmemintstypes
// $$ {
 
// ------------------
 
#ifndef CORESYSMEMINTSTYPES_H
#define CORESYSMEMINTSTYPES_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> outer corelib packages here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

// --> inner corelib package libraries here

#include "coresysmemintsmint8s.h"
#include "coresysmemintsmint16s.h"
#include "coresysmemintsmint32s.h"
#include "coresysmemintsmint64s.h"
#include "coresysmemintsmint128s.h"
#include "coresysmemintsmint256s.h"
#include "coresysmemintsmint24s.h"
#include "coresysmemintsmint48s.h"
#include "coresysmemintsmint80s.h"

// ------------------

// --> other NON "corelib" libraries here
// #include <put your library here>
// #include <put your library here>
// #include <put your library here>

// ------------------

#define MOD_coresysmemintstypes
// $$ {0x02,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

enum memoryintegers
{
  memoryintegers_memint8_t,    
  memoryintegers_memint16_t,   
  memoryintegers_memint32_t,   
  memoryintegers_memint64_t,   
  memoryintegers_memint128_t,  
  memoryintegers_memint256_t,
  memoryintegers_memint24_t,
  memoryintegers_memint48_t,
  memoryintegers_memint80_t,
} ;

typedef
  enum memoryintegers /* as */ coresysmemintstypes_memoryintegers;


// ------------------




 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysmemintstypes_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysmemintstypes_finish
  ( noparams );

// ------------------

#endif // CORESYSMEMINTSTYPES_H

// $$ } // namespace  coresysmemintstypes