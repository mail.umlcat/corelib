/** Module: "coresysmemintsmint24s.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace  coresysmemintsmint24s
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

// --> this module own header goes here
#include "coresysmemintsmint24s.h"
 

// ------------------

/* functions */

bool /* func*/ coresysmemintsmint24s_isempty
  (/* in */ const memint24_t /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* procedures */

void /* $$ func */ coresysmemintsmint24s_clear
  (/* $$ inout */ memint24_t /* $$ param */ ADest)
{
  coresysbasefuncs_nothing();
} // func

void /* $$ func */ coresysmemintsmint24s_assign
  (/* $$ inout */ memint24_t* /* $$ param */ ADest,
   /* in */    const memint24_t  /* $$ param */ ASource)
{
  coresysbasefuncs_nothing();
} // func 

// ------------------

/* operators */

bool /* $$ func */ coresysmemintsmint24s_equal
  (/* in */ const memint24_t /* $$ param */ A,
   /* in */ const memint24_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func


bool /* $$ func */ coresysmemintsmint24s_different
  (/* in */ const memint24_t /* $$ param */ A,
   /* in */ const memint24_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

 

bool /* $$ func */ coresysmemintsmint24s_rttiisdefaultvalue
  (/* in */ const pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysmemintsmint24s_rttitryapplydefaultvalue
  (/* $$ inout */ pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* $$ func */ coresysmemintsmint24s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue)
{
  coresysbasefuncs_nothing();
} // func

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysmemintsmint24_start
  ( noparams )
{
  coresysbasefuncs_nothing();
  
  return 0;
} // func

/* $$ override */ int /* $$ func */ coresysmemintsmint24_finish
  ( noparams )
{
  coresysbasefuncs_nothing();
  
  return 0;
} // func

// ------------------


 // ...

// $$ } // namespace  coresysmemintsmint24s