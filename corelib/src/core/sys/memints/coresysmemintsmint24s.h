/** Module: "coresysmemintsmint24s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace  coresysmemintsmint24s
// $$ {
 
// ------------------
 
#ifndef CORESYSMEMINTSMINT24S_H
#define CORESYSMEMINTSMINT24S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"


// ------------------

/* functions */

bool /* func*/ coresysmemintsmint24s_isempty
  (/* in */ const memint24_t /* $$ param */ ASource);


// ------------------

/* procedures */

void /* $$ func */ coresysmemintsmint24s_clear
  (/* $$ inout */ memint24_t /* $$ param */ ADest);
  
void /* $$ func */ coresysmemintsmint24s_assign
  (/* $$ inout */ memint24_t* /* $$ param */ ADest,
   /* in */    const memint24_t  /* $$ param */ ASource);
   
// ------------------

/* operators */

bool /* $$ func */ coresysmemintsmint24s_equal
  (/* in */ const memint24_t /* $$ param */ A,
   /* in */ const memint24_t /* $$ param */ B);


bool /* $$ func */ coresysmemintsmint24s_different
  (/* in */ const memint24_t /* $$ param */ A,
   /* in */ const memint24_t /* $$ param */ B);
   
// ------------------

bool /* $$ func */ coresysmemintsmint24s_rttiisdefaultvalue
  (/* in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysmemintsmint24s_rttitryapplydefaultvalue
  (/* $$ inout */ pointer* /* $$ param */ AValue);

void /* $$ func */ coresysmemintsmint24s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysmemintsmint24_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysmemintsmint24_finish
  ( noparams );

// ------------------

#endif // CORESYSMEMINTSMINT24S_H

// $$ } // namespace  coresysmemintsmint24s