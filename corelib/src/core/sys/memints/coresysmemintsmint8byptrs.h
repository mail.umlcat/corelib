/** Module: "coresysmemintsmint8byptrs.h"
 ** Descr.: "..."
 **/
 
// $$ namespace  coresysmemintsmint8byptrs
// $$ {
 
// ------------------
 
#ifndef CORESYSMEMINTSMINT8BYPTRS_H
#define CORESYSMEMINTSMINT8BYPTRS_H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

/* functions */


bool /* func*/ coresysmemintsmint8byptrs_isempty
  (/* in */ const pointer* /* $$ param */ ASource);

pointer* /* func*/ coresysmemintsmint8byptrs_consttoptr
  (/* in */ const memint8_t /* $$ param */ ASource);
  
// ------------------

/* procedures */

void /* $$ func */ coresysmemintsmint8byptrs_clear
  (/* $$ inout */ nonconst pointer* /* $$ param */ ADest);

void /* $$ func */ coresysmemintsmint8byptrs_assign
  (/* $$ inout */ nonconst pointer** /* $$ param */ ADest,
   /* in */    const    pointer*  /* $$ param */ ASource);

void /* $$ func */ coresysmemintsmint8byptrs_dropptr
  (/* $$ inout */ nonconst pointer** /* $$ param */ ADest);
   
// ------------------

/* operators */

enum comparison /* $$ func */ coresysmemintsmint8byptrs_compare
  (/* in */ const pointer* /* $$ param */ A,
   /* in */ const pointer* /* $$ param */ B);

bool /* $$ func */ coresysmemintsmint8byptrs_equal
  (/* in */ const pointer* /* $$ param */ A,
   /* in */ const pointer* /* $$ param */ B);

bool /* $$ func */ coresysmemintsmint8byptrs_different
  (/* in */ const pointer* /* $$ param */ A,
   /* in */ const pointer* /* $$ param */ B);
   
// ------------------

/* rtti */

#define MOD_coresysmemintsmint8byptrs
// $$ {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};


// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysmemintsmint8byptrs_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysmemintsmint8byptrs_finish
  ( noparams );

// ------------------

#endif // CORESYSMEMINTSMINT8BYPTRS_H

// $$ } // namespace  coresysmemintsmint8byptrs