/** Module: "coresysmemintsmint8byptrs.c"
 ** Descr.: "Unsigned 8 Bits Integer Library."
 **/

// $$ namespace  coresysmemintsmint8byptrs
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

// --> this module own header goes here
#include "coresysmemintsmint8byptrs.h"
 
// ------------------

/* functions */

bool /* func*/ coresysmemintsmint8byptrs_isempty
  (/* in */ const pointer* /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

pointer* /* func*/ coresysmemintsmint8byptrs_consttoptr
  (/* in */ const memint8_t /* $$ param */ ASource)
{
  pointer* /* $$ var */ Result = NULL;
  // ---
     
  memint8_t* /* $$ var */ S = NULL;
  
  // ---
    
  S =
    (memint8_t*) malloc(sizeof(memint8_t));
  *S = ASource;
  Result =
    (pointer*) S;
     
  // ---
  return Result;
} // func

// ------------------

/* procedures */

void /* $$ func */ coresysmemintsmint8byptrs_clear
  (/* $$ inout */ nonconst pointer* /* $$ param */ ADest)
{
  coresysbasefuncs_nothing();
} // func

void /* $$ func */ coresysmemintsmint8byptrs_assign
  (/* $$ inout */ nonconst pointer** /* $$ param */ ADest,
   /* in */    const    pointer*  /* $$ param */ ASource)
{
  coresysbasefuncs_nothing();
} // func 

void /* $$ func */ coresysmemintsmint8byptrs_dropptr
  (/* $$ inout */ nonconst pointer** /* $$ param */ ADest)
{
  free(*ADest);
  ADest = NULL;
} // func 
  
// ------------------

/* operators */

enum comparison /* $$ func */ coresysmemintsmint8byptrs_compare
  (/* in */ const pointer* /* $$ param */ A,
   /* in */ const pointer* /* $$ param */ B)
{
  enum comparison /* $$ var */ Result = comparison_equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysmemintsmint8byptrs_equal
  (/* in */ const pointer* /* $$ param */ A,
   /* in */ const pointer* /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func


bool /* $$ func */ coresysmemintsmint8byptrs_different
  (/* in */ const pointer* /* $$ param */ A,
   /* in */ const pointer* /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
  
  memint8_t* /* $$ var */ C = NULL;
  memint8_t* /* $$ var */ D = NULL;
  
  C = (memint8_t*) A;
  D = (memint8_t*) B;
  
  Result =
    (*C != *D);
     
  // ---
  return Result;
} // func

// ------------------



// ------------------



 // ...

// ------------------

/* $$ override */ int /* $$ func */ coresysmemintsmint8byptrs_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysmemintsmint8byptrs_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------


 // ...

// $$ } // namespace  coresysmemintsmint8byptrs