/** Module: "coresysmemintsmint48s.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace  coresysmemintsmint48s
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

// --> this module own header goes here
#include "coresysmemintsmint48s.h"
 

// ------------------

/* functions */

bool /* func*/ coresysmemintsmint48s_isempty
  (/* in */ const memint48_t /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* procedures */

void /* $$ func */ coresysmemintsmint48s_clear
  (/* $$ inout */ memint48_t /* $$ param */ ADest)
{
  coresysbasefuncs_nothing();
} // func

void /* $$ func */ coresysmemintsmint48s_assign
  (/* $$ inout */ memint48_t* /* $$ param */ ADest,
   /* in */    const memint48_t  /* $$ param */ ASource)
{
  coresysbasefuncs_nothing();
} // func 

// ------------------

/* operators */

bool /* $$ func */ coresysmemintsmint48s_equal
  (/* in */ const memint48_t /* $$ param */ A,
   /* in */ const memint48_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func


bool /* $$ func */ coresysmemintsmint48s_different
  (/* in */ const memint48_t /* $$ param */ A,
   /* in */ const memint48_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func


// ------------------

void /* $$ func */ coresysmemintsmint48s_encodebytesto48
  (/* out */ nonconst memint48_t* /* $$ param */ ADest,
   /* in */  const    memint8_t*  /* $$ param */ ASource)
{
  bool /* $$ var */ CanContinue = false;

//  CanContinue =
//    (AHiValue != NULL) &&
//    (ALoValue != NULL);
  if (CanContinue)
  {
//    *AHiValue = ((ASource >> 16) & 0xffff0000);
//    *ALoValue = (ASource & 0x0000ffff);
  } // if
} // func

void /* $$ func */ coresysmemintsmint48s_encodewordsto48
  (/* out */ nonconst memint48_t* /* $$ param */ ADest,
   /* in */  const    memint16_t  /* $$ param */ ASource)
{
  bool /* $$ var */ CanContinue = false;

  CanContinue =
    (ADest != NULL);
  if (CanContinue)
  {
//    *AHiValue = ((ASource >> 16) & 0xffff0000);
//    *ALoValue = (ASource & 0x0000ffff);
  } // if
} // func

void /* $$ func */ coresysmemintsmint48s_decode48tobytes
  (/* in */  const    memint48_t /* $$ param */ ASource,
   /* out */ nonconst memint8_t* /* $$ param */ ADest)
{
  bool /* $$ var */ CanContinue = false;

  CanContinue =
    (ADest != NULL);
  if (CanContinue)
  {
//    *AHiValue = ((ASource >> 16) & 0xffff0000);
//    *ALoValue = (ASource & 0x0000ffff);
  } // if
} // func

void /* $$ func */ coresysmemintsmint48s_decode48towords
  (/* in */  const    memint48_t  /* $$ param */ ASource,
   /* out */ nonconst memint16_t* /* $$ param */ ADest)
{
  bool /* $$ var */ CanContinue = false;

  CanContinue =
    (ADest != NULL);
  if (CanContinue)
  {
//    *AHiValue = ((ASource >> 16) & 0xffff0000);
//    *ALoValue = (ASource & 0x0000ffff);
  } // if
} // func

// ------------------

bool /* $$ func */ coresysmemintsmint48s_rttiisdefaultvalue
  (/* in */ const pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysmemintsmint48s_rttitryapplydefaultvalue
  (/* $$ inout */ pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* $$ func */ coresysmemintsmint48s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue)
{
  coresysbasefuncs_nothing();
} // func

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysmemintsmint48s_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysmemintsmint48s_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------


 // ...

// $$ } // namespace  coresysmemintsmint48s