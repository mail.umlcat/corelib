/** Module: "coresysmemintsmint32s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace  coresysmemintsmint32s
// $$ {
 
// ------------------
 
#ifndef CORESYSMEMINTSMINT32S_H
#define CORESYSMEMINTSMINT32S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"


// ------------------

/* functions */

bool /* func*/ coresysmemintsmint32s_isempty
  (/* in */ const memint32_t /* $$ param */ ASource);


// ------------------

/* procedures */

void /* $$ func */ coresysmemintsmint32s_clear
  (/* $$ inout */ memint32_t* /* $$ param */ ADest);
  
void /* $$ func */ coresysmemintsmint32s_assign
  (/* $$ inout */ memint32_t* /* $$ param */ ADest,
   /* in */    const memint32_t  /* $$ param */ ASource);
   
// ------------------

/* operators */

bool /* $$ func */ coresysmemintsmint32s_equal
  (/* in */ const memint32_t /* $$ param */ A,
   /* in */ const memint32_t /* $$ param */ B);


bool /* $$ func */ coresysmemintsmint32s_different
  (/* in */ const memint32_t /* $$ param */ A,
   /* in */ const memint32_t /* $$ param */ B);


// ------------------

void /* $$ func */ coresysmemintsmint32s_encode8to32
  (/* out */ nonconst memint32_t* /* $$ param */ ADest,
   /* in */  const    memint8_t   /* $$ param */ A,
   /* in */  const    memint8_t   /* $$ param */ B,
   /* in */  const    memint8_t   /* $$ param */ C,
   /* in */  const    memint8_t   /* $$ param */ D);

void /* $$ func */ coresysmemintsmint32s_encode16to32
  (/* out */ nonconst memint32_t* /* $$ param */ ADest,
   /* in */  const    memint16_t  /* $$ param */ AHiValue,
   /* in */  const    memint16_t  /* $$ param */ ALoValue);
   
void /* $$ func */ coresysmemintsmint32s_decode32to16
  (/* in */  const    memint32_t  /* $$ param */ ASource,
   /* out */ nonconst memint16_t* /* $$ param */ AHiValue,
   /* out */ nonconst memint16_t* /* $$ param */ ALoValue);

void /* $$ func */ coresysmemintsmint32s_decode32to8
  (/* in */  const    memint32_t  /* $$ param */ ASource,
   /* out */ nonconst memint16_t* /* $$ param */ A,
   /* out */ nonconst memint16_t* /* $$ param */ B,
   /* out */ nonconst memint16_t* /* $$ param */ C,
   /* out */ nonconst memint16_t* /* $$ param */ D);



// ------------------

bool /* $$ func */ coresysmemintsmint32s_rttiisdefaultvalue
  (/* in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysmemintsmint32s_rttitryapplydefaultvalue
  (/* $$ inout */ pointer* /* $$ param */ AValue);

void /* $$ func */ coresysmemintsmint32s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

 
// ------------------

/* $$ override */ int /* $$ func */ coresysmemintsmint32s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysmemintsmint32s_finish
  ( noparams );

// ------------------

#endif // CORESYSMEMINTSMINT32S_H

// $$ } // namespace  coresysmemintsmint32s