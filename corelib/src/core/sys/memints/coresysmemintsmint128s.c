/** Module: "coresysmemintsmint128s.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace  coresysmemintsmint128s
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

// --> this module own header goes here
#include "coresysmemintsmint128s.h"
 

// ------------------

/* functions */

bool /* func*/ coresysmemintsmint128s_isempty
  (/* in */ const memint128_t /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* procedures */

void /* $$ func */ coresysmemintsmint128s_clear
  (/* $$ inout */ memint128_t /* $$ param */ ADest)
{
  coresysbasefuncs_nothing();
} // func

void /* $$ func */ coresysmemintsmint128s_assign
  (/* $$ inout */ memint128_t* /* $$ param */ ADest,
   /* in */    const memint128_t  /* $$ param */ ASource)
{
  coresysbasefuncs_nothing();
} // func 

// ------------------

/* operators */

bool /* $$ func */ coresysmemintsmint128s_equal
  (/* in */ const memint128_t /* $$ param */ A,
   /* in */ const memint128_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func


bool /* $$ func */ coresysmemintsmint128s_different
  (/* in */ const memint128_t /* $$ param */ A,
   /* in */ const memint128_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func


 // ------------------

bool /* $$ func */ coresysmemintsmint128s_rttiisdefaultvalue
  (/* in */ const pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysmemintsmint128s_rttitryapplydefaultvalue
  (/* $$ inout */ pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* $$ func */ coresysmemintsmint128s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue)
{
  coresysbasefuncs_nothing();
} // func

// ------------------

/* $$ override */ int /* $$ func */ coresysmemintsmint128s_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysmemintsmint128s_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------


 // ...

// $$ } // namespace  coresysmemintsmint128s