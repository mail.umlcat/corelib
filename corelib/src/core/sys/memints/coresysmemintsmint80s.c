/** Module: "coresysmemintsmint80s.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace  coresysmemintsmint80s
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

// --> this module own header goes here
#include "coresysmemintsmint80s.h"
 

// ------------------

/* functions */

bool /* func*/ coresysmemintsmint80s_isempty
  (/* in */ const memint80_t /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* procedures */


void /* $$ func */ coresysmemintsmint80s_clear
  (/* $$ inout */ memint80_t /* $$ param */ ADest)
{
  coresysbasefuncs_nothing();
} // func

void /* $$ func */ coresysmemintsmint80s_assign
  (/* $$ inout */ memint80_t* /* $$ param */ ADest,
   /* in */    const memint80_t  /* $$ param */ ASource)
{
  coresysbasefuncs_nothing();
} // func 

// ------------------

/* operators */

bool /* $$ func */ coresysmemintsmint80s_equal
  (/* in */ const memint80_t /* $$ param */ A,
   /* in */ const memint80_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func


bool /* $$ func */ coresysmemintsmint80s_different
  (/* in */ const memint80_t /* $$ param */ A,
   /* in */ const memint80_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func


 // ------------------

bool /* $$ func */ coresysmemintsmint80s_rttiisdefaultvalue
  (/* in */ const pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysmemintsmint80s_rttitryapplydefaultvalue
  (/* $$ inout */ pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* $$ func */ coresysmemintsmint80s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue)
{
  coresysbasefuncs_nothing();
} // func

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysmemintsmint80s_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysmemintsmint80s_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------


 // ...

// $$ } // namespace  coresysmemintsmint80s