/** Module: "coresysmemintsmint64s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace  coresysmemintsmint64s
// $$ {
 
// ------------------
 
#ifndef CORESYSMEMINTSMINT64S_H
#define CORESYSMEMINTSMINT64S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"


// ------------------

/* functions */

bool /* func*/ coresysmemintsmint64s_isempty
  (/* in */ const memint64_t /* $$ param */ ASource);


// ------------------

/* procedures */


void /* $$ func */ coresysmemintsmint64s_clear
  (/* $$ inout */ memint64_t /* $$ param */ ADest);

  
void /* $$ func */ coresysmemintsmint64s_assign
  (/* $$ inout */ memint64_t* /* $$ param */ ADest,
   /* in */    const memint64_t  /* $$ param */ ASource);
   
// ------------------

/* operators */

bool /* $$ func */ coresysmemintsmint64s_equal
  (/* in */ const memint64_t /* $$ param */ A,
   /* in */ const memint64_t /* $$ param */ B);


bool /* $$ func */ coresysmemintsmint64s_different
  (/* in */ const memint64_t /* $$ param */ A,
   /* in */ const memint64_t /* $$ param */ B);

// ------------------

void /* $$ func */ coresysmemintsmint64s_encode8to64
  (/* out */ nonconst memint64_t* /* $$ param */ ADest,
   /* in */  const    memint8_t*  /* $$ param */ ASource);

void /* $$ func */ coresysmemintsmint64s_encode32to64
  (/* out */ nonconst memint64_t* /* $$ param */ ADest,
   /* in */  const    memint32_t  /* $$ param */ AHiValue,
   /* in */  const    memint32_t  /* $$ param */ ALoValue);

void /* $$ func */ coresysmemintsmint64s_decode64to8
  (/* in */  const    memint64_t /* $$ param */ ASource,
   /* out */ nonconst memint8_t* /* $$ param */ ADest);

void /* $$ func */ coresysmemintsmint64s_decode64to32
  (/* in */  const    memint64_t  /* $$ param */ ASource,
   /* out */ nonconst memint32_t* /* $$ param */ AHiValue,
   /* out */ nonconst memint32_t* /* $$ param */ ALoValue);


// ------------------

bool /* $$ func */ coresysmemintsmint64s_rttiisdefaultvalue
  (/* in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysmemintsmint64s_rttitryapplydefaultvalue
  (/* $$ inout */ pointer* /* $$ param */ AValue);

void /* $$ func */ coresysmemintsmint64s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysmemintsmint64s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysmemintsmint64s_finish
  ( noparams );

// ------------------

#endif // CORESYSMEMINTSMINT64S_H

// $$ } // namespace  coresysmemintsmint64s