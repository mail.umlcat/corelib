/** Module: "coresysmemintsmint48s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace  coresysmemintsmint48s
// $$ {
 
// ------------------
 
#ifndef CORESYSMEMINTSMINT48S_H
#define CORESYSMEMINTSMINT48S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"


// ------------------

/* functions */

bool /* func*/ coresysmemintsmint48s_isempty
  (/* in */ const memint48_t /* $$ param */ ASource);


// ------------------

/* procedures */

void /* $$ func */ coresysmemintsmint48s_clear
  (/* $$ inout */ memint48_t /* $$ param */ ADest);
  
void /* $$ func */ coresysmemintsmint48s_assign
  (/* $$ inout */ memint48_t* /* $$ param */ ADest,
   /* in */    const memint48_t  /* $$ param */ ASource);
   
// ------------------

/* operators */

bool /* $$ func */ coresysmemintsmint48s_equal
  (/* in */ const memint48_t /* $$ param */ A,
   /* in */ const memint48_t /* $$ param */ B);


bool /* $$ func */ coresysmemintsmint48s_different
  (/* in */ const memint48_t /* $$ param */ A,
   /* in */ const memint48_t /* $$ param */ B);


// ------------------

void /* $$ func */ coresysmemintsmint48s_encodebytesto48
  (/* out */ nonconst memint48_t* /* $$ param */ ADest,
   /* in */  const    memint8_t*  /* $$ param */ ASource);

void /* $$ func */ coresysmemintsmint48s_encodewordsto48
  (/* out */ nonconst memint48_t* /* $$ param */ ADest,
   /* in */  const    memint16_t  /* $$ param */ ASource);

void /* $$ func */ coresysmemintsmint48s_decode48tobytes
  (/* in */  const    memint48_t /* $$ param */ ASource,
   /* out */ nonconst memint8_t* /* $$ param */ ADest);

void /* $$ func */ coresysmemintsmint48s_decode48towords
  (/* in */  const    memint48_t  /* $$ param */ ASource,
   /* out */ nonconst memint16_t* /* $$ param */ ADest);


// ------------------

bool /* $$ func */ coresysmemintsmint48s_rttiisdefaultvalue
  (/* in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysmemintsmint48s_rttitryapplydefaultvalue
  (/* $$ inout */ pointer* /* $$ param */ AValue);

void /* $$ func */ coresysmemintsmint48s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysmemintsmint48s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysmemintsmint48s_finish
  ( noparams );

// ------------------

#endif // CORESYSMEMINTSMINT48S_H

// $$ } // namespace  coresysmemintsmint48s