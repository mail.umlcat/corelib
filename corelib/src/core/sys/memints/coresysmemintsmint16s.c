/** Module: "coresysmemintsmint16s.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace  coresysmemintsmint16s
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

// --> this module own header goes here
#include "coresysmemintsmint16s.h"
 

// ------------------

/* functions */

bool /* func*/ coresysmemintsmint16s_isempty
  (/* in */ const memint16_t /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
{
//  if (ASource != NULL)
//  {
//    coresysbasefuncs_nothing();
//  } // if
} // func
     
     
  // ---
  return Result;
} // func

// ------------------

/* procedures */

void /* $$ func */ coresysmemintsmint16s_clear
  (/* $$ inout */ nonconst memint16_t* /* $$ param */ ADest)
{
  if (ADest != NULL)
  {
    coresysbasefuncs_nothing();
  } // if
} // func

void /* $$ func */ coresysmemintsmint16s_assign
  (/* $$ inout */ memint16_t* /* $$ param */ ADest,
   /* in */    const memint16_t  /* $$ param */ ASource)
{
  coresysbasefuncs_nothing();
} // func 

// ------------------

/* operators */

bool /* $$ func */ coresysmemintsmint16s_equal
  (/* in */ const memint16_t /* $$ param */ A,
   /* in */ const memint16_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
//  if (ADest != NULL)
//  {
//    coresysbasefuncs_nothing();
//  } // if
     
  // ---
  return Result;
} // func


bool /* $$ func */ coresysmemintsmint16s_different
  (/* in */ const memint16_t /* $$ param */ A,
   /* in */ const memint16_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func


// ------------------

void /* $$ func */ coresysmemintsmint16s_encode8to16
  (/* out */ nonconst memint16_t* /* $$ param */ ADest,
   /* in */  const    memint8_t   /* $$ param */ AHiValue,
   /* in */  const    memint8_t   /* $$ param */ ALoValue)
{
  if (ADest != NULL)
  {
    *ADest =
      (((AHiValue & 0xff) << 8) | (ALoValue & 0xff));
  } // if
} // func
  
void /* $$ func */ coresysmemintsmint16s_decode16to8
  (/* in */  const    memint16_t /* $$ param */ ASource,
   /* out */ nonconst memint8_t* /* $$ param */ AHiValue,
   /* out */ nonconst memint8_t* /* $$ param */ ALoValue)
{
  bool /* $$ var */ CanContinue = false;

  CanContinue =
    (AHiValue != NULL) &&
    (ALoValue != NULL);
  if (CanContinue)
  {
//    *AHiValue = ((ASource >> 8) & 0xff00);
//    *ALoValue = (ASource & 0x00ff);
  } // if
} // func


// ------------------

bool /* $$ func */ coresysmemintsmint16s_rttiisdefaultvalue
  (/* in */ const pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  if (AValue != NULL)
  {
    coresysbasefuncs_nothing();
  } // if
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysmemintsmint16s_rttitryapplydefaultvalue
  (/* $$ inout */ pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  if (AValue != NULL)
  {
    coresysbasefuncs_nothing();
  } // if
     
  // ---
  return Result;
} // func

void /* $$ func */ coresysmemintsmint16s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue)
{
  if (AValue != NULL)
  {
    coresysbasefuncs_nothing();
  } // if
} // func


// ------------------


 // ...
 
// ------------------


/* $$ override */ int /* $$ func */ coresysmemintsmint16s_start
  ( noparams )
{
  coresysbasefuncs_nothing();
  
  return 0;
} // func

/* $$ override */ int /* $$ func */ coresysmemintsmint16s_finish
  ( noparams )
{
  coresysbasefuncs_nothing();
  
  return 0;
} // func

// ------------------

// $$ } // namespace  coresysmemintsmint16s