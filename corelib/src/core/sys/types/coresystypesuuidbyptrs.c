/** Module: "coresystypesuuidbyptrs.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresystypesuuidbyptrs
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

// --> this same module header goes here
#include "coresystypesuuidbyptrs.h"
 
// ------------------

/* functions */

bool /* $$ func */ coresystypesuuidbyptrs_isempty
  (/* $$ in */ const pointer* /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

pointer* /* $$ func */ coresystypesuuidbyptrs_consttoptr
  (/* $$ in */ const uuid* /* $$ param */ ASource)
{
  pointer* /* $$ var */ Result = NULL;
  // ---
     
  uuid* /* $$ var */ S = NULL;
  
  // ---
    
  S =
    (uuid*) malloc(sizeof(uuid));
  //*S = ASource;
  Result =
    (pointer*) S;
     
  // ---
  return Result;
} // func

// ------------------

/* procedures */

void /* $$ func */ coresystypesuuidbyptrs_clear
  (/* $$ inout */ nonconst pointer* /* $$ param */ ADest)
{
  coresysbasefuncs_nothing();
} // func

void /* $$ func */ coresystypesuuidbyptrs_assign
  (/* $$ inout */ nonconst pointer** /* $$ param */ ADest,
   /* $$ in */    const    pointer*  /* $$ param */ ASource)
{
  coresysbasefuncs_nothing();
} // func

void /* $$ func */ coresystypesuuidbyptrs_dropptr
  (/* $$ inout */ nonconst pointer** /* $$ param */ ADest)
{
  // ---
     
  uuid* /* $$ var */ D = NULL;
  
  // ---
    
  D =
    (uuid*) (*ADest);
  free(D);
  
  D = NULL;
  ADest = NULL;
} // func 
   
// ------------------

/* operators */

bool /* $$ func */ coresystypesuuidbyptrs_equal
  (/* $$ in */ const pointer* /* $$ param */ A,
   /* $$ in */ const pointer* /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result =
    ((A != NULL) && (B != NULL));
  if (Result)
  {
    // toxdo: ...
  } // if

  // ---
  return Result;
} // func

bool /* $$ func */ coresystypesuuidbyptrs_different
  (/* $$ in */ const pointer* /* $$ param */ A,
   /* $$ in */ const pointer* /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result =
    ((A != NULL) && (B != NULL));
  if (Result)
  {
    // toxdo: ...
  } // if

  // ---
  return Result;
} // func
   
// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresystypesuuidbyptrs_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresystypesuuidbyptrs_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresystypesuuidbyptrs