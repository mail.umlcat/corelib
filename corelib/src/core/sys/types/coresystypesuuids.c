/** Module: "coresystypesuuids.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresystypesuuids
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

// --> this same module header goes here
#include "coresystypesuuids.h"
 
// ------------------

/* functions */

bool /* $$ func */ coresystypesuuids_isempty
  (/* $$ in */ const uuid* /* $$ param */ ASource)
{
  bool /* $$ var */ Result = true;
  // ---
    
  byte_t* /* $$ var */ P = NULL;
  
  P =
   (byte_t*) ASource;
  if (P != NULL)
  {
    size_t /* $$ var */ i = sizeof(uuid);
    while (i > 0)
    {
      if (*P != 0)
	    {
	      Result = false;
	    	break;
	    }
	    P++;
	    i--;
  	} // for
  } // if
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresystypesuuids_areequal
  (/* $$ in */ const uuid* /* $$ param */ A,
   /* $$ in */ const uuid* /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func


// ------------------

/* procedures */

void /* $$ func */ coresystypesuuids_clear
  (/* $$ inout */ nonconst uuid* /* $$ param */ ADest)
{
  memset((void*)ADest, 0, sizeof(uuid));
} // func
  
void /* $$ func */ coresystypesuuids_assign
  (/* $$ inout */ nonconst uuid* /* $$ param */ ADest,
   /* $$ in */    const    uuid* /* $$ param */ ASource)
{
  bool /* $$ var */ CanContinue = false;
  
  // ---
  
  CanContinue =
    ((ADest != NULL) && (ASource != NULL));
  if (CanContinue)
  {
    byte_t* /* $$ var */ S = NULL;
    byte_t* /* $$ var */ D = NULL;
    size_t  /* $$ var */ C = 0;
  
    // ---
  
    D = ((byte_t*) ADest);
    S = ((byte_t*) ASource);
    
    C = sizeof(uuid);
    
    for (byte_t /* $$ var */ i = 0; i < C; i++)
    {
      *D = *S;
      
      S++;
      D++;    
    } // for
  } // if
} // func  
  
void /* $$ func */ coresystypesuuids_encodearraytouuid
  (/* $$ inout */ nonconst uuid*    /* $$ param */ ADest,
   /* $$ in */    const    uint8_t* /* $$ param */ ASource)
{
  bool /* $$ var */ CanContinue = false;
  
  // ---
  
  CanContinue =
    ((ADest != NULL) && (ASource != NULL));
  if (CanContinue)
  {
    byte_t* /* $$ var */ S = NULL;
    byte_t* /* $$ var */ D = NULL;
    size_t  /* $$ var */ C = 0;
  
    // ---
  
    D = ((byte_t*) ADest);
    S = ((byte_t*) ASource);
    
    C = sizeof(uuid);
    
    for (byte_t /* $$ var */ i = 0; i < C; i++)
    {
      *D = *S;
      
      S++;
      D++;
    } // for
  } // if
} // func  

// ------------------

/* operators */

bool /* $$ func */ coresystypesuuids_equal
  (/* $$ in */ const uuid* /* $$ param */ A,
   /* $$ in */ const uuid* /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result =
    ((A != NULL) && (B != NULL));
  if (Result)
  {
    // toxdo: ...
  } // if

  // ---
  return Result;
} // func



// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresystypesuuids_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresystypesuuids_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresystypesuuids