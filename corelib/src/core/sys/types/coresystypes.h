/** Module: "coresystypes.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresystypes
// $$ {
 
// ------------------
 
#ifndef CORESYSTYPES_H
#define CORESYSTYPES_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

// --> contained modules here
#include "coresystypesuuids.h"
#include "coresystypesuuidbyptrs.h"

// ------------------

/* rtti */

#define MOD_coresystypes
// $$ {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresystypes_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresystypes_finish
  ( noparams );

// ------------------

#endif // CORESYSTYPES_H

// ------------------

// $$ } // namespace coresystypes