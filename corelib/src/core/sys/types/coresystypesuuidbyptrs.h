/** Module: "coresystypesuuidbyptrs.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresystypesuuidbyptrs
// $$ {
 
// ------------------
 
#ifndef CORESYSUUIDBYPTRS_H
#define CORESYSUUIDBYPTRS_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

// --> contained modules here
// .include "coresystypesuuidbyptrsbinfloat16s.h"
// .include "coresystypesuuidbyptrsbinfloat32s.h"
// .include "coresystypesuuidbyptrsbinfloat64s.h"
// .include "coresystypesuuidbyptrsbinfloat128s.h"
// .include "coresystypesuuidbyptrsbinfloat256s.h"
// .include "coresystypesuuidbyptrslongfloats.h"

// ------------------

/* rtti */

#define MOD_coresystypesuuidbyptrs {0x0E,0xB2,0xC1,0x46,0x15,0x18,0x04,0x42,0x88,0xC3,0x0B,0x47,0x4C,0x10,0x20,0x78};

// ------------------

/* functions */

bool /* $$ func */ coresystypesuuidbyptrs_isempty
  (/* $$ in */ const pointer* /* $$ param */ ASource);

pointer* /* $$ func */ coresystypesuuidbyptrs_consttoptr
  (/* $$ in */ const uuid* /* $$ param */ ASource);

// ------------------

/* procedures */

void /* $$ func */ coresystypesuuidbyptrs_clear
  (/* $$ inout */ nonconst pointer* /* $$ param */ ADest);

void /* $$ func */ coresystypesuuidbyptrs_assign
  (/* $$ inout */ nonconst pointer** /* $$ param */ ADest,
   /* $$ in */    const    pointer*  /* $$ param */ ASource);

void /* $$ func */ coresystypesuuidbyptrs_dropptr
  (/* $$ inout */ nonconst pointer** /* $$ param */ ADest);
   
// ------------------

/* operators */

bool /* $$ func */ coresystypesuuidbyptrs_equal
  (/* $$ in */ const pointer* /* $$ param */ A,
   /* $$ in */ const pointer* /* $$ param */ B);

bool /* $$ func */ coresystypesuuidbyptrs_different
  (/* $$ in */ const pointer* /* $$ param */ A,
   /* $$ in */ const pointer* /* $$ param */ B);
   
// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresystypesuuidbyptrs_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresystypesuuidbyptrs_finish
  ( noparams );

// ------------------

#endif // CORESYSUUIDBYPTRS_H

// ------------------

// $$ } // namespace coresystypesuuidbyptrs