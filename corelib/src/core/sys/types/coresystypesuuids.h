/** Module: "coresystypesuuids.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresystypesuuids
// $$ {
 
// ------------------
 
#ifndef CORESYSUUIDS_H
#define CORESYSUUIDS_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

// --> contained modules here
// .include "coresystypesuuidsbinfloat16s.h"
// .include "coresystypesuuidsbinfloat32s.h"
// .include "coresystypesuuidsbinfloat64s.h"
// .include "coresystypesuuidsbinfloat128s.h"
// .include "coresystypesuuidsbinfloat256s.h"
// .include "coresystypesuuidslongfloats.h"

// ------------------

/* rtti */

#define MOD_coresystypesuuids {0x75,0x6F,0xED,0xB9,0xB5,0x78,0x1B,0x4E,0x97,0x81,0x19,0x87,0x87,0x2D,0xD9,0xCA}


// ------------------

/* functions */

bool /* $$ func */ coresystypesuuids_isempty
  (/* $$ in */ const uuid* /* $$ param */ ASource);





// ------------------

/* procedures */

void /* $$ func */ coresystypesuuids_clear
  (/* $$ inout */ nonconst uuid* /* $$ param */ ADest);
  
void /* $$ func */ coresystypesuuids_assign
  (/* $$ inout */ nonconst uuid* /* $$ param */ ADest,
   /* $$ in */    const    uuid* /* $$ param */ ASource);
  
void /* $$ func */ coresystypesuuids_encodearraytouuid
  (/* $$ inout */ nonconst uuid*    /* $$ param */ ADest,
   /* $$ in */    const    uint8_t* /* $$ param */ ASource);
   
// ------------------

/* operators */

bool /* $$ func */ coresystypesuuids_equal
  (/* $$ in */ const uuid* /* $$ param */ A,
   /* $$ in */ const uuid* /* $$ param */ B);





// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresystypesuuids_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresystypesuuids_finish
  ( noparams );

// ------------------

#endif // CORESYSUUIDS_H

// ------------------

// $$ } // namespace coresystypesuuids