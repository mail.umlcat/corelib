/** Module: "coresystypes<packagename>s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresystypes<packagename>s
// $$ {
 
// ------------------
 
#ifndef CORESYS<PACKAGENAME>S_H
#define CORESYS<PACKAGENAME>S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresystypesbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresystypesmemints.h"

// ------------------

// --> contained modules here
// .include "coresystypes<packagename>sbinfloat16s.h"
// .include "coresystypes<packagename>sbinfloat32s.h"
// .include "coresystypes<packagename>sbinfloat64s.h"
// .include "coresystypes<packagename>sbinfloat128s.h"
// .include "coresystypes<packagename>sbinfloat256s.h"
// .include "coresystypes<packagename>slongfloats.h"

// ------------------

/* rtti */

#define MOD_coresystypes<packagename>s
// $$ {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresystypes<packagename>s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresystypes<packagename>s_finish
  ( noparams );

// ------------------

#endif // CORESYS<PACKAGENAME>S_H

// ------------------

// $$ } // namespace coresystypes<packagename>s