/** Module: "coresysbasefunctors.c"
 ** Descr.: "Predefined function pointers."
 **/

// $$ namespace coresysbasefunctors
// $$ {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> same package libraries here
#include "coresysbasemacros.h"
#include "coresysbaseuuidtypes.h"
#include "coresysbasetypes.h"
#include "coresysbaseastrtypes.h"
#include "coresysbasewstrtypes.h"

// ------------------

#include "coresysbasefunctors.h"
 
// ------------------

 // ...

// ------------------

/* $$ override */ int /* $$ func */ coresysbasefunctors_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasetem_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysbasefunctors_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasetem_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresysbasefunctors