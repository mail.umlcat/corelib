/** Module: "coresysbasetem.h"
 ** File:   "coresysbaseerrorcodes.h"
 ** Descr.: "Is a 'coresysbasetem.*' complement."
 **/

// $$ namespace coresysbaseerrorcodes
// $$ {
 
// ------------------
 
#ifndef CORESYSBASEERRORCODES_H
#define CORESYSBASEERRORCODES_H
 
// ------------------

#define ERRORCODE_SUCCESS           0
#define ERRORCODE_UNEXPECTED        -1
#define ERRORCODE_DEPRECATED        -2
#define ERRORCODE_ABSTRACT          -3
#define ERRORCODE_UNDERCONSTRUCTION -4

// ------------------


// ------------------

#endif // CORESYSBASEERRORCODES_H

// $$ } // namespace coresysbaseerrorcodes