/** Module: "coresysbaseenums.c"
 ** Descr.:
 ** "<put description here>"
 ** "<put description here>"
 ** "<put description here>"
 ** "<put description here>"
 **/

// $$ namespace coresysbaseenums
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "coresysbasemacros.h"

// ------------------

// --> other "corelib" libraries here
// #include <put your library here>
// #include <put your library here>
// #include <put your library here>

// ------------------

// --> other NON "corelib" libraries here
// #include <put your library here>
// #include <put your library here>
// #include <put your library here>

// ------------------

// this module own header goes here
#include "coresysbaseenums.h"
 
// ------------------
 


 // ...

// ------------------

/* $$ override */ int /* $$ func */ coresysbaseenums_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasetem_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysbaseenums_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasetem_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresysbaseenums