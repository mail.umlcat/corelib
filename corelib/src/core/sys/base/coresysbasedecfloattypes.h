/** Module: "coresysbasedecfloattypes.h"
 ** File:   "coresysbasedecfloattypes.h"
 ** Descr.: "Is a 'coresysbase.*' complement."
 **/
 
// $$ namespace coresysbasedecfloattypes
// $$ {
 
// ------------------
 
#ifndef CORESYSBASEDECFLOATTYPES_H
#define CORESYSBASEDECFLOATTYPES_H
 
// ------------------

typedef
  memint32_t   /* as */ decfloat32_t;
typedef
  memint32_t*  /* as */ decfloat32_p;

typedef
  memint64_t   /* as */ decfloat64_t;
typedef
  memint64_t*  /* as */ decfloat64_p;

typedef
  memint128_t  /* as */ decfloat128_t;
typedef
  memint128_t* /* as */ decfloat128_p;

// ------------------

#endif // CORESYSBASEDECFLOATTYPES_H

// $$ } // namespace coresysbasedecfloattypes
