/** Module: "coresysbase"
 ** Filename: "coresysbase.h"
 ** Qualified Identifier:
 ** "core::system" 
 ** Descr.:
 ** "System Package Module"
 **/

// $$ namespace coresysbase
// $$ {
 
// ------------------
 
#ifndef CORESYSBASE_H
#define CORESYSBASE_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> contained modules here
#include "coresysbasemacros.h"
#include "coresysbaseerrorcodes.h"
#include "coresysbaseenums.h"
#include "coresysbasemem.h"
#include "coresysbaseuuidtypes.h"
#include "coresysbasetypes.h"
#include "coresysbasemodules.h"
#include "coresysbasesinttypes.h"
#include "coresysbaseuinttypes.h"
#include "coresysbasememinttypes.h"
#include "coresysbasedecfloattypes.h"
#include "coresysbasebinfloattypes.h"
#include "coresysbaseastrtypes.h"
#include "coresysbasewstrtypes.h"
#include "coresysbasefuncs.h"
#include "coresysbasefunctors.h"
#include "coresysbaseparams.h"

// ------------------

// --> other "corelib" libraries here
// #include <put your library here>
// #include <put your library here>
// #include <put your library here>

// ------------------

// --> other NON "corelib" libraries here
// #include <put your library here>
// #include <put your library here>
// #include <put your library here>

// ------------------

/* rtti */

#define MOD_coresys     {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
#define MOD_coresysbase {0x02,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------




 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysbase_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysbase_finish
  ( noparams );

// ------------------

#endif // CORESYSBASE_H

// ------------------

// $$ } // namespace coresysbase