/** Module: "coresysbasemem.c"
 ** Descr.:
 ** "Memory Management Library."
 **/

// $$ namespace coresysbasemem {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "coresysbasemacros.h"
#include "coresysbaseerrorcodes.h"
#include "coresysbaseenums.h"

// ------------------

/* $$ inline */ bool /* $$ func */ coresysbasemem_isassigned
  (/* $$ in */ const pointer* /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---

  // Result =
    // (ASource != NULL); 
     
  // ---
  return Result;
} // func

/* $$ inline */ bool /* $$ func */ coresysbasemem_isassignedsize
  (/* $$ in */ const pointer* /* $$ param */ ASourceBuffer, 
   /* $$ in */ const size_t   /* $$ param */ ASourceSize)
{
  bool /* $$ var */ Result = false;
  // ---

  // Result =
    // (ASourceBuffer != NULL) && (ASourceSize > 0); 

  // ---
  return Result;
} // func

// ------------------

bool /* $$ func */ coresysbasemem_tryclear
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    size_t   /* $$ param */ ACount)
{
  bool /* $$ var */ Result = false;
  // ---

  Result =
    ((ADest != NULL) && (ACount > 0));
  if (Result)
  {
    memset
      ((void*) ADest, 0, ACount);
  } 

  // ---
  return Result;
} // func

bool /* $$ func */ coresysbasemem_trycleararray
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    size_t   /* $$ param */ AItemSize,   
   /* $$ in */  const    count_t  /* $$ param */ AItemCount)
{
  bool /* $$ var */ Result = false;
  // ---

  Result =
    ((ADest != NULL) && (AItemSize > 0) && (AItemCount > 0));
  if (Result)
  {
    memset
      ((void*) ADest, 0, AItemSize * AItemCount);
  } 

  // ---
  return Result;
} // func

bool /* $$ func */ coresysbasemem_tryfill
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    uint8_t  /* $$ param */ AValue,
   /* $$ in */  const    size_t   /* $$ param */ ACount)
{
  bool /* $$ var */ Result = false;
  // ---

  Result =
    ((ADest != NULL) && (ACount > 0));
  if (Result)
  {
    memset
      ((void*) ADest, AValue, ACount);
  } 

  // ---
  return Result;
} // func 

bool /* $$ func */ coresysbasemem_tryfillarray
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    uint8_t  /* $$ param */ AValue,
   /* $$ in */  const    size_t   /* $$ param */ AItemSize,   
   /* $$ in */  const    count_t  /* $$ param */ AItemCount)
{
  bool /* $$ var */ Result = false;
  // ---

  Result =
    ((ADest != NULL) && (AItemSize > 0) && (AItemCount > 0));
  if (Result)
  {
    memset
      ((void*) ADest, AValue, AItemSize * AItemCount);
  } 

  // ---
  return Result;
} // func 

bool /* $$ func */ coresysbasemem_trycopy
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    pointer* /* $$ param */ ASource,
   /* $$ in */  const    size_t   /* $$ param */ ACount)
{
  bool /* $$ var */ Result = false;
  // ---

  Result =
    ((ADest != NULL) && (ASource != NULL) && (ACount > 0));
  if (Result)
  {
    memcpy
      ((void*) ADest, (void*) ASource, ACount);
  } 

  // ---
  return Result;
} // func

bool /* $$ func */ coresysbasemem_trycopyarray
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    pointer* /* $$ param */ ASource,
   /* $$ in */  const    size_t   /* $$ param */ AItemSize,   
   /* $$ in */  const    count_t  /* $$ param */ AItemCount)
{
  bool /* $$ var */ Result = false;
  // ---

  Result =
    ((ADest != NULL) && (ASource != NULL) && (AItemSize > 0) && (AItemCount > 0));
  if (Result)
  {
    memcpy
      ((void*) ADest, (void*) ASource, AItemSize * AItemCount);
  } 

  // ---
  return Result;
} // func

bool /* $$ func */ coresysbasemem_tryrevcopyarray
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    pointer* /* $$ param */ ASource,
   /* $$ in */  const    size_t   /* $$ param */ AItemSize,   
   /* $$ in */  const    count_t  /* $$ param */ AItemCount)
{
  bool /* $$ var */ Result = false;
  // ---

  Result =
    ((ADest != NULL) && (ASource != NULL) && (AItemSize > 0) && (AItemCount > 0));
  // if (Result)
  // {
    // memcpy
      // ((void*) ADest, (void*) ASource, AItemSize * AItemCount);
  // } 

  // ---
  return Result;
} // func

bool /* $$ func */ coresysbasemem_trycompare
  (/* $$ out */ nonconst enum comparison* /* $$ param */ AComparison,
   /* $$ out */ const    pointer*         /* $$ param */ A,
   /* $$ in */  const    pointer*         /* $$ param */ B,
   /* $$ in */  const    count_t          /* $$ param */ ACount)
{
  bool /* $$ var */ Result = false;
  // ---

  Result =
    ((AComparison != NULL) && (A != NULL) && (B != NULL) && (ACount > 0));
  if (Result)
  {
    *AComparison =
      (enum comparison) memcmp
        ((void*) A, (void*) B, ACount);
  } 

  // ---
  return Result;
} // func
   
bool /* $$ func */ coresysbasemem_trycomparearray
  (/* $$ out */ nonconst enum comparison* /* $$ param */ AComparison,
   /* $$ out */ const    pointer*         /* $$ param */ A,
   /* $$ in */  const    pointer*         /* $$ param */ B,
   /* $$ in */  const    size_t           /* $$ param */ AItemSize,   
   /* $$ in */  const    count_t          /* $$ param */ AItemCount)
{
  bool /* $$ var */ Result = false;
  // ---

  Result =
    ((AComparison != NULL) && (A != NULL) && (B != NULL) && (AItemSize > 0) && (AItemCount > 0));
  if (Result)
  {
    *AComparison =
      (enum comparison) memcmp
        ((void*) A, (void*) B, AItemSize * AItemSize);
  } 

  // ---
  return Result;
} // func

bool /* $$ func */ coresysbasemem_tryreversearray
  (/* $$ out */ const    pointer* /* $$ param */ ASource,
   /* $$ in */  const    size_t   /* $$ param */ AItemSize,   
   /* $$ in */  const    count_t  /* $$ param */ AItemCount)
{
  bool /* $$ var */ Result = false;
  // ---

  // Result =
    // ((AComparison != NULL) && (A != NULL) && (B != NULL) && (AItemSize > 0) && (AItemCount > 0));
  // if (Result)
  // {
    // *AComparison =
      // (enum comparison) memcmp
        // ((void*) A, (void*) B, AItemSize * AItemSize);
  // } 

  // ---
  return Result;
} // func

// ------------------

bool /* $$ func */ coresysbasemem_areequal
  (/* $$ in */ const pointer* /* $$ param */ A,
   /* $$ in */ const pointer* /* $$ param */ B,
   /* $$ in */ const size_t   /* $$ param */ AItemSize)
{
    bool /* $$ var */ Result = false;
    // ---
    
	Result =
	  (memcmp((void*) A, (void*) B, AItemSize) == 0);
	
	
    // result =
      // (a != null) && (b != null) && (acount > 0);
    // if (result)
    // $$ {
      // byte_t* /* $$ var */ c = null;
      // byte_t* /* $$ var */ d = null;
    
      // int  /* $$ var */ i = 0;
      // bool /* $$ var */ cancontinue = false;
      // bool /* $$ var */ found = false;
    
      // c = (pointer*) a;
      // d = (pointer*) b;
      
      // while (cancontinue)
      // $$ {
         // found =
          // (*c != *d);
      	
         // cancontinue =
          // (i < acount) && (!found);
         // i++;
      // $$ } // while
      
      // result = (!found);
    // $$ } // if
     
    // ---
    return Result;
} // func

bool /* $$ func */ coresysbasemem_areequalarray
  (/* $$ in */ const pointer* /* $$ param */ A,
   /* $$ in */ const pointer* /* $$ param */ B,
   /* $$ in */ const size_t   /* $$ param */ AItemSize,
   /* $$ in */ const count_t  /* $$ param */ AItemCount)
{
    bool /* $$ var */ Result = false;
    // ---
    
	Result =
	  (memcmp((void*) A, (void*) B, AItemSize * AItemCount) == 0);
	
    // Result =
      // (A != NULL) && (B != NULL) && (AItemCount > 0);
    // if (Result)
    // $$ {
      // byte_t* /* $$ var */ C = NULL;
      // byte_t* /* $$ var */ D = NULL;

      // size_t  /* $$ var */ ASize = 0;
    
      // int  /* $$ var */ i = 0;
      // bool /* $$ var */ CanContinue = false;
      // bool /* $$ var */ Found = false;
    
      // ASize = (AItemSize * AItemCount);
      // C = (pointer*) A;
      // D = (pointer*) B;
      
      // while (CanContinue)
      // $$ {
         // Found =
          // (*C != *D);
      	
         // CanContinue =
          // (i < ASize) && (!Found);
         // i++;
      // $$ } // while
      
      // Result = (!Found);
    // $$ } // if
     
    // ---
    return Result;
} // func

enum comparison /* $$ func */ coresysbasemem_compare
  (/* $$ out */ const pointer* /* $$ param */ A,
   /* $$ in */  const pointer* /* $$ param */ B,
   /* $$ in */  const count_t  /* $$ param */ ACount)
{
  enum comparison /* $$ var */ Result = comparison_equal;
  // ---
    
  bool /* $$ var */ CanContinue = false;

  // ---
  
  CanContinue =
    coresysbasemem_trycompare((enum comparison*) &Result, A, B, ACount);
  if (! CanContinue)
  {
    // raise error
	  exit(-1);
  } 

  // ---
  return Result;
} // func

enum comparison /* $$ func */ coresysbasemem_comparearray
  (/* $$ out */ const pointer* /* $$ param */ A,
   /* $$ in */  const pointer* /* $$ param */ B,
   /* $$ in */  const size_t   /* $$ param */ AItemSize,   
   /* $$ in */  const count_t  /* $$ param */ AItemCount)
{
  enum comparison /* $$ var */ Result = comparison_equal;
  // ---
    
  bool /* $$ var */ CanContinue = false;

  // ---
  
  CanContinue =
    coresysbasemem_trycomparearray
	  ((enum comparison*) &Result, A, B, AItemSize, AItemCount);
  if (! CanContinue)
  {
    // raise error
	  exit(-1);
  } 

  // ---
  return Result;
} // func

pointer* /* $$ func */ coresysbasemem_allocate
  (/* $$ in */ const size_t  /* $$ param */ AItemSize)
{
  pointer* /* $$ var */ Result = NULL;
  // ---
  
  Result =
    (pointer*) malloc(AItemSize);
     
  // ---
  return Result;
} // func

pointer* /* $$ func */ coresysbasemem_allocatearray
  (/* $$ in */ const size_t  /* $$ param */ AItemSize,
   /* $$ in */ const count_t /* $$ param */ AItemCount)
{
  pointer* /* $$ var */ Result = NULL;
  // ---
  
  size_t /* $$ var */ SizeInBytes = 0;
  // ---

  SizeInBytes =
    (AItemSize * AItemCount);
  Result =
    (pointer*) malloc(SizeInBytes);
     
  // ---
  return Result;
} // func

pointer* /* $$ func */ coresysbasemem_allocateclear
  (/* $$ in */ const size_t /* $$ param */ ASize)
{
  pointer* /* $$ var */ Result = NULL;
  // ---

  Result =
    (pointer*) malloc(ASize);
  memset
    (Result, 0, ASize);
     
  // ---
  return Result;
} // func

pointer* /* $$ func */ coresysbasemem_allocatearrayclear
  (/* $$ in */ const size_t  /* $$ param */ AItemSize,
   /* $$ in */ const count_t /* $$ param */ AItemCount)
{
  pointer* /* $$ var */ Result = NULL;
  // ---
  
  size_t /* $$ var */ SizeInBytes = 0;
  // ---

  SizeInBytes =
    (AItemSize * AItemCount);

  Result =
    (pointer*) malloc(SizeInBytes);
  memset
    (Result, 0, SizeInBytes);
     
  // ---
  return Result;
}

pointer* /* $$ func */ coresysbasemem_duplicatecopy
  (/* $$ in */ const pointer* /* $$ param */ ASourceBuffer,
   /* $$ in */ const size_t   /* $$ param */ ASourceSize)
{
  pointer* /* $$ var */ Result = NULL;
  // ---
  
 Result =
   (pointer*) coresysbasemem_allocate(ASourceSize);
   
  memcpy
    ((void*) Result, (void*) ASourceBuffer, ASourceSize);
   
/*
void  $$ func  coresysbasemem_copy
  ( nonconst pointer*  $$ param  ADest,
    const    pointer*  $$ param  ASource,
    const    size_t    $$ param  ACount);
*/
   
// coresysbasemem_copy
//   (Result, ASourceBuffer, ASourceSize);
     
  // ---
  return Result;
} // func

pointer* /* $$ func */ coresysbasemem_duplicatearraycopy
  (/* $$ in */ const pointer* /* $$ param */ ASourceBuffer,
   /* $$ in */ const size_t   /* $$ param */ AItemSize,
   /* $$ in */ const count_t  /* $$ param */ AItemCount)
{
  pointer* /* $$ var */ Result = NULL;
  // ---
  
  size_t /* $$ var */ SizeInBytes =
    (AItemSize * AItemCount);

  Result =
    coresysbasemem_duplicatecopy
      (ASourceBuffer, SizeInBytes);
     
  // ---
  return Result;
} // func

// ------------------

/* $$ inline */ size_t /* $$ func */ coresysbasemem_minsize
  (/* $$ in */ const size_t /* $$ param */ A,
   /* $$ in */ const size_t /* $$ param */ B)
{
  size_t /* $$ var */ Result = 0;
  // ---
  
  // Result = (A < B) ? A : B; 
     
  // ---
  return Result;
} // func

/* $$ inline */ size_t /* $$ func */ coresysbasemem_maxsize
  (/* $$ in */ const size_t /* $$ param */ A,
   /* $$ in */ const size_t /* $$ param */ B)
{
  size_t /* $$ var */ Result = 0;
  // ---
     
  // Result = (A > B) ? A : B; 
    
  // ---
  return Result;
} // func

/* $$ inline */ size_t /* $$ func */ coresysbasemem_zerominsize
  (/* $$ in */ const size_t /* $$ param */ A,
   /* $$ in */ const size_t /* $$ param */ B)
{
  size_t /* $$ var */ Result = 0;
  // ---
     
  // A = (A > 0) ? A : 0; 
  // B = (B > 0) ? B : 0; 
  // Result = (A > B) ? A : B; 
     
  // ---
  return Result;
} // func

/* $$ inline */ size_t /* $$ func */ coresysbasemem_zeromaxsize
  (/* $$ in */ const size_t /* $$ param */ A,
   /* $$ in */ const size_t /* $$ param */ B)
{
  size_t /* $$ var */ Result = 0;
  // ---
     
  // A = (A > 0) ? A : 0; 
  // B = (B > 0) ? B : 0; 
  // Result = (A < B) ? A : B; 
     
  // ---
  return Result;
} // func

/* $$ inline */ size_t /* $$ func */ coresysbasemem_oneminsize
  (/* $$ in */ const size_t /* $$ param */ A,
   /* $$ in */ const size_t /* $$ param */ B)
{
  size_t /* $$ var */ Result = 0;
  // ---
     
  // A = (A > 1) ? A : 1; 
  // B = (B > 1) ? B : 1; 
  // Result = (A > B) ? A : B; 
     
  // ---
  return Result;
} // func

/* $$ inline */ size_t /* $$ func */ coresysbasemem_onemaxsize
  (/* $$ in */ const size_t /* $$ param */ A,
   /* $$ in */ const size_t /* $$ param */ B)
{
  size_t /* $$ var */ Result = 0;
  // ---
     
  // A = (A > 1) ? A : 1; 
  // B = (B > 1) ? B : 1; 
  // Result = (A < B) ? A : B; 
     
  // ---
  return Result;
} // func


// ------------------

void /* $$ func */ coresysbasemem_deallocateclear
  (/* $$ out */ nonconst pointer** /* $$ param */ ADest,
   /* $$ in */  const    size_t    /* $$ param */ ASize)
{
  memset
    (*ADest, 0, ASize);
  free(*ADest);
  *ADest = NULL;
}

void /* $$ func */ coresysbasemem_deallocatearrayclear
  (/* $$ out */ nonconst pointer** /* $$ param */ ADest,
   /* $$ in */  const size_t       /* $$ param */ AItemSize,
   /* $$ in */  const count_t      /* $$ param */ AItemCount)
{
  size_t /* $$ var */ SizeInBytes =
    (AItemSize * AItemCount);

  memset
    (*ADest, 0, SizeInBytes);
  free(*ADest);
  *ADest = NULL;
}

// ------------------

void /* $$ func */ coresysbasemem_deallocate
  (/* $$ out */ pointer**    /* $$ param */ ADest,
   /* $$ in */  const size_t /* $$ param */ ASize)
{
  free(*ADest);
  *ADest = NULL;
} // func

void /* $$ func */ coresysbasemem_deallocaterray
  (/* $$ out */ nonconst pointer** /* $$ param */ ADest,
   /* $$ in */  const    size_t    /* $$ param */ AItemSize,
   /* $$ in */  const    count_t   /* $$ param */ AItemCount)
{
  // size_t /* $$ var */ SizeInBytes =
    // (AItemSize * AItemCount);
  // memset(*ADest, 0, SizeInBytes);
  free(*ADest);
}

void /* $$ func */ coresysbasemem_clear
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    size_t   /* $$ param */ ACount)
{
  bool /* $$ var */ CanContinue = false;

  // ---
  
  CanContinue =
    coresysbasemem_tryclear(ADest, ACount);
  if (! CanContinue)
  {
    // raise error
    exit(-1);
  } 
} // func 

void /* $$ func */ coresysbasemem_cleararray
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const size_t      /* $$ param */ AItemSize,
   /* $$ in */  const count_t     /* $$ param */ AItemCount)
{
  bool /* $$ var */ CanContinue = false;

  // ---
  
  CanContinue =
    coresysbasemem_trycleararray
	  (ADest, AItemSize, AItemCount);
  if (! CanContinue)
  {
    // raise error
    exit(-1);
  } 
} // func 

void /* $$ func */ coresysbasemem_fill
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    size_t   /* $$ param */ ACount,
   /* $$ in */  const    uint8_t  /* $$ param */ AValue)
{
  bool /* $$ var */ CanContinue = false;

  // ---
  
//  CanContinue =
//    coresysbasemem_tryfill(ADest, ACount, AValue);

  memset
    ((void*) ADest, AValue, ACount);
  CanContinue = true;

  if (! CanContinue)
  {
    // raise error
    exit(-1);
  } 
} // func

void /* $$ func */ coresysbasemem_fillarray
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    uint8_t  /* $$ param */ AValue, 
   /* $$ in */  const    size_t   /* $$ param */ AItemSize,
   /* $$ in */  const    count_t  /* $$ param */ AItemCount)
{
  bool /* $$ var */ CanContinue = false;

  // ---
  
  CanContinue =
    coresysbasemem_tryfillarray
	  (ADest, AValue, AItemSize, AItemCount);
  if (! CanContinue)
  {
    // raise error
    exit(-1);
  } 
} // func
   
void /* $$ func */ coresysbasemem_copy
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    pointer* /* $$ param */ ASource,
   /* $$ in */  const    size_t   /* $$ param */ ACount)
{
  bool /* $$ var */ CanContinue = false;

  // ---
  
  CanContinue =
    coresysbasemem_trycopy(ADest, ASource, ACount);
  if (! CanContinue)
  {
    // raise error
    exit(-1);
  } 
} // func
   
void /* $$ func */ coresysbasemem_copyarray
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    pointer* /* $$ param */ ASource,
   /* $$ in */  const    size_t   /* $$ param */ AItemSize,
   /* $$ in */  const    count_t  /* $$ param */ AItemCount)
{
  bool /* $$ var */ CanContinue = false;

  // ---
  
  CanContinue =
    coresysbasemem_trycopyarray
	  (ADest, ASource, AItemSize, AItemCount);
  if (! CanContinue)
  {
    // raise error
    exit(-1);
  } 
} // func

// ------------------

 // ...

// ------------------

/* $$ override */ int /* $$ func */ coresysbasemem_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  // coresysbasetem_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysbasemem_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  // coresysbasetem_nothing();

  // ---
  return Result;
} // func

// ------------------


// $$ } // namespace coresysbasemem