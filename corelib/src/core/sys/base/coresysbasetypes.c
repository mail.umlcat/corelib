/** Module: "coresysbasetem.*"
 ** File:   "coresysbasetypes.c"
 ** Descr.: "Is a 'coresysbasetem.*' complement."
 **/

// $$ namespace coresysbasetypes
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "coresysbasemacros.h"
#include "coresysbaseerrorcodes.h"
#include "coresysbaseenums.h"
#include "coresysbasemem.h"
#include "coresysbaseuuidtypes.h"

// ------------------


// ------------------

// $$ } // namespace coresysbasetypes