/** Module: "coresysbasemem.h"
 ** Descr.:
 ** "Memory Management Library."
 **/

// $$ namespace coresysbasemem
// $$ {
 
// ------------------
 
#ifndef CORESYSBASEMEM_H
#define CORESYSBASEMEM_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "coresysbasemacros.h"
#include "coresysbaseerrorcodes.h"
#include "coresysbaseenums.h"

// ------------------

/* $$ inline */ bool /* $$ func */ coresysbasemem_isassigned
  (/* $$ in */ const pointer* /* $$ param */ ASource);

/* $$ inline */ bool /* $$ func */ coresysbasemem_isassignedsize
  (/* $$ in */ const pointer* /* $$ param */ ASourceBuffer, 
   /* $$ in */ const size_t   /* $$ param */ ASourceSize);

// ------------------

bool /* $$ func */ coresysbasemem_tryclear
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    size_t   /* $$ param */ ACount);

bool /* $$ func */ coresysbasemem_trycleararray
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    size_t   /* $$ param */ AItemSize,   
   /* $$ in */  const    count_t  /* $$ param */ AItemCount);

bool /* $$ func */ coresysbasemem_tryfill
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    uint8_t  /* $$ param */ AValue,
   /* $$ in */  const    size_t   /* $$ param */ ACount);

bool /* $$ func */ coresysbasemem_tryfillarray
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    uint8_t  /* $$ param */ AValue,
   /* $$ in */  const    size_t   /* $$ param */ AItemSize,   
   /* $$ in */  const    count_t  /* $$ param */ AItemCount);

bool /* $$ func */ coresysbasemem_trycopy
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    pointer* /* $$ param */ ASource,
   /* $$ in */  const    size_t   /* $$ param */ ACount);

bool /* $$ func */ coresysbasemem_trycopyarray
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    pointer* /* $$ param */ ASource,
   /* $$ in */  const    size_t   /* $$ param */ AItemSize,   
   /* $$ in */  const    count_t  /* $$ param */ AItemCount);

bool /* $$ func */ coresysbasemem_tryrevcopyarray
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    pointer* /* $$ param */ ASource,
   /* $$ in */  const    size_t   /* $$ param */ AItemSize,   
   /* $$ in */  const    count_t  /* $$ param */ AItemCount);

bool /* $$ func */ coresysbasemem_trycompare
  (/* $$ out */ nonconst enum comparison* /* $$ param */ AComparison,
   /* $$ out */ const    pointer*         /* $$ param */ A,
   /* $$ in */  const    pointer*         /* $$ param */ B,
   /* $$ in */  const    count_t          /* $$ param */ ACount);

bool /* $$ func */ coresysbasemem_trycomparearray
  (/* $$ out */ nonconst enum comparison* /* $$ param */ AComparison,
   /* $$ out */ const    pointer*         /* $$ param */ A,
   /* $$ in */  const    pointer*         /* $$ param */ B,
   /* $$ in */  const    size_t           /* $$ param */ AItemSize,   
   /* $$ in */  const    count_t          /* $$ param */ AItemCount);

bool /* $$ func */ coresysbasemem_tryreversearray
  (/* $$ out */ const    pointer* /* $$ param */ ASource,
   /* $$ in */  const    size_t   /* $$ param */ AItemSize,   
   /* $$ in */  const    count_t  /* $$ param */ AItemCount);
   
// ------------------

bool /* $$ func */ coresysbasemem_areequal
  (/* $$ in */ const pointer* /* $$ param */ A,
   /* $$ in */ const pointer* /* $$ param */ B,
   /* $$ in */ const size_t   /* $$ param */ AItemSize);

bool /* $$ func */ coresysbasemem_areequalarray
  (/* $$ in */ const pointer* /* $$ param */ A,
   /* $$ in */ const pointer* /* $$ param */ B,
   /* $$ in */ const size_t   /* $$ param */ AItemSize,
   /* $$ in */ const count_t  /* $$ param */ AItemCount);
   
enum comparison /* $$ func */ coresysbasemem_compare
  (/* $$ out */ const pointer* /* $$ param */ A,
   /* $$ in */  const pointer* /* $$ param */ B,
   /* $$ in */  const count_t  /* $$ param */ ACount);
   
enum comparison /* $$ func */ coresysbasemem_comparearray
  (/* $$ out */ const pointer* /* $$ param */ A,
   /* $$ in */  const pointer* /* $$ param */ B,
   /* $$ in */  const size_t   /* $$ param */ AItemSize,   
   /* $$ in */  const count_t  /* $$ param */ AItemCount);

pointer* /* $$ func */ coresysbasemem_allocate
  (/* $$ in */ const size_t  /* $$ param */ AItemSize);

pointer* /* $$ func */ coresysbasemem_allocatearray
  (/* $$ in */ const size_t  /* $$ param */ AItemSize,
   /* $$ in */ const count_t /* $$ param */ AItemCount);

pointer* /* $$ func */ coresysbasemem_allocateclear
  (/* $$ in */ const size_t /* $$ param */ ASize);
  
pointer* /* $$ func */ coresysbasemem_allocatearrayclear
  (/* $$ in */ const size_t  /* $$ param */ AItemSize,
   /* $$ in */ const count_t /* $$ param */ AItemCount);
   
pointer* /* $$ func */ coresysbasemem_duplicatecopy
  (/* $$ in */ const pointer* /* $$ param */ ASourceBuffer,
   /* $$ in */ const size_t   /* $$ param */ ASourceSize);

pointer* /* $$ func */ coresysbasemem_duplicatearraycopy
  (/* $$ in */ const pointer* /* $$ param */ ASourceBuffer,
   /* $$ in */ const size_t   /* $$ param */ AItemSize,
   /* $$ in */ const count_t  /* $$ param */ AItemCount);
   
// ------------------

/* $$ inline */ size_t /* $$ func */ coresysmem_minsize
  (/* $$ in */ const size_t /* $$ param */ A,
   /* $$ in */ const size_t /* $$ param */ B);

/* $$ inline */ size_t /* $$ func */ coresysmem_maxsize
  (/* $$ in */ const size_t /* $$ param */ A,
   /* $$ in */ const size_t /* $$ param */ B);

/* $$ inline */ size_t /* $$ func */ coresysbasemem_zerominsize
  (/* $$ in */ const size_t /* $$ param */ A,
   /* $$ in */ const size_t /* $$ param */ B);

/* $$ inline */ size_t /* $$ func */ coresysbasemem_zeromaxsize
  (/* $$ in */ const size_t /* $$ param */ A,
   /* $$ in */ const size_t /* $$ param */ B);

/* $$ inline */ size_t /* $$ func */ coresysbasemem_oneminsize
  (/* $$ in */ const size_t /* $$ param */ A,
   /* $$ in */ const size_t /* $$ param */ B);

/* $$ inline */ size_t /* $$ func */ coresysbasemem_onemaxsize
  (/* $$ in */ const size_t /* $$ param */ A,
   /* $$ in */ const size_t /* $$ param */ B);

// ------------------

void /* $$ func */ coresysbasemem_deallocate
  (/* $$ out */ nonconst pointer** /* $$ param */ ADest,
   /* $$ in */  const size_t       /* $$ param */ ASize);

void /* $$ func */ coresysbasemem_deallocaterray
  (/* $$ out */ nonconst pointer** /* $$ param */ ADest,
   /* $$ in */  const    size_t    /* $$ param */ AItemSize,
   /* $$ in */  const    count_t   /* $$ param */ AItemCount);

void /* $$ func */ coresysbasemem_deallocateclear
  (/* $$ out */ nonconst pointer** /* $$ param */ ADest,
   /* $$ in */  const    size_t    /* $$ param */ ASize);

void /* $$ func */ coresysbasemem_deallocatearrayclear
  (/* $$ out */ nonconst pointer** /* $$ param */ ADest,
   /* $$ in */  const size_t       /* $$ param */ AItemSize,
   /* $$ in */  const count_t      /* $$ param */ AItemCount);
   
void /* $$ func */ coresysbasemem_clear
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    size_t   /* $$ param */ ACount);
   
void /* $$ func */ coresysbasemem_cleararray
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const size_t      /* $$ param */ AItemSize,
   /* $$ in */  const count_t     /* $$ param */ AItemCount);

void /* $$ func */ coresysbasemem_fill
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    uint8_t  /* $$ param */ AValue,
   /* $$ in */  const    size_t   /* $$ param */ ACount);

void /* $$ func */ coresysbasemem_fillarray
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    uint8_t  /* $$ param */ AValue, 
   /* $$ in */  const    size_t   /* $$ param */ AItemSize,
   /* $$ in */  const    count_t  /* $$ param */ AItemCount);
   
void /* $$ func */ coresysbasemem_copy
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    pointer* /* $$ param */ ASource,
   /* $$ in */  const    size_t   /* $$ param */ ACount);
   
void /* $$ func */ coresysbasemem_copyarray
  (/* $$ out */ nonconst pointer* /* $$ param */ ADest,
   /* $$ in */  const    pointer* /* $$ param */ ASource,
   /* $$ in */  const    size_t   /* $$ param */ AItemSize,
   /* $$ in */  const    count_t  /* $$ param */ AItemCount);
   
// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysbasemem_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysbasemem_finish
  ( noparams );

// ------------------

#endif // CORESYSBASEMEM_H

// $$ } // namespace coresysbasemem