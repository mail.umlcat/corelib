/** Module: "coresysbasememinttypes.c"
 ** Descr.:
 ** "Memory Types Library."
 **/

// $$ namespace coresysbasememinttypes
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "coresysbasemacros.h"
#include "coresysbaseerrorcodes.h"
#include "coresysbaseenums.h"

//#include "coresysbaseuuids.h"
//#include "coresysbasetypecodes.h"
//#include "coresysbasemodcodes.h"
//#include "coresysbasetypes.h"
//#include "coresysbasesints.h"
//#include "coresysbaseuints.h"

// ------------------
 
#include "coresysbasememinttypes.h"

// ------------------


 // ...

// ------------------

/* $$ override */ int /* $$ func */ coresysbasememinttypes_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  // coresysbasetem_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysbasememinttypes_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  // coresysbasetem_nothing();

  // ---
  return Result;
} // func

// ------------------


// $$ } // namespace coresysbasememinttypes