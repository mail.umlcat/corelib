/** Module: "coresysbase.h"
 ** File:   "coresysbaseuuidtypes.h"
 ** Descr.: "Is a 'coresysbasetem.*' complement."
 **/

// $$ namespace coresysbaseuuidtypes
// $$ {
 
// ------------------
 
#ifndef CORESYSBASEUUIDTYPES_H
#define CORESYSBASEUUIDTYPES_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// unique identifier
typedef
  uint32_t    /* as */ uuid[4];

typedef
  uuid        /* as */ uuid_t;
typedef
  uuid*       /* as */ uuid_p;

typedef
  uuid_t      /* as */ system_uuid_t;
typedef
  uuid*       /* as */ system_uuid_p;

#define nouuid 0

#define coresysbaseuuidtypes_nouuid 0

// ------------------

#endif // CORESYSBASEUUIDTYPES_H

// $$ } // namespace coresysbaseuuidtypes