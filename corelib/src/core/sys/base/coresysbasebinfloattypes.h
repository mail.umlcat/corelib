/** Module: "coresysbasebinfloattypes.h"
 ** File:   "coresysbasebinfloattypes.h"
 ** Descr.: "Is a 'coresysbase.*' complement."
 **/
 
// $$ namespace coresysbasebinfloattypes
// $$ {
 
// ------------------
 
#ifndef CORESYSBASEBINFLOATTYPES_H
#define CORESYSBASEBINFLOATTYPES_H
 
// ------------------

typedef
  memint16_t   /* as */ binfloat16_t;
typedef
  memint16_t*  /* as */ binfloat16_p;

typedef
  memint32_t   /* as */ binfloat32_t;
typedef
  memint32_t*  /* as */ binfloat32_p;

typedef
  memint64_t   /* as */ binfloat64_t;
typedef
  memint64_t*  /* as */ binfloat64_p;

typedef
  memint128_t  /* as */ binfloat128_t;
typedef
  memint128_t* /* as */ binfloat128_p;

typedef
  memint256_t  /* as */ binfloat256_t;
typedef
  memint256_t* /* as */ binfloat256_p;

// ------------------

#endif // CORESYSBASEBINFLOATTYPES_H

// $$ } // namespace coresysbasebinfloattypes
