/** Module: "coresysbasetem.h"
 ** File:   "coresysbasesinttypes.h"
 ** Descr.: "Is a 'coresysbasetem.*' complement."
 **/

// $$ namespace coresysbasesinttypes
// $$ {
 
// ------------------
 
#ifndef CORESYSBASESINTTYPES_H
#define CORESYSBASESINTTYPES_H
 
// ------------------

// add includes ? {
typedef
  int8_t   /* as */ sint8_t;
typedef
  int16_t  /* as */ sint16_t;
typedef
  int32_t  /* as */ sint32_t;
typedef
  int64_t  /* as */ sint64_t;

// ------------------

typedef
  sint8_t   /* as */ sint8_p;
typedef
  sint16_t  /* as */ sint16_p;
typedef
  sint32_t  /* as */ sint32_p;
typedef
  sint64_t  /* as */ sint64_p;

// add includes ? }

// ------------------

// used for floats
// and other CPU architectures
typedef
  sint8_t        /* as */ sint24_t[3];
typedef
  sint24_t*      /* as */ sint24_p;

// used for floats
// and other CPU architectures
typedef
  sint8_t        /* as */ sint48_t[6];
typedef
  sint48_t*      /* as */ sint48_p;

// used for floats
// and other CPU architectures
typedef
  sint8_t        /* as */ sint80_t[10];
typedef
  sint80_t*      /* as */ sint80_p;

typedef
  sint8_t        /* as */ sint128_t[64];
typedef
  sint128_t*     /* as */ sint128_p;

typedef
  sint8_t        /* as */ sint256_t[32];
typedef
  sint256_t*     /* as */ sint256_p;

// ------------------

// typedef
//   sint8_t*       /* as */ sint8_p;
// typedef
//   sint16_t*      /* as */ sint16_p;
// typedef
//   sint32_t*      /* as */ sint32_p;
// typedef
//   sint64_t*      /* as */ sint64_p;

// clang?

// ------------------

//ifndef <SOME_PLATFORM_DEPENDANT_DIRECTIVE>
//define <SOME_PLATFORM_DEPENDANT_DIRECTIVE>

typedef
  sint256_t  /* as */ maxsfwsint_t;
typedef
  sint256_t* /* as */ maxsfwsint_p;

//endif // <SOME_PLATFORM_DEPENDANT_DIRECTIVE>

// ------------------

//ifndef <SOME_PLATFORM_DEPENDANT_DIRECTIVE>
//define <SOME_PLATFORM_DEPENDANT_DIRECTIVE>

typedef
  sint32_t  /* as */ maxhdwsint_t;
typedef
  sint32_t* /* as */ maxhdwsint_p;

//endif // <SOME_PLATFORM_DEPENDANT_DIRECTIVE>

// ------------------

#endif // CORESYSBASESINTTYPES_H

// $$ } // namespace coresysbasesinttypes
