/** Module: "coresysbase"
 ** Filename: "coresysbase.h"
 ** Qualified Identifier:
 ** "core::system" 
 ** Descr.:
 ** "System Package Module"
 **/

// $$ namespace coresysbase
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "coresysbasemacros.h"
#include "coresysbaseerrorcodes.h"
#include "coresysbaseenums.h"
#include "coresysbasemem.h"
#include "coresysbaseuuidtypes.h"
#include "coresysbasetypes.h"
#include "coresysbasemodules.h"
#include "coresysbasesinttypes.h"
#include "coresysbaseuinttypes.h"
#include "coresysbasememinttypes.h"
#include "coresysbasedecfloattypes.h"
#include "coresysbasebinfloattypes.h"
#include "coresysbaseastrtypes.h"
#include "coresysbasewstrtypes.h"
#include "coresysbasefuncs.h"
#include "coresysbasefunctors.h"
#include "coresysbaseparams.h"

// ------------------

// --> other "corelib" libraries here
// #include <put your library here>
// #include <put your library here>
// #include <put your library here>

// ------------------

// --> other NON "corelib" libraries here
// #include <put your library here>
// #include <put your library here>
// #include <put your library here>

// ------------------

// --> this module own header goes here
#include "coresysbase.h"
 
// ------------------


 // ...

// ------------------

/* $$ override */ int /* $$ func */ coresysbase_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasetem_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysbase_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasetem_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresysbase