/** Module: "coresysbasetem.h"
 ** File:   "coresysbasemacros.h"
 ** Descr.: "Is a 'coresysbasetem.*' complement."
 **/

// $$ namespace coresysbasemacros
// $$ {
 
// ------------------
 
#ifndef CORESYSBASEMACROS_H
#define CORESYSBASEMACROS_H
 
// ------------------

// empty value for pointers
#define undefined NULL

// ------------------

// empty value for functors
#define nosize 0

// ------------------

// activate "const"
#define enconst /* const */
// deactivate "const"
#define deconst /* nonconst */

// ------------------

// "void SomeFunc ( noparams );" =>
// "void SomeFunc ( void );"
#define noparams void

// ------------------

// "noresult SomeFunc (void* x);" =>
// "void     SomeFunc (void* x);"
#define noresult void

// ------------------

// "int SomeFunc (int deref var x);" =>
// "int SomeFunc (int deref var x);"

// "y = z + (deref x + 1);" =>
// "y = z + (* x + 1);"
#define asref *

// ------------------

// "y = SomeFunc(ref y);" =>
// "y = SomeFunc(& y);"
#define intoref &

// ------------------

// "pointer* SomeFunc ( void );" =>
// "void*    SomeFunc ( void );"
typedef
  void*     /* as */ pointer;

// ------------------

// indicate a function
// that explicit returns a value
#define discard /* discard */

// ------------------

// complement "const" parameters
// void somefunc ( nonconst char* item);
#define nonconst

// ------------------

// @toxdo: check compiler

// "restrict" pointer
#define restricted
#define unrestricted

// ------------------


// ------------------

#endif // CORESYSBASEMACROS_H

// $$ } // namespace coresysbasemacros