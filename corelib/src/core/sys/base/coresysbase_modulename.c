/** Module: "coresysbase<modulename>s.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysbase<modulename>s
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

// --> same package libraries here
// .include "coresysbase<modulename>sbinfloat16s.h"
// .include "coresysbase<modulename>sbinfloat32s.h"
// .include "coresysbase<modulename>sbinfloat64s.h"
// .include "coresysbase<modulename>sbinfloat128s.h"
// .include "coresysbase<modulename>sbinfloat256s.h"
// .include "coresysbase<modulename>slongfloats.h"

// ------------------

// --> this same module header goes here
#include "coresysbase<modulename>s.h"
 
// ------------------

/* types */

// ------------------

/* functions */

// ------------------

/* procedures */

// ------------------

/* rtti */

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysbase<modulename>s_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysbase<modulename>s_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresysbase<modulename>s