/** Module: "coresysbasetem.h"
 ** File:   "coresysbaseuinttypes.h"
 ** Descr.: "Is a 'coresysbasetem.*' complement."
 **/
 
// $$ namespace coresysbaseuinttypes
// $$ {
 
// ------------------
 
#ifndef CORESYSBASEUINTTYPES_H
#define CORESYSBASEUINTTYPES_H
 
// ------------------

// used for floats
// and other CPU architectures
typedef
  uint8_t        /* as */ uint24_t[3];
typedef
  uint24_t*      /* as */ uint24_p;

// used for floats
// and other CPU architectures
typedef
  uint8_t        /* as */ uint48_t[6];
typedef
  uint48_t*      /* as */ uint48_p;

// used for floats
// and other CPU architectures
typedef
  uint8_t        /* as */ uint80_t[10];
typedef
  uint80_t*      /* as */ uint80_p;

typedef
  uint8_t        /* as */ uint128_t[64];
typedef
  uint128_t*     /* as */ uint128_p;

typedef
  uint8_t        /* as */ uint256_t[32];
typedef
  uint256_t*     /* as */ uint256_p;

// ------------------

// clang?

typedef
  uint8_t*       /* as */ uint8_p;
typedef
  uint16_t*      /* as */ uint16_p;
typedef
  uint32_t*      /* as */ uint32_p;
typedef
  uint64_t*      /* as */ uint64_p;

// ------------------

//ifndef <SOME_PLATFORM_DEPENDANT_DIRECTIVE>
//define <SOME_PLATFORM_DEPENDANT_DIRECTIVE>

typedef
  uint256_t  /* as */ maxsfwuint_t;
typedef
  uint256_t* /* as */ maxsfwuint_p;

//endif // <SOME_PLATFORM_DEPENDANT_DIRECTIVE>

// ------------------

//ifndef <SOME_PLATFORM_DEPENDANT_DIRECTIVE>
//define <SOME_PLATFORM_DEPENDANT_DIRECTIVE>

typedef
  uint32_t  /* as */ maxhdwuint_t;
typedef
  uint32_t* /* as */ maxhdwuint_p;

//endif // <SOME_PLATFORM_DEPENDANT_DIRECTIVE>

// ------------------

#endif // CORESYSBASEUINTTYPES_H

// $$ } // namespace coresysbaseuinttypes
