/** Module: "coresysbasetem.h"
 ** File:   "coresysbasetypes.h"
 ** Descr.: "Is a 'coresystem.*' complement."
 **/

// $$ namespace coresysbasetypes
// $$ {
 
// ------------------
 
#ifndef CORESYSBASETYPES_H
#define CORESYSBASETYPES_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "coresysbasemacros.h"
#include "coresysbaseerrorcodes.h"
#include "coresysbaseenums.h"
#include "coresysbasemem.h"
#include "coresysbaseuuidtypes.h"

// ------------------

// identifier for types, not pointer
// "typeid" is already taken
typedef
  uuid         /* as */ typecode;

typedef
  typecode     /* as */ typecode_t;
typedef
  typecode*    /* as */ typecode_p;

// ------------------

#define coresysbasetypes_typecode typecode

#define coresysbasetypes_typecode_t typecode_t

#define coresysbasetypes_typecode_p typecode_p

// ------------------

#define unknowntype 0

#define coresysbasetypes_unknowntype 0

// ------------------


#endif // CORESYSBASETYPES_H

// $$ } // namespace coresysbasetypes