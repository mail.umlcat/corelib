/** Module: "coresysbasetem.h"
 ** File:   "coresysbasewstrtypes.h"
 ** Descr.: "Is a 'coresysbasetem.*' complement."
 **/

// $$ namespace coresysbasewstrtypes
// $$ {
 
// ------------------
 
#ifndef CORESYSBASEWSTRTYPES_H
#define CORESYSBASEWSTRTYPES_H
 
// ------------------

typedef
  uint16_t /* as */ widechar;
  
typedef
  widechar /* as */ widenullstring;

// ------------------

/* types */

#define WIDECHARSETMAXSIZE 65532

typedef
  widechar  /* as */ widecharset[WIDECHARSETMAXSIZE];
  
// ------------------

#define WIDENULLCHAR L'\0'

#define CORESYSBASE_WIDENULLCHAR L'\0'

// ------------------


// ------------------

#endif // CORESYSBASEWSTRTYPES_H

// $$ } // namespace coresysbasewstrtypes

