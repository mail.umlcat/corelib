/** Module: "coresysbasefuncs.c"
 ** Descr.:
 ** "<put description here>"
 ** "<put description here>"
 ** "<put description here>"
 ** "<put description here>"
 **/

// $$ namespace coresysbasefuncs
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresysbasemacros.h"
#include "coresysbaseuuidtypes.h"
#include "coresysbaseenums.h"
#include "coresysbaseastrtypes.h"
#include "coresysbasewstrtypes.h"
#include "coresysbaseerrorcodes.h"

// ------------------

// --> other "corelib" libraries here
// #include <put your library here>
// #include <put your library here>
// #include <put your library here>

// ------------------

// --> other NON "corelib" libraries here
// #include <put your library here>
// #include <put your library here>
// #include <put your library here>

// ------------------

// --> this module own header goes here
#include "coresysbasefuncs.h"
 
// ------------------


void /* $$ func */ coresysbasefuncs_nothing
  ( noparams )
{
  // explicitly does nothing !!!
} // func

// ------------------

void /* $$ func */ coresysbasefuncs_exit
  (/* $$ in */ const int /* $$ param */ AExitCode)
{     
  exit(AExitCode);
} // func

void /* $$ func */ coresysbasefuncs_exitdeprecated
  ( noparams )
{
  int /* $$ var */ AExitCode =
    ERRORCODE_DEPRECATED;

  exit(AExitCode);
} // func

void /* $$ func */ coresysbasefuncs_exitabstract
  ( noparams )
{     
  int /* $$ var */ AExitCode =
    ERRORCODE_ABSTRACT;

  exit(AExitCode);
} // func

void /* $$ func */ coresysbasefuncs_underconstruction
  ( noparams )
{     
  int /* $$ var */ AExitCode =
    ERRORCODE_UNDERCONSTRUCTION;

  exit(AExitCode);
} // func

// -------------------

enum comparison /* $$ func */ coresysbasefuncs_inttocmp
  (/* $$ in */ const int /* $$ param */ AExitCode)
{
  enum comparison /* $$ var */ Result = comparison_equal;
  // ---
  
  if (AExitCode < 0)
    Result = comparison_lesser;
  else if (AExitCode > 0)
    Result = comparison_greater;
  else if (AExitCode > 0)
    Result = comparison_equal;
    
  // ---
  return Result;
} // func

// ------------------

 // ...

// ------------------

/* $$ override */ int /* $$ func */ coresysbasefuncs_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysbasefuncs_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresysbasefuncs