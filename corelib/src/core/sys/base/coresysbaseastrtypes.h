 /** Module: "coresysbasetem.h"
 ** File:   "coresysbaseastrtypes.h"
 ** Descr.: "Is a 'coresysbasetem.*' complement."
 **/

// $$ namespace coresysbaseastrtypes
// $$ {
 
// ------------------
 
#ifndef CORESYSBASEASTRTYPES_H
#define CORESYSBASEASTRTYPES_H
 
// ------------------

typedef
  unsigned char  /* as */ ansichar;
typedef
  ansichar       /* as */ ansinullstring;
  
// ------------------

/* types */

#define ANSICHARSETMAXSIZE 256

typedef
  ansichar  /* as */ ansicharset[256];
  
// ------------------

struct ansicharrange
{
  ansichar /* $$ var */ lo, hi;
} ;

// ------------------

#define ANSINULLCHAR '\0'
#define CORESYSBASE_ANSINULLCHAR '\0'


// ------------------



// ------------------

#endif // CORESYSBASEASTRTYPES_H

// $$ } // namespace coresysbaseastrtypes