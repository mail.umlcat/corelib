/** Module: "coresysbasetem.h"
 ** File:   "coresysbasemodules.h"
 ** Descr.: "Is a 'coresysbasetem.*' complement."
 **/

// $$ namespace coresysbasemodules
// $$ {
 
// ------------------
 
#ifndef CORESYSBASEMODULES_H
#define CORESYSBASEMODULES_H
 
// ------------------

#define unknowntype 0

// identifier for modules
typedef
  uuid         /* as */ modulecode;

typedef
  modulecode   /* as */ modulecode_t;
typedef
  modulecode*  /* as */ modulecode_p;

// ------------------

#define unknownmodule 0

#define coresysbasemodules_unknownmodule 0

// ------------------

#endif // CORESYSBASEMODULES_H

// $$ } // namespace coresysbasemodules