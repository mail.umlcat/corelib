/** Module: "coresysbaseenumtypes.h"
 ** Descr.:
 ** "<put description here>"
 ** "<put description here>"
 ** "<put description here>"
 ** "<put description here>"
 **/

// $$ namespace coresysbaseenums
// $$ {
 
// ------------------
 
#ifndef CORESYSBASEENUMS_H
#define CORESYSBASEENUMS_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "coresysbasemacros.h"

// ------------------

// --> other "corelib" libraries here
// #include <put your library here>
// #include <put your library here>
// #include <put your library here>

// ------------------

// --> other NON "corelib" libraries here
// #include <put your library here>
// #include <put your library here>
// #include <put your library here>

// ------------------

enum stringcasemodes
{
  // mode not assigned
  stringcasemodes_none,
  
  // "Hello","HELLO","hello",
  // are considered the same text
  stringcasemodes_insensitive,

  // "Hello","HELLO","hello",
  // can be inserted as different text
  stringcasemodes_sensitive,

  // "Hello","HELLO","hello",
  // are always stored and compare as "hello"
  stringcasemodes_lowercase,

  // "Hello","HELLO","hello",
  // are always stored and compare as "HELLO"
  stringcasemodes_uppercase,
} ;

// ------------------

enum comparison
{
  comparison_unassigned  = -99,
  comparison_lesser      = -1,
  comparison_equal       = 0,
  comparison_greater     = +1,
} ;

// ------------------

typedef
  size_t   /* as */ errorcode_t;

// ------------------

// similar to "Java" exception (s)
typedef
  pointer* /* as */ warning;

// ------------------

typedef
  pointer* /* as */ varargs;

// ------------------

typedef
  size_t   index_t;

typedef
  size_t   coresysbase_index_t;
  
typedef
  size_t   count_t;
  
typedef
  size_t   coresysbase_count_t;
  
// ------------------

typedef
  FILE /* as */ coresysbase_file;

typedef
  FILE /* as */ coresysbaseenums_file;
  
// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysbaseenums_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysbaseenums_finish
  ( noparams );

// ------------------

#endif // CORESYSBASEENUMS_H

// $$ } // namespace coresysbaseenums