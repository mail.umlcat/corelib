/** Module: "coresysbasememinttypes.h"
 ** Descr.:
 ** "Memory Types Library."
 **/

// $$ namespace coresysbasememinttypes
// $$ {
 
// ------------------
 
#ifndef CORESYSBASEMEMINTTYPES_H
#define CORESYSBASEMEMINTTYPES_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "coresysbasemacros.h"
#include "coresysbaseerrorcodes.h"
#include "coresysbaseenums.h"

//#include "coresysbaseuuids.h"
//#include "coresysbasetypecodes.h"
//#include "coresysbasemodcodes.h"
//#include "coresysbasetypes.h"
//#include "coresysbasesints.h"
//#include "coresysbaseuints.h"

// ------------------

//#include "corememint8s.h"
//#include "corememint16s.h"
//#include "corememint32s.h"
//#include "corememint64s.h"
//#include "corememint128s.h"
//#include "corememint256s.h"
//#include "corememint48s.h"
//#include "corememint80s.h"

// ------------------

//enum memoryintegers
//{
//  memoryintegers_8_t,    
//  memoryintegers_16_t,   
//  memoryintegers_32_t,   
//  memoryintegers_64_t,   
//  memoryintegers_128_t,  
//  memoryintegers_256_t,  
//  memoryintegers_48_t,
//  memoryintegers_80_t,
//} ;
//
//typedef
//  enum memoryintegers /* as */ coresysbasememinttypes_memoryintegers;

// ------------------

// used to separate memory allocation
// from math arithmetic

typedef
  uint8_t       /* as */ memint8_t;
typedef
  uint8_t*      /* as */ memint8_p;
  
typedef
  uint16_t      /* as */ memint16_t;
typedef
  uint16_t*     /* as */ memint16_p;

typedef
  uint32_t      /* as */ memint32_t;
typedef
  uint32_t*     /* as */ memint32_p;
  
typedef
  uint64_t      /* as */ memint64_t;
typedef
  uint64_t*     /* as */ memint64_p;

typedef
  uint32_t      /* as */ memint128_t[4];
typedef
  memint128_t*  /* as */ memint128_p;

typedef
  uint32_t      /* as */ memint256_t[8];
typedef
  memint256_t*  /* as */ memint256_p;

// ------------------

// commonly used aliases
typedef
  memint8_t   /* as */ byte_t;
typedef
  memint8_p   /* as */ byte_p;
  
typedef
  memint16_t  /* as */ word_t;
typedef
  memint16_p  /* as */ word_p;

typedef
  memint32_t  /* as */ dword_t;
typedef
  memint32_p  /* as */ dword_p;
  
typedef
  memint64_t  /* as */ qword_t;
typedef
  memint64_p  /* as */ qword_p;

typedef
  memint128_t /* as */ oword_t;
typedef
  memint128_p /* as */ oword_p;

// ------------------

// used for floats
typedef
  uint8_t        /* as */ memint24_t[3];
typedef
  memint24_t*    /* as */ memint24_p;

// used for floats
typedef
  uint8_t        /* as */ memint48_t[6];
typedef
  memint48_t*    /* as */ memint48_p;

// used for floats
typedef
  uint8_t        /* as */ memint80_t[10];
typedef
  memint80_t*    /* as */ memint80_p;

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysbasememinttypes_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysbasememinttypes_finish
  ( noparams );

// ------------------

#endif // CORESYSBASEMEMINTTYPES_H

// $$ } // namespace coresysbasememinttypes