/** Module: "coresysbaseparams.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysbaseparams
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
// .include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
// .include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
// .include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
// .include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

// --> same package libraries here
#include "coresysbasemacros.h"
#include "coresysbaseuuidtypes.h"
#include "coresysbasetypes.h"
#include "coresysbaseastrtypes.h"
#include "coresysbasewstrtypes.h"
#include "coresysbasefuncs.h"

// ------------------

// --> this same module header goes here
#include "coresysbaseparams.h"

// ------------------

/* functions */

count_t /* $$ func */ coresysbaseparams_paramsreadmaxcount
  (/* $$ in */ const constparams /* $$ param */ AParams)
{
  count_t /* $$ var */ Result = 0;
  // ---
 
  struct constparamsheader* /* $$ var */ AParamsHeader = NULL;
  
  // ---

  AParamsHeader =
    (struct constparamsheader*) AParams;
  if (AParamsHeader != NULL)
  {
    Result = 
      AParamsHeader->ItemsMaxCount;
  }
     
  // ---
  return Result;
} // func

count_t /* $$ func */ coresysbaseparams_paramsreadcurrcount
  (/* $$ in */ const constparams /* $$ param */ AParams)
{
  count_t /* $$ var */ Result = 0;
  // ---
 
  struct constparamsheader* /* $$ var */ AParamsHeader = NULL;
  
  // ---

  AParamsHeader =
    (struct constparamsheader*) AParams;
  if (AParamsHeader != NULL)
  {
    Result = 
      AParamsHeader->ItemsCurrCount;
  }
     
  // ---
  return Result;
} // func
  
constparam /* $$ func */ coresysbaseparams_paramsreadparamat
  (/* $$ in */ const constparams /* $$ param */ AParams,
   /* $$ in */ const index_t     /* $$ param */ AIndex)
{
  constparam /* $$ var */ Result = NULL;
  // ---
 
  bool               /* $$ var */ CanContinue = false;
  struct constparamsheader* /* $$ var */ AParamsHeader = NULL;
  constparams        /* $$ var */ AItems = NULL;
  count_t      /* $$ var */ ACurrCount = 0;
  
  // ---

  AParamsHeader =
    (struct constparamsheader*) AParams;
  CanContinue =
    (AParamsHeader != NULL);
  if (CanContinue)
  {
    ACurrCount =
      AParamsHeader->ItemsCurrCount;
    
    CanContinue =
      (AIndex < ACurrCount);
    if (CanContinue)
    {
      AItems = 
        AParamsHeader->Items;
      
      Result = 
        (constparam) AItems[AIndex];
    } // if (CanContinue)
  } // if (CanContinue)
     
  // ---
  return Result;
} // func
  
constparams /* $$ func */ coresysbaseparams_paramscreate
  (/* $$ in */ const count_t /* $$ param */ AMaxCount)
{
  constparams /* $$ var */ Result = NULL;
  // ---
 
  bool               /* $$ var */ CanContinue = false;
  struct constparamsheader* /* $$ var */ AParamsHeader = NULL;
  
  // ---
  
  CanContinue =
    (AMaxCount > 0);
  if (CanContinue)
  {
    AParamsHeader =
	    (struct constparamsheader*) coresysbasemem_allocateclear
        (sizeof(struct constparamsheader));
	  
    AParamsHeader->ItemsMaxCount =
	    AMaxCount;
	  
    AParamsHeader->ItemsCurrCount =
	    0;
	  
    AParamsHeader->Items =
      coresysbasemem_allocatearrayclear
        (sizeof(pointer*), AMaxCount);
	  
    Result =
      (constparams) AParamsHeader;
  } // if (CanContinue)
  
  // ---
  return Result;
} // func
  
bool /* $$ func */ coresysbaseparams_paramstryadd
  (/* $$ inout */ nonconst constparams /* $$ param */ AParams,
   /* $$ in */    const    constparam  /* $$ param */ AItem)
{
  bool /* $$ var */ Result = false;
  
  // ---
  
  struct constparamsheader* /* $$ var */ AParamsHeader = NULL;
  constparams* /* $$ var */ AItems = NULL;
  constparam*  /* $$ var */ AItemRef = NULL;
  count_t      /* $$ var */ ACurrCount = 0;
  count_t      /* $$ var */ AMaxCount = 0;
  
  // ---
   
  Result =
    ((AParams != NULL) && (AItem != NULL));
  if (Result)
  {
    AParamsHeader =
	    (struct constparamsheader*) AParams;
    
    ACurrCount =
      AParamsHeader->ItemsCurrCount;
    
    AMaxCount =
      AParamsHeader->ItemsMaxCount;

    Result =
      (ACurrCount < AMaxCount);
    if (Result)
    {
      AItems = 
        (constparam*)AParamsHeader->Items;
    
      AItemRef = 
        (constparam*) &(AItems[ACurrCount]);
      
      AParamsHeader->ItemsCurrCount =
        (AParamsHeader->ItemsCurrCount + 1);
      
      *AItemRef =
  	    (constparam) AItem;
    } // if (Result)
  } // if (Result)
  
  // ---
  return Result;
} // func
  
// ------------------

/* procedures */
  
void /* $$ func */ coresysbaseparams_paramsdrop
  (/* $$ inout */ nonconst constparams asref /* $$ param */ AParamsRef)
{
  bool        /* $$ var */ CanContinue = false;
  constparams /* $$ var */ AParams     = NULL;
  struct constparamsheader* /* $$ var */ AParamsHeader = NULL;
  
  // ---
  
  CanContinue =
   ((AParams != NULL) && (*AParams != NULL));
  if (CanContinue)
  {
    AParams = 
       (asref AParamsRef);
    AParamsHeader =
	    (struct constparamsheader*) AParams;
      
    coresysbasemem_deallocatearrayclear
      (&(AParamsHeader->Items), sizeof(pointer*), AParamsHeader->ItemsMaxCount);

    coresysbasemem_deallocateclear
      ((pointer* asref) intoref AParamsHeader, sizeof(struct constparamsheader*));
  } // if (CanContinue)
} // func  
  
void /* $$ func */ coresysbaseparams_paramsadd
  (/* $$ inout */ nonconst constparams /* $$ param */ AParams,
   /* $$ in */    const    constparam  /* $$ param */ AItem)
{
  bool /* $$ var */ CanContinue = false;
  
  // ---

  CanContinue =
    (coresysbaseparams_paramstryadd(AParams, AItem));
  if (! CanContinue)
  {
    // raise error
    exit(-1);
  } 
} // func  

// ------------------

/* rtti */

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysbaseparams_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysbaseparams_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresysbaseparams