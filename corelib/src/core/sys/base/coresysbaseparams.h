/** Module: "coresysbaseparams.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysbaseparams
// $$ {
 
// ------------------
 
#ifndef CORESYSBASEPARAMS_H
#define CORESYSBASEPARAMS_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
// .include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
// .include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
// .include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
// .include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

// --> same package libraries here
#include "coresysbasemacros.h"
#include "coresysbaseuuidtypes.h"
#include "coresysbasetypes.h"
#include "coresysbaseastrtypes.h"
#include "coresysbasewstrtypes.h"
#include "coresysbasefuncs.h"

// ------------------

/* types */

// --> single parameters

typedef
  pointer* /* as */ constparam;

typedef
  pointer* /* as */ coresysbaseparams_constparam;
  
// ---
  
// --> several parameters

typedef
  pointer* /* as */ constparams;

typedef
  pointer* /* as */ coresysbaseparams_constparams;
  
// ---

struct constparamsheader
{
  // how many items can be stored
  count_t  /* $$ var */ ItemsMaxCount;

  // how many items are currently stored
  count_t  /* $$ var */ ItemsCurrCount;
  
  pointer* /* $$ var */ Items;
  
  // ...
} ; // struct

// ------------------

/* functions */

count_t /* $$ func */ coresysbaseparams_paramsreadmaxcount
  (/* $$ in */ const constparams /* $$ param */ AParams);

count_t /* $$ func */ coresysbaseparams_paramsreadcurrcount
  (/* $$ in */ const constparams /* $$ param */ AParams);
  
constparam /* $$ func */ coresysbaseparams_paramsreadparamat
  (/* $$ in */ const constparams /* $$ param */ AParams,
   /* $$ in */ const index_t     /* $$ param */ AIndex);
  
constparams /* $$ func */ coresysbaseparams_paramscreate
  (/* $$ in */ const count_t /* $$ param */ AMaxCount);
  
bool /* $$ func */ coresysbaseparams_paramstryadd
  (/* $$ inout */ nonconst constparams /* $$ param */ AParams,
   /* $$ in */    const    constparam  /* $$ param */ AItem);
   
// ------------------

/* procedures */
  
void /* $$ func */ coresysbaseparams_paramsdrop
  (/* $$ inout */ nonconst constparams asref /* $$ param */ AParamsRef);
  
void /* $$ func */ coresysbaseparams_paramsadd
  (/* $$ inout */ nonconst constparams /* $$ param */ AParams,
   /* $$ in */    const    constparam  /* $$ param */ AItem);
  
// ------------------

/* rtti */

#define MOD_coresysbaseparams {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysbaseparams_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysbaseparams_finish
  ( noparams );

// ------------------

#endif // CORESYSBASEPARAMS_H

// $$ } // namespace coresysbaseparams