/** Module: "coresysbasefuncs.h"
 ** Descr.:
 ** "<put description here>"
 ** "<put description here>"
 ** "<put description here>"
 ** "<put description here>"
 **/

// $$ namespace coresysbasefuncs
// $$ {
 
// ------------------
 
#ifndef CORESYSBASEFUNCS_H
#define CORESYSBASEFUNCS_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "coresysbasemacros.h"
#include "coresysbaseuuidtypes.h"
#include "coresysbaseenums.h"
#include "coresysbaseastrtypes.h"
#include "coresysbasewstrtypes.h"
#include "coresysbaseerrorcodes.h"

// ------------------

// --> other "corelib" libraries here
// #include <put your library here>
// #include <put your library here>
// #include <put your library here>

// ------------------

// --> other NON "corelib" libraries here
// #include <put your library here>
// #include <put your library here>
// #include <put your library here>

// ------------------

void /* $$ func */ coresysbasefuncs_nothing
  ( noparams );

// ------------------

void /* $$ func */ coresysbasefuncs_exit
  (/* $$ in */ const int /* $$ param */ AExitCode);

void /* $$ func */ coresysbasefuncs_exitdeprecated
  ( noparams );

void /* $$ func */ coresysbasefuncs_exitabstract
  ( noparams );

void /* $$ func */ coresysbasefuncs_underconstruction
  ( noparams );

// ------------------

enum comparison /* $$ func */ coresysbasefuncs_inttocmp
  (/* $$ in */ const int /* $$ param */ AExitCode);

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysbasefuncs_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysbasefuncs_finish
  ( noparams );

// ------------------

#endif // CORESYSBASEFUNCS_H

// $$ } // namespace coresysbasefuncs