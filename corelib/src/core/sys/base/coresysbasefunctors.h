/** Module: "coresysbase	functors.h"
 ** Descr.: "Predefined function pointers."
 **/

// $$ namespace coresysbasefunctors
// $$ {
 
// ------------------
 
#ifndef CORESYSBASEFUNCTORS_H
#define CORESYSBASEFUNCTORS_H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> same package libraries here
#include "coresysbasemacros.h"
#include "coresysbaseuuidtypes.h"
#include "coresysbasetypes.h"
#include "coresysbaseastrtypes.h"
#include "coresysbasewstrtypes.h"

// ------------------

typedef
  bool /* functor */ (*tryprocedural) /* as */
    (/* $$ inout */ pointer* /* $$ param */ AItem);

typedef
  bool /* functor */ (*tryfunctional) /* as */
    (/* $$ inout */ pointer* /* $$ param */ AItem,
     /* $$ out */   bool*    /* $$ param */ AResult);

typedef
  bool /* functor */ (*trycopyable) /* as */
    (/* $$ out */ pointer*       /* $$ param */ ADest,
     /* $$ in */  const pointer* /* $$ param */ ASource);

typedef
  bool /* functor */ (*tryduplicable) /* as */
    (/* $$ in */  const pointer* /* $$ param */ AItem,
     /* $$ out */       pointer* /* $$ param */ AResult);

typedef
  bool /* functor */ (*tryconversion) /* as */
    (/* $$ out */       pointer* /* $$ param */ ADest,
     /* in  */ const pointer* /* $$ param */ ASource);

typedef
  bool /* functor */ (*trycomparefunctor) /* as */
    (/* $$ in */  pointer*    /* $$ param */ A, 
     /* $$ in */  pointer*    /* $$ param */ B,
     /* $$ out */ enum comparison* /* $$ param */ AResult);

typedef
  bool /* functor */ (*tryarequalfunctor) /* as */
    (/* $$ in */  pointer* /* $$ param */ A, 
     /* $$ in */  pointer* /* $$ param */ B,
     /* $$ out */ bool*    /* $$ param */ AResult);

// ------------------

typedef
  void /* functor */ (*procedural) /* as */
    (/* $$ inout */ pointer* /* $$ param */ AItem);

typedef
  bool /* functor */ (*functional) /* as */
    (/* $$ inout */ pointer* /* $$ param */ AItem);

typedef
  void /* functor */ (*copyable) /* as */
    (/* $$ out */ pointer*       /* $$ param */ ADest,
     /* $$ in */  const pointer* /* $$ param */ ASource);

typedef
  pointer* /* functor */ (*duplicatefunctor) /* as */
    (/* $$ in */ const pointer* /* $$ param */ ASource);

typedef
  void /* functor */ (*conversion) /* as */
    (/* $$ out */       pointer* /* $$ param */ ADest,
     /* in  */ const pointer* /* $$ param */ ASource);

typedef
  pointer* /* functor */ (*comparative) /* as */
    (/* $$ in */  pointer*    /* $$ param */ A, 
     /* $$ in */  pointer*    /* $$ param */ B,
     /* $$ out */ enum comparison* /* $$ param */ AResult);

typedef
  pointer* /* functor */ (*operational) /* as */
    (pointer* /* $$ param */ AItem);

typedef
  pointer* /* functor */ (*operationalparam) /* as */
    (pointer* /* $$ param */ AItem,
     pointer* /* $$ param */ AData);

typedef
  int /* functor */ (*process) /* as */
    (pointer* /* $$ param */ AData);
    
typedef
  enum comparison /* functor */ (*comparefunctor) /* as */
    (pointer* /* $$ param */ A, 
     pointer* /* $$ param */ B);
    
typedef
  bool /* functor */ (*areequalfunctor) /* as */
    (pointer* /* $$ param */ A, 
     pointer* /* $$ param */ B);

typedef
  bool /* functor */ (*areequalparamfunctor) /* as */
    (pointer* /* $$ param */ A,
     pointer* /* $$ param */ B,
     pointer* /* $$ param */ AParam);

 // ------------------
 
typedef
  bool /* functor */ (*isdefaultvaluefunctor) /* as */
    (/* $$ in */ const pointer* /* $$ param */ AValue);

typedef
  bool /* functor */ (*tryapplydefaultvaluefunctor) /* as */
    (/* $$ inout */ pointer* /* $$ param */ AValue);

typedef
  void /* functor */ (*applydefaultvaluefunctor) /* as */
    (/* $$ inout */ const pointer* /* $$ param */ AValue);

// ------------------

typedef
  bool /* functor */ (*trybintotxtbyptrfunctor) /* as */
  (/* $$ in */  const /* restricted */ pointer*        /* $$ param */ ASource,
   /* $$ out */ const /* restricted */ ansinullstring* /* $$ param */ ADest);

typedef
  bool /* functor */ (*trybintotxtbyptrcountfunctor) /* as */
  (/* $$ in */  const /* restricted */ pointer*        /* $$ param */ ASource,
   /* $$ out */       /* restricted */ ansinullstring* /* $$ param */ ADest,
   /* $$ in */  const                  size_t          /* $$ param */ ADestSize);

typedef
  bool /* functor */ (*trytxttobinbyptrfunctor) /* as */
   (/* $$ in */  const /* restricted */ ansinullstring* /* $$ param */ ASource,
    /* $$ out */ const /* restricted */ pointer*        /* $$ param */ ADest);

typedef
  bool /* functor */ (*trytxttobinbyptrcountfunctor) /* as */
   (/* $$ in */ const /* restricted */ ansinullstring* /* $$ param */ ASource,
    /* $$ in */ const                  size_t          /* $$ param */ ASourceSize,
    /* $$ in */       /* restricted */ pointer*        /* $$ param */ ADest);
 
// ------------------

typedef
  int /* functor */ (*moduleloaderfunctor) /* as */
    ( noparams );
typedef
  int /* functor */ (*moduleunloaderfunctor) /* as */
    ( noparams );

 
// ------------------


// ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysbasefunctors_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysbasefunctors_finish
  ( noparams );

// ------------------

#endif // CORESYSBASEFUNCTORS_H

// $$ } // namespace coresysbasefunctors