/** Module: "coresysstrsansinullstrs.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysstrsansinullstrs
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"

// ------------------

// --> contained modules here
#include "coresysstrsansicharfatptrs.h"

// ------------------

// --> this same module header goes here
#include "coresysstrsansinullstrs.h"
 
// ------------------

/* functions */

/* inline */ bool /* $$ func */ coresysstrsansinullstrs_isassigned
  (/* $$ in */ nonconst ansinullstring* /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result = 
    (ASource != NULL);
     
  // ---
  return Result;
} // func

/* inline */ bool /* $$ func */ coresysstrsansinullstrs_isassignedsize
  (/* $$ in */ const ansinullstring* /* $$ param */ ASourceBuffer, 
   /* $$ in */ const size_t          /* $$ param */ ASourceSize)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result = 
   (
    (ASourceBuffer != NULL) ||
    (ASourceSize > 0)
   );
     
  // ---
  return Result;
} // func

// ------------------
   
/* inline */ bool /* $$ func */ coresysstrsansinullstrs_isrefassigned
  (/* $$ in */ nonconst ansinullstring* asref /* $$ param */ ADest)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result =
    ( true
      && (ADest != NULL) 
      && ((*ADest) != NULL)
	);
     
  // ---
  return Result;
} // func

/* inline */ bool /* $$ func */ coresysstrsansinullstrs_isrefassignedsize
  (/* $$ in */ nonconst ansinullstring* asref /* $$ param */ ADestBuffer,
   /* $$ in */ const    size_t                /* $$ param */ ADestSize)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result =
    ( true
      && (ADestBuffer != NULL) 
      && ((*ADestBuffer) != NULL)
      && (ADestSize > 0)
	);
     
  // ---
  return Result;
} // func


// ------------------

/* inline */ bool /* $$ func */ coresysstrsansinullstrs_isempty
  (/* $$ in */ const ansinullstring* /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result = 
    ((ASource == NULL) || (*ASource == ANSINULLCHAR));
     
  // ---
  return Result;
} // func

/* inline */ bool /* $$ func */ coresysstrsansinullstrs_isemptysize
  (/* $$ in */ const ansinullstring* /* $$ param */ ASourceBuffer, 
   /* $$ in */ const size_t          /* $$ param */ ASourceSize)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result = 
   (
    (ASourceBuffer == NULL) ||
    (*ASourceBuffer == ANSINULLCHAR) ||
    (ASourceSize < 1)
   );
     
  // ---
  return Result;
} // func

/* inline */ bool /* $$ func */ coresysstrsansinullstrs_isrefempty
  (/* $$ in */ nonconst ansinullstring* asref /* $$ param */ ADest)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result =
    ( false
      || (ADest == NULL) 
      || ((*ADest) == NULL)
      || ((**ADest) == ANSINULLCHAR)
	);
     
  // ---
  return Result;
} // func

/* inline */ bool /* $$ func */ coresysstrsansinullstrs_isrefemptysize
  (/* $$ in */ nonconst ansinullstring* asref /* $$ param */ ADestBuffer,
   /* $$ in */ const    size_t                /* $$ param */ ADestSize)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result =
    ( false
      || (ADestBuffer == NULL) 
      || ((*ADestBuffer) == NULL)
      || ((**ADestBuffer) == ANSINULLCHAR)
      || (ADestSize < 1)
	);
     
  // ---
  return Result;
} // func


// ------------------

count_t /* $$ func */ coresysstrsansinullstrs_length
  (/* $$ in */ const ansinullstring* /* $$ param */ ASource)
{
  count_t /* $$ var */ Result = 0;
  // ---
     
  Result = strlen(ASource);
  
  // ---
  return Result;
} // func

count_t /* $$ func */ coresysstrsansinullstrs_lengthsize
  (/* $$ in */ const ansinullstring* /* $$ param */ ASourceBuffer,
   /* $$ in */ const size_t          /* $$ param */ ASourceMaxSize)
{
  count_t /* $$ var */ Result = 0;
  // ---

  count_t   /* $$ var */ AMaxSize  = 0;
  index_t   /* $$ var */ EachIndex = 0;
  ansichar* /* $$ var */ EachChar  = NULL;
  bool      /* $$ var */ CanContinue = false;

  // ---

  if (ASourceBuffer != NULL)
  {
    AMaxSize =
      (ASourceMaxSize >= 0 ? ASourceMaxSize : 0);
    
    EachChar = (ansichar*) ASourceBuffer;
    
    CanContinue = true;
    while (CanContinue)
    {
      CanContinue =
        ((*EachChar != ANSINULLCHAR) &&
         (EachIndex < AMaxSize));
    
      // count starts with 1, not 0
      if (CanContinue)
      {
        EachIndex++;
        EachChar++;
      }
    } // while
    
    Result = EachIndex;
  } // if

  // ---
  return Result;
} // func

// ------------------

/* friend */ enum comparison /* $$ func */ coresysstrsansinullstrs_compare
  (/* $$ in */ const ansinullstring* /* $$ param */ A,
   /* $$ in */ const ansinullstring* /* $$ param */ B)
{
  enum comparison /* $$ var */ Result = comparison_equal;
  // ---
  
//  int /* $$ var */ ErrorCode = 0;
//
//  if ((A != NULL) && (B != NULL))
//  {
//    ErrorCode =
//      strcmp(A, B);
//    Result =
//       coresystem_inttocmp(ErrorCode);
//  } // if
 
  // ---
  return Result;
} // func

/* friend */ enum comparison /* $$ func */ coresysstrsansinullstrs_comparesize
  (/* $$ in */ const ansinullstring* /* $$ param */ AFirstBuffer,
   /* $$ in */ const size_t          /* $$ param */ AFirstSize,
   /* $$ in */ const ansinullstring* /* $$ param */ ASecondBuffer,
   /* $$ in */ const size_t          /* $$ param */ ASecondSize)
{
  enum comparison /* $$ var */ Result = comparison_equal;
  // ---
     
//  if ((! coresysstrsansinullstrs_isemptysize(AFirstBuffer, AFirstSize)) &&
//     (! coresysstrsansinullstrs_isemptysize(ASecondBuffer, ASecondSize)))
//  {
//    size_t    /* $$ var */ AMaxSize;
//
//    ansichar* /* $$ var */ AA;
//    ansichar* /* $$ var */ BB;
//
//    bool      /* $$ var */ CanContinue;
//    index_t   /* $$ var */ Count;
//    size_t    /* $$ var */ AMaxSize;
//
//    AMaxSize =
//      corememory_oneminsize
//        (AFirstSize, ASecondSize);
//
//    AA = (ansichar*) A;
//    BB = (ansichar*) B;
//
//    Count = 0;
//    CanContinue = true;
//
//    while (CanContinue)
//    {
//      Result =
//        coreansicharptrs_safecompare(AA, BB);
//
//      AA++;
//      BB++;
//
//      Count++;
//
//      CanContinue =
//        (*AA != ANSINULLCHAR) &&
//        (*BB != ANSINULLCHAR) &&
//        (Result == comparison_lesser) &&
//        (Count <= AMaxSize);
//    } // while
//
//    // if one parameter is a full substring,
//    // from another, and are equal,
//    // the shorter is sorted lesser
//    if (Result == comparison_equal)
//    {
//      if (AFirstSize < ASecondSize)
//      {
//        Result = comparison_lesser;
//      } 
//      else
//      {
//        Result = comparison_greater;
//      } 
//    } 
//  } // if
    
  // ---
  return Result;
} // func

// ------------------

/* inline */ bool /* $$ func */ coresysstrsansinullstrs_canconcatdest
  (/* $$ in */ const ansinullstring* /* $$ param */ ADest)
{
  bool /* $$ var */ Result = false;
  // ---

  Result = 
    (ADest != NULL);

  // ---
  return Result;
} // func

/* inline */ bool /* $$ func */ coresysstrsansinullstrs_canconcatdestsize
  (/* $$ in */ const ansinullstring* /* $$ param */ ADestBuffer, 
   /* $$ in */ const size_t          /* $$ param */ ADestSize)
{
  bool /* $$ var */ Result = false;
  // ---

  Result = 
    (ADestBuffer != NULL) &&
    (ADestSize > 0);

  // ---
  return Result;
} // func

/* inline */ bool /* $$ func */ coresysstrsansinullstrs_canconcatsrc
  (/* $$ in */ const ansinullstring* /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---

  Result = 
    ((ASource != NULL) &&
    (*ASource != ANSINULLCHAR));

  // ---
  return Result;
} // func

/* inline */ bool /* $$ func */ coresysstrsansinullstrs_canconcatsrcsize
  (/* $$ in */ const ansinullstring* /* $$ param */ ASourceBuffer, 
   /* $$ in */ const size_t          /* $$ param */ ASourceSize)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result = 
    (ASourceBuffer != NULL) &&
    (*ASourceBuffer != ANSINULLCHAR) &&
    (ASourceSize > 0);

  // ---
  return Result;
} // func

// ------------------

/* allocation */

/* friend */ ansinullstring* /* $$ func */ coresysstrsansinullstrs_newstr
  (/* $$ in */ const size_t /* $$ param */ ADestSize)
{
  ansinullstring* /* $$ var */ Result = NULL;
  // ---
       
  size_t /* $$ var */ AMaxSize = (ADestSize + 1);
  // ---
       
  Result =
    (ansinullstring*) coresysbasemem_allocate(AMaxSize);

  // ---
  return Result;
} // func

/* friend */ ansinullstring* /* $$ func */ coresysstrsansinullstrs_newstrclear
  (/* $$ in */ const size_t /* $$ param */ ADestSize)
{
  ansinullstring* /* $$ var */ Result = NULL;
  // ---
       
  size_t /* $$ var */ AMaxSize = (ADestSize + 1);
  // ---
       
  Result =
    (ansinullstring*) coresysbasemem_allocate(AMaxSize);

  // ---
  return Result;
} // func

/* friend */ void /* $$ func */ coresysstrsansinullstrs_dropstr
  (/* $$ inout */ nonconst ansinullstring* asref /* $$ param */ ADestBuffer,
   /* $$ in */    const    size_t                /* $$ param */ ADestSize)
{
  if ((ADestBuffer != NULL) && (ADestSize > 0))
  {
    size_t /* $$ var */ AMaxSize = (ADestSize + 1);
    // ---
  
    coresysbasemem_deallocate((pointer**) ADestBuffer, AMaxSize);
  } // if
} // func

/* friend */ void /* $$ func */ coresysstrsansinullstrs_dropstrclear
  (/* $$ inout */ nonconst ansinullstring* asref /* $$ param */ ADestBuffer,
   /* $$ in */    const    size_t                /* $$ param */ ADestSize)
{
  if ((ADestBuffer != NULL) && (ADestSize > 0))
  {
    size_t /* $$ var */ AMaxSize = (ADestSize + 1);
    // ---
  
    coresysbasemem_deallocateclear((pointer**) ADestBuffer, AMaxSize);
  } // if
} // func

// ------------------

ansinullstring* /* $$ func */ coresysstrsansinullstrs_compactcopy
  (/* $$ in */ const ansinullstring* /* $$ param */ ASource)
{
  ansinullstring* /* $$ var */ Result = NULL;
  // ---

  size_t /* $$ var */ ASize = 0;

  ansinullstring* /* $$ var */ Marker = NULL;
    
  // ---

  ASize =
    coresysstrsansinullstrs_lengthsize
      (ASource, ANSICHARSETMAXSIZE);
      

  Result =
    (ansinullstring*) coresysbasemem_allocateclear(ASize + 1);
  coresysbasemem_copy((pointer *) Result, (pointer *) ASource, ASize);
    
  Marker = Result;
  Marker = Marker + (ASize + 1);
    
  coresysstrsansinullstrs_writenullmarker
    (Marker);

  // ---
  return Result;
} // func

ansinullstring* /* $$ func */ coresysstrsansinullstrs_compactcopysize
  (/* $$ in */ const ansinullstring* /* $$ param */ ASourceBuffer,
   /* $$ in */ const size_t          /* $$ param */ ASourceSize)
{
  ansinullstring* /* $$ var */ Result = NULL;
  // ---
  
  bool /* $$ var */ CanContinue = false;
  
  // ---
  
  CanContinue =
    ((ASourceBuffer != NULL) && (ASourceSize > 0));
  if (CanContinue)
  {
    size_t /* $$ var */ ASize = 0;
    
    // ---
    
    struct ansicharsizedptr* /* $$ var */ FatS = NULL;
    struct ansicharsizedptr* /* $$ var */ FatD = NULL;
    
    // ---

    ansinullstring* /* $$ var */ Marker = NULL;
    
    // ---

    FatS =
      coresysstrsansicharfatptrs_new();
    FatD =
      coresysstrsansicharfatptrs_new();
    
    // ---
  
    coresysstrsansicharfatptrs_pack
      (intoref FatS, (ansinullstring*) ASourceBuffer, ASourceSize);
      
    // ---
  
    CanContinue =
      (coresysstrsansicharfatptrs_tryseeknullmarkersize
        (&FatD, FatS));
    if (CanContinue)
    {
      ASize =
        FatD->ItemSize;
    }

    Result =
      (ansinullstring*) coresysbasemem_allocateclear(ASize + 1);
    coresysbasemem_copy((pointer *) Result, (pointer *) ASourceBuffer, ASize);
    
    Marker = Result;
    Marker = Marker + (ASize + 1);
    
    coresysstrsansinullstrs_writenullmarker
      (Marker);
    
    // ---
	
    coresysstrsansicharfatptrs_drop(intoref FatD);
    coresysstrsansicharfatptrs_drop(intoref FatS);
  } // if (CanContinue)
    
  // ---
  return Result;
} // func

ansinullstring* /* $$ func */ coresysstrsansinullstrs_extendedcopy
  (/* $$ in */ const ansinullstring* /* $$ param */ ASourceBuffer,
   /* $$ in */ const size_t          /* $$ param */ ASourceSize)
{
  ansinullstring* /* $$ var */ Result = NULL;
  // ---
     
  if ((ASourceBuffer != NULL) && (ASourceSize > 0))
  {
//    size_t /* $$ var */ ASize = 0;
//    // ---
//    
//    ASize = 
//      (coresysstrsansinullstrs_lengthsize(ASourceBuffer, ASourceSize) + 1);
//
//    coresysbasemem_allocateclear(&Result, ASize);
//    coresysbasemem_copy(&Result, ASourceBuffer, ASize);
  } // if
    
  // ---
  return Result;
} // func

// ------------------

/* procedures */

void /* $$ func */ coresysstrsansinullstrs_clear
  (/* $$ inout */ nonconst ansinullstring* /* $$ param */ ADest)
{
  if (ADest != NULL)
  {
    *ADest = ANSINULLCHAR;
  } // if
} // func 

void /* $$ func */ coresysstrsansinullstrs_clearsize
  (/* $$ inout */ nonconst ansinullstring* /* $$ param */ ADestBuffer,
   /* $$ in */    const    size_t          /* $$ param */ ADestSize)
{
  if (! coresysstrsansinullstrs_isemptysize(ADestBuffer, ADestSize))
  {
    //coresysbasemem_fill
     //((pointer*) ADestBuffer, ADestSize, (byte_t) ANSINULLCHAR);
     
    memset
      ((void*) ADestBuffer, (byte_t) ANSINULLCHAR, ADestSize);
  } // if
} // func 

void /* $$ func */ coresysstrsansinullstrs_assign
  (/* $$ out */ nonconst ansinullstring* /* $$ param */ ADest,
   /* $$ in */  const    ansinullstring* /* $$ param */ ASource)
{
  strcpy(ADest, ASource);

//  size_t  var ADestSize;
//  size_t var ASourceSize;
//
//  ADestSize   = 1024;
//  ASourceSize = 1024;
//
//  coresysstrsansinullstrs_assignsize
//    (ADest, ADestSize, ASource, ASourceSize);
} // func 

void /* $$ func */ coresysstrsansinullstrs_assignsize
  (/* $$ out */ nonconst ansinullstring* /* $$ param */ ADestBuffer,
   /* $$ in */  const    size_t          /* $$ param */ ADestSize,
   /* $$ in */  const    ansinullstring* /* $$ param */ ASourceBuffer,
   /* $$ in */  const    size_t          /* $$ param */ ASourceSize)
{
  ansinullstring* /* $$ var */ AItemPtr = ADestBuffer;
  AItemPtr = (AItemPtr + ASourceSize);
  
  strncpy(ADestBuffer, ASourceBuffer, ASourceSize);
  coresysstrsansinullstrs_writenullmarker
    (AItemPtr);
} // func 

void /* $$ func */ coresysstrsansinullstrs_concat
  (/* $$ out */ nonconst ansinullstring* /* $$ param */ ADest,
   /* $$ in */  const    ansinullstring* /* $$ param */ ASource)
{
  if (coresysstrsansinullstrs_canconcatsrc(ASource) &&
      coresysstrsansinullstrs_canconcatdest(ADest))
  {
    strcat(ADest, ASource);
  } // if
} // func 

void /* $$ func */ coresysstrsansinullstrs_concatsize
  (/* $$ inout */ nonconst ansinullstring* /* $$ param */ ADestBuffer,
   /* $$ in */    const    size_t          /* $$ param */ ADestSize,
   /* $$ in */    const    ansinullstring* /* $$ param */ ASourceBuffer,
   /* $$ in */    const    size_t          /* $$ param */ ASourceSize)
{
//  if ((coresysstrsansinullstrs_canconcatsrcsize(ASourceBuffer, ASourceSize) &&
//      (coresysstrsansinullstrs_canconcatdestsize(ADestBuffer, ADestSize))
//  {
//    strncat(ADestBuffer, ASourceBuffer, ASourceSize);
//  } // if
} // func 

void /* $$ func */ coresysstrsansinullstrs_addchar
  (/* $$ inout */ ansinullstring* /* $$ param */ ADest,
   /* $$ in */    ansichar        /* $$ param */ ASource)
{
  if (coresysstrsansinullstrs_canconcatdest(ADest))
  {
    // bool             /* $$ var */ Found;
    // ansinullstring** /* $$ var */ ADestBuffer;
    // size_t*          /* $$ var */ ADestSize;

    // Found =
         // coresysstrsansinullstrs_tryseeknullmarker
       // !!!  (ADest, &ADestBuffer, &ADestSize);
    // if (Found)
    // $$ {

    // $$ }

    //strncat(ADestBuffer, ASourceBuffer, ASourceSize);
  } // if
} // func 

void /* $$ func */ coresysstrsansinullstrs_addcharsize
  (/* $$ inout */ ansinullstring* /* $$ param */ ADestBuffer,
   /* $$ in */    const size_t    /* $$ param */ ADestSize,
   /* $$ in */    const ansichar  /* $$ param */ ASource)
{
  if (coresysstrsansinullstrs_canconcatdestsize(ADestBuffer, ADestSize))
  {
     bool             /* $$ var */ Found;
     
     ansinullstring* /* $$ var */ ASourceBuffer = NULL;
     size_t          /* $$ var */ ASourceSize = 0;
     
     ansinullstring* /* $$ var */ ANewDestBuffer = NULL;
     size_t          /* $$ var */ ANewDestSize = 0;
     
     // ---
     
     ASourceBuffer =
       ADestBuffer;
     ASourceSize =
       ADestSize;
     
     // ---
     
     Found =
        coresysstrsansinullstrs_tryseeknullmarker
          (&ANewDestBuffer, &ANewDestSize, ASourceBuffer, ASourceSize);
     if (Found)
     {
       // @toxdo: check overflow
       *ANewDestBuffer =
         ASource;
         
       ANewDestBuffer++;
       coresysstrsansinullstrs_writenullmarker
         (ANewDestBuffer);  
     } // if (Found)

    //strncat(ADestBuffer, ASourceBuffer, ASourceSize);
  } // if
} // func 

// ------------------

void /* $$ func */ coresysstrsansinullstrs_assignleftsize
  (/* $$ inout */ nonconst ansinullstring* /* $$ param */ ADestBuffer,
   /* $$ in */    const    size_t          /* $$ param */ ADestSize,
   /* $$ in */    const    ansinullstring* /* $$ param */ ASourceBuffer,
   /* $$ in */    const    size_t          /* $$ param */ ASourceSize,
   /* $$ in */    const    count_t         /* $$ param */ ADestCount)
{
  bool /* $$ var */ CanContinue = false;
  
  // ---

  CanContinue =
    (  true
	  && (coresysstrsansinullstrs_isassignedsize(ADestBuffer, ADestSize))
	  && (! coresysstrsansinullstrs_isemptysize(ASourceBuffer, ASourceSize))
	  && (ADestCount > 0)
	);
  if (CanContinue)
  {
    size_t /* $$ var */ ARealSourceSize;
    size_t /* $$ var */ AConfirmedCount;
	
    // ---
    
    ansichar* /* $$ var */ D = NULL;
      
    // ---
      
    ARealSourceSize =
	  coresysstrsansinullstrs_lengthsize
	    (ASourceBuffer, ASourceSize);

    AConfirmedCount =
	    (ASourceSize >= ADestCount ? ADestCount : ASourceSize);
    AConfirmedCount =
	    (AConfirmedCount >= ADestSize ? ADestSize : AConfirmedCount);
		
    CanContinue =
      coresysbasemem_trycopy
	      ((pointer*) ADestBuffer, (pointer*) ASourceBuffer, AConfirmedCount);
        
    D = (ansichar*) /* & */ ADestBuffer;
    D = D + AConfirmedCount;
    coresysstrsansinullstrs_writenullmarker
      (D);
  } // if (CanContinue)
} // func

void /* $$ func */ coresysstrsansinullstrs_assignrightsize
  (/* $$ inout */ nonconst ansinullstring* /* $$ param */ ADestBuffer,
   /* $$ in */    const    size_t          /* $$ param */ ADestSize,
   /* $$ in */    const    ansinullstring* /* $$ param */ ASourceBuffer,
   /* $$ in */    const    size_t          /* $$ param */ ASourceSize,
   /* $$ in */    const    count_t         /* $$ param */ ADestCount)
{
  bool /* $$ var */ CanContinue = false;
  
  // ---

  CanContinue =
    (  true
	  && (coresysstrsansinullstrs_isassignedsize(ADestBuffer, ADestSize))
	  && (! coresysstrsansinullstrs_isemptysize(ASourceBuffer, ASourceSize))
	  && (ADestCount > 0)
	);
  if (CanContinue)
  {
    // ---
    
    struct ansicharsizedptr* /* $$ var */ FatS = NULL;
    struct ansicharsizedptr* /* $$ var */ FatD = NULL;
    
    // ---

    FatS =
      coresysstrsansicharfatptrs_new();
    FatD =
      coresysstrsansicharfatptrs_new();
    
    // ---
  
    coresysstrsansicharfatptrs_pack
      (intoref FatS, (ansinullstring*) ASourceBuffer, ASourceSize);
      
    // ---
  
    CanContinue =
      (coresysstrsansicharfatptrs_tryseeknullmarkersize
        (&FatD, FatS));
    if (CanContinue)
    {
      size_t    /* $$ var */ AConfirmedCount = 0;
      ansichar* /* $$ var */ S = NULL;
      ansichar* /* $$ var */ D = NULL;
      // ---
      
      // avoid underflow / overflow
      AConfirmedCount =
        ((ADestCount >= FatD->ItemSize) ?  FatD->ItemSize : ADestCount);
      coresysstrsansicharfatptrs_decbycount
        (FatD, AConfirmedCount);
        
      S = FatD->ItemPtr;
		
      CanContinue =
        coresysbasemem_trycopy
	        ((pointer*) ADestBuffer, (pointer*) S, AConfirmedCount);
        
      D = ADestBuffer + AConfirmedCount;
      coresysstrsansinullstrs_writenullmarker
        (D);
    } // if (CanContinue)

    // ---
	
    coresysstrsansicharfatptrs_drop(intoref FatD);
    coresysstrsansicharfatptrs_drop(intoref FatS);
  } // if (CanContinue)
} // func

// ------------------

void /* $$ func */ coresysstrsansinullstrs_assignreversesize
  (/* $$ inout */ nonconst ansinullstring* /* $$ param */ ADestBuffer,
   /* $$ in */    const    size_t          /* $$ param */ ADestSize,
   /* $$ in */    const    ansinullstring* /* $$ param */ ASourceBuffer,
   /* $$ in */    const    size_t          /* $$ param */ ASourceSize)
{
  bool /* $$ var */ CanContinue = false;
  
  // ---

  CanContinue =
    (  true
	  && (coresysstrsansinullstrs_isassignedsize(ADestBuffer, ADestSize))
	  && (! coresysstrsansinullstrs_isemptysize(ASourceBuffer, ASourceSize))
	);
  if (CanContinue)
  {
    size_t    /* $$ var */ ConfirmedSize = 0;
    
    ansichar* /* $$ var */ S = NULL;
    ansichar* /* $$ var */ D = NULL;
    
    // ---

    struct ansicharsizedptr* /* $$ var */ FatS = NULL;
    struct ansicharsizedptr* /* $$ var */ FatD = NULL;
    
    // ---

    FatS =
      coresysstrsansicharfatptrs_new();
    FatD =
      coresysstrsansicharfatptrs_new();
    
    // ---
  
    ConfirmedSize =
      coresysstrsansinullstrs_lengthsize
        (ASourceBuffer, ASourceSize);
    
    coresysstrsansicharfatptrs_pack
      (intoref FatS, (ansinullstring*) ASourceBuffer, ConfirmedSize);
    
    CanContinue =
      (coresysstrsansicharfatptrs_tryseeklastchar
        (intoref FatD, FatS));
    if (CanContinue)
    {
      D = (ansichar*) /* & */ ADestBuffer;
      S = FatD->ItemPtr;
          
      do
      {
        CanContinue =
          (ConfirmedSize > 0);
        if (CanContinue)
        {
          *D = *S;
        } //

        D++;
        S--;
        
        ConfirmedSize--;
      } // while
      while (CanContinue);

      coresysstrsansinullstrs_writenullmarker
        (D);
    } // if (CanContinue)
    
    // ---
	
    coresysstrsansicharfatptrs_drop(intoref FatD);
    coresysstrsansicharfatptrs_drop(intoref FatS);	
  } // if (CanContinue)
} // func

void /* $$ func */ coresysstrsansinullstrs_assignsamecharsize
  (/* $$ inout */ nonconst ansinullstring* /* $$ param */ ADestBuffer,
   /* $$ in */    const    size_t          /* $$ param */ ADestSize,
   /* $$ in */    const    ansichar        /* $$ param */ ASourceValue,
   /* $$ in */    const    size_t          /* $$ param */ ASourceCount)
{
  bool /* $$ var */ CanContinue = false;
  
  // ---

  CanContinue =
    (  true
	  && (coresysstrsansinullstrs_isassignedsize(ADestBuffer, ADestSize))
	);
  if (CanContinue)
  {
    count_t    /* $$ var */ i = 0;
    count_t    /* $$ var */ ConfirmedCount = 0;
    ansichar*  /* $$ var */ D = NULL;
    
    // ---
  
    ConfirmedCount =
      ((ASourceCount > (ADestSize - 1)) ? (ADestSize - 1) : ASourceCount);
	D =
	  ADestBuffer;
	  
    while (i < ConfirmedCount)
	{
	  *D = ASourceValue;
	  
	  D++;
      i++;
	} // while
	
	coresysstrsansinullstrs_writenullmarker
	  (D);
  } // if (CanContinue)
} // func

// ------------------

bool /* $$ func */ coresysstrsansinullstrs_tryseeknullmarker
  (/* $$ inout */ nonconst ansinullstring** /* $$ param */ ADestBuffer,
   /* $$ inout */ nonconst size_t*          /* $$ param */ ADestSize,
   /* $$ in */    const    ansinullstring*  /* $$ param */ ASourceBuffer,
   /* $$ in */    const    size_t           /* $$ param */ ASourceSize)
{
  bool /* $$ var */ Result = false;
    
  // ---
    
  /* $$ out */ *ADestBuffer = NULL;
  /* $$ out */ *ADestSize = 0;
    
  // ---

  Result =
    (ASourceBuffer != NULL);

//  bool /* $$ var */ A = false;
//  bool /* $$ var */ B = false;
//  bool /* $$ var */ C = false;
//  A = (ADestBuffer != NULL);
//  B = (ADestSize != NULL);
//  C = (ASourceBuffer != NULL);
//  Result =
//  (  true
//	  && A
//	  && B
//	  && C
//	);
  
  if (Result)
  {
    ansichar* /* $$ var */ S = NULL;
    size_t    /* $$ var */ I = 0;
    size_t    /* $$ var */ L = 0;
    
    bool      /* $$ var */ CanContinue = false;

    // ---
	
    S =
      (ansinullstring*) ASourceBuffer;
    L =
      ASourceSize;
      
    do
    {
      CanContinue =
        ((*S != ANSINULLCHAR) && (I < L));	    	
      if (CanContinue)
      {
      	++I;
      	++S;
      } // if
	  }
	  while (CanContinue);
    
    Result =
      (*S == ANSINULLCHAR);
    if (Result)
    {
      *ADestBuffer =
        S;
	  	*ADestSize =
        I;
  	} // if
  } // if (Result)
    
  // ---
  return Result;
} // func

bool /* $$ func */ coresysstrsansinullstrs_trysearchlastcharsize
  (/* $$ inout */ nonconst ansinullstring* /* $$ param */ ADestBuffer,
   /* $$ inout */ nonconst size_t*         /* $$ param */ ADestSize,
   /* $$ in */    const    ansinullstring* /* $$ param */ ASourceBuffer,
   /* $$ in */    const    size_t          /* $$ param */ ASourceSize)
{
  bool /* $$ var */ Result = false;
    
  // ---
    
  /* $$ out */ ADestBuffer = NULL;
  /* $$ out */ ADestSize = 0;
    
  // ---

  Result =
    (  true
	  && (ADestBuffer != NULL)
	  && (ADestSize != NULL)
	  && (! coresysstrsansinullstrs_isemptysize(ASourceBuffer, ASourceSize))
	);
  if (Result)
  {
    ansichar* /* $$ var */ S = NULL;
    size_t    /* $$ var */ I = 0;
    size_t    /* $$ var */ L = 0;
    
    bool      /* $$ var */ CanContinue = false;
      
    // ---
	
    S =
      (ansinullstring*) ASourceBuffer;
    L =
      ASourceSize;
    
    do
    {
      CanContinue =
        ((*S != ANSINULLCHAR) && (I < L));	    	
      if (CanContinue)
      {
      	++I;
      	++S;
      } // if
	  }
	  while (CanContinue);
    
    Result =
      (*S == ANSINULLCHAR);
    if (Result)
    {
      ADestBuffer =
        S;
	  	*ADestSize =
        I;
  	} // if
  } // if (Result)
    
  // ---
  return Result;
} // func
 
// ------------------

/* inline */ void /* $$ func */ coresysstrsansinullstrs_writenullmarker
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestBuffer)
{
  if (coresysstrsansinullstrs_canconcatdest(ADestBuffer))
  {
    *ADestBuffer = ANSINULLCHAR;
  } // if
} // func 

// ------------------

/* operators */



// ------------------

/* rtti */



// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysstrsansinullstrs_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysstrsansinullstrs_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
    
  //	 
	 
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresysstrsansinullstrs