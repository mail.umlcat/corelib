/** Module: "coresysstrsansinullstrs.*"
 ** Descr.:
 ** "A.N.S.I. Character encoded"
 ** "Null character terminated strings,"
 ** predefined library."
 **/
 
// $$ namespace coresysstrsansinullstrs
// $$ {
 
// ------------------
 
#ifndef CORESYSSTRSANSINULLSTRS_H
#define CORESYSSTRSANSINULLSTRS_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"

// ------------------

// --> contained modules here
#include "coresysstrsansicharfatptrs.h"

// ------------------

/* functions */

// ------------------

/* inline */ bool /* $$ func */ coresysstrsansinullstrs_isassigned
  (/* $$ in */ nonconst ansinullstring* /* $$ param */ ASource);

/* inline */ bool /* $$ func */ coresysstrsansinullstrs_isassignedsize
  (/* $$ in */ const ansinullstring* /* $$ param */ ASourceBuffer, 
   /* $$ in */ const size_t          /* $$ param */ ASourceSize);

// ------------------
   
/* inline */ bool /* $$ func */ coresysstrsansinullstrs_isrefassigned
  (/* $$ in */ nonconst ansinullstring* asref /* $$ param */ ADest);

/* inline */ bool /* $$ func */ coresysstrsansinullstrs_isrefassignedsize
  (/* $$ in */ nonconst ansinullstring* asref /* $$ param */ ADestBuffer,
   /* $$ in */ const    size_t                /* $$ param */ ADestSize);

// ------------------
   
/* inline */ bool /* $$ func */ coresysstrsansinullstrs_isempty
  (/* $$ in */ const ansinullstring* /* $$ param */ ASource);

/* inline */ bool /* $$ func */ coresysstrsansinullstrs_isemptysize
  (/* $$ in */ const ansinullstring* /* $$ param */ ASourceBuffer, 
   /* $$ in */ const size_t          /* $$ param */ ASourceSize);

// ------------------
   
/* inline */ bool /* $$ func */ coresysstrsansinullstrs_isrefempty
  (/* $$ in */ nonconst ansinullstring* asref /* $$ param */ ADest);

/* inline */ bool /* $$ func */ coresysstrsansinullstrs_isrefemptysize
  (/* $$ in */ nonconst ansinullstring* asref /* $$ param */ ADestBuffer,
   /* $$ in */ const    size_t                /* $$ param */ ADestSize);
   
// ------------------

count_t /* $$ func */ coresysstrsansinullstrs_length
  (/* $$ in */ const ansinullstring* /* $$ param */ ASource);

count_t /* $$ func */ coresysstrsansinullstrs_lengthsize
  (/* $$ in */ const ansinullstring* /* $$ param */ ASourceBuffer,
   /* $$ in */ const size_t          /* $$ param */ ASourceMaxSize);

// ------------------

/* friend */ enum comparison /* $$ func */ coresysstrsansinullstrs_compare
  (/* $$ in */ const ansinullstring* /* $$ param */ A,
   /* $$ in */ const ansinullstring* /* $$ param */ B);

/* friend */ enum comparison /* $$ func */ coresysstrsansinullstrs_comparesize
  (/* $$ in */ const ansinullstring* /* $$ param */ AFirstBuffer,
   /* $$ in */ const size_t          /* $$ param */ AFirstSize,
   /* $$ in */ const ansinullstring* /* $$ param */ ASecondBuffer,
   /* $$ in */ const size_t          /* $$ param */ ASecondSize);

// ------------------

/* inline */ bool /* $$ func */ coresysstrsansinullstrs_canconcatdest
  (/* $$ in */ const ansinullstring* /* $$ param */ ADest);

/* inline */ bool /* $$ func */ coresysstrsansinullstrs_canconcatdestsize
  (/* $$ in */ const ansinullstring* /* $$ param */ ADestBuffer, 
   /* $$ in */ const size_t          /* $$ param */ ADestSize);

/* inline */ bool /* $$ func */ coresysstrsansinullstrs_canconcatsrc
  (/* $$ in */ const ansinullstring* /* $$ param */ ASource);

/* inline */ bool /* $$ func */ coresysstrsansinullstrs_canconcatsrcsize
  (/* $$ in */ const ansinullstring* /* $$ param */ ASourceBuffer, 
   /* $$ in */ const size_t          /* $$ param */ ASourceSize);

// ------------------

ansinullstring* /* $$ func */ coresysstrsansinullstrs_lastptrofchar
  (/* $$ in */ nonconst ansinullstring* /* $$ param */ AHaystack,
   /* $$ in */ const    ansichar        /* $$ param */ ANeedle);

// ------------------

/* allocation */

ansinullstring* /* $$ func */ coresysstrsansinullstrs_newstr
  (/* $$ in */ const size_t /* $$ param */ ADestSize);

ansinullstring* /* $$ func */ coresysstrsansinullstrs_newstrclear
  (/* $$ in */ const size_t /* $$ param */ ADestSize);

void /* $$ func */ coresysstrsansinullstrs_dropstr
  (/* $$ inout */ nonconst ansinullstring* asref /* $$ param */ ADestBuffer,
   /* $$ in */    const    size_t                /* $$ param */ ADestSize);

void /* $$ func */ coresysstrsansinullstrs_dropstrclear
  (/* $$ inout */ nonconst ansinullstring* asref /* $$ param */ ADestBuffer,
   /* $$ in */    const    size_t                /* $$ param */ ADestSize);


// ------------------

ansinullstring* /* $$ func */ coresysstrsansinullstrs_compactcopy
  (/* $$ in */ const ansinullstring* /* $$ param */ ASource);

ansinullstring* /* $$ func */ coresysstrsansinullstrs_compactcopysize
  (/* $$ in */ const ansinullstring* /* $$ param */ ASourceBuffer,
   /* $$ in */ const size_t          /* $$ param */ ASourceSize);
  
ansinullstring* /* $$ func */ coresysstrsansinullstrs_extendedcopy
  (/* $$ in */ const ansinullstring* /* $$ param */ ASourceBuffer,
   /* $$ in */ const size_t          /* $$ param */ ASourceSize);

// ------------------

/* procedures */

void /* $$ func */ coresysstrsansinullstrs_clear
  (/* $$ inout */ nonconst ansinullstring* /* $$ param */ ADest);

void /* $$ func */ coresysstrsansinullstrs_clearsize
  (/* $$ inout */ nonconst ansinullstring* /* $$ param */ ADest,
   /* $$ in */    const    size_t          /* $$ param */ ADestSize);

void /* $$ func */ coresysstrsansinullstrs_assign
  (/* $$ out */ nonconst ansinullstring* /* $$ param */ ADest,
   /* $$ in */  const   ansinullstring*  /* $$ param */ ASource);

void /* $$ func */ coresysstrsansinullstrs_assignsize
  (/* $$ out */ nonconst ansinullstring* /* $$ param */ ADestBuffer,
   /* $$ in */  const    size_t          /* $$ param */ ADestSize,
   /* $$ in */  const    ansinullstring* /* $$ param */ ASourceBuffer,
   /* $$ in */  const    size_t          /* $$ param */ ASourceSize);

void /* $$ func */ coresysstrsansinullstrs_concat
  (/* $$ out */ nonconst ansinullstring* /* $$ param */ ADest,
   /* $$ in */  const    ansinullstring* /* $$ param */ ASource);

void /* $$ func */ coresysstrsansinullstrs_concatsize
  (/* $$ inout */ nonconst ansinullstring* /* $$ param */ ADestBuffer,
   /* $$ in */    const    size_t          /* $$ param */ ADestSize,
   /* $$ in */    const    ansinullstring* /* $$ param */ ASourceBuffer,
   /* $$ in */    const    size_t          /* $$ param */ ASourceSize);

void /* $$ func */ coresysstrsansinullstrs_addchar
  (/* $$ inout */ nonconst ansinullstring* /* $$ param */ ADest,
   /* $$ in */    const    ansichar        /* $$ param */ ASource);

void /* $$ func */ coresysstrsansinullstrs_addcharsize
  (/* $$ inout */ nonconst ansinullstring* /* $$ param */ ADestBuffer,
   /* $$ in */    const    size_t          /* $$ param */ ADestSize,
   /* $$ in */    const    ansichar        /* $$ param */ ASource);

// ------------------

void /* $$ func */ coresysstrsansinullstrs_assignleftsize
  (/* $$ inout */ nonconst ansinullstring* /* $$ param */ ADestBuffer,
   /* $$ in */    const    size_t          /* $$ param */ ADestSize,
   /* $$ in */    const    ansinullstring* /* $$ param */ ASourceBuffer,
   /* $$ in */    const    size_t          /* $$ param */ ASourceSize,
   /* $$ in */    const    count_t         /* $$ param */ ADestCount);

void /* $$ func */ coresysstrsansinullstrs_assignrightsize
  (/* $$ inout */ nonconst ansinullstring* /* $$ param */ ADestBuffer,
   /* $$ in */    const    size_t          /* $$ param */ ADestSize,
   /* $$ in */    const    ansinullstring* /* $$ param */ ASourceBuffer,
   /* $$ in */    const    size_t          /* $$ param */ ASourceSize,
   /* $$ in */    const    count_t         /* $$ param */ ADestCount);

// ------------------

void /* $$ func */ coresysstrsansinullstrs_assignreversesize
  (/* $$ inout */ nonconst ansinullstring* /* $$ param */ ADestBuffer,
   /* $$ in */    const    size_t          /* $$ param */ ADestSize,
   /* $$ in */    const    ansinullstring* /* $$ param */ ASourceBuffer,
   /* $$ in */    const    size_t          /* $$ param */ ASourceSize);

void /* $$ func */ coresysstrsansinullstrs_assignsamecharsize
  (/* $$ inout */ nonconst ansinullstring* /* $$ param */ ADestBuffer,
   /* $$ in */    const    size_t          /* $$ param */ ADestSize,
   /* $$ in */    const    ansichar        /* $$ param */ ASourceValue,
   /* $$ in */    const    size_t          /* $$ param */ ASourceCount);
   
// ------------------

bool /* $$ func */ coresysstrsansinullstrs_tryseeknullmarker
  (/* $$ inout */ nonconst ansinullstring** /* $$ param */ ADestBuffer,
   /* $$ inout */ nonconst size_t*          /* $$ param */ ADestSize,
   /* $$ in */    const    ansinullstring*  /* $$ param */ ASourceBuffer,
   /* $$ in */    const    size_t           /* $$ param */ ASourceSize);
   
bool /* $$ func */ coresysstrsansinullstrs_trysearchlastcharsize
  (/* $$ inout */ nonconst ansinullstring* /* $$ param */ ADestBuffer,
   /* $$ inout */ nonconst size_t*         /* $$ param */ ADestSize,
   /* $$ in */    const    ansinullstring* /* $$ param */ ASourceBuffer,
   /* $$ in */    const    size_t          /* $$ param */ ASourceSize);
   
// ------------------

/* inline */ void /* $$ func */ coresysstrsansinullstrs_writenullmarker
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestBuffer);

// ------------------

/* operators */



// ------------------

/* rtti */

#define MOD_coresysstrsansinullstrs {0x9B,0x89,0xAF,0x45,0xEA,0x46,0x46,0x40,0xB8,0xFF,0x8B,0xA4,0x3B,0x46,0x00,0x38};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysstrsansinullstrs_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysstrsansinullstrs_finish
  ( noparams );

// ------------------

#endif // CORESYSSTRSANSINULLSTRS_H

// $$ } // namespace coresysstrsansinullstrs