/** Module: "coresysstrs.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysstrs
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"

// ------------------

// --> contained modules here
#include "coresysstrsansinullstrs.h"
#include "coresysstrsansicharfatptrs.h"
// .include "coresysstrsbinfloat64s.h"
// .include "coresysstrsbinfloat128s.h"
// .include "coresysstrsbinfloat256s.h"
// .include "coresysstrslongfloats.h"

// ------------------

// --> this same module header goes here
#include "coresysstrs.h"
 
// ------------------


// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysstrs_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysstrs_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresysstrs