/** Module: "coresysstrsansicharfatptrs.*"
 ** Descr.: "A composite pointer and index operations library,"
 **         "also known as a 'fat pointer',"
 **         "the base data type is 'ansichar' ."
 **/

// $$ namespace coresysstrsansicharfatptrs
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"

// ------------------

// --> this same module header goes here
#include "coresysstrsansicharfatptrs.h"
 
// ------------------

bool /* $$ func */ coresysstrsansicharfatptrs_isempty 
  (/* $$ in */ const struct ansicharsizedptr* /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result =
    (ASource == NULL) ||
    (ASource->ItemPtr == NULL) ||
    (*(ASource->ItemPtr) == ANSINULLCHAR) ||
    (ASource->ItemSize < 0);

  // ---
  return Result;
} // func

bool /* $$ func */ coresysstrsansicharfatptrs_isassigned 
  (/* $$ in */ const struct ansicharsizedptr* /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result =
    (ASource != NULL) &&
    (ASource->ItemPtr != NULL) &&
    (ASource->ItemSize >= 0);

  // ---
  return Result;
} // func

bool /* $$ func */ coresysstrsansicharfatptrs_isrefassigned 
  (/* $$ in */ nonconst struct ansicharsizedptr* asref /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result =
    (ASource != NULL) &&
    (*ASource != NULL) &&
    ((*ASource)->ItemPtr != NULL);
    ((*ASource)->ItemSize >= 0);

  // ---
  return Result;
} // func


// ------------------

struct ansicharsizedptr* /* $$ func */ coresysstrsansicharfatptrs_new
  ( noparams )
{
  struct ansicharsizedptr* /* $$ var */ Result = NULL;
  // ---
 
  Result =
    (struct ansicharsizedptr*) coresysbasemem_allocateclear
    	(sizeof(struct ansicharsizedptr));

  // ---
  return Result;
} // func

void /* $$ func */ coresysstrsansicharfatptrs_drop
  (/* $$ out */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest)
{
  if (ADest != NULL)
  {
    coresysbasemem_deallocate((pointer**) ADest, sizeof(struct ansicharsizedptr));
  } // if
} // func

// ------------------

void /* $$ func */ coresysstrsansicharfatptrs_pack
  (/* $$ out */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest,
   /* $$ in */  nonconst ansichar*                      /* $$ param */ ASourcePtr,
   /* $$ in */  nonconst size_t                         /* $$ param */ ASourceSize)
{
  if (ADest != NULL)
  {
    (*ADest)->ItemPtr  = ASourcePtr;
    (*ADest)->ItemSize = ASourceSize;
  } // if
} // func

void /* $$ func */ coresysstrsansicharfatptrs_unpack
  (/* $$ in */  const    struct ansicharsizedptr* /* $$ param */ ASource,
   /* $$ out */ nonconst ansichar**               /* $$ param */ ADestPtr,
   /* $$ out */ nonconst size_t*                  /* $$ param */ ADestSize)
{
  if (ASource != NULL)
  {
    *ADestPtr  = ASource->ItemPtr;
    *ADestSize = ASource->ItemSize;
  } // if
} // func

// ------------------

void /* $$ func */ coresysstrsansicharfatptrs_clear
  (/* $$ inout */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest)
{
  if ((ADest != NULL) && ((*ADest) != NULL))
  {
    (*ADest)->ItemPtr  = NULL;
    (*ADest)->ItemSize = 0;
  } // if
} // func

void /* $$ func */ coresysstrsansicharfatptrs_assign
  (/* $$ out */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest,
   /* $$ in */  const    struct ansicharsizedptr*       /* $$ param */ ASource)
{
  if ((ADest != NULL) && ((*ADest) != NULL) && (ASource != NULL))
  {
    (*ADest)->ItemPtr  = ASource->ItemPtr;
    (*ADest)->ItemSize = ASource->ItemSize;
  } // if
} // func

void /* $$ func */ coresysstrsansicharfatptrs_copy
  (/* $$ out */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest,
   /* $$ in */  const    struct ansicharsizedptr*       /* $$ param */ ASource)
{
  bool /* $$ var */ CanContinue = false;
  // ---
 
  CanContinue =
    ( true
      && coresysstrsansicharfatptrs_isrefassigned(ADest)
      && coresysstrsansicharfatptrs_isassigned(ASource)
	);
  if (CanContinue)
  {
//  	coresysstrsansinullstrs_assignsize
//      ((ansinullstring*) (*ADest)->ItemPtr, (*ADest)->ItemSize,
//       (const ansinullstring*) ASource->ItemPtr, ASource->ItemSize
//	  );
  } // if
} // func

// ------------------

struct ansicharsizedptr* /* $$ func */ coresysstrsansicharfatptrs_pred
  (/* $$ in */ const struct ansicharsizedptr* /* $$ param */ ASource)
{
  struct ansicharsizedptr* /* $$ var */ Result = NULL;
  // ---

  if (coresysstrsansicharfatptrs_isassigned(ASource))
  {
    Result =
      coresysstrsansicharfatptrs_new();

    Result->ItemPtr  = (ASource->ItemPtr - 1);
    Result->ItemSize = (ASource->ItemSize - 1);
  } // if

  // ---
  return Result;
} // func

struct ansicharsizedptr* /* $$ func */ coresysstrsansicharfatptrs_predbycount
  (/* $$ in */ const struct ansicharsizedptr* /* $$ param */ ASource,
   /* $$ in */ const count_t                  /* $$ param */ ACount)
{
  struct ansicharsizedptr* /* $$ var */ Result = NULL;
  // ---

  if (coresysstrsansicharfatptrs_isassigned(ASource) &&
      (ACount >= 0))
  {
    Result =
      coresysstrsansicharfatptrs_new();

    Result->ItemPtr  = (ASource->ItemPtr - ACount);
    Result->ItemSize = (ASource->ItemSize - ACount);
  } // if

  // ---
  return Result;
} // func

struct ansicharsizedptr* /* $$ func */ coresysstrsansicharfatptrs_succ
  (/* $$ in */ const struct ansicharsizedptr* /* $$ param */ ASource)
{
  struct ansicharsizedptr* /* $$ var */ Result = NULL;
  // ---

  if (coresysstrsansicharfatptrs_isassigned(ASource))
  {
    Result =
      coresysstrsansicharfatptrs_new();

    Result->ItemPtr  = (ASource->ItemPtr +  1);
    Result->ItemSize = (ASource->ItemSize + 1);
  } // if

  // ---
  return Result;
} // func

struct ansicharsizedptr* /* $$ func */ coresysstrsansicharfatptrs_succbycount
  (/* $$ in */ const struct ansicharsizedptr* /* $$ param */ ASource,
   /* $$ in */ const count_t                  /* $$ param */ ACount)
{
  struct ansicharsizedptr* /* $$ var */ Result = NULL;
  // ---

  if (coresysstrsansicharfatptrs_isassigned(ASource))
  {
    Result =
      coresysstrsansicharfatptrs_new();

    Result->ItemPtr  = (ASource->ItemPtr + 1);
    Result->ItemSize = (ASource->ItemSize + 1);
  } // if

  // ---
  return Result;
} // func

// ------------------

void /* $$ func */ coresysstrsansicharfatptrs_inc
  (/* $$ inout */  struct ansicharsizedptr* /* $$ param */ ADest)
{
  if (coresysstrsansicharfatptrs_isassigned(ADest))
  {
    ADest->ItemPtr  = (ADest->ItemPtr + 1);
    ADest->ItemSize = (ADest->ItemSize + 1);
  } // if
} // func

void /* $$ func */ coresysstrsansicharfatptrs_incbycount
  (/* $$ inout */ struct ansicharsizedptr* /* $$ param */ ADest,
   /* $$ in */    const count_t            /* $$ param */ ACount)
{
  if (coresysstrsansicharfatptrs_isassigned(ADest))
  {
    ADest->ItemPtr  = (ADest->ItemPtr + ACount);
    ADest->ItemSize = (ADest->ItemSize + ACount);
  } // if
} // func

void /* $$ func */ coresysstrsansicharfatptrs_dec
  (/* $$ inout */ struct ansicharsizedptr* /* $$ param */ ADest)
{
  if (coresysstrsansicharfatptrs_isassigned(ADest))
  {
    ADest->ItemPtr  = (ADest->ItemPtr - 1);
    ADest->ItemSize = (ADest->ItemSize - 1);
  } // if
} // func

void /* $$ func */ coresysstrsansicharfatptrs_decbycount
  (/* $$ inout */ struct ansicharsizedptr* /* $$ param */ ADest,
   /* $$ in */    const count_t            /* $$ param */ ACount)
{
  if (coresysstrsansicharfatptrs_isassigned(ADest))
  {
    ADest->ItemPtr  = (ADest->ItemPtr - ACount);
    ADest->ItemSize = (ADest->ItemSize - ACount);
  } // if
} // func

// ------------------

bool /* $$ func */ coresysstrsansicharfatptrs_tryseeknullmarker
  (/* $$ out */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest,
   /* $$ in */  const    struct ansicharsizedptr*       /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result =
    ( true
      && coresysstrsansicharfatptrs_isassigned(ASource)
      && (ADest != NULL)
      && (*ADest != NULL)
    );
  if (Result)
  {
    ansichar* /* $$ var */ S = NULL;
    size_t    /* $$ var */ I = 0;
    size_t    /* $$ var */ L = 0;
    
    bool      /* $$ var */ CanContinue = false;

    // ---
    
//    L =
//	    coresysstrsansinullstrs_length
//        ((const ansinullstring*) ASource->ItemPtr);
        
//    L =
//	    strlen
//        ((const ansinullstring*) ASource->ItemPtr);
	
    coresysstrsansicharfatptrs_unpack
      (ASource, &S, &L);
    
    do
    {
      CanContinue =
        ((*S != ANSINULLCHAR) && (I < L));	    	
      if (CanContinue)
      {
    	++I;
    	++S;
      } // if
	  }
	  while (CanContinue);
    
    Result =
      (*S == ANSINULLCHAR);
    if (Result)
    {
      coresysstrsansicharfatptrs_pack
	  	(ADest, S, I);
  	} // if
  } // if (Result)

  // ---
  return Result;
} // func

bool /* $$ func */ coresysstrsansicharfatptrs_tryseeknullmarkersize
  (/* $$ out */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest,
   /* $$ in */  const    struct ansicharsizedptr*       /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result =
    ( true
      && coresysstrsansicharfatptrs_isassigned(ASource))
      && ((ADest != NULL)
      && (*ADest != NULL)
    );
  if (Result)
  {
    ansichar* /* $$ var */ S = NULL;
    size_t    /* $$ var */ I = 0;
    size_t    /* $$ var */ L = 0;
    
    bool      /* $$ var */ CanContinue = false;
    
    coresysstrsansicharfatptrs_unpack
     (ASource, &S, &L);
    
    do
    {
      CanContinue =
        ((*S != ANSINULLCHAR) && (I < L));	    	
      if (CanContinue)
      {
      	++I;
      	++S;
      } // if
  	}
	  while (CanContinue);
    
    Result =
      (*S == ANSINULLCHAR);
    if (Result)
    {
      coresysstrsansicharfatptrs_pack
	  	  (ADest, S, I);
	  } // if
  } // if (Result)

  // ---
  return Result;
} // func

bool /* $$ func */ coresysstrsansicharfatptrs_tryseeklastchar
  (/* $$ out */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest,
   /* $$ in */  const    struct ansicharsizedptr*       /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result =
    (! coresysstrsansicharfatptrs_isempty(ASource));
  if (Result)
  {   
    struct ansicharsizedptr* /* $$ var */ FatS = NULL;
    struct ansicharsizedptr* /* $$ var */ FatD = NULL;
   
    // ---

    FatS =
      coresysstrsansicharfatptrs_new();
    FatD =
      coresysstrsansicharfatptrs_new();
   
    // ---

    coresysstrsansicharfatptrs_assign
      (&FatS, ASource);
      
	// ---

    Result =
      coresysstrsansicharfatptrs_tryseeknullmarker
       (&FatD, FatS); 
    if (Result) 
    {
      FatD->ItemPtr--;
      FatD->ItemSize;
      coresysstrsansicharfatptrs_assign
        (ADest, FatD);
    } // if (Result) 

    // ---
	
    coresysstrsansicharfatptrs_drop(&FatD);
    coresysstrsansicharfatptrs_drop(&FatS);
  } // if (Result)

  // ---
  return Result;
} // func

// ------------------

bool /* $$ func */ coresysstrsansicharfatptrs_tryfirstptrofchar
  (/* $$ out */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest,
   /* $$ in */  nonconst struct ansicharsizedptr*       /* $$ param */ AHaystack,
   /* $$ in */  const    ansichar                       /* $$ param */ ANeedle)
{
  bool /* $$ var */ Result = false;
  // ---

  Result =
    ( true
	  && (! coresysstrsansicharfatptrs_isempty(AHaystack))
      && (ADest != NULL)
      && (ANeedle != ANSINULLCHAR)
	);
  if (Result)
  {
    bool      /* $$ var */ Found;
    bool      /* $$ var */ CanContinue;
    bool      /* $$ var */ OutOfBounds;

    ansichar* /* $$ var */ EachChar;
    size_t    /* $$ var */ EachIndex;

    // ---

    // clear the result
    coresysstrsansicharfatptrs_clear(ADest);

    // ---

    EachChar =
      (ansichar*) AHaystack->ItemPtr;
    EachIndex =
      0;
		
    // ---

    CanContinue = true;
    OutOfBounds = false;
    Found       = false;

    while (CanContinue)
    {
      Found =
        (*EachChar == ANeedle);
      if (Found)
      {
        (*ADest)->ItemPtr  = EachChar;
        (*ADest)->ItemSize = EachIndex;
      }
      
      // check it doesn't go out of the string
      OutOfBounds =
        ((*EachChar == ANSINULLCHAR) || (EachIndex >= AHaystack->ItemSize));

      CanContinue =
        ((! Found) && (! OutOfBounds));

      EachChar++;
      EachIndex++;
    } // if
  } // if
  
  // ---
  return Result;
} // func

bool /* $$ func */ coresysstrsansicharfatptrs_trylastptrofchar
  (/* $$ out */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest,
   /* $$ in */  nonconst struct ansicharsizedptr*       /* $$ param */ AHaystack,
   /* $$ in */  const    ansichar                       /* $$ param */ ANeedle)
{
  bool /* $$ var */ Result = false;
  // ---

  Result =
    ( true
	  && (! coresysstrsansicharfatptrs_isempty(AHaystack))
      && (ADest != NULL)
      && (ANeedle != ANSINULLCHAR)
	);
  if (Result)
  {
    bool      /* $$ var */ Found;
    bool      /* $$ var */ CanContinue;
    bool      /* $$ var */ OutOfBounds;

    ansichar* /* $$ var */ EachChar;
    size_t    /* $$ var */ EachIndex;

    // ---

    struct ansicharsizedptr* /* $$ var */ ALastChar;

    // ---

    // clear the result
    coresysstrsansicharfatptrs_clear(ADest);

    // ---

    ALastChar =
      coresysstrsansicharfatptrs_new();

    // ---

    Result =
      (coresysstrsansicharfatptrs_tryseeklastchar(intoref ALastChar, AHaystack));
    if (Result)
    {
      EachChar =
	    (ansichar*) ALastChar->ItemPtr;
      EachIndex =
        ALastChar->ItemSize;
		
	  // ---

      CanContinue = true;
      OutOfBounds = false;
      Found       = false;

      while (CanContinue)
      {
        Found =
          (*EachChar == ANeedle);
        if (Found)
        {
          coresysstrsansicharfatptrs_pack
            (ADest, EachChar, EachIndex);
        }

        EachChar--;
        EachIndex--;
        
        // check it doesn't go out of the string
        OutOfBounds =
          (EachIndex < 0);

        CanContinue =
          ((! Found) && (! OutOfBounds));
      } // while

      Result = Found;
    } // if

    // ---

    coresysstrsansicharfatptrs_drop(&ALastChar);
  } // if

  // ---
  return Result;
} // func

// ------------------

bool /* $$ func */ coresysstrsansicharfatptrs_startswithcharequal
  (/* $$ out */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest,
   /* $$ in */  nonconst struct ansicharsizedptr*       /* $$ param */ AHaystack,
   /* $$ in */  const    ansichar                       /* $$ param */ ANeedle)
{
  bool /* $$ var */ Result = false;
  // ---

  Result =
    ( true
	  && (! coresysstrsansicharfatptrs_isempty(AHaystack))
      && (ADest != NULL)
      && (ANeedle != ANSINULLCHAR)
	);
  if (Result)
  {
    // clear the result
    coresysstrsansicharfatptrs_clear(ADest);

    // ---

    Result =
      (*(AHaystack->ItemPtr) == ANeedle);
    if (Result)
    {
      coresysstrsansicharfatptrs_pack
        (ADest, AHaystack->ItemPtr, 0);
    } // if
  } // if

  // ---
  return Result;
} // func

// ------------------

bool /* $$ func */ coresysstrsansicharfatptrs_tryreverse
  (/* $$ out */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest,
   /* $$ in */  const    struct ansicharsizedptr*       /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result =
    ( true
      && (coresysstrsansicharfatptrs_isassigned(ASource))
      && (coresysstrsansicharfatptrs_isrefassigned(ADest))
	);
  if (Result)
  {  
    struct ansicharsizedptr* /* $$ var */ FatS = NULL;
    struct ansicharsizedptr* /* $$ var */ FatD = NULL;
    
	// ---
		
    ansichar* /* $$ var */ S = NULL;
    ansichar* /* $$ var */ D = NULL;
    
    // ---

    FatS =
      coresysstrsansicharfatptrs_new();
    FatD =
      coresysstrsansicharfatptrs_new();
    
    // ---

    coresysstrsansicharfatptrs_assign
      (&FatS, ASource);
      
	// ---

    Result =
      coresysstrsansicharfatptrs_tryseeknullmarker
       (&FatD, FatS); 
    if (Result)
    {
      S = /* & */ FatD->ItemPtr;
      D = /* & */ (*ADest)->ItemPtr; 
       
      S--;
      for (int i = 0; i < FatD->ItemSize; i++)
      {
        *D = *S;
      
        --S;
        ++D;
  	} // for
	
  	*D = ANSINULLCHAR;
    } // if (Found) 
  	
    coresysstrsansicharfatptrs_pack
      (&FatD, FatS->ItemPtr, FatD->ItemSize);

	// ---
	
	coresysstrsansicharfatptrs_drop(intoref FatD);
	coresysstrsansicharfatptrs_drop(intoref FatS);
  } // if (Result)

  // ---
  return Result;
} // func

// ------------------


 // ...

// ------------------

/* $$ override */ int /* $$ func */ coresysstrsansicharfatptrs_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysstrsansicharfatptrs_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresysstrsansicharfatptrs