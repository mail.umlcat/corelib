/** Module: "coresysstrs.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysstrs
// $$ {
 
// ------------------
 
#ifndef CORESYSSTRS_H
#define CORESYSSTRS_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"

// ------------------

// --> contained modules here
#include "coresysstrsansinullstrs.h"
#include "coresysstrsansicharfatptrs.h"
// .include "coresysstrsbinfloat64s.h"
// .include "coresysstrsbinfloat128s.h"
// .include "coresysstrsbinfloat256s.h"
// .include "coresysstrslongfloats.h"

// ------------------

/* rtti */

#define MOD_coresysstrs {0x7A,0xB7,0x77,0x84,0x23,0xE2,0xCE,0x4F,0x8F,0x92,0x04,0xB7,0x54,0x70,0x22,0xBB}

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysstrs_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysstrs_finish
  ( noparams );

// ------------------

#endif // CORESYSSTRS_H

// ------------------

// $$ } // namespace coresysstrs