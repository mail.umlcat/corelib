/** Module: "coresysstrsansicharfatptrs.*"
 ** Descr.: "A composite pointer and index operations library,"
 **         "also known as a 'fat pointer',"
 **         "the base data type is 'ansichar' ."
 **/

// $$ namespace coresysstrsansicharfatptrs
// $$ {
 
// ------------------

#ifndef CORESYSSTRSANSICHARFATPTRS_H
#define CORESYSSTRSANSICHARFATPTRS_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"


// ------------------

/* rtti */

#define MOD_coresysstrsansicharfatptrs {0x4F,0x61,0xB4,0x06,0x01,0x64,0x5B,0x4B,0x97,0x25,0x7F,0x6B,0x1B,0xF2,0x7D,0xDF};

// ------------------


/* types */

struct ansicharsizedptr
{
  ansichar* /* $$ var */ ItemPtr;
  size_t    /* $$ var */ ItemSize;
} ;

typedef
  struct ansicharsizedptr  /* as */ ansicharsizedptr_t;
typedef
  struct ansicharsizedptr* /* as */ ansicharsizedptr_p;

typedef
  ansicharsizedptr_t /* as */ coresysstrsansicharfatptrs_ansicharsizedptr_t;
typedef
  ansicharsizedptr_p /* as */ coresysstrsansicharfatptrs_ansicharsizedptr_p;

// ------------------

struct ansicharsizedptr* /* $$ func */ coresysstrsansicharfatptrs_new
  ( noparams );

void /* $$ func */ coresysstrsansicharfatptrs_drop
  (/* $$ out */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest);

// ------------------

bool /* $$ func */ coresysstrsansicharfatptrs_isempty
  (/* $$ in */ const struct ansicharsizedptr* /* $$ param */ ASource);
  
bool /* $$ func */ coresysstrsansicharfatptrs_isassigned
  (/* $$ in */ const struct ansicharsizedptr* /* $$ param */ ASource);

bool /* $$ func */ coresysstrsansicharfatptrs_isrefassigned
  (/* $$ in */ nonconst struct ansicharsizedptr* asref /* $$ param */ ASource);
  // ------------------

void /* $$ func */ coresysstrsansicharfatptrs_pack
  (/* $$ out */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest,
   /* $$ in */  nonconst ansichar*                      /* $$ param */ ASourcePtr,
   /* $$ in */  nonconst size_t                         /* $$ param */ ASourceSize);

void /* $$ func */ coresysstrsansicharfatptrs_unpack
  (/* $$ in */  const    struct ansicharsizedptr* /* $$ param */ ASource,
   /* $$ out */ nonconst ansichar**               /* $$ param */ ADestPtr,
   /* $$ out */ nonconst size_t*                  /* $$ param */ ADestSize);

// ------------------

void /* $$ func */ coresysstrsansicharfatptrs_clear
  (/* $$ inout */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest);

void /* $$ func */ coresysstrsansicharfatptrs_assign
  (/* $$ out */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest,
   /* $$ in */  const    struct ansicharsizedptr*       /* $$ param */ ASource);

void /* $$ func */ coresysstrsansicharfatptrs_copy
  (/* $$ out */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest,
   /* $$ in */  const    struct ansicharsizedptr*       /* $$ param */ ASource);

// ------------------

struct ansicharsizedptr* /* $$ func */ coresysstrsansicharfatptrs_pred
  (/* $$ in */ const struct ansicharsizedptr* /* $$ param */ ASource);

struct ansicharsizedptr* /* $$ func */ coresysstrsansicharfatptrs_predbycount
  (/* $$ in */ const struct ansicharsizedptr* /* $$ param */ ASource,
   /* $$ in */ const count_t                  /* $$ param */ ACount);

struct ansicharsizedptr* /* $$ func */ coresysstrsansicharfatptrs_succ
  (/* $$ in */ const struct ansicharsizedptr* /* $$ param */ ASource);

struct ansicharsizedptr* /* $$ func */ coresysstrsansicharfatptrs_succbycount
  (/* $$ in */ const struct ansicharsizedptr* /* $$ param */ ASource,
   /* $$ in */ const count_t                  /* $$ param */ ACount);

// ------------------

void /* $$ func */ coresysstrsansicharfatptrs_inc
  (/* $$ inout */  struct ansicharsizedptr* /* $$ param */ ADest);

void /* $$ func */ coresysstrsansicharfatptrs_incbycount
  (/* $$ inout */ struct ansicharsizedptr* /* $$ param */ ADest,
   /* $$ in */    const  count_t           /* $$ param */ ACount);

void /* $$ func */ coresysstrsansicharfatptrs_dec
  (/* $$ inout */  struct ansicharsizedptr* /* $$ param */ ADest);

void /* $$ func */ coresysstrsansicharfatptrs_decbycount
  (/* $$ inout */  struct ansicharsizedptr* /* $$ param */ ADest,
   /* $$ in */     const  count_t           /* $$ param */ ACount);

// ------------------

bool /* $$ func */ coresysstrsansicharfatptrs_tryseeknullmarker
  (/* $$ out */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest,
   /* $$ in */  const    struct ansicharsizedptr*       /* $$ param */ ASource);

bool /* $$ func */ coresysstrsansicharfatptrs_tryseeknullmarkersize
  (/* $$ out */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest,
   /* $$ in */  const    struct ansicharsizedptr*       /* $$ param */ ASource);

bool /* $$ func */ coresysstrsansicharfatptrs_tryseeklastchar
  (/* $$ out */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest,
   /* $$ in */  const    struct ansicharsizedptr*       /* $$ param */ ASource);

// ------------------

bool /* $$ func */ coresysstrsansicharfatptrs_tryfirstptrofchar
  (/* $$ out */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest,
   /* $$ in */  nonconst struct ansicharsizedptr*       /* $$ param */ AHaystack,
   /* $$ in */  const    ansichar                       /* $$ param */ ANeedle);
   
bool /* $$ func */ coresysstrsansicharfatptrs_trylastptrofchar
  (/* $$ out */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest,
   /* $$ in */  nonconst struct ansicharsizedptr*       /* $$ param */ AHaystack,
   /* $$ in */  const    ansichar                       /* $$ param */ ANeedle);

// ------------------

bool /* $$ func */ coresysstrsansicharfatptrs_startswithcharequal
  (/* $$ out */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest,
   /* $$ in */  nonconst struct ansicharsizedptr*       /* $$ param */ AHaystack,
   /* $$ in */  const    ansichar                       /* $$ param */ ANeedle);


// ------------------

bool /* $$ func */ coresysstrsansicharfatptrs_tryreverse
  (/* $$ out */ nonconst struct ansicharsizedptr* asref /* $$ param */ ADest,
   /* $$ in */  const    struct ansicharsizedptr*       /* $$ param */ ASource);
 
// ------------------



 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysstrsansicharfatptrs_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysstrsansicharfatptrs_finish
  ( noparams );

// ------------------

#endif // CORESYSSTRSANSICHARFATPTRS_H

// $$ } // namespace coresysstrsansicharfatptrs