/** Module: "coresyssintssint16s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresyssintssint16s
// $$ {
 
// ------------------
 
#ifndef CORESYSSINTSINT16S_H
#define CORESYSSINTSINT16S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
 
// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

/* functions */


bool /* $$ func */ coresyssintssint16s_isempty
  (/* $$ in */ const sint16_t /* $$ param */ ASource);
  
// ------------------

/* procedures */

void /* $$ func */ coresyssintssint16s_clear
  (/* $$ inout */ nonconst sint16_t* /* $$ param */ ADest);

void /* $$ func */ coresyssintssint16s_assign
  (/* $$ inout */ nonconst sint16_t* /* $$ param */ ADest,
   /* $$ in */    const    sint16_t  /* $$ param */ ASource);
   
// ------------------

/* operators */

enum comparison /* $$ func */ coresyssintssint16s_compare
  (/* $$ in */ const sint16_t /* $$ param */ A,
   /* $$ in */ const sint16_t /* $$ param */ B);

bool /* $$ func */ coresyssintssint16s_equal
  (/* $$ in */ const sint16_t /* $$ param */ A,
   /* $$ in */ const sint16_t /* $$ param */ B);

bool /* $$ func */ coresyssintssint16s_different
  (/* $$ in */ const sint16_t /* $$ param */ A,
   /* $$ in */ const sint16_t /* $$ param */ B);


// ------------------

/* rtti */

bool /* $$ func */ coresyssintssint16s_isdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresyssintssint16s_tryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresyssintssint16s_applydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);


 
// ------------------

/* $$ override */ int /* $$ func */ coresyssintssint16s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresyssintssint16s_finish
  ( noparams );

// ------------------

#endif // CORESYSSINTSINT16S_H

// $$ } // namespace coresyssintssint16s