/** Module: "coresyssintssint128s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresyssintssint128s
// $$ {
 
// ------------------
 
#ifndef CORESYSSINTSINT128S_H
#define CORESYSSINTSINT128S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
 
// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

/* functions */

bool /* $$ func */ coresyssintssint128s_isempty
  (/* $$ in */ const sint128_t /* $$ param */ ASource);

// ------------------

/* procedures */

void /* $$ func */ coresyssintssint128s_clear
  (/* $$ inout */ nonconst sint128_t* /* $$ param */ ADest);

void /* $$ func */ coresyssintssint128s_assign
  (/* $$ inout */ nonconst sint128_t* /* $$ param */ ADest,
   /* $$ in */    const    sint128_t  /* $$ param */ ASource);

bool /* $$ func */ coresyssintssint128s_different
  (/* $$ in */ const sint128_t /* $$ param */ A,
   /* $$ in */ const sint128_t /* $$ param */ B);
   
// ------------------

/* operators */

enum comparison /* $$ func */ coresyssintssint128s_compare
  (/* $$ in */ const sint128_t /* $$ param */ A,
   /* $$ in */ const sint128_t /* $$ param */ B);

bool /* $$ func */ coresyssintssint128s_equal
  (/* $$ in */ const sint128_t /* $$ param */ A,
   /* $$ in */ const sint128_t /* $$ param */ B);

// ------------------

/* rtti */

bool /* $$ func */ coresyssintssint128s_isdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresyssintssint128s_tryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresyssintssint128s_applydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresyssintssint128s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresyssintssint128s_finish
  ( noparams );

// ------------------

#endif // CORESYSSINTSINTS128S_H

// $$ } // namespace coresyssintssint128s