/** Module: "coresyssintssint64s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresyssintssint64s
// $$ {
 
// ------------------
 
#ifndef CORESYSSINTSINT64S_H
#define CORESYSSINTSINT64S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
 
// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

/* functions */

bool /* $$ func */ coresyssintssint64s_isempty
  (/* $$ in */ const sint64_t /* $$ param */ ASource);

// ------------------

/* procedures */

void /* $$ func */ coresyssintssint64s_clear
  (/* $$ inout */ nonconst sint64_t* /* $$ param */ ADest);

void /* $$ func */ coresyssintssint64s_assign
  (/* $$ inout */ nonconst sint64_t* /* $$ param */ ADest,
   /* $$ in */    const    sint64_t  /* $$ param */ ASource);

bool /* $$ func */ coresyssintssint64s_different
  (/* $$ in */ const sint64_t /* $$ param */ A,
   /* $$ in */ const sint64_t /* $$ param */ B);
   
// ------------------

/* operators */

enum comparison /* $$ func */ coresyssintssint64s_compare
  (/* $$ in */ const sint64_t /* $$ param */ A,
   /* $$ in */ const sint64_t /* $$ param */ B);

bool /* $$ func */ coresyssintssint64s_equal
  (/* $$ in */ const sint64_t /* $$ param */ A,
   /* $$ in */ const sint64_t /* $$ param */ B);

   
// ------------------

/* rtti */

bool /* $$ func */ coresyssintssint64s_isdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresyssintssint64s_tryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresyssintssint64s_applydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresyssintssint64s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresyssintssint64s_finish
  ( noparams );

// ------------------

#endif // CORESYSSINTSINT64S_H

// $$ } // namespace coresyssintssint64s