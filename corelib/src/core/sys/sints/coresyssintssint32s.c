/** Module: "coresyssintssint32s.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresyssintssint32s
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
 
// ------------------

// --> this module own header goes here
// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

#include "coresyssintssint32s.h"
 
// ------------------

/* functions */

bool /* $$ func */ coresyssintssint32s_isempty
  (/* $$ in */ const sint32_t /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* procedures */

void /* $$ func */ coresyssintssint32s_clear
  (/* $$ inout */ sint32_t* /* $$ param */ ADest)
{
  //coresysbasefuncs_nothing();
} // func

void /* $$ func */ coresyssintssint32s_assign
  (/* $$ inout */ nonconst sint32_t* /* $$ param */ ADest,
   /* $$ in */    const    sint32_t  /* $$ param */ ASource)
{
  coresysbasefuncs_nothing();
} // func 

// ------------------

/* operators */

enum comparison /* $$ func */ coresyssintssint32s_compare
  (/* $$ in */ const sint32_t /* $$ param */ A,
   /* $$ in */ const sint32_t /* $$ param */ B)
{
  enum comparison /* $$ var */ Result = comparison_equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresyssintssint32s_equal
  (/* $$ in */ const sint32_t /* $$ param */ A,
   /* $$ in */ const sint32_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresyssintssint32s_different
  (/* $$ in */ const sint32_t /* $$ param */ A,
   /* $$ in */ const sint32_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* rtti */

bool /* $$ func */ coresyssintssint32s_isdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresyssintssint32s_tryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* $$ func */ coresyssintssint32s_applydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue)
{
  //coresysbasefuncs_nothing();
} // func


// ------------------

 // ...


// ------------------

/* $$ override */ int /* $$ func */ coresyssintssint32s_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresyssintssint32s_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresyssintssint32s