/** Module: "coresyssintssint32s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresyssintssint32s
// $$ {
 
// ------------------
 
#ifndef CORESYSSINTSINT32S_H
#define CORESYSSINTSINT32S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
 
// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

/* functions */

bool /* $$ func */ coresyssintssint32s_isempty
  (/* $$ in */ const sint32_t /* $$ param */ ASource);

// ------------------

/* procedures */

void /* $$ func */ coresyssintssint32s_clear
  (/* $$ inout */ nonconst sint32_t* /* $$ param */ ADest);

void /* $$ func */ coresyssintssint32s_assign
  (/* $$ inout */ nonconst sint32_t* /* $$ param */ ADest,
   /* $$ in */    const    sint32_t  /* $$ param */ ASource);

bool /* $$ func */ coresyssintssint32s_different
  (/* $$ in */ const sint32_t /* $$ param */ A,
   /* $$ in */ const sint32_t /* $$ param */ B);
   
// ------------------

/* operators */

enum comparison /* $$ func */ coresyssintssint32s_compare
  (/* $$ in */ const sint32_t /* $$ param */ A,
   /* $$ in */ const sint32_t /* $$ param */ B);

bool /* $$ func */ coresyssintssint32s_equal
  (/* $$ in */ const sint32_t /* $$ param */ A,
   /* $$ in */ const sint32_t /* $$ param */ B);

// ------------------

/* rtti */

bool /* $$ func */ coresyssintssint32s_isdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresyssintssint32s_tryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresyssintssint32s_applydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

pointer* /* $$ func */ coresyssintssint32s_sint32ptrcopy
  (/* $$ in */ const sint32_t /* $$ param */ AValue);

 
// ------------------

/* $$ override */ int /* $$ func */ coresyssintssint32s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresyssintssint32s_finish
  ( noparams );

// ------------------

#endif // CORESYSSINTSINT32S_H

// $$ } // namespace coresyssintssint32s