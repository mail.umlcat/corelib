/** Module: "coresyssintssint24s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresyssintssint24s
// $$ {
 
// ------------------
 
#ifndef CORESYSSINTSINT24S_H
#define CORESYSSINTSINT24S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
 
// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

/* functions */


bool /* $$ func */ coresyssintssint24s_isempty
  (/* $$ in */ const sint24_t /* $$ param */ ASource);
  
// ------------------

/* procedures */

void /* $$ func */ coresyssintssint24s_clear
  (/* $$ inout */ nonconst sint24_t* /* $$ param */ ADest);

void /* $$ func */ coresyssintssint24s_assign
  (/* $$ inout */ nonconst sint24_t* /* $$ param */ ADest,
   /* $$ in */    const    sint24_t  /* $$ param */ ASource);
   
// ------------------

/* operators */

enum comparison /* $$ func */ coresyssintssint24s_compare
  (/* $$ in */ const sint24_t /* $$ param */ A,
   /* $$ in */ const sint24_t /* $$ param */ B);

bool /* $$ func */ coresyssintssint24s_equal
  (/* $$ in */ const sint24_t /* $$ param */ A,
   /* $$ in */ const sint24_t /* $$ param */ B);

bool /* $$ func */ coresyssintssint24s_different
  (/* $$ in */ const sint24_t /* $$ param */ A,
   /* $$ in */ const sint24_t /* $$ param */ B);
   
// ------------------

/* rtti */

bool /* $$ func */ coresyssintssint24s_isdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresyssintssint24s_tryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresyssintssint24s_applydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresyssintssint24_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresyssintssint24_finish
  ( noparams );

// ------------------

#endif // CORESYSSINTSINT24S_H

// $$ } // namespace coresyssintssint24s