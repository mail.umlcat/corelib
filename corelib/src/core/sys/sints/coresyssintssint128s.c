/** Module: "coresyssintssint128s.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresyssintssint128s
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
 
// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

// --> this module own header goes here
#include "coresyssintssint128s.h"
 
// ------------------

/* functions */

bool /* $$ func */ coresyssintssint128s_isempty
  (/* $$ in */ const sint128_t /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* procedures */

void /* $$ func */ coresyssintssint128s_clear
  (/* $$ inout */ sint128_t* /* $$ param */ ADest)
{
  //coresysbasefuncs_nothing();
} // func

void /* $$ func */ coresyssintssint128s_assign
  (/* $$ inout */ nonconst sint128_t* /* $$ param */ ADest,
   /* $$ in */    const    sint128_t  /* $$ param */ ASource)
{
  coresysbasefuncs_nothing();
} // func 

// ------------------

/* operators */

enum comparison /* $$ func */ coresyssintssint128s_compare
  (/* $$ in */ const sint128_t /* $$ param */ A,
   /* $$ in */ const sint128_t /* $$ param */ B)
{
  enum comparison /* $$ var */ Result = comparison_equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresyssintssint128s_equal
  (/* $$ in */ const sint128_t /* $$ param */ A,
   /* $$ in */ const sint128_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresyssintssint128s_different
  (/* $$ in */ const sint128_t /* $$ param */ A,
   /* $$ in */ const sint128_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* rtti */

bool /* $$ func */ coresyssintssint128s_isdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresyssintssint128s_tryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* $$ func */ coresyssintssint128s_applydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue)
{
  //coresysbasefuncs_nothing();
} // func

// ------------------


 // ...

// ------------------

/* $$ override */ int /* $$ func */ coresyssintssint128s_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresyssintssint128s_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------


 // ...

// $$ } // namespace coresyssintssint128s