/** Module: "coresyssints.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresyssints
// $$ {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

// --> this same module header goes here
#include "coresyssints.h"

// ------------------

#include "coresyssintssint8s.h"
#include "coresyssintssint16s.h"
#include "coresyssintssint32s.h"
#include "coresyssintssint64s.h"
#include "coresyssintssint128s.h"
#include "coresyssintssint256s.h"
#include "coresyssintssint24s.h"
#include "coresyssintssint48s.h"
#include "coresyssintssint80s.h"

// ------------------



 // ...

// ------------------

/* $$ override */ int /* $$ func */ coresyssints_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresyssints_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresyssints