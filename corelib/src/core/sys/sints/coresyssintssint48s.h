/** Module: "coresyssintssint48s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresyssintssint48s
// $$ {
 
// ------------------
 
#ifndef CORESYSSINTSINT48S_H
#define CORESYSSINTSINT48S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
 
// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

/* functions */

bool /* $$ func */ coresyssintssint48s_isempty
  (/* $$ in */ const sint48_t /* $$ param */ ASource);

// ------------------

/* procedures */

void /* $$ func */ coresyssintssint48s_clear
  (/* $$ inout */ nonconst sint48_t* /* $$ param */ ADest);

void /* $$ func */ coresyssintssint48s_assign
  (/* $$ inout */ nonconst sint48_t* /* $$ param */ ADest,
   /* $$ in */    const    sint48_t  /* $$ param */ ASource);
   
// ------------------

/* operators */

enum comparison /* $$ func */ coresyssintssint48s_compare
  (/* $$ in */ const sint48_t /* $$ param */ A,
   /* $$ in */ const sint48_t /* $$ param */ B);

bool /* $$ func */ coresyssintssint48s_equal
  (/* $$ in */ const sint48_t /* $$ param */ A,
   /* $$ in */ const sint48_t /* $$ param */ B);

bool /* $$ func */ coresyssintssint48s_different
  (/* $$ in */ const sint48_t /* $$ param */ A,
   /* $$ in */ const sint48_t /* $$ param */ B);
   
// ------------------

/* rtti */

bool /* $$ func */ coresyssintssint48s_isdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresyssintssint48s_tryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresyssintssint48s_applydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresyssintssint48s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresyssintssint48s_finish
  ( noparams );

// ------------------

#endif // CORESYSSINTSINT48S_H

// $$ } // namespace coresyssintssint48s