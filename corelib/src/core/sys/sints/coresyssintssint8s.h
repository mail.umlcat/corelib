/** Module: "coresyssintssint8s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresyssintssint8s
// $$ {
 
// ------------------
 
#ifndef CORESYSSINTSINT8S_H
#define CORESYSSINTSINT8S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
 
// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

/* functions */


bool /* $$ func */ coresyssintssint8s_isempty
  (/* $$ in */ const sint8_t /* $$ param */ ASource);

  
// ------------------

/* procedures */

void /* $$ func */ coresyssintssint8s_clear
  (/* $$ inout */ nonconst sint8_t* /* $$ param */ ADest);

void /* $$ func */ coresyssintssint8s_assign
  (/* $$ inout */ nonconst sint8_t* /* $$ param */ ADest,
   /* $$ in */    const    sint8_t  /* $$ param */ ASource);

// ------------------

/* operators */

enum comparison /* $$ func */ coresyssintssint8s_compare
  (/* $$ in */ const sint8_t /* $$ param */ A,
   /* $$ in */ const sint8_t /* $$ param */ B);

bool /* $$ func */ coresyssintssint8s_equal
  (/* $$ in */ const sint8_t /* $$ param */ A,
   /* $$ in */ const sint8_t /* $$ param */ B);

bool /* $$ func */ coresyssintssint8s_different
  (/* $$ in */ const sint8_t /* $$ param */ A,
   /* $$ in */ const sint8_t /* $$ param */ B);
   
// ------------------

/* rtti */
  
bool /* $$ func */ coresyssintssint8s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresyssintssint8s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresyssintssint8s_rttiapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresyssintssint8_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresyssintssint8_finish
  ( noparams );

// ------------------

#endif // CORESYSSINTSINT8S_H

// $$ } // namespace coresyssintssint8s