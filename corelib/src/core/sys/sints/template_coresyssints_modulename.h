/** Module: "coresyssints<modulename>s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresyssints<modulename>s
// $$ {
 
// ------------------
 
#ifndef CORESYSSINTS<MODULENAME>S_H
#define CORESYSSINTS<MODULENAME>S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
 
// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

/* functions */


bool /* $$ func */ coresyssints<modulename>s_isempty
  (/* $$ in */ const <typename>_t /* $$ param */ ASource);

  
// ------------------

/* procedures */

void /* $$ func */ coresyssints<modulename>s_clear
  (/* $$ inout */ nonconst <typename>_t* /* $$ param */ ADest);

void /* $$ func */ coresyssints<modulename>s_assign
  (/* $$ inout */ nonconst <typename>_t* /* $$ param */ ADest,
   /* $$ in */    const    <typename>_t  /* $$ param */ ASource);

// ------------------

/* operators */

enum comparison /* $$ func */ coresyssints<modulename>s_compare
  (/* $$ in */ const <typename>_t /* $$ param */ A,
   /* $$ in */ const <typename>_t /* $$ param */ B);

bool /* $$ func */ coresyssints<modulename>s_equal
  (/* $$ in */ const <typename>_t /* $$ param */ A,
   /* $$ in */ const <typename>_t /* $$ param */ B);

bool /* $$ func */ coresyssints<modulename>s_different
  (/* $$ in */ const <typename>_t /* $$ param */ A,
   /* $$ in */ const <typename>_t /* $$ param */ B);
   
// ------------------

/* rtti */

#define MOD_coresysbinfloats<modulename>s
// $$ {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

bool /* $$ func */ coresyssints<modulename>s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresyssints<modulename>s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresyssints<modulename>s_rttiapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresyssintssint8_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresyssintssint8_finish
  ( noparams );

// ------------------

#endif // CORESYSSINTS<MODULENAME>S_H

// ------------------

// $$ } // namespace coresyssints<modulename>s
