/** Module: "coresyssintssint80s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresyssintssint80s
// $$ {
 
// ------------------
 
#ifndef CORESYSSINTSINT80S_H
#define CORESYSSINTSINT80S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
 
// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

/* functions */

bool /* $$ func */ coresyssintssint80s_isempty
  (/* $$ in */ const sint80_t /* $$ param */ ASource);

// ------------------

/* procedures */

void /* $$ func */ coresyssintssint80s_clear
  (/* $$ inout */ nonconst sint80_t* /* $$ param */ ADest);

void /* $$ func */ coresyssintssint80s_assign
  (/* $$ inout */ nonconst sint80_t* /* $$ param */ ADest,
   /* $$ in */    const    sint80_t  /* $$ param */ ASource);

   
// ------------------

/* operators */

enum comparison /* $$ func */ coresyssintssint80s_compare
  (/* $$ in */ const sint80_t /* $$ param */ A,
   /* $$ in */ const sint80_t /* $$ param */ B);

bool /* $$ func */ coresyssintssint80s_equal
  (/* $$ in */ const sint80_t /* $$ param */ A,
   /* $$ in */ const sint80_t /* $$ param */ B);

bool /* $$ func */ coresyssintssint80s_different
  (/* $$ in */ const sint80_t /* $$ param */ A,
   /* $$ in */ const sint80_t /* $$ param */ B);

// ------------------

/* rtti */

bool /* $$ func */ coresyssintssint80s_isdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresyssintssint80s_tryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresyssintssint80s_applydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresyssintssint80s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresyssintssint80s_finish
  ( noparams );

// ------------------

#endif // CORESYSSINTSINT80S_H

// $$ } // namespace coresyssintssint80s