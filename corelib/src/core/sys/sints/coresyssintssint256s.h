/** Module: "coresyssintssint256s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresyssintssint256s
// $$ {
 
// ------------------
 
#ifndef CORESYSSINTSINT256S_H
#define CORESYSSINTSINT256S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
 
// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

/* functions */

bool /* $$ func */ coresyssintssint256s_isempty
  (/* $$ in */ const sint256_t /* $$ param */ ASource);

// ------------------

/* procedures */

void /* $$ func */ coresyssintssint256s_clear
  (/* $$ inout */ nonconst sint256_t* /* $$ param */ ADest);

void /* $$ func */ coresyssintssint256s_assign
  (/* $$ inout */ nonconst sint256_t* /* $$ param */ ADest,
   /* $$ in */    const    sint256_t  /* $$ param */ ASource);

bool /* $$ func */ coresyssintssint256s_different
  (/* $$ in */ const sint256_t /* $$ param */ A,
   /* $$ in */ const sint256_t /* $$ param */ B);
   
// ------------------

/* operators */

enum comparison /* $$ func */ coresyssintssint256s_compare
  (/* $$ in */ const sint256_t /* $$ param */ A,
   /* $$ in */ const sint256_t /* $$ param */ B);

bool /* $$ func */ coresyssintssint256s_equal
  (/* $$ in */ const sint256_t /* $$ param */ A,
   /* $$ in */ const sint256_t /* $$ param */ B);

// ------------------

/* rtti */

bool /* $$ func */ coresyssintssint256s_isdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresyssintssint256s_tryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresyssintssint256s_applydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresyssintssint256s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresyssintssint256s_finish
  ( noparams );

// ------------------

#endif // CORESYSSINTSINTS256S_H

// $$ } // namespace coresyssintssint256s