/** Module: "coresyssints<modulename>s.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresyssints<modulename>s
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>
 
// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

// --> this module own header goes here
#include "coresyssints<modulename>s.h"
 
// ------------------

/* functions */

bool /* $$ func */ coresyssints<modulename>s_isempty
  (/* $$ in */ const <modulename>_t /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* procedures */

void /* $$ func */ coresyssints<modulename>s_clear
  (/* $$ inout */ nonconst <modulename>_t* /* $$ param */ ADest)
{
  //coresysbasefuncs_nothing();
} // func

void /* $$ func */ coresyssints<modulename>s_assign
  (/* $$ inout */ nonconst <modulename>_t* /* $$ param */ ADest,
   /* $$ in */    const    <modulename>_t  /* $$ param */ ASource)
{
  coresysbasefuncs_nothing();
} // func 

// ------------------

/* operators */

enum comparison /* $$ func */ coresyssints<modulename>s_compare
  (/* $$ in */ const <modulename>_t /* $$ param */ A,
   /* $$ in */ const <modulename>_t /* $$ param */ B)
{
  enum comparison /* $$ var */ Result = comparison_equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresyssints<modulename>s_equal
  (/* $$ in */ const <modulename>_t /* $$ param */ A,
   /* $$ in */ const <modulename>_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresyssints<modulename>s_different
  (/* $$ in */ const <modulename>_t /* $$ param */ A,
   /* $$ in */ const <modulename>_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* rtti */

bool /* $$ func */ coresyssints<modulename>s_isdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresyssints<modulename>s_tryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* $$ func */ coresyssints<modulename>s_applydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue)
{
  //coresysbasefuncs_nothing();
} // func

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresyssintssint8_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresyssintssint8_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresyssints<modulename>s