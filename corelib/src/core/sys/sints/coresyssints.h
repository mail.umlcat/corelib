/** Module: "coresyssints.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresyssints
// $$ {
 
// ------------------
 
#ifndef CORESYSSINTS_H
#define CORESYSSINTS_H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

#include "coresyssintssint8s.h"
#include "coresyssintssint16s.h"
#include "coresyssintssint32s.h"
#include "coresyssintssint64s.h"
#include "coresyssintssint128s.h"
#include "coresyssintssint256s.h"
#include "coresyssintssint24s.h"
#include "coresyssintssint48s.h"
#include "coresyssintssint80s.h"

// ------------------

enum signedintegers
{
  signedintegers_sint8_t,    
  signedintegers_sint16_t,   
  signedintegers_sint32_t,   
  signedintegers_sint64_t,   
  signedintegers_sint128_t,  
  signedintegers_sint256_t,  
  signedintegers_sint24_t,
  signedintegers_sint48_t,
  signedintegers_sint80_t,
} ;

typedef
  enum signedintegers   /* $$ as */ coresyssints_signedintegers;

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresyssints_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresyssints_finish
  ( noparams );

// ------------------

#endif // CORESYSSINTS_H

// ------------------

// $$ } // namespace coresyssints