/** Module: "coresys"
 ** Filename: "coresys.h"
 ** Qualified Identifier:
 ** "core::system" 
 ** Descr.:
 ** "System Package Module"
 **/

// namespace coresys {
 
// ------------------
 
#ifndef CORESYS_H
#define CORESYS_H
 
// ------------------

// "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysuints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresyssints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbinfloats.h"
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysdecfloats.h"
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysstrs.h"
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresystypes.h"
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysconv.h"

// ------------------

// --> basic libraries here
// #include "coresysmacros.h"
// #include "coresyserrorcodes.h"
// #include "coresysenums.h"
// #include "coresysmem.h"
// #include "coresysuuids.h"
// #include "coresystypes.h"
// #include "coresysmodules.h"
// #include "coresyssinttypes.h"
// #include "coresysuinttypes.h"
// #include "coresysmeminttypes.h"
// // #include "coresysdecs.h"
// #include "coresysfloattypes.h"
// #include "coresysastrtypes.h"
// #include "coresyswstrtypes.h"
// #include "coresysfuncs.h"
// #include "coresysfunctors.h"

// ------------------

// other "corelib" libraries here
// #include <put your library here>
// #include <put your library here>
// #include <put your library here>

// ------------------

// other NON "corelib" libraries here
// #include <put your library here>
// #include <put your library here>
// #include <put your library here>

// ------------------

#define MOD_coresys {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x02};

// ------------------




 // ...
 
// ------------------

/* override */ int /* func */ coresys__setup
  ( noparams );

/* override */ int /* func */ coresys__setoff
  ( noparams );

// ------------------

#endif // CORESYS_H

// } // namespace coresys