/** Module: "coresys"
 ** Filename: "coresys.h"
 ** Qualified Identifier:
 ** "core::system" 
 ** Descr.:
 ** "System Package Module"
 **/

// namespace coresys {
 
// ------------------

// "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// basic libraries here
#include "coresysmacros.h"
#include "coresyserrorcodes.h"
#include "coresysenumtypes.h"
#include "coresysmem.h"
#include "coresysuuids.h"
#include "coresystypes.h"
#include "coresysmodules.h"
#include "coresyssinttypes.h"
#include "coresysuinttypes.h"
#include "coresysmeminttypes.h"
// #include "coresysdecs.h"
#include "coresysfloattypes.h"
#include "coresysastrtypes.h"
#include "coresyswstrtypes.h"
#include "coresysfuncs.h"
#include "coresysfunctors.h"

// ------------------

// other "corelib" libraries here
// #include <put your library here>
// #include <put your library here>
// #include <put your library here>

// ------------------

// other NON "corelib" libraries here
// #include <put your library here>
// #include <put your library here>
// #include <put your library here>

// ------------------

// --> this module own header goes here
#include "coresys.h"
 
// ------------------


 // ...

// ------------------

/* override */ int /* func */ coresys__setup
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  //coresystem__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coresys__setoff
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  //coresystem__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coresys
