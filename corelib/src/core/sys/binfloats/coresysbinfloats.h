/** Module: "coresysbinfloats.h"
 ** Descr.: "..."
 **/

// $$ namespace coresysbinfloats
// $$ {

// ------------------

#ifndef CORESYSBINFLOATS_H
#define CORESYSBINFLOATS_H

// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

#include "coresysbinfloatsbf16s.h"
#include "coresysbinfloatsbf32s.h"
#include "coresysbinfloatsbf64s.h"
#include "coresysbinfloatsbf128s.h"
#include "coresysbinfloatsbf256s.h"

// ------------------


// ------------------

 // ...

// ------------------

/* $$ override */ int /* $$ func */ coresysbinfloats_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysbinfloats_finish
  ( noparams );

// ------------------

#endif // CORESYSBINFLOATS_H

// ------------------

// $$ } // namespace coresysbinfloats
