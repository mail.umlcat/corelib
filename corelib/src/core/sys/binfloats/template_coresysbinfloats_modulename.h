/** Module: "coresysbinfloats<modulename>s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysbinfloats<modulename>s
// $$ {
 
// ------------------
 
#ifndef CORESYSBINFLOATS<MODULENAME>S_H
#define CORESYSBINFLOATS<MODULENAME>S_H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

 
// ------------------

/* functions */

bool /* $$ func */ coresysbinfloats<modulename>s_isempty
  (/* $$ in */ const <typename>_t /* $$ param */ ASource);
  
// ------------------

/* procedures */


void /* $$ func */ coresysbinfloats<modulename>s_clear
  (/* $$ inout */ nonconst <typename>_t* /* $$ param */ ADest);
  
void /* $$ func */ coresysbinfloats<modulename>s_assign
  (/* $$ inout */ nonconst <typename>_t* /* $$ param */ ADest,
   /* $$ in */    const    <typename>_t  /* $$ param */ ASource);
   
// ------------------

/* operators */


enum comparison /* $$ func */ coresysbinfloats<modulename>s_compare
  (/* $$ in */ const <typename>_t /* $$ param */ A,
   /* $$ in */ const <typename>_t /* $$ param */ B);

bool /* $$ func */ coresysbinfloats<modulename>s_equal
  (/* $$ in */ const <typename>_t /* $$ param */ A,
   /* $$ in */ const <typename>_t /* $$ param */ B);

bool /* $$ func */ coresysbinfloats<modulename>s_different
  (/* $$ in */ const <typename>_t /* $$ param */ A,
   /* $$ in */ const <typename>_t /* $$ param */ B);
   
// ------------------

/* rtti */

#define MOD_coresysbinfloats<modulename>s
// $$ {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

bool /* $$ func */ coresysbinfloats<modulename>s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysbinfloats<modulename>s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresysbinfloats<modulename>s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysbinfloats<modulename>s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysbinfloats<modulename>s_finish
  ( noparams );

// ------------------

#endif // CORESYSBINFLOATS<MODULENAME>S_H

// ------------------

//  $$ } // $$ namespace coresysbinfloats<modulename>s
