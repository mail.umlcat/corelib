/** Module: "coresysbinfloatsbf256s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysbinfloatsbf256s
// $$ {
 
// ------------------
 
#ifndef CORESYSBINFLOATSBF256S_H
#define CORESYSBINFLOATSBF256S_H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

 
// ------------------

/* functions */

bool /* $$ func */ coresysbinfloatsbf256s_isempty
  (/* $$ in */ const binfloat256_t /* $$ param */ ASource);
  
// ------------------

/* procedures */


void /* $$ func */ coresysbinfloatsbf256s_clear
  (/* $$ inout */ nonconst binfloat256_t* /* $$ param */ ADest);
  
void /* $$ func */ coresysbinfloatsbf256s_assign
  (/* $$ inout */ nonconst binfloat256_t* /* $$ param */ ADest,
   /* $$ in */    const    binfloat256_t  /* $$ param */ ASource);
   
// ------------------

/* operators */


enum comparison /* $$ func */ coresysbinfloatsbf256s_compare
  (/* $$ in */ const binfloat256_t /* $$ param */ A,
   /* $$ in */ const binfloat256_t /* $$ param */ B);

bool /* $$ func */ coresysbinfloatsbf256s_equal
  (/* $$ in */ const binfloat256_t /* $$ param */ A,
   /* $$ in */ const binfloat256_t /* $$ param */ B);

bool /* $$ func */ coresysbinfloatsbf256s_different
  (/* $$ in */ const binfloat256_t /* $$ param */ A,
   /* $$ in */ const binfloat256_t /* $$ param */ B);
   
// ------------------

/* rtti */

bool /* $$ func */ coresysbinfloatsbf256s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysbinfloatsbf256s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresysbinfloatsbf256s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysbinfloatsbf256s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysbinfloatsbf256s_finish
  ( noparams );

// ------------------

#endif // CORESYSBINFLOATSBF256S_H

//  $$ } // $$ namespace coresysbinfloatsbf256s