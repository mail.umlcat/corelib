/** Module: "coresysbinfloatsbf16s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysbinfloatsbf16s
// $$ {
 
// ------------------
 
#ifndef CORESYSBINFLOATSBF16S_H
#define CORESYSBINFLOATSBF16S_H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

 
// ------------------

/* functions */

bool /* $$ func */ coresysbinfloatsbf16s_isempty
  (/* $$ in */ const binfloat16_t /* $$ param */ ASource);
  
// ------------------

/* procedures */


void /* $$ func */ coresysbinfloatsbf16s_clear
  (/* $$ inout */ nonconst binfloat16_t* /* $$ param */ ADest);
  
void /* $$ func */ coresysbinfloatsbf16s_assign
  (/* $$ inout */ nonconst binfloat16_t* /* $$ param */ ADest,
   /* $$ in */    const    binfloat16_t  /* $$ param */ ASource);
   
// ------------------

/* operators */


enum comparison /* $$ func */ coresysbinfloatsbf16s_compare
  (/* $$ in */ const binfloat16_t /* $$ param */ A,
   /* $$ in */ const binfloat16_t /* $$ param */ B);

bool /* $$ func */ coresysbinfloatsbf16s_equal
  (/* $$ in */ const binfloat16_t /* $$ param */ A,
   /* $$ in */ const binfloat16_t /* $$ param */ B);

bool /* $$ func */ coresysbinfloatsbf16s_different
  (/* $$ in */ const binfloat16_t /* $$ param */ A,
   /* $$ in */ const binfloat16_t /* $$ param */ B);
   
// ------------------

/* rtti */

bool /* $$ func */ coresysbinfloatsbf16s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysbinfloatsbf16s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresysbinfloatsbf16s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysbinfloatsbf16s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysbinfloatsbf16s_finish
  ( noparams );

// ------------------

#endif // CORESYSBINFLOATSBF16S_H

//  $$ } // $$ namespace coresysbinfloatsbf16s