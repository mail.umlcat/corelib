/** Module: "coresysbinfloatsbf32s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysbinfloatsbf32s
// $$ {
 
// ------------------
 
#ifndef CORESYSBINFLOATSBF32S_H
#define CORESYSBINFLOATSBF32S_H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

 
// ------------------

/* functions */

bool /* $$ func */ coresysbinfloatsbf32s_isempty
  (/* $$ in */ const binfloat32_t /* $$ param */ ASource);
  
// ------------------

/* procedures */


void /* $$ func */ coresysbinfloatsbf32s_clear
  (/* $$ inout */ nonconst binfloat32_t* /* $$ param */ ADest);
  
void /* $$ func */ coresysbinfloatsbf32s_assign
  (/* $$ inout */ nonconst binfloat32_t* /* $$ param */ ADest,
   /* $$ in */    const    binfloat32_t  /* $$ param */ ASource);
   
// ------------------

/* operators */


enum comparison /* $$ func */ coresysbinfloatsbf32s_compare
  (/* $$ in */ const binfloat32_t /* $$ param */ A,
   /* $$ in */ const binfloat32_t /* $$ param */ B);

bool /* $$ func */ coresysbinfloatsbf32s_equal
  (/* $$ in */ const binfloat32_t /* $$ param */ A,
   /* $$ in */ const binfloat32_t /* $$ param */ B);

bool /* $$ func */ coresysbinfloatsbf32s_different
  (/* $$ in */ const binfloat32_t /* $$ param */ A,
   /* $$ in */ const binfloat32_t /* $$ param */ B);
   
// ------------------

/* rtti */

bool /* $$ func */ coresysbinfloatsbf32s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysbinfloatsbf32s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresysbinfloatsbf32s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysbinfloatsbf32s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysbinfloatsbf32s_finish
  ( noparams );

// ------------------

#endif // CORESYSBINFLOATSBF32S_H

//  $$ } // $$ namespace coresysbinfloatsbf32s