/** Module: "coresysbinfloatsbf16s.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysbinfloatsbf16s
// $$ {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

#include "coresysbinfloatsbf16s.h"
 
// ------------------

/* functions */

bool /* $$ func */ coresysbinfloatsbf16s_isempty
  (/* $$ in */ const binfloat16_t /* $$ param */ ASource);
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* procedures */


void /* $$ func */ coresysbinfloatsbf16s_clear
  (/* $$ inout */ nonconst binfloat16_t* /* $$ param */ ADest);
{
  coresysbasefuncs_nothing();
} // func
  
void /* $$ func */ coresysbinfloatsbf16s_assign
  (/* $$ inout */ nonconst binfloat16_t* /* $$ param */ ADest,
   /* $$ in */    const    binfloat16_t  /* $$ param */ ASource);
{
  coresysbasefuncs_nothing();
} // func
  
// ------------------

/* operators */


enum comparison /* $$ func */ coresysbinfloatsbf16s_compare
  (/* $$ in */ const binfloat16_t /* $$ param */ A,
   /* $$ in */ const binfloat16_t /* $$ param */ B);
{
  enum comparison /* $$ var */ Result = comparison_equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysbinfloatsbf16s_equal
  (/* $$ in */ const binfloat16_t /* $$ param */ A,
   /* $$ in */ const binfloat16_t /* $$ param */ B);
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysbinfloatsbf16s_different
  (/* $$ in */ const binfloat16_t /* $$ param */ A,
   /* $$ in */ const binfloat16_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* rtti */
 
bool /* $$ func */ coresysbinfloatsbf16s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysbinfloatsbf16s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* $$ func */ coresysbinfloatsbf16s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue)
{
  coresysbasefuncs_nothing();
} // func

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysbinfloatsbf16s_start
  ( noparams )
{
  coresysbasefuncs_nothing();
} // func

/* $$ override */ int /* $$ func */ coresysbinfloatsbf16s_finish
  ( noparams )
{
  coresysbasefuncs_nothing();
} // func

// ------------------


 // ...

//  $$ } // $$ namespace coresysbinfloatsbf16s