/** Module: "coresysbinfloatsbf128s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysbinfloatsbf128s
// $$ {
 
// ------------------
 
#ifndef CORESYSBINFLOATSBF128S_H
#define CORESYSBINFLOATSBF128S_H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

 
// ------------------

/* functions */

bool /* $$ func */ coresysbinfloatsbf128s_isempty
  (/* $$ in */ const binfloat128_t /* $$ param */ ASource);
  
// ------------------

/* procedures */


void /* $$ func */ coresysbinfloatsbf128s_clear
  (/* $$ inout */ nonconst binfloat128_t* /* $$ param */ ADest);
  
void /* $$ func */ coresysbinfloatsbf128s_assign
  (/* $$ inout */ nonconst binfloat128_t* /* $$ param */ ADest,
   /* $$ in */    const    binfloat128_t  /* $$ param */ ASource);
   
// ------------------

/* operators */


enum comparison /* $$ func */ coresysbinfloatsbf128s_compare
  (/* $$ in */ const binfloat128_t /* $$ param */ A,
   /* $$ in */ const binfloat128_t /* $$ param */ B);

bool /* $$ func */ coresysbinfloatsbf128s_equal
  (/* $$ in */ const binfloat128_t /* $$ param */ A,
   /* $$ in */ const binfloat128_t /* $$ param */ B);

bool /* $$ func */ coresysbinfloatsbf128s_different
  (/* $$ in */ const binfloat128_t /* $$ param */ A,
   /* $$ in */ const binfloat128_t /* $$ param */ B);
   
// ------------------

/* rtti */

bool /* $$ func */ coresysbinfloatsbf128s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysbinfloatsbf128s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresysbinfloatsbf128s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysbinfloatsbf128s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysbinfloatsbf128s_finish
  ( noparams );

// ------------------

#endif // CORESYSBINFLOATSBF128S_H

//  $$ } // $$ namespace coresysbinfloatsbf128s