/** Module: "coresysbinfloatsbf128s.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysbinfloatsbf128s
// $$ {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

#include "coresysbinfloatsbf128s.h"
 
// ------------------

/* functions */

bool /* $$ func */ coresysbinfloatsbf128s_isempty
  (/* $$ in */ const binfloat128_t /* $$ param */ ASource);
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* procedures */


void /* $$ func */ coresysbinfloatsbf128s_clear
  (/* $$ inout */ nonconst binfloat128_t* /* $$ param */ ADest);
{
  coresysbasefuncs_nothing();
} // func
  
void /* $$ func */ coresysbinfloatsbf128s_assign
  (/* $$ inout */ nonconst binfloat128_t* /* $$ param */ ADest,
   /* $$ in */    const    binfloat128_t  /* $$ param */ ASource);
{
  coresysbasefuncs_nothing();
} // func
  
// ------------------

/* operators */


enum comparison /* $$ func */ coresysbinfloatsbf128s_compare
  (/* $$ in */ const binfloat128_t /* $$ param */ A,
   /* $$ in */ const binfloat128_t /* $$ param */ B);
{
  enum comparison /* $$ var */ Result = comparison_equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysbinfloatsbf128s_equal
  (/* $$ in */ const binfloat128_t /* $$ param */ A,
   /* $$ in */ const binfloat128_t /* $$ param */ B);
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysbinfloatsbf128s_different
  (/* $$ in */ const binfloat128_t /* $$ param */ A,
   /* $$ in */ const binfloat128_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* rtti */
 
bool /* $$ func */ coresysbinfloatsbf128s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysbinfloatsbf128s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* $$ func */ coresysbinfloatsbf128s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue)
{
  coresysbasefuncs_nothing();
} // func

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysbinfloatsbf128s_start
  ( noparams )
{
  coresysbasefuncs_nothing();
} // func

/* $$ override */ int /* $$ func */ coresysbinfloatsbf128s_finish
  ( noparams )
{
  coresysbasefuncs_nothing();
} // func

// ------------------


 // ...

//  $$ } // $$ namespace coresysbinfloatsbf128s