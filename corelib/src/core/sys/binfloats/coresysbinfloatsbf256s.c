/** Module: "coresysbinfloatsbf256s.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysbinfloatsbf256s
// $$ {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

#include "coresysbinfloatsbf256s.h"
 
// ------------------

/* functions */

bool /* $$ func */ coresysbinfloatsbf256s_isempty
  (/* $$ in */ const binfloat256_t /* $$ param */ ASource);
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* procedures */


void /* $$ func */ coresysbinfloatsbf256s_clear
  (/* $$ inout */ nonconst binfloat256_t* /* $$ param */ ADest);
{
  coresysbasefuncs_nothing();
} // func
  
void /* $$ func */ coresysbinfloatsbf256s_assign
  (/* $$ inout */ nonconst binfloat256_t* /* $$ param */ ADest,
   /* $$ in */    const    binfloat256_t  /* $$ param */ ASource);
{
  coresysbasefuncs_nothing();
} // func
  
// ------------------

/* operators */


enum comparison /* $$ func */ coresysbinfloatsbf256s_compare
  (/* $$ in */ const binfloat256_t /* $$ param */ A,
   /* $$ in */ const binfloat256_t /* $$ param */ B);
{
  enum comparison /* $$ var */ Result = comparison_equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysbinfloatsbf256s_equal
  (/* $$ in */ const binfloat256_t /* $$ param */ A,
   /* $$ in */ const binfloat256_t /* $$ param */ B);
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysbinfloatsbf256s_different
  (/* $$ in */ const binfloat256_t /* $$ param */ A,
   /* $$ in */ const binfloat256_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* rtti */
 
bool /* $$ func */ coresysbinfloatsbf256s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysbinfloatsbf256s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* $$ func */ coresysbinfloatsbf256s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue)
{
  coresysbasefuncs_nothing();
} // func

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysbinfloatsbf256s_start
  ( noparams )
{
  coresysbasefuncs_nothing();
} // func

/* $$ override */ int /* $$ func */ coresysbinfloatsbf256s_finish
  ( noparams )
{
  coresysbasefuncs_nothing();
} // func

// ------------------


 // ...

//  $$ } // $$ namespace coresysbinfloatsbf256s