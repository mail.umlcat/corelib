/** Module: "coresysbinfloatsbf64s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysbinfloatsbf64s
// $$ {
 
// ------------------
 
#ifndef CORESYSBINFLOATSBF64S_H
#define CORESYSBINFLOATSBF64S_H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

 
// ------------------

/* functions */

bool /* $$ func */ coresysbinfloatsbf64s_isempty
  (/* $$ in */ const binfloat64_t /* $$ param */ ASource);
  
// ------------------

/* procedures */


void /* $$ func */ coresysbinfloatsbf64s_clear
  (/* $$ inout */ nonconst binfloat64_t* /* $$ param */ ADest);
  
void /* $$ func */ coresysbinfloatsbf64s_assign
  (/* $$ inout */ nonconst binfloat64_t* /* $$ param */ ADest,
   /* $$ in */    const    binfloat64_t  /* $$ param */ ASource);
   
// ------------------

/* operators */


enum comparison /* $$ func */ coresysbinfloatsbf64s_compare
  (/* $$ in */ const binfloat64_t /* $$ param */ A,
   /* $$ in */ const binfloat64_t /* $$ param */ B);

bool /* $$ func */ coresysbinfloatsbf64s_equal
  (/* $$ in */ const binfloat64_t /* $$ param */ A,
   /* $$ in */ const binfloat64_t /* $$ param */ B);

bool /* $$ func */ coresysbinfloatsbf64s_different
  (/* $$ in */ const binfloat64_t /* $$ param */ A,
   /* $$ in */ const binfloat64_t /* $$ param */ B);
   
// ------------------

/* rtti */

bool /* $$ func */ coresysbinfloatsbf64s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysbinfloatsbf64s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresysbinfloatsbf64s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysbinfloatsbf64s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysbinfloatsbf64s_finish
  ( noparams );

// ------------------

#endif // CORESYSBINFLOATSBF64S_H

//  $$ } // $$ namespace coresysbinfloatsbf64s