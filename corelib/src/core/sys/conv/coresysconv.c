/** Module: "coresysconv.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysconv 
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\uints\coresysuints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\sints\coresyssints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbinfloats.h"
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysdecfloats.h"
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysstrs.h"
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresystypes.h"

// ------------------

// --> contained modules here
#include "coresysconvansihexas.h"
#include "coresysconvansiocts.h"
#include "coresysconvmemintbyptransidecconv.h"
#include "coresysconvmemintbyptransihexconv.h"
#include "coresysconvmemintbyptransibinconv.h"
#include "coresysconvmemintbyptransioctconv.h"
#include "coresysconvuintbyptransidecconv.h"
#include "coresysconvuintbyptransihexconv.h"
#include "coresysconvuintbyptransibinconv.h"
#include "coresysconvuintbyptransioctconv.h"
// .include "coresysconvbinfloat256s.h"
// .include "coresysconvlongfloats.h"

// ------------------

// --> this same module header goes here
#include "coresysconv.h"
 
// ------------------


// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysconv_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysconv_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresysconv