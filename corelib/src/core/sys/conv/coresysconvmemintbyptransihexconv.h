/** Module: "coresysconvmemintbyptransihexconv.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysconvmemintbyptransihexconv {
 
// ------------------
 
#ifndef CORESYSCONVMEMINTBYPTRANSIHEXCONV_H
#define CORESYSCONVMEMINTBYPTRANSIHEXCONV_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\uints\coresysuints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

// --> same package libraries here
#include "coresysconvansihexas.h"
// .include "coresysconvmemintbyptransihexconvbinfloat32s.h"
// .include "coresysconvmemintbyptransihexconvbinfloat64s.h"
// .include "coresysconvmemintbyptransihexconvbinfloat128s.h"
// .include "coresysconvmemintbyptransihexconvbinfloat256s.h"
// .include "coresysconvmemintbyptransihexconvlongfloats.h"

// ------------------

/* types */

// ------------------

/* functions */

bool /* $$ func */ coresysconvmemintbyptransihexconv_tryconcatmemint8ptrtohexstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource);
   
// ------------------

/* procedures */

void /* $$ func */ coresysconvmemintbyptransihexconv_concatmemint8ptrtohexstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource);

// ------------------

/* rtti */

#define MOD_coresysconvmemintbyptransihexconv {0xBD,0xA9,0xD6,0x37,0xBE,0xC9,0xE8,0x43,0xB5,0x5E,0x20,0xE8,0x4E,0x84,0x68,0x2B};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysconvmemintbyptransihexconv_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysconvmemintbyptransihexconv_finish
  ( noparams );

// ------------------

#endif // CORESYSCONVMEMINTBYPTRANSIHEXCONV_H

// $$ } //  namespace coresysconvmemintbyptransihexconv