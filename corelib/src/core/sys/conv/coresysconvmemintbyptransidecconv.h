/** Module: "coresysconvmemintbyptransidecconv.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysconvmemintbyptransidecconv {
 
// ------------------
 
#ifndef CORESYSCONVMEMINTBYPTRANSIDECCONV_H
#define CORESYSCONVMEMINTBYPTRANSIDECCONV_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\uints\coresysuints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

// --> same package libraries here
// .include "coresysconvmemintbyptransidecconvbinfloat16s.h"
// .include "coresysconvmemintbyptransidecconvbinfloat32s.h"
// .include "coresysconvmemintbyptransidecconvbinfloat64s.h"
// .include "coresysconvmemintbyptransidecconvbinfloat128s.h"
// .include "coresysconvmemintbyptransidecconvbinfloat256s.h"
// .include "coresysconvmemintbyptransidecconvlongfloats.h"

// ------------------

/* types */

// ------------------

/* functions */

bool /* $$ func */ coresysconvmemintbyptransidecconv_tryconcatmemint8ptrtodecstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource);

// ------------------

/* procedures */

void /* $$ func */ coresysconvmemintbyptransidecconv_concatmemint8ptrtodecstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource);

// ------------------

/* rtti */

#define MOD_coresysconvmemintbyptransidecconv {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysconvmemintbyptransidecconv_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysconvmemintbyptransidecconv_finish
  ( noparams );

// ------------------

#endif // CORESYSCONVMEMINTBYPTRANSIDECCONV_H

// $$ } //  namespace coresysconvmemintbyptransidecconv