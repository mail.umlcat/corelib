/** Module: "coresysconvmemintbyptransidecconv.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysconvmemintbyptransidecconv 
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\uints\coresysuints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

// --> same package libraries here
// .include "coresysconvmemintbyptransidecconvbinfloat16s.h"
// .include "coresysconvmemintbyptransidecconvbinfloat32s.h"
// .include "coresysconvmemintbyptransidecconvbinfloat64s.h"
// .include "coresysconvmemintbyptransidecconvbinfloat128s.h"
// .include "coresysconvmemintbyptransidecconvbinfloat256s.h"
// .include "coresysconvmemintbyptransidecconvlongfloats.h"

// ------------------

// --> this same module header goes here
#include "coresysconvmemintbyptransidecconv.h"
 
// ------------------

/* functions */

bool /* $$ func */ coresysconvmemintbyptransidecconv_tryconcatmemint8ptrtodecstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource)
{
  bool       /* $$ var */ Result = false; 
  // ---

  memint8_t*   /* $$ var */ ASourcePtr;
  
  memint8_t    /* $$ var */ ANumber;
  
  // ---
  
  memint8_t    /* $$ var */ X;
  memint8_t    /* $$ var */ Y;
  memint8_t    /* $$ var */ Z;
  
  ansichar   /* $$ var */ TempBuffer[256];
  size_t     /* $$ var */ TempSize;
  ansichar*  /* $$ var */ TempPtr;
  
  ansichar   /* $$ var */ C;
  
  // ---

  Result =
    ( true
    && (ADestPtr != NULL)
    && (ADestSize > 0)
    );
  if (Result)
  {
    ASourcePtr =
      (memint8_t*) ASource;
	  
	// ---
  
    coresysstrsansinullstrs_clearsize
      (ADestPtr, ADestSize);
      
    TempSize =
      sizeof(TempBuffer);
    TempPtr =
      /* & */ TempBuffer;
      
    coresysstrsansinullstrs_clearsize
      (TempPtr, TempSize);

    if ((*ASourcePtr) != 0)
    {
      ANumber =
        (*ASourcePtr);
        
      while (ANumber > 0)
      {
        X = (ANumber % 10);
        Z = (ANumber / 10);
        Y = (X + 48);
        
        C =
          coresyscharsansichars_dectochar(Y);
        coresysstrsansinullstrs_addcharsize
          (TempPtr, TempSize, C);
          
        ANumber = Z;
      } // while
      
      // reverse text
      coresysstrsansinullstrs_assignreversesize
        (ADestPtr, ADestSize, TempPtr, TempSize);
    }
    else
    {
      coresysstrsansinullstrs_addcharsize
        (ADestPtr, ADestSize, '0');
    }
  } // if (Result)
  
  // ---
  return Result;
} // func


// ------------------

/* procedures */

void /* $$ func */ coresysconvmemintbyptransidecconv_concatmemint8ptrtodecstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource)
{
  bool /* $$ var */ CanContinue = false;
  // ---

  CanContinue =
    coresysconvmemintbyptransidecconv_tryconcatmemint8ptrtodecstr
	  (ADestPtr, ADestSize, ASource);
  if (! CanContinue)
  {
    // raise error
  } // if (Result)
} // func

// ------------------

/* rtti */

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysconvmemintbyptransidecconv_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysconvmemintbyptransidecconv_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } //  namespace coresysconvmemintbyptransidecconv