/** Module: "coresysconvansihexas.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysconvansihexas
// $$ {
 
// ------------------
 
#ifndef CORESYSCONVANSIHEXAS_H
#define CORESYSCONVANSIHEXAS_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"

// ------------------

// --> same package libraries here
// .include "coresysconvansihexasbinfloat16s.h"
// .include "coresysconvansihexasbinfloat32s.h"
// .include "coresysconvansihexasbinfloat64s.h"
// .include "coresysconvansihexasbinfloat128s.h"
// .include "coresysconvansihexasbinfloat256s.h"
// .include "coresysconvansihexaslongfloats.h"

// ------------------

/* types */

// ------------------

/* functions */

ansichar const * /* $$ func */ coresysconvansihexas_hexchars
  ( noparams );

byte_t /* $$ func */ coresysconvansihexas_hexchartodec
  (/* $$ in */ const ansichar /* $$ param */ AValue);

ansichar /* $$ func */ coresysconvansihexas_dectohexchar
  (/* $$ in */ const byte_t /* $$ param */ AValue);
  
// ------------------

/* procedures */

// ------------------

/* rtti */

#define MOD_coresysconvansihexas {0xA8,0x79,0x2C,0x1A,0xFD,0x2E,0xA1,0x4D,0xAE,0xE9,0xD9,0xC5,0x44,0xDC,0x20,0xEF};};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysconvansihexas_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysconvansihexas_finish
  ( noparams );

// ------------------

#endif // CORESYSCONVANSIHEXAS_H

// $$ } //  namespace coresysconvansihexas