/** Module: "coresysconvuintbyptransidecconv.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysconvuintbyptransidecconv 
// $$ {
 
// ------------------
 
#ifndef CORESYSCONVUINTBYPTRANSIDECCONV_H
#define CORESYSCONVUINTBYPTRANSIDECCONV_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\uints\coresysuints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

// --> same package libraries here
// .include "coresysconvuintbyptransidecconvbinfloat16s.h"
// .include "coresysconvuintbyptransidecconvbinfloat32s.h"
// .include "coresysconvuintbyptransidecconvbinfloat64s.h"
// .include "coresysconvuintbyptransidecconvbinfloat128s.h"
// .include "coresysconvuintbyptransidecconvbinfloat256s.h"
// .include "coresysconvuintbyptransidecconvlongfloats.h"

// ------------------

/* types */

// ------------------

/* functions */

bool /* $$ func */ coresysconvuintbyptransidecconv_tryconcatuint8ptrtodecstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource);

// ------------------

/* procedures */

void /* $$ func */ coresysconvuintbyptransidecconv_concatuint8ptrtodecstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource);

// ------------------

/* rtti */

#define MOD_coresysconvuintbyptransidecconv 
// $$ {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysconvuintbyptransidecconv_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysconvuintbyptransidecconv_finish
  ( noparams );

// ------------------

#endif // CORESYSCONVUINTBYPTRANSIDECCONV_H

// $$ } //  namespace coresysconvuintbyptransidecconv