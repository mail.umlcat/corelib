/** Module: "coresysconv<modulename>s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysconv<modulename>s
// $$ {
 
// ------------------
 
#ifndef CORESYSCONV<MODULENAME>S_H
#define CORESYSCONV<MODULENAME>S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

// --> same package libraries here
// .include "coresysconv<modulename>sbinfloat16s.h"
// .include "coresysconv<modulename>sbinfloat32s.h"
// .include "coresysconv<modulename>sbinfloat64s.h"
// .include "coresysconv<modulename>sbinfloat128s.h"
// .include "coresysconv<modulename>sbinfloat256s.h"
// .include "coresysconv<modulename>slongfloats.h"

// ------------------

/* types */

// ------------------

/* functions */

// ------------------

/* procedures */

// ------------------

/* rtti */

#define MOD_coresysconv<modulename>s
// $$ {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysconv<modulename>s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysconv<modulename>s_finish
  ( noparams );

// ------------------

#endif // CORESYSCONV<MODULENAME>S_H

// ------------------

// $$ } //  namespace coresysconv<modulename>s