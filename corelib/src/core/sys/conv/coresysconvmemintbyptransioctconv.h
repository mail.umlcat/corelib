/** Module: "coresysconvmemintbyptransioctconv.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysconvmemintbyptransioctconv 
// $$ {
 
// ------------------
 
#ifndef CORESYSCONVMEMINTBYPTRANSIOCTCONV_H
#define CORESYSCONVMEMINTBYPTRANSIOCTCONV_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\uints\coresysuints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

// --> same package libraries here
#include "coresysconvansiocts.h"
// .include "coresysconvmemintbyptransioctconvbinfloat32s.h"
// .include "coresysconvmemintbyptransioctconvbinfloat64s.h"
// .include "coresysconvmemintbyptransioctconvbinfloat128s.h"
// .include "coresysconvmemintbyptransioctconvbinfloat256s.h"
// .include "coresysconvmemintbyptransioctconvlongfloats.h"

// ------------------

/* types */

// ------------------

/* functions */

bool /* $$ func */ coresysconvmemintbyptransioctconv_tryconcatmemint8ptrtooctstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource);
   
// ------------------


/* procedures */

void /* $$ func */ coresysconvmemintbyptransioctconv_concatmemint8ptrtooctstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource);
   
// ------------------

/* rtti */

#define MOD_coresysconvmemintbyptransioctconv 
// $$ {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysconvmemintbyptransioctconv_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysconvmemintbyptransioctconv_finish
  ( noparams );

// ------------------

#endif // CORESYSCONVMEMINTBYPTRANSIOCTCONV_H

// $$ } //  namespace coresysconvmemintbyptransioctconv