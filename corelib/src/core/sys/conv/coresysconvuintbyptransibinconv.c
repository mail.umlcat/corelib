/** Module: "coresysconvuintbyptransibinconv.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysconvuintbyptransibinconv 
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\uints\coresysuints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

// --> same package libraries here
// .include "coresysconvuintbyptransibinconvbinfloat16s.h"
// .include "coresysconvuintbyptransibinconvbinfloat32s.h"
// .include "coresysconvuintbyptransibinconvbinfloat64s.h"
// .include "coresysconvuintbyptransibinconvbinfloat128s.h"
// .include "coresysconvuintbyptransibinconvbinfloat256s.h"
// .include "coresysconvuintbyptransibinconvlongfloats.h"

// ------------------

// --> this same module header goes here
#include "coresysconvuintbyptransibinconv.h"
 
// ------------------

/* types */

// ------------------

/* functions */

bool /* $$ func */ coresysconvuintbyptransibinconv_tryconcatuint8ptrtobinstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource)
{
  bool       /* $$ var */ Result = false; 
  // ---

  uint8_t*   /* $$ var */ ASourcePtr;
  
  // ---
  
  uint64_t   /* $$ var */ p_nb_int = 0;
  byte_t     /* $$ var */ p_nb_digits = 0;
  
  // ---

  Result =
    ( true
    && (ADestPtr != NULL)
    && (ADestSize > 0)
    );
  if (Result)
  {
    ASourcePtr =
      (uint8_t*) ASource;

    // ---


    if ((*ASourcePtr) != 0)
    {
	    p_nb_int    = *ASourcePtr;
	    p_nb_digits = 8;

      coresysstrsansinullstrs_assignsamecharsize
	      (ADestPtr, ADestSize, '?', (p_nb_digits - 1));
	  
	    while (p_nb_digits > 0)
	    {
	    	if (coresysuintsuint8s_isodd(p_nb_int))
	    	{
 	    	  ADestPtr[p_nb_digits - 1] = '1';
	    	}
	    	else
	    	{
	    	  ADestPtr[p_nb_digits - 1] = '0';
    		}
		
	    	p_nb_int = 
	    	  (p_nb_int >> 1);

	     	(--p_nb_digits);
	    } // while
    }
    else
    {
      coresysstrsansinullstrs_addcharsize
        (ADestPtr, ADestSize, '0');
    }
  } // if (Result)
  
  // ---
  return Result;
} // func

 
// ------------------

/* procedures */

void /* $$ func */ coresysconvuintbyptransibinconv_concatuint8ptrtobinstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource)
{
  bool /* $$ var */ CanContinue = false;
  // ---

  CanContinue =
    coresysconvuintbyptransibinconv_tryconcatuint8ptrtobinstr
	  (ADestPtr, ADestSize, ASource);
  if (! CanContinue)
  {
    // raise error
  } // if (Result)
} // func

// ------------------

/* rtti */

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysconvuintbyptransibinconv_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysconvuintbyptransibinconv_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } //  namespace coresysconvuintbyptransibinconv