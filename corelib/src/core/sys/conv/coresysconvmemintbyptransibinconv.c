/** Module: "coresysconvmemintbyptransibinconv.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysconvmemintbyptransibinconv 
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\uints\coresysuints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

// --> same package libraries here
// .include "coresysconvmemintbyptransibinconvbinfloat16s.h"
// .include "coresysconvmemintbyptransibinconvbinfloat32s.h"
// .include "coresysconvmemintbyptransibinconvbinfloat64s.h"
// .include "coresysconvmemintbyptransibinconvbinfloat128s.h"
// .include "coresysconvmemintbyptransibinconvbinfloat256s.h"
// .include "coresysconvmemintbyptransibinconvlongfloats.h"

// ------------------

// --> this same module header goes here
#include "coresysconvmemintbyptransibinconv.h"
 
// ------------------

/* types */

// ------------------

/* functions */

bool /* $$ func */ coresysconvmemintbyptransibinconv_tryconcatmemint8ptrtobinstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource)
{
  bool       /* $$ var */ Result = false; 
  // ---

  memint8_t*   /* $$ var */ ASourcePtr;
  
  // ---
  
  uint64_t   /* $$ var */ p_nb_int = 0;
  byte_t     /* $$ var */ p_nb_digits = 0;
  
  // ---

  Result =
    ( true
    && (ADestPtr != NULL)
    && (ADestSize > 0)
    );
  if (Result)
  {
    ASourcePtr =
      (memint8_t*) ASource;

    // ---


    if ((*ASourcePtr) != 0)
    {
	    p_nb_int    = *ASourcePtr;
	    p_nb_digits = 8;

      coresysstrsansinullstrs_assignsamecharsize
	      (ADestPtr, ADestSize, '?', (p_nb_digits - 1));
	  
	    while (p_nb_digits > 0)
	    {
	    	if (coresysuintsuint8s_isodd(p_nb_int))
	    	{
 	    	  ADestPtr[p_nb_digits - 1] = '1';
	    	}
	    	else
	    	{
	    	  ADestPtr[p_nb_digits - 1] = '0';
    		}
		
	    	p_nb_int = 
	    	  (p_nb_int >> 1);

	     	(--p_nb_digits);
	    } // while
    }
    else
    {
      coresysstrsansinullstrs_addcharsize
        (ADestPtr, ADestSize, '0');
    }
  } // if (Result)
  
  // ---
  return Result;
} // func

 
// ------------------

/* procedures */

void /* $$ func */ coresysconvmemintbyptransibinconv_concatmemint8ptrtobinstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource)
{
  bool /* $$ var */ CanContinue = false;
  // ---

  CanContinue =
    coresysconvmemintbyptransibinconv_tryconcatmemint8ptrtobinstr
	  (ADestPtr, ADestSize, ASource);
  if (! CanContinue)
  {
    // raise error
  } // if (Result)
} // func

// ------------------

/* rtti */

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysconvmemintbyptransibinconv_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysconvmemintbyptransibinconv_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } //  namespace coresysconvmemintbyptransibinconv