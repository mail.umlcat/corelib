/** Module: "coresysconvansiocts.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysconvansiocts
// $$ {
 
// ------------------
 
#ifndef CORESYSCONVANSIOCTS_H
#define CORESYSCONVANSIOCTS_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"

// ------------------

// --> same package libraries here
// .include "coresysconvansioctsbinfloat16s.h"
// .include "coresysconvansioctsbinfloat32s.h"
// .include "coresysconvansioctsbinfloat64s.h"
// .include "coresysconvansioctsbinfloat128s.h"
// .include "coresysconvansioctsbinfloat256s.h"
// .include "coresysconvansioctslongfloats.h"

// ------------------

/* types */

// ------------------

/* functions */

const ansichar const * /* $$ func */ coresysconvansiocts_octchars
  ( noparams );

byte_t /* $$ func */ coresysconvansiocts_octchartodec
  (/* $$ in */ const ansichar /* $$ param */ AValue);

ansichar /* $$ func */ coresysconvansiocts_dectooctchar
  (/* $$ in */ const byte_t /* $$ param */ AValue);
  
// ------------------

/* procedures */

// ------------------

/* rtti */

#define MOD_coresysconvansiocts
// $$ {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysconvansiocts_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysconvansiocts_finish
  ( noparams );

// ------------------

#endif // CORESYSCONVANSIOCTS_H

// $$ } //  namespace coresysconvansiocts