/** Module: "coresysconvmemintbyptransibinconv.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysconvmemintbyptransibinconv
// $$ {
 
// ------------------
 
#ifndef CORESYSCONVMEMINTBYPTRANSIBINCONV_H
#define CORESYSCONVMEMINTBYPTRANSIBINCONV_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\uints\coresysuints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

// --> same package libraries here
// .include "coresysconvmemintbyptransibinconvbinfloat16s.h"
// .include "coresysconvmemintbyptransibinconvbinfloat32s.h"
// .include "coresysconvmemintbyptransibinconvbinfloat64s.h"
// .include "coresysconvmemintbyptransibinconvbinfloat128s.h"
// .include "coresysconvmemintbyptransibinconvbinfloat256s.h"
// .include "coresysconvmemintbyptransibinconvlongfloats.h"

// ------------------

/* types */

// ------------------

/* functions */

bool /* $$ func */ coresysconvmemintbyptransibinconv_tryconcatmemint8ptrtobinstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource);
 
// ------------------

/* procedures */

void /* $$ func */ coresysconvmemintbyptransibinconv_concatmemint8ptrtobinstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource);
   
// ------------------

/* rtti */

#define MOD_coresysconvmemintbyptransibinconv 
// $$ {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysconvmemintbyptransibinconv_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysconvmemintbyptransibinconv_finish
  ( noparams );

// ------------------

#endif // CORESYSCONVMEMINTBYPTRANSIBINCONV_H

// $$ } //  namespace coresysconvmemintbyptransibinconv