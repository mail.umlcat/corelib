/** Module: "coresysconv.h"
 ** Descr.: "..."
 **/

// $$ namespace coresysconv 
// $$ {

// ------------------

#ifndef CORESYSCONV_H
#define CORESYSCONV_H

// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\uints\coresysuints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\sints\coresyssints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\binfloats\coresysbinfloats.h"
#include "C:\softdev\corelib\corelib\src\core\sys\decfloats\coresysdecfloats.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"
#include "C:\softdev\corelib\corelib\src\core\sys\types\coresystypes.h"

// ------------------

// --> contained modules here
#include "coresysconvansihexas.h"
#include "coresysconvansiocts.h"
#include "coresysconvmemintbyptransidecconv.h"
#include "coresysconvmemintbyptransihexconv.h"
#include "coresysconvmemintbyptransibinconv.h"
#include "coresysconvmemintbyptransioctconv.h"
#include "coresysconvuintbyptransidecconv.h"
#include "coresysconvuintbyptransihexconv.h"
#include "coresysconvuintbyptransibinconv.h"
#include "coresysconvuintbyptransioctconv.h"
// .include "coresysconvlongfloats.h"

// ------------------

/* rtti */

#define MOD_coresysconv 
// $$ {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...

// ------------------

/* $$ override */ int /* $$ func */ coresysconv_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysconv_finish
  ( noparams );

// ------------------

#endif // CORESYSCONV_H

// ------------------

// $$ } // namespace coresysconv
