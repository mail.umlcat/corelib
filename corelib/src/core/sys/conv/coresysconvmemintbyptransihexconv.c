/** Module: "coresysconvmemintbyptransihexconv.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysconvmemintbyptransihexconv 
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\uints\coresysuints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

// --> same package libraries here
#include "coresysconvansihexas.h"
// .include "coresysconvmemintbyptransihexconvbinfloat32s.h"
// .include "coresysconvmemintbyptransihexconvbinfloat64s.h"
// .include "coresysconvmemintbyptransihexconvbinfloat128s.h"
// .include "coresysconvmemintbyptransihexconvbinfloat256s.h"
// .include "coresysconvmemintbyptransihexconvlongfloats.h"

// ------------------

// --> this same module header goes here
#include "coresysconvmemintbyptransihexconv.h"
 
// ------------------

/* types */

// ------------------

/* functions */

bool /* $$ func */ coresysconvmemintbyptransihexconv_tryconcatmemint8ptrtohexstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource)
{
  bool       /* $$ var */ Result = false; 
  // ---

  memint8_t*   /* $$ var */ ASourcePtr;
  
  memint8_t    /* $$ var */ ANumber;
  
  memint8_t    /* $$ var */ Division;
  memint8_t    /* $$ var */ Remainder;
  
  ansichar   /* $$ var */ TempBuffer[256];
  size_t     /* $$ var */ TempSize;
  ansichar*  /* $$ var */ TempPtr;
  
  ansichar   /* $$ var */ C;
  
  int        /* $$ var */ Count;
  
  // ---

  Result =
    ( true
    && (ADestPtr != NULL)
    && (ADestSize > 0)
    );
  if (Result)
  {
    ASourcePtr =
      (memint8_t*) ASource;
  
    coresysstrsansinullstrs_clearsize
      (ADestPtr, ADestSize);
      
    TempSize =
      sizeof(TempBuffer);
    TempPtr =
      /* & */ TempBuffer;
      
    coresysstrsansinullstrs_clearsize
      (TempPtr, TempSize);

    if ((*ASourcePtr) != 0)
    {
      ANumber =
        (*ASourcePtr);
		
	    Count = 0;
	  
      do
	    {
        if (ANumber >= 0)
	    	{
          Remainder = ANumber % 16;
          Division  = ANumber / 16;
        
          C =
            coresysconvansihexas_dectohexchar(Remainder);
          coresysstrsansinullstrs_addcharsize
            (TempPtr, TempSize, C);
		  
          ANumber   = Division;
	    	} // if (ANumber >= 0)
	    }
	    while (ANumber >= 1);
      
      // reverse text
      coresysstrsansinullstrs_assignreversesize
        (ADestPtr, ADestSize, TempPtr, TempSize);
    }
    else
    {
      coresysstrsansinullstrs_addcharsize
        (ADestPtr, ADestSize, '0');
    }
  } // if (Result)
  
  // ---
  return Result;
} // func
   
// ------------------

/* procedures */

void /* $$ func */ coresysconvmemintbyptransihexconv_concatmemint8ptrtohexstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource)
{
  bool /* $$ var */ CanContinue = false;
  // ---

  CanContinue =
    coresysconvmemintbyptransihexconv_tryconcatmemint8ptrtohexstr
	  (ADestPtr, ADestSize, ASource);
  if (! CanContinue)
  {
    // raise error
  } // if (Result)
} // func

// ------------------

/* rtti */

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysconvmemintbyptransihexconv_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysconvmemintbyptransihexconv_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } //  namespace coresysconvmemintbyptransihexconv