/** Module: "coresysconvuintbyptransidecconv.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysconvuintbyptransidecconv 
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\uints\coresysuints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

// --> same package libraries here
// .include "coresysconvuintbyptransidecconvbinfloat16s.h"
// .include "coresysconvuintbyptransidecconvbinfloat32s.h"
// .include "coresysconvuintbyptransidecconvbinfloat64s.h"
// .include "coresysconvuintbyptransidecconvbinfloat128s.h"
// .include "coresysconvuintbyptransidecconvbinfloat256s.h"
// .include "coresysconvuintbyptransidecconvlongfloats.h"

// ------------------

// --> this same module header goes here
#include "coresysconvuintbyptransidecconv.h"
 
// ------------------

/* functions */

bool /* $$ func */ coresysconvuintbyptransidecconv_tryconcatuint8ptrtodecstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource)
{
  bool       /* $$ var */ Result = false; 
  // ---

  uint8_t*   /* $$ var */ ASourcePtr;
  
  uint8_t    /* $$ var */ ANumber;
  
  // ---
  
  uint8_t    /* $$ var */ X;
  uint8_t    /* $$ var */ Y;
  uint8_t    /* $$ var */ Z;
  
  ansichar   /* $$ var */ TempBuffer[256];
  size_t     /* $$ var */ TempSize;
  ansichar*  /* $$ var */ TempPtr;
  
  ansichar   /* $$ var */ C;
  
  // ---

  Result =
    ( true
    && (ADestPtr != NULL)
    && (ADestSize > 0)
    );
  if (Result)
  {
    ASourcePtr =
      (uint8_t*) ASource;
	  
	// ---
  
    coresysstrsansinullstrs_clearsize
      (ADestPtr, ADestSize);
      
    TempSize =
      sizeof(TempBuffer);
    TempPtr =
      /* & */ TempBuffer;
      
    coresysstrsansinullstrs_clearsize
      (TempPtr, TempSize);

    if ((*ASourcePtr) != 0)
    {
      ANumber =
        (*ASourcePtr);
        
      while (ANumber > 0)
      {
        X = (ANumber % 10);
        Z = (ANumber / 10);
        Y = (X + 48);
        
        C =
          coresyscharsansichars_dectochar(Y);
        coresysstrsansinullstrs_addcharsize
          (TempPtr, TempSize, C);
          
        ANumber = Z;
      } // while
      
      // reverse text
      coresysstrsansinullstrs_assignreversesize
        (ADestPtr, ADestSize, TempPtr, TempSize);
    }
    else
    {
      coresysstrsansinullstrs_addcharsize
        (ADestPtr, ADestSize, '0');
    }
  } // if (Result)
  
  // ---
  return Result;
} // func


// ------------------

/* procedures */

void /* $$ func */ coresysconvuintbyptransidecconv_concatuint8ptrtodecstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource)
{
  bool /* $$ var */ CanContinue = false;
  // ---

  CanContinue =
    coresysconvuintbyptransidecconv_tryconcatuint8ptrtodecstr
	  (ADestPtr, ADestSize, ASource);
  if (! CanContinue)
  {
    // raise error
  } // if (Result)
} // func

// ------------------

/* rtti */

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysconvuintbyptransidecconv_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysconvuintbyptransidecconv_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } //  namespace coresysconvuintbyptransidecconv