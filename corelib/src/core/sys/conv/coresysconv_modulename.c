/** Module: "coresysconv<modulename>s.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysconv<modulename>s
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

// --> same package libraries here
// .include "coresysconv<modulename>sbinfloat16s.h"
// .include "coresysconv<modulename>sbinfloat32s.h"
// .include "coresysconv<modulename>sbinfloat64s.h"
// .include "coresysconv<modulename>sbinfloat128s.h"
// .include "coresysconv<modulename>sbinfloat256s.h"
// .include "coresysconv<modulename>slongfloats.h"

// ------------------

// --> this same module header goes here
#include "coresysconv<modulename>s.h"
 
// ------------------

/* types */

// ------------------

/* functions */

// ------------------

/* procedures */

// ------------------

/* rtti */

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysconv<modulename>s_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysconv<modulename>s_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } //  namespace coresysconv<modulename>s