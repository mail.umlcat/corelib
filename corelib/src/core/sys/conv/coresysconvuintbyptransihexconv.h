/** Module: "coresysconvuintbyptransihexconv.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysconvuintbyptransihexconv 
// $$ {
 
// ------------------
 
#ifndef CORESYSCONVUINTBYPTRANSIHEXCONV_H
#define CORESYSCONVUINTBYPTRANSIHEXCONV_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\uints\coresysuints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

// --> same package libraries here
#include "coresysconvansihexas.h"
// .include "coresysconvuintbyptransihexconvbinfloat32s.h"
// .include "coresysconvuintbyptransihexconvbinfloat64s.h"
// .include "coresysconvuintbyptransihexconvbinfloat128s.h"
// .include "coresysconvuintbyptransihexconvbinfloat256s.h"
// .include "coresysconvuintbyptransihexconvlongfloats.h"

// ------------------

/* types */

// ------------------

/* functions */

bool /* $$ func */ coresysconvuintbyptransihexconv_tryconcatuint8ptrtohexstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource);
   
// ------------------

/* procedures */

void /* $$ func */ coresysconvuintbyptransihexconv_concatuint8ptrtohexstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource);

// ------------------

/* rtti */

#define MOD_coresysconvuintbyptransihexconv 
// $$ {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysconvuintbyptransihexconv_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysconvuintbyptransihexconv_finish
  ( noparams );

// ------------------

#endif // CORESYSCONVUINTBYPTRANSIHEXCONV_H

// $$ } //  namespace coresysconvuintbyptransihexconv