/** Module: "coresysconvuintbyptransibinconv.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysconvuintbyptransibinconv 
// $$ {
 
// ------------------
 
#ifndef CORESYSCONVUINTBYPTRANSIBINCONV_H
#define CORESYSCONVUINTBYPTRANSIBINCONV_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\uints\coresysuints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

// --> same package libraries here
// .include "coresysconvuintbyptransibinconvbinfloat16s.h"
// .include "coresysconvuintbyptransibinconvbinfloat32s.h"
// .include "coresysconvuintbyptransibinconvbinfloat64s.h"
// .include "coresysconvuintbyptransibinconvbinfloat128s.h"
// .include "coresysconvuintbyptransibinconvbinfloat256s.h"
// .include "coresysconvuintbyptransibinconvlongfloats.h"

// ------------------

/* types */

// ------------------

/* functions */

bool /* $$ func */ coresysconvuintbyptransibinconv_tryconcatuint8ptrtobinstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource);
 
// ------------------

/* procedures */

void /* $$ func */ coresysconvuintbyptransibinconv_concatuint8ptrtobinstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource);
   
// ------------------

/* rtti */

#define MOD_coresysconvuintbyptransibinconv 
// $$ {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysconvuintbyptransibinconv_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysconvuintbyptransibinconv_finish
  ( noparams );

// ------------------

#endif // CORESYSCONVUINTBYPTRANSIBINCONV_H

// $$ } //  namespace coresysconvuintbyptransibinconv