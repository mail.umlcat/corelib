/** Module: "coresysconvansihexas.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysconvansihexas
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"

// ------------------

// --> same package libraries here
// .include "coresysconvansihexasbinfloat16s.h"
// .include "coresysconvansihexasbinfloat32s.h"
// .include "coresysconvansihexasbinfloat64s.h"
// .include "coresysconvansihexasbinfloat128s.h"
// .include "coresysconvansihexasbinfloat256s.h"
// .include "coresysconvansihexaslongfloats.h"

// ------------------

// --> this same module header goes here
#include "coresysconvansihexas.h"

// ------------------

/* functions */

ansichar const * /* $$ func */ coresysconvansihexas_hexchars
  ( noparams )
{
  static ansichar /* $$ var */ setbuffer[] =
    {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F', '\0'};
  
  return (const ansichar const *) setbuffer;
} // func

byte_t /* $$ func */ coresysconvansihexas_hexchartodec
  (/* $$ in */ const ansichar /* $$ param */ AValue)
{
  byte_t /* $$ var */ Result = 0;
  // ---
  
  ansichar /* $$ var */ ASource = '\0';
  int      /* $$ var */ i = '\0';
  bool     /* $$ var */ Found = false;

  // ---
  
  const ansichar const * /* $$ var */ Items =
    coresysconvansihexas_hexchars();

  // ---
  
  ASource =
    /* & */ coresyscharsansichars_chartoupper(AValue);

  i = 0;
  Found = false;
  while ((! Found) && (i <= 15))
  {
    Found = (ASource == Items[i]);
    i++;
  }

  if (Found)
  {
    Result = (i - 1);
  }
  
  // ---
  return Result;
} // func

ansichar /* $$ func */ coresysconvansihexas_dectohexchar
  (/* $$ in */ const byte_t /* $$ param */ AValue)
{
  ansichar /* $$ var */ Result = '\0';
  // ---
  
  const ansichar const * /* $$ var */ Items =
    /* & */ coresysconvansihexas_hexchars();

  // ---
  
  if (AValue <= 15)
  {
     Result = Items[AValue];
  } // if
  
  // ---
  return Result;
} // func 
  
// ------------------

/* procedures */

// ------------------

/* rtti */

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysconvansihexas_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysconvansihexas_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } //  namespace coresysconvansihexas