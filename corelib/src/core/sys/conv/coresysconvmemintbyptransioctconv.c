/** Module: "coresysconvmemintbyptransioctconv.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysconvmemintbyptransioctconv 
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\uints\coresysuints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

// --> same package libraries here
#include "coresysconvansiocts.h"
// .include "coresysconvmemintbyptransioctconvbinfloat32s.h"
// .include "coresysconvmemintbyptransioctconvbinfloat64s.h"
// .include "coresysconvmemintbyptransioctconvbinfloat128s.h"
// .include "coresysconvmemintbyptransioctconvbinfloat256s.h"
// .include "coresysconvmemintbyptransioctconvlongfloats.h"

// ------------------

// --> this same module header goes here
#include "coresysconvmemintbyptransioctconv.h"
 
// ------------------

/* types */

// ------------------

/* functions */

bool /* $$ func */ coresysconvmemintbyptransioctconv_tryconcatmemint8ptrtooctstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource)
{
  bool       /* $$ var */ Result = false; 
  // ---

  memint8_t*   /* $$ var */ ASourcePtr;
  
  memint8_t    /* $$ var */ ASourceValue;
  
  byte_t     /* $$ var */ Digit;
  
  ansichar   /* $$ var */ C;
  
  ansichar   /* $$ var */ TempBuffer[256];
  size_t     /* $$ var */ TempSize;
  ansichar*  /* $$ var */ TempPtr;
  
  // ---

  Result =
    ( true
    && (ADestPtr != NULL)
    && (ADestSize > 0)
    );
  if (Result)
  {
    ASourcePtr =
      (memint8_t*) ASource;
  
    coresysstrsansinullstrs_clearsize
      (ADestPtr, ADestSize);
      
    TempSize =
      sizeof(TempBuffer);
    TempPtr =
      /* & */ TempBuffer;
      
    coresysstrsansinullstrs_clearsize
      (TempPtr, TempSize);
	  
	// ---

    if ((*ASourcePtr) != 0)
    {
      ASourceValue =
      *ASourcePtr;

	  while (ASourceValue > 0)
	  {
        Digit =
          (ASourceValue & 7);
		  
	    	// This is a bitwize AND, meaning:
	    	//      001001010011010 (Some number)
	    	//  AND 000000000000111 ( 7 )
	    	//      ---------------
	    	//      000000000000010 ( 2 )

	    	// convert this digit to Octal:
		
	    	ASourceValue =
	    	  (ASourceValue >> 3);
	     	// this "shifts" the number right 3 bits:
    	  	//         001001010011010 (Some number)
    	  	// becomes 000001001010011 (Some number DIV 8)
        C =
          coresysconvansiocts_dectooctchar(Digit);
		  
        coresysstrsansinullstrs_addcharsize
          (TempPtr, TempSize, C);
      } // while
      
       // reverse text
       coresysstrsansinullstrs_assignreversesize
          (ADestPtr, ADestSize, TempPtr, TempSize);
    }
    else
    {
        coresysstrsansinullstrs_addcharsize
          (ADestPtr, ADestSize, '0');
    }
  } // if (Result)
  
  // ---
  return Result;
} // func

// ------------------

/* procedures */

void /* $$ func */ coresysconvmemintbyptransioctconv_concatmemint8ptrtooctstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource)
{
  bool /* $$ var */ CanContinue = false;
  // ---

  CanContinue =
    coresysconvmemintbyptransioctconv_tryconcatmemint8ptrtooctstr
	  (ADestPtr, ADestSize, ASource);
  if (! CanContinue)
  {
    // raise error
  } // if (Result)
} // func
   
// ------------------

/* rtti */

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysconvmemintbyptransioctconv_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysconvmemintbyptransioctconv_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } //  namespace coresysconvmemintbyptransioctconv