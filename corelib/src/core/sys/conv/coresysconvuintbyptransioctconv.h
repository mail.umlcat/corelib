/** Module: "coresysconvuintbyptransioctconv.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysconvuintbyptransioctconv 
// $$ {
 
// ------------------
 
#ifndef CORESYSCONVUINTBYPTRANSIOCTCONV_H
#define CORESYSCONVUINTBYPTRANSIOCTCONV_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\uints\coresysuints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

// --> same package libraries here
#include "coresysconvansiocts.h"
// .include "coresysconvuintbyptransioctconvbinfloat32s.h"
// .include "coresysconvuintbyptransioctconvbinfloat64s.h"
// .include "coresysconvuintbyptransioctconvbinfloat128s.h"
// .include "coresysconvuintbyptransioctconvbinfloat256s.h"
// .include "coresysconvuintbyptransioctconvlongfloats.h"

// ------------------

/* types */

// ------------------

/* functions */

bool /* $$ func */ coresysconvuintbyptransioctconv_tryconcatuint8ptrtooctstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource);
   
// ------------------


/* procedures */

void /* $$ func */ coresysconvuintbyptransioctconv_concatuint8ptrtooctstr
  (/* $$ inout */ nonconst ansichar* /* $$ param */ ADestPtr,
   /* $$ in */    const    size_t    /* $$ param */ ADestSize,
   /* $$ in */    const    pointer*  /* $$ param */ ASource);
   
// ------------------

/* rtti */

#define MOD_coresysconvuintbyptransioctconv 
// $$ {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysconvuintbyptransioctconv_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysconvuintbyptransioctconv_finish
  ( noparams );

// ------------------

#endif // CORESYSCONVUINTBYPTRANSIOCTCONV_H

// $$ } //  namespace coresysconvuintbyptransioctconv