/** Module: "coresysuintsuint16s.h"
 ** Descr.:
 ** "Unsigned 16 Bits Integer Library."
 **/

// $$ namespace coresysuintsuint16s
// $$ {
 
// ------------------
 
#ifndef CORESYSUINTSUINT16S_H
#define CORESYSUINTSUINT16S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

/* functions */


bool /* $$ func */ coresysuintsuint16s_isempty
  (/* $$ in */ const uint16_t /* $$ param */ ASource);


// ------------------

/* procedures */


void /* $$ func */ coresysuintsuint16s_clear
  (/* $$ inout */ nonconst uint16_t* /* $$ param */ ADest);


void /* $$ func */ coresysuintsuint16s_assign
  (/* $$ inout */ nonconst uint16_t* /* $$ param */ ADest,
   /* $$ in */    const    uint16_t  /* $$ param */ ASource);
   
// ------------------

/* operators */

enum comparison /* $$ func */ coresysuintsuint16s_compare
  (/* $$ in */ const uint16_t /* $$ param */ A,
   /* $$ in */ const uint16_t /* $$ param */ B);

bool /* $$ func */ coresysuintsuint16s_equal
  (/* $$ in */ const uint16_t /* $$ param */ A,
   /* $$ in */ const uint16_t /* $$ param */ B);

bool /* $$ func */ coresysuintsuint16s_different
  (/* $$ in */ const uint16_t /* $$ param */ A,
   /* $$ in */ const uint16_t /* $$ param */ B);
   
// ------------------

bool /* $$ func */ coresysuintsuint16s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysuintsuint16s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresysuintsuint16s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);


 
 
// ------------------

/* $$ override */ int /* $$ func */ coresysuintsuint16s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysuintsuint16s_finish
  ( noparams );

// ------------------

#endif // CORESYSUINTSUINT16S_H

// $$ } // namespace coresysuintsuint16s