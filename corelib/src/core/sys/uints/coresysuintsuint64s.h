/** Module: "coresysuintsuint64s.h"
 ** Descr.: "Unsigned 64 Bits Integer Library."
 **/

// $$ namespace coresysuintsuint64s
// $$ {
 
// ------------------
 
#ifndef CORESYSUINTSUINT64S_H
#define CORESYSUINTSUINT64S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

/* functions */


bool /* $$ func */ coresysuintsuint64s_isempty
  (/* $$ in */ const uint64_t /* $$ param */ ASource);


// ------------------

/* procedures */


void /* $$ func */ coresysuintsuint64s_clear
  (/* $$ inout */ nonconst uint64_t /* $$ param */ ADest);


void /* $$ func */ coresysuintsuint64s_assign
  (/* $$ inout */ nonconst uint64_t* /* $$ param */ ADest,
   /* $$ in */    const    uint64_t  /* $$ param */ ASource);
   
// ------------------

/* operators */

enum comparison /* $$ func */ coresysuintsuint64s_compare
  (/* $$ in */ const uint64_t /* $$ param */ A,
   /* $$ in */ const uint64_t /* $$ param */ B);

bool /* $$ func */ coresysuintsuint64s_equal
  (/* $$ in */ const uint64_t /* $$ param */ A,
   /* $$ in */ const uint64_t /* $$ param */ B);

bool /* $$ func */ coresysuintssint64s_different
  (/* $$ in */ const uint64_t /* $$ param */ A,
   /* $$ in */ const uint64_t /* $$ param */ B);
   
// ------------------

/* rtti */

bool /* $$ func */ coresysuintsuint64s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysuintsuint64s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresysuintsuint64s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysuintsuint64s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysuintsuint64s_finish
  ( noparams );

// ------------------

#endif // CORESYSUINTSUINT64S_H

// $$ } // namespace coresysuintsuint64s