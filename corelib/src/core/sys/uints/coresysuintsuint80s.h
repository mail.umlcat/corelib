/** Module: "coresysuintsuint80s.h"
 ** Descr.: "Unsigned 80 Bits Integer Library."
 **/

// $$ namespace coresysuintsuint80s
// $$ {
 
// ------------------
 
#ifndef CORESYSUINTSUINT80S_H
#define CORESYSUINTSUINT80S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

/* functions */


bool /* $$ func */ coresysuintsuint80s_isempty
  (/* $$ in */ const uint80_t /* $$ param */ ASource);

// ------------------

/* procedures */


void /* $$ func */ coresysuintsuint80s_clear
  (/* $$ inout */ nonconst uint80_t /* $$ param */ ADest);

void /* $$ func */ coresysuintsuint80s_assign
  (/* $$ inout */ nonconst uint80_t* /* $$ param */ ADest,
   /* $$ in */    const    uint80_t  /* $$ param */ ASource);

bool /* $$ func */ coresysuintssint80s_different
  (/* $$ in */ const uint80_t /* $$ param */ A,
   /* $$ in */ const uint80_t /* $$ param */ B);
   
// ------------------

/* operators */

enum comparison /* $$ func */ coresysuintsuint80s_compare
  (/* $$ in */ const uint80_t /* $$ param */ A,
   /* $$ in */ const uint80_t /* $$ param */ B);

bool /* $$ func */ coresysuintsuint80s_equal
  (/* $$ in */ const uint80_t /* $$ param */ A,
   /* $$ in */ const uint80_t /* $$ param */ B);


// ------------------

/* rtti */

bool /* $$ func */ coresysuintsuint80s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysuintsuint80s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresysuintsuint80s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysuintsuint80s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysuintsuint80s_finish
  ( noparams );

// ------------------

#endif // CORESYSUINTSUINT80S_H

// $$ } // namespace coresysuintsuint80s