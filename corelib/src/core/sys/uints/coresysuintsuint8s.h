/** Module: "coresysuintsuint8s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysuintsuint8s
// $$ {
 
// ------------------
 
#ifndef CORESYSUINTSUINT8S_H
#define CORESYSUINTSUINT8S_H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

/* functions */

bool /* $$ func */ coresysuintsuint8s_isempty
  (/* $$ in */ const uint8_t /* $$ param */ ASource);

bool /* $$ func */ coresysuintsuint8s_iseven
  (/* $$ in */ const uint8_t /* $$ param */ ASource);

bool /* $$ func */ coresysuintsuint8s_isodd
  (/* $$ in */ const uint8_t /* $$ param */ ASource);
  
// ------------------

/* procedures */


void /* $$ func */ coresysuintsuint8s_clear
  (/* $$ inout */ nonconst uint8_t /* $$ param */ ADest);


void /* $$ func */ coresysuintsuint8s_assign
  (/* $$ inout */ nonconst uint8_t* /* $$ param */ ADest,
   /* $$ in */    const    uint8_t  /* $$ param */ ASource);
   
// ------------------

/* operators */

enum comparison /* $$ func */ coresysuintsuint8s_compare
  (/* $$ in */ const uint8_t /* $$ param */ A,
   /* $$ in */ const uint8_t /* $$ param */ B);

bool /* $$ func */ coresysuintsuint8s_equal
  (/* $$ in */ const uint8_t /* $$ param */ A,
   /* $$ in */ const uint8_t /* $$ param */ B);

bool /* $$ func */ coresysuintsuint8s_different
  (/* $$ in */ const uint8_t /* $$ param */ A,
   /* $$ in */ const uint8_t /* $$ param */ B);
   
// ------------------

/* rtti */

bool /* $$ func */ coresysuintsuint8s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysuintsuint8s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresysuintsuint8s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysuintsuint8_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysuintsuint8_finish
  ( noparams );

// ------------------

#endif // CORESYSUINTSUINT8S_H

// ------------------

// $$ } // namespace coresysuintsuint8s