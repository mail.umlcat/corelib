/** Module: "coresysuintsuint32s.h"
 ** Descr.: "Unsigned 32 Bits Integer Library."
 **/

// $$ namespace coresysuintsuint32s
// $$ {
 
// ------------------
 
#ifndef CORESYSUINTSUINT32S_H
#define CORESYSUINTSUINT32S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

/* functions */


bool /* $$ func */ coresysuintsuint32s_isempty
  (/* $$ in */ const uint32_t /* $$ param */ ASource);


// ------------------

/* procedures */


void /* $$ func */ coresysuintsuint32s_clear
  (/* $$ inout */ nonconst uint32_t /* $$ param */ ADest);
  

void /* $$ func */ coresysuintsuint32s_assign
  (/* $$ inout */ nonconst uint32_t* /* $$ param */ ADest,
   /* $$ in */    const    uint32_t  /* $$ param */ ASource);
   
// ------------------

/* operators */

enum comparison /* $$ func */ coresysuintsuint32s_compare
  (/* $$ in */ const uint32_t /* $$ param */ A,
   /* $$ in */ const uint32_t /* $$ param */ B);

bool /* $$ func */ coresysuintsuint32s_equal
  (/* $$ in */ const uint32_t /* $$ param */ A,
   /* $$ in */ const uint32_t /* $$ param */ B);


bool /* $$ func */ coresysuintssint32s_different
  (/* $$ in */ const uint32_t /* $$ param */ A,
   /* $$ in */ const uint32_t /* $$ param */ B);
   
// ------------------

/* rtti */

bool /* $$ func */ coresysuintsuint32s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysuintsuint32s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresysuintsuint32s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

 
// ------------------

/* $$ override */ int /* $$ func */ coresysuintsuint32s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysuintsuint32s_finish
  ( noparams );

// ------------------

#endif // CORESYSUINTSUINT32S_H

// $$ } // namespace coresysuintsuint32s