# corelib

The "corelib" projects is an Open Source "C" library,
aimed to help developers to built cross-platform applications.

It started in 1994, as a school project. Have received, several ideas,
from PHP and Delphi (Free Pascal), code migrations.

It was lost, due to a hard drive crash, but rebuilt from scratch.

The library project had to be restarted, to be used for a cross-platform application,
due to existing libraries, doesn't met the requirements, of the project.

This library emphazises design over efficency, efficency comes automatically,
identifiers used are longer than usual, and some operations, are duplicated,
and lower memory-efficient than the existing libraries.

Example:

The "coreansinullstrs.h" library replaces the "string.h" standard library,
with the "coreansinullstrs.h" library, which internally uses "string.h" .

And "wraps" several standard functions, with a more defined identifer,
the "strcmp" in "string.h", is replaced by the "coreansinullstrs__compare" function,
from the "coreansinullstrs.h" library.

This feature may seem unefficient, for some developers,
but useful, for others.

Also, adds special comments, to allow the source code to be easier to read,
as "/* function */" between the return type,
and the identifier of a function declaration, similar to Javascript or Delphi.

Features

* The Open Source License is LGPL 2.1, allowed to be used both,
in open source and commercial programs.

* All, non inclusion, source code files, start with the lowercase "core" prefix,
indicating the main library project.

* The main feature is to add a quick modular, namespace replacement.

* Each, non inclusion, source code file, is considered a namespace.

* All, non inclusion, source code files, start with the lowercase "core" prefix,
followed by with a lowercase identifier.

* "Class and Object Oriented Programming" is not used,
altought can be emulated with several programing techniques.

This library it's implemented in procedural "C", not Object and Class Oriented "C++",
altought a "C++" compiler, may be used.

* All functions start with the same lowercase identifier, as the filename,
followed by two consecutive underscores.

Those two consecutive underscores, should be treated as the "::" scope,
character of "C++" namespaces.

Example:

The "corestreams" file has functions like "corestreams__openonlyreadfile",
and "corestreams__closefile". They should be read as "corestreams::openonlyreadfile",
and "corestreams::closefile".

* Other identifiers, for variables, functions, types,
may vary wheter use the "core" prefix, or wheter should use lowercase,
uppercase, or mixed case. That's open to other developers to decide.

Is it possible to have an identifier,
with the lowercase "core*__" prefix, followed by a mixed case text.

Many commonly used, simple types, avoid using the "core*__" prefix,
to avoid making the sourcecode too complicated.

* The multiple usages of the keyword "void" are removed. as macro "noparams" is defined,
and used instead of "void", for functions that doesn't have any parameters. 

The macro or typedef "pointer", borrowed from "pascal", it's used instead of "void.
There fore "void" is only used to indicate a function does not have a return type.

* Each library has a "*__setup" loader function, and "*__setoff" unloader function,
that must be called explicitly. In some cases these methods are empty.

* Several libraries are "wrappers" to existing, sometimes, standard libraries,
and its respective functions.

The "coreansinullstrs.h" library replaces the "string.h" standard library,
and "wraps" several standard functions, with a more defined identifer,
the "strcmp" in "string.h", is replaced by the "coreansinullstrs__compare" function,
from the "coreansinullstrs.h" library.

A developer may use both "string.h" and "coreansinullstrs.h" libraries,
in its application.

* Some libraries are intended to avoid confusion, function overloading,
(several functions with different parameters and same identifier), are avoided,
as possible.

* In functions, with pointers, there are cases wheter the pointer addresses a structure.

In some cases, an operation requires to modify an existing structure,
in others, a new copy to be generated.

For example, in order to get a lowercase-only version of a string,
two functions are provided:

"coreansinullstrs__lowercasecopy(...)" for a new generated copy,
that must be deallocated from memory, by the programmer,
and "coreansinullstrs__lowercasechange(...)",
that may use a local character array, that is disposed by a function, automtically.

* Other features in process.





