/** Module: "coresysuintsypes"
 ** Filename: "coresysuintsypes.c"
 ** Qualified Identifier:
 ** "core::system::uints" 
 ** Descr.: "Unsigned Integer Types Library."
 **/

// $$ namespace coresysuintsypes
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

// --> this module own header goes here
#include "coresysuintstypes.h"

// ------------------

#include "coresysuintsuint8s.h"
#include "coresysuintsuint16s.h"
#include "coresysuintsuint32s.h"
#include "coresysuintsuint64s.h"
#include "coresysuintsuint128s.h"
#include "coresysuintsuint256s.h"
#include "coresysuintsuint24s.h"
#include "coresysuintsuint48s.h"
#include "coresysuintsuint80s.h"

// ------------------

/* functions */
 

// ------------------

/* procedures */


// ------------------

/* operators */


 
// ------------------

 // ...

// ------------------

/* $$ override */ int /* $$ func */ coresysuintsypes_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbase_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysuintsypes_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbase_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresysuintsypes