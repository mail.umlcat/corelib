/** Module: "coresysuintsuint256s.c"
 ** Descr.: "Unsigned 256 Bits Integer Library."
 **/

// $$ namespace coresysuintsuint256s
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

// --> this module own header goes here
#include "coresysuintsuint256s.h"
 
// ------------------

/* functions */

bool /* $$ func */ coresysuintsuint256s_isempty
  (/* $$ in */ const uint256_t /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* procedures */


void /* $$ func */ coresysuintsuint256s_clear
  (/* $$ inout */ nonconst uint256_t /* $$ param */ ADest)
{
  coresysbasefuncs_nothing();
} // func

void /* $$ func */ coresysuintsuint256s_assign
  (/* $$ inout */ nonconst uint256_t* /* $$ param */ ADest,
   /* $$ in */    const    uint256_t  /* $$ param */ ASource)
{
  coresysbasefuncs_nothing();
} // func 

// ------------------

/* operators */

enum comparison /* $$ func */ coresysuintsuint256s_compare
  (/* $$ in */ const uint256_t /* $$ param */ A,
   /* $$ in */ const uint256_t /* $$ param */ B)
{
  enum comparison /* $$ var */ Result = comparison_equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysuintsuint256s_equal
  (/* $$ in */ const uint256_t /* $$ param */ A,
   /* $$ in */ const uint256_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func


bool /* $$ func */ coresysuintsuint256s_different
  (/* $$ in */ const uint256_t /* $$ param */ A,
   /* $$ in */ const uint256_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

bool /* $$ func */ coresysuintsuint256s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysuintsuint256s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* $$ func */ coresysuintsuint256s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue)
{
  coresysbasefuncs_nothing();
} // func

// ------------------



 // ...

// ------------------

/* $$ override */ int /* $$ func */ coresysuintsuint256s_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysuintsuint256s_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------


 // ...

// $$ } // namespace coresysuintsuint256s