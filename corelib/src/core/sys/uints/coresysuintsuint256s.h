/** Module: "coresysuintsuint256s.h"
 ** Descr.: "Unsigned 256 Bits Integer Library."
 **/

// $$ namespace coresysuintsuint256s
// $$ {
 
// ------------------
 
#ifndef CORESYSUINTSUINT256S_H
#define CORESYSUINTSUINT256S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

/* functions */


bool /* $$ func */ coresysuintsuint256s_isempty
  (/* $$ in */ const uint256_t /* $$ param */ ASource);


// ------------------

/* procedures */


void /* $$ func */ coresysuintsuint256s_clear
  (/* $$ inout */ nonconst uint256_t /* $$ param */ ADest);

void /* $$ func */ coresysuintsuin256s_assign
  (/* $$ inout */ nonconst uint256_t* /* $$ param */ ADest,
   /* $$ in */    const    uint256_t  /* $$ param */ ASource);

bool /* $$ func */ coresysuintsuint256s_different
  (/* $$ in */ const uint256_t /* $$ param */ A,
   /* $$ in */ const uint256_t /* $$ param */ B);
   
// ------------------

/* operators */

enum comparison /* $$ func */ coresysuintsuint256s_compare
  (/* $$ in */ const uint256_t /* $$ param */ A,
   /* $$ in */ const uint256_t /* $$ param */ B);

bool /* $$ func */ coresysuintsuint256s_equal
  (/* $$ in */ const uint256_t /* $$ param */ A,
   /* $$ in */ const uint256_t /* $$ param */ B);

// ------------------

/* rtti */

bool /* $$ func */ coresysuintsuint256s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysuintsuint256s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresysuintsuint256s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysuintsuint256s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysuintsuint256s_finish
  ( noparams );

// ------------------

#endif // CORESYSUINTSUINTS256S_H

// $$ } // namespace coresysuintsuint256s