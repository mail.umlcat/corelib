/** Module: "coresysuintsuint48s.h"
 ** Descr.: "Unsigned 48 Bits Integer Library."
 **/

// $$ namespace coresysuintsuint48s
// $$ {
 
// ------------------
 
#ifndef CORESYSUINTSUINT48S_H
#define CORESYSUINTSUINT48S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

/* functions */


bool /* $$ func */ coresysuintsuint48s_isempty
  (/* $$ in */ const uint48_t /* $$ param */ ASource);


// ------------------

/* procedures */


void /* $$ func */ coresysuintsuint48s_clear
  (/* $$ inout */ nonconst uint48_t /* $$ param */ ADest);


void /* $$ func */ coresysuintsuint48s_assign
  (/* $$ inout */ nonconst uint48_t* /* $$ param */ ADest,
   /* $$ in */    const    uint48_t  /* $$ param */ ASource);
   
// ------------------

/* operators */

enum comparison /* $$ func */ coresysuintsuint48s_compare
  (/* $$ in */ const uint48_t /* $$ param */ A,
   /* $$ in */ const uint48_t /* $$ param */ B);

bool /* $$ func */ coresysuintsuint48s_equal
  (/* $$ in */ const uint48_t /* $$ param */ A,
   /* $$ in */ const uint48_t /* $$ param */ B);

bool /* $$ func */ coresysuintssint48s_different
  (/* $$ in */ const uint48_t /* $$ param */ A,
   /* $$ in */ const uint48_t /* $$ param */ B);
   
// ------------------

/* rtti */

bool /* $$ func */ coresysuintsuint48s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysuintsuint48s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresysuintsuint48s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

 // ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysuintsuint48s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysuintsuint48s_finish
  ( noparams );

// ------------------

#endif // CORESYSUINTSUINT48S_H

// $$ } // namespace coresysuintsuint48s