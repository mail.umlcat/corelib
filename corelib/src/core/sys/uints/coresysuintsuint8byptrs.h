/** Module: "coresysuintsuint8byptrs.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysuintsuint8byptrs
// $$ {
 
// ------------------
 
#ifndef CORESYSUINTSUINT8BYPTRS_H
#define CORESYSUINTSUINT8BYPTRS_H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

/* functions */


bool /* $$ func */ coresysuintsuint8byptrs_isempty
  (/* $$ in */ const pointer* /* $$ param */ ASource);

pointer* /* $$ func */ coresysuintsuint8byptrs_consttoptr
  (/* $$ in */ const uint8_t /* $$ param */ ASource);
  
// ------------------

/* procedures */

void /* $$ func */ coresysuintsuint8byptrs_clear
  (/* $$ inout */ nonconst pointer* /* $$ param */ ADest);

void /* $$ func */ coresysuintsuint8byptrs_assign
  (/* $$ inout */ nonconst pointer** /* $$ param */ ADest,
   /* $$ in */    const    pointer*  /* $$ param */ ASource);

void /* $$ func */ coresysuintsuint8byptrs_dropptr
  (/* $$ inout */ nonconst pointer** /* $$ param */ ADest);
   
// ------------------

/* operators */

enum comparison /* $$ func */ coresysuintsuint8byptrs_compare
  (/* $$ in */ const pointer* /* $$ param */ A,
   /* $$ in */ const pointer* /* $$ param */ B);

bool /* $$ func */ coresysuintsuint8byptrs_equal
  (/* $$ in */ const pointer* /* $$ param */ A,
   /* $$ in */ const pointer* /* $$ param */ B);

bool /* $$ func */ coresysuintsuint8byptrs_different
  (/* $$ in */ const pointer* /* $$ param */ A,
   /* $$ in */ const pointer* /* $$ param */ B);
   
// ------------------

/* rtti */

#define MOD_coresysuintsuint8byptrs {0x4A,0xCA,0x94,0xED,0x84,0x25,0xD0,0x40,0xA0,0x20,0xEE,0x86,0x49,0xDC,0x12,0x1B};


// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysuintsuint8byptrs_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysuintsuint8byptrs_finish
  ( noparams );

// ------------------

#endif // CORESYSUINTSUINT8BYPTRS_H

// $$ } // namespace coresysuintsuint8byptrs