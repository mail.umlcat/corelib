/** Module: "coresysuintsuint16s.c"
 ** Descr.:
 ** "Unsigned 16 Bits Integer Library."
 **/

// $$ namespace coresysuintsuint16s
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

// --> this module own header goes here
#include "coresysuintsuint16s.h"
 
// ------------------

/* functions */

bool /* $$ func */ coresysuintsuint16s_isempty
  (/* $$ in */ const uint16_t /* $$ param */ ASource)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = (ASource == 0);

  // ---
  return Result;
} // func

// ------------------

/* procedures */


void /* $$ func */ coresysuintsuint16s_clear
  (/* $$ inout */ nonconst uint16_t* /* $$ param */ ADest)
{
  if (ADest != NULL)
  { 
    *ADest = 0;
  }
} // func

void /* $$ func */ coresysuintsuint16s_assign
  (/* $$ inout */ nonconst uint16_t* /* $$ param */ ADest,
   /* $$ in */    const    uint16_t  /* $$ param */ ASource)
{
  coresysbasefuncs_nothing();
} // func 

// ------------------

/* operators */

enum comparison /* $$ func */ coresysuintsuint16s_compare
  (/* $$ in */ const uint16_t /* $$ param */ A,
   /* $$ in */ const uint16_t /* $$ param */ B)
{
  enum comparison /* $$ var */ Result = comparison_equal;
  // ---
     
  if (A < B)
  { 
    Result = comparison_lesser;
  }
  else if (A > B)
  { 
    Result = comparison_greater;
  }
  else
  { 
    Result = comparison_equal;
  }

  // ---
  return Result;
} // func

bool /* $$ func */ coresysuintsuint16s_equal
  (/* $$ in */ const uint16_t /* $$ param */ A,
   /* $$ in */ const uint16_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = (A == B);

  // ---
  return Result;
} // func


bool /* $$ func */ coresysuintsuint16s_different
  (/* $$ in */ const uint16_t /* $$ param */ A,
   /* $$ in */ const uint16_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

bool /* $$ func */ coresysuintsuint16s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---

  Result =
    (AValue != NULL);
  if (Result)
  {
    uint16_t* /* $$ var */ ThisDest;

    // ---

    ThisDest =
      (uint16_t*) AValue;

    Result = (*ThisDest == 0);
  } // if

  // ---
  return Result;
} // func

bool /* $$ func */ coresysuintsuint16s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ ADestValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
  if (Result)
  {
    uint16_t* /* $$ var */ ThisDest;

    // ---

    ThisDest =
      (uint16_t*) ADestValue;

    *ThisDest = 0;
  } // if

  // ---
  return Result;
} // func

void /* $$ func */ coresysuintsuint16s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ ADestValue)
{
//  discard coresysuintsuint16s_rttitryapplydefaultvalue
//    (ADestValue);
} // func

// ------------------



 // ...

// ------------------


/* $$ override */ int /* $$ func */ coresysuintsuint16s_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysuintsuint16s_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  //coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresysuintsuint16s