/** Module: "coresysuintsuint128s.h"
 ** Descr.: "Unsigned 128 Bits Integer Library."
 **/

// $$ namespace coresysuintsuint128s
// $$ {
 
// ------------------
 
#ifndef CORESYSUINTSUINT128S_H
#define CORESYSUINTSUINT128S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

/* functions */


bool /* $$ func */ coresysuintsuint128s_isempty
  (/* $$ in */ const uint128_t /* $$ param */ ASource);


// ------------------

/* procedures */


void /* $$ func */ coresysuintsuint128s_clear
  (/* $$ inout */ nonconst uint128_t /* $$ param */ ADest);

void /* $$ func */ coresysuintsuint128s_assign
  (/* $$ inout */ nonconst uint128_t* /* $$ param */ ADest,
   /* $$ in */    const    uint128_t  /* $$ param */ ASource);

bool /* $$ func */ coresysuintsuint128s_different
  (/* $$ in */ const uint128_t /* $$ param */ A,
   /* $$ in */ const uint128_t /* $$ param */ B);
   
// ------------------

/* operators */

enum comparison /* $$ func */ coresysuintsuint128s_compare
  (/* $$ in */ const uint128_t /* $$ param */ A,
   /* $$ in */ const uint128_t /* $$ param */ B);

bool /* $$ func */ coresysuintsuint128s_equal
  (/* $$ in */ const uint128_t /* $$ param */ A,
   /* $$ in */ const uint128_t /* $$ param */ B);

bool /* $$ func */ coresysuintsuint128s_different
  (/* $$ in */ const uint128_t /* $$ param */ A,
   /* $$ in */ const uint128_t /* $$ param */ B);
   
// ------------------

/* rtti */

bool /* $$ func */ coresysuintsuint128s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysuintsuint128s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresysuintsuint128s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysuintsuint128s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysuintsuint128s_finish
  ( noparams );

// ------------------

#endif // CORESYSUINTSUINTS128S_H

// $$ } // namespace coresysuintsuint128s