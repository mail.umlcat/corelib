/** Module: "coresysuintsuint24s.h"
 ** Descr.: "Unsigned 24 Bits Integer Library."
 **/

// $$ namespace coresysuintsuint24s
// $$ {
 
// ------------------
 
#ifndef CORESYSUINTSUINT24S_H
#define CORESYSUINTSUINT24S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"

// ------------------

/* functions */


bool /* $$ func */ coresysuintsuint24s_isempty
  (/* $$ in */ const uint24_t /* $$ param */ ASource);

// ------------------

/* procedures */


void /* $$ func */ coresysuintsuint24s_clear
  (/* $$ inout */ nonconst uint24_t /* $$ param */ ADest);


void /* $$ func */ coresysuintsuint24s_assign
  (/* $$ inout */ nonconst uint24_t* /* $$ param */ ADest,
   /* $$ in */    const    uint24_t  /* $$ param */ ASource);
   
// ------------------

/* operators */

enum comparison /* $$ func */ coresysuintsuint24s_compare
  (/* $$ in */ const uint24_t /* $$ param */ A,
   /* $$ in */ const uint24_t /* $$ param */ B);

bool /* $$ func */ coresysuintsuint24s_equal
  (/* $$ in */ const uint24_t /* $$ param */ A,
   /* $$ in */ const uint24_t /* $$ param */ B);

bool /* $$ func */ coresysuintsuint24s_different
  (/* $$ in */ const uint24_t /* $$ param */ A,
   /* $$ in */ const uint24_t /* $$ param */ B);
   
// ------------------

/* rtti */

bool /* $$ func */ coresysuintsuint24s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysuintsuint24s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresysuintsuint24s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysuintsuint24_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysuintsuint24_finish
  ( noparams );

// ------------------

#endif // CORESYSUINTSUINT24S_H

// $$ } // namespace coresysuintsuint24s