/** Module: "coresysio.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysio {
 
// ------------------
 
#ifndef CORESYSIO_H
#define CORESYSIO_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

// --> contained modules here
#include "coresysiofiles.h"
#include "coresysioansitxtfiles.h"
#include "coresysiobinfiles.h"
#include "coresysioansipaths.h"
// .include "coresysiobinfloat256s.h"
// .include "coresysiolongfloats.h"

// ------------------

/* rtti */

#define MOD_coresysio {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysio_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysio_finish
  ( noparams );

// ------------------

#endif // CORESYSIO_H

// ------------------

// $$ } // namespace coresysio