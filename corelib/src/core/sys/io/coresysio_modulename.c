/** Module: "coresys<modulename>s.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresys<modulename>s
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"

// ------------------

// --> same package libraries here
// .include "coresys<modulename>sbinfloat16s.h"
// .include "coresys<modulename>sbinfloat32s.h"
// .include "coresys<modulename>sbinfloat64s.h"
// .include "coresys<modulename>sbinfloat128s.h"
// .include "coresys<modulename>sbinfloat256s.h"
// .include "coresys<modulename>slongfloats.h"

// ------------------

// --> this same module header goes here
#include "coresys<modulename>s.h"
 
// ------------------

/* types */

// ------------------

/* functions */

// ------------------

/* procedures */

// ------------------

/* rtti */

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresys<modulename>s_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresys<modulename>s_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresys<modulename>s