/** Module: "coresysiobinfiles.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysiobinfiles
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"

// ------------------

// --> same package libraries here
#include "coresysiofiles.h"
// .include "coresysiobinfilesbinfloat32s.h"
// .include "coresysiobinfilesbinfloat64s.h"
// .include "coresysiobinfilesbinfloat128s.h"
// .include "coresysiobinfilesbinfloat256s.h"
// .include "coresysiobinfileslongfloats.h"

// ------------------

// --> this same module header goes here
#include "coresysiobinfiles.h"
 
// ------------------

/* functions */

int /* $$ func */ coresysiobinfiles_geterrorcode
  (coresysiofiles_binfile* /* param */ AFile)
{
  int /* $$ var */ Result = 0;
  // ---
     
  Result = 
	  ferror(AFile);

  // ---
  return Result;
} // func

// ---

bool /* $$ func */ coresysiobinfiles_iseof
  (coresysiofiles_binfile* /* param */ AFile)
{
  bool /* $$ var */ Result = false;
  // ---
  
  int /* $$ var */ ErrorCode = 0;
  // ---
     
  ErrorCode = 
	feof(AFile);
  Result =
    (ErrorCode != 0);

  // ---
  return Result;
} // func
 
// ---

coresysiofiles_binfile* /* $$ func */ coresysiobinfiles_openreadonly
  (const ansichar* /* param */ filename)
{
  coresysiofiles_binfile* /* $$ var */ Result = NULL;
  // ---
     
  Result = 
	  (coresysiofiles_binfile*) fopen((const char *) filename, "rb");

  // ---
  return Result;
} // func

coresysiofiles_binfile* /* $$ func */ coresysiobinfiles_openwriteonly
  (const ansichar* /* param */ filename)
{
  coresysiofiles_binfile* /* $$ var */ Result = NULL;
  // ---
     
  Result = 
	  (coresysiofiles_binfile*) fopen((const char *) filename, "wb");

  // ---
  return Result;
} // func


coresysiofiles_binfile* /* $$ func */ coresysiobinfiles_openappendonly
  (const ansichar* /* param */ filename)
{
  coresysiofiles_binfile* /* $$ var */ Result = NULL;
  // ---
     
  Result = 
  	(coresysiofiles_binfile*) fopen((const char *) filename, "ab");

  // ---
  return Result;
} // func

coresysiofiles_binfile* /* $$ func */ coresysiobinfiles_openreadupdate
  (const ansichar* /* param */ filename)
{
  coresysiofiles_binfile* /* $$ var */ Result = NULL;
  // ---
     
  Result = 
	  (coresysiofiles_binfile*) fopen((const char *) filename, "r+b");

  // ---
  return Result;
} // func

coresysiofiles_binfile* /* $$ func */ coresysiobinfiles_openwriteupdate
  (const ansichar* /* param */ filename)
{
  coresysiofiles_binfile* /* $$ var */ Result = NULL;
  // ---
     
  Result = 
  	(coresysiofiles_binfile*) fopen((const char *) filename, "w+b");

  // ---
  return Result;
} // func

coresysiofiles_binfile* /* $$ func */ coresysiobinfiles_openappendupdate
  (const ansichar* /* param */ filename)
{
  coresysiofiles_binfile* /* $$ var */ Result = NULL;
  // ---
     
  Result = 
  	(coresysiofiles_binfile*) fopen((const char *) filename, "a+b");

  // ---
  return Result;
} // func

coresysiofiles_binfile* /* $$ func */ coresysiobinfiles_open
  (const ansichar*         /* param */ filename,
   const enum fileopenmode /* param */ mode)
{
  coresysiofiles_binfile* /* $$ var */ Result = NULL;
  // ---
  
  switch (mode)
  {
	case fileopenmode_readonly:
	  Result = 
	    coresysiobinfiles_openreadonly(filename);
	break;
	
	case fileopenmode_writeonly:
	  Result = 
	    coresysiobinfiles_openwriteonly(filename);
	break;
	
	case fileopenmode_appendonly:
	  Result = 
	    coresysiobinfiles_openappendonly(filename);
	break;
	  
	case fileopenmode_readupdate:
	  Result = 
	    coresysiobinfiles_openreadupdate(filename);
	break;
	
	case fileopenmode_writeupdate:
	  Result = 
	    coresysiobinfiles_openwriteupdate(filename);
	break;
	
	case fileopenmode_appendupdate:
	  Result = 
	    coresysiobinfiles_openappendupdate(filename);
	break;

    default:
      coresysbasefuncs_nothing();
	  break;
  } // switch

  // ---
  return Result;
} // func

// ------------------

/* procedures */

void /* $$ func */ coresysiobinfiles_clearerrorcode
  (coresysiofiles_binfile* /* param */ AFile)
{
  clearerr(AFile);
} // func

// ---

void /* $$ func */ coresysiobinfiles_close
  (coresysiofiles_binfile* asref /* param */ AFile)
{
  fclose(asref AFile);
  AFile = NULL;
} // func

// ---
     
bool /* $$ func */ coresysiobinfiles_tryreadsize
  (coresysiofiles_binfile* /* param */ AFile,
   nonconst pointer*        /* param */ ABufferPtr,
   const    size_t          /* param */ ABufferSize)
{
  bool /* $$ var */ Result = false;
  // ---
     
  Result = 
	  ((ABufferPtr != NULL) && (ABufferSize > 0));
  if (Result)
  {
    size_t /* $$ var */ ReadCount = 0;

    ReadCount =
      fread((void*) ABufferPtr, ABufferSize, 1, AFile);
    Result =
      (ReadCount == ABufferSize);
  } // if

  // ---
  return Result;
} // func

// ---
 
void /* $$ func */ coresysiobinfiles_writesize
  (coresysiofiles_binfile* /* param */ AFile,
   const pointer*           /* param */ ABufferPtr,
   const size_t             /* param */ ABufferSize)
{
  bool /* $$ var */ CanContinue = false;
  // ---
     
  CanContinue = 
	((AFile != NULL) && (ABufferPtr != NULL) && (ABufferSize > 0));
  if (CanContinue)
  {
//    // @toxdo: verify buffer size using another buffer
//
    fwrite((void*) ABufferPtr, ABufferSize, 1, (FILE *) AFile);
  } // if
} // func


// ------------------

/* rtti */

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysiobinfiles_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysiobinfiles_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresysiobinfiles