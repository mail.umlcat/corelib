/** Module: "coresysiobinfiles.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysiobinfiles
// $$ {
 
// ------------------
 
#ifndef CORESYSIOBINFILES_H
#define CORESYSIOBINFILES_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"

// ------------------

// --> same package libraries here
#include "coresysiofiles.h"
// .include "coresysiobinfilesbinfloat32s.h"
// .include "coresysiobinfilesbinfloat64s.h"
// .include "coresysiobinfilesbinfloat128s.h"
// .include "coresysiobinfilesbinfloat256s.h"
// .include "coresysiobinfileslongfloats.h"

// ------------------

/* functions */

int /* $$ func */ coresysiobinfiles_geterrorcode
  (coresysiofiles_binfile* /* param */ AFile);

// ---

bool /* $$ func */ coresysiobinfiles_iseof
  (coresysiofiles_binfile* /* param */ AFile);
 
// ---

coresysiofiles_binfile* /* $$ func */ coresysiobinfiles_openreadonly
  (const ansichar* /* param */ filename);

coresysiofiles_binfile* /* $$ func */ coresysiobinfiles_openwriteonly
  (const ansichar* /* param */ filename);

coresysiofiles_binfile* /* $$ func */ coresysiobinfiles_openappendonly
  (const ansichar* /* param */ filename);

coresysiofiles_binfile* /* $$ func */ coresysiobinfiles_openreadupdate
  (const ansichar* /* param */ filename);

coresysiofiles_binfile* /* $$ func */ coresysiobinfiles_openwriteupdate
  (const ansichar* /* param */ filename);

coresysiofiles_binfile* /* $$ func */ coresysiobinfiles_openappendupdate
  (const ansichar* /* param */ filename);
  
coresysiofiles_binfile* /* $$ func */ coresysiobinfiles_open
  (const ansichar*         /* param */ filename,
   const enum fileopenmode /* param */ mode);
   
// ------------------

/* procedures */

void /* $$ func */ coresysiobinfiles_clearerrorcode
  (coresysiofiles_binfile* /* param */ AFile);

// ---

void /* $$ func */ coresysiobinfiles_close
  (coresysiofiles_binfile* asref /* param */ AFile);

// ---
     
bool /* $$ func */ coresysiobinfiles_tryreadsize
  (coresysiofiles_binfile* /* param */ AFile,
   nonconst pointer*        /* param */ ABufferPtr,
   const    size_t          /* param */ ABufferSize);

// ---

void /* $$ func */ coresysiobinfiles_writesize
  (coresysiofiles_binfile* /* param */ AFile,
   const pointer*           /* param */ ABufferPtr,
   const size_t             /* param */ ABufferSize);
   
// ------------------

/* rtti */

#define MOD_coresysiobinfiles
// $$ {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysiobinfiles_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysiobinfiles_finish
  ( noparams );

// ------------------

#endif // CORESYSIOBINFILES_H

// $$ } // namespace coresysiobinfiles