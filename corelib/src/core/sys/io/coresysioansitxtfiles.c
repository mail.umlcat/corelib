/** Module: "coresysioansitxtfiles.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysioansitxtfiles
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"

// ------------------

// --> same package libraries here
#include "coresysiofiles.h"
// .include "coresysioansitxtfilesbinfloat32s.h"
// .include "coresysioansitxtfilesbinfloat64s.h"
// .include "coresysioansitxtfilesbinfloat128s.h"
// .include "coresysioansitxtfilesbinfloat256s.h"
// .include "coresysioansitxtfileslongfloats.h"

// ------------------

// --> this same module header goes here
#include "coresysioansitxtfiles.h"
 
// ------------------

/* functions */

int /* $$ func */ coresysioansitxtfiles_geterrorcode
  (coresysiofiles_textfile* /* param */ AFile)
{
  int /* $$ var */ Result = 0;
  // ---
     
  Result = 
	  ferror((FILE*) AFile);

  // ---
  return Result;
} // func

// ---

bool /* $$ func */ coresysioansitxtfiles_iseof
  (coresysiofiles_textfile* /* param */ AFile)
{
  bool /* $$ var */ Result = false;
  // ---
  
  int /* $$ var */ ErrorCode = 0;
  // ---
     
  ErrorCode = 
	feof(AFile);
  Result =
    (ErrorCode != 0);

  // ---
  return Result;
} // func
 
// ---

coresysiofiles_textfile* /* $$ func */ coresysioansitxtfiles_openreadonly
  (const ansichar* /* param */ filename)
{
  coresysiofiles_textfile* /* $$ var */ Result = NULL;
  // ---
     
  Result = 
	  (coresysiofiles_textfile*) fopen((const char *) filename, "r");

  // ---
  return Result;
} // func

coresysiofiles_textfile* /* $$ func */ coresysioansitxtfiles_openwriteonly
  (const ansichar* /* param */ filename)
{
  coresysiofiles_textfile* /* $$ var */ Result = NULL;
  // ---
     
  Result = 
  	(coresysiofiles_textfile*) fopen((const char *) filename, "w");

  // ---
  return Result;
} // func

coresysiofiles_textfile* /* $$ func */ coresysioansitxtfiles_openappendonly
  (const ansichar* /* param */ filename)
{
  coresysiofiles_textfile* /* $$ var */ Result = NULL;
  // ---
     
  Result = 
  	(coresysiofiles_textfile*) fopen((const char *) filename, "a");

  // ---
  return Result;
} // func

coresysiofiles_textfile* /* $$ func */ coresysioansitxtfiles_openreadupdate
  (const ansichar* /* param */ filename)
{
  coresysiofiles_textfile* /* $$ var */ Result = NULL;
  // ---
     
  Result = 
	  (coresysiofiles_textfile*) fopen((const char *) filename, "r+");

  // ---
  return Result;
} // func

coresysiofiles_textfile* /* $$ func */ coresysioansitxtfiles_openwriteupdate
  (const ansichar* /* param */ filename)
{
  coresysiofiles_textfile* /* $$ var */ Result = NULL;
  // ---
     
  Result = 
  	(coresysiofiles_textfile*) fopen((const char *) filename, "w+");

  // ---
  return Result;
} // func

coresysiofiles_textfile* /* $$ func */ coresysioansitxtfiles_openappendupdate
  (const ansichar* /* param */ filename)
{
  coresysiofiles_textfile* /* $$ var */ Result = NULL;
  // ---
     
  Result = 
  	(coresysiofiles_textfile*) fopen((const char *) filename, "a+");

  // ---
  return Result;
} // func

coresysiofiles_textfile* /* $$ func */ coresysioansitxtfiles_open
  (const ansichar*         /* param */ filename,
   const enum fileopenmode /* param */ mode)
{
  coresysiofiles_textfile* /* $$ var */ Result = NULL;
  // ---
  
  switch (mode)
  {
	case fileopenmode_readonly:
	  Result = 
	    coresysioansitxtfiles_openreadonly(filename);
	break;
	
	case fileopenmode_writeonly:
	  Result = 
	    coresysioansitxtfiles_openwriteonly(filename);
	break;
	
	case fileopenmode_appendonly:
	  Result = 
	    coresysioansitxtfiles_openappendonly(filename);
	break;
	  
	case fileopenmode_readupdate:
	  Result = 
	    coresysioansitxtfiles_openreadupdate(filename);
	break;
	
	case fileopenmode_writeupdate:
	  Result = 
	    coresysioansitxtfiles_openwriteupdate(filename);
	break;
	
	case fileopenmode_appendupdate:
	  Result = 
	    coresysioansitxtfiles_openappendupdate(filename);
	break;

    default:
      coresysbasefuncs_nothing();
	  break;
  } // switch

  // ---
  return Result;
} // func

// ---

bool /* $$ func */ coresysioansitxtfiles_trygetchar
  (coresysiofiles_textfile* /* param */ AFile,
   nonconst ansichar*        /* param */ ADestBuffer)
{
  bool /* $$ var */ Result = false;
  // ---
  
  *ADestBuffer = ANSINULLCHAR;
  // ---
  
  int /* $$ var */ Code = 0;
  // ---
     
  Code = 
	fgetc(AFile);
  Result =
    (Code != EOF);
  if (Result)
  {
    *ADestBuffer =
	  (ansichar) Code;
  } // if

  // ---
  return Result;
} // func

ansichar /* $$ func */ coresysioansitxtfiles_getchar
  (coresysiofiles_textfile*/* param */ AFile)
{
  ansichar /* $$ var */ Result = ANSINULLCHAR;
  // ---
  
  int /* $$ var */ Code = 0;
  // ---
     
  Code = 
	fgetc(AFile);
  if (Code != EOF)
  {
    Result =
	  (ansichar) Code;
  } // if

  // ---
  return Result;
} // func

ansichar* /* $$ func */ coresysioansitxtfiles_getstr
  (coresysiofiles_textfile* /* param */ AFile,
   nonconst ansichar*        /* param */ ABufferData,
   const    size_t           /* param */ ABufferSize)
{
  ansichar* /* $$ var */ Result = NULL;
  // ---
	
  bool /* $$ var */ CanContinue = false;
  // ---
     
  CanContinue = 
	((AFile != NULL) && (ABufferData != NULL) && (ABufferSize > 1));
  if (CanContinue)
  {
  	Result = 
      fgets((char *) ABufferData, ABufferSize, (FILE *) AFile);
  } // if
  
  // ---
  return Result;
} // func

// ------------------

/* procedures */

void /* $$ func */ coresysioansitxtfiles_clearerrorcode
  (coresysiofiles_textfile* /* param */ AFile)
{
  clearerr(AFile);
} // func

// ---

void /* $$ func */ coresysioansitxtfiles_close
  (coresysiofiles_textfile** /* param */ AFile)
{
  fclose(asref AFile);
  AFile = NULL;
} // func

// ---

void /* $$ func */ coresysioansitxtfiles_stdputchar
  (const ansichar /* param */ AChar)
{
  putchar((int) AChar);
} // func

void /* $$ func */ coresysioansitxtfiles_stdputstr
  (const ansichar* /* param */ ABufferData)
{
  bool /* $$ var */ CanContinue = false;
  // ---
     
  CanContinue = 
	(ABufferData != NULL);
  if (CanContinue)
  {
    // @toxdo: verify buffer size using another buffer
	
    puts((char *) ABufferData);
  } // if
} // func

void /* $$ func */ coresysioansitxtfiles_stdputstrsize
  (const ansichar* /* param */ ABufferData,
   const size_t    /* param */ ABufferSize)
{
  bool /* $$ var */ CanContinue = false;
  // ---
     
  CanContinue = 
	((ABufferData != NULL) && (ABufferSize > 0));
  if (CanContinue)
  {
    size_t /* $$ var */ i = 0;

    ansichar* /* $$ var */ p = NULL;
    
    p = (ansichar*) ABufferData;
    while (CanContinue)
    {
      CanContinue =
        ((*p != ANSINULLCHAR) && (i < ABufferSize));
      if (CanContinue)
      {
        putchar((int) *p);
      }
      i++;
      p++;
    } // while (CanContinue)
  } // if
} // func

void /* $$ func */ coresysioansitxtfiles_stdputnewline
  ( noparams )
{
  bool /* $$ var */ CanContinue = false;
  // ---
  
  ansichar /* $$ var */ BufferData[16];
  // ---

  strcpy(BufferData, "\n");
  puts((char *) BufferData);
} // func

// ---

void /* $$ func */ coresysioansitxtfiles_putchar
  (coresysiofiles_textfile* /* param */ AFile,
   const ansichar            /* param */ AChar)
{
  bool /* $$ var */ CanContinue = false;
  // ---
     
  CanContinue = 
	(AFile != NULL);
  if (CanContinue)
  {
    fputc((int) AChar, (FILE *) AFile);
  } // if
} // func

void /* $$ func */ coresysioansitxtfiles_putstr
  (coresysiofiles_textfile* /* param */ AFile,
   const ansichar*           /* param */ ABufferData)
{
  bool /* $$ var */ CanContinue = false;
  // ---
     
  CanContinue = 
	((AFile != NULL) && (ABufferData != NULL));
  if (CanContinue)
  {
  	size_t /* $$ var */ ABufferSize = 1024;
    // @toxdo: verify buffer size using another buffer
	
    fputs((char *) ABufferData, (FILE *) AFile);
  } // if
} // func

void /* $$ func */ coresysioansitxtfiles_putstrsize
  (coresysiofiles_textfile* /* param */ AFile,
   const ansichar*           /* param */ ABufferData,
   const size_t              /* param */ ABufferSize)
{
  bool /* $$ var */ CanContinue = false;
  // ---
     
  CanContinue = 
	((AFile != NULL) && (ABufferData != NULL) && (ABufferSize > 0));
  if (CanContinue)
  {
    // @toxdo: verify buffer size using another buffer
	
    fputs((char *) ABufferData, (FILE *) AFile);
  } // if
} // func

void /* $$ func */ coresysioansitxtfiles_putnewline
  (coresysiofiles_textfile* /* param */ AFile)
{
  bool /* $$ var */ CanContinue = false;
  // ---
  
  ansichar /* $$ var */ BufferData[16];
  // ---
 
  CanContinue = 
	(AFile != NULL);
  if (CanContinue)
  {
    // @toxdo: verify buffer size using another buffer
	
    strcpy(BufferData, "\n");
    fputs((char *) BufferData, (FILE *) AFile);
  } // if
} // func
  
// ------------------

/* rtti */

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysioansitxtfiles_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysioansitxtfiles_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresysioansitxtfiles