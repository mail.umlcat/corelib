/** Module: "coresysioansipaths.*"
 ** Descr.:
 ** Contains several utility functions,
 ** to change paths, without accessing the filesystem.
 **/
 
// $$ namespace coresysioansipaths
// $$ {
 
// ------------------
 
#ifndef CORESYSIOANSIPATHS_H
#define CORESYSIOANSIPATHS_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

// --> same package libraries here
// .include "coresysioansipathsbinfloat16s.h"
// .include "coresysioansipathsbinfloat32s.h"
// .include "coresysioansipathsbinfloat64s.h"
// .include "coresysioansipathsbinfloat128s.h"
// .include "coresysioansipathsbinfloat256s.h"
// .include "coresysioansipathslongfloats.h"

// ------------------

/* types */

typedef
  void* coresysioansipaths_scanpath;

struct coresysioansipaths_scanpathheader
{
  ansichar* /* $$ var */ StartPtr;
  size_t    /* $$ var */ StartSize;
  
  ansichar* /* $$ var */ CurrentPtr;
  size_t    /* $$ var */ CurrentSize;
} ;

// ------------------

/* functions */

bool /* $$ func */ coresysioansipaths_trychangefileext
  (/* $$ out */ nonconst ansinullstring* asref /* param */ ADestFullFileName, 
   /* $$ in */  nonconst ansinullstring*       /* param */ ASourceFullFileName,
   /* $$ in */  const    ansinullstring*       /* param */ ANewFileExt);

bool /* $$ func */ coresysioansipaths_tryextractpath
  (/* $$ out */ nonconst ansinullstring* asref /* param */ ADestPath, 
   /* $$ in */  nonconst ansinullstring*       /* param */ ASourceFullFileName);

bool /* $$ func */ coresysioansipaths_tryextractname
  (/* $$ out */ nonconst ansinullstring* asref /* param */ ADestName, 
   /* $$ in */  nonconst ansinullstring*       /* param */ ASourceFullFileName);

bool /* $$ func */ coresysioansipaths_tryextractext
  (/* $$ out */ nonconst ansinullstring* asref /* param */ ADestExt, 
   /* $$ in */  nonconst ansinullstring*       /* param */ ASourceFullFileName);

bool /* $$ func */ coresysioansipaths_tryextractnameext
  (/* $$ out */ nonconst ansinullstring* asref /* param */ ADestNameExt, 
   /* $$ in */  nonconst ansinullstring*       /* param */ ASourceFullFileName);
  
   
// ------------------

bool /* $$ func */ coresysioansipaths_tryparsestartsize
  (/* inout */ nonconst coresysioansipaths_scanpath asref /* param */ AValue,
   /* $$ in */    nonconst ansinullstring*                    /* param */ ASourceFullPathBuffer,
   /* $$ in */    const    size_t                             /* param */ ASourceFullPathSize);

bool /* $$ func */ coresysioansipaths_tryparsenextsize
  (/* inout */ nonconst coresysioansipaths_scanpath asref /* param */ AValue,
   /* $$ out */   nonconst ansinullstring*                    /* param */ ADestBuffer,
   /* $$ out */   const    size_t                             /* param */ ADestSize);
 
bool /* $$ func */ coresysioansipaths_tryparsefinish
  (/* inout */ nonconst coresysioansipaths_scanpath asref /* param */ AValue);
  
// ------------------

/* procedures */

void /* $$ func */ coresysioansipaths_changeext
  (/* $$ out */ nonconst ansinullstring* asref /* param */ ADestFullFileName, 
   /* $$ in */  nonconst ansinullstring*       /* param */ ASourceFullFileName,
   /* $$ in */  const    ansinullstring*       /* param */ ANewFileExt);

void /* $$ func */ coresysioansipaths_extractpath
  (/* $$ out */ nonconst ansinullstring* asref /* param */ ADestPath, 
   /* $$ in */  nonconst ansinullstring*       /* param */ ASourceFullFileName);

void /* $$ func */ coresysioansipaths_extractname
  (/* $$ out */ nonconst ansinullstring* asref /* param */ ADestName, 
   /* $$ in */  nonconst ansinullstring*       /* param */ ASourceFullFileName);

void /* $$ func */ coresysioansipaths_extractext
  (/* $$ out */ nonconst ansinullstring* asref /* param */ ADestExt, 
   /* $$ in */  nonconst ansinullstring*       /* param */ ASourceFullFileName);   

void /* $$ func */ coresysioansipaths_extractnameext
  (/* $$ out */ nonconst ansinullstring* asref /* param */ ADestNameExt, 
   /* $$ in */  nonconst ansinullstring*       /* param */ ASourceFullFileName);   

// ------------------

//void /* $$ func */ coresysioansipaths_parsestart
//  (/* inout */ nonconst coresysioansipaths_scanpath asref /* param */ AValue,
//   /* $$ in */    nonconst ansinullstring*                           /* param */ ASourceFullPath);   

//void /* $$ func */ coresysioansipaths_parsestartsize
//  (/* inout */ nonconst coresysioansipaths_scanpath asref /* param */ AValue,
//   /* $$ in */    nonconst ansinullstring*                           /* param */ ASourceFullPath); 
   
//bool /* $$ func */ coresysioansipaths_parsenext
//  (/* inout */ nonconst coresysioansipaths_scanpath asref /* param */ AValue,
//   /* $$ out */   nonconst ansinullstring*                    /* param */ ADest);  

//void /* $$ func */ coresysioansipaths_parsefinish
//  (/* inout */ nonconst coresysioansipaths_scanpath asref /* param */ AValue);


// ------------------

/* rtti */

#define MOD_coresysioansipaths
// $$ {0x22,0x57,0x4C,0xD8,0xF4,0xAD,0x59,0x47,0x8A,0xDD,0x5F,0xE1,0x82,0x08,0x33,0xBC};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysioansipaths_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysioansipaths_finish
  ( noparams );

// ------------------

#endif // CORESYSIOANSIPATHS_H

// $$ } // namespace coresysioansipaths