/** Module: "coresysiofiles.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysiofiles
// $$ {
 
// ------------------
 
#ifndef CORESYSIOFILES_H
#define CORESYSIOFILES_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"

// ------------------

// --> same package libraries here
// .include "coresysiofilesbinfloat16s.h"
// .include "coresysiofilesbinfloat32s.h"
// .include "coresysiofilesbinfloat64s.h"
// .include "coresysiofilesbinfloat128s.h"
// .include "coresysiofilesbinfloat256s.h"
// .include "coresysiofileslongfloats.h"

// ------------------

/* types */

typedef
  FILE /* as */ coresysiofiles_textfile;

typedef
  FILE /* as */ coresysiofiles_binfile;

// ---

enum fileopenmode
{
  fileopenmode_undefined,    // = 0;
  
  fileopenmode_readonly,     // "r"
  fileopenmode_writeonly,    // "w"
  fileopenmode_appendonly,   // "a"
  
  fileopenmode_readupdate,   // "r+"
  fileopenmode_writeupdate,  // "w+"
  fileopenmode_appendupdate, // "a+"
  
} ;

// ------------------

/* functions */

// ------------------

/* procedures */

// ------------------

/* rtti */

#define MOD_coresysiofiles
// $$ {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysiofiles_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysiofiles_finish
  ( noparams );

// ------------------

#endif // CORESYSIOFILES_H

// $$ } // namespace coresysiofiles