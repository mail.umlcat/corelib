/** Module: "coresysioansipaths.*"
 ** Descr.:
 ** Contains several utility functions,
 ** to change paths, without accessing the filesystem.
 **/
 
// $$ namespace coresysioansipaths
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

// --> same package libraries here
// .include "coresysioansipathsbinfloat16s.h"
// .include "coresysioansipathsbinfloat32s.h"
// .include "coresysioansipathsbinfloat64s.h"
// .include "coresysioansipathsbinfloat128s.h"
// .include "coresysioansipathsbinfloat256s.h"
// .include "coresysioansipathslongfloats.h"

// ------------------

// --> this same module header goes here
#include "coresysioansipaths.h"
 
// ------------------

/* functions */

bool /* $$ func */ coresysioansipaths_trychangefileext
  (/* $$ out */ nonconst ansinullstring* asref /* param */ ADestFullFileName, 
   /* $$ in */  nonconst ansinullstring*       /* param */ ASourceFullFileName,
   /* $$ in */  const    ansinullstring*       /* param */ ANewFileExt)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result =
    ( true
       && (ADestFullFileName != NULL)
       && (! coresysstrsansinullstrs_isempty(ASourceFullFileName))
       && (! coresysstrsansinullstrs_isempty(ANewFileExt))
	 );
  if (Result)
  {
    // remove extra text from source file extension
	  ansichar  /* $$ var */ ThisNewFileExtBuffer[256];
  	size_t    /* $$ var */ ThisNewFileExtSize;
	
    ansichar* /* $$ var */ S = NULL;
    ansichar* /* $$ var */ D = NULL;
  
    ansichar* /* $$ var */ E = NULL;
    
    // ---

    struct ansicharsizedptr* /* $$ var */ FatS = NULL;
    struct ansicharsizedptr* /* $$ var */ FatD = NULL;
    
    // ---

    FatS =
      coresysstrsansicharfatptrs_new();
    FatD =
      coresysstrsansicharfatptrs_new();
    
    // ---
  
    // clear confirmed new extension
    D = ThisNewFileExtBuffer;
    
//    coresysbasemem_clear
//      ((pointer*) D, sizeof(ThisNewFileExt));
    memset
      ((void*) ThisNewFileExtBuffer, 0, sizeof(ThisNewFileExtBuffer));
      
      
      //((pointer*) ThisNewFileExt, sizeof(ThisNewFileExt));

    // check if any extension provided ?
    ThisNewFileExtSize = 0;
    if (! coresysstrsansinullstrs_isempty(ANewFileExt))
    {
      // just copy extension
      ThisNewFileExtSize = sizeof(ThisNewFileExtBuffer);
      
      coresysstrsansinullstrs_assignsize
         (ThisNewFileExtBuffer, ThisNewFileExtSize,
          ANewFileExt, coresysstrsansinullstrs_length(ANewFileExt));
    } // if isempty
    
    // ---

    coresysstrsansicharfatptrs_pack
      (intoref FatS, ASourceFullFileName, strlen(ASourceFullFileName) + 1);
    
    // ---

    // --> copy path before file extension separator
    Result =
      (coresysstrsansicharfatptrs_trylastptrofchar
        (intoref FatD, FatS, '.'));
    if (Result)
    {
      // has text before the '.' separator ?
      Result =
        FatD->ItemSize > 0;
      if (Result)
      {
      	// skip the '.' separator
        FatD->ItemPtr =
          FatD->ItemPtr++;
        FatD->ItemSize =
          FatD->ItemSize++;
          
        D =
          *ADestFullFileName;
        S = 
          ASourceFullFileName;
          
        E =
          *ADestFullFileName;
          
        for (int /* $$ var */ i = FatD->ItemSize; i > 0; i--)
        {
          *D = *S;
          D++;
          S++;
		} // for
		
		// ...

        // copy source file extension without prefix or separator
	    S = /* & */ ThisNewFileExtBuffer;
	    while (*S != ANSINULLCHAR)
	    {
          *D = *S;
          S++;
          D++;
        } // 
	  
		// add marker
        coresysstrsansinullstrs_writenullmarker
         (D);
      } // CanContinue
    } // CanContinue

    // ---
	
    coresysstrsansicharfatptrs_drop(intoref FatD);
    coresysstrsansicharfatptrs_drop(intoref FatS);	
  } // if
	
  // ---
  return Result;	 
} // func

bool /* $$ func */ coresysioansipaths_tryextractpath
  (/* $$ out */ nonconst ansinullstring* asref /* param */ ADestPath, 
   /* $$ in */  nonconst ansinullstring*       /* param */ ASourceFullFileName)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result =
    ( true
       && (ADestPath != NULL)
       && (! coresysstrsansinullstrs_isempty(ASourceFullFileName))
	 );
  if (Result)
  {
    
	
  } // if
	
  // ---
  return Result;	 
} // func 

bool /* $$ func */ coresysioansipaths_tryextractname
  (/* $$ out */ nonconst ansinullstring* asref /* param */ ADestName, 
   /* $$ in */  nonconst ansinullstring*       /* param */ ASourceFullFileName)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result =
    ( true
       && (ADestName != NULL)
       && (! coresysstrsansinullstrs_isempty(ASourceFullFileName))
	 );
  if (Result)
  {
    
	
  } // if
	
  // ---
  return Result;	 
} // func 

bool /* $$ func */ coresysioansipaths_tryextractext
  (/* $$ out */ nonconst ansinullstring* asref /* param */ ADestExt, 
   /* $$ in */  nonconst ansinullstring*       /* param */ ASourceFullFileName)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result =
    ( true
       && (ADestExt != NULL)
       && (! coresysstrsansinullstrs_isempty(ASourceFullFileName))
	 );
  if (Result)
  {
    

  } // if
	
  // ---
  return Result;	 
} // func 

bool /* $$ func */ coresysioansipaths_tryextractextname
  (/* $$ out */ nonconst ansinullstring* asref /* param */ ADestNameExt, 
   /* $$ in */  nonconst ansinullstring*       /* param */ ASourceFullFileName)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result =
    ( true
       && (ADestNameExt != NULL)
       && (! coresysstrsansinullstrs_isempty(ASourceFullFileName))
	 );
  if (Result)
  {
    
	
  } // if
	
  // ---
  return Result;	 
} // func 

   
// ------------------

bool /* $$ func */ coresysioansipaths_tryparsestartsize
  (/* inout */ nonconst coresysioansipaths_scanpath asref /* param */ AValue,
   /* $$ in */    nonconst ansinullstring*                    /* param */ ASourceFullPathBuffer,
   /* $$ in */    const    size_t                             /* param */ ASourceFullPathSize)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result =
    ( true
       && (AValue != NULL)
       && (! coresysstrsansinullstrs_isempty(ASourceFullPathBuffer))
       && (ASourceFullPathSize > 0)
	 );
  if (Result)
  {
    struct coresysioansipaths_scanpathheader* /* $$ var */ ThisScanPath = NULL;
    
    // ---
    
    ThisScanPath =
      (struct coresysioansipaths_scanpathheader*) coresysbasemem_allocateclear
        (sizeof(struct coresysioansipaths_scanpathheader));
      
    *AValue = (coresysioansipaths_scanpath *) ThisScanPath;
      
    // ---
      
	  ThisScanPath->StartPtr =
      coresysstrsansinullstrs_compactcopysize
        (ASourceFullPathBuffer, ASourceFullPathSize);
	  ThisScanPath->StartSize =
      coresysstrsansinullstrs_lengthsize
        (ASourceFullPathBuffer, ASourceFullPathSize);
        
	  ThisScanPath->CurrentPtr =
      ThisScanPath->StartPtr;
	  ThisScanPath->CurrentSize =
      ThisScanPath->StartSize;
  } // if
	
  // ---
  return Result;	 
} // func 

bool /* $$ func */ coresysioansipaths_tryparsenextsize
  (/* inout */ nonconst coresysioansipaths_scanpath asref /* param */ AValue,
   /* $$ out */   nonconst ansinullstring*                    /* param */ ADestBuffer,
   /* $$ out */   const    size_t                             /* param */ ADestSize)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result =
    ( true
       && (AValue != NULL)
       && (ADestBuffer != NULL)
       && (ADestSize > 0)
	 );
  if (Result)
  {
    struct coresysioansipaths_scanpathheader* /* $$ var */ ThisScanPath = NULL;
    
    // ---
    
    ThisScanPath =
      (struct coresysioansipaths_scanpathheader*) asref AValue;
      
    // ---

    struct ansicharsizedptr* /* $$ var */ FatS = NULL;
    struct ansicharsizedptr* /* $$ var */ FatD = NULL;
    
    // ---

    bool /* $$ var */ CanContinue;
    bool /* $$ var */ Found;
    
    size_t   /* $$ var */ MinSize;
    ansichar /* $$ var */ Separator;
    
    // ---

    Separator = '\\';
    
    // ---

    FatS =
      coresysstrsansicharfatptrs_new();
    FatD =
      coresysstrsansicharfatptrs_new();
    
    // ---
  
    coresysstrsansicharfatptrs_pack
      (intoref FatS, ThisScanPath->CurrentPtr, 0);

    CanContinue = true; 
    Found = false;
    
    while (CanContinue)
    {
      CanContinue =
        (*(FatS->ItemPtr) != ANSINULLCHAR) &&
        (*(FatS->ItemPtr) != Separator);
      
      if (CanContinue)
      {
        Found = CanContinue;
        
        FatS->ItemPtr++;
        FatS->ItemSize++;
      } // if
    } // while

    if (Found)
    {
      MinSize =
        (ADestSize > FatS->ItemSize ? FatS->ItemSize : ADestSize);
      
//      coresysstrsansinullstrs_assignsize
//        (ADestBuffer, MinSize, ThisScanPath->CurrentPtr, MinSize);
      
      coresysstrsansinullstrs_assignleftsize
        (ADestBuffer, ADestSize, ThisScanPath->CurrentPtr, ThisScanPath->CurrentSize, MinSize);
        
      ThisScanPath->CurrentPtr =
        (FatS->ItemPtr + 1);
      ThisScanPath->CurrentSize =
        (ThisScanPath->CurrentSize - MinSize + 1);
    } // if
    
    Result =
      Found;

    // ---
	
    coresysstrsansicharfatptrs_drop(intoref FatD);
    coresysstrsansicharfatptrs_drop(intoref FatS);
  } // if
	
  // ---
  return Result;	 
} // func 

bool /* $$ func */ coresysioansipaths_tryparsefinish
  (/* inout */ nonconst coresysioansipaths_scanpath asref /* param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
 
  Result =
    ( true
       && (AValue != NULL)
	 );
  if (Result)
  {
    struct coresysioansipaths_scanpathheader* /* $$ var */ ThisScanPath = NULL;
    
    // ---
    
    ThisScanPath =
      (struct coresysioansipaths_scanpathheader*) AValue;
      
    // ---
     
    
    coresysstrsansinullstrs_dropstrclear
      ((ansinullstring**) intoref (ThisScanPath->StartPtr), ThisScanPath->StartSize);
    ThisScanPath->CurrentPtr = NULL;
    
    coresysbasemem_deallocateclear
      ((pointer**) ThisScanPath, sizeof(struct coresysioansipaths_scanpathheader));
    AValue = NULL;
  } // if
	
  // ---
  return Result; 
} // func 


// ------------------


// ------------------

/* procedures */

void /* $$ func */ coresysioansipaths_changefileext
  (/* $$ out */ nonconst ansinullstring* asref /* param */ ADestFullFileName, 
   /* $$ in */  nonconst ansinullstring*       /* param */ ASourceFullFileName,
   /* $$ in */  const    ansinullstring*       /* param */ ANewFileExt)
{
  bool /* $$ var */ CanContinue = false;
  // ---
 
  CanContinue =
    (coresysioansipaths_trychangefileext(ADestFullFileName, ASourceFullFileName, ANewFileExt));
  if (! CanContinue)
  {
    // raise error
  } // if
} // func

void /* $$ func */ coresysioansipaths_extractpath
  (/* $$ out */ nonconst ansinullstring* asref /* param */ ADestPath, 
   /* $$ in */  nonconst ansinullstring*       /* param */ ASourceFullFileName)
{
  bool /* $$ var */ CanContinue = false;
  // ---
  
  CanContinue =
    (coresysioansipaths_tryextractpath(ADestPath, ASourceFullFileName));
  if (! CanContinue)
  {
    // raise error
  } // if
} // func

void /* $$ func */ coresysioansipaths_extractname
  (/* $$ out */ nonconst ansinullstring* asref /* param */ ADestName, 
   /* $$ in */  nonconst ansinullstring*       /* param */ ASourceFullFileName)
{
  bool /* $$ var */ CanContinue = false;
  // ---
    
  CanContinue =
    (coresysioansipaths_tryextractname(ADestName, ASourceFullFileName));
  if (! CanContinue)
  {
    // raise error
  } // if
} // func

void /* $$ func */ coresysioansipaths_extractext
  (/* $$ out */ nonconst ansinullstring* asref /* param */ ADestExt, 
   /* $$ in */  nonconst ansinullstring*       /* param */ ASourceFullFileName)
{
  bool /* $$ var */ CanContinue = false;
  // ---
    
  CanContinue =
    (coresysioansipaths_tryextractext(ADestExt, ASourceFullFileName));
  if (! CanContinue)
  {
    // raise error
  } // if
} // func

void /* $$ func */ coresysioansipaths_extractnameext
  (/* $$ out */ nonconst ansinullstring* asref /* param */ ADestNameExt, 
   /* $$ in */  nonconst ansinullstring*       /* param */ ASourceFullFileName)
{
  bool /* $$ var */ CanContinue = false;
  // ---
    
  CanContinue =
    (coresysioansipaths_tryextractextname(ADestNameExt, ASourceFullFileName));
  if (! CanContinue)
  {
    // raise error
  } // if
} // func



// ------------------

/* rtti */

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysioansipaths_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysioansipaths_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresysioansipaths