/** Module: "coresysioansitxtfiles.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysioansitxtfiles
// $$ {
 
// ------------------
 
#ifndef CORESYSIOANSITXTFILES_H
#define CORESYSIOANSITXTFILES_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"

// ------------------

// --> same package libraries here
#include "coresysiofiles.h"
// .include "coresysioansitxtfilesbinfloat32s.h"
// .include "coresysioansitxtfilesbinfloat64s.h"
// .include "coresysioansitxtfilesbinfloat128s.h"
// .include "coresysioansitxtfilesbinfloat256s.h"
// .include "coresysioansitxtfileslongfloats.h"

// ------------------

/* functions */

int /* $$ func */ coresysioansitxtfiles_geterrorcode
  (coresysiofiles_textfile* /* param */ AFile);

// ---

bool /* $$ func */ coresysioansitxtfiles_iseof
  (coresysiofiles_textfile* /* param */ AFile);
 
// ---

coresysiofiles_textfile* /* $$ func */ coresysioansitxtfiles_openreadonly
  (const ansichar* /* param */ filename);

coresysiofiles_textfile* /* $$ func */ coresysioansitxtfiles_openwriteonly
  (const ansichar* /* param */ filename);

coresysiofiles_textfile* /* $$ func */ coresysioansitxtfiles_openappendonly
  (const ansichar* /* param */ filename);

coresysiofiles_textfile* /* $$ func */ coresysioansitxtfiles_openreadupdate
  (const ansichar* /* param */ filename);

coresysiofiles_textfile* /* $$ func */ coresysioansitxtfiles_openwriteupdate
  (const ansichar* /* param */ filename);

coresysiofiles_textfile* /* $$ func */ coresysioansitxtfiles_openappendupdate
  (const ansichar* /* param */ filename);
  
coresysiofiles_textfile* /* $$ func */ coresysioansitxtfiles_open
  (const ansichar*         /* param */ filename,
   const enum fileopenmode /* param */ mode);

// ---

bool /* $$ func */ coresysioansitxtfiles_trygetchar
  (coresysiofiles_textfile* /* param */ AFile,
   nonconst ansichar*        /* param */ ADestBuffer);
  
ansichar /* $$ func */ coresysioansitxtfiles_getchar
  (coresysiofiles_textfile* /* param */ AFile);
     
ansichar* /* $$ func */ coresysioansitxtfiles_getstr
  (coresysiofiles_textfile* /* param */ AFile,
   nonconst ansichar*        /* param */ ABufferData,
   const    size_t           /* param */ ABufferSize);
  
// ------------------

/* procedures */

void /* $$ func */ coresysioansitxtfiles_clearerrorcode
  (coresysiofiles_textfile* /* param */ AFile);

// ---

void /* $$ func */ coresysioansitxtfiles_close
  (coresysiofiles_textfile** /* param */ AFile);

// ---

void /* $$ func */ coresysioansitxtfiles_stdputchar
  (const ansichar /* param */ AChar);

void /* $$ func */ coresysioansitxtfiles_stdputstr
  (const ansichar* /* param */ ABufferData);

void /* $$ func */ coresysioansitxtfiles_stdputstrsize
  (const ansichar* /* param */ ABufferData,
   const size_t    /* param */ ABufferSize);
   
void /* $$ func */ coresysioansitxtfiles_stdputnewline
  ( noparams );

// ---

void /* $$ func */ coresysioansitxtfiles_putchar
  (coresysiofiles_textfile* /* param */ AFile,
   const ansichar            /* param */ AChar);
   
void /* $$ func */ coresysioansitxtfiles_putstr
  (coresysiofiles_textfile* /* param */ AFile,
   const ansichar*           /* param */ ABufferData);
   
void /* $$ func */ coresysioansitxtfiles_putstrsize
  (coresysiofiles_textfile* /* param */ AFile,
   const ansichar*           /* param */ ABufferData,
   const size_t              /* param */ ABufferSize);
   
void /* $$ func */ coresysioansitxtfiles_putnewline
  (coresysiofiles_textfile* /* param */ AFile);  
  
// ------------------

/* rtti */

#define MOD_coresysioansitxtfiles
// $$ {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysioansitxtfiles_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysioansitxtfiles_finish
  ( noparams );

// ------------------

#endif // CORESYSIOANSITXTFILES_H

// $$ } // namespace coresysioansitxtfiles