/** Module: "coresys<modulename>s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresys<modulename>s
// $$ {
 
// ------------------
 
#ifndef CORESYS<MODULENAME>S_H
#define CORESYS<MODULENAME>S_H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"

// ------------------

// --> same package libraries here
// .include "coresys<modulename>sbinfloat16s.h"
// .include "coresys<modulename>sbinfloat32s.h"
// .include "coresys<modulename>sbinfloat64s.h"
// .include "coresys<modulename>sbinfloat128s.h"
// .include "coresys<modulename>sbinfloat256s.h"
// .include "coresys<modulename>slongfloats.h"

// ------------------

/* types */

// ------------------

/* functions */

// ------------------

/* procedures */

// ------------------

/* rtti */

#define MOD_coresys<modulename>s
// $$ {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresys<modulename>s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresys<modulename>s_finish
  ( noparams );

// ------------------

#endif // CORESYS<MODULENAME>S_H

// ------------------

// $$ } // namespace coresys<modulename>s