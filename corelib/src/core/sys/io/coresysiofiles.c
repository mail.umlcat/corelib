/** Module: "coresysiofiles.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysiofiles
// $$ {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"

// ------------------

// --> same package libraries here
// .include "coresysiofilesbinfloat16s.h"
// .include "coresysiofilesbinfloat32s.h"
// .include "coresysiofilesbinfloat64s.h"
// .include "coresysiofilesbinfloat128s.h"
// .include "coresysiofilesbinfloat256s.h"
// .include "coresysiofileslongfloats.h"

// ------------------

// --> this same module header goes here
#include "coresysiofiles.h"
 
// ------------------

/* types */

// ------------------

/* functions */

// ------------------

/* procedures */

// ------------------

/* rtti */

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysiofiles_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysiofiles_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresysiofiles