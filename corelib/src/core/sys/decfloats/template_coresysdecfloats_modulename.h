/** Module: "coresysdecfloats<modulename>s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysdecfloats<modulename>s
// $$ {
 
// ------------------
 
#ifndef CORESYSDECFLOATS<MODULENAME>S_H
#define CORESYSDECFLOATS<MODULENAME>S_H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

 
// ------------------

/* functions */

bool /* $$ func */ coresysdecfloats<modulename>s_isempty
  (/* $$ in */ const <typename>_t /* $$ param */ ASource);
  
// ------------------

/* procedures */


void /* $$ func */ coresysdecfloats<modulename>s_clear
  (/* $$ inout */ nonconst <typename>_t* /* $$ param */ ADest);
  
void /* $$ func */ coresysdecfloats<modulename>s_assign
  (/* $$ inout */ nonconst <typename>_t* /* $$ param */ ADest,
   /* $$ in */    const    <typename>_t  /* $$ param */ ASource);
   
// ------------------

/* operators */


enum comparison /* $$ func */ coresysdecfloats<modulename>s_compare
  (/* $$ in */ const <typename>_t /* $$ param */ A,
   /* $$ in */ const <typename>_t /* $$ param */ B);

bool /* $$ func */ coresysdecfloats<modulename>s_equal
  (/* $$ in */ const <typename>_t /* $$ param */ A,
   /* $$ in */ const <typename>_t /* $$ param */ B);

bool /* $$ func */ coresysdecfloats<modulename>s_different
  (/* $$ in */ const <typename>_t /* $$ param */ A,
   /* $$ in */ const <typename>_t /* $$ param */ B);
   
// ------------------

/* rtti */

#define MOD_coresysdecfloats<modulename>s
// $$ {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

bool /* $$ func */ coresysdecfloats<modulename>s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysdecfloats<modulename>s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresysdecfloats<modulename>s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysdecfloats<modulename>s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysdecfloats<modulename>s_finish
  ( noparams );

// ------------------

#endif // CORESYSDECFLOATS<MODULENAME>S_H

// ------------------

// $$ } // namespace coresysdecfloats<modulename>s
