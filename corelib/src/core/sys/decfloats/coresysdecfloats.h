/** Module: "coresysdecfloats.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysdecfloats
// $$ {
 
// ------------------
 
#ifndef CORESYSDECFLOATS_H
#define CORESYSDECFLOATS_H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

// --> contained libraries here
#include "coresysdecfloatsdf32s.h"
#include "coresysdecfloatsdf64s.h"
#include "coresysdecfloatsdf128s.h"

// ------------------


// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysdecfloats_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysdecfloats_finish
  ( noparams );

// ------------------

#endif // CORESYSDECFLOATS_H

// ------------------

// $$ } // namespace coresysdecfloats