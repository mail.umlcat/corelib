/** Module: "coresysdecfloatsdf128s.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysdecfloatsdf128s
// $$ {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

#include "coresysdecfloatsdf128s.h"
 
// ------------------

/* functions */

bool /* $$ func */ coresysdecfloatsdf128s_isempty
  (/* $$ in */ const decfloat128_t /* $$ param */ ASource);
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* procedures */


void /* $$ func */ coresysdecfloatsdf128s_clear
  (/* $$ inout */ nonconst decfloat128_t* /* $$ param */ ADest);
{
  coresysbasefuncs_nothing();
} // func
  
void /* $$ func */ coresysdecfloatsdf128s_assign
  (/* $$ inout */ nonconst decfloat128_t* /* $$ param */ ADest,
   /* $$ in */    const    decfloat128_t  /* $$ param */ ASource);
{
  coresysbasefuncs_nothing();
} // func
  
// ------------------

/* operators */


enum comparison /* $$ func */ coresysdecfloatsdf128s_compare
  (/* $$ in */ const decfloat128_t /* $$ param */ A,
   /* $$ in */ const decfloat128_t /* $$ param */ B);
{
  enum comparison /* $$ var */ Result = comparison_equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysdecfloatsdf128s_equal
  (/* $$ in */ const decfloat128_t /* $$ param */ A,
   /* $$ in */ const decfloat128_t /* $$ param */ B);
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysdecfloatsdf128s_different
  (/* $$ in */ const decfloat128_t /* $$ param */ A,
   /* $$ in */ const decfloat128_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* rtti */
 
bool /* $$ func */ coresysdecfloatsdf128s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysdecfloatsdf128s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* $$ func */ coresysdecfloatsdf128s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue)
{
  coresysbasefuncs_nothing();
} // func

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysdecfloatsdf128s_start
  ( noparams )
{
  coresysbasefuncs_nothing();
} // func

/* $$ override */ int /* $$ func */ coresysdecfloatsdf128s_finish
  ( noparams )
{
  coresysbasefuncs_nothing();
} // func

// ------------------


 // ...

// $$ } // namespace coresysdecfloatsdf128s