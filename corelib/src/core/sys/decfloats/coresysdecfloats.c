/** Module: "coresysdecfloats.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysdecfloats
// $$ {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

// --> contained libraries here
#include "coresysdecfloatsdf32s.h"
#include "coresysdecfloatsdf64s.h"
#include "coresysdecfloatsdf128s.h"

// ------------------

// --> this same module header goes here
#include "coresysdecfloats.h"
 
// ------------------


// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysdecfloats_start
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

/* $$ override */ int /* $$ func */ coresysdecfloats_finish
  ( noparams )
{
  int /* $$ var */ Result = 0;
  // ---
     
  coresysbasefuncs_nothing();

  // ---
  return Result;
} // func

// ------------------

// $$ } // namespace coresysdecfloats