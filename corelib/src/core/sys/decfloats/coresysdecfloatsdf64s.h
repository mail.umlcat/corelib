/** Module: "coresysdecfloatsdf64s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysdecfloatsdf64s
// $$ {
 
// ------------------
 
#ifndef CORESYSDECFLOATSDF64S_H
#define CORESYSDECFLOATSDF64S_H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

 
// ------------------

/* functions */

bool /* func*/ coresysdecfloatsdf64s_isempty
  (/* $$ in */ const decfloat64_t /* $$ param */ ASource);
  
// ------------------

/* procedures */


void /* $$ func */ coresysdecfloatsdf64s_clear
  (/* $$ inout */ nonconst decfloat64_t* /* $$ param */ ADest);
  
void /* $$ func */ coresysdecfloatsdf64s_assign
  (/* $$ inout */ nonconst decfloat64_t* /* $$ param */ ADest,
   /* $$ in */    const    decfloat64_t  /* $$ param */ ASource);
   
// ------------------

/* operators */


enum comparison /* $$ func */ coresysdecfloatsdf64s_compare
  (/* $$ in */ const decfloat64_t /* $$ param */ A,
   /* $$ in */ const decfloat64_t /* $$ param */ B);

bool /* $$ func */ coresysdecfloatsdf64s_equal
  (/* $$ in */ const decfloat64_t /* $$ param */ A,
   /* $$ in */ const decfloat64_t /* $$ param */ B);

bool /* $$ func */ coresysdecfloatsdf64s_different
  (/* $$ in */ const decfloat64_t /* $$ param */ A,
   /* $$ in */ const decfloat64_t /* $$ param */ B);
   
// ------------------

/* rtti */

bool /* $$ func */ coresysdecfloatsdf64s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysdecfloatsdf64s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresysdecfloatsdf64s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysdecfloatsdf64s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysdecfloatsdf64s_finish
  ( noparams );

// ------------------

#endif // CORESYSDECFLOATSDF64S_H

// $$ } // namespace coresysdecfloatsdf64s