/** Module: "coresysdecfloats<modulename>s.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysdecfloats<modulename>s
// $$ {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

#include "coresysdecfloats<modulename>s.h"
 
// ------------------

/* functions */

bool /* func*/ coresysdecfloats<modulename>s_isempty
  (/* $$ in */ const <typename>_t /* $$ param */ ASource);
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* procedures */


void /* $$ func */ coresysdecfloats<modulename>s_clear
  (/* $$ inout */ nonconst <typename>_t* /* $$ param */ ADest);
{
  coresysbasefuncs_nothing();
} // func
  
void /* $$ func */ coresysdecfloats<modulename>s_assign
  (/* $$ inout */ nonconst <typename>_t* /* $$ param */ ADest,
   /* $$ in */    const    <typename>_t  /* $$ param */ ASource);
{
  coresysbasefuncs_nothing();
} // func
  
// ------------------

/* operators */


enum comparison /* $$ func */ coresysdecfloats<modulename>s_compare
  (/* $$ in */ const <typename>_t /* $$ param */ A,
   /* $$ in */ const <typename>_t /* $$ param */ B);
{
  enum comparison /* $$ var */ Result = comparison_equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysdecfloats<modulename>s_equal
  (/* $$ in */ const <typename>_t /* $$ param */ A,
   /* $$ in */ const <typename>_t /* $$ param */ B);
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysdecfloats<modulename>s_different
  (/* $$ in */ const <typename>_t /* $$ param */ A,
   /* $$ in */ const <typename>_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* rtti */
 
bool /* $$ func */ coresysdecfloats<modulename>s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysdecfloats<modulename>s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* $$ func */ coresysdecfloats<modulename>s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue)
{
  coresysbasefuncs_nothing();
} // func

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysdecfloats<modulename>s_start
  ( noparams )
{
  coresysbasefuncs_nothing();
} // func

/* $$ override */ int /* $$ func */ coresysdecfloats<modulename>s_finish
  ( noparams )
{
  coresysbasefuncs_nothing();
} // func

// ------------------

// $$ } // namespace coresysdecfloats<modulename>s