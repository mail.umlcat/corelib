/** Module: "coresysdecfloatsdf128s.h"
 ** Descr.: "..."
 **/
 
// $$ namespace coresysdecfloatsdf128s
// $$ {
 
// ------------------
 
#ifndef CORESYSDECFLOATSDF128S_H
#define CORESYSDECFLOATSDF128S_H
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

 
// ------------------

/* functions */

bool /* func*/ coresysdecfloatsdf128s_isempty
  (/* $$ in */ const decfloat128_t /* $$ param */ ASource);
  
// ------------------

/* procedures */


void /* $$ func */ coresysdecfloatsdf128s_clear
  (/* $$ inout */ nonconst decfloat128_t* /* $$ param */ ADest);
  
void /* $$ func */ coresysdecfloatsdf128s_assign
  (/* $$ inout */ nonconst decfloat128_t* /* $$ param */ ADest,
   /* $$ in */    const    decfloat128_t  /* $$ param */ ASource);
   
// ------------------

/* operators */


enum comparison /* $$ func */ coresysdecfloatsdf128s_compare
  (/* $$ in */ const decfloat128_t /* $$ param */ A,
   /* $$ in */ const decfloat128_t /* $$ param */ B);

bool /* $$ func */ coresysdecfloatsdf128s_equal
  (/* $$ in */ const decfloat128_t /* $$ param */ A,
   /* $$ in */ const decfloat128_t /* $$ param */ B);

bool /* $$ func */ coresysdecfloatsdf128s_different
  (/* $$ in */ const decfloat128_t /* $$ param */ A,
   /* $$ in */ const decfloat128_t /* $$ param */ B);
   
// ------------------

/* rtti */

bool /* $$ func */ coresysdecfloatsdf128s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysdecfloatsdf128s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresysdecfloatsdf128s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

// ------------------


 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysdecfloatsdf128s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysdecfloatsdf128s_finish
  ( noparams );

// ------------------

#endif // CORESYSDECFLOATSDF128S_H

// $$ } // namespace coresysdecfloatsdf128s