/** Module: "coresysdecfloatsdf32s.h"
 ** Descr.: "..."
 **/

// $$ namespace coresysdecfloatsdf32s
// $$ {

// ------------------

#ifndef CORESYSDECFLOATSDF32S_H
#define CORESYSDECFLOATSDF32S_H

// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"


// ------------------

/* functions */

bool /* $$ func */ coresysdecfloatsdf32s_isempty
  (/* $$ in */ const decfloat32_t /* $$ param */ ASource);

// ------------------

/* procedures */


void /* $$ func */ coresysdecfloatsdf32s_clear
  (/* $$ inout */ nonconst decfloat32_t* /* $$ param */ ADest);

void /* $$ func */ coresysdecfloatsdf32s_assign
  (/* $$ inout */ nonconst decfloat32_t* /* $$ param */ ADest,
   /* $$ in */    const    decfloat32_t  /* $$ param */ ASource);

// ------------------

/* operators */


enum comparison /* $$ func */ coresysdecfloatsdf32s_compare
  (/* $$ in */ const decfloat32_t /* $$ param */ A,
   /* $$ in */ const decfloat32_t /* $$ param */ B);

bool /* $$ func */ coresysdecfloatsdf32s_equal
  (/* $$ in */ const decfloat32_t /* $$ param */ A,
   /* $$ in */ const decfloat32_t /* $$ param */ B);

bool /* $$ func */ coresysdecfloatsdf32s_different
  (/* $$ in */ const decfloat32_t /* $$ param */ A,
   /* $$ in */ const decfloat32_t /* $$ param */ B);

// ------------------

/* rtti */

bool /* $$ func */ coresysdecfloatsdf32s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue);

bool /* $$ func */ coresysdecfloatsdf32s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue);

void /* $$ func */ coresysdecfloatsdf32s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue);

// ------------------


 // ...

// ------------------

/* $$ override */ int /* $$ func */ coresysdecfloatsdf32s_start
  ( noparams );

/* $$ override */ int /* $$ func */ coresysdecfloatsdf32s_finish
  ( noparams );

// ------------------

#endif // CORESYSDECFLOATSDF32S_H

// $$ } // namespace coresysdecfloatsdf32s
