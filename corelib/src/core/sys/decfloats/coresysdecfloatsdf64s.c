/** Module: "coresysdecfloatsdf64s.c"
 ** Descr.: "predefined library"
 **/
 
// $$ namespace coresysdecfloatsdf64s
// $$ {
 
// ------------------

#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

#include "coresysdecfloatsdf64s.h"
 
// ------------------

/* functions */

bool /* $$ func */ coresysdecfloatsdf64s_isempty
  (/* $$ in */ const decfloat64_t /* $$ param */ ASource);
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* procedures */


void /* $$ func */ coresysdecfloatsdf64s_clear
  (/* $$ inout */ nonconst decfloat64_t* /* $$ param */ ADest);
{
  coresysbasefuncs_nothing();
} // func
  
void /* $$ func */ coresysdecfloatsdf64s_assign
  (/* $$ inout */ nonconst decfloat64_t* /* $$ param */ ADest,
   /* $$ in */    const    decfloat64_t  /* $$ param */ ASource);
{
  coresysbasefuncs_nothing();
} // func
  
// ------------------

/* operators */


enum comparison /* $$ func */ coresysdecfloatsdf64s_compare
  (/* $$ in */ const decfloat64_t /* $$ param */ A,
   /* $$ in */ const decfloat64_t /* $$ param */ B);
{
  enum comparison /* $$ var */ Result = comparison_equal;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysdecfloatsdf64s_equal
  (/* $$ in */ const decfloat64_t /* $$ param */ A,
   /* $$ in */ const decfloat64_t /* $$ param */ B);
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysdecfloatsdf64s_different
  (/* $$ in */ const decfloat64_t /* $$ param */ A,
   /* $$ in */ const decfloat64_t /* $$ param */ B)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

// ------------------

/* rtti */
 
bool /* $$ func */ coresysdecfloatsdf64s_rttiisdefaultvalue
  (/* $$ in */ const pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

bool /* $$ func */ coresysdecfloatsdf64s_rttitryapplydefaultvalue
  (/* $$ inout */ nonconst pointer* /* $$ param */ AValue)
{
  bool /* $$ var */ Result = false;
  // ---
     
     
  // ---
  return Result;
} // func

void /* $$ func */ coresysdecfloatsdf64s_rttiapplydefaultvalue
  (/* $$ inout */ const pointer* /* $$ param */ AValue)
{
  coresysbasefuncs_nothing();
} // func

// ------------------

 // ...
 
// ------------------

/* $$ override */ int /* $$ func */ coresysdecfloatsdf64s_start
  ( noparams )
{
  coresysbasefuncs_nothing();
} // func

/* $$ override */ int /* $$ func */ coresysdecfloatsdf64s_finish
  ( noparams )
{
  coresysbasefuncs_nothing();
} // func

// ------------------


 // ...

// $$ } // namespace coresysdecfloatsdf64s