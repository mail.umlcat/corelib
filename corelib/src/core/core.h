/** Module: "core"
 ** Filename: "core.h"
 ** Qualified Identifier:
 ** "core" 
 ** Descr.:
 ** "Global (Root) Core Library Package Module"
 **/

// namespace core {
 
// ------------------
 
#ifndef CORE_H
#define CORE_H
 
// ------------------

// "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <float.h>
#include <time.h>
//#include <chrono.h>
#include <stdio.h>
#include <math.h>

// ------------------

#include "C:\softdev\corelib\corelib\src\core\sys\coresysmacros.h"

// ------------------

#define MOD_none {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
#define MOD_core {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01};

// ------------------


 // ...
 
// ------------------

/* override */ int /* func */ core__setup
  ( noparams );

/* override */ int /* func */ core__setoff
  ( noparams );

// ------------------

#endif // CORE_H

// } // namespace core