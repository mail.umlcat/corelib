/** Module: "coresysfmt.c"
 ** Descr.: "predefined library"
 **/
 
// namespace coresysfmt {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysuints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresyssints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\coresyschars.h"
#include "C:\softdev\corelib\corelib\src\core\sys\strs\coresysstrs.h"

// ------------------

// --> contained modules here
#include "corefmtansinullfmt.h"
// .include "coresysfmtbinfloat32s.h"
// .include "coresysfmtbinfloat64s.h"
// .include "coresysfmtbinfloat128s.h"
// .include "coresysfmtbinfloat256s.h"
// .include "coresysfmtlongfloats.h"

// ------------------

// --> this same module header goes here
#include "coresysfmt.h"
 
// ------------------


// ------------------

 // ...
 
// ------------------

/* override */ int /* func */ coresysfmt__start
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysbasefuncs__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ coresysfmt__finish
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  coresysbasefuncs__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace coresysfmt