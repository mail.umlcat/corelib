/** Module: "corefmtansinullfmt.c"
 ** Descr.: "predefined library"
 **/
 
// namespace corefmtansinullfmt {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\corefmtbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\corefmtmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\corefmtchars.h"

// ------------------

// --> same package libraries here
// .include "corefmtansinullfmtbinfloat16s.h"
// .include "corefmtansinullfmtbinfloat32s.h"
// .include "corefmtansinullfmtbinfloat64s.h"
// .include "corefmtansinullfmtbinfloat128s.h"
// .include "corefmtansinullfmtbinfloat256s.h"
// .include "corefmtansinullfmtlongfloats.h"

// ------------------

// --> this same module header goes here
#include "corefmtansinullfmt.h"
 
// ------------------

/* types */

// ------------------

/* functions */

bool /* func */ corefmtansinullfmt__tryformat
  (/* out */   nonconst ansinullstring*  /* param */ ADestFmtPtr,
   /* in */    const    size_t           /* param */ ADestFmtSize,
   /* in */    const    ansinullstring*  /* param */ ASourceFmtStr,
   /* in */    const    size_t           /* param */ ASourceFmtSize,
   /* inout */ umlcconstantparameters    /* param */ AParams)
{
   
} // func

void /* func */ corefmtansinullfmt__format
  (/* out */   nonconst ansinullstring*  /* param */ ADestFmtPtr,
   /* in */    const    size_t           /* param */ ADestFmtSize,
   /* in */    const    ansinullstring*  /* param */ ASourceFmtStr,
   /* in */    const    size_t           /* param */ ASourceFmtSize,
   /* inout */ umlcconstantparameters    /* param */ AParams)
{
  bool /* var */ CanContinue =
    corefmtansinullfmt__tryformat
	(ADestFmtPtr, ADestFmtSize, ASourceFmtStr, ASourceFmtStr, ASourceFmtSize, AParams);
  if (! CanContinue)
  {
    // raise error
  } // if
} // func

// ------------------

/* procedures */

// ------------------

/* rtti */

// ------------------

 // ...
 
// ------------------

/* override */ int /* func */ corefmtansinullfmt__start
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  corefmtbasefuncs__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corefmtansinullfmt__finish
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  corefmtbasefuncs__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace corefmtansinullfmt