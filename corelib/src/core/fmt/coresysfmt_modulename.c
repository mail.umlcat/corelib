/** Module: "corefmt<modulename>s.c"
 ** Descr.: "predefined library"
 **/
 
// namespace corefmt<modulename>s {
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\corefmtbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\corefmtmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\corefmtchars.h"

// ------------------

// --> same package libraries here
// .include "corefmt<modulename>sbinfloat16s.h"
// .include "corefmt<modulename>sbinfloat32s.h"
// .include "corefmt<modulename>sbinfloat64s.h"
// .include "corefmt<modulename>sbinfloat128s.h"
// .include "corefmt<modulename>sbinfloat256s.h"
// .include "corefmt<modulename>slongfloats.h"

// ------------------

// --> this same module header goes here
#include "corefmt<modulename>s.h"
 
// ------------------

/* types */

// ------------------

/* functions */

// ------------------

/* procedures */

// ------------------

/* rtti */

// ------------------

 // ...
 
// ------------------

/* override */ int /* func */ corefmt<modulename>s__start
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  corefmtbasefuncs__nothing();

  // ---
  return Result;
} // func

/* override */ int /* func */ corefmt<modulename>s__finish
  ( noparams )
{
  int /* var */ Result = 0;
  // ---
     
  corefmtbasefuncs__nothing();

  // ---
  return Result;
} // func

// ------------------

// } // namespace corefmt<modulename>s