/** Module: "coresys<packagename>s.h"
 ** Descr.: "..."
 **/
 
// namespace coresys<packagename>s {
 
// ------------------
 
#ifndef CORESYS<PACKAGENAME>S__H
#define CORESYS<PACKAGENAME>S__H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\coresysbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\coresysmemints.h"

// ------------------

// --> contained modules here
// .include "coresys<packagename>sbinfloat16s.h"
// .include "coresys<packagename>sbinfloat32s.h"
// .include "coresys<packagename>sbinfloat64s.h"
// .include "coresys<packagename>sbinfloat128s.h"
// .include "coresys<packagename>sbinfloat256s.h"
// .include "coresys<packagename>slongfloats.h"

// ------------------

/* rtti */

#define MOD_coresys<packagename>s {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* override */ int /* func */ coresys<packagename>s__start
  ( noparams );

/* override */ int /* func */ coresys<packagename>s__finish
  ( noparams );

// ------------------

#endif // CORESYS<PACKAGENAME>S__H

// } // namespace coresys<packagename>s