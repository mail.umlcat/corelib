/** Module: "corefmt<modulename>s.h"
 ** Descr.: "..."
 **/
 
// namespace corefmt<modulename>s {
 
// ------------------
 
#ifndef COREFMT<MODULENAME>S__H
#define COREFMT<MODULENAME>S__H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\corefmtbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\corefmtmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\corefmtchars.h"

// ------------------

// --> same package libraries here
// .include "corefmt<modulename>sbinfloat16s.h"
// .include "corefmt<modulename>sbinfloat32s.h"
// .include "corefmt<modulename>sbinfloat64s.h"
// .include "corefmt<modulename>sbinfloat128s.h"
// .include "corefmt<modulename>sbinfloat256s.h"
// .include "corefmt<modulename>slongfloats.h"

// ------------------

/* types */

// ------------------

/* functions */

// ------------------

/* procedures */

// ------------------

/* rtti */

#define MOD_corefmt<modulename>s {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* override */ int /* func */ corefmt<modulename>s__start
  ( noparams );

/* override */ int /* func */ corefmt<modulename>s__finish
  ( noparams );

// ------------------

#endif // COREFMT<MODULENAME>S__H

// } // namespace corefmt<modulename>s