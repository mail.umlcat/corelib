/** Module: "corefmtansinullfmt.h"
 ** Descr.: "..."
 **/
 
// namespace corefmtansinullfmt {
 
// ------------------
 
#ifndef COREFMTANSINULLFMT__H
#define COREFMTANSINULLFMT__H
 
// ------------------

// --> "C" predefined libraries here
#include <stdlib.h>
#include <stddef.h>
#include <setjmp.h>
#include <string.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdio.h>
#include <math.h>

// ------------------

// --> basic libraries here
#include "C:\softdev\corelib\corelib\src\core\sys\base\corefmtbase.h"
#include "C:\softdev\corelib\corelib\src\core\sys\memints\corefmtmemints.h"
#include "C:\softdev\corelib\corelib\src\core\sys\chars\corefmtchars.h"

// ------------------

// --> same package libraries here
// .include "corefmtansinullfmtbinfloat16s.h"
// .include "corefmtansinullfmtbinfloat32s.h"
// .include "corefmtansinullfmtbinfloat64s.h"
// .include "corefmtansinullfmtbinfloat128s.h"
// .include "corefmtansinullfmtbinfloat256s.h"
// .include "corefmtansinullfmtlongfloats.h"

// ------------------

/* types */

// ------------------

/* functions */

bool /* func */ corefmtansinullfmt__tryformat
  (/* out */   nonconst ansinullstring*  /* param */ ADestFmtPtr,
   /* in */    const    size_t           /* param */ ADestFmtSize,
   /* in */    const    ansinullstring*  /* param */ ASourceFmtStr,
   /* in */    const    size_t           /* param */ ASourceFmtSize,
   /* inout */ umlcconstantparameters    /* param */ AParams);

void /* func */ corefmtansinullfmt__format
  (/* out */   nonconst ansinullstring*  /* param */ ADestFmtPtr,
   /* in */    const    size_t           /* param */ ADestFmtSize,
   /* in */    const    ansinullstring*  /* param */ ASourceFmtStr,
   /* in */    const    size_t           /* param */ ASourceFmtSize,
   /* inout */ umlcconstantparameters    /* param */ AParams);
   
// ------------------

/* procedures */

// ------------------

/* rtti */

#define MOD_corefmtansinullfmt {0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

// ------------------

 // ...
 
// ------------------

/* override */ int /* func */ corefmtansinullfmt__start
  ( noparams );

/* override */ int /* func */ corefmtansinullfmt__finish
  ( noparams );

// ------------------

#endif // COREFMTANSINULLFMT__H

// } // namespace corefmtansinullfmt
